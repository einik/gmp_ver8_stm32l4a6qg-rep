#ifndef __UTILS_H__
#define __UTILS_H__

#include "common.h"


typedef enum {
	UTILS_RESULT_OK,
	UTILS_RESULT_WRONG_PARAM,
	UTILS_RESULT_ERROR
}UTILS_RESULT_E;


uint8_t ascii2Hex(uint8_t char1, uint8_t char2);



#endif //__UTILS_H__
