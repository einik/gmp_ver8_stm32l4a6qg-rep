#ifndef __ADS_H__
#define __ADS_H__

#include "global_defs.h"
#include "common.h"
#include "cmsis_os.h"



#define ADS_SET_LENGTH = 11; //TBD - check defines
#define ADS_EVB_CONNECTED
#define WREG 	0x40
#define CONFIG1	0x01

#define ADS_REG_CHA_1_GAIN_3	0X30
#define ADS_REG_CHA_1_GAIN_6	0X00
#define ADS_REG_CHA_1_GAIN_12	0X60
		

#define ADS_DATA_START_TOKEN	0xC0

#define ADS_GAIN_3
//#define ADS_GAIN_6
//#define ADS_GAIN_12

#if defined (ADS_GAIN_3)
	#define ADS_GAIN			3
	#define ADS_REG_CHA_1		ADS_REG_CHA_1_GAIN_3
#elif defined (ADS_GAIN_6)
	#define ADS_GAIN			6
	#define ADS_REG_CHA_1		ADS_REG_CHA_1_GAIN_6
#elif defined (ADS_GAIN_12)
	#define ADS_GAIN			12
	#define ADS_REG_CHA_1		ADS_REG_CHA_1_GAIN_12
#endif


#define ADS_VREF			2.42
#define ADS_UNITS 			0x7FFFFF//((2 << 23) - 1)//8388607


#define ADS_CONVERSION_CONST (ADS_VREF / (double)(ADS_GAIN * ADS_UNITS) )

//#define ECG_BUFFER_LENGTH       G_ECG_NUMBER_OF_SAMPLES
//#define ECG_DMA_RX_DATA_LEN     6       // 24 bit START + 24 bit x N channels
#define ECG_DATA_LEN            4
#define LOFF_STAT_MASK          

#define LEAD_OFF_SAMPLE 		0x7FFFFF
#define ADS_DMA_RX_DATA_LEN     9//6       // 24 bit START + 24 bit x N channels

typedef enum{
	ADS_LEAD_STATUS_ALL_ON	= 0,
	ADS_LEAD_STATUS_LA		= 0x01,
	ADS_LEAD_STATUS_RA		= 0x02,
	ADS_LEAD_STATUS_LL		= 0x04,
	ADS_LEAD_STATUS_RL		= 0x10
}ADS_LEAD_STATUS;

typedef struct ecg_sample { uint8_t x[ADS_DMA_RX_DATA_LEN]; } ecg_sample;
/*
  ECG DATA structure: STAT_WORD: 0xC + LOFF_STAT[5 bits] + GPIO[2 bits] + 13x'0'
                      [24 bits] x CHANNEL ...
  ECG DMA is to be written to buffer backwards: start at the end 
              then, copy LOFF_STAT CHANNEL bits to the 4th byte, 
              then, override last 2 bytes with new input                      */

typedef enum {
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_0,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_11_25,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_22_5,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_33_75,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_45,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_56_25,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_67_5,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_78_75,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_90,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_101_25,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_112_5,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_123_75,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_135,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_146_25,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_157_5,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_168_75,
	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_MAX
}ADS_PARAMS_RESPIRATION_PHASE_SHIFT_ENUM;

typedef enum {
	ADS_PARAMS_RESPIRATION_CLOCK_FREQ_32KHZ,
	ADS_PARAMS_RESPIRATION_CLOCK_FREQ_64KHZ,
	ADS_PARAMS_RESPIRATION_CLOCK_FREQ_MAX
}ADS_PARAMS_RESPIRATION_CLOCK_FREQ_ENUM;

typedef enum {
	ADS_PARAMS_GAIN_1,
	ADS_PARAMS_GAIN_2,
	ADS_PARAMS_GAIN_3,
	ADS_PARAMS_GAIN_4,
	ADS_PARAMS_GAIN_6,
	ADS_PARAMS_GAIN_8,
	ADS_PARAMS_GAIN_12,
	ADS_PARAMS_GAIN_MAX
}ADS_PARAMS_GAIN_ENUM;

typedef enum {
	ADS_PARAMS_INIT_TYPE_REGULAR,
	ADS_PARAMS_INIT_TYPE_TEST_SIGNAL_ENABLED,
	ADS_PARAMS_INIT_TYPE_BIT,
	ADS_PARAMS_INIT_TYPE_IMPEDANCE,
	ADS_PARAMS_TEST_SIGNAL_MAX
}ADS_PARAMS_INIT_TYPE_ENUM;

typedef enum {
	ADS_RESULT_OK,
	ADS_RESULT_ERROR,
	ADS_RESULT_ERROR_WRONG_PARAM,
	ADS_RESULT_ERROR_ALREADY_RUNNING,
	ADS_RESULT_ERROR_NOT_RUNNING,
	ADS_RESULT_ERROR_TX_FAILED,
	ADS_RESULT_ERROR_RX_FAILED,
	ADS_RESULT_ERROR_VERIFICATION_FAILED,
	ADS_RESULT_ERROR_SPI_DUMMY_BUFF,
	ADS_RESULT_ERROR_INIT_FAILED
}ADS_RESULT_E;

typedef enum{
	ADS_STATE_IDLE,
	ADS_STATE_RUNNING,
	ADS_STATE_NOT_INITIALIZED,
	ADS_STATE_ERROR,
	ADS_STATE_BIT
}ADS_STATE_E;


typedef enum {
 ADS_SAMPLE_RATE_250_HZ,
 ADS_SAMPLE_RATE_500_HZ,
 ADS_SAMPLE_RATE_MAX
}ADS_SAMPLE_RATE_E;

typedef enum {
	ADS_PARAMS_RESPIRATION_ENABLED_OFF,
	ADS_PARAMS_RESPIRATION_ENABLED_ON,
	ADS_PARAMS_RESPIRATION_ENABLED_MAX
}ADS_PARAMS_RESPIRATION_ENABLED_E;

typedef struct {
	ADS_PARAMS_GAIN_ENUM						gain;
	ADS_PARAMS_GAIN_ENUM						respiration_gain;
	ADS_SAMPLE_RATE_E                   		sample_rate;
	ADS_PARAMS_RESPIRATION_ENABLED_E			resp_chan_en;

	ADS_PARAMS_RESPIRATION_PHASE_SHIFT_ENUM		respiration_phase_shift;
	ADS_PARAMS_RESPIRATION_CLOCK_FREQ_ENUM		respiration_clock_freq;
	ADS_PARAMS_INIT_TYPE_ENUM					init_type;
}ADS_PARAMS_STRUCT;

/***********************************************************************
 * Function name: ads_init
 * Description: Initialize ADS
 * Parameters :
 *		osMessageQId - Message queue ID
 *		bit - Boolean pararmter for BIT
 *
 * Returns :
 *		ADS_RESULT_OK - on success
 *		ADS_RESULT_ERROR_TX_FAILED - TX Failed
 *		ADS_RESULT_ERROR_RX_FAILED - RX Failed
 *
 ***********************************************************************/
//ADS_RESULT_E ads_init(osMessageQId ECGQueueHandle);
ADS_RESULT_E adsInit(ADS_PARAMS_STRUCT* ads_params_struct);
/***********************************************************************
 * Function name: adsStart
 * Description: Start ADS
 * Parameters :
 *		void
 *
 * Returns :
 *		ADS_RESULT_OK - on success
 *
 ***********************************************************************/
ADS_RESULT_E adsStart(ADS_PARAMS_STRUCT* ads_params_struct);
/***********************************************************************
 * Function name: adsStop
 * Description: Stop ADS
 * Parameters :
 *		void
 *
 * Returns :
 *		ADS_RESULT_OK - on success
 *
 ***********************************************************************/
ADS_RESULT_E adsStop(void);
//uint8_t u8ECG_ADS_Poll();
/***********************************************************************
 * Function name: ecg_ads_dma_rx_callback
 * Description: A callback for receiving data from interrupt
 * Parameters :
 *		void
 *
 * Returns :
 *		ADS_RESULT_OK - on success
 *		ADS_RESULT_ERROR_RX_FAILED - RX failed
 *
 ***********************************************************************/
ADS_RESULT_E ecg_ads_dma_rx_call_back();//ECG_ADS_DMA_Rx
//void    ECG_Toggle_Buffers();
//void    ECG_ADS_DMA_IncIndx();
/***********************************************************************
 * Function name: ads_spi_complete_irq_call_back
 * Description: A callback for receiving data from interrupt
 * Parameters :
 *		void
 *
 * Returns :
 *		ADS_RESULT_OK - on success
 *
 ***********************************************************************/
ADS_RESULT_E ads_spi_complete_irq_callback();


ADS_RESULT_E ads_square_init(ADS_PARAMS_STRUCT* ads_params_struct);
ADS_RESULT_E ads_input_shorted_init(ADS_PARAMS_STRUCT* ads_params_struct);
ADS_RESULT_E ads_read_reg(void);
ADS_RESULT_E ads_bit(void);
ADS_RESULT_E ads_init_resp(ADS_PARAMS_STRUCT* ads_params_struct);
ADS_RESULT_E adsFirstInit(void);

void adsSampleReceived(int32_t ecg1_sample, int32_t ecg2_sample, int32_t resp_sample ,uint8_t lead);
ADS_RESULT_E adsPrintIntCount();

ADS_RESULT_E adsCheckSpiDummyBuff();

#endif  //__ADS_H__
