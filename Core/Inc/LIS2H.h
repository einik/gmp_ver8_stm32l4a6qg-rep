#ifndef __LIS2H_H__
#define __LIS2H_H__

typedef enum LIS2H_RESULT_E{
	LIS2H_RESULT_OK,
	LIS2H_RESULT_ERROR_ALREADY_INITIALIZED,
	LIS2H_RESULT_ERROR_INIT_FAILED,
	LIS2H_RESULT_ERROR,
	LIS2H_RESULT_ERROR_WRITE_REGISTER_FAILED,
	LIS2H_RESULT_ERROR_READ_REGISTER_FAILED,
	LIS2H_RESULT_ERROR_INVALID_AXIS,
	LIS2H_RESULT_ERROR_SIZE_INVALID
}LIS2H_RESULT_E;

typedef enum{
	LIS2H_AXIS_X,
	LIS2H_AXIS_Y,
	LIS2H_AXIS_Z	
}LIS2H_AXIS_E;

typedef struct{
	int16_t		x;
	int16_t		y;
	int16_t		z;
}LIS2H_AXIS;

/***********************************************************************
 * Function name: lis2hInit
 * Description: Init LIS2H
 * Parameters : 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hInit(void);
/***********************************************************************
 * Function name: lis2hWriteReg
 * Description: Write LIS2H registers
 * Parameters : address - register address, data - data to write in register
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hWriteReg(uint8_t address, uint8_t data);
/***********************************************************************
 * Function name: lis2hGetAllAxis 
 * Description: Read all axis from registers
 * Parameters : x - position on x axis, y - position on y axis, z - position on z axis
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *		LIS2H_RESULT_ERROR_READ_REGISTER_FAILED - register read failed
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hGetAllAxis(LIS2H_AXIS* axis);
/***********************************************************************
 * Function name: lis2hGetXAxis
 * Description: Read and calculate x axis from registers
 * Parameters : x - 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *		LIS2H_RESULT_ERROR_READ_REGISTER_FAILED - register read failed
 *		LIS2H_RESULT_ERROR_INVALID_AXIS - wrong axis
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hGetAxis(LIS2H_AXIS_E axis, uint8_t* position);
/***********************************************************************
 * Function name: lis2hSleep 
 * Description: LIS2H sleep
 * Parameters : 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR_READ_REGISTER_FAILED - register read failed
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hSleep(void);
LIS2H_RESULT_E lis2hPrintRegisters(void);
#endif //__LIS2H_H__
