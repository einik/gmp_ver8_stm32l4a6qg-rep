/*
 * HRV_Parameters_Calculations.h
 *
 *  Created on: 20/02/2015 by Yariv Amos
 */

#ifndef HRV_CALCULATION_H_
#define HRV_CALCULATION_H_

#define HRV_CAL_CYCLE 5
#define HRV_MIN_QRS_TO_ANALYZE 6
   
typedef struct HRV_Parameters
{
	float32_t RRI_LENGTH,Mean,VARNN,VLF,LF,HF,HF_SRESS; 
}HRV_param; 

#define DefaultRRI 1.166667 //correlate to 70 bpm
////////////////////
// Init Function  //
////////////////////
void  InitiateBergerHR(void);
void  InitializeHRV(void);

//struct HRV_Parameters Calculate_HRV(long double *newQRS_Time,short* QRS_Codes, short newRRLength, short FirstBalk);
float32_t HRV_Runner(uint16_t QRS_Array[], uint16_t QRS_Last_Index, struct HRV_Parameters *HRVoutput);
void HRV_calc_mean(uint16_t QRS_Array[], uint16_t QRS_size, float32_t * mean);
void HRV_reset();
//void MYAPI_API HRV_MussiveRunnner(void);

#endif  //HRV_CALCULATION_H_ 
