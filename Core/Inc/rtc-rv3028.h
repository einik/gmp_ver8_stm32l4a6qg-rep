#ifndef __RTC_RV_3028_H__
#define __RTC_RV_3028_H__

#include "stm32l4xx_hal.h"
#include "common.h"
#include <stdbool.h>

typedef enum
{
   RV3028_OK = 0,
   RV3028_ERR,
   RV3028_TIMEOUT_ERR,
}RV3028_Errors;

typedef struct{
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
}RV30287_rtc_time;

RV3028_Errors RV3028_Write_Time(I2C_HandleTypeDef* hi2c2,RV30287_rtc_time *tm);
RV3028_Errors RV3028_Read_Time(I2C_HandleTypeDef* hi2c2, RV30287_rtc_time *tm);

#endif  //__RTC_RV_3028_H__