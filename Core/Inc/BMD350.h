#ifndef __BMD350_H__
#define __BMD350__

#include "cmsis_os.h"

#define BMD_AT_COMMAND_BAUD_RATE							"at$ubr"
#define BMD_AT_COMMAND_FLOW_CONTROL_ENABLE					"at$ufc"
#define BMD_AT_COMMAND_PARITY_ENABLE						"at$upar"
#define BMD_AT_COMMAND_PASS_THROUGH							"at$uen"
#define BMD_AT_COMMAND_GET_MAC_ADDRESS						"at$mac?"
#define BMD_AT_COMMAND_CONFIGURE_STATUS_PIN					"at$gstc 4"
#define BMD_AT_COMMAND_READ_DEVICE_NAME						"at$name?"
#define BMD_AT_COMMAND_WRITE_DEVICE_NAME					"at$name"
#define BMD_AT_COMMAND_CONNECTABLE_ADV_ENABLE				"at$conadv 1"
#define BMD_AT_COMMAND_CONNECTABLE_ADV_DISABLE				"at$conadv 0"
#define BMD_AT_COMMAND_CONNECTABLE_ADV						"at$conadv"
#define BMD_AT_COMMAND_RESET								"at$restart"

typedef enum{
	BMD_AT_COM_NONE,
	BMD_AT_COM_RESET,
	BMD_AT_COM_CONNECTABLE_ADV_DISABLE,
	BMD_AT_COM_BAUD_RATE, 
	BMD_AT_COM_FLOW_CONTROL_ENABLE,
	BMD_AT_COM_PARITY_ENABLE,
	BMD_AT_COM_PASS_THROUGH, 
	BMD_AT_COM_GET_MAC_ADDRESS, 
	BMD_AT_COM_CONFIGURE_STATUS_PIN,
	BMD_AT_COM_READ_DEVICE_NAME,
	BMD_AT_COM_WRITE_DEVICE_NAME,
	BMD_AT_COM_CONNECTABLE_ADV_ENABLE,
	BMD_AT_COM_MAX
}BMD_AT_COMMANDS_E;

typedef struct{
	BMD_AT_COMMANDS_E	at_index;
	char*				at_command;
}BMD_AT_COMMANDS;


static const BMD_AT_COMMANDS bmd_at_commands[] = {
	{.at_index = BMD_AT_COM_NONE, 					.at_command = ""},
	{.at_index = BMD_AT_COM_RESET,					.at_command = BMD_AT_COMMAND_RESET},
	{.at_index = BMD_AT_COM_CONNECTABLE_ADV_DISABLE,.at_command = BMD_AT_COMMAND_CONNECTABLE_ADV_DISABLE},
	{.at_index = BMD_AT_COM_BAUD_RATE, 				.at_command = BMD_AT_COMMAND_BAUD_RATE},
	{.at_index = BMD_AT_COM_FLOW_CONTROL_ENABLE,	.at_command = BMD_AT_COMMAND_FLOW_CONTROL_ENABLE},
	{.at_index = BMD_AT_COM_PARITY_ENABLE, 			.at_command = BMD_AT_COMMAND_PARITY_ENABLE},
	{.at_index = BMD_AT_COM_PASS_THROUGH, 			.at_command = BMD_AT_COMMAND_PASS_THROUGH},
	{.at_index = BMD_AT_COM_GET_MAC_ADDRESS, 		.at_command = BMD_AT_COMMAND_GET_MAC_ADDRESS},
	{.at_index = BMD_AT_COM_CONFIGURE_STATUS_PIN, 	.at_command = BMD_AT_COMMAND_CONFIGURE_STATUS_PIN},
	{.at_index = BMD_AT_COM_READ_DEVICE_NAME, 		.at_command = BMD_AT_COMMAND_READ_DEVICE_NAME},
	{.at_index = BMD_AT_COM_WRITE_DEVICE_NAME, 		.at_command = BMD_AT_COMMAND_WRITE_DEVICE_NAME},
	{.at_index = BMD_AT_COM_CONNECTABLE_ADV_ENABLE, .at_command = BMD_AT_COMMAND_CONNECTABLE_ADV_ENABLE}};

#define AT_COMMAND(num, value) 					\
do {                                            \
	if (bmd_at_commands[num].at_index != num)	\
		value = NULL;                           \
	else                                        \
		value = bmd_at_commands[num].at_command;  \
} while (0);
	


typedef enum{
	BMD_RESULT_OK,
	BMD_RESULT_ERROR_ALREADY_INITIALIZED,
	BMD_RESULT_ERROR_NOT_RUNNING,
	BMD_RESULT_ERROR,
	BMD_RESULT_ERROR_WRONG_PARAM,
	BMD_RESULT_ERROR_INIT_FAILED,
	BMD_RESULT_ERROR_COMMAND_TOO_LONG,
	BMD_RESULT_ERROR_AT_COMMAND_FAILED
}BMD_RESULT_E;

typedef enum{
	BMD_MODE_NORMAL,
	BMD_MODE_AT_COMMAND	
}BMD_MODE_E;

typedef enum{
	BMD_COMM_MODE_ENABLE,
	BMD_COMM_MODE_DISABLE
}BMD_COMM_MODE_E;

typedef enum{
	BMD_EVENT_BLE_CONNECTED 		= 0,
	BMD_EVENT_BLE_DISCONNECTED 		= 1
}BMD_EVENT_E;

typedef enum{
	BMD_POWER_ON,
	BMD_POWER_OFF	
}BMD_POWER_E;



/***********************************************************************
 * Function name: bmdInit
 * Description: Init BMD350 BLE module
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdInit(void);
/***********************************************************************
 * Function name: bmdAtCommandSend
 * Description: Set mode 
 * Parameters : data1 - pointer to first data
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR_COMMAND_TOO_LONG - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdAtCommandSend(IN uint8_t* data1, 
						  IN uint8_t* data2);
/***********************************************************************
 * Function name: bmdAtCommandMode
 * Description: Set BMD350 module in or out of AT COMMANDS MODE
 * Parameters : BMD_OPERATION_MODE mode - AT command mode
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdAtCommandMode(IN BMD_MODE_E mode);
/***********************************************************************
 * Function name: bmdReset
 * Description: Reset BMD350 module
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdReset(void);
/***********************************************************************
 * Function name: bmdGetDataFromBle
 * Description: Configure UART DMA to receive data asyncorinos
 * Parameters : data - pointer to data, length - length of data to receive
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdGetDataFromBle(INOUT uint8_t* data, 
							   IN uint16_t length);
/***********************************************************************
 * Function name: bmdStopReadingDataFromBle
 * Description: Stop UART DMA 
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdStopReadingDataFromBle(void);
/***********************************************************************
 * Function name: bmdGetDataLength
 * Description: Get length of data buffer size left 
 * Parameters : length - pointer to data length
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdGetDataLength(OUT uint16_t* length);
/***********************************************************************
 * Function name: bmdModeGet
 * Description: Get BMD350 mode
 * Parameters : mode - pointer to BMD mode
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdModeGet(OUT BMD_MODE_E* mode);
/***********************************************************************
 * Function name: bmdSendDataToBle
 * Description: Send data to BLE
 * Parameters : ptr - pointer for data to send, length - length of data to send
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdSendDataToBle(IN uint8_t *ptr, 
							  IN uint16_t length);
/***********************************************************************
 * Function name: bmdBleCommSet
 * Description: Set UART mode enable/disable
 * Parameters : mode - enable/disable UART
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdBleCommSet(IN BMD_COMM_MODE_E mode);
/***********************************************************************
 * Function name: bmdPowerSet
 * Description: Set Rigado power on/off
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdPowerSet(BMD_POWER_E power);
#endif  //__BMD350_H__
