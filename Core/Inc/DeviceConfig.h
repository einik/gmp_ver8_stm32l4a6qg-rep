#ifndef __DEVICE_CONFIG_H__
#define __DEVICE_CONFIG_H__

#include "ecg.h"

// DEFINITIONS

typedef enum {
	DEVICE_CONFIG_RESULT_OK,
	DEVICE_CONFIG_RESULT_WRONG_PARAM,
	DEVICE_CONFIG_RESULT_FILE_NOT_FOUND,
	DEVICE_CONFIG_RESULT_SD_ERROR,
	DEVICE_CONFIG_RESULT_NEW_FILE_CREATED_WITH_DEFAULTS,
	DEVICE_CONFIG_RESULT_ERROR,
	DEVICE_CONFIG_RESULT_FAILED_TO_DELETE_PREVIOUS_CONFIG
}DEVICE_CONFIG_RESULT_E;

//
//typedef enum {
//	DEVICE_CONFIG_MODE_HOLTER_ONLY,
//	DEVICE_CONFIG_MODE_PATCH,
//	DEVICE_CONFIG_MODE_MAX
//}DEVICE_CONFIG_MODE_E;

//
//typedef enum {
//	DEVICE_CONFIG_PACE_MAKER_OFF,
//	DEVICE_CONFIG_PACE_MAKER_ON,
//	DEVICE_CONFIG_PACE_MAKER_MAX
//}DEVICE_CONFIG_PACE_MAKER_E;

typedef struct {
	ECG_GENERAL_CONFIG_DEVICE_MODE_E 						device_mode;
	ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_ENUM 	pace_maker_enable;
}DEVICE_CONFIG_STRUCT;


// FUNCTIONS

/***********************************************************************
 * Function name: deviceConfigGet
 * Description: Load configuration from SD card
 * Parameters : conf - pointer to supported configuration structure
 *
 * Returns :
 *		DEVICE_CONFIG_RESULT_OK - on success
 *		DEVICE_CONFIG_RESULT_E - for other failures
 *
 ***********************************************************************/
DEVICE_CONFIG_RESULT_E deviceConfigGet(DEVICE_CONFIG_STRUCT * conf);

/***********************************************************************
 * Function name: deviceConfigChangeMode
 * Description: Change and save configuration to SD card
 * Parameters : conf - pointer to supported configuration structure
 *
 * Returns :
 *		DEVICE_CONFIG_RESULT_OK - on success
 *		DEVICE_CONFIG_RESULT_E - for other failures
 *
 ***********************************************************************/
DEVICE_CONFIG_RESULT_E deviceConfigChangeMode(DEVICE_CONFIG_STRUCT * conf);

#endif // __DEVICE_CONFIG_H__
