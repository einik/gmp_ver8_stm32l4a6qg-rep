#ifndef __PBS_H__
#define __PBS_H__

#include "common.h"
#include "ecg.h"

#include <stdbool.h>

#define PBS_FLOW_CONTROL_MAX						100
					
#define PBS_MCU_S_N_LEN 							12
#define PBS_BLE_MAC_LEN 							6
#define PBS_DEVICE_S_N_LEN 							13
#define PBS_DEVICE_NAME_MAX_LEN 					26
#define PBS_TIMESTAMP_LENGTH						8	// "YYYYMMDDHHmmss<ms><ms><ms>"

#define DEVICE_TYPE_PATCH							1	// PATCH

#define PBS_ECG_REMOTE_EVENT_DURATION_SEC_MIN 		30
#define PBS_ECG_REMOTE_EVENT_DURATION_SEC_MAX 		300

#define PBS_ECG_EVENT_IND_HEADER_FC					(-1)

//Headers
#define PBS_HEADER              					0xAAAA
// MACROS			
#define U16_LOW_8(_x)            					(_x & 0xFF)
#define U16_HIGH_8(_x)           					((_x & 0xFF00) >> 8)
#define IS_PBS_HEADER(_x)        					(*_x == U16_LOW_8(PBS_HEADER) && *(_x+1) == U16_HIGH_8(PBS_HEADER))
#define GET_PBS_LEN_FROM_HEADER_LOC(_x)				(((*(_x + 5)) << 8 & 0xFF00) | (*(_x + 4)))
#define PBS_RX_MAX_PACKET_SIZE          			400
#define PBS_RX_MIN_PACKET_SIZE          			6
#define PBS_RX_NUM_OF_BUFFERS           			2
#define PBS_TX_MAX_PACKET_SIZE          			1000//600//64
#define PBS_TX_NUM_OF_BUFFERS           			2
#define PBS_HEADER_LENGTH               			2
#define PBS_TYPE_LENGTH                 			2
#define PBS_LENGTH_LENGTH               			2
#define PBS_TX_OVERHEAD                 			(PBS_HEADER_LENGTH + PBS_LENGTH_LENGTH + PBS_TYPE_LENGTH) 
#define PBS_RX_HEADER_TO_TYPE_OFFSET    			PBS_HEADER_LENGTH
#define PBS_RX_HEADER_TO_LEN_OFFSET     			(PBS_RX_HEADER_TO_TYPE_OFFSET + PBS_TYPE_LENGTH)
#define PBS_RX_HEADER_TO_VALUE_OFFSET   			(PBS_RX_HEADER_TO_LEN_OFFSET + PBS_LENGTH_LENGTH)
#define PBS_TLV_TYPE_SIZE          					2
#define PBS_TLV_LENGTH_SIZE         				2
#define PBS_TLV_VALUE_OFFSET    					(PBS_TLV_TYPE_SIZE + PBS_TLV_LENGTH_SIZE)
#define PBS_TLV_SUB_HEADER_SIZE						(PBS_TLV_TYPE_SIZE + PBS_TLV_LENGTH_SIZE)
			
#define PBS_CRC16_LENGTH							2
			
#define PBS_TX_TIMEOUT 								1000     		//osWaitForever
#define PBS_INCOMPLETE_MSG_TOLLERANCE   			20
#define PBS_TLV_ARRAY_MAX_LENGTH        			30	//9//7

#define PBS_SD_EVENT_MAP_FILE_NAME					"events.txt"

#define PBS_TIMESTAMP_INVALID_PATTERN				0xFF

typedef enum {
   PBS_OUTPUT_TYPE_OFF,
   PBS_OUTPUT_TYPE_SD_ONLY,
   PBS_OUTPUT_TYPE_BLE_ONLY,
   PBS_OUTPUT_TYPE_SD_BLE,
   PBS_OUTPUT_TYPE_MAX
}PBS_OUTPUT_TYPE_E;

typedef enum {
   PBS_OUTPUT_TYPE_HOURLY_OFF = 0,
   PBS_OUTPUT_TYPE_BLE_HOURLY = 1,
   PBS_OUTPUT_TYPE_HOURLY_MAX
}PBS_HOURLY_TYPE_E;



typedef enum {
	PBS_CONNECTION_STATE_CONNECTED,
	PBS_CONNECTION_STATE_DISCONNECTED,
	PBS_CONNECTION_STATE_MAX
}PBS_CONNECTION_STATE_ENUM;

typedef enum {
	PBS_RESULT_OK,
	PBS_RESULT_ERROR,
	PBS_RESULT_ERROR_WRONG_PARAM,
	PBS_RESULT_ERROR_CRC,
	PBS_RESULT_ERROR_NESTED_TLV_TOO_DEEP,
	PBS_RESULT_ERROR_NESTED_TLV_ERROR,
	PBS_RESULT_ERROR_MESSAGE_TOO_LONG_BYTE_ARRAY,
	PBS_RESULT_ERROR_MESSAGE_TOO_LONG_TLV_ARRAY,
	PBS_RESULT_ERROR_UNKNOWN_TYPE,
	PBS_RESULT_ERROR_BLE,
	PBS_RESULT_ERROR_OS,
	PBS_RESULT_ERROR_TX_FLOW_CONTROL_NOT_FOUND,
	PBS_RESULT_ERROR_TX_QUEUE_ERROR,
	PBS_RESULT_ERROR_TYPE_NOT_FOUND,
	PBS_RESULT_ERROR_HEADER_NOT_FOUND,
	PBS_RESULT_ERROR_SD,
	PBS_RESULT_ERROR_FLUSH_BLE,
	PBS_RESULT_ERROR_RETRY_COUNT_EXCEED_MAX,
	PBS_RESULT_ERROR_CYCLIC_BUFFER_NOT_READY,
	PBS_RESULT_NOT_IMPLEMENTED,
	PBS_RESULT_ERROR_ECG_EVENT_NOT_FOUND_IN_MAP_FILE,
	PBS_RESULT_ERROR_ECG_EVENT_MAP_FILE_NOT_FOUND,
	PBS_RESULT_ERROR_ECG_EVENT_MAP_FILE_ERROR,
	PBS_RESULT_ERROR_TX_HALTED,
	PBS_RESULT_ERROR_TIMESTAMP_EMPTY,
	PBS_RESULT_ERROR_STATISTICS_FILE_DOES_NOT_EXISTS,
	PBS_RESULT_ERROR_STATISTICS_FILE_ERROR,
	PBS_RESULT_ERROR_FILE_READ_ERROR,
	PBS_RESULT_ERROR_RTC_READ_ERROR,
	PBS_RESULT_ERROR_ECG_EVENT_IS_ALREADY_ON,
	PBS_RESULT_ERROR_TOO_MANY_FILES,
	PBS_RESULT_ERROR_MAX
}PBS_RESULT_ENUM;

typedef enum {
	SW_FLOW_CONTROL_OFF = 0,
	SW_FLOW_CONTROL_ON = 1,
}SW_FLOW_CONTROL_ENUM;

typedef enum {
	PBS_DEVICE_STATUS_STATE_IDLE = 0,
	PBS_DEVICE_STATUS_STATE_RECORDING  = 1,
	PBS_DEVICE_STATUS_STATE_ERROR = 2,
	PBS_DEVICE_STATUS_STATE_LEAD_DETECTION = 3
}PBS_DEVICE_STATUS_STATE_ENUM;

typedef enum {
	PBS_DEVICE_STATUS_BIT_NO_ERROR                        	= 0x00,
	PBS_DEVICE_STATUS_BIT_ERROR_ECG                       	= 0x01,
	PBS_DEVICE_STATUS_BIT_ERROR_LED                       	= 0x02,
	PBS_DEVICE_STATUS_BIT_ERROR_BLE                       	= 0x04,
	PBS_DEVICE_STATUS_BIT_ERROR_SD                		  	= 0x08,
	PBS_DEVICE_STATUS_BIT_ERROR_OS                		  	= 0x10,
	PBS_DEVICE_STATUS_BIT_ERROR_BATTERY_MONITOR           	= 0x20,
	PBS_DEVICE_STATUS_BIT_ERROR_SD_NOT_ENOUGH_FREE_SPACE  	= 0x40,
	PBS_DEVICE_STATUS_BIT_ERROR_WATCH_DOG					= 0x100
}PBS_DEVICE_STATUS_BIT_ERROR_ENUM;

typedef enum {
	PBS_DEVICE_STATUS_BATTERY_LEVEL_UNKNOWN = -1,
	PBS_DEVICE_STATUS_BATTERY_LEVEL_OK = 0,
	PBS_DEVICE_STATUS_BATTERY_LEVEL_LOW = 1,
	PBS_DEVICE_STATUS_BATTERY_LEVEL_CRITICALLY_LOW = 2
}PBS_DEVICE_STATUS_BATTERY_LEVEL_ENUM;

typedef enum {
	PBS_DEVICE_STATUS_NOT_OWNED = 0,
	PBS_DEVICE_STATUS_OWNED 	=1
}PBS_DEVICE_STATUS_OWNED_ENUM;

typedef enum {
	PBS_DEVICE_STATUS_BUTTON_NOT_PRESSED = 0,
	PBS_DEVICE_STATUS_BUTTON_PRESSED = 1
}PBS_DEVICE_STATUS_BUTTON_ENUM;

typedef enum {
   PBS_COMMAND_GENERAL_GET_INFO_RESP_MCU_SN = 0,
   PBS_COMMAND_GENERAL_GET_INFO_RESP_FW_VER = 1,
   PBS_COMMAND_GENERAL_GET_INFO_RESP_HW_VER = 2,
   PBS_COMMAND_GENERAL_GET_INFO_RESP_BLE_MAC = 3,
   PBS_COMMAND_GENERAL_GET_INFO_RESP_BOOTLOADER_VER = 4,
   PBS_COMMAND_GENERAL_GET_INFO_RESP_DEVICE_S_N = 6,
   PBS_COMMAND_GENERAL_GET_INFO_RESP_DEVICE_TYPE = 7,
}PBS_COMMAND_GENERAL_GET_INFO_RESP_ENUM;

typedef enum {
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_STATE = 0,
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_ERROR_TYPE = 1,
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_BIT_ERROR = 2,
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_BATTERY_LEVEL = 3,
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_TEMPERATURE = 4,
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_OWNED = 5,
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_VOLTAGE = 6,
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_CURRENT = 7,
   PBS_COMMAND_GENERAL_GET_STATUS_RESP_BUTTON = 8,
}PBS_COMMAND_GENERAL_GET_STATUS_RESP_ENUM;

typedef enum {
   PBS_COMMAND_DEVICE_CONTROL_RESET_INTO_DEFAULTS 			= 1,
   PBS_COMMAND_DEVICE_CONTROL_RESET_EXTERNAL_MEMEORY 		= 2,
   PBS_COMMAND_DEVICE_CONTROL_SET_DEVICE_NAME 				= 7,
   PBS_COMMAND_DEVICE_CONTROL_UPDATE_BOOTLOADER 			= 8,
   PBS_COMMAND_DEVICE_CONTROL_SEND_ECG_DATA_FROM_SD_CARD	= 12,
   PBS_COMMAND_DEVICE_CONTROL_SERIAL_INTERFACE_ECHO			= 13,
   PBS_COMMAND_DEVICE_CONTROL_CHANGE_DEVICE_MODE			= 20,
   PBS_COMMAND_DEVICE_CONTROL_RUN_SD_CARD_TEST				= 21
}PBS_COMMAND_DEVICE_CONTROL_ENUM;

typedef enum {
	PBS_COMMAND_DEVICE_CONTROL_RESET_EXTERNAL_MEMEORY_RESET_MEMORY = 1
}PBS_COMMAND_DEVICE_CONTROL_RESET_EXTERNAL_MEMEORY_E;

typedef enum {
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS = 0,
}PBS_COMMAND_DEVICE_CONTROL_RESP_ENUM;

typedef enum {
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_OK 										= 0,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_BATTERY_TOO_LOW 							= 1,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_BOTTLOADER_VERSION_MATCH 					= 2,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_NO_BOOTLOADER_IN_FW 						= 3,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_SD_CARD_READ_FAILED 						= 4,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_SD_CARD_READ_FAILED_DEVICE_IN_MEASUREMENT = 5,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_SD_CARD_READ_FAILED_NO_ECG_DATA			= 6,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_SD_CARD_RESET_MEMORY_FAILED				= 7,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_SET_SERIAL_ECHO_ON_OFF_FAILED				= 13,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_CHANGE_DEVICE_MODE_FAILED					= 20,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_SD_CARD_TEST_FAILED						= 21,
   PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_GENERAL_ERROR 							= 1000,
}PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_ENUM;

typedef enum {
   PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_STATUS = 0
}PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_ENUM;

typedef enum {
   PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_STATUS_OK = 0,
   PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_STATUS_GENERAL_ERROR = 1000,
}PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_STATUS_ENUM;

typedef enum {
   PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_TIME = 1,
   PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LED = 4,
//   PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_OWNER = 6,
   PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_DEVICE_NAME = 7,
}PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_ENUM;


typedef enum {
	PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LEDS_BITMAP_NONE = 0,
	PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LEDS_BITMAP_RED = 1,
	PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LEDS_BITMAP_BLUE = 2,

	PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LEDS_BITMAP_AUTOMATIC = 0x80,
	PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LEDS_BITMAP_MAX
}PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LEDS_BITMAP_ENUM;

typedef enum {
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME = 1,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESET_BUFFER = 2,
}PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_ENUM;

//typedef enum {
//   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_DISABLE = 0,
//   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_ENABLE = 1,
//}PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_ENUM;
//
//typedef enum {
//   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESET_BUFFER_NO_NOT_RESET = 0,
//   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESET_BUFFER_RESET = 1,
//}PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESET_BUFFER_ENUM;

typedef enum {
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS = 0,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_BASE_TIMESTAMP =1
}PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_ENUM;

typedef enum {
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_OK = 0,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_ADS_BIT_FAILED  = 1,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_ALREADY_RUNNING = 2,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_DEVICE_IN_ERROR_STATE = 3,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_TEMPERATURE_TOO_LOW = 4,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_TEMPERATURE_TOO_HIGH = 5,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_REAL_TIME_DATA_DISABLED = 6,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_ECG_MEASUREMENT_DISABLED = 7,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_GENERAL_ERROR = 1000,
}PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_ENUM;

typedef enum {
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_SW_FC_INDEX = -1,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ECG_INDEX = 0,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ECG_STATUS = 1,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_STATUS = 2,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_TIMESTAMP = 3,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_PACE_MAKER_START_INDEX = 4,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_PACE_MAKER_STOP_INDEX = 5,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_RAW_ECG_DATA = 10,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_RAW_ECG_DATA_MAP = 11,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_RAW_ECG_DATA = 12,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_RAW_ECG_DATA_MAP = 13,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_FILTERED_ECG_DATA = 14,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_FILTERED_ECG_DATA_MAP = 15,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_FILTERED_ECG_DATA = 16,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_FILTERED_ECG_DATA_MAP = 17,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_RAW_ECG_DATA = 18,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_RAW_ECG_DATA = 19,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_FILTERED_ECG_DATA = 20,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_FILTERED_ECG_DATA = 21,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_RAW_DATA_COMPRESSED = 40,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_RAW_DATA_COMPRESSED_MAP = 41,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_COMPRESSED_FILTERED_DATA = 42,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_COMPRESSED_FILTERED_DATA_MAP = 43,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_RAW_DATA = 44,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_FILTERED_DATA = 45,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_HEART_RATE = 60,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ECG_PEAKS = 61,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_RATE = 70,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ALGORITHM_EXTENDED_RESULT = 80,
   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_IMPEDANCE					 = 90
}PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ENUM;

typedef enum {
	ECG_MEASUREMENT_REALTIME_PROGRESS_IND_RESP_SW_FLOW_CONTROL_INDEX = 0
}PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_IND_RESP_ENUM;

typedef enum {
	PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT = 1,
	PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_DURATION = 2
}PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_ENUM;

typedef enum {
	PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_DISABLE = 0,
	PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENABLE = 1
}PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENUM;

typedef enum {
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_OK = 0,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_ADS_BIT_FAILED  = 1,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_ALREADY_RUNNING = 2,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_DEVICE_IN_ERROR_STATE = 3,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_TEMPERATURE_TOO_LOW = 4,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_TEMPERATURE_TOO_HIGH = 5,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_REAL_TIME_DATA_DISABLED = 6,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_ECG_MEASUREMENT_DISABLED = 7,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_GENERAL_ERROR = 1000,
}PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_ENUM;

typedef enum {
	PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS = 0,
	PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_BASE_TIMESTAMP = 1,
}PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_ENUM;

typedef enum {
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE = 0,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TIME_PERIOD = 1,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_EVENTS_BITMAP = 10,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_RUNS = 11,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_PVC = 12,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_BIGEMENY = 13,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_COUPLET_OF_PVCS = 14,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TRIPLET_OF_PVCS = 15,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TRIGEMENY = 16,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_R_ON_T = 17,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_PREMATURE_ATRIAL_CONTRACTION = 18,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_MULTIFOCAL_PVCS = 19,
	PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_INTERPOLATED_PVCS = 20,

	PBS_ECG_ALGORITHM_EXTENDED_RESULTS_RESPIRATION_APNEA_DURATION = 30
}PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_ENUM;

typedef enum {
	PBS_ECG_IMPEDANCE_RESULTS_SUMMARY = 0,	//bitfield results
	PBS_ECG_IMPEDANCE_RESULTS_RA_KOHM = 1,
	PBS_ECG_IMPEDANCE_RESULTS_LA_KOHM = 2,
	PBS_ECG_IMPEDANCE_RESULTS_LL_KOHM = 3
}PBS_ECG_IMPEDANCE_RESULTS_ENUM;


typedef enum {
   PBS_ECG_MEASUREMENT_EVENT_IND_SW_FC_INDEX = -1,
   PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX = 0,
   PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_PACKET_TYPE = 1,
   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_STATUS = 2,
   PBS_ECG_MEASUREMENT_EVENT_IND_BASE_TIMESTAMP = 3,
   PBS_ECG_MEASUREMENT_EVENT_IND_PACE_MAKER_START_INDEX = 4,
   PBS_ECG_MEASUREMENT_EVENT_IND_PACE_MAKER_STOP_INDEX = 5,
   PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_TYPE = 6,
   PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_UUID = 7,
   PBS_ECG_MEASUREMENT_EVENT_IND_ONSET_BASE_TIMESTAMP = 8,
   PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_DURATION = 9,
   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_1_COMPRESSED_RAW_ECG_DATA = 10,
   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_1_COMPRESSED_RAW_ECG_DATA_MAP = 11,
   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_2_COMPRESSED_RAW_ECG_DATA = 12,
   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_2_COMPRESSED_RAW_ECG_DATA_MAP = 13,

//   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_1_COMPRESSED_FILTERED_ECG_DATA = 14,
//   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_1_COMPRESSED_FILTERED_ECG_DATA_MAP = 15,
//   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_2_COMPRESSED_FILTERED_ECG_DATA = 16,
//   PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_2_COMPRESSED_FILTERED_ECG_DATA_MAP = 17,
   PBS_ECG_MEASUREMENT_EVENT_IND_RESPIRATION_COMPRESSED_FILTERED_DATA = 42,
   PBS_ECG_MEASUREMENT_EVENT_IND_RESPIRATION_COMPRESSED_FILTERED_DATA_MAP = 43,
   PBS_ECG_MEASUREMENT_EVENT_IND_HEART_RATE = 60,
   PBS_ECG_MEASUREMENT_EVENT_IND_ECG_PEAKS = 61,
   PBS_ECG_MEASUREMENT_EVENT_IND_RESPIRATION_RATE = 70,
   PBS_ECG_MEASUREMENT_EVENT_IND_ALGORITHM_EXTENDED_RESULT    = 80,
   PBS_ECG_MEASUREMENT_EVENT_IND_IMPEDANCE					 = 90
}PBS_ECG_MEASUREMENT_EVENT_IND_ENUM;

typedef enum {
	PBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_RESP_SW_FC_INDEX = 0,
	PBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_BASE_TIMESTAMP = 3
}PBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_RESP_ENUM;

typedef enum {
   PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS = 0
}PBS_ECG_COMMAND_CONFIG_SET_RESP_ENUM;

typedef enum{
	PBS_ECG_GET_INFO_IMPEDANCE,
	PBS_ECG_GET_INFO_LEAD_CONTACT_STATUS,
	PBS_ECG_GET_INFO_STATISTICS,
	PBS_ECG_GET_INFO_MAX
}PBS_ECG_GET_INFO_E;

typedef enum{
	PBS_ECG_GET_INFO_STATUS_OK,
	PBS_ECG_GET_INFO_STATUS_IMPEDANCE_FAILED,
	PBS_ECG_GET_INFO_STATUS_CONTACT_DETECTION_FAILED,
	PBS_ECG_GET_INFO_STATUS_NO_STATISTICS_FILE,
	PBS_ECG_GET_INFO_STATUS_STATISTICS_FILE_ERROR
}PBS_ECG_GET_INFO_STATUS_E;

typedef enum{
	PBS_ECG_CONTACT_INFO_RESP_STATUS = 0
}PBS_ECG_CONTACT_INFO_RESP_E;

typedef enum{
	PBS_ECG_CONTACT_INFO_CMD_REQUEST = 0
}PBS_ECG_CONTACT_INFO_CMD_E;

typedef enum {
   PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_OK = 0,
   PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_CANNOT_SET_WHILE_RECORDING = 1,
   PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_GENERAL_ERROR = 1000,
}PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_ENUM;

typedef enum {
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_MEASUREMENT_ENABLE = 1,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_COMPRESSED_SIGNAL = 11,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_RECORD_TO_MEMORY = 12,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_SEND_EVENTS = 13,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ON_LEAD_OFF_EVENT = 14,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_PROGRESS_INDICATIONS_FLOW_CONTROL = 15,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_PACE_MAKER_ENABLE = 16,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_IMPEDANCE_MEASUREMENT_SETTINGS = 17,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_IMPEDANCE_MEASUREMENT_INTERVAL = 18,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_OUTPUT_FREQUENCY = 30,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_BANDPASS_FILTER = 31,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_50_60HZ_FILTER = 32,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_PEAKS_OUTPUT = 33,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_LSB_WEIGHT = 34,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ADS_GAIN_ECG = 35,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_OUTPUT_FILTER = 36,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_OUTPUT = 50,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_BANDPASS_FILTER = 51,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_50_60HZ_FILTER = 52,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_LSB_WEIGHT = 53,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_ADS_GAIN_RESPIRATION = 54,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_ALGORITHM_GAIN = 55,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_PHASE_SHIFT = 56,
   PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_CLOCK = 57,
}PBS_ECG_RESPIRATION_CONFIG_OBJECT_ENUM;

typedef enum {
   PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_STATUS = 0,
}PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_ENUM ;

typedef enum {
   PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_STATUS_OK = 0,
   PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_STATUS_GENERAL_ERROR = 1000,
}PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_STATUS_ENUM;

typedef enum
{
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_RESULTS_TIME_PERIOD = 0,

	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_RUNS_LIMIT = 20,
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_BIGEMINY_LIMIT = 21,
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_PAUSE_LIMIT = 22,
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_VTACH_LIMIT_1 = 23,
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_EBRAD_LIMIT_1 = 24,
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_ETACH_LIMIT_1 = 25,
//   PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_VTACH_LIMIT_2 = 26,
//   PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_EBRAD_LIMIT_2 = 27,
//   PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_ETACH_LIMIT_2 = 28,
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_APNEA_DETECTION_LIMIT = 40,
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_MINIMUM_RESPIRATION_RATE = 41,
	PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_MAXIMUM_RESPIRATION_RATE = 42,
}PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_ENUM;

typedef enum {
   PBS_ACCELEROMETER_CONFIG_SET_RESP_STATUS = 0,
}PBS_ACCELEROMETER_CONFIG_SET_RESP_ENUM;

typedef enum {
   PBS_ACCELEROMETER_CONFIG_SET_RESP_STATUS_OK = 0,
   PBS_ACCELEROMETER_CONFIG_SET_RESP_STATUS_GENERAL_ERROR = 1000,
}PBS_ACCELEROMETER_CONFIG_SET_RESP_STATUS_ENUM;

typedef enum {
   PBS_ACCELEROMETER_CONFIG_OBJECT_ACCELEROMETER_ENABLE = 0,
   PBS_ACCELEROMETER_CONFIG_OBJECT_ACCELEROMETER_PERIOD_TIME = 1,
   PBS_ACCELEROMETER_CONFIG_OBJECT_AXIS_DATA_IN_EVENT_ = 2,
   PBS_ACCELEROMETER_CONFIG_OBJECT_ACCELEROMETER_EVENTS = 3,
}PBS_ACCELEROMETER_CONFIG_OBJECT_ENUM;
typedef enum {
   PBS_ACCELEROMETER_EVENT_IND_SW_FC_INDEX = -1,
   PBS_ACCELEROMETER_EVENT_IND_EVENT_TIMESTAMP = 0,
   PBS_ACCELEROMETER_EVENT_IND_EVENT_TYPE = 1,
   PBS_ACCELEROMETER_EVENT_IND_BODY_POSITION = 2,
   PBS_ACCELEROMETER_EVENT_IND_X_AXIS = 10,
   PBS_ACCELEROMETER_EVENT_IND_Y_AXIS = 11,
   PBS_ACCELEROMETER_EVENT_IND_Z_AXIS = 12,
}PBS_ACCELEROMETER_EVENT_IND_ENUM;
typedef enum {
   GMP_ACCELEROMETER_EVENT_IND_RESP_SW_FC_INDEX = 0,
   GMP_ACCELEROMETER_EVENT_IND_RESP_BASE_TIMESTAMP = 3
}GMP_ACCELEROMETER_EVENT_IND_RESP_ENUM;

typedef enum {
   PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER_STATUS = 0,
}PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER_RESP_ENUM;

typedef enum {
   PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER_STATUS_OK = 0,
   PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER_STATUS_NO_BOOTLOADER = 2,
   PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER_STATUS_GENERAL_ERROR = 1000,
}PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER_STATUS_ENUM;

typedef enum {
   PBS_GENERAL_ERRORS_EVENTS_OK 							= 0,
   PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER 				= 1000,
   PBS_GENERAL_ERRORS_EVENTS_DEVICE_IN_MEASUREMENT 			= 1001,
   PBS_GENERAL_ERRORS_EVENTS_LOW_BATTERY 					= 1002,
   PBS_GENERAL_ERRORS_EVENTS_DEVICE_TEMPERATURE_OK 			= 1003,
   PBS_GENERAL_ERRORS_EVENTS_BUTTON_PRESSED 				= 1004,
   PBS_GENERAL_ERRORS_EVENTS_BUTTON_RELEASED 				= 1005,
   PBS_GENERAL_ERRORS_EVENTS_UNKNOWN_ERROR 					= 1999,
   PBS_GENERAL_ERRORS_EVENTS_DEVICE_TEMPERATURE_TOO_LOW 	= 2000,
   PBS_GENERAL_ERRORS_EVENTS_DEVICE_TEMPERATURE_TOO_HIGH 	= 2001,
   PBS_GENERAL_ERRORS_EVENTS_CRITICALLY_LOW_BATTERY 		= 2002,
   PBS_GENERAL_ERRORS_EVENTS_ADS_FAILURE 					= 2003,
   PBS_GENERAL_ERRORS_EVENTS_ACCELEROMETER_FAILURE 			= 2004,
   PBS_GENERAL_ERRORS_EVENTS_SD_CARD_FULL		 			= 2005,
   PBS_GENERAL_ERRORS_EVENTS_IMPEDANCE_TOO_HIGH	 			= 2006
}PBS_GENERAL_ERRORS_EVENTS_ENUM;

typedef enum {
   PBS_COMMAND_GENERAL_GET_INFO   							= 0x0001,   
   PBS_COMMAND_GENERAL_GET_STATUS 							= 0x0002,   
   PBS_COMMAND_GENERAL_CONTROL    							= 0x0003,   
   
   PBS_COMMAND_DEVICE_CONFIGURATION_SET   					= 0x0004,   
   PBS_COMMAND_DEVICE_CONFIGURATION_GET   					= 0x0014,
   
   // ECG and Respiration Commands
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE  			=0x0211,
   PBS_COMMAND_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESP  =0x0213,
   PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT   				= 0x0214,
   PBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_RESP   			= 0x0215,
   PBS_COMMAND_ECG_MEASUREMENT_INFO_GET						= 0x0216,
   
   PBS_COMMAND_ECG_STATISTICS_IND_RESP						= 0x0217,

   PBS_COMMAND_ECG_MEASUREMENT_CONFIG_SET   				= 0x0310,
   PBS_COMMAND_ECG_MEASUREMENT_CONFIG_GET   				= 0x0311,
   
   PBS_COMMAND_ECG_ALGORITHM_CONFIG_SET   					= 0x0410,
   PBS_COMMAND_ECG_ALGORITHM_CONFIG_GET   					= 0x0411,
					
   PBS_COMMAND_ACCELEROMETER_CONFIG_SET   					= 0x0360,
   PBS_COMMAND_ACCELEROMETER_CONFIG_GET   					= 0x0361,
   
   PBS_COMMAND_ACCELEROMETER_REMOTE_EVENT					= 0x0611,
   PBS_ACCELEROMETER_EVENT_IND_RESP							= 0x0613,

   PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER 				= 0x0800
}PBS_COMMAND_ENUM;

typedef enum {
   PBS_COMMAND_RESP_GENERAL_GET_INFO   						= 0x1001,
   PBS_COMMAND_RESP_GENERAL_GET_STATUS 						= 0x1002,
   PBS_COMMAND_RESP_GENERAL_CONTROL    						= 0x1003,

   PBS_COMMAND_RESP_DEVICE_CONFIGURATION_SET   				= 0x1004,
   PBS_COMMAND_RESP_DEVICE_CONFIGURATION_GET   				= 0x1014,

   // ECG and Respiration Commands
   PBS_COMMAND_RESP_ECG_MEASUREMENT_REALTIME_ENABLE  		= 0x1211,

   PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_IND  				= 0x1213,
   PBS_COMMAND_RESP_ECG_MEASUREMENT_REMOTE_EVENT			= 0x1214,
   PBS_ECG_MEASUREMENT_EVENT_IND   							= 0x1215,
   PBS_ECG_MEASUREMENT_CONTACT_IND							= 0x1216,

   PBS_COMMAND_ECG_STATISTICS_IND							= 0x1217,

   PBS_COMMAND_RESP_ECG_MEASUREMENT_CONFIG_SET   			= 0x1310,
   PBS_COMMAND_RESP_ECG_MEASUREMENT_CONFIG_GET   			= 0x1311,

   PBS_COMMAND_RESP_ECG_ALGORITHM_CONFIG_SET   				= 0x1410,
   PBS_COMMAND_RESP_ECG_ALGORITHM_CONFIG_GET   				= 0x1411,
				
   PBS_COMMAND_RESP_ACCELEROMETER_CONFIG_SET   				= 0x1360,
   PBS_COMMAND_RESP_ACCELEROMETER_CONFIG_GET   				= 0x1361,

   PBS_ACCELEROMETER_EVENT_IND								= 0x1613,

   PBS_FW_UPGRADE_RESP_COMMAND_JUMP_TO_BOOTLOADER 			= 0x1800,

   PBS_UNKNOWN_COMMAND										= 0x10FF,
			
   PBS_ERROR_EVENT											= 0x1F00
}PBS_COMMAND_RESP_ENUM;

typedef enum{
	PBS_TX_TASK_MODE_ON,
	PBS_TX_TASK_MODE_OFF	
}PBS_TX_TASK_MODE_E;

typedef struct PBS_TLV_LIST_STRUCT{
  uint16_t      subtype;
  uint16_t      length;
  uint8_t       *value;
  struct PBS_TLV_LIST_STRUCT * next;
}PBS_TLV_LIST_STRUCT;

typedef union {
	data_u 			* data;
	PBS_TLV_LIST_STRUCT	* tlv_list;
}PBS_RX_PAYLOAD_STRUCT;

typedef struct {
	uint8_t			data[PBS_TIMESTAMP_LENGTH];
}PBS_TIMESTAMP_STRUCT;

typedef struct {
	uint8_t			mcu_sn[PBS_MCU_S_N_LEN];
	VERSION_STRUCT	fw;
	VERSION_STRUCT	hw;
	uint8_t			ble_mac[PBS_BLE_MAC_LEN];
	VERSION_STRUCT	bootloader;
	uint8_t			device_sn[PBS_DEVICE_S_N_LEN];
	uint8_t			device_type;	// gmp is always 1
}PBS_DEVICE_INFO_STRUCT;

typedef struct {
	PBS_DEVICE_STATUS_STATE_ENUM 			state;
	PBS_GENERAL_ERRORS_EVENTS_ENUM 			error;
	PBS_DEVICE_STATUS_BIT_ERROR_ENUM		bit_status;
	PBS_DEVICE_STATUS_BATTERY_LEVEL_ENUM	bat_level;
	float32_t								temperature;
	PBS_DEVICE_STATUS_OWNED_ENUM			owned;
	int16_t									voltage;
	PBS_DEVICE_STATUS_BUTTON_ENUM			button;
}PBS_DEVICE_STATUS_STRUCT;

typedef struct {
	uint8_t			leds_state;	//LEDS_BITMAP_E
	uint8_t			device_name_length;
	uint8_t			device_name[PBS_DEVICE_NAME_MAX_LEN];
	uint8_t			timestamp[PBS_TIMESTAMP_LENGTH];
}PBS_DEVICE_CONFIG_OBJECT;


typedef union
{
	uint8_t txBuffer[PBS_TX_MAX_PACKET_SIZE];
	struct
	{
		uint16_t header;
		uint16_t type;
		uint16_t length;
		uint8_t* value;
	};
}PBS_BUFFER_U;


typedef enum
{
	PBS_MSG_UNINITIALIZED						= 0xFFFF
} PBS_RX_COMMAND_TYPE;

typedef enum
{
	PBS_ECG_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE_CURRENT_STATE,
	PBS_ECG_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE_SUMMARY
}PBS_ECG_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE_ENUM;

typedef enum {
	PBS_PAYLOAD_INTEGER,
	PBS_PAYLOAD_FLOAT,
	PBS_PAYLOAD_CHAR,
	PBS_PAYLOAD_SHORT,
	PBS_PAYLOAD_BYTE_ARRAY,
	PBS_PAYLOAD_TLV_ARRAY,
	PBS_PAYLOAD_NONE,
}PBS_PAYLOAD_TYPE;

typedef union {
	int32_t				i;
	float32_t			f;
	uint8_t 			ch;
	uint16_t 			sh;
	uint8_t	* 			byte_array;
	struct TLV_STRUCT		*tlv_list;
}PBS_DATA_U;

typedef struct {
	PBS_DATA_U 			data;
	PBS_PAYLOAD_TYPE 	payload_type;
}PBS_PAYLOAD_STRUCT;

typedef struct TLV_STRUCT{
	 uint16_t 				type;
	 uint16_t 				length;
	 PBS_PAYLOAD_STRUCT		data;
	 struct TLV_STRUCT * 	next;
}TLV_STRUCT;

typedef struct {
	uint8_t					counter	:7;
	SW_FLOW_CONTROL_ENUM 	enable	:1;
}SW_FLOW_CONTROL;



PBS_RESULT_ENUM pbsInit(PBS_TX_TASK_MODE_E mode);

PBS_RESULT_ENUM pbsFlowControlEnable(SW_FLOW_CONTROL_ENUM en);
PBS_RESULT_ENUM prvPbsFlowControlReset();

PBS_RESULT_ENUM pbsSendGetDeviceInfoResp(PBS_DEVICE_INFO_STRUCT * info);
PBS_RESULT_ENUM pbsSendGetStatusResp(PBS_DEVICE_STATUS_STRUCT * status);
PBS_RESULT_ENUM pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_ENUM status);
PBS_RESULT_ENUM pbsSendDeviceConfigSetResp(PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_STATUS_ENUM status);
PBS_RESULT_ENUM pbsSendDeviceConfigGetResp(PBS_DEVICE_CONFIG_OBJECT * conf);

PBS_RESULT_ENUM pbsSendECGMeasurementRealtimeEnableResp(PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_ENUM status, PBS_TIMESTAMP_STRUCT * time);
PBS_RESULT_ENUM pbsSendECGMeasurementRemoteEventResp(PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_ENUM status, PBS_TIMESTAMP_STRUCT * time);


PBS_RESULT_ENUM pbsSendECGMeasurementRealtimeProgressInd(ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT * msg, uint8_t is_send_arrythmias);
PBS_RESULT_ENUM pbsSendECGMeasurementEventInd(ECG_MEAS_EVENT_INDICATION_STRUCT * msg, IMPEDANCE_EVENT_STRUCT * impedance_results);

PBS_RESULT_ENUM pbsEcgHolterMeasurementRealtimeProgressInd(ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT * msg);
PBS_RESULT_ENUM pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_E status, ECG_LEAD_STATUS_ENUM lead_status, IMPEDANCE_EVENT_STRUCT * impedance_results);

PBS_RESULT_ENUM pbsSendECGConfigSetResp(PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_ENUM status);
PBS_RESULT_ENUM pbsSendECGConfigGetResp(ECG_RESP_CONFIG_OBJECT_STRUCT * ecg_params);

PBS_RESULT_ENUM pbsSendECGAlgoConfigSetResp(PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_STATUS_ENUM status);
PBS_RESULT_ENUM pbsSendECGAlgoConfigGetResp(ECG_RESP_CONFIG_OBJECT_STRUCT * ecg_params);

PBS_RESULT_ENUM pbsSendAccelerometerConfigSetResp();
PBS_RESULT_ENUM pbsSendAccelerometerConfigGetResp();
PBS_RESULT_ENUM pbsSendAccelerometerEventInd ();

PBS_RESULT_ENUM pbsSendJumpToBootloaderResp();
RESULT_E pbsSendErrorEvent(PBS_GENERAL_ERRORS_EVENTS_ENUM error_event);
//PBS_RESULT_ENUM pbsSend();


PBS_RESULT_ENUM pbsTxTaskTest(int8_t cmd, int32_t param);
PBS_RESULT_ENUM pbsMessageAckRecieved(uint8_t flow_control_index);
PBS_RESULT_ENUM pbsSetConnectionState(PBS_CONNECTION_STATE_ENUM new_state);
PBS_RESULT_ENUM pbsTxEmptyQueue();


PBS_RESULT_ENUM pbsSetOutputMode(PBS_OUTPUT_TYPE_E mode);
PBS_RESULT_ENUM pbsFlushSdEvent(ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT * arr_change, uint16_t duration, PBS_TIMESTAMP_STRUCT * timestamp);
PBS_RESULT_ENUM pbsGetHighWaterMark(UBaseType_t * ret_val);
PBS_RESULT_ENUM pbsTxGetHighWaterMark(UBaseType_t * ret_val);

PBS_RESULT_ENUM pbsSaveEventEntryToSdCard(ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT * arr_change, uint16_t duration, PBS_TIMESTAMP_STRUCT * timestamp);

PBS_RESULT_ENUM pbsFlushStatisticsStart();

PBS_RESULT_ENUM pbsCheckArrhythmia(ECG_ALGO_ARRYTHMIA_EVENT_HEADER_STRUCT current_arr);
PBS_RESULT_ENUM pbsHolterRemoteEvent(PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENUM command, uint16_t duration);
PBS_RESULT_ENUM pbsSetFileDurationLen(uint8_t duration);
//read from sdcard and send to ble last minutes:
PBS_RESULT_ENUM prvPbsReadLastMinutes(uint8_t minutes, bool panicButton);
PBS_CONNECTION_STATE_ENUM pbsGetBleConnectionState(void); // get connection state : connect to ble or not
void pbsSetHourlyMode(PBS_HOURLY_TYPE_E command); //set if to enable transmiting to ble in function: prvPbsFlushTxBuffer(...)

#endif //__PBS_H__
