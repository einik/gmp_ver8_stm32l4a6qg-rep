#ifndef __FLASH_IF_H__
#define __FLASH_IF_H__

#include "cmsis_os.h"
#include "common.h"

#define FILE_SYSTEM_VERSION 					1

#define FLASH_FILE_TABLE_DESC_NAME_LEN 			7
#define VERSION_LENGTH							3
#define FLASH_BOOTLOADER_SECTOR_1				0
#define FLASH_BOOTLOADER_SECTOR_2				1

//STM32L411
#define FLASH_IF_PAGE_SIZE							0x0800 //2048
#define FLASH_NUM_OF_PAGES						0xFF   //255 //There are 512 but we use only 512K

#define FLASH_APPLICATION_ADDRESS     			(uint32_t)(DEVICE_BASE_ADDRESS +  DEVICE_APPLICATION_OFFSET)     /* Start user code address */
#define FLASH_BASE_ADDRESS     					(uint32_t)0x08000000
#define FLASH_APPLICATION_OFFSET     			(uint32_t)0x10000
#define FLASH_PAGE_SIZE							0x800//0x7FF
#define FLASH_APP_MAX_SIZE						FLASH_BANK_SIZE - FLASH_BOOTLOADER_SIZE - FLASH_CONFIG_PAGES_SIZE
#define FLASH_BANK_SIZE							0x80000 //524288
#define FLASH_BOOTLOADER_SIZE					0x10000 //65536
#define FLASH_CONFIG_PAGES_SIZE					FLASH_NUM_OF_CONFIG_PAGES * FLASH_PAGE_SIZE //0x2000, 8192
#define FLASH_NUM_OF_CONFIG_PAGES				4
#define FLASH_EMPTY_FLASH						(uint64_t)0xFFFFFFFFFFFFFFFF
#define FLASH_SIZE     							((uint16_t)1 * DEVICE_PAGE_SIZE)
#define FLASH_CHECKSUM_ADDRESS     				(FLASH_BASE_ADDRESS + FLASH_BANK_SIZE - (FLASH_PAGE_SIZE * 2)) //0x807F000 //TODO: set constant
#define FLASH_VERSION_OFFSET					0x400
#define FLASH_VERSION_ADDRESS					(FLASH_BASE_ADDRESS + FLASH_APPLICATION_OFFSET + FLASH_VERSION_OFFSET)


typedef enum FLASH_RES{
  FLASH_OK,
  FLASH_ERROR_FILE_NAME_TOO_BIG,
  FLASH_ERROR_WRITE_TO_FLASH,
  FLASH_ERROR_ADDRESS_OUT_OF_BOUNDS,
  FLASH_ERROR_NOT_INITIALIZED,
  FLASH_ERROR_NOT_ENOUGH_SPACE,
  FLASH_ERROR_FILE_NOT_FOUND,
  FLASH_ERROR_FILE_SIZE_TOO_BIG,
  FLASH_ERROR_COULD_NOT_CREATE_MUTEX,
  FLASH_ERROR,
  FLASH_ERROR_COULD_NOT_LOCK_FLASH,
  FLASH_ERROR_COULD_NOT_UNLOCK_FLASH,
  FLASH_ERROR_LENGTH_TOO_LONG,
  FLASH_ERROR_ADDRESS_NOT_VALID
}FLASH_RES;

typedef enum {
	FW_STATE_ERASED				= 0,
	FW_STATE_FW_NEW				= 1,
	FW_STATE_FW_RUN				= 2,
	FW_STATE_FW_OK				= 3,
	FW_STATE_FW_UPDATE			= 4,
	FW_STATE_RESET_BL_GO_APP	= 5,
	FW_STATE_COPY_NEW_FW		= 6
}FW_STATE_TYPE;

typedef enum FILE_STATE_E{
	FILE_STATE_FREE 		= 0xFFFF,
	FILE_STATE_IN_WRITING	= 0x0FFF,
	FILE_STATE_OK			= 0x00FF,
	FILE_STATE_INVALID	= 0x000F
}FILE_STATE_E;

typedef enum{
	FLASH_OTP_BLOCK_0,
	FLASH_OTP_BLOCK_1,
	FLASH_OTP_BLOCK_2,
	FLASH_OTP_BLOCK_3,
	FLASH_OTP_BLOCK_4,
	FLASH_OTP_BLOCK_5,
	FLASH_OTP_BLOCK_6,
	FLASH_OTP_BLOCK_7,
	FLASH_OTP_BLOCK_8,
	FLASH_OTP_BLOCK_9,
	FLASH_OTP_BLOCK_10,
	FLASH_OTP_BLOCK_11,
	FLASH_OTP_BLOCK_12,
	FLASH_OTP_BLOCK_13,
	FLASH_OTP_BLOCK_14,
	FLASH_OTP_BLOCK_15,
	FLASH_OTP_BLOCK_MAX
}FLASH_OTP_BLOCK_E;

//typedef struct VERSION_STRUCT{
//	uint8_t major;
//	uint8_t minor;
//	uint8_t patch;
//}VERSION_STRUCT;

typedef struct SYS_INFO_STRUCT{
	uint32_t			sw_checksum;
	uint8_t				sw_state;
	VERSION_STRUCT 		bl_ver;
	VERSION_STRUCT 		sw_ver;
}SYS_INFO_STRUCT;


FLASH_RES flashInit(void);
FLASH_RES flashWriteData(uint32_t page_num, uint32_t len,  uint8_t* data);
FLASH_RES flashEraseData(uint32_t page_num, uint8_t num_of_pages);
FLASH_RES flashReadData(uint32_t page_num, uint32_t len,  uint8_t* data);
/***********************************************************************
 * Function name: flashReadOtp
 * Description: write data to OTP memory
 * Parameters : offset - in OTP memory section
 *				data - pointer to data
 *				length - data length
 *
 * Returns :
 *		FLASH_OK - on success
 *		FLASH_ERROR_ADDRESS_NOT_VALID
 *		FLASH_ERROR_LENGTH_TOO_LONG
 *		FLASH_ERROR_COULD_NOT_UNLOCK_FLASH
 *		FLASH_ERROR_WRITE_TO_FLASH
 *		FLASH_ERROR_COULD_NOT_LOCK_FLASH
 ***********************************************************************/
FLASH_RES flashReadOtp(uint32_t offset, uint8_t* data, uint8_t length);
/***********************************************************************
 * Function name: flashWriteOtp
 * Description: write data to OTP memory
 * Parameters : offset - in OTP memory section
 *				data - pointer to data
 *				len - data length
 *
 * Returns :
 *		FLASH_OK - on success
 *		FLASH_ERROR_ADDRESS_NOT_VALID
 *		FLASH_ERROR_LENGTH_TOO_LONG
 *		FLASH_ERROR_COULD_NOT_UNLOCK_FLASH
 *		FLASH_ERROR_WRITE_TO_FLASH
 *		FLASH_ERROR_COULD_NOT_LOCK_FLASH
 ***********************************************************************/
FLASH_RES flashWriteOtp(uint32_t offset, uint8_t* data, uint16_t len);
FLASH_RES flashCalcFwChecksum(uint32_t size, uint32_t* checksum);
void print_fs(BOOL verbose); //debug
#endif  //__FLASH_IF_H__
