#ifndef __IMP_SIN_TABLE_H__
#define __IMP_SIN_TABLE_H__

//#include <stdlib.h>
#include "common.h"
/*
	LoockUp table for sine values 2P(360 degrees) range divided by 256 steps.
	For impedance calculation Pi/2, Pi and (3/2)P are used (table indexes 64,128 and 192)
*/

#define SINE_0_DEGREE_IDX		0
#define SINE_90_DEGREE_IDX		64
#define PWM_MAX					imp_sin_table[SINE_90_DEGREE_IDX]
#define PWM_MIDDLE				imp_sin_table[SINE_0_DEGREE_IDX]
#define CHECKPOINT1_IDX			64		//90 degrees
#define CHECKPOINT2_IDX			128		//180 degree
#define CHECKPOINT3_IDX			192		//270 degree

#define MEASURING_WAVES_NUMBER	4

extern const uint16_t imp_sin_table[256];


#endif //__IMP_SIN_TABLE_H__
