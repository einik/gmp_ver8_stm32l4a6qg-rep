

#ifndef _ECG_MMI_API_
#define _ECG_MMI_API_


#define NUM_SAMP_320  80
#define NUM_HRATE_PRM  1
#define NUM_ARRH_VAL  19
#define NUM_BEAT_PRM  (4*4)
#define NUM_STAT_PRM   6 
#define NUM_CONF_PRM  10
#define NUM_VER_BYTES 5
#define NUM_PACE_LOC  2

//typedef struct ECG_WFRM {
//   short samples[NUM_SAMPLES];
//}ECG_WFRM;

typedef struct ECG_PACE_LOC {
   short val[NUM_PACE_LOC];
}ECG_PACE_LOC;

typedef struct ECG_ARRH {
//   short value[NUM_ARRH_VAL];
   //basic arrhythmias:
   short asyst;   // boolean flag
   short vfib;    // boolean flag
   short vtach;   // boolean flag
   short runs;    // runs per minute
   short pvc;     // beats per minute
   short pause;   // boolean flag
   //extended arrhythmias:
   short bigem;   // bigemeny complexes per minute
   short ebrad;   // bradicardia
   short etach;   // tachicardia
   short irrythm; // irregular rythm
   short idrythm; // ideovent rythm
   short couplet; // couplet of pvc's
   short triplet; // triplet of pvc's
   short trigem;  // trigemeny
   short ront;    // R on T
   short pac;     // premature atrial contraction
   short svtach;  // supravent. tachicardia
   short mpvc;    // multifocal pvc
   short ipvc;    // interpolated pvc's
}ECG_ARRH;

typedef struct ECG_BEAT {
//   short param[NUM_BEAT_PRM];
   short beatLocation;
   short beatAnnotation;
   short qrsDuration;
   short rrInterval;
}ECG_BEAT;

typedef struct ECG_STAT {
//   unsigned short param[NUM_STAT_PRM];
   short learn_on; 
   short artifact; 
   short low_ampl; 
   short unstable; 
   short hr_valid;
   short lead_off;
}ECG_STAT;

typedef struct ECG_CONF {
//   short param[NUM_CONF_PRM];
   //processing
   short notch_mode;
   short filter_mode;
   short gain_mul;
   short gain_div;
   //arrhythmias
   short RUN_limit;
   short BGM_limit;
   short VTACH_limit;
   short PAUSE_limit;
   short EBRAD_limit;
   short ETACH_limit;
}ECG_CONF;

typedef struct ECG_VERS {
   unsigned char ver[NUM_VER_BYTES];
}ECG_VERS;

typedef enum {
    ECG_NOTCH_NONE = 0,
    ECG_NOTCH_50Hz,
    ECG_NOTCH_60Hz
}ECG_NOTCH_OPT;

typedef enum {
    ECG_FILT_NONE = 0,
    ECG_FILT_MONITOR,      /* 0.5Hz  -  40Hz */
    ECG_FILT_DIAGNOSTIC,   /* 0.05Hz - 150Hz     */
}ECG_FILT_OPT;

#define LOFF_BIT0 0x01
#define LOFF_BIT1 0x02

void  ECG_initProc (void);
void  ECG_resetProc(void);
void  ECG_procSample(long *pSample, unsigned char lead_off, int pace_flg);

//void  ECG_bindWformBuf(void *buf, int numSamp, int numChan);
//void  ECG_add2Wform(short sample);
//void  ECG_cpyWform (void *out_buf); //(ECG_WFRM *pWfrm);
//void* ECG_getWform (void);

int  ECG_downSample_500_250(short Samples[], int num_samp, short outSamples[]);
int  ECG_downSample_500_320(short Samples[], int num_samp, short outSamples[]);

//void ECG_latchLeadFault(unsigned char lead_fault, int ind);
void ECG_latchPaceLocat(int paceFlg, int loc);
void ECG_resetPaceLocat(void);
void ECG_getPaceLocat(ECG_PACE_LOC *pPaceLoc);
void ECG_getPaceLocat_250(ECG_PACE_LOC *pPaceLoc);

void ECG_initArrh (void); 
void ECG_resetArrh(void);
void ECG_execArrh (short sbuf1[], short sbuf2[], int loff1, int loff2);  //, short pace_loc);
void ECG_getArrh  (ECG_ARRH *pArrh); 
int  ECG_getHRate (short *pHrate);  
int  ECG_getBeat  (ECG_BEAT *pBeat); 
int  ECG_isBeat   (void);

//void ECG_execArrh_320sps (short sbuf1[], short sbuf2[]); //, short pace_loc);
//int  ECG_getBeat_320sps (ECG_BEAT *out_buf);

void ECG_getStat (ECG_STAT *pStat); 
void ECG_getConf (ECG_CONF *pConf); 
int  ECG_getVers (char *out_buf);

void ECG_setFilter(int filter); //ECG_FILT_OPT
void ECG_setNotch (int notch);  //ECG_NOTCH_OPT
void ECG_setGain  (int gain_mul, int gain_div);

void ARRH_setRunLimit   (short limit);
void ARRH_setBgmLimit   (short limit);
void ARRH_setPauseLimit (short limit);
void ARRH_setVtachLimit (short limit);
void ARRH_setEbradLimit (short limit);
void ARRH_setEtachLimit (short limit);
void ARRH_setDefaults   (void);

int  Arrh_getVers(char *out_buf);


#endif //_ECG_MMI_API_