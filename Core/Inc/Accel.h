#ifndef __ACCEL_H__
#define __ACCEL_H__

typedef enum ACCEL_RESULT_E{
	ACCEL_RESULT_OK,
	ACCEL_RESULT_ERROR_ALREADY_INITIALIZED,
	ACCEL_RESULT_ERROR_INIT_FAILED,
	ACCEL_RESULT_ERROR,
	ACCEL_RESULT_ERROR_AXIS_READ_FAILED,
	ACCEL_RESULT_ERROR_WRONG_PARAM,
	ACCEL_RESULT_ERROR_SLEEP_FAILED
}ACCEL_RESULT_E;

typedef enum{
	ACCEL_CONFIG_MODE_ENABLE,
	ACCEL_CONFIG_MODE_DISABLE
}ACCEL_CONFIG_MODE_E;

typedef enum{
	ACCEL_CONFIG_AXIS_DATA_IN_EVENT_ENABLE,
	ACCEL_CONFIG_AXIS_DATA_IN_EVENT_DISABLE
}ACCEL_CONFIG_AXIS_DATA_IN_EVENT_E;

typedef enum{
	ACCEL_CONFIG_SEND_EVENTS_DISABLE,
	ACCEL_CONFIG_SEND_EVENTS_ENABLE
}ACCEL_CONFIG_SEND_EVENTS_E;

typedef struct{
	ACCEL_CONFIG_MODE_E 				accel_config_mode;
	uint16_t							accel_config_period_time;
	ACCEL_CONFIG_AXIS_DATA_IN_EVENT_E	accel_config_axis_data_in_event;
	ACCEL_CONFIG_SEND_EVENTS_E			accel_config_send_events;
}ACCEL_CONFIG_STRUCT;

/***********************************************************************
 * Function name: accelInit
 * Description: Init Accelerometer
 * Parameters : 
 *
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
ACCEL_RESULT_E accelInit(void);
/***********************************************************************
 * Function name: accelWriteLis2hReg
 * Description: Write LIS2H registers
 * Parameters : address - register address, data - data to write in register
 *
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
ACCEL_RESULT_E accelWriteLis2hReg(uint8_t address, uint8_t data);
/***********************************************************************
 * Function name: accelPrintRegisters
 * Description: Print accelerometer registers
 * Parameters :
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *
 ***********************************************************************/
ACCEL_RESULT_E accelPrintRegisters(void);
/***********************************************************************
 * Function name: accelGetHighWaterMark
 * Description: Get accelerometer task size usage
 * Parameters :
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_WRONG_PARAM - in case of 
 ***********************************************************************/
ACCEL_RESULT_E accelGetHighWaterMark(UBaseType_t * ret_val);
/***********************************************************************
 * Function name: accelSleep
 * Description: Shutdown Accelerometer sensor
 * Parameters : 
 *
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_SLEEP_FAILED - on failure
 *
 ***********************************************************************/
ACCEL_RESULT_E accelSleep(void);
#endif //__ACCEL_H__

