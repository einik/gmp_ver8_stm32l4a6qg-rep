#ifndef __ACTL_IDENTIFY_ACTIVITY_TYPE_H__
#define __ACTL_IDENTIFY_ACTIVITY_TYPE_H__
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "actl_identify_activity_type_types.h"

//version 6.0, 25.6.2018

typedef enum { none = 0, walk, run, lie_back, lie_right, lie_left, lie_stomach, sit_or_stand, fall } activity_type;

extern unsigned char actl_identify_activity_type(short acc1, short acc2, short
  acc3, boolean_T reset_flag);
extern void actl_identify_activity_type_initialize(void);
extern void actl_identify_activity_type_terminate(void);

#endif
