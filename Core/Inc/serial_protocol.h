#ifndef __SERIAL_PROTOCOL_H
#define __SERIAL_PROTOCOL_H


#include <stdint.h>
#include "common.h"
#include "cmsis_os.h"

typedef enum {
	SERIAL_PROTOCOL_RESULT_OK,
	SERIAL_PROTOCOL_RESULT_WRONG_PARAM,
	SERIAL_PROTOCOL_RESULT_ERROR,
}SERIAL_PROTOCOL_RESULT_E;


typedef enum COMMAND_PARAM_TYPE{
  COMMAND_PARAM_NONE,
  COMMAND_PARAM_INT,
  COMMAND_PARAM_STR,
  COMMAND_PARAM_STR_2,
  COMMAND_PARAM_CHAR,
}COMMAND_PARAM_TYPE;

typedef struct command_desc{
  int commandType;
  char * command_string;
  char * description;
  char * ack;
  COMMAND_PARAM_TYPE command_param;
}command_desc;

typedef struct{
  char * str;
  char * str2;
  char c;
  int i;
}command_param;


//debug
typedef enum{
	DEBUG_LEVEL_NONE = 0x0,
	DEBUG_LEVEL_1 	= 0x1,
	DEBUG_LEVEL_2	= 0x2,
	DEBUG_LEVEL_3	= 0x4,
	DEBUG_LEVEL_4	= 0x8,
	DEBUG_LEVEL_5	= 0x10,
	DEBUG_LEVEL_6	= 0x20,
	DEBUG_LEVEL_7	= 0x40,
	DEBUG_LEVEL_8	= 0x80,
	DEBUG_LEVEL_9	= 0x200,
	DEBUG_LEVEL_10	= 0x400,
	DEBUG_LEVEL_11  = 0x800,
	DEBUG_LEVEL_ALL	= 0xFF,
	DEBUG_LEVEL_HEX	= 0x100,
	DEBUG_LEVEL_RAW	= 0x20000000,
	DEBUG_LEVEL_ALWAYS	= 0x40000000,
	DEBUG_LEVEL_ERROR	= 0x80000000,
}DEBUG_LEVEL;


typedef enum {
	DEBUG_MODULE_DEVICE,
	DEBUG_MODULE_ECG,
	DEBUG_MODULE_SD,
	DEBUG_MODULE_ACCEL,
	DEBUG_MODULE_BATTERY,
	DEBUG_MODULE_BLE,
	DEBUG_MODULE_PBS,
	DEBUG_MODULE_MASTER,
	DEBUG_MODULE_MAX
}DEBUG_MODULE_E;

//callback function prototype for rx message handling
typedef RESULT_E (*command_handler)(const command_desc *commDesc, command_param * param);

RESULT_E serialProtocolSetCommands(command_handler rxCallbackFunc, const command_desc * cmd_list, int cmd_list_max);
RESULT_E serialProtocolInit();
SERIAL_PROTOCOL_RESULT_E serialProtocolGetHighWaterMark(UBaseType_t * ret_val);
//int print_sync(DEBUG_LEVEL dbgLvl, const char* format, ...);
void setDebugLevel(DEBUG_LEVEL dbgLvl);
void serialProtocolExternalProtocolInterfaceRx(uint8_t *buff, uint16_t length);
void print_buffer(char* string, uint8_t* ptr_data, uint16_t length);
int send_tx_data(uint8_t data[], int length); //vh

#define SERIAL_PROTOCOL_DEFAULT_DEBUG_LEVEL (DEBUG_LEVEL_NONE | DEBUG_LEVEL_HEX)	//(DEBUG_LEVEL_8 | )	//

#define DEBUG



SERIAL_PROTOCOL_RESULT_E serialProtocolSetModuleLogLevel(DEBUG_MODULE_E module, DEBUG_LEVEL level);
SERIAL_PROTOCOL_RESULT_E serialProtocolModuleLog(DEBUG_MODULE_E module, DEBUG_LEVEL dbg_level, const char* format, ...);
SERIAL_PROTOCOL_RESULT_E serialProtocolLogsEnable(uint8_t en);
SERIAL_PROTOCOL_RESULT_E serialProtocolEchoEnable(uint8_t en);

#define SERIAL_PROTOCOL_DEFAULT_LOG_LEVEL (DEBUG_LEVEL_NONE | DEBUG_LEVEL_HEX)	//(DEBUG_LEVEL_8 | )	//


#ifdef DEBUG
#define LOG(...)							serialProtocolModuleLog(DEBUG_MODULE_MASTER, DEBUG_LEVEL_ALWAYS, __VA_ARGS__)
#define LOG_D(_module, _debugLevel, ...)	serialProtocolModuleLog(_module, _debugLevel, __VA_ARGS__)
#define LOG_ERR(...)  						serialProtocolModuleLog(DEBUG_MODULE_MASTER, DEBUG_LEVEL_ERROR, __VA_ARGS__)
#else
#define LOG(...) {}
#define LOG_D(...) {}
#define LOG_ERR(...) {}
#endif

#endif //__SERIAL_PROTOCOL_H
