#ifndef __SD_H__
#define __SD_H__

#include "common.h"

#define SD_FILE_NAME_MAX_LENGTH						30


typedef enum SD_RESULT_E{
	SD_RESULT_OK,
	SD_RESULT_ERROR_ALREADY_INITIALIZED,
	SD_RESULT_ERROR_INIT_FAILED,
	SD_RESULT_ERROR,
	SD_RESULT_WRONG_PARAM,
	SD_RESULT_ERROR_FILE_CREATE,
	SD_RESULT_ERROR_WRITE,
	SD_RESULT_ERROR_WRITE_FAILED_LENGTH_MISMATCH,
	SD_RESULT_ERROR_MOUNT_FAILED,
	SD_RESULT_ERROR_MUTEX_INIT_FAILED,
	SD_RESULT_ERROR_QUEUE_CREATE_FAILED,
	SD_RESULT_ERROR_TASK_CREATE_FAILED,
	SD_RESULT_ERROR_READ,
	SD_RESULT_ERROR_CREATE_DIRECTORY_FAILED,
	SD_RESULT_ERROR_FILE_DOES_NOT_EXIST,
	SD_RESULT_ERROR_DIRECTORY_DOES_NOT_EXIST,
	SD_RESULT_ERROR_OPEN_FILE,
	SD_RESULT_ERROR_REPLACE_INDEX_OUT_OF_BOUNDS,
	SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE,
	SD_RESULT_ERROR_DELETE_FAILED,
	SD_RESULT_ERROR_FAILED_TO_OPEN_DIR,
	SD_RESULT_ERROR_FAILED_TO_RENAME_FILE,
	SD_RESULT_ERROR_DIRECTORY_NAME_TOO_LONG,
	SD_RESULT_ERROR_READING_SD_INFO,
	SD_RESULT_ERROR_WRONG_PARAMETER
}SD_RESULT_E;

typedef enum{
	SD_STATUS_MOUNT,
	SD_STATUS_UNMOUNT	
}SD_STATUS_E;

typedef enum{
	SD_MUTEX_COMMAND_WAIT,
	SD_MUTEX_COMMAND_RELEASE
}SD_MUTEX_COMMAND_E;

typedef struct
{
	union
	{
		struct
		{
			uint32_t second:5;
			uint32_t minute:6;
			uint32_t hour:5;
			uint32_t day:5;
			uint32_t month:4;
			uint32_t year:7;
		}bitmap;
		
		struct
		{
			uint32_t time:16;
			uint32_t date:16;			
		}bitmap_word;
		
		uint32_t time;
	};
}SD_TIME_E;

/***********************************************************************
 * Function name: sdInit
 * Description: Init SD card
 * Parameters : 
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdInit(void);
/***********************************************************************
 * Function name: sdOpenFile
 * Description: Create file
 * Parameters : file_name - File name
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdOpenFile(uint8_t* file_name);
/***********************************************************************
 * Function name: sdWriteDataToFile
 * Description: Write data to file
 * Parameters : file_name - File name to write in
 *				data - pointer to data, length - data length
 *				* write_index return value of writing index (file size before write)
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_OPEN_FILE - error openning file
 *		SD_RESULT_ERROR_FILE_CREATE - error creating file
 *		SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE - error placing pointer at end of file
 *		SD_RESULT_ERROR_WRITE - error writing to file
 *		SD_RESULT_ERROR_MOUNT_FAILED - mount SD failed
 ***********************************************************************/
SD_RESULT_E sdWriteDataToFile(IN uint8_t* file_name, 
							  IN uint8_t* data, 
							  IN uint32_t length,
							  OUT uint32_t * write_index);
/***********************************************************************
 * Function name: sdReplaceDataInFile
 * Description: Replace data on file
 * Parameters : file_name - File name to write in
 *				data - pointer to data
 *				length - data length
 *				write_index - offset of file for data replacement
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_OPEN_FILE - error openning file
 *		SD_RESULT_ERROR_FILE_CREATE - error creating file
 *		SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE - error placing pointer at end of file
 *		SD_RESULT_ERROR_WRITE - error writing to file
 *		SD_RESULT_ERROR_MOUNT_FAILED - mount SD failed
 *		SD_RESULT_ERROR_REPLACE_INDEX_OUT_OF_BOUNDS - requested index exceeds file size
 ***********************************************************************/
SD_RESULT_E sdReplaceDataInFile(IN uint8_t* file_name,
							  IN uint8_t* data,
							  IN uint16_t length,
							  IN uint32_t write_index);
/***********************************************************************
 * Function name: sdDir
 * Description: Prints all files in root directory
 * Parameters : 
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdDir(void);
/***********************************************************************
 * Function name: sdMakeDir
 * Description: Create directory
 * Parameters : 
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_CREATE_DIRECTORY_FAILED - Directory creation failed
 *		SD_RESULT_ERROR_MOUNT_FAILED - mount or unmount failed
 ***********************************************************************/
SD_RESULT_E sdMakeDir(IN uint8_t* dir_name);
/***********************************************************************
 * Function name: sdReadDataFromFile
 * Description: Read data from file
 * Parameters : file_name - File name to read from, data_index - data index to read from
 *				length - data length, data - pointer to data, 
 *				data_length - length of read data
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 * 		SD_RESULT_ERROR_READ - error reading
 * 		SD_RESULT_ERROR_MOUNT_FAILED - SD mount failed
 * 		SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE - error moving pointer to data
 *
 ***********************************************************************/
SD_RESULT_E sdReadDataFromFile(IN uint8_t* file_name, 
							   IN uint16_t length,
							   IN uint32_t data_index,
							   OUT uint8_t* data,
							   OUT uint16_t* data_length);
/***********************************************************************
 * Function name: sdDeleteFile
 * Description: Delete specific file in SD card
 * Parameters : file_name - File name to delete
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_MOUNT_FAILED - SD mount failed
 * 		SD_RESULT_ERROR_DELETE_FAILED - file delete failed
 ***********************************************************************/
SD_RESULT_E sdDeleteFile(IN uint8_t* file_name);
/***********************************************************************
 * Function name: sdRenameFile
 * Description: Rename file name
 * Parameters : file_name_old - old file name, file_name_new - new file name
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_FAILED_TO_RENAME_FILE - failed to rename file
 *
 ***********************************************************************/
SD_RESULT_E sdRenameFile(IN uint8_t* file_name_old, 
						 IN uint8_t* file_name_new);
/***********************************************************************
 * Function name: sdDeleteDirContect
 * Description: Write data to file
 * Parameters : 
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_DELETE_FAILED - file delete failed
 *		SD_RESULT_ERROR_FAILED_TO_OPEN_DIR - failed to opern directory
 *		SD_RESULT_ERROR_MOUNT_FAILED - Fatfs mount failed
 *		SD_RESULT_ERROR_DIRECTORY_NAME_TOO_LONG - directory name too long
 ***********************************************************************/
SD_RESULT_E sdDeleteDirContect(IN uint8_t* dir_name_in); 
/***********************************************************************
 * Function name: sdFormat
 * Description: Write data to file
 * Parameters : 
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_DELETE_FAILED - file delete failed
 *		SD_RESULT_ERROR_FAILED_TO_OPEN_DIR - failed to opern directory
 *		SD_RESULT_ERROR_MOUNT_FAILED - Fatfs mount failed
 ***********************************************************************/
SD_RESULT_E sdFormat(void);
/***********************************************************************
 * Function name: sdSendMsgToQueue
 * Description: Send message to SD card queue
 * Parameters : file_name - file name to write 
 * 				buff - pointer to buffer, length - data length
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdSendMsgToQueue(uint8_t* file_name, uint8_t* buff, uint16_t length);
/***********************************************************************
 * Function name: sdFileExist
 * Description: Checks if file exist
 * Parameters : file_name - File name to check
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_MOUNT_FAILED - SD mount failed
 *		SD_RESULT_ERROR_FILE_DOES_NOT_EXIST - file doesn't exist
 ***********************************************************************/
SD_RESULT_E sdFileExist(IN uint8_t* file_name);
/***********************************************************************
 * Function name: sdDirExist
 * Description: Checks if directory exist
 * Parameters : 
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_DIRECTORY_DOES_NOT_EXIST - directory doesn't exist
 * 		SD_RESULT_ERROR_MOUNT_FAILED - SD mount failed
 *
 ***********************************************************************/
SD_RESULT_E sdDirExist(uint8_t* dir_name);
/***********************************************************************
 * Function name: sdChangeDir
 * Description: Set current directory
 * Parameters : 
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdChangeDir(IN uint8_t* dir_name);
/***********************************************************************
 * Function name: sdGetDriveInfo
 * Description: Write data to file
 * Parameters : 
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_DELETE_FAILED - file delete failed
 *		SD_RESULT_ERROR_FAILED_TO_OPEN_DIR - failed to opern directory
 *		SD_RESULT_ERROR_MOUNT_FAILED - Fatfs mount failed
 ***********************************************************************/
SD_RESULT_E sdGetSdInfo(uint32_t *total_size, uint32_t *free_size);
/***********************************************************************
 * Function name: sdGetFileSize
 * Description: Read file size
 * Parameters : file_name - file name to read, file_size - file size
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR_MOUNT_FA   ILED - Fatfs mount failed
 *		SD_RESULT_ERROR_FILE_DOES_NOT_EXIST - file doesn't exist
 ***********************************************************************/
SD_RESULT_E sdGetFileSize(IN uint8_t* file_name, 
						  OUT uint32_t *file_size);
 /***********************************************************************
 * Function name: sdGetSdWriteBufferFree
 * Description: Get size of free space in buffer
 * Parameters : free_space - free space in precentage
 * Returns :
 *
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR_WRONG_PARAMETER - wrong parameter
 ***********************************************************************/
SD_RESULT_E sdGetSdWriteBufferFree(OUT uint8_t* free_space);
SD_RESULT_E sdTestSdCard(void);
SD_RESULT_E sdGetHighWaterMark(UBaseType_t * ret_val);
#endif //__SD_H__
