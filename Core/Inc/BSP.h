#ifndef __BSP_H__
#define __BSP_H__


#include "stm32l4xx_hal.h"
#include "common.h"


#define TEMPERATURE_3_3_MV							3300
#define TEMPERATURE_12_BIT							0xFFF
#define TEMPERATURE_REFERENCE_VOLTAGE_AT_25C		0.760
#define TEMPERATURE_AVERAGE_SLOPE_VOLTAGE			0.0025
#define TEMPERATURE_OFFSET_VOLTAGE					25


#define GMP_EVB //STM32L476RG
#define GMP_HW_REV_1 //STM32L471

#ifdef GMP_HW_REV_1
#undef GMP_EVB
#endif //GMP_HW_REV_1


#ifdef GMP_EVB

// -------------------SPI 1----------------------

	#define SPI1_SCLK_PIN	GPIO_PIN_5
	#define SPI1_SCLK_PORT	GPIOA

	#define SPI1_MISO_PIN	GPIO_PIN_6
	#define SPI1_MISO_PORT	GPIOA

	#define SPI1_MOSI_PIN	GPIO_PIN_7
	#define SPI1_MOSI_PORT	GPIOA
// -------------------SPI 1----------------------


// -------------------SPI 2 - accelerometer----------------------
	#define PPG_SPI	SPI2

	#define	SPI2_PORT		GPIOB

	#define SPI2_SCLK_PIN	GPIO_PIN_10
	#define SPI2_SCLK_PORT   GPIOB

	#define SPI2_MISO_PIN	GPIO_PIN_2
	#define SPI2_MISO_PORT   GPIOC

	#define SPI2_MOSI_PIN	GPIO_PIN_3
	#define SPI2_MOSI_PORT   GPIOC
// -------------------SPI 2----------------------

// --------------------I2C-----------------------

// --------------------I2C-----------------------

	#define ECG1_CS_PORT		GPIOB
	#define ECG1_CS_PIN			GPIO_PIN_9

	#define ECG2_CS_PORT		GPIOA
	#define ECG2_CS_PIN			GPIO_PIN_4

	#define ECG_START_PORT		GPIOB
	#define ECG_START_PIN		GPIO_PIN_5

	#define ECG1_DRDY_PORT		GPIOB
	#define ECG1_DRDY_PIN		GPIO_PIN_6

	#define ECG2_DRDY_PORT		GPIOB
	#define ECG2_DRDY_PIN		GPIO_PIN_7

	#define ECG1_RESET_PORT		GPIOC
	#define ECG1_RESET_PIN		GPIO_PIN_7

	#define MCU_PWRD_INT_PORT	GPIOB
	#define MCU_PWRD_INT_PIN	GPIO_PIN_13

	#define BLE_MCU_WKUP_PIN	GPIO_PIN_13
	#define BLE_MCU_WKUP_PORT	GPIOC

	#define DC_IN_PIN			GPIO_PIN_1
	#define DC_IN_PORT			GPIOA

	#define LED_RED_PIN			GPIO_PIN_0
	#define LED_RED_PORT		GPIOC

	#define LED_BLUE_PIN		GPIO_PIN_1
	#define LED_BLUE_PORT		GPIOC

	#define INTB_PIN			GPIO_PIN_6
	#define INTB_PORT			GPIOC

	#define INT2B_PIN			GPIO_PIN_8
	#define INT2B_PORT			GPIOC

	#define MCU_BLE_WKUP_PIN	GPIO_PIN_11
	#define MCU_BLE_WKUP_PORT	GPIOC

	#define BLE_RST_N_PIN		GPIO_PIN_0
	#define BLE_RST_N_PORT		GPIOB

	#define CHG_ON_OFF_IND_PIN	GPIO_PIN_2
	#define CHG_ON_OFF_IND_PORT	GPIOD

#endif //GMP_EVB

#ifdef GMP_HW_REV_1


// -------------------SPI 1----------------------
	#define SPI1_SCLK_PIN	GPIO_PIN_13
	#define SPI1_SCLK_PORT	GPIOE

	#define SPI1_MISO_PIN	GPIO_PIN_14
	#define SPI1_MISO_PORT	GPIOE

	#define SPI1_MOSI_PIN	GPIO_PIN_15
	#define SPI1_MOSI_PORT	GPIOE
// -------------------SPI 1----------------------

// -------------------SPI 2 - accelerometer----------------------
	#define SPI2_SCLK_PIN	GPIO_PIN_13
	#define SPI2_SCLK_PORT	GPIOB

	#define SPI2_MISO_PIN	GPIO_PIN_14
	#define SPI2_MISO_PORT	GPIOB

	#define SPI2_MOSI_PIN	GPIO_PIN_15
	#define SPI2_MOSI_PORT	GPIOB
// -------------------SPI 2----------------------

// --------------------I2C-----------------------

// --------------------I2C-----------------------

	#define ECG1_CS_PORT		GPIOE
	#define ECG1_CS_PIN			GPIO_PIN_12

	#define ECG2_CS_PORT		GPIOA
	#define ECG2_CS_PIN			GPIO_PIN_4

	#define ECG_START_PORT		GPIOE
	#define ECG_START_PIN		GPIO_PIN_11

	#define ECG1_DRDY_PORT		GPIOE
	#define ECG1_DRDY_PIN		GPIO_PIN_7

	#define ECG2_DRDY_PORT		GPIOE
	#define ECG2_DRDY_PIN		GPIO_PIN_8

	#define ECG1_RESET_PORT		GPIOE
	#define ECG1_RESET_PIN		GPIO_PIN_9

	#define ECG2_RESET_PORT		GPIOE
	#define ECG2_RESET_PIN		GPIO_PIN_10

	#define BLE_STATUS_PIN		GPIO_PIN_13
	#define BLE_STATUS_PORT		GPIOC

	#define LED_RED_PIN			GPIO_PIN_0
	#define LED_RED_PORT		GPIOC

	#define LED_BLUE_PIN		GPIO_PIN_1
	#define LED_BLUE_PORT		GPIOC

	#define BLE_RESET_PIN		GPIO_PIN_0
	#define BLE_RESET_PORT		GPIOB

	#define BLE_UART_EN_PIN		GPIO_PIN_1
	#define BLE_UART_EN_PORT	GPIOB

	#define BLE_AT_COMM_EN_PIN	GPIO_PIN_2
	#define BLE_AT_COMM_EN_PORT	GPIOB

	#define ADC12_IN11_PIN		GPIO_PIN_6
	#define ADC12_IN11_PORT		GPIOA

	#define ACCEL_INT1_PIN		GPIO_PIN_11
	#define ACCEL_INT1_PORT		GPIOA

	#define ACCEL_INT2_PIN		GPIO_PIN_12
	#define ACCEL_INT2_PORT		GPIOA

	#define TP1_PIN				GPIO_PIN_8
	#define TP1_PORT			GPIOB

	#define PM_TIM2_CH4_PIN		GPIO_PIN_11
	#define PM_TIM2_CH4_PORT	GPIOB

	#define ACCEL_CS_PIN		GPIO_PIN_12
	#define ACCEL_CS_PORT		GPIOB

	#define DTM_BLE_RX_PIN		GPIO_PIN_4
	#define DTM_BLE_RX_PORT		GPIOC

	#define DTM_BLE_TX_PIN		GPIO_PIN_5
	#define DTM_BLE_TX_PORT		GPIOC

	#define BUTTON_INT_PIN		GPIO_PIN_6
	#define BUTTON_INT_PORT		GPIOC

	#define TP2_PIN				GPIO_PIN_9
	#define TP2_PORT			GPIOC

	#define SDMMC1_EN_PIN		GPIO_PIN_3
	#define SDMMC1_EN_PORT		GPIOD

	#define BAT_LOAD_EN_PIN		GPIO_PIN_4
	#define BAT_LOAD_EN_PORT	GPIOE

	#define SDCARD_IN_PIN		GPIO_PIN_3
	#define SDCARD_IN_PORT		GPIOF

	#define HW_VER_ADC3_IN7_PIN	 GPIO_PIN_4
	#define HW_VER_ADC3_IN7_PORT GPIOF

	#define IMP_LL_ON_PIN		GPIO_PIN_5
	#define IMP_LL_ON_PORT		GPIOD

	#define IMP_RA_ON_PIN		GPIO_PIN_6
	#define IMP_RA_ON_PORT		GPIOD

	#define IMP_LA_ON_PIN		GPIO_PIN_7
	#define IMP_LA_ON_PORT		GPIOD

	#define PM_OFF_PIN			GPIO_PIN_10
	#define PM_OFF_PORT			GPIOD

	#define IMP_PWM_TIM4_CH2_PIN  GPIO_PIN_13
	#define IMP_PWM_TIM4_CH2_PORT GPIOD

	#define IMP_ADC12_IN11_PIN	GPIO_PIN_6
	#define IMP_ADC12_IN11_PORT	GPIOA

	#define SD_PWR_ON_PORT		GPIOE
	#define SD_PWR_ON_PIN		GPIO_PIN_1


#endif //GMP_HW_REV_1

	#define   BSP_PLLM 9
	#define   BSP_PLLN 211
	//TODO:Asaf commented out
	//#define   BSP_PLLP RCC_PLLP_DIV4
	#define   BSP_PLLQ 8
	#define BSP_HSE_VALUE    ((uint32_t)16384000) /*!< Value of the External oscillator in Hz */



typedef enum{
	BSP_SPI_ECG1,
	BSP_SPI_ECG2,
	BSP_SPI_ACCEL//,
//	BSP_SPI_RAM
}BSP_SPI_DEVICE_TYPE_E;

typedef enum{
	BSP_TEST_TYPE_ECG 		= 1,
	BSP_TEST_TYPE_PPG		= 2,
	BSP_TEST_TYPE_THERMO	= 4
}BSP_TESTS_TYPE_E;

typedef enum{
	BSP_UART_DEBUG,
	BSP_UART_BLE
}BSP_UART_E;

typedef enum {
	BSP_RESULT_OK,
	BSP_RESULT_ERROR,
	BSP_RESULT_ERROR_ALREADY_RUNNING,
	BSP_RESULT_BAD_COMMAND,
	BSP_RESULT_ERROR_INVALID_PIN,
	BSP_RESULT_ERROR_INVALID_IRQ,
	BSP_RESULT_ERROR_SPI_TX_FAILED,
	BSP_RESULT_ERROR_SPI_RX_FAILED,
	BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST,
	BSP_RESULT_ERROR_NOT_INITIALIZED,
	BSP_RESULT_ERROR_UART_TX_FAILED,
	BSP_RESULT_ERROR_UART_DOES_NOT_EXIST,
	BSP_RESULT_ERROR_MUTEX_ERROR,
	BSP_RESULT_ERROR_HAL_BUSY,
	BSP_RESULT_ERROR_TIMEOUT,
	BSP_RESULT_ERROR_LED_INVALID,
	BSP_RESULT_ERROR_ADC_DOES_NOT_EXIST,
	BSP_RESULT_ERROR_PM,
	BSP_RESULT_ERROR_IMPEDANCE_INIT_FAILED,
	BSP_RESULT_ERROR_GPIO_ARRAY_MISMATCH,
	BSP_RESULT_ERROR_ADC_ERROR,
	BSP_RESULT_ERROR_NOT_IMPLEMENTED,
	BSP_RESULT_ERROR_INVALID_ENUM,
	BSP_RESULT_ERROR_INVALID_PIN_STATE
}BSP_RESULT_E;

typedef enum{
	BSP_SLEEP_MODE,
	BSP_STANDBY_MODE,
	BSP_POWER_DOWN_MODE,
	BSP_POWER_UP_MODE
}BSP_POWER_MODE_E;

typedef enum
{
	BSP_PIN_RESET = 0,
	BSP_PIN_SET,
    BSP_PIN_TOOGLE
}BSP_PIN_STATE_E;

typedef enum
{
	BSP_IRQ_WWDG 				= 0,
	BSP_IRQ_BLE_MCU_WKUP		= 6,
  	BSP_IRQ_DC_IN				= 7,
  	BSP_IRQ_CHG_ON_OFF_IND		= 8,
  	BSP_IRQ_LB_INT				= 9,
	BSP_IRQ_PPG 				= 23,
	BSP_IRQ_USART1       		= 37,
	BSP_IRQ_ADS 				= 40,
	BSP_IRQ_OTG_FS       		= 67,
}BSP_IRQ_E;

typedef enum
{
	BSP_ADC_HW_IND 				= 0,
	BSP_ADC_BATTERY_LEVEL		= 1,
	BSP_ADC_TEMPERATURE			= 2,
	BSP_ADC_IMPEDANCE			= 3,
	BSP_ADC_MAX
}BSP_ADC_E;

typedef enum{
	BSP_FLASH_SECTOR_0 = 0,		/*!< Sector Number 0   */
	BSP_FLASH_SECTOR_1 = 1,     /*!< Sector Number 1   */
	BSP_FLASH_SECTOR_2 = 2,     /*!< Sector Number 2   */
	BSP_FLASH_SECTOR_3 = 3,     /*!< Sector Number 3   */
	BSP_FLASH_SECTOR_4 = 4,     /*!< Sector Number 4   */
	BSP_FLASH_SECTOR_5 = 5,     /*!< Sector Number 5   */
	BSP_FLASH_SECTOR_6 = 6,     /*!< Sector Number 6   */
	BSP_FLASH_SECTOR_7 = 7      /*!< Sector Number 7   */
}BSP_FLASH_SECTORS;

typedef enum{
	BSP_LED_RED,
	BSP_LED_BLUE,
	BSP_LED_MAX
}BSP_LEDS_E;

typedef enum
{
	BSP_LED_OFF = 0,
	BSP_LED_ON
}BSP_LED_STATE_E;


typedef enum {
	BSP_PIN_TYPE_ECG1_CS,
	BSP_PIN_TYPE_ECG2_CS,
	BSP_PIN_TYPE_ECG_START,
	BSP_PIN_TYPE_ECG1_DRDY,
	BSP_PIN_TYPE_ECG2_DRDY,
	BSP_PIN_TYPE_ECG1_RESET,
	BSP_PIN_TYPE_ECG2_RESET,
	BSP_PIN_TYPE_LED_RED,
	BSP_PIN_TYPE_LED_BLUE,
	BSP_PIN_TYPE_BLE_RESET,
	BSP_PIN_TYPE_BLE_UART_EN,
	BSP_PIN_TYPE_BLE_AT_COMM_EN,
	BSP_PIN_TYPE_ACCEL_CS,
	BSP_PIN_TYPE_SDMMC1_EN,
	BSP_PIN_TYPE_BAT_LOAD_EN,
	BSP_PIN_TYPE_SDCARD_IN,
//	BSP_PIN_TYPE_DTM_BLE_RX,
//	BSP_PIN_TYPE_DTM_BLE_TX,
	BSP_PIN_TYPE_PM,
	BSP_PIN_TYPE_PM_OFF,
	BSP_PIN_TYPE_ACCEL_INT1,
	BSP_PIN_TYPE_ACCEL_INT2,
	BSP_PIN_TYPE_IMP_LL_SWITCH,
	BSP_PIN_TYPE_IMP_LA_SWITCH,
	BSP_PIN_TYPE_IMP_RA_SWITCH,
	BSP_PIN_TYPE_BUTTON,
	BSP_PIN_TYPE_TP1,
	BSP_PIN_TYPE_TP2,
	BSP_PIN_TYPE_SD_PWR_ON,
	BSP_PIN_TYPE_MAX_USED
}BSP_PIN_TYPE_E;

typedef enum{
	BSP_BLE_CONFIG_SOURCE_MCU,
	BSP_BLE_CONFIG_SOURCE_DEBUG
}BSP_BLE_CONFIG_SOURCE_E;


typedef enum {
	BSP_PM_EVENT_NOTHING,
	BSP_PM_EVENT_PM_DETECTED,
    BSP_PM_EVENT_PM_NOT_DETECTED,
    BSP_PM_EVENT_NOISE_STATE,
	BSP_PM_EVENT_PM_IDLE_STATE
}BSP_PM_EVENT_TYPE_E;

typedef enum {
	BSP_BUTTON_EVENT_PRESSED,
	BSP_BUTTON_EVENT_RELEASED,
	BSP_BUTTON_EVENT_BOOKMARK,
	BSP_BUTTON_EVENT_MAX
}BSP_BUTTON_EVENT_E;


typedef enum {
//	BSP_IMPEDANCE_PWM_MODE_INIT,
	BSP_IMPEDANCE_PWM_MODE_START,
	BSP_IMPEDANCE_PWM_MODE_STOP,
	BSP_IMPEDANCE_PWM_MODE_MAX
} BSP_IMPEDANCE_PWM_MODE_E;

typedef enum{
	BSP_WD_TIME_4_SEC,
	BSP_WD_TIME_8_SEC
}BSP_WD_TIME_E;

typedef enum{
	BSP_MODE_SET_ON,
	BSP_MODE_SET_OFF
}BSP_MODE_SET_E;

typedef struct {
	BSP_PM_EVENT_TYPE_E 	event_type;
	//data_u				payload;
}bsp_pm_event_s;

typedef struct{
	uint8_t 	seconds;
	uint8_t		minutes;
	uint8_t		hours;
	uint16_t	msec;
}BSP_TIME_STRUCT;

typedef struct{
	uint8_t 	month;
	uint8_t		date;
	uint8_t		year;
}BSP_DATE_STRUCT;

#define BSP_TIMESTAMP_LENGTH	8

typedef union {
	struct {
		uint8_t 	year;
		uint8_t 	month;
		uint8_t		date;
		uint8_t		hours;
		uint8_t		minutes;
		uint8_t 	seconds;
		uint16_t	msec;
	}field;
	uint8_t raw[BSP_TIMESTAMP_LENGTH];
}BSP_RTC_TIMESTAMP;

typedef struct {
	BSP_DATE_STRUCT	date;
	BSP_TIME_STRUCT	time;
}BSP_TIME_DATE_STRUCT;

// typedef struct{
//     int tm_sec;
//     int tm_min;
//     int tm_hour;
//     int tm_mday;
//     int tm_mon;
//     int tm_year;
//     int tm_wday;
//     int tm_yday;
//     int tm_isdst;
// }RV30287_rtc_time;

//{
//	{GPIOA, GPIO_PIN_4},//BSP_PIN_TYPE_ECG1_CS,
//	{GPIOB, GPIO_PIN_0},//BSP_PIN_TYPE_ECG_START,
//	{GPIOB, GPIO_PIN_10},//BSP_PIN_TYPE_ECG1_DRDY,
//	{GPIOA, GPIO_PIN_9},//BSP_PIN_TYPE_ECG1_RESET,
//	{GPIOB, GPIO_PIN_12},//BSP_PIN_TYPE_PPG_CS,
//	{GPIOB, GPIO_PIN_5},//BSP_PIN_TYPE_PPG_DRDY,
//	{GPIOB, GPIO_PIN_1},//BSP_PIN_TYPE_PPG_RESET,
//	{GPIOB, GPIO_PIN_2},//BSP_PIN_TYPE_PPG_PWRDN,
//	{GPIOC, GPIO_PIN_13},//BSP_PIN_TYPE_LED,
//	{GPIOA, GPIO_PIN_0},//BSP_PIN_TYPE_BLE_MCU_WKUP,
//	{GPIOA, GPIO_PIN_8},//BSP_PIN_TYPE_ECG_CLK
//	{GPIOA, GPIO_PIN_1},//BSP_PIN_TYPE_PA1_NOT_CONNECTED
//	{GPIOA, GPIO_PIN_5},//BSP_PIN_TYPE_SPI1_CLK,
//	{GPIOA, GPIO_PIN_6},//BSP_PIN_TYPE_SPI1_MISO,
//	{GPIOA, GPIO_PIN_7},//BSP_PIN_TYPE_SPI1_MOSI,
//	{GPIOB, GPIO_PIN_10},//BSP_PIN_TYPE_SPI2_CLK,
//	{GPIOB, GPIO_PIN_14},//BSP_PIN_TYPE_SPI2_MISO,
//	{GPIOB, GPIO_PIN_15},//BSP_PIN_TYPE_SPI2_MOSI,
//	{GPIOB, GPIO_PIN_8},//BSP_PIN_TYPE_I2C_CLK,
//	{GPIOB, GPIO_PIN_9},//BSP_PIN_TYPE_I2C_SDA,
//	{GPIOB, GPIO_PIN_6},//BSP_PIN_TYPE_UART1_TX,
//	{GPIOB, GPIO_PIN_7},//BSP_PIN_TYPE_UART1_RX,
//	{GPIOA, GPIO_PIN_2},//BSP_PIN_TYPE_UART2_TX,
//	{GPIOA, GPIO_PIN_3},//BSP_PIN_TYPE_UART2_RX,
//	{GPIOA, GPIO_PIN_12},//BSP_PIN_TYPE_USB_DP,
//	{GPIOA, GPIO_PIN_11},//BSP_PIN_TYPE_USB_DM,
//	{GPIOB, GPIO_PIN_13},//BSP_PIN_TYPE_PPG_DIAG_EN,
//	{GPIOB, GPIO_PIN_0},//BSP_PIN_TYPE_ADS_BFA_NOT_SWITCH,
//	{GPIOA, GPIO_PIN_14},//BSP_PIN_TYPE_JTCK_SWDCLK,
//	{GPIOA, GPIO_PIN_13},//BSP_PIN_TYPE_JTMS_SWDIO,
//	{GPIOA, GPIO_PIN_15},//BSP_PIN_TYPE_JTDI,
//	{GPIOB, GPIO_PIN_3},//BSP_PIN_TYPE_JTDO_SWO
//	{GPIOB, GPIO_PIN_4},//BSP_PIN_TYPE_JTRST
//
//};


// Exported functions
/***********************************************************************
 * Function name: bspInit
 * Description: BSP init
 * Parameters :
 *		None
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - Mutex isn't created
 ***********************************************************************/
BSP_RESULT_E bspInit(void);
// mutex protected call for i2c (common for few peripherals)
/***********************************************************************
 * Function name: bsp_enable_button
 * Description: enable button's interrupt
 * Parameters :
 *		enable - enable/disable to button interrupt
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 ***********************************************************************/
BSP_RESULT_E bsp_enable_button(IN BOOL enable);
/***********************************************************************
 * Function name: bsp_button_pressed_callback
 * Description: callback that need implementation
 * set parameter :"button_pressed" to true
 * Parameters :
 *		enable -
  * Returns :
 *		BSP_RESULT_OK - on success
 ***********************************************************************/
BSP_RESULT_E bsp_button_pressed_callback(IN BOOL enable);
/***********************************************************************
 * Function name: bsp_check_button
 * Description: check status of parameter "button_pressed" and if true set to false
 * Parameters :
 *		None
 * Returns :
 *		BSP_RESULT_OK - on success
 ***********************************************************************/
BSP_RESULT_E bsp_check_button(void);
/***********************************************************************
 * Function name: bsp_i2c_mem_read
 * Description: Read an amount of data in blocking mode from a specific memory address
 * Synchronized function (mutex)
 * Parameters :
 * 		i2c_addr - Target device address
 * 		reg_addr - Internal memory address
 * 		reg_add_size - Size of internal memory address
 * 		buff - Pointer to data buffer
 * 		len - Amount of data to be sent *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - length 0 or data is NULL
 *		BSP_RESULT_ERROR_ALREADY_RUNNING - Mutex is locked
 *		BSP_RESULT_ERROR - Mutex isn't released or message not received
 ***********************************************************************/
BSP_RESULT_E bsp_i2c_mem_read(
		IN uint8_t 		i2c_addr,
		IN uint8_t 		reg_addr,
		IN uint8_t 		reg_add_size,
		IN uint16_t 	len,
		OUT uint8_t* 	buff);
/***********************************************************************
 * Function name: bsp_i2c_mem_write
 * Description: Write an amount of data in blocking mode to a specific memory address.
 * Synchronized function (mutex)
 * Parameters :
 * 		i2c_addr - Target device address
 * 		reg_addr - Internal memory address
 * 		reg_add_size - Size of internal memory address
 * 		buff - Pointer to data buffer
 * 		len - Amount of data to be sent
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - length 0 or data is NULL
 *		BSP_RESULT_ERROR_ALREADY_RUNNING - Mutex is locked
 *		BSP_RESULT_ERROR - Mutex isn't released or message not sent
 ***********************************************************************/
BSP_RESULT_E bsp_i2c_mem_write(
		IN uint8_t 		i2c_addr,
		IN uint8_t 		reg_addr,
		IN uint8_t 		reg_add_size,
		IN uint8_t* 	buff,
		IN uint16_t 	len);
/***********************************************************************
 * Function name: bsp_i2c_rx
 * Description: I2C RX
 * Synchronized function (mutex)
 * Parameters :
 *		i2c_addr - I2C address, buff - buffer to read into,
 *		len - data length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - length 0 or data is NULL
 *		BSP_RESULT_ERROR_ALREADY_RUNNING - Mutex is locked
 *		BSP_RESULT_ERROR - Mutex isn't released or message not received
 ***********************************************************************/
BSP_RESULT_E bsp_i2c_rx(
		IN uint8_t 		i2c_addr,
		IN uint8_t 		len,
 		OUT uint8_t* 	buff);
/***********************************************************************
 * Function name: bsp_i2c_tx
 * Description: I2C TX
 * Synchronized function (mutex)
 * Parameters :
 *		i2c_addr - I2C address, buff - buffer to send,
 *		len - data length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - length 0 or data is NULL
 *		BSP_RESULT_ERROR_ALREADY_RUNNING - Mutex is locked
 *		BSP_RESULT_ERROR - Mutex isn't released or message not sent
 ***********************************************************************/
BSP_RESULT_E bsp_i2c_tx(
		IN uint8_t 		i2c_addr,
		IN uint8_t* 	buff,
		IN uint8_t 		len);
/***********************************************************************
 * Function name: bsp_enter_sleep_mode
 * Description: Put device in sleep/standby mode
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *
 ***********************************************************************/
BSP_RESULT_E bsp_enter_mode(uint8_t mode);
/***********************************************************************
 * Function name: bsp_write_pin
 * Description: Write state to a specific pin
 * Parameters :
 *		gpio - port and pin (BSP_PIN_TYPE_E),
 *		pin_state - high/low
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_INVALID_PIN - if pin doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_write_pin(
		IN BSP_PIN_TYPE_E 	gpio,
		IN BSP_PIN_STATE_E 	pin_state);
///***********************************************************************
// * Function name: bsp_set_pin_as_input
// * Description: Set pin as input
// * Parameters :
// *		gpio - port and pin (BSP_PIN_TYPE_E),
// *
// * Returns :
// *		BSP_RESULT_OK - on success
// *		BSP_RESULT_ERROR_GPIO_ARRAY_MISMATCH - if pin doesn't exist
// *
// ***********************************************************************/
//BSP_RESULT_E bsp_set_pin_as_input(IN BSP_PIN_TYPE_E 	gpio);
/***********************************************************************
 * Function name: bspReadPin
 * Description: Read state of a specific pin
 * Parameters :
 *		gpio - port and pin (BSP_PIN_TYPE_E),
 *		pin_state - high/low
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_INVALID_PIN - if pin doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bspReadPin(
		IN BSP_PIN_TYPE_E 		gpio,
		OUT BSP_PIN_STATE_E* 	pin_state);
/***********************************************************************
 * Function name: bsp_set_irq_mode
 * Description: enable/disable a specific irq
 * Parameters :
 *		irq - IRQ number (BSP_IRQ_E)
 *		mode - enable/disable
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_WRONG_IRQ - if irq number invalid
 *
 ***********************************************************************/
BSP_RESULT_E bsp_set_irq_mode(
		IN BSP_IRQ_E irq,
		IN BOOL mode);
/***********************************************************************
 * Function name: bsp_set_irq_priority
 * Description: enable/disable a specific irq
 * Parameters :
 *		irq - IRQ number (BSP_IRQ_E)
 *		preempt_priority - Main priority
 *		sub_priority - Sub priority
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_WRONG_IRQ - if irq number invalid
 *
 ***********************************************************************/
BSP_RESULT_E bsp_set_irq_priority(
		IN BSP_IRQ_E irq,
		IN uint32_t preempt_priority,
		IN uint32_t sub_priority);
/***********************************************************************
 * Function name: bsp_spi_transmit
 * Description: SPI tx
 * Parameters :
 *		device_type - SPI type (BSP_SPI_DEVICE_TYPE_E), pData - data in,
 *		size - data length, timeout - timeout
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_SPI_TX_FAILED - SPI TX failed
 *		BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST - SPI doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_spi_transmit(
		IN BSP_SPI_DEVICE_TYPE_E device_type,
		IN uint8_t*		pData,
		IN uint16_t 	size,
		IN uint32_t 	timeout);
/***********************************************************************
 * Function name: bsp_spi_transmit_receive
 * Description: SPI tx and rx Blocking
 * Parameters :
 *		device_type - SPI type (BSP_SPI_DEVICE_TYPE_E), pTxData - tx data,
 *		pRxData - rx data,  size - data rx length, timeout - timeout
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_SPI_TX_FAILED - SPI TX failed
 *		BSP_RESULT_ERROR_SPI_RX_FAILED - SPI RX failed
 *		BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST - SPI doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_spi_transmit_receive(
		IN BSP_SPI_DEVICE_TYPE_E device_type,
		IN uint8_t *pTxData,
		IN uint16_t size,
		IN uint32_t timeout,
		OUT uint8_t *pRxData);
/***********************************************************************
 * Function name: bsp_spi_transmit_receive_dma
 * Description: SPI tx and rx with DMA ASynchronized
 * Parameters :
 *		device_type - SPI type (BSP_SPI_DEVICE_TYPE_E), pTxData - tx data,
 *		pRxData - rx data,  size - data rx length,
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_SPI_TX_FAILED - SPI TX failed
 *		BSP_RESULT_ERROR_SPI_RX_FAILED - SPI RX failed
 *		BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST - SPI doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_spi_transmit_receive_dma(
		IN BSP_SPI_DEVICE_TYPE_E device_type,
		IN uint8_t*		pTxData,
		IN uint16_t 	size,
		OUT uint8_t*	pRxData);

/***********************************************************************
 * Function name: bspSpiDmaStop
 * Description: stop SPI DMA last transaction
 * Parameters :
 *		device_type - SPI type (BSP_SPI_DEVICE_TYPE_E)
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - wrong type
 *		BSP_RESULT_ERROR - HAL error escalation
 *
 ***********************************************************************/
BSP_RESULT_E bspSpiDmaStop(IN BSP_SPI_DEVICE_TYPE_E device_type);

/***********************************************************************
 * Function name: bsp_uart_tx_dma
 * Description: UART TX
 * Parameters :
 *		uart - uart handle , pTxData - tx data,
 *		size - data tx length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_UART_TX_FAILED -  TX failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_dma(
		IN BSP_UART_E uart,
		IN uint8_t*		pTxData,
		IN uint16_t 	size);


/***********************************************************************
 * Function name: bsp_uart_tx_dma_serial_comm
 * Description: UART TX
 * Parameters :
 *		pTxData - tx data,
 *		size - data tx length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_UART_TX_FAILED -  TX failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_dma_serial_comm(
		IN uint8_t*		pTxData,
		IN uint16_t 	size);

/***********************************************************************
 * Function name: bsp_uart_tx_dma_ble
 * Description: UART TX
 * Parameters :
 *		pTxData - tx data,
 *		size - data tx length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_UART_TX_FAILED -  TX failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_dma_ble(
		IN uint8_t*		pTxData,
		IN uint16_t 	size);
/***********************************************************************
 * Function name: bsp_uart_tx_it
 * Description: UART TX
 * Parameters :
 *		uart - uart handle , pTxData - tx data,
 *		size - data tx length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_UART_TX_FAILED -  TX failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_it(
		IN BSP_UART_E uart,
		IN uint8_t*		pTxData,
		IN uint16_t 	size);
/***********************************************************************
 * Function name: bsp_uart_tx_complete_callback
 * Description: A call back for when a uart tx is finished
 * Parameters :
 *		uart - uart handle
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *			BSP_RESULT_ERROR_UART_DOES_NOT_EXIST - uart does not exist
 *			BSP_RESULT_ERROR_MUTEX_ERROR - mutex was not released
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_complete_callback(IN BSP_UART_E uart);
/***********************************************************************
 * Function name: bsp_uart_init
 * Description: UART init
 * Parameters :
 *		uart - uart handle
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - init failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_init(IN BSP_UART_E uart);
/***********************************************************************
 * Function name: bsp_uart_rx_complete_callback
 * Description: A call back for when a uart rx is finished
 * Parameters :
 *		uart - uart handle
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *			BSP_RESULT_ERROR_UART_DOES_NOT_EXIST - uart does not exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_rx_complete_callback(IN BSP_UART_E uart);
/***********************************************************************
 * Function name: bsp_uart_rx_dma
 * Description: Receive an amount of data in DMA mode
 * Parameters :
 *		uart - uart handle , rx_data - pointer to data buffer
 *		size - amount of data to be received
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - failed to receive data
 *		BSP_RESULT_ERROR_HAL_BUSY -
 *		BSP_RESULT_ERROR_HAL_TIMEOUT - timeout
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_rx_dma(
		IN BSP_UART_E 	uart,
		IN uint8_t 		rx_data,
		IN uint8_t* 	size);
/***********************************************************************
 * Function name: bsp_uart_receive_it
 * Description: UART receive via interrupt
 * Parameters :
 *		uart - uart handle , rx_buffer - rx data,
 *		size - Amount of data to be received
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_receive_it(
		IN BSP_UART_E uart,
		OUT uint8_t*		rx_buffer,
		IN uint16_t 	size);
/***********************************************************************
 * Function name: bsp_uart_receive
 * Description: UART receive
 * Parameters :
 *		uart - uart handle , rx_buffer - rx data,
 *		size - Amount of data to be received, timeout: Timeout duration
 *		size_rx - Amount of data received
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_receive(
		IN BSP_UART_E uart,
		IN uint8_t*		rx_buffer,
		IN uint16_t 	size,
		IN uint16_t		timeout,
		OUT uint16_t*		size_rx);
/***********************************************************************
 * Function name: bsp_uart_receive_dma
 * Description: UART receive via dma
 * Parameters :
 *		uart - uart handle , rx_buffer - rx data,
 *		size - Amount of data to be received
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_receive_dma(
		IN BSP_UART_E 	uart,
		OUT uint8_t*	rx_buffer,
		IN uint16_t 	size);
/***********************************************************************
 * Function name: bsp_uart_stop_dma
 * Description: Stop UART dma
 * Parameters :
 *		uart - uart handle
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_stop_dma(IN BSP_UART_E uart);
/***********************************************************************
 * Function name: bspUartDmaDataLeft
 * Description: Number of bytes to read in dma register
 * Parameters :
 *		uart - uart handle
 *		length - Number of bytes remaining
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspUartDmaDataLeft(
		IN BSP_UART_E uart,
		OUT uint16_t* length);
/***********************************************************************
 * Function name: bsp_wd_init
 * Description: Watchdog init
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_wd_init(void);
/***********************************************************************
 * Function name: bsp_led
 * Description: Turn On/Off specific Led
 * Parameters :
 * led - led type, led_state - On/Off
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_LED_NOT_VALID - Led not valid
 *
 ***********************************************************************/
BSP_RESULT_E bsp_led(
		IN BSP_LEDS_E led,
		IN BSP_LED_STATE_E led_state);
/***********************************************************************
 * Function name: enable_peripherals
 * Description: Peripherals Enable/Disable
 * Parameters : status - TRUE or FALSE
 *				test - type of test
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E enable_peripherals(IN BSP_TESTS_TYPE_E test,
								IN uint8_t status);
/***********************************************************************
 * Function name: bsp_ble_config
 * Description: BLE Config - set UART1 to input, set ble config/test to '0',
 * toggle ble reset pin
 * Parameters : source - from which interface the ble will be configured
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_ble_config(IN BSP_BLE_CONFIG_SOURCE_E source);
/***********************************************************************
 * Function name: bsp_ble_reset
 * Description: BLE Reset
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_ble_reset(void);
/***********************************************************************
 * Function name: bsp_restart_sensors
 * Description: Restart all sensors
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_restart_sensors(void);
/***********************************************************************
 * Function name: bsp_system_reset
 * Description: Restart System
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_system_reset(void);
/***********************************************************************
 * Function name: bspReadAdc
 * Description: Read ADC Channel
 * Parameters : adc - ADC channel
 * 				adc_value - Value from ADC
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *		BSP_RESULT_ERROR_ADC_DOES_NOT_EXIST - ADC not valid
 *
 ***********************************************************************/
BSP_RESULT_E bspReadAdc(IN BSP_ADC_E adc,
						OUT uint32_t* adc_value);
/***********************************************************************
 * Function name: bspHwVerGet
 * Description: Gets HW revision from BSP
 * Parameters : hw_ver - HW version
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspHwVerGet(OUT uint8_t* hw_ver);
/***********************************************************************
 * Function name: bspReadTemp
 * Description: Read ADC1 10
 * Parameters : temperature - MCU temperature
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspReadTemp(IN float32_t* temperature);
/***********************************************************************
 * Function name: bspRtcTimeGet
 * Description: Reads RTC time and Date
 * Parameters : BSP_TIME_STRUCT* time, BSP_DATE_STRUCT* date
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeGet(BSP_TIME_STRUCT* time, BSP_DATE_STRUCT* date);
/***********************************************************************
 * Function name: bspRtcTimeDiff
 * Description: Calculates difference between 2 dates
 * Parameters : BSP_TIME_DATE_STRUCT* time, BSP_TIME_DATE_STRUCT* date
 *				uint32_t * out_sec - result of time difference [seconds]
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeDiff(IN BSP_TIME_DATE_STRUCT* start, IN BSP_TIME_DATE_STRUCT * end, OUT uint32_t * out_sec);
/***********************************************************************
 * Function name: bspRtcTimeSet
 * Description: Sets RTC time
 * Parameters : BSP_TIME_STRUCT, BSP_DATE_STRUCT
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeSet(BSP_TIME_STRUCT* time, BSP_DATE_STRUCT* date);
/***********************************************************************
 * Function name: bspRtcTimeGetBCD
 * Description: Reads RTC time and Date in BCD format
 * Parameters : BSP_RTC_TIMESTAMP * bcd
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - on wrong input (*bcd)
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeGetBCD(BSP_RTC_TIMESTAMP * bcd);
/***********************************************************************
 * Function name: bspRtcTimestampToBCD
 * Description: convert BSP_DATE_STRUCT & BSP_TIME_STRUCT to BSP_RTC_TIMESTAMP
 * Parameters : BSP_DATE_STRUCT,  BSP_TIME_STRUCT, BSP_RTC_TIMESTAMP
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimestampToBCD(BSP_DATE_STRUCT	* date, BSP_TIME_STRUCT	* time, OUT BSP_RTC_TIMESTAMP * bcd);
/***********************************************************************
 * Function name: bspRtcTimeSetBCD
 * Description: Sets RTC time using BCD string format
 * Parameters : BSP_RTC_TIMESTAMP
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeSetBCD(BSP_RTC_TIMESTAMP * time);
/***********************************************************************
 * Function name: bspRtcDateSet
 * Description: Sets RTC Date
 * Parameters : BSP_DATE_STRUCT* date
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcDateSet(BSP_DATE_STRUCT* date);
/***********************************************************************
 * Function name: bsp_disable_shutdown_prizma
 * Description: Disable Shut Down Prizma
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_disable_shutdown_prizma(void);
/***********************************************************************
 * Function name: bspCheckWdReset
 * Description: Check if Prizma reseted by watchdog and clear flags
 *				If reseted by WD close device
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspCheckWdReset(void);
	 /***********************************************************************
 * Function name: bspLockOptionBytes
 * Description: Lock option bytes, disable flash reading
 *
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspLockOptionBytes(void);
/***********************************************************************
 * Function name: bsp_flash_unlock
 * Description: Flash unlock
 *
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspFlashUnlock(void);
/***********************************************************************
 * Function name: bspFlashLock
 * Description: Flash unlock
 *
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspFlashLock(void);
/***********************************************************************
 * Function name: bspFlashProgramByte
 * Description: Program flash in form of 1 byte
 *
 * Parameters : address - flash address to write in
 *				data - address of data to write
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspFlashProgramByte(uint32_t address, uint8_t data);
/***********************************************************************
 * Function name: bspFlashProgramDw
 * Description: Program flash in form of double word
 *
 * Parameters : address - flash address to write in
 *				data - address of data to write
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspFlashProgramDw(uint32_t address, uint64_t data);
/***********************************************************************
 * Function name: bspFlashPagesErase
 * Description: Erase pages in flash
 *
 * Parameters : page_num - start page number to erase
 *				num_of_pages - number of pages to delete from start page number
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspFlashPagesErase(uint32_t page_num, uint32_t num_of_pages);



/***********************************************************************
 * Function name: bsp_PM_power_switch
 * Description: power/depower picemaker resources
 * Parameters :
 * mode - 	BSP_POWER_DOWN_MODE
			BSP_POWER_UP_MODE
 *
 * Returns :
 *		BSP_RESULT_OK - switching success
 *		BSP_RESULT_ERROR - switching failed
 *
 ***********************************************************************/
BSP_RESULT_E bspPmPowerSwitch(BSP_POWER_MODE_E mode);


BSP_RESULT_E bspUart2Init(uint32_t baud_rate, uint8_t flow_control);
BSP_RESULT_E  bspUart1Init(void);
BSP_RESULT_E bspUart1Valid(void);

BSP_RESULT_E SystemClock_Config_5Mhz(void);
BSP_RESULT_E SystemClock_Config_40Mhz(void);
BSP_RESULT_E SystemClock_Config_80Mhz(void);
BSP_RESULT_E SystemClock_Config_4Mhz(void);
BSP_RESULT_E SystemClock_Config_16Mhz(void);
BSP_RESULT_E SystemClock_Config_3_5Mhz(void);
BSP_RESULT_E SystemClock_Config_3Mhz(void);
BSP_RESULT_E SystemClock_Config_2Mhz(void);
void SystemClock_Decrease(void);



/***********************************************************************
* Function name: bspImpedanceHWinit
* Description:  configure impedance measure
* Parameters : pwm_period, pwm_pulse
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
BSP_RESULT_E bspImpedanceHWinit(uint32_t pwm_period, uint32_t pwm_pulse);
/***********************************************************************
* Function name: bspImpedancePwmStart
* Description:  start or stop impedance measure PWM timer (sine generator)
* Parameters : 	mode (start /stop)
* 				pwm_val
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
BSP_RESULT_E bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_E mode, uint32_t pwm_val);
/***********************************************************************
* Function name: bspImpedanceSetPwmWidth
* Description:  sets PWM width
* Parameters : 	width
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
BSP_RESULT_E bspImpedanceSetPwmWidth(uint32_t width);
/***********************************************************************
* Function name: bspCompModeSet
* Description:  Turn on or off the comparator
* Parameters : 	mode - ON/OFF
*
* Returns :
*		BSP_RESULT_OK - operation success
*		BSP_RESULT_ERROR_INVALID_ENUM - enum does not exist
***********************************************************************/
BSP_RESULT_E bspCompModeSet(IN BSP_MODE_SET_E mode);
/***********************************************************************
* Function name: bspSdModeSet
* Description:  Turn on or off the SD card
* Parameters : 	mode - ON/OFF
*
* Returns :
*		BSP_RESULT_OK - operation success
*		BSP_RESULT_ERROR_INVALID_ENUM - enum does not exist
***********************************************************************/
BSP_RESULT_E bspSdModeSet(IN BSP_MODE_SET_E mode);
/***********************************************************************
 * Function name: bspReadAdcIt
 * Description: Read ADC Channel
 * Parameters : adc - ADC channel
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *		BSP_RESULT_ERROR_ADC_DOES_NOT_EXIST - ADC not valid
 *
 ***********************************************************************/
BSP_RESULT_E bspReadAdcIt(IN BSP_ADC_E adc);
/***********************************************************************
 * Function name: bspGpioSet
 * Description: set gpio
 *				
 * Parameters : gpio - gpio to set, state - state for gpio
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspGpioSet(char* gpio, char* state);
/***********************************************************************
 * Function name: bspReadComp
 * Description: Read Comparator
 * Parameters : value - value from comparator
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspReadComp(INOUT uint32_t *value);
/***********************************************************************
 * Function name: bspWdInit
 * Description: Init watchdog task
 * Parameters : 
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspWdInit(void);
/***********************************************************************
 * Function name: bspSetWdTime
 * Description: Set Watchdog time
 * Parameters : time - time to set the watchdog
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspSetWdTime(BSP_WD_TIME_E time);

/***********************************************************************
* Function name: bspUartDmaCyclicSerialStart
* Description:  Starts DMA reception for UART in cyclic mode (according to HAL_UART_MspInit config (@ bsp.c))
* Parameters : 	none
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
BSP_RESULT_E bspUartDmaCyclicSerialStart();
void bspUart1Uninit(void);
void bspUart2Uninit(void);
//TODO:added by asaf from generated main.h file
//#define B1_Pin GPIO_PIN_13
//#define B1_GPIO_Port GPIOC
#define USART_TX_Pin GPIO_PIN_2
//#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
//#define USART_RX_GPIO_Port GPIOA
//#define TMS_Pin GPIO_PIN_13
//#define TMS_GPIO_Port GPIOA
//#define TCK_Pin GPIO_PIN_14
//#define TCK_GPIO_Port GPIOA
//#define SWO_Pin GPIO_PIN_3
//#define SWO_GPIO_Port GPIOB

#ifdef G_UNIT_TEST_ENABLE
/***********************************************************************
 * Function name: bsp_set_unit_test
 * Description: sets unit test flag for redirection of data received (SPI complete events for ADS and AFE drivers)
 * Parameters :enable = 0 disable / else = enable
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_set_unit_test(uint8_t enable);
//void bspSd(uint8_t status);

#endif //G_UNIT_TEST_ENABLE

#ifdef TP_ENABLED		//TP1 is TP48(PB8) on the shematics; TP2 is TP49(PC9) on the shematics;
  #define	  TP1_SET		bsp_write_pin(BSP_PIN_TYPE_TP1, BSP_PIN_SET);
  #define	  TP1_RESET		bsp_write_pin(BSP_PIN_TYPE_TP1, BSP_PIN_RESET);
  #define	  TP1_TOOGLE 	bsp_toogle_pin(BSP_PIN_TYPE_TP1);
  #define	  TP2_SET		bsp_write_pin(BSP_PIN_TYPE_TP2, BSP_PIN_SET);
  #define	  TP2_RESET		bsp_write_pin(BSP_PIN_TYPE_TP2, BSP_PIN_RESET);
  #define	  TP2_TOOGLE 	bsp_toogle_pin(BSP_PIN_TYPE_TP2);

#else
  #define	  TP1_SET
  #define	  TP1_RESET
  #define	  TP1_TOOGLE
  #define	  TP2_SET
  #define	  TP2_RESET
  #define	  TP2_TOOGLE
#endif


#endif //__BSP_H__
