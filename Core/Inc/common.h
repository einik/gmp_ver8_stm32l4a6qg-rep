#ifndef __COMMON_H
#define __COMMON_H

#include "global_defs.h"
#include "arm_math.h"  //float32_t
//#include "SEGGER_SYSVIEW.h"


typedef enum {
	RESULT_OK						= 0,
	RESULT_ERROR 					= 1,
	RESULT_ERROR_ALREADY_RUNNING 	= 2,
	RESULT_ERROR_DEVICE_CHARGING 	= 3,
	RESULT_BAD_COMMAND 				= 4,
	RESULT_WRONG_PARAMTER			= 5
}RESULT_E;

typedef struct {
	uint8_t		*ptr;
	uint32_t	length;
}byte_array_s;

typedef struct {
	float32_t	*ptr;
	uint32_t	length;
}float_array_s;

typedef union {
	float32_t 	f;
	int32_t		i;
	uint8_t		ch;
    uint16_t     sh;
	byte_array_s	byte_array;
	float_array_s	float_array;
}data_u;

typedef struct VERSION_STRUCT{
	uint8_t major;
	uint8_t minor;
	uint8_t patch;
	uint8_t build;
}VERSION_STRUCT;

typedef void (* raw_data_send_cb)(const uint8_t * data_ptr, const uint32_t length);

#define null '\0'	// null pointer

#define BOOL _Bool
#define TRUE  1
#define FALSE 0

//#define PRINT_STACK_WATER_MARK
#define MAIN_STACK_SIZE					200	//1000//700
#define ECG_STACK_SIZE					900//1200  //1400
#define BLE_STACK_SIZE					200//900
#define BAT_MON_STACK_SIZE				200//400
#define DEVICE_STACK_SIZE				1200//900
#define SERIAL_PROTOCOL_STACK_SIZE		800
#define PBS_STACK_SIZE					1200//1700
#define PBS_TX_STACK_SIZE				750	//1000
#define SD_STACK_SIZE					400
#define MENNEN_ALGO_STACK_SIZE			1500//1000
#define IMPEDANCE_STACK_SIZE			500
#define ACCEL_STACK_SIZE				2500
#define WD_STACK_SIZE					100

//#define MENNEN_ALGO
//#define MENNEN_LIB

#define IWDG_ENABLE	// independent watchdog
#define SERIAL_PROTOCOL_FAST
//#define SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
//#define G_UNIT_TEST_ENABLE
//#define ECG_ONLY
//#define PBS
//#define ACCEL_ENABLED
//#define GMP_DEMO_CHINA
//#define VSMS_EXTENDED_HOLTER_MOCKUP
//#define SEGGER_SYSTEM_VIEW

#ifdef VSMS_EXTENDED_HOLTER_MOCKUP
#define BLE_ADVERTISEMENT_ON
#endif //VSMS_EXTENDED_HOLTER_MOCKUP

#ifdef MENNEN_ALGO
#undef IWDG_ENABLE
#undef SERIAL_PROTOCOL_FAST
#endif // MENNEN_ALGO

//Min and max
#ifndef max
#define max(A, B)   ((A) > (B) ? (A) : (B))
#endif
#ifndef min
#define min(A, B)   ((A) < (B) ? (A) : (B))
#endif


#define IN
#define OUT
#define INOUT


#endif //__COMMON_H
