#ifndef __BLE_H__
#define __BLE_H__

//#include "common.h"
#include "cmsis_os.h"


#define BLE_MAX_MSG_SIZE								300
#define BLE_VERSION_SIZE								4
#define BLE_BLUETOOTH_ADDRESS							6
#define BLE_RX_MAX_BUFFER								150
#define BLE_RX_NUM_OF_BUFFERS							2
		
#define BLE_MSG_HEADER									0xAA
#define BLE_CONNECTION_HANDLE_DEFAULT					0x80
		
#define BLE_PEER_ADDRESS_LENGTH							6
#define BLE_DEVICE_NAME_MAX_LENGTH						8
#define BLE_DEVICE_MAC_MAX_LENGTH						12

#define BLE_COMMON_COMMAND_REPLY_SUCCESS				0
#define BLE_COMMON_COMMAND_REPLY_OPCODE_LOCATION		4
#define BLE_COMMON_COMMAND_REPLY_DATA_LOCATION			5

#define PBS_QUEUE_SIZE									5//10
#define PBS_MSG_SIZE									70


#define BLE_TX_BYTE_PER_MSEC (3.6)

#define BLE_TX_MAX_SEND_TIME 1000
#define BLE_TX_MIN_SEND_TIME 10
#define BLE_TX_DEFAULT_SEND_TIME 250


typedef enum{
	BLE_RESULT_OK,
	BLE_RESULT_ERROR_ALREADY_INITIALIZED,
	BLE_RESULT_ERROR_ALREADY_RUNNING,
	BLE_RESULT_ERROR_NOT_RUNNING,
	BLE_RESULT_ERROR_ADS_DRIVER,
	BLE_RESULT_ERROR_OS,
	BLE_RESULT_ERROR_QUEUE,
	BLE_RESULT_ERROR_MUTEX,
	BLE_RESULT_ERROR,
	BLE_RESULT_ERROR_MSG_TOO_SHORT,
	BLE_RESULT_ERROR_CRC_ERROR,
	BLE_RESULT_ERROR_WRONG_PARAM,
	BLE_RESULT_ERROR_DEVICE_NAME_LENGTH,
	BLE_RESULT_ERROR_AT_COMMAND_FAILED
}BLE_RESULT;

typedef enum{
	BLE_MODE_NORMAL,
	BLE_MODE_AT_COMMAND	
}BLE_MODE_E;

typedef enum{
	BLE_POWER_ON,
	BLE_POWER_OFF	
}BLE_POWER_E;

typedef enum{
	BLE_ADV_ON,
	BLE_ADV_OFF	
}BLE_ADV_E;


#ifdef G_UNIT_TEST_ENABLE
//TEST FUNCTIONS
void ble_test(uint8_t test_num);
void ble_test_inc_read(uint8_t* ptr_data, uint16_t size);
void ble_test_inc(void);
#endif //G_UNIT_TEST_ENABLE
BLE_RESULT bleInit(void);
BLE_RESULT bleGetHighWaterMark(UBaseType_t * ret_val);
BLE_RESULT bleSendMsg(uint8_t *ptr, uint16_t length);
//BLE_RESULT ble_snd_get_status(void);
//BLE_RESULT ble_snd_conn_interval(uint16_t con_int, uint16_t con_latency, uint16_t sup_timeout);
BLE_RESULT bleSendTransparentMsg(uint8_t* ptr_data, uint16_t length);
BLE_RESULT bleAtCommand(uint8_t* data1, uint8_t* data2);
BLE_RESULT bleAtCommandMode(BLE_MODE_E mode);

#pragma pack(1)
typedef struct{
	uint8_t 					header;
	uint8_t						length[2];
	uint8_t 					opcode;
	uint8_t						msg_data[BLE_MAX_MSG_SIZE];
}BLE_MSG_STRUCT;

typedef struct{
	uint8_t 					header;
	uint8_t						length[2];
	uint8_t 					opcode;
	uint8_t 					handle;
	uint8_t						msg_data[BLE_MAX_MSG_SIZE];
}BLE_MSG_HANDLE_STRUCT;

//typedef struct{
//	uint16_t									ble_ver;
//	char								 		ble_config_device_name[BLE_DEVICE_NAME_MAX_LENGTH + 1];
//	BLE_BAUD_RATE_E								baud_rate;
//}BLE_CONFIG_STRUCT;

typedef enum{
	BLE_EVENT_GMP_CONNECTED 		= 0,
	BLE_EVENT_GMP_DISCONNECTED 		= 1,
	BLE_EVENT_BLE_BIT_FAILED 		= 2
}BLE_EVENT_E;

typedef enum{
	BLE_CONFIG_STATUS_INIT, 			//Init state
	BLE_CONFIG_STATUS_WORKING_ADV_OFF,	//Initialization OK, advertisement off
	BLE_CONFIG_STATUS_WORKING_ADV_ON	//Initialization OK, advertisement on
}BLE_CONFIG_STATUS;

typedef struct{
	uint8_t msg_length;
	uint8_t	pbs_data[PBS_MSG_SIZE];
}PBS_QUEUE_MSG_STRUCT;

BLE_RESULT bleDeviceNameSet(char* name);
BLE_RESULT bleConfigStatusGet(BLE_CONFIG_STATUS* status);
BLE_RESULT blePowerSet(BLE_POWER_E power);
BLE_RESULT bleDeviceNameGet(char* name);
BLE_RESULT bleDeviceMacGet(char* mac);
BLE_RESULT bleReset(void);
BLE_RESULT bleCalculateSendTime(IN uint16_t msg_lengh, OUT uint16_t * out_msec);
void bleTest(uint8_t test_num);
BLE_RESULT bleStartConfig(void);
BLE_RESULT bleEnableBleAdv(void);
BLE_RESULT bleAdvMode(BLE_ADV_E mode);
#endif  //__BLE_H__
