#ifndef __VERSIONS_H__
#define __VERSIONS_H__

// Firmware versions control (8 bit each field)

#ifdef __RELEASE
#define FW_MAJOR_VERSION        	2
#else
#define FW_MAJOR_VERSION        	255
#endif //__RELEASE
#define FW_MINOR_VERSION        	0
#define FW_PATCH_VERSION        	2
#define FW_BUILD_VERSION			13


//Bootloader FW
#define BL_FW_MAJOR_VERSION        	3
#define BL_FW_MINOR_VERSION        	0
#define BL_FW_PATCH_VERSION        	0
#define BL_FW_BUILD_VERSION			4

#endif //__VERSIONS_H__
