
#ifndef _RESP_MMI_API_
#define _RESP_MMI_API_

#define RESP_NUM_SAMPLES  80
#define RESP_NUM_STAT_PRM  1 
#define RESP_NUM_CONF_PRM  6
#define RESP_NUM_RSLT_PRM  3
#define RESP_NUM_VER_BYTES 4

//typedef struct RESP_WFRM {
//   short samples[RESP_NUM_SAMPLES];
//}RESP_WFRM;


typedef struct RESP_STAT {
//   unsigned short param[RESP_NUM_STAT_PRM];
   short stat;
}RESP_STAT;

typedef struct RESP_CONF {
//   short param[RESP_NUM_CONF_PRM];
   short notch_mode;
   short filter_mode;
   short gain_code;
   short gain_mul;
   short gain_div;
   short apnea_limit;
}RESP_CONF;

typedef struct RESP_RSLT {
//   short param[RESP_NUM_RSLT_PRM];
   short breath_rate;
   short apnea_durat;
   short coinc_flag;
}RESP_RSLT;

typedef struct RESP_VERS {
   unsigned char ver[RESP_NUM_VER_BYTES];
}RESP_VERS;

enum {
   RESP_STAT_INIT,
   RESP_STAT_APNEA,
   RESP_STAT_NORM
};

enum {
   RESP_NOTCH_NONE = 0,
   RESP_NOTCH_50HZ,
   RESP_NOTCH_60HZ
};

enum resp_filt_mode {
   RESP_FILT_NONE = 0, 
   RESP_FILT_01_4HZ,  //ADULT, 
   RESP_FILT_03_4HZ, //NEONAT,	
};

enum resp_gain_code{
   RESP_GAIN_AUTO = 0,
   RESP_GAIN_FRACT,
   RESP_GAIN_1_8,
   RESP_GAIN_1_4,
   RESP_GAIN_1_2,
   RESP_GAIN_1,
   RESP_GAIN_2,
   RESP_GAIN_4,
   RESP_GAIN_8
};

enum resp_apn_limit{
   RESP_APN_NONE   =0,
   RESP_APN_10SEC,
   RESP_APN_15SEC,
   RESP_APN_20SEC,
   RESP_APN_30SEC,
   RESP_APN_45SEC,
   RESP_APN_60SEC,
   RESP_APN_90SEC
};


void  Resp_initProc(void);
void  Resp_resetProc(void);
short Resp_procSample(long *pSample, int lead_off);

void Resp_setFilter(int filter);
void Resp_setNotch (int notch);
void Resp_setGain  (int gain);
void Resp_setFractGain(char mul, char div);

//int  Resp_downSample_500_320(short Samples[], int n_samp, short outSamples[]);
int  Resp_downSample_500_50(short Samples[], int n_samp, short outSamples[]);
void Resp_applyGain(short samples[], int n_samp);

void Resp_initCalc (void); 
void Resp_resetCalc(void);
int  Resp_execCalc (short samples[],int n_samp);
//int  Resp_execCalc_320sps (short samples[],int n_samp);

int  Resp_getBreathRate(short *out_buf); //(RESP_RATE *p)
int  Resp_getApneaTime(short *out_buf); //(RESP_APNEA *p)

void Resp_setApneaLimit(int limit_code);

void Resp_getStat (RESP_STAT *pStat);
void Resp_getConf (RESP_CONF *pConf);
int  Resp_getVers (char *out_str);

void Resp_initSim(int resp_rate, int noise_freq, int offset);
long Resp_execSim(long mul, long div);

int  Resp_ECG_coincidence(short resp_rate, short heart_rate);


#endif //_RESP_MMI_API_