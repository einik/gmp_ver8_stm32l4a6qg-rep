#ifndef __GLOBAL_DEFS_H__
#define __GLOBAL_DEFS_H__

//#define ECG_ADS_TEST_SIGNAL_1_HZ        // ADS Internal 1 Hz Square 1 Hz
//#define ARM_MATH_CM4

//#define RESULT_DB_DISABLE

#define G_ECG_SAMPLING_RATE 			250
#define G_ECG_NUMBER_OF_SAMPLES 		500
#define G_UART_BAUD_RATE 				230400//460800 //460800 //230400;//921600;//230400;//115200;
#define G_UART_BAUD_RATE_BLE			230400//115200//230400;//921600;//230400;//921600;
#define G_UART_BAUD_RATE_AT_COMMAND_BLE	57600
#define G_UART_BAUD_RATE_MENNEN			115200

//#define G_UNIT_TEST_ENABLE

//Emulations
//#define G_ECG_EMULATE_INTERRUPTS //to emulate ADS interrupts
//#define G_ECG_EMULATE_DATA //to emulate ADS data
//#define G_ECG_MAX_TEST_BUF_SIZE 10000




#endif  //__GLOBAL_DEFS_H__
