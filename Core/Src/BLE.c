#include "cmsis_os.h"
#include "common.h"

#include "serial_protocol.h"
//#include "BSP.h"
#include "BLE.h"
#include "BMD350.h"

/****************************************************************/
/*						Local Definitions	 					*/
#define BLE_TIMER_PERIOD						15//20
#define BLE_AT_CMD_TIMER_PERIOD					3000
#define BLE_AT_CMD_DELAY						2200
//#define AT_COMMAND_MAX_LENGTH					30
//#define AT_COMMAND_CR							0x0D
//#define AT_COMMAND_SPACE						0x20
#define BLE_AT_COMMAND_NUM_OF_RETRIES			5

#define BLE_BAUD_RATE_DEFAULT					"230400"
#define BLE_FLOW_CONTROL_DEFAULT				"1"
#define BLE_PARITY_ENABLE_DEFAULT				"0"
#ifdef BLE_ADVERTISEMENT_ON
#define BLE_PASS_THROUGH_DEFAULT				"1"
#else
#define BLE_PASS_THROUGH_DEFAULT				"0"
#endif //BLE_ADVERTISEMENT_ON
#define BLE_DEVICE_NAME_DEFAULT					"GPR"
#define BLE_CONFIGURE_STATUS_PIN				"4"
#define BLE_CONFIGURE_STATUS_PIN_POLARITY		"1"
#define BLE_RESPONSE_OK							"OK\n"
#define BLE_RESPONSE_ERROR						"ERR"
#define BLE_AT_COMMAND_MAX_RESPONSE				BLE_DEVICE_MAC_MAX_LENGTH
#define BLE_AT_COMMAND_MAX_RETRIES				3

#define BLE_CONN_INT 							(18.75)
#define BLE_BYTES_PER_2_TX_PACKETS 				(27)
#define BLE_TX_PER_CONN_INT 					(4)
#define BLE_BYTES_PER_CONN_INT 					(BLE_BYTES_PER_2_TX_PACKETS * BLE_TX_PER_CONN_INT / 2)	// Rigado sends 20 / 7 so each couple tx's are 27

#define BLE_PREPARE_AT_MSG(prev, current, ptr) 	\
{                                               \
	AT_COMMAND(current, ptr)              		\
	ble_last_at_com_sent = current;       		\
	ble_prev_at_com_sent = prev;     			\
}

#define BLE_ADVE_OFF							(ble_config_status == BLE_CONFIG_STATUS_WORKING_ADV_OFF)
#define BLE_ADVE_ON								(ble_config_status == BLE_CONFIG_STATUS_WORKING_ADV_ON)

typedef struct{
	uint8_t* 	ptr;
	uint16_t	length;
}BLE_TX_QUEUE_MSG_STRUCT;

/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/
void bleRxTask(void const * argument);
static BLE_RESULT prvBleParseAtCommands(uint8_t* data);
/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
static uint8_t 							bleRxBuffer[BLE_RX_MAX_BUFFER];
static osTimerId 						ble_timer;
static osTimerId 						ble_at_cmd_timer;
osThreadId 								bleRxTaskHandle;
osMessageQId 							bleRxQueueHandle;
static char								ble_device_name[BLE_DEVICE_NAME_MAX_LENGTH + 1] = {0};
static char								ble_device_mac[BLE_DEVICE_MAC_MAX_LENGTH + 1] = {0};
static BLE_CONFIG_STATUS				ble_config_status = BLE_CONFIG_STATUS_INIT;
static BMD_AT_COMMANDS_E				ble_last_at_com_sent = BMD_AT_COM_MAX;
static BMD_AT_COMMANDS_E				ble_prev_at_com_sent = BMD_AT_COM_MAX;
#ifdef G_UNIT_TEST_ENABLE
static uint8_t 							test_10_read[155];
static uint32_t 						ble_msg_counter = 0;
static uint32_t 						ble_msg_failed_counter = 0;
#endif //G_UNIT_TEST_ENABLE
static PBS_QUEUE_MSG_STRUCT				pbs_msg[PBS_QUEUE_SIZE];
static uint8_t							pbs_msg_index;
static uint8_t							ble_at_com_retries = 0;
static uint8_t							ble_return_value[BLE_AT_COMMAND_MAX_RESPONSE] = {0};
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/
extern osMessageQId 					pbs_rx_queue_handle;
/****************************************************************/

__weak void ble_event(BLE_EVENT_E event)
{

}

BLE_RESULT bleInit(void)
{  
	//init queue
	osMessageQDef(bleRxQueue, 10, uint8_t );
	
	bleRxQueueHandle = osMessageCreate(osMessageQ(bleRxQueue), NULL);
	if (!bleRxQueueHandle)
		return BLE_RESULT_ERROR;

	//Init task
	osThreadDef(bleRxTask, bleRxTask, osPriorityNormal, 0, BLE_STACK_SIZE);
	bleRxTaskHandle = osThreadCreate(osThread(bleRxTask), NULL);
	if (!bleRxTaskHandle)
		return BLE_RESULT_ERROR;
	
	bmdInit();
	
	ble_config_status = BLE_CONFIG_STATUS_INIT;
	return BLE_RESULT_OK;
}

BLE_RESULT bleGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (bleRxTaskHandle == NULL) || (ret_val == NULL) )
		return BLE_RESULT_ERROR_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark( bleRxTaskHandle );
	return BLE_RESULT_OK;
}

BLE_RESULT bleSendMsg(uint8_t *ptr, uint16_t length)
{

	if (bmdSendDataToBle(ptr, length) == BMD_RESULT_OK)
	{
		LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_4, "BLE TX: ");
		LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_HEX, (char*)ptr, length);
		LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_4, "\r\n");
	//	DEBUG_PRINT(DEBUG_LEVEL_8, "\r\n");
	}
	else
	{
		LOG_ERR("BLE TX failed\r\n");
		return BLE_RESULT_ERROR;
	}
	
	return BLE_RESULT_OK;
		
}

void bmdRxCptCallback(void)
{
	bmdGetDataFromBle(&bleRxBuffer[1], BLE_RX_MAX_BUFFER - 1);	
	if (osMessagePut (bleRxQueueHandle, 0xFF, 0) != osOK)
		LOG_ERR("bmdRxCptCallback\r\n");
	
}

void bleTimerCallback(void const *n)
{

	uint16_t length = 0;
	
//	bsp_uart_stop_dma(BSP_UART_BLE);
	bmdStopReadingDataFromBle();
	
	bmdGetDataLength(&length);
//	bspUartDmaDataLeft(BSP_UART_BLE, &length);
	length = BLE_RX_MAX_BUFFER - length;
	if ((length > 0) && ((length <= BLE_RX_MAX_BUFFER)))
		if (osMessagePut (bleRxQueueHandle, length, 0) != osOK)
			LOG_ERR("bleRxQueueHandle failed\r\n");
	
}

void bleAtComTimerCallback(void const *n)
{
	ble_event(BLE_EVENT_BLE_BIT_FAILED);
}

BLE_RESULT bleReset(void)
{
		
	BMD_RESULT_E res = bmdReset();
	return (res == BMD_RESULT_OK ? BLE_RESULT_OK : BLE_RESULT_ERROR);
}

BLE_RESULT bleAtCommandMode(BLE_MODE_E mode)
{
	
	if (mode == BLE_MODE_AT_COMMAND)
	{
		bmdAtCommandMode(BMD_MODE_AT_COMMAND);
	}
	else if (mode == BLE_MODE_NORMAL)
	{
		bmdAtCommandMode(BMD_MODE_NORMAL);	
	}
	else
		return BLE_RESULT_ERROR_WRONG_PARAM;
	
	osDelay(50);
	
	bleReset();
	
//	bsp_uart_receive_dma(BSP_UART_BLE, bleRxBuffer, 1);
	bmdGetDataFromBle(bleRxBuffer, 1);
	
	
	return BLE_RESULT_OK;
}

BLE_RESULT bleAtCommand(uint8_t* data1, uint8_t* data2)
{
	
	BMD_RESULT_E res = bmdAtCommandSend(data1, data2);
	
		
	return (res == BMD_RESULT_OK ? BLE_RESULT_OK : BLE_RESULT_ERROR);
}

osTimerDef(ble_timer, bleTimerCallback);
osTimerDef(ble_at_cmd_timer, bleAtComTimerCallback);

void bleRxTask(void const * argument)
{
	osEvent  						ret;
	BMD_MODE_E		 				bmd_mode;
	osStatus						os_ret;
	
	bmdBleCommSet(BMD_COMM_MODE_ENABLE);

	ble_at_cmd_timer = osTimerCreate(osTimer(ble_at_cmd_timer), osTimerOnce, ( void * ) 0);
	bleStartConfig();
	
	ble_timer = osTimerCreate(osTimer(ble_timer), osTimerOnce, ( void * ) 0);
	
	while(1)
	{
		ret =  osMessageGet (bleRxQueueHandle, osWaitForever );

		if (ret.value.v == 0xFF)
		{
			if (osTimerStart(ble_timer, BLE_TIMER_PERIOD) != osOK)
				LOG("ble_timer start failed\r\n");
//			timer_flag = TRUE;
		}
		else
		{
			LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_4, "BLE RX: ");
			LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_HEX, (char*)bleRxBuffer, ret.value.v);
			LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_4, "\r\n");
			
			bmdModeGet(&bmd_mode);
			if (bmd_mode == BMD_MODE_AT_COMMAND)
			{//AT command enabled
				bleRxBuffer[ret.value.v] = 0;
				LOG("AT: %s\r\n", bleRxBuffer);
				//PRINT(""(char*)bleRxBuffer, ret.value.v);
				LOG("\r\n");
				if (ble_config_status == BLE_CONFIG_STATUS_INIT)
					prvBleParseAtCommands(bleRxBuffer);
				else
					strncpy(ble_return_value, bleRxBuffer, sizeof(ble_return_value));
			}
			else
			{
				if (ret.value.v > sizeof(pbs_msg[pbs_msg_index].pbs_data))
					LOG("BLE message too long: %d\r\n", ret.value.v);
				else
				{//Send message to PBS parser
					pbs_msg[pbs_msg_index].msg_length = ret.value.v;
					memcpy(pbs_msg[pbs_msg_index].pbs_data, bleRxBuffer, sizeof(pbs_msg[pbs_msg_index].pbs_data));
					osMessagePut (pbs_rx_queue_handle, (uint32_t)&pbs_msg[pbs_msg_index], 0);//PBS
					pbs_msg_index = (pbs_msg_index + 1) % PBS_QUEUE_SIZE;
				}
			}
			bmdGetDataFromBle(bleRxBuffer, 1);

		}
//		bsp_uart_receive_dma(BSP_UART_BLE, bleRxBuffer, 1);
		
	}
}

BLE_RESULT bleStartConfig(void)
{
	char* ptr_at_command;
	
	ble_config_status = BLE_CONFIG_STATUS_INIT;
	
	bleAtCommandMode(BLE_MODE_AT_COMMAND);
	osDelay(BLE_AT_CMD_DELAY);
	ble_at_com_retries = 0;

	BLE_PREPARE_AT_MSG(BMD_AT_COM_NONE, BMD_AT_COM_CONNECTABLE_ADV_DISABLE, ptr_at_command)

	bmdAtCommandSend((uint8_t*)ptr_at_command, "");
	
	osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
	
	return BLE_RESULT_OK;
}

static BLE_RESULT prvBleParseAtCommands(uint8_t* data) 
{
	char* ptr_at_command;
	
	osTimerStop(ble_at_cmd_timer);
	
	if (strncmp((char*)data, BLE_RESPONSE_ERROR, 3) == 0)
	{//If received "ERR" from BLE module
		if (ble_at_com_retries >= BLE_AT_COMMAND_NUM_OF_RETRIES)
		{
			ble_event(BLE_EVENT_BLE_BIT_FAILED);
			return BLE_RESULT_ERROR;
		}
		ble_at_com_retries++;
		LOG_ERR("AT command %d failed, retry number: %d \r\n", ble_last_at_com_sent, ble_at_com_retries);
		ble_last_at_com_sent = ble_prev_at_com_sent;
	}
	
	switch (ble_last_at_com_sent)
	{
		case BMD_AT_COM_NONE:
		{//For retry
			BLE_PREPARE_AT_MSG(BMD_AT_COM_NONE, BMD_AT_COM_CONNECTABLE_ADV_DISABLE, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, "");	
		}
		break;

//		case BMD_AT_COM_RESET:
//		{
//			osDelay(3000);
//			BLE_PREPARE_AT_MSG(BMD_AT_COM_NONE, BMD_AT_COM_CONNECTABLE_ADV_DISABLE, ptr_at_command)
//			bmdAtCommandSend((uint8_t*)ptr_at_command, "");
//		}
//		break;		
		
		case BMD_AT_COM_CONNECTABLE_ADV_DISABLE:
		{
			BLE_PREPARE_AT_MSG(BMD_AT_COM_CONNECTABLE_ADV_DISABLE, BMD_AT_COM_RESET, ptr_at_command)				
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, "");
			
		}
		break;
		
		case BMD_AT_COM_RESET:
		{//BLE reset
			osDelay(900);
			BLE_PREPARE_AT_MSG(BMD_AT_COM_RESET, BMD_AT_COM_BAUD_RATE, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, BLE_BAUD_RATE_DEFAULT);
		}
		break;
		
		case BMD_AT_COM_BAUD_RATE:
		{//Flow control
			BLE_PREPARE_AT_MSG(BMD_AT_COM_BAUD_RATE, BMD_AT_COM_FLOW_CONTROL_ENABLE, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, BLE_FLOW_CONTROL_DEFAULT);
		}
		break;

		case BMD_AT_COM_FLOW_CONTROL_ENABLE:
		{//Parity enable
			BLE_PREPARE_AT_MSG(BMD_AT_COM_FLOW_CONTROL_ENABLE, BMD_AT_COM_PARITY_ENABLE, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, BLE_PARITY_ENABLE_DEFAULT);			
		}
		break;

		case BMD_AT_COM_PARITY_ENABLE:
		{//UART pass-through
			BLE_PREPARE_AT_MSG(BMD_AT_COM_PARITY_ENABLE, BMD_AT_COM_PASS_THROUGH, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, BLE_PASS_THROUGH_DEFAULT);			
		}
		break;		

		case BMD_AT_COM_PASS_THROUGH:
		{//Get device name
			BLE_PREPARE_AT_MSG(BMD_AT_COM_PASS_THROUGH, BMD_AT_COM_READ_DEVICE_NAME, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, "");
		}
		break;
		
		case BMD_AT_COM_READ_DEVICE_NAME:
		{//Get MAC address
			if (strlen((char*)data) <= BLE_DEVICE_NAME_MAX_LENGTH)
				strncpy(ble_device_name, (char*)data, strlen((char*)data) - 1);
			BLE_PREPARE_AT_MSG(BMD_AT_COM_READ_DEVICE_NAME, BMD_AT_COM_GET_MAC_ADDRESS, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, "");
		}
		break;		
		
		case BMD_AT_COM_GET_MAC_ADDRESS:
		{//Configure status pin
			for(uint8_t i = 0, j = 0;i < BLE_DEVICE_MAC_MAX_LENGTH;i = i + 2, j = j + 3)
				strncpy(&ble_device_mac[i], (char*)&data[j], 2);
			BLE_PREPARE_AT_MSG(BMD_AT_COM_GET_MAC_ADDRESS, BMD_AT_COM_CONFIGURE_STATUS_PIN, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, BLE_CONFIGURE_STATUS_PIN_POLARITY);
		}
		break;
#ifdef BLE_ADVERTISEMENT_ON
		case BMD_AT_COM_CONFIGURE_STATUS_PIN:		
		{//Enable advetisement
//			osDelay(2000);
			BLE_PREPARE_AT_MSG(BMD_AT_COM_NONE, BMD_AT_COM_CONNECTABLE_ADV_ENABLE, ptr_at_command)
			osTimerStart(ble_at_cmd_timer, BLE_AT_CMD_TIMER_PERIOD);
			bmdAtCommandSend((uint8_t*)ptr_at_command, "");	
		}
		break;		

		case BMD_AT_COM_CONNECTABLE_ADV_ENABLE:
		{//Finish
			bleAtCommandMode(BLE_MODE_NORMAL);
			ble_last_at_com_sent = BMD_AT_COM_MAX;
			ble_config_status = BLE_CONFIG_STATUS_WORKING_ADV_OFF;
		}
		break;
#else
		case BMD_AT_COM_CONFIGURE_STATUS_PIN:
		{//Finish
			bleAtCommandMode(BLE_MODE_NORMAL);
			ble_last_at_com_sent = BMD_AT_COM_MAX;
			ble_config_status = BLE_CONFIG_STATUS_WORKING_ADV_OFF;
		}
		break;		
#endif //BLE_ADVERTISEMENT_ON
		default:
		{
			LOG_ERR("wrong at command ble_last_at_com_sent: %d\r\n", ble_last_at_com_sent);
			bleAtCommandMode(BLE_MODE_NORMAL);
			ble_last_at_com_sent = BMD_AT_COM_MAX;
			
			ble_config_status = BLE_CONFIG_STATUS_WORKING_ADV_OFF;
		}
		break;
	}
	return BLE_RESULT_OK;
}

BLE_RESULT bleEnableBleAdv(void)
{
	char* ptr_at_command;
	
	BLE_PREPARE_AT_MSG(BMD_AT_COM_NONE, BMD_AT_COM_CONNECTABLE_ADV_ENABLE, ptr_at_command)
	bmdAtCommandSend((uint8_t*)ptr_at_command, "");	
	
	return BLE_RESULT_OK;
}

BLE_RESULT bleDeviceNameSet(char* name)
{
	uint8_t length;
	
	length = strnlen(name, BLE_DEVICE_NAME_MAX_LENGTH);
	
	if  (length > 0 && length <= BLE_DEVICE_NAME_MAX_LENGTH)
	{
		memset(ble_device_name, 0, sizeof(ble_device_name));
		strcpy(ble_device_name, name);
		LOG("Device name saved, Write configuration\r\n");
		return BLE_RESULT_OK;
	}
	else
		return BLE_RESULT_ERROR_DEVICE_NAME_LENGTH;
	
}

BLE_RESULT bleDeviceNameGet(char* name)
{

	strcpy(name, ble_device_name);
	
	return BLE_RESULT_OK;

}

BLE_RESULT bleDeviceMacGet(char* mac)
{

	strcpy(mac, ble_device_mac);
	
	return BLE_RESULT_OK;

}

BLE_RESULT bleConfigStatusGet(BLE_CONFIG_STATUS* status)
{
	*status = ble_config_status;
	
	return BLE_RESULT_OK;
}

BLE_RESULT blePowerSet(BLE_POWER_E power)
{
	BMD_RESULT_E ret = BMD_RESULT_ERROR;
	
	if (power == BLE_POWER_OFF)
		ret = bmdPowerSet(BMD_POWER_OFF);
	else if (power == BLE_POWER_ON)
		ret = bmdPowerSet(BMD_POWER_ON);

	return (ret == BMD_RESULT_OK ? 	BLE_RESULT_OK : BLE_RESULT_ERROR);
	
}

BLE_RESULT prvBleAtCommandSendWithAck(uint8_t* str1, uint8_t* str2, char* str_resp)
{
	uint8_t 	retries = 0;
	
	do{
		if (bmdAtCommandSend(str1, str2) != BMD_RESULT_OK)
			return BLE_RESULT_ERROR_AT_COMMAND_FAILED;
		osDelay(500);
		if (strcmp((char*)ble_return_value, str_resp) == 0)
		{
			memset(ble_return_value, 0, sizeof(ble_return_value));
			return BLE_RESULT_OK;
		}
	}while(retries++ < BLE_AT_COMMAND_MAX_RETRIES);	
	
	return BLE_RESULT_ERROR_AT_COMMAND_FAILED;
	
}

BLE_RESULT bleAdvMode(BLE_ADV_E mode)
{
	
	uint8_t		ok[] = BLE_RESPONSE_OK;
	uint8_t		str2[2] = {0x30, 0x00};
	
	if (mode == BLE_ADV_ON)
	{
		if BLE_ADVE_ON
			return 	BLE_RESULT_OK;
		str2[0] = 0x31;
	}
	else if (mode == BLE_ADV_OFF)
	{
		if BLE_ADVE_OFF
			return BLE_RESULT_OK;
		str2[0] = 0x30;
	}
	else
		return BLE_RESULT_ERROR_WRONG_PARAM;	
	
	//Set AT Command mode
	if (bleAtCommandMode(BLE_MODE_AT_COMMAND) != BLE_RESULT_OK)
		return BLE_RESULT_ERROR_AT_COMMAND_FAILED;
	osDelay(BLE_AT_CMD_DELAY);
	
	if (prvBleAtCommandSendWithAck(BMD_AT_COMMAND_PASS_THROUGH, str2, ok) != BLE_RESULT_OK)
		return BLE_RESULT_ERROR_AT_COMMAND_FAILED;
	
	if (prvBleAtCommandSendWithAck(BMD_AT_COMMAND_CONNECTABLE_ADV, str2, ok) != BLE_RESULT_OK)
		return BLE_RESULT_ERROR_AT_COMMAND_FAILED;		
	
	//Close AT Command mode
	if (bleAtCommandMode(BLE_MODE_NORMAL) != BLE_RESULT_OK)
		return BLE_RESULT_ERROR_AT_COMMAND_FAILED;
		
	if (mode == BLE_ADV_ON)
		ble_config_status = BLE_CONFIG_STATUS_WORKING_ADV_ON;
	else
		ble_config_status = BLE_CONFIG_STATUS_WORKING_ADV_OFF;
	
	return BLE_RESULT_OK;
}
//void bspBleStatus(uint8_t ble_status)
//{
//	
//	if (ble_status)
//		ble_event(BLE_EVENT_GMP_CONNECTED);
//	else
//		ble_event(BLE_EVENT_GMP_DISCONNECTED);
//}

void bmdEvent(BMD_EVENT_E event)
{
	if (event == BMD_EVENT_BLE_CONNECTED)
		ble_event(BLE_EVENT_GMP_CONNECTED);
	else
		ble_event(BLE_EVENT_GMP_DISCONNECTED);
}

BLE_RESULT bleCalculateSendTime(IN uint16_t msg_lengh, OUT uint16_t * out_msec)
{
	if ( (out_msec == NULL) || (msg_lengh == 0) )
		return BLE_RESULT_ERROR;

	* out_msec = (uint16_t)( 50 + ( (msg_lengh / BLE_BYTES_PER_CONN_INT) + 1) * BLE_CONN_INT );	// +1 for round up

	if (*out_msec > BLE_TX_MAX_SEND_TIME)
		*out_msec = BLE_TX_MAX_SEND_TIME;

	if (*out_msec < BLE_TX_MIN_SEND_TIME)
		*out_msec = BLE_TX_MIN_SEND_TIME;

	return BLE_RESULT_OK;
}

void bleTest(uint8_t test_num)
{
//	static uint8_t test_10_1[] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x00, 0x11, 0x22, 0x33, 0x44, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44};	
	static uint8_t test_10_2[] = {
	0xAA, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
	0x00, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 
	0x11, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
	0x22, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 
	0x33, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
	0x44, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30,
	0x55, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
	0x66, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 
	0x77, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
	0x88, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30,

	0x99, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
	0xBB, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0xFF 
//	0x41, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49
//	0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 
//	0x41, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
//	0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30,
//	0x41, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
//	0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30
//	0x41, 0x42, 0x43, 0x44, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 
//	0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30		
	};	
	uint16_t i = 0;
	
	
	switch (test_num)
	{
		case 1:
		{
			bleSendMsg(test_10_2, sizeof(test_10_2));
		}
		break;
		
		case 2:
		{
			for (i = 0;i < 1000;i++)
			{
				osDelay(100);
				bleSendMsg(test_10_2, sizeof(test_10_2));
			}
		}
		break;		

		case 3:
		{
			for (i = 0;i < 10000;i++)
			{
				osDelay(50);
				bleSendMsg(test_10_2, sizeof(test_10_2));
			}
		}
		break;		

		case 4:
		{
			for (i = 0;i < 10000;i++)
			{
				osDelay(30);
				bleSendMsg(test_10_2, sizeof(test_10_2));
			}
		}
		break;		

		case 5:
		{
			for (i = 0;i < 10;i++)
			{
				osDelay(30);
				bleSendMsg(test_10_2, sizeof(test_10_2));
			}
		}
		break;		
		
		default:
		{
			LOG_ERR("Test number does not exist: %d", test_num);
		}
		break;
	}
	
}
#ifdef G_UNIT_TEST_ENABLE_

void ble_test_inc(void)
{
	static uint8_t index_test = 0;	
	
	test_10_1[0] = index_test++;
	memcpy(test_10_read,test_10_1, sizeof(test_10_1));
	bleSendMsg(test_10_1, sizeof(test_10_1));
	
	if (index_test == 255)
		index_test = 0;
}

void ble_test_inc_read(uint8_t* ptr_data, uint16_t size)
{
	if (memcmp(ptr_data, test_10_read, size) != 0)
	{
		LOG("Msg not complete, Counter: %d\r\n", ble_msg_failed_counter);
		ble_msg_failed_counter++;
	}
}

void ble_test(uint8_t test_num)
{
	uint8_t i;//, j;
	//ble_config_index = 0;
	//static uint8_t test_10_1[] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44, 0x00, 0x11, 0x22, 0x33, 0x44, 0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44};

	//static uint8_t test_10_1[1000] = {2};
	//static uint8_t test_10_2[10] = {0x99, 0x88,0x77,0x66,0x55, 0x00, 0x11, 0x22, 0x33, 0x44};
	
	switch (test_num)
	{
		case 1:
		{
			while (1)
			{
				for (i = 0;i < 50 ; i++)
				{
					//test_10_2[0] = i;

					bleSendMsg(test_10_2, sizeof(test_10_2));
					
					//PRINT("ble_msg_failed_counter: %d\r\n",++ble_msg_failed_counter);
					//for (j = 0;j < sizeof(test_10_1) ; j++)
						//PRINT("%02X ", test_10_1[j]);
					//PRINT("\r\n");
					osDelay(40);
				}
			}
		}
		break;
		
		case 2:
		default:
		{
			//osDelay(50);
	
			bleSendMsg(test_10_1, sizeof(test_10_1));

		}
		break;
		
		case 3:
		{
			while (1)
			{
				for (i = 0;i < 50 ; i++)
				{
					//test_10_2[0] = i;
					bleSendMsg(test_10_2, sizeof(test_10_2));
					
					//PRINT("ble_msg_failed_counter: %d\r\n",++ble_msg_failed_counter);
					//for (j = 0;j < sizeof(test_10_1) ; j++)
						//PRINT("%02X ", test_10_1[j]);
					//PRINT("\r\n");
					osDelay(100);
				}
			}
		}
		break;
		
		case 4:
		{
//			prvBleSendBroadcast(BLE_ADV_STATUS_ENTER_STANDBY_MODE);
		}
		break;

		case 5:
		{
//			prvBleSendBroadcast(BLE_ADV_STATUS_LEAVE_STANDBY_MODE);
		}
		break;	
		
		case 9:
		{
			uint8_t connect[] = {0x01, 0x05, 0x04, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
			uint8_t negotiate_1[] = {02, 0xFF, 0x0F, 0x07, 0x00, 0x00, 0x01, 0x03, 0x00, 0x03, 0x00, 0x00};
			bleSendMsg(connect, sizeof(connect));
			osDelay(20);
			
			bleSendMsg(negotiate_1, sizeof(negotiate_1));
			
			osDelay(20);
		}		
		break;	
		
		
		
		
	}
}

#endif //G_UNIT_TEST_ENABLE
