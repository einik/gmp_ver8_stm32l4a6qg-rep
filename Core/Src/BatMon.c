//#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

#include "common.h"
#include "serial_protocol.h"
#include "BatMon.h"
#include "BSP.h"
//#include "assert.h"


/****************************************************************/
/*						Local Definitions	 					*/
#define BAT_MON_ALGO_WINDOW_SIZE 				4
#define BAT_MON_LOW_BATTERY_VOLTAGE				2950//2910//3200 //per Benny's request
#define BAT_MON_CRIT_LOW_BATTERY_VOLTAGE		2900//2860//3150
#define BAT_MON_START_VOLTAGE					2700
#define BAT_MON_START_VOLTAGE_DIFFERENCE		200//250
#define BAT_MON_BATTERY_READ_INTERVAL			10000
#define BAT_MON_SECONDS_IN_MIN					60
#define BAT_MON_BATTERY_READ_INTERVAL_MIN		3//30 //per Benny's request
#define BAT_MON_BATTERY_READ_INTERVAL_SECONDS	5//(BAT_MON_BATTERY_READ_INTERVAL_MIN * BAT_MON_SECONDS_IN_MIN) //1800
#define BAT_MON_ONE_SECOND_INTERVAL				1000
#define BAT_MON_BATTERY_READ_MAX_RETRIES		3
#define BAT_MON_TRAIN_TIMER_PERIOD				10//10000//5000
#define BAT_MON_BATTERY_TRAINING_LEVEL			3200
#define BAT_MON_READ_INTERRUPT_PERIOD			1000//500
/****************************************************************/


/****************************************************************/
/*						Local Functions		 					*/
static void batMonTask(void const * argument);
static BAT_MON_RESULT prvBatMonBatteryGet(void);
static BAT_MON_RESULT prvBatMonAvgVolt(float32_t sample_in, uint32_t sample_index, float32_t * result);
static void batTrainTimerCallback(void const *n);
static void batReadInterruptTimerCallback(void const *n);
static BAT_MON_RESULT prvBatMonValidateBattery(void);
static BAT_MON_RESULT prvBatMonStartBatValidation(void);
/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
static BATTERY_MONITOR_LEVEL_ENUM		batt_level = BAT_MON_LEVEL_UNKNOWN;
static uint16_t							current_volt  = 0;
static osThreadId 						bat_mon_task_handle;
static uint8_t							bat_mon_print_bat = 0;
static osTimerId 						bat_train_timer;
static osTimerId 						bat_read_interrupt_timer;
static uint8_t							bat_training_mode = FALSE;
static uint8_t							bat_training_counter = 0;
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/

/****************************************************************/

/****************************************************************/
/*						  Weak function							*/
__weak void bat_mon_event(bat_mon_event_s * event){}
__weak void batMonBatteryValidState(BATT_MON_STATE_E state){}
/****************************************************************/

BAT_MON_RESULT batMonInit(void)
{
	osThreadDef(batMonTask, batMonTask, osPriorityNormal, 0, BAT_MON_STACK_SIZE);
	bat_mon_task_handle = osThreadCreate(osThread(batMonTask), NULL);
	if (!bat_mon_task_handle)
		return BAT_MON_RESULT_ERROR;
	
	osTimerDef(bat_train_timer, batTrainTimerCallback);
	bat_train_timer = osTimerCreate(osTimer(bat_train_timer), osTimerPeriodic, ( void * ) 0);
	osTimerDef(bat_read_interrupt_timer, batReadInterruptTimerCallback);
	bat_read_interrupt_timer = osTimerCreate(osTimer(bat_read_interrupt_timer), osTimerOnce, ( void * ) 0);
	
	prvBatMonValidateBattery();

	return BAT_MON_RESULT_OK;
}
BAT_MON_RESULT batMonGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (bat_mon_task_handle == NULL) || (ret_val == NULL) )
		return BAT_MON_RESULT_ERROR_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark( bat_mon_task_handle );
	return BAT_MON_RESULT_OK;
}
BAT_MON_RESULT batMonBatLevelGet(uint16_t* volt)
{
	if (volt == NULL)
		return BAT_MON_RESULT_ERROR;


	if ( current_volt == 0 )
	{
		BAT_MON_RESULT res = prvBatMonBatteryGet();
		if (res != BAT_MON_RESULT_OK)
			return res;
	}

	* volt = current_volt;
	
	current_volt = 0;//TODO: TEMP, remove after battery montor is activated
	return BAT_MON_RESULT_OK;

}

static BAT_MON_RESULT prvBatMonBatteryGet(void)
{
	uint16_t			volt = 0;
	bat_mon_event_s		event;
	static uint16_t		volt_reading_counter = 0;
	float32_t 			volt_float = 0;
	uint32_t			adc_value = 0;


	if (bspReadAdc(BSP_ADC_BATTERY_LEVEL, &adc_value) != BSP_RESULT_OK)
	{
		return BAT_MON_RESULT_ERROR_READ_VOLTAGE;
	}

	volt = (uint16_t)(adc_value * (((float32_t)2800 / (1 << 12)) * 3));
	LOG_D(DEBUG_MODULE_BATTERY, DEBUG_LEVEL_1, "Battery: %dmV\r\n", volt);

	if (volt == 0)
		return BAT_MON_RESULT_ERROR;	
	
	BAT_MON_RESULT res = prvBatMonAvgVolt((float32_t)volt, volt_reading_counter++, &volt_float);

	if (res != BAT_MON_RESULT_OK)
	{
		return 	res;
	}
	
	volt = (uint16_t)volt_float;

	if (volt == 0)
		return BAT_MON_RESULT_ERROR;	
	
	current_volt = volt;
	
//	if ((volt <= BAT_MON_CRIT_LOW_BATTERY_VOLTAGE) && (batt_level != BAT_MON_LEVEL_CRITICALLY_LOW))
//	{
//		LOG("Critically Low Battery: %d \r\n",volt);
//		batt_level = BAT_MON_LEVEL_CRITICALLY_LOW;
//		event.event_type = BAT_MON_EVENT_CRIT_LOW_BATTERY;
//		bat_mon_event(&event);			
//	}
//	else if ((volt > BAT_MON_CRIT_LOW_BATTERY_VOLTAGE) && (volt <= BAT_MON_LOW_BATTERY_VOLTAGE) && (batt_level != BAT_MON_LEVEL_LOW) )
//	{
//		batt_level = BAT_MON_LEVEL_LOW;
//		LOG("Low Battery: %d \r\n",volt);
//		event.event_type = BAT_MON_EVENT_LOW_BATTERY;
//		bat_mon_event(&event);		
//	}
	
	return BAT_MON_RESULT_OK;
}




static BAT_MON_RESULT prvBatMonAvgVolt(float32_t sample_in, uint32_t sample_index, float32_t * result)
{
	static float32_t sample_buffer[BAT_MON_ALGO_WINDOW_SIZE];
	static float32_t window_sum;

	if (result == NULL)
		return BAT_MON_RESULT_ERROR_WRONG_PARAM;

	if (sample_index == 0)
		window_sum = 0;

	if (sample_index < BAT_MON_ALGO_WINDOW_SIZE)
	{
		sample_buffer[sample_index] = sample_in;
		window_sum += sample_in;
		*result = NAN;
		return BAT_MON_RESULT_ERROR_NOT_ENOUGH_DATA;
	}

	window_sum = window_sum - sample_buffer[sample_index % BAT_MON_ALGO_WINDOW_SIZE] + sample_in;
	sample_buffer[sample_index % BAT_MON_ALGO_WINDOW_SIZE] = sample_in;


	*result = (window_sum / (float32_t)BAT_MON_ALGO_WINDOW_SIZE);

	return BAT_MON_RESULT_OK;
}

static BAT_MON_RESULT prvBatMonBattAvgGet(void)
{
	uint8_t				battery_read_retries = 0;
	uint8_t				i = 0;
	BAT_MON_RESULT		bat_mon_res;
	
	
	for (i = 0;i <= BAT_MON_ALGO_WINDOW_SIZE;i++)
	{
		osDelay(BAT_MON_ONE_SECOND_INTERVAL / 2);
		bat_mon_res = prvBatMonBatteryGet();
		if ((i == BAT_MON_ALGO_WINDOW_SIZE) && (bat_mon_res != BAT_MON_RESULT_OK))
		{//If final reading wasn't successful add 3 more readings
			if (battery_read_retries < BAT_MON_BATTERY_READ_MAX_RETRIES)
			{
				battery_read_retries++;
				i--;
			}
			
		}
	}	
	
	return BAT_MON_RESULT_OK;
}

static BAT_MON_RESULT prvBatMonValidateBattery(void)
{
	uint16_t			bat_volt = 0, bat_volt_3rd;
	uint8_t 			bat_valid = TRUE;
	bat_mon_event_s		event;
	
	if (batMonVoltGet(&bat_volt) == BAT_MON_RESULT_OK)
	{
		LOG("1st Battery and Capacitor read: %dV\r\n", bat_volt);
		if (bat_volt > BAT_MON_START_VOLTAGE)
		{//Battery above 2.7V, wait for 1 second and check again
			osDelay(BAT_MON_ONE_SECOND_INTERVAL);
			if (batMonVoltGet(&bat_volt) == BAT_MON_RESULT_OK)
			{
				LOG("2nd Battery and Capacitor read %dV\r\n", bat_volt);
				if (bat_volt > (BAT_MON_START_VOLTAGE + BAT_MON_START_VOLTAGE_DIFFERENCE))
				{//Battery above (2.7V + 0.25V), wait 1 second and check again
					osDelay(BAT_MON_ONE_SECOND_INTERVAL);
					if (batMonVoltGet(&bat_volt_3rd) == BAT_MON_RESULT_OK)
					{
						LOG("3rd Battery and Capacitor read %dV\r\n", bat_volt_3rd);
						if ((bat_volt_3rd + 50) < bat_volt) //Battery too low, turn on battery training
							bat_valid = FALSE;
					}
					else //Could not read battery from ADC
						return BAT_MON_RESULT_ERROR_READ_VOLTAGE;
				}
				else //Battery too low, turn on battery training
					bat_valid = FALSE;
			}
			else //Could not read battery from ADC
				return BAT_MON_RESULT_ERROR_READ_VOLTAGE;				
		}
		else //Battery too low, turn on battery training
			bat_valid = FALSE;
	}
	else //Could not read battery from ADC
		return BAT_MON_RESULT_ERROR_READ_VOLTAGE;	
			
	if (!bat_valid)
	{//Battery too low, turn on battery training
		prvBatMonBattAvgGet();
		if (current_volt < BAT_MON_BATTERY_TRAINING_LEVEL)
		{
			batMonStartBatteryTraining();
			return BAT_MON_RESULT_ERROR_TRAINING_REQUIRED;
		}
		
	}
	
	event.event_type = BAT_MON_EVENT_IDLE_BATTERY;
	bat_mon_event(&event);

	prvBatMonStartBatValidation();
	
	return BAT_MON_RESULT_OK;
			
}

static BAT_MON_RESULT prvBatMonStartBatValidation(void)
{
	uint32_t			batt_comp = 1;
		
	if (bspCompModeSet(BSP_MODE_SET_ON) != BSP_RESULT_OK)
	{
		LOG("Mode not defined\r\n");
		return BAT_MON_RESULT_ERROR_WRONG_PARAM;
	}

	if (bspReadComp(&batt_comp) != BSP_RESULT_OK)
	{
		LOG("Comparator read failed\r\n");
		return BAT_MON_RESULT_ERROR;
	}
	
	if (!batt_comp)//Battery is low
		osTimerStart(bat_read_interrupt_timer, BAT_MON_READ_INTERRUPT_PERIOD);
	
	return BAT_MON_RESULT_OK;
}

static void batMonTask(void const * argument)
{
//	uint32_t			get_bat_counter = 0;

//	prvBatMonStartBatValidation();

	while(1)
	{
//		if (get_bat_counter == 0)
//		{
//			prvBatMonBattAvgGet();
//			LOG("Battery %dV\r\n", current_volt);
//		}
		
		osDelay(BAT_MON_ONE_SECOND_INTERVAL);
//		get_bat_counter = (get_bat_counter + 1) % BAT_MON_BATTERY_READ_INTERVAL_SECONDS;		
		
		
		if (bat_mon_print_bat)
		{//In case we want to print battery readings in faster interval
			osDelay(BAT_MON_BATTERY_READ_INTERVAL);
			LOG("Battery: %dmV\r\n", current_volt);
			prvBatMonBatteryGet();
		}
	}
}


BAT_MON_RESULT batMonReadEnable(uint8_t en)
{
	if (en)
		bat_mon_print_bat = 1;
	else
		bat_mon_print_bat = 0;

	return BAT_MON_RESULT_OK;
}

BAT_MON_RESULT batMonVoltGet(uint16_t* volt)
{
	uint32_t adc_value = 0;
	
	if (bspReadAdc(BSP_ADC_BATTERY_LEVEL, &adc_value) != BSP_RESULT_OK)
	{
		return BAT_MON_RESULT_ERROR_READ_VOLTAGE;
	}

	*volt = (uint16_t)(adc_value * (((float32_t)2800 / (1 << 12)) * 3));

	return BAT_MON_RESULT_OK;
}

BAT_MON_RESULT batMonStartBatteryTraining(void)
{
	bat_training_counter = 0;
	
	LOG("Battery training started\r\n");
	BSP_RESULT_E res = bsp_write_pin(BSP_PIN_TYPE_BAT_LOAD_EN, BSP_PIN_SET);
	
	bat_training_mode = TRUE;
	osTimerStart(bat_train_timer, BAT_MON_ONE_SECOND_INTERVAL);//BAT_MON_TRAIN_TIMER_PERIOD);
	
	return BAT_MON_RESULT_OK;
}

static void batTrainTimerCallback(void const *n)
{
	bat_mon_event_s		event;
	uint16_t			voltage = 0;
	
	batMonVoltGet(&voltage);//prvBatMonBatteryGet();
	
	if (voltage > BAT_MON_BATTERY_TRAINING_LEVEL)
	{
		bsp_write_pin(BSP_PIN_TYPE_BAT_LOAD_EN, BSP_PIN_RESET);
		event.event_type = BAT_MON_EVENT_IDLE_BATTERY;
		bat_mon_event(&event);
		osTimerStop(bat_train_timer);
		LOG("Battery training completed %dV, battery is valid\r\n", voltage);
		prvBatMonStartBatValidation();
	}	
	else if (bat_training_counter++ > BAT_MON_TRAIN_TIMER_PERIOD)
	{
		bsp_write_pin(BSP_PIN_TYPE_BAT_LOAD_EN, BSP_PIN_RESET);
		bat_training_mode = FALSE;
		event.event_type = BAT_MON_EVENT_CRIT_LOW_BATTERY;
		bat_mon_event(&event);
		LOG("Battery training completed %dV, battery is low\r\n", voltage);
		osTimerStop(bat_train_timer);
	}
	else
		LOG("Battery training in progress %dV\r\n", voltage);
}

void batReadInterruptTimerCallback(void const *n)
{
	bat_mon_event_s event;
	
	event.event_type = BAT_MON_EVENT_CRIT_LOW_BATTERY;
	bat_mon_event(&event);	
}

void bspCompBatteryLowCallback()
{
	uint32_t	batt_comp = 1;

	bspReadComp(&batt_comp);
	
	if (batt_comp)
	{
//		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
		osTimerStop(bat_read_interrupt_timer);
	}
	else
	{
//		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
		osTimerStart(bat_read_interrupt_timer, BAT_MON_READ_INTERRUPT_PERIOD);
		LOG_ERR("Low Battery timer started\r\n");		
	}

}

//
//void test_bat_mon(void)
//{
//	 uint8_t aa[8000];
//	 for (int i=0; i< sizeof(aa); i++)
//	 {
//		 aa[i] = i;
//	 }
//	assert(1==1);
//}
