#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
#include "serial_protocol.h"
#include "common.h"
#include "lis2h.h"
/****************************************************************/
/*						Local Definitions						*/

#define LIS2H_REGISTER_TEMP_L						0x0B
#define LIS2H_REGISTER_TEMP_H						0x0C
#define LIS2H_REGISTER_WHO_AM_I						0x0F
#define LIS2H_REGISTER_ACT_THS						0x1E
#define LIS2H_REGISTER_ACT_DUR						0x1F
#define LIS2H_REGISTER_CONTROL1						0x20
#define LIS2H_REGISTER_CONTROL2						0x21
#define LIS2H_REGISTER_CONTROL3						0x22
#define LIS2H_REGISTER_CONTROL4						0x23
#define LIS2H_REGISTER_CONTROL5						0x24
#define LIS2H_REGISTER_CONTROL6						0x25
#define LIS2H_REGISTER_CONTROL7						0x26
#define LIS2H_REGISTER_STATUS						0x27
#define LIS2H_REGISTER_OUT_X_L						0x28
#define LIS2H_REGISTER_OUT_X_H						0x29
#define LIS2H_REGISTER_OUT_Y_L						0x2A
#define LIS2H_REGISTER_OUT_Y_H						0x2B
#define LIS2H_REGISTER_OUT_Z_L						0x2C
#define LIS2H_REGISTER_OUT_Z_H						0x2D
#define LIS2H_REGISTER_FIFO_CONTROL					0x2E
#define LIS2H_REGISTER_FIFO_SOURCE					0x2F
#define LIS2H_REGISTER_IG_CFG1						0x30
#define LIS2H_REGISTER_IG_SRC1						0x31
#define LIS2H_DEVICE_ID								0x41
#define LIS2H_READ_BIT								0x80

#define LIS2H_REGISTER_READ_MIN_SIZE				1
#define LIS2H_REGISTER_READ_MAX_SIZE				20

#define LIS2H_REG_CTRL1_VALUE						0xD7
#define LIS2H_REG_CTRL1_SLEEP_VALUE					0x07
#define LIS2H_REG_CTRL4_VALUE						0x34


typedef struct{
	uint8_t		act_ths;
	uint8_t		act_dur;
	uint8_t		ctrl1;
	uint8_t		ctrl2;
	uint8_t		ctrl3;
	uint8_t		ctrl4;
	uint8_t		ctrl5;
	uint8_t		ctrl6;
	uint8_t		ctrl7;
	uint8_t		status;	
}LIS2H_REGISTERS_STRUCT;

/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/

/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/
static LIS2H_RESULT_E prvLis2hWhoAmI(void);
static LIS2H_RESULT_E prvLis2hWriteRegister(uint8_t reg, uint8_t data);
static LIS2H_RESULT_E prvLis2hReadRegister(IN uint8_t reg, IN uint8_t size, OUT uint8_t* data);
//static LIS2H_RESULT_E prvLis2hPrintRegisters(void);
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/

/****************************************************************/

/****************************************************************/
/*						Weak Functions							*/

/****************************************************************/

/***********************************************************************
 * Function name: lis2hInit
 * Description: Init Accelerometer
 * Parameters : 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hInit(void)
{
	LIS2H_REGISTERS_STRUCT	registers;
	
	if (prvLis2hWhoAmI() != LIS2H_RESULT_OK)
	{
		LOG_ERR("LIS2H Who Am I command failed\r\n");
		return LIS2H_RESULT_ERROR_INIT_FAILED;
	}

	if (prvLis2hReadRegister(LIS2H_REGISTER_ACT_THS, sizeof(registers), (uint8_t*)&registers) != LIS2H_RESULT_OK)
	{
		LOG_ERR("LIS2H read registers failed\r\n");
		return LIS2H_RESULT_ERROR_INIT_FAILED;
	}
	
	if (registers.ctrl1 != LIS2H_REG_CTRL1_VALUE)
	{
		if (prvLis2hWriteRegister(LIS2H_REGISTER_CONTROL1, LIS2H_REG_CTRL1_VALUE) != LIS2H_RESULT_OK)
		{
			LOG_ERR("LIS2H LIS2H_REGISTER_CONTROL1 write failed\r\n");
			return LIS2H_RESULT_ERROR_INIT_FAILED;
		}		
	}	
	
	if (registers.ctrl4 != LIS2H_REG_CTRL4_VALUE)
	{
		if (prvLis2hWriteRegister(LIS2H_REGISTER_CONTROL4, LIS2H_REG_CTRL4_VALUE) != LIS2H_RESULT_OK)
		{
			LOG_ERR("LIS2H LIS2H_REGISTER_CONTROL4 write failed\r\n");
			return LIS2H_RESULT_ERROR_INIT_FAILED;
		}		
	}
	
//	if (prvLis2hWriteRegister(LIS2H_REGISTER_ACT_THS, 0) != LIS2H_RESULT_OK)
//	{
//		LOG_ERR("LIS2H LIS2H_REGISTER_ACT_THS write failed\r\n");
//		return LIS2H_RESULT_ERROR_INIT_FAILED;
//	}		
//
//	if (prvLis2hWriteRegister(LIS2H_REGISTER_ACT_DUR, 0x05) != LIS2H_RESULT_OK)
//	{
//		LOG_ERR("LIS2H LIS2H_REGISTER_ACT_DUR write failed\r\n");
//		return LIS2H_RESULT_ERROR_INIT_FAILED;
//	}	
	
//	if (prvLis2hWriteRegister(LIS2H_REGISTER_CONTROL1, 0x97) != LIS2H_RESULT_OK)
//	{
//		LOG_ERR("LIS2H LIS2H_REGISTER_CONTROL1 write failed\r\n");
//		return LIS2H_RESULT_ERROR_INIT_FAILED;
//	}	
	
//	if (prvLis2hWriteRegister(LIS2H_REGISTER_CONTROL3, 0x01) != LIS2H_RESULT_OK)
//	{
//		LOG_ERR("LIS2H LIS2H_REGISTER_CONTROL3 write failed\r\n");
//		return LIS2H_RESULT_ERROR_INIT_FAILED;
//	}

//    if (prvLis2hWriteRegister(LIS2H_REGISTER_FIFO_CONTROL, 0x00) != LIS2H_RESULT_OK)
//    {
//        LOG_ERR("LIS2H LIS2H_REGISTER_FIFO_CONTROL write failed\r\n");
//        return LIS2H_RESULT_ERROR_INIT_FAILED;
//    }    

//    if (prvLis2hWriteRegister(LIS2H_REGISTER_IG_CFG1, 0x7F) != LIS2H_RESULT_OK)
//    {
//        LOG_ERR("LIS2H LIS2H_REGISTER_IG_CFG1 write failed\r\n");
//        return LIS2H_RESULT_ERROR_INIT_FAILED;
//    } 	

//	if (prvLis2hWriteRegister(LIS2H_REGISTER_FIFO_SOURCE, 0x60) != LIS2H_RESULT_OK)
//	{
//		LOG_ERR("LIS2H LIS2H_REGISTER_FIFO_SOURCE write failed\r\n");
//		return LIS2H_RESULT_ERROR_INIT_FAILED;
//	}	
	
//	prvLis2hPrintRegisters();
	
	return LIS2H_RESULT_OK;
}

/***********************************************************************
 * Function name: prvLis2hWhoAmI
 * Description: Send Who Am I command and check response
 * Parameters : 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *
 ***********************************************************************/
static LIS2H_RESULT_E prvLis2hWhoAmI(void)
{
	BSP_RESULT_E 	bsp_res;
	uint8_t			reg_address[] = {LIS2H_READ_BIT | LIS2H_REGISTER_WHO_AM_I, 0xFF};
	uint8_t			read_buff[2] = {0};


	bsp_res = bsp_spi_transmit_receive(BSP_SPI_ACCEL, reg_address, 2, 1000, read_buff);//TODO: fix later, works only from second command
	osDelay(1);
	bsp_res = bsp_spi_transmit_receive(BSP_SPI_ACCEL, reg_address, 2, 1000, read_buff);	
	
	if (bsp_res != BSP_RESULT_OK)
		return LIS2H_RESULT_ERROR;
	
	if (read_buff[1] != LIS2H_DEVICE_ID)
		return LIS2H_RESULT_ERROR;
	
	return LIS2H_RESULT_OK;
}

/***********************************************************************
 * Function name: prvLis2hWriteRegister
 * Description: Write data to register and verify it 
 * Parameters : 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *
 ***********************************************************************/
static LIS2H_RESULT_E prvLis2hWriteRegister(uint8_t reg, uint8_t data)
{
	uint8_t			write_buff[2] = {0};
	uint8_t			read_buff[2] = {0};
	
	
	write_buff[0] = reg;
	write_buff[1] = data;
	
	if (bsp_spi_transmit(BSP_SPI_ACCEL, write_buff, sizeof(write_buff), 1000) != BSP_RESULT_OK)
		return LIS2H_RESULT_ERROR_WRITE_REGISTER_FAILED;
	osDelay(1);
	
	write_buff[0] = LIS2H_READ_BIT | reg;
	write_buff[1] = 0xFF;
	if (bsp_spi_transmit_receive(BSP_SPI_ACCEL, write_buff, sizeof(read_buff), 1000, read_buff) != BSP_RESULT_OK)
		return LIS2H_RESULT_ERROR_WRITE_REGISTER_FAILED;
	
	if (read_buff[1] != data)
		return LIS2H_RESULT_ERROR_WRITE_REGISTER_FAILED;
	
	return LIS2H_RESULT_OK;
}

/***********************************************************************
 * Function name: prvLis2hReadRegister
 * Description: Read data from register
 * Parameters : 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *
 ***********************************************************************/
static LIS2H_RESULT_E prvLis2hReadRegister(IN uint8_t reg, 
										   IN uint8_t size, 
										   OUT uint8_t* data)
{
	BSP_RESULT_E 	bsp_res;
	uint8_t			reg_address[] = {LIS2H_READ_BIT | reg, 0xFF};
	uint8_t			read_buff[LIS2H_REGISTER_READ_MAX_SIZE + 1] = {0};

	if ((size < LIS2H_REGISTER_READ_MIN_SIZE) || (size > LIS2H_REGISTER_READ_MAX_SIZE))
		return LIS2H_RESULT_ERROR_SIZE_INVALID;

	bsp_res = bsp_spi_transmit_receive(BSP_SPI_ACCEL, reg_address, size + 1, 1000, read_buff);	
	
	if (bsp_res != BSP_RESULT_OK)
		return LIS2H_RESULT_ERROR;
	
	memcpy(data, &read_buff[1], size);
	
	return LIS2H_RESULT_OK;
}
//static LIS2H_RESULT_E prvLis2hReadRegister(IN uint8_t reg, 
////										   IN uint8_t size, 
//										   OUT uint8_t* data)
//{
//	BSP_RESULT_E 	bsp_res;
//	uint8_t			reg_address[] = {LIS2H_READ_BIT | reg, 0xFF};
//	uint8_t			read_buff[2] = {0};
//
//
//	bsp_res = bsp_spi_transmit_receive(BSP_SPI_ACCEL, reg_address, 2, 1000, read_buff);	
//	
//	if (bsp_res != BSP_RESULT_OK)
//		return LIS2H_RESULT_ERROR;
//	
//	*data = read_buff[1];
//	
//	return LIS2H_RESULT_OK;
//}



/***********************************************************************
 * Function name: prvLis2hPrintRegisters
 * Description: Print all registers
 * Parameters : 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hPrintRegisters(void)
{
	uint8_t reg_data = 0;
//	uint8_t x, y, z;
	
	if (prvLis2hReadRegister(LIS2H_REGISTER_ACT_THS, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("ACT_THS: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_ACT_DUR, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("ACT_DUR: 0x%02X\r\n", reg_data);

	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_CONTROL1, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("CONTROL1: 0x%02X\r\n", reg_data);

	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_CONTROL2, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("CONTROL2: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_CONTROL3, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("CONTROL3: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_CONTROL4, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("CONTROL4: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_CONTROL5, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("CONTROL5: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_CONTROL6, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("CONTROL6: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_CONTROL7, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("CONTROL7: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_STATUS, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("STATUS: 0x%02X\r\n", reg_data);

	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_FIFO_CONTROL, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("FIFO_CONTROL: 0x%02X\r\n", reg_data);

	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_FIFO_SOURCE, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("FIFO_SOURCE: 0x%02X\r\n", reg_data);

	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_IG_CFG1, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("IG_CFG1: 0x%02X\r\n", reg_data);

	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_IG_SRC1, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("IG_SRC1: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_OUT_X_L, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("OUT_X_L: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_OUT_X_H, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("OUT_X_H: 0x%02X\r\n", reg_data);

	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_OUT_Y_L, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("OUT_Y_L: 0x%02X\r\n", reg_data);

	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_OUT_Y_H, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("OUT_Y_H: 0x%02X\r\n", reg_data);

	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_OUT_Z_L, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("OUT_Z_L: 0x%02X\r\n", reg_data);
	
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_OUT_Z_H, 1, &reg_data) == LIS2H_RESULT_OK)
		LOG("OUT_Z_H: 0x%02X\r\n", reg_data);


	return LIS2H_RESULT_OK;
}

/***********************************************************************
 * Function name: lis2hWriteReg
 * Description: Write LIS2H registers
 * Parameters : address - register address, data - data to write in register
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hWriteReg(uint8_t address, uint8_t data)
{
	prvLis2hWriteRegister(address, data);	
	
	return LIS2H_RESULT_OK;
}

/***********************************************************************
 * Function name: lis2hGetXAxis
 * Description: Read and calculate x axis from registers
 * Parameters : x - 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *		LIS2H_RESULT_ERROR_READ_REGISTER_FAILED - register read failed
 *		LIS2H_RESULT_ERROR_INVALID_AXIS - wrong axis
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hGetAxis(LIS2H_AXIS_E axis, uint8_t* position)
{
	uint8_t			reg_address_axis_l;
	uint8_t			reg_address_axis_h;
	uint8_t 		reg_data;
		
	switch (axis)
	{
		case LIS2H_AXIS_X:
			reg_address_axis_l = LIS2H_REGISTER_OUT_X_L;
			reg_address_axis_h = LIS2H_REGISTER_OUT_X_H;
		break;

		case LIS2H_AXIS_Y:
			reg_address_axis_l = LIS2H_REGISTER_OUT_Y_L;
			reg_address_axis_h = LIS2H_REGISTER_OUT_Y_H;
		break;
		
		case LIS2H_AXIS_Z:
			reg_address_axis_l = LIS2H_REGISTER_OUT_Z_L;
			reg_address_axis_h = LIS2H_REGISTER_OUT_Z_H;
		break;
		
		default:
			return LIS2H_RESULT_ERROR_INVALID_AXIS;
		break;
	}
	
	if (prvLis2hReadRegister(reg_address_axis_l, 1, &reg_data) == LIS2H_RESULT_OK)
	{
//		LOG("OUT_X_L: 0x%02X\r\n", reg_data);
		*position = reg_data;
	}
	else
		return LIS2H_RESULT_ERROR_READ_REGISTER_FAILED;
	
	osDelay(1);
	if (prvLis2hReadRegister(reg_address_axis_h, 1, &reg_data) == LIS2H_RESULT_OK)
	{
		LOG("OUT_X_H: 0x%02X\r\n", reg_data);
		*position -= reg_data;
	}
	else
		return LIS2H_RESULT_ERROR_READ_REGISTER_FAILED;
	
	return LIS2H_RESULT_OK;
}

/***********************************************************************
 * Function name: lis2hGetAllAxis 
 * Description: Read all axis from registers
 * Parameters : x - position on x axis, y - position on y axis, z - position on z axis
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR - on failure
 *		LIS2H_RESULT_ERROR_READ_REGISTER_FAILED - register read failed
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hGetAllAxis(LIS2H_AXIS* axis)//uint16_t* x, uint16_t* y, uint16_t* z)
{
	osDelay(1);
	if (prvLis2hReadRegister(LIS2H_REGISTER_OUT_X_L, sizeof(LIS2H_AXIS), (uint8_t*)axis) != LIS2H_RESULT_OK)
		return LIS2H_RESULT_ERROR_READ_REGISTER_FAILED;
	
	return LIS2H_RESULT_OK;
}

/***********************************************************************
 * Function name: lis2hSleep 
 * Description: LIS2H sleep
 * Parameters : 
 *
 * Returns :
 *		LIS2H_RESULT_OK - on success
 *		LIS2H_RESULT_ERROR_READ_REGISTER_FAILED - register read failed
 *
 ***********************************************************************/
LIS2H_RESULT_E lis2hSleep(void)
{
	uint8_t		reg_value = 0;
	
	if (prvLis2hReadRegister(LIS2H_REGISTER_CONTROL1, sizeof(uint8_t), (uint8_t*)&reg_value) != LIS2H_RESULT_OK)
		return LIS2H_RESULT_ERROR_READ_REGISTER_FAILED;
	
	if (reg_value == LIS2H_REG_CTRL1_SLEEP_VALUE)
		return LIS2H_RESULT_OK;
	
	if (prvLis2hWriteRegister(LIS2H_REGISTER_CONTROL1, LIS2H_REG_CTRL1_SLEEP_VALUE) != LIS2H_RESULT_OK)
		return LIS2H_RESULT_ERROR_READ_REGISTER_FAILED;
	
	return LIS2H_RESULT_OK;
}