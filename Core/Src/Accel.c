#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
#include "serial_protocol.h"
#include "common.h"
#include "Accel.h"
#include "lis2h.h"
#include "actl_identify_activity_type.h"

/****************************************************************/
/*						Local Definitions						*/

#define ACCEL_MODE_DEFAULT				ACCEL_CONFIG_MODE_ENABLE
#define ACCEL_ALGORITHM_VERSION			6
/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
static ACCEL_CONFIG_STRUCT		accel_config = {.accel_config_mode = ACCEL_CONFIG_MODE_DISABLE, 
												.accel_config_period_time = 0, 
												.accel_config_axis_data_in_event = ACCEL_CONFIG_AXIS_DATA_IN_EVENT_DISABLE,
												.accel_config_send_events = ACCEL_CONFIG_SEND_EVENTS_DISABLE};
/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/
void accelTask(void const * argument);
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/
osThreadId 										accelTaskHandle;
/****************************************************************/

/****************************************************************/
/*						Weak Functions							*/

/****************************************************************/

/***********************************************************************
 * Function name: accelInit
 * Description: Init Accelerometer
 * Parameters : 
 *
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
ACCEL_RESULT_E accelInit(void)
{

	if (lis2hInit() != LIS2H_RESULT_OK)
		return ACCEL_RESULT_ERROR_INIT_FAILED;
	
	//Init task
	osThreadDef(accelTask, accelTask, osPriorityNormal, 0, ACCEL_STACK_SIZE);
	accelTaskHandle = osThreadCreate(osThread(accelTask), NULL);
	if (!accelTaskHandle)
		return ACCEL_RESULT_ERROR;	
	
	actl_identify_activity_type_initialize();
	
	return ACCEL_RESULT_OK;
}

/***********************************************************************
 * Function name: accelWriteLis2hReg
 * Description: Write LIS2H registers
 * Parameters : address - register address, data - data to write in register
 *
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
ACCEL_RESULT_E accelWriteLis2hReg(uint8_t address, uint8_t data)
{
	lis2hWriteReg(address, data);
	
	return ACCEL_RESULT_OK;
}

/***********************************************************************
 * Function name: accelGetPositions
 * Description: Get accelerometer positions
 * Parameters : x - x axis, y - y axis, z - z axis
 *
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_AXIS_READ_FAILED - on failure
 *
 ***********************************************************************/
ACCEL_RESULT_E accelGetPositions(uint8_t* x, uint8_t* y, uint8_t* z)
{
	
	if (lis2hGetAxis(LIS2H_AXIS_X, x) != LIS2H_RESULT_OK)
		return ACCEL_RESULT_ERROR_AXIS_READ_FAILED;

	if (lis2hGetAxis(LIS2H_AXIS_Y, y) != LIS2H_RESULT_OK)
		return ACCEL_RESULT_ERROR_AXIS_READ_FAILED;

	if (lis2hGetAxis(LIS2H_AXIS_Z, z) != LIS2H_RESULT_OK)
		return ACCEL_RESULT_ERROR_AXIS_READ_FAILED;
	
	
	return ACCEL_RESULT_OK;
}

/***********************************************************************
 * Function name: accelGetHighWaterMark
 * Description: Get accelerometer task size usage
 * Parameters :
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_WRONG_PARAM - in case of 
 ***********************************************************************/
ACCEL_RESULT_E accelGetHighWaterMark(UBaseType_t * ret_val)
{
	if ((accelTaskHandle == NULL) || (ret_val == NULL))
		return ACCEL_RESULT_ERROR_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark(accelTaskHandle);
	return ACCEL_RESULT_OK;
}

/***********************************************************************
 * Function name: accelPrintRegisters
 * Description: Print accelerometer registers
 * Parameters :
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *
 ***********************************************************************/
ACCEL_RESULT_E accelPrintRegisters(void)
{
	lis2hPrintRegisters();
	
	return ACCEL_RESULT_OK;
}

/***********************************************************************
 * Function name: accelSleep
 * Description: Shutdown Accelerometer sensor
 * Parameters : 
 *
 * Returns :
 *		ACCEL_RESULT_OK - on success
 *		ACCEL_RESULT_ERROR_SLEEP_FAILED - on failure
 *
 ***********************************************************************/
ACCEL_RESULT_E accelSleep(void)
{
	osDelay(10);
	if (lis2hSleep() != LIS2H_RESULT_OK)
		return ACCEL_RESULT_ERROR_SLEEP_FAILED;
	
	return ACCEL_RESULT_OK;
}

void accelTask(void const * argument)
{
	LIS2H_AXIS 			axis;
	static 				uint8_t index = 0;
	uint8_t				curr_position = 0, prev_position = 200;
	osDelay(10);
	lis2hGetAllAxis(&axis);
	
	LOG("Accelerometer Algorithm version: %d\r\n", ACCEL_ALGORITHM_VERSION);
	
	actl_identify_activity_type(axis.x, axis.y, axis.z, TRUE);
	
	while(1)
	{
		osDelay(10);
		lis2hGetAllAxis(&axis);
		LOG_D(DEBUG_MODULE_ACCEL, DEBUG_LEVEL_2,"%d\t%d\t%d\t%d\r\n", index, axis.x, axis.y, axis.z);
		LOG_D(DEBUG_MODULE_ACCEL, DEBUG_LEVEL_1, "Position: %d\r\n", curr_position);
		curr_position = actl_identify_activity_type(axis.x, axis.y, axis.z, FALSE);
		if (prev_position != curr_position)
		{
//			LOG("Position: %d\r\n", curr_position);
			LOG("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n");
			LOG("               position: ");
			switch (curr_position)
			{
			case none:
				LOG("none\r\n");
				break;
			case walk:
				LOG("walk\r\n");
				break;
			case run:
				LOG("run\r\n");
				break;
			case lie_back:
				LOG("lie back\r\n");
				break;
			case lie_right:
				LOG("lie right\r\n");
				break;
			case lie_left:
				LOG("lie left\r\n");
				break;
			case lie_stomach:
				LOG("lie stomach\r\n");
				break;
			case sit_or_stand:
				LOG("sit or stand\r\n");
				break;
			case fall:
				LOG("fall\r\n");
				break;			
			}
			LOG("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n");
			prev_position = curr_position;
		}
		
		index = (index + 1) % 256;
	}
	
}