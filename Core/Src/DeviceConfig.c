#include "deviceConfig.h"
#include "SD.h"
#include "cmsis_os.h"
#include "common.h"
#include "string.h"
#include "ecg.h"
#include "serial_protocol.h"
#include "stdio.h"

// DEFINITIONS

#define DEVICE_CONFIG_FILE_NAME 				"CONFIG/config.txt"
#define DEVICE_CONFIG_DIRECTORY_NAME 			"CONFIG"
#define DEVICE_CONFIG_DATA_OFFSET				0

#define DEVICE_CONFIG_MAX_FILE_SIZE				200


#define DEVICE_CONFIG_ATTR_DEV_MODE 			"DEVICE_MODE"
#define DEVICE_CONFIG_DEV_MODE_PARAM_HOLTER		'0'
#define DEVICE_CONFIG_DEV_MODE_PARAM_PATCH		'1'
#define DEVICE_CONFIG_DEV_MODE_PARAM_TESTER		'5'


#define DEVICE_CONFIG_ATTR_PACE_MAKER 			"PACE_MAKER"
#define DEVICE_CONFIG_PACE_MAKER_PARAM_OFF	   	'0'
#define DEVICE_CONFIG_PACE_MAKER_PARAM_ON	   	'1'

#define DEVICE_CONFIG_LOG_DIRECTORY_NAME 		"LOG"

#define DEVICE_CONFIG_DEFAULT_DEVICE_MODE 		ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY
#define DEVICE_CONFIG_DEFAULT_PACE_MAKER_EN		ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_OFF

// FUNCTIONS


/***********************************************************************
 * Function name: prvDeviceConfigParseElement
 * Description: search text to match configuration entries
 * Parameters : conf - pointer to supported configuration structure to be filled,
 * 				str - point to current location in opened file,
 * 				length of read bytes (matched to configuration entry)
 *
 * Returns :
 *		DEVICE_CONFIG_RESULT_OK - on success (configuration entry found)
 *		DEVICE_CONFIG_RESULT_WRONG_PARAM - no configuration entry found
 *
 ***********************************************************************/
DEVICE_CONFIG_RESULT_E prvDeviceConfigParseElement(INOUT DEVICE_CONFIG_STRUCT * conf, IN uint8_t * str, OUT uint16_t * length)
{
	uint16_t len;
	uint16_t r = strncmp((char*)str, DEVICE_CONFIG_ATTR_DEV_MODE, strlen(DEVICE_CONFIG_ATTR_DEV_MODE));
	if (r == 0)
	{
		len = strlen(DEVICE_CONFIG_ATTR_DEV_MODE) + 1;
		if (str[len] == DEVICE_CONFIG_DEV_MODE_PARAM_PATCH)
		{
			conf->device_mode = ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH;
		}
		else if (str[len] == DEVICE_CONFIG_DEV_MODE_PARAM_TESTER)
		{
			conf->device_mode = ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER;
		}
		else
		{
			conf->device_mode = DEVICE_CONFIG_DEFAULT_DEVICE_MODE;
		}
		*length = len;
		return DEVICE_CONFIG_RESULT_OK;
	}

	r = strncmp((char*)str, DEVICE_CONFIG_ATTR_PACE_MAKER, strlen(DEVICE_CONFIG_ATTR_PACE_MAKER));
	if (r == 0)
	{
		len = strlen(DEVICE_CONFIG_ATTR_PACE_MAKER) + 1;
		if (str[len] == DEVICE_CONFIG_PACE_MAKER_PARAM_ON)
		{
			conf->pace_maker_enable = ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_ON;
		}
		else
		{
			conf->pace_maker_enable = DEVICE_CONFIG_DEFAULT_PACE_MAKER_EN;
		}
		*length = len;
		return DEVICE_CONFIG_RESULT_OK;
	}
	return DEVICE_CONFIG_RESULT_WRONG_PARAM;
}


/***********************************************************************
 * Function name: deviceConfigChangeMode
 * Description: Change and save configuration to SD card
 * Parameters : conf - pointer to supported configuration structure
 *
 * Returns :
 *		DEVICE_CONFIG_RESULT_OK - on success
 *		DEVICE_CONFIG_RESULT_E - for other failures
 *
 ***********************************************************************/
DEVICE_CONFIG_RESULT_E deviceConfigChangeMode(DEVICE_CONFIG_STRUCT * conf)
{
	if (conf == NULL)
		return DEVICE_CONFIG_RESULT_WRONG_PARAM;

	if (conf->device_mode >= ECG_GENERAL_CONFIG_DEVICE_MODE_MAX)
		return DEVICE_CONFIG_RESULT_WRONG_PARAM;

	if (conf->pace_maker_enable >= ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_MAX)
		return DEVICE_CONFIG_RESULT_WRONG_PARAM;

	SD_RESULT_E sd_res;
	char temp[DEVICE_CONFIG_MAX_FILE_SIZE];
	uint16_t temp_len;
	uint16_t curr_count;

	//check if directory exists
	sd_res = sdDirExist(DEVICE_CONFIG_DIRECTORY_NAME);
	if (sd_res == SD_RESULT_ERROR_DIRECTORY_DOES_NOT_EXIST)
	{
		if (sdMakeDir(DEVICE_CONFIG_DIRECTORY_NAME) != SD_RESULT_OK)
			LOG_ERR("Failed to create CONFIG directory\r\n");
	}

	// check if file exists
	sd_res = sdFileExist(DEVICE_CONFIG_FILE_NAME);
	if (sd_res == SD_RESULT_OK)
	{
		sd_res = sdDeleteFile(DEVICE_CONFIG_FILE_NAME);
		if (sd_res != SD_RESULT_OK)
			return DEVICE_CONFIG_RESULT_FAILED_TO_DELETE_PREVIOUS_CONFIG;
	}

	temp_len = sprintf(temp, "%s=%c\r\n%s=%c", DEVICE_CONFIG_ATTR_DEV_MODE,
				DEVICE_CONFIG_DEV_MODE_PARAM_HOLTER+conf->device_mode,
				DEVICE_CONFIG_ATTR_PACE_MAKER,
				DEVICE_CONFIG_PACE_MAKER_PARAM_OFF+conf->pace_maker_enable
				);

	// create and save new file with defaults
	sd_res = sdWriteDataToFile(DEVICE_CONFIG_FILE_NAME, (uint8_t*)temp, temp_len, NULL);
	if (sd_res == SD_RESULT_OK)
		return DEVICE_CONFIG_RESULT_OK;
	else
	{
		LOG("Failed to write file, error: %d\r\n", sd_res);
		return DEVICE_CONFIG_RESULT_SD_ERROR;
	}

}


/***********************************************************************
 * Function name: deviceConfigGet
 * Description: Load configuration from SD card
 * Parameters : conf - pointer to supported configuration structure
 *
 * Returns :
 *		DEVICE_CONFIG_RESULT_OK - on success
 *		DEVICE_CONFIG_RESULT_E - for other failures
 *
 ***********************************************************************/
DEVICE_CONFIG_RESULT_E deviceConfigGet(DEVICE_CONFIG_STRUCT * conf)
{
	if (conf == NULL)
		return DEVICE_CONFIG_RESULT_WRONG_PARAM;

	SD_RESULT_E sd_res;
	char temp[DEVICE_CONFIG_MAX_FILE_SIZE];
	uint16_t temp_len;
	uint16_t curr_count;

	// reset to defaults
	conf->device_mode = DEVICE_CONFIG_DEFAULT_DEVICE_MODE;
	conf->pace_maker_enable = DEVICE_CONFIG_DEFAULT_PACE_MAKER_EN;


	//check if LOG directory exists
	sd_res = sdDirExist(DEVICE_CONFIG_LOG_DIRECTORY_NAME);
	if (sd_res == SD_RESULT_ERROR_DIRECTORY_DOES_NOT_EXIST)
	{
		if (sdMakeDir(DEVICE_CONFIG_LOG_DIRECTORY_NAME) != SD_RESULT_OK)
			LOG_ERR("Failed to create LOG directory\r\n");			
	}	
	
	//check if directory exists
	sd_res = sdDirExist(DEVICE_CONFIG_DIRECTORY_NAME);
	if (sd_res == SD_RESULT_ERROR_DIRECTORY_DOES_NOT_EXIST)
	{
		if (sdMakeDir(DEVICE_CONFIG_DIRECTORY_NAME) != SD_RESULT_OK)
			LOG_ERR("Failed to create CONFIG directory\r\n");			
	}
		
	// check if file exists
	sd_res = sdFileExist(DEVICE_CONFIG_FILE_NAME);
	if (sd_res == SD_RESULT_ERROR_FILE_DOES_NOT_EXIST)
	{
		temp_len = sprintf(temp, "%s=%c\r\n%s=%c", DEVICE_CONFIG_ATTR_DEV_MODE,
				DEVICE_CONFIG_DEV_MODE_PARAM_HOLTER,
				DEVICE_CONFIG_ATTR_PACE_MAKER,
				DEVICE_CONFIG_PACE_MAKER_PARAM_OFF
				);

		// create and save new file with defaults
		sd_res = sdWriteDataToFile(DEVICE_CONFIG_FILE_NAME, (uint8_t*)temp, temp_len, NULL);

		if (sd_res == SD_RESULT_OK)
			return DEVICE_CONFIG_RESULT_NEW_FILE_CREATED_WITH_DEFAULTS;
		else
		{
			LOG("Failed to write file, error: %d\r\n", sd_res);
			return DEVICE_CONFIG_RESULT_SD_ERROR;
		}
	}
   

   // file does exist, read
	sd_res = sdReadDataFromFile(DEVICE_CONFIG_FILE_NAME, DEVICE_CONFIG_MAX_FILE_SIZE, DEVICE_CONFIG_DATA_OFFSET, (uint8_t *)&temp, &temp_len);
	if (sd_res != SD_RESULT_OK)
	{
		LOG("Failed to read file, error: %d\r\n", sd_res);
		return  DEVICE_CONFIG_RESULT_SD_ERROR;
	}
   
	// init for parsing and error handling (missing params)
	memset(conf, 0xff, sizeof(DEVICE_CONFIG_STRUCT));
	sd_res = SD_RESULT_OK;

   // parse
	for (int16_t i = 0; i<temp_len;)
	{
		if (prvDeviceConfigParseElement(conf, (uint8_t*)&temp[i], &curr_count) == DEVICE_CONFIG_RESULT_OK)
		{
			i += curr_count;
		}
		else
		{
			i++;
		}
	}

	// add default value to file (if absent)
	if (conf->device_mode >= ECG_GENERAL_CONFIG_DEVICE_MODE_MAX)
	{
		conf->device_mode = DEVICE_CONFIG_DEFAULT_DEVICE_MODE;
		temp_len = sprintf(temp, "\r\n%s=%c", DEVICE_CONFIG_ATTR_DEV_MODE, DEVICE_CONFIG_DEV_MODE_PARAM_HOLTER);
		sd_res = sdWriteDataToFile(DEVICE_CONFIG_FILE_NAME, (uint8_t*)temp, temp_len, NULL);
	}

	if (conf->pace_maker_enable >= ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_MAX)
	{
		conf->pace_maker_enable = DEVICE_CONFIG_DEFAULT_PACE_MAKER_EN;
		temp_len = sprintf(temp, "\r\n%s=%c", DEVICE_CONFIG_ATTR_PACE_MAKER, DEVICE_CONFIG_PACE_MAKER_PARAM_OFF);
		sd_res |= sdWriteDataToFile(DEVICE_CONFIG_FILE_NAME, (uint8_t*)temp, temp_len, NULL);
	}

	if (sd_res != SD_RESULT_OK)
	{
		LOG("Failed to write file, error: %d\r\n", sd_res);
		return DEVICE_CONFIG_RESULT_SD_ERROR;
	}

	return  DEVICE_CONFIG_RESULT_OK;
}


