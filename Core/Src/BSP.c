
#include "BSP.h"

#include "common.h"
#include "global_defs.h"
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
#include "fatfs.h"
#include "stm32l4xx_hal_tim.h"
#include "rtc-rv3028.h"
#include "usb_device.h"



/****************************************************************/
/*						Local Definitions						*/
#define BSP_HW_VOLTAGE							372 //HW Version 1
				
//#define PM_WIDTH_MIN_uS							80
//#define PM_WIDTH_MAX_uS							2200
//				
//#define PM_TIMER_MAX							0xFFFF
//#define SYS_CLOCK_FREQ  						80000000UL
//#define TIM_CLOCK_FREQ  						1000000
//#define uSEC_PER_SEC							1000000
//#define uSEC_PER_TICK 							(uSEC_PER_SEC/TIM_CLOCK_FREQ)
//#define PM_WIDTH_MIN_TICKS						80 		// [MARK changed] ( PM_WIDTH_MIN_uS / uSEC_PER_TICK )
//#define PM_WIDTH_MAX_TICKS						2200	// [MARK changed] ( PM_WIDTH_MAX_uS / uSEC_PER_TICK )
#define PM_WIDTH_MIN_uS		80
#define PM_WIDTH_MAX_uS		2200

#define PM_TIMER_MAX		0xFFFF
#define SYS_CLOCK_FREQ  	80000000UL
#define TIM_CLOCK_FREQ  	10000000
#define uSEC_PER_SEC		1000000
#define uSEC_PER_TICK 		(uSEC_PER_SEC/TIM_CLOCK_FREQ)
//#define PM_WIDTH_MIN_TICKS	(PM_WIDTH_MIN_uS/uSEC_PER_TICK)
//#define PM_WIDTH_MAX_TICKS	(PM_WIDTH_MAX_uS/uSEC_PER_TICK)

#define PM_WIDTH_MIN_TICKS	800
#define PM_WIDTH_MAX_TICKS	22000



#define ADC_MUTEX_TIMEOUT						2000
#define ADC_TEMP_SENSOR_30						30
#define ADC_TEMP_SENSOR_110						110
#define ADC_TEMP_SENSOR_CAL1_30_ADDR			0x1FFF75A8
#define ADC_TEMP_SENSOR_CAL2_110_ADDR			0x1FFF75AA

#define SEC_PER_MIN								60
#define SEC_PER_HOUR							(SEC_PER_MIN * 60)
#define SEC_PER_DAY								(24 * SEC_PER_HOUR)
#define BSP_BUTTON_PRESSED_DEBOUNCE_TIME_MSEC	500

#define DMA_BUF_SIZE        					50      /* DMA circular buffer size in bytes */
#define DMA_TIMEOUT_MS      					1      /* DMA Timeout duration in msec */


typedef struct
{
    volatile uint8_t  flag;     /* Timeout event flag */
    uint16_t timer;             /* Timeout duration in msec */
    uint16_t prevCNDTR;         /* Holds previous value of DMA_CNDTR */
} DMA_Event_t;


/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
//SPI_HandleTypeDef hspi_ppg;
static SPI_HandleTypeDef 							hspi_ecg;
static SPI_HandleTypeDef 							hspi_accel;
static DMA_HandleTypeDef 							hdma_spi_ecg_rx;
static DMA_HandleTypeDef 							hdma_spi_ecg_tx;
static DMA_HandleTypeDef 							hdma_spi_accel_rx;
static DMA_HandleTypeDef 							hdma_spi_accel_tx;
static ADC_HandleTypeDef 							hadc1;
SD_HandleTypeDef  							hsd1;
static RTC_HandleTypeDef 							hrtc;
static ADC_HandleTypeDef 							hadc3;
static UART_HandleTypeDef 							huart1;
static UART_HandleTypeDef 							huart2;
static TIM_HandleTypeDef    						timer_handler_pace_maker;
static DMA_HandleTypeDef 							hdma_usart2_rx;
static DMA_HandleTypeDef 							hdma_usart2_tx;
static DMA_HandleTypeDef 							hdma_usart1_rx;
static DMA_HandleTypeDef 							hdma_usart1_tx;
static osMutexId 									i2cMutex;
static osMutexId 									bspFlashMutex;
static osMutexId 									bspAdcMutex;
static I2C_HandleTypeDef 							hi2c1;
I2C_HandleTypeDef hi2c2;
COMP_HandleTypeDef							hcomp2;
static DAC_HandleTypeDef							hdac1;
//static 										uint8_t shutdown_enable = TRUE;								
static uint8_t 										bsp_hw_ver = 0; //TODO: first HW version
static osTimerId 									bsp_button_timer;
		
static TIM_HandleTypeDef    						timer_handler_pace_maker;
static TIM_HandleTypeDef    						timer_handler_impedance;

static DMA_Event_t 									dma_uart_rx = {0,0,DMA_BUF_SIZE};
		
static uint8_t 										dma_rx_buf[DMA_BUF_SIZE];       /* Circular buffer for DMA */
static uint8_t 										data[DMA_BUF_SIZE] = {'\0'};    /* Data buffer that contains newly received data */
#ifdef IWDG_ENABLE	// @ BSP.h
IWDG_HandleTypeDef   								IwdgHandle;
osThreadId 											wdgTaskHandle;
static uint8_t 										watchdog_reset_flag = 0;
static uint32_t 									watchdoc_count = 0;
#endif

#ifdef G_UNIT_TEST_ENABLE
uint8_t bsp_unit_test_enable;
#endif //G_UNIT_TEST_ENABLE

uint32_t BSP_GPIO_ARRAY[BSP_PIN_TYPE_MAX_USED][3]=
{
			{BSP_PIN_TYPE_ECG1_CS,              (uint32_t)ECG1_CS_PORT, 		ECG1_CS_PIN},		//BSP_PIN_TYPE_ECG1_CS,
			{BSP_PIN_TYPE_ECG2_CS,              (uint32_t)ECG2_CS_PORT, 		ECG2_CS_PIN},		//BSP_PIN_TYPE_ECG2_CS,
			{BSP_PIN_TYPE_ECG_START,            (uint32_t)ECG_START_PORT, 		ECG_START_PIN},		//BSP_PIN_TYPE_ADS_START,
			{BSP_PIN_TYPE_ECG1_DRDY,            (uint32_t)ECG1_DRDY_PORT, 		ECG1_DRDY_PIN},		//BSP_PIN_TYPE_ECG1_DRDY,
			{BSP_PIN_TYPE_ECG2_DRDY,            (uint32_t)ECG2_DRDY_PORT, 		ECG2_DRDY_PIN},		//BSP_PIN_TYPE_ECG2_DRDY,
			{BSP_PIN_TYPE_ECG1_RESET,           (uint32_t)ECG1_RESET_PORT, 		ECG1_RESET_PIN},	//BSP_PIN_TYPE_ECG1_RESET,
			{BSP_PIN_TYPE_ECG2_RESET,           (uint32_t)ECG2_RESET_PORT, 		ECG2_RESET_PIN},	//BSP_PIN_TYPE_ECG2_RESET,
			{BSP_PIN_TYPE_LED_RED,              (uint32_t)LED_RED_PORT, 		LED_RED_PIN},		//BSP_PIN_TYPE_LED_RED,
			{BSP_PIN_TYPE_LED_BLUE,             (uint32_t)LED_BLUE_PORT, 		LED_BLUE_PIN},		//BSP_PIN_TYPE_LED_BLUE,
			{BSP_PIN_TYPE_BLE_RESET,            (uint32_t)BLE_RESET_PORT, 		BLE_RESET_PIN},		//BSP_PIN_TYPE_BLE_RESET,
			{BSP_PIN_TYPE_BLE_UART_EN,          (uint32_t)BLE_UART_EN_PORT, 	BLE_UART_EN_PIN},	//BSP_PIN_TYPE_BLE_UART_EN,
			{BSP_PIN_TYPE_BLE_AT_COMM_EN,       (uint32_t)BLE_AT_COMM_EN_PORT, BLE_AT_COMM_EN_PIN},//BSP_PIN_TYPE_BLE_AT_COMM_EN
			{BSP_PIN_TYPE_ACCEL_CS,             (uint32_t)ACCEL_CS_PORT, 		ACCEL_CS_PIN},		//BSP_PIN_TYPE_ACCEL_CS,
			{BSP_PIN_TYPE_SDMMC1_EN,            (uint32_t)SDMMC1_EN_PORT, 		SDMMC1_EN_PIN},		//BSP_PIN_TYPE_SDMMC1_EN,
			{BSP_PIN_TYPE_BAT_LOAD_EN,          (uint32_t)BAT_LOAD_EN_PORT, 	BAT_LOAD_EN_PIN},	//BSP_PIN_TYPE_BAT_LOAD_EN,
			{BSP_PIN_TYPE_SDCARD_IN,            (uint32_t)SDCARD_IN_PORT, 		SDCARD_IN_PIN},		//BSP_PIN_TYPE_SDCARD_IN
			{BSP_PIN_TYPE_PM,                   (uint32_t)PM_TIM2_CH4_PORT, 	PM_TIM2_CH4_PIN},	//BSP_PIN_TYPE_PM
			{BSP_PIN_TYPE_PM_OFF,               (uint32_t)PM_OFF_PORT, 		   	PM_OFF_PIN},		//BSP_PIN_TYPE_PM_OFF,
			{BSP_PIN_TYPE_ACCEL_INT1,           (uint32_t)ACCEL_INT1_PORT,		ACCEL_INT1_PIN},	//BSP_PIN_TYPE_ACCEL_INT1,
			{BSP_PIN_TYPE_ACCEL_INT2,           (uint32_t)ACCEL_INT2_PORT,		ACCEL_INT2_PIN},	//BSP_PIN_TYPE_ACCEL_INT2,
			{BSP_PIN_TYPE_IMP_LL_SWITCH,        (uint32_t)IMP_LL_ON_PORT,		IMP_LL_ON_PIN},     //BSP_PIN_TYPE_IMP_LL_SWITCH,
         	{BSP_PIN_TYPE_IMP_LA_SWITCH,        (uint32_t)IMP_LA_ON_PORT,		IMP_LA_ON_PIN},     //BSP_PIN_TYPE_IMP_LA_SWITCH,
         	{BSP_PIN_TYPE_IMP_RA_SWITCH,        (uint32_t)IMP_RA_ON_PORT,		IMP_RA_ON_PIN},     //BSP_PIN_TYPE_IMP_RA_SWITCH,
			{BSP_PIN_TYPE_BUTTON,		        (uint32_t)BUTTON_INT_PORT,		BUTTON_INT_PIN},    //BSP_PIN_TYPE_BUTTON,
			{BSP_PIN_TYPE_TP1,                  (uint32_t)TP1_PORT, 			TP1_PIN},			//BSP_PIN_TYPE_TP1,
			{BSP_PIN_TYPE_TP2,                  (uint32_t)TP2_PORT, 			TP2_PIN},			//BSP_PIN_TYPE_TP2,
			{BSP_PIN_TYPE_SD_PWR_ON,			(uint32_t)SD_PWR_ON_PORT,		SD_PWR_ON_PIN}		//BSP_PIN_TYPE_SD_PWR_ON
};

/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/
/*static*/ BSP_RESULT_E SystemClock_Config(void);
static BSP_RESULT_E MX_GPIO_Init(void);
static BSP_RESULT_E MX_DMA_Init(void);
static BSP_RESULT_E MX_SPI_ECG_Init(void);
static BSP_RESULT_E MX_SPI_ACCEL_Init(void);
static BSP_RESULT_E MX_ADC1_Init(void);
static BSP_RESULT_E MX_ADC3_Init(void);
static BSP_RESULT_E MX_SDMMC1_SD_Init(void);
static BSP_RESULT_E MX_RTC_Init(void);
static BSP_RESULT_E MX_COMP2_Init(void);
static BSP_RESULT_E MX_DAC1_Init(void);
static BSP_RESULT_E MX_IWDG_Init(uint32_t prescaler);
//static BSP_RESULT_E	prvBspButtonPressedDebouncer(uint8_t dc_in);
static BSP_RESULT_E prvBspSetHwRev(void);
static BSP_RESULT_E prvBspEnableInterrupts(void);
static void MX_I2C2_Init(void);
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/

/****************************************************************/

/****************************************************************/
/*						Weak Functions							*/
__weak void bspButtonEvent(BSP_BUTTON_EVENT_E button_status){}
__weak void bsp_bat_mon_dc_in_callback(uint8_t dc_in){}
__weak void bsp_ads1_irq_callback(){}
__weak void bsp_ads2_irq_callback(){}
__weak void bspAdsSpiCmpltCallback(uint8_t * rxData, uint16_t size){}
__weak void bspAccelSpiCmplt(uint8_t * rxData, uint16_t size){}
__weak void bsp_uart_debug_tx_cplt_callback(){}
__weak void bspUartBleTxCpltCallback(){}
__weak void bsp_uart_debug_rx_cplt_callback(){}
__weak void bspUartBleRxCpltCallback(void){}
__weak void bsp_uart_ble_rx_ind(){}
__weak void bspUartErrorCallback(uint32_t hal_err_code){}
__weak void bspUartBleErrorCallback(uint32_t hal_err_code){}
__weak void bspBleStatus(uint8_t ble_status){}
__weak void pmEvent(bsp_pm_event_s * event) {}
__weak void bspImpedanceAdcResultReadyCallback(){}
__weak void bspSerialCallback(uint8_t * ptr, uint16_t length){}
__weak void bspAdcReadCompleteCallbackImpedance(uint32_t read_result){}
__weak void bspAdcReadCompleteCallbackTemperature(uint32_t read_result){}	// TODO: not implemented yet
__weak void bspAdcReadCompleteCallbackBattery(uint32_t read_result){}	// TODO: not implemented yet
__weak void bspCompBatteryLowCallback(){}
#ifdef G_UNIT_TEST_ENABLE
// Redirection of data read after interrupts, to avoid further data processing, user should override and return true
__weak uint8_t bspAdsSpiCmpltCallbackRedirect(uint8_t * rxData, uint16_t size) { return 0;}
__weak uint8_t bspPpgSpiCmpltCallbackRedirect(uint8_t * rxData, uint16_t size) { return 0;}
#endif //G_UNIT_TEST_ENABLE
/****************************************************************/

//static void prvBspButtonPressedDebouncerCallback(void const *n);

//osTimerDef(bsp_button_timer, prvBspButtonPressedDebouncerCallback);


void Error_Handler()
{
	while(1);
}
//void HAL_PWR_PVDCallback(void)
//{
	
//	uint8_t tmp = READ_BIT(PWR->SR2, PWR_SR2_PVDO);
//}

//void PVD_Init(void)
//{
//	PWR_PVDTypeDef sConfigPVD;
//	sConfigPVD.PVDLevel = PWR_PVDLEVEL_6; /*! & lt; PVD threshold around 2.8 V */
//	sConfigPVD.Mode = PWR_PVD_MODE_IT_RISING_FALLING;
//	HAL_PWR_ConfigPVD(&sConfigPVD);
//	HAL_PWR_EnablePVD();
//}
/***********************************************************************
 * Function name: bspInit
 * Description: BSP init
 * Parameters :
 *		None
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - Mutex isn't created
 ***********************************************************************/
BSP_RESULT_E bspInit()
{
	BSP_RESULT_E res = BSP_RESULT_OK;

	HAL_Init();
	/* Configure the system clock */
	res |= SystemClock_Config();

	  //BSP_TEST_IOs();

	/* Initialize all configured peripherals */
	res |= MX_GPIO_Init();
	res |= MX_DMA_Init();

	res |= MX_SPI_ECG_Init();
//#ifdef ACCEL_ENABLED
//	res |= MX_SPI_ACCEL_Init();
//#endif //ACCEL_ENABLED
	res |= MX_SDMMC1_SD_Init();
	MX_I2C2_Init();
	res |= MX_RTC_Init();	
	// res |= MX_IWDG_Init(IWDG_PRESCALER_128);	
	res |= MX_COMP2_Init();

	res |= MX_DAC1_Init();
	//MX_FATFS_Init();	

	//MX_USB_DEVICE_Init();
   

	if (res != BSP_RESULT_OK)
		return BSP_RESULT_ERROR;
	
	osMutexDef(bspFlashMutex);
	bspFlashMutex = osMutexCreate (osMutex(bspFlashMutex));
	if (!bspFlashMutex)
		return BSP_RESULT_ERROR;
	
	osMutexDef(bspAdcMutex);
	bspAdcMutex = osMutexCreate (osMutex(bspAdcMutex));
	if (!bspAdcMutex)
		return BSP_RESULT_ERROR;
//	bsp_button_timer = osTimerCreate(osTimer(bsp_button_timer), osTimerOnce, (void *)0);

	res |= prvBspSetHwRev();

	res |= prvBspEnableInterrupts();
	
	if (res != BSP_RESULT_OK)
		return BSP_RESULT_ERROR;

	return BSP_RESULT_OK;
}

//#define RCC_CLOCK_80_MHZ
#define RCC_CLOCK_40_MHZ
//#define RCC_CLOCK_5_MHZ
//#define RCC_CLOCK_2_5_MHZ

void SystemClock_Decrease(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};

  /* MSI is enabled in range 0 (100Khz) */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_5;//RCC_MSIRANGE_0;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
  
  /* Select MSI as system clock source and keep HCLK, PCLK1 and PCLK2 clocks dividers as before */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI; 
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
  
  /* Disable HSI to reduce power consumption since MSI is used from that point */
  __HAL_RCC_HSI_DISABLE();
  
}

BSP_RESULT_E SystemClock_Config_80Mhz(void)
{
	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 40;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV16;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV16;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_SDMMC1
			| RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 25;	//16;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure LSE Drive Capability
	 */
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

	return BSP_RESULT_OK;
}

BSP_RESULT_E SystemClock_Config_40Mhz(void)
{

	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 20;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV8;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_SDMMC1
			| RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 25;	//16;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure LSE Drive Capability
	 */
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

	return BSP_RESULT_OK;
}


BSP_RESULT_E SystemClock_Config_5Mhz(void)
{

	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 20;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_SDMMC1
			| RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 25;	//16;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure LSE Drive Capability
	 */
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

	return BSP_RESULT_OK;
}

BSP_RESULT_E SystemClock_Config_4Mhz_TEST(void)
{

	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;//RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 20;//16;//24;-3MHz//16;-4MHz//20;-5MHz
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV8;//RCC_PLLR_DIV4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;//RCC_SYSCLK_DIV4;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_SDMMC1
			| RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;//RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 25;	//16;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;//RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure LSE Drive Capability
	 */
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

	return BSP_RESULT_OK;
}

BSP_RESULT_E SystemClock_Config_16Mhz(void){
	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 3;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_I2C2|RCC_PERIPHCLK_USB
                              |RCC_PERIPHCLK_SDMMC1|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_HSI48;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 3;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 25;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  
  /**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
  
  return BSP_RESULT_OK;
}

BSP_RESULT_E SystemClock_Config_4Mhz(void)
{

	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 16;//24;-3MHz//16;-4MHz//20;-5MHz
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV8;//RCC_PLLR_DIV4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;//RCC_SYSCLK_DIV4;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_SDMMC1
			| RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 25;	//16;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;//RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure LSE Drive Capability
	 */
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

	return BSP_RESULT_OK;
}

BSP_RESULT_E SystemClock_Config_3_5Mhz(void)
{

	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 28;//-3.5MHz//24;-3MHz//16;-4MHz//20;-5MHz
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV8;//RCC_PLLR_DIV4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_SDMMC1
			| RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 25;	//16;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;//RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure LSE Drive Capability
	 */
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

	return BSP_RESULT_OK;
}

BSP_RESULT_E SystemClock_Config_3Mhz(void)
{

	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 24;//-3MHz//28;//-3.5MHz//24;-3MHz//16;-4MHz//20;-5MHz
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV8;//RCC_PLLR_DIV4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_SDMMC1
			| RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 25;	//16;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;//RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure LSE Drive Capability
	 */
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

	return BSP_RESULT_OK;
}

BSP_RESULT_E SystemClock_Config_2Mhz(void)
{

	HAL_RCC_DeInit();

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 16;//24;-3MHz//16;-4MHz//20;-5MHz
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV8;//RCC_PLLR_DIV4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_SDMMC1
			| RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 25;	//16;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;//RCC_PLLQ_DIV4;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK | RCC_PLLSAI1_ADC1CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure LSE Drive Capability
	 */
	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

	return BSP_RESULT_OK;
}



#ifdef RCC_CLOCK_40_MHZ
BSP_RESULT_E SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 3;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV8;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_I2C2|RCC_PERIPHCLK_USB
                              |RCC_PERIPHCLK_SDMMC1|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_HSI48;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 3;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 25;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  return BSP_RESULT_OK;
}

#if 0 
// 40 Mhz
/*static*/ BSP_RESULT_E SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks
*/
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV8;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

  PeriphClkInit.PeriphClockSelection = 
    RCC_PERIPHCLK_RTC
//#ifndef USB_MODE 
    |RCC_PERIPHCLK_USART1
    |RCC_PERIPHCLK_USART2
//#endif
    |RCC_PERIPHCLK_SDMMC1
    |RCC_PERIPHCLK_ADC
    |RCC_PERIPHCLK_I2C2
    |RCC_PERIPHCLK_USB;
//#ifndef USB_MODE 
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
//#endif
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
  PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 25;//16;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = 
//#ifndef USB_MODE
    RCC_PLLSAI1_48M2CLK
//#endif
    |RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure LSE Drive Capability
    */
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Configure the main internal regulator output voltage
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

  return BSP_RESULT_OK;
}
#endif

#elif defined (RCC_CLOCK_5_MHZ) 
// 5 MHz
static BSP_RESULT_E SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_SDMMC1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 25;//16;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK|RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure LSE Drive Capability
    */
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Configure the main internal regulator output voltage
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

  return BSP_RESULT_OK;
}

#elif defined (RCC_CLOCK_2_5_MHZ) 
// 2.5 MHz
//SYSCLK 10MHz
static BSP_RESULT_E SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV8; //RCC_PLLR_DIV4; //To reduce SYSCLK 20->10
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_SDMMC1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_SYSCLK;//RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 25;//16;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK|RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure LSE Drive Capability
    */
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Configure the main internal regulator output voltage
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

  return BSP_RESULT_OK;
}


#elif defined (RCC_CLOCK_80_MHZ)

/** System Clock Configuration
*/
static BSP_RESULT_E SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV8;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV16;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV16;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_SDMMC1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 6;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 25;//16;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK|RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure LSE Drive Capability 
    */
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
  
  return BSP_RESULT_OK;
}

#endif


//static
BSP_RESULT_E  bspUart1Init(void)
{
  huart1.Instance = USART1;
#ifdef MENNEN_ALGO
  huart1.Init.BaudRate = G_UART_BAUD_RATE_MENNEN;
#else	//MENNEN_ALGO
  huart1.Init.BaudRate = G_UART_BAUD_RATE;
#endif	//MENNEN_ALGO
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_8;

//  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
//  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
//

  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
	return BSP_RESULT_ERROR;
  }

  return BSP_RESULT_OK;
}

void bspUart1Uninit(void)
{
	HAL_UART_DeInit(&huart1);
}

BSP_RESULT_E bspUart2Init(uint32_t baud_rate, uint8_t flow_control)
{

	huart2.Instance = USART2;
	huart2.Init.BaudRate = baud_rate;//G_UART_BAUD_RATE;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	if (flow_control)
		huart2.Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
	else
		huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_8;
	huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	return BSP_RESULT_OK;

}

void bspUart2Uninit(void)
{
	HAL_UART_DeInit(&huart2);
}

BSP_RESULT_E bspUart1Valid(void)
{
	GPIO_InitTypeDef 	GPIO_InitStruct;
	GPIO_PinState		pin_state;
	
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	pin_state = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9);
	
	return (pin_state == GPIO_PIN_SET ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}

/* SPI1 init function */
static BSP_RESULT_E MX_SPI_ECG_Init(void)
{

  hspi_ecg.Instance = SPI1;
  hspi_ecg.Init.Mode = SPI_MODE_MASTER;
  hspi_ecg.Init.Direction = SPI_DIRECTION_2LINES;
  hspi_ecg.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi_ecg.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi_ecg.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi_ecg.Init.NSS = SPI_NSS_SOFT;
  hspi_ecg.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;//SPI_BAUDRATEPRESCALER_256;  // -> 292 Kbit/s (Prizma1 was 375 kBit/s)
  hspi_ecg.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi_ecg.Init.TIMode = SPI_TIMODE_DISABLED;
  hspi_ecg.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;
  hspi_ecg.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
//  hspi1.Init.CRCPolynomial = 7;
//  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;  
  if (HAL_SPI_Init(&hspi_ecg) != HAL_OK)
	  return BSP_RESULT_ERROR;

  return BSP_RESULT_OK;

}

static BSP_RESULT_E MX_SPI_ACCEL_Init(void)
{

  hspi_accel.Instance = SPI2;
  hspi_accel.Init.Mode = SPI_MODE_MASTER;
  hspi_accel.Init.Direction = SPI_DIRECTION_2LINES;
  hspi_accel.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi_accel.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi_accel.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi_accel.Init.NSS = SPI_NSS_SOFT;
  hspi_accel.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;//SPI_BAUDRATEPRESCALER_4;
  hspi_accel.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi_accel.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi_accel.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi_accel.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  
  if (HAL_SPI_Init(&hspi_accel) != HAL_OK)
	return BSP_RESULT_ERROR;

  return BSP_RESULT_OK;
}

/**
  * Enable DMA controller clock
  */
static BSP_RESULT_E MX_DMA_Init(void)
{
	/* DMA controller clock enable */
	__DMA1_CLK_ENABLE();
	__DMA2_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA1_Channel4_IRQn interrupt configuration */
  	//UART1 TX
//	HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 5, 0);
//	HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
	
	//HAL_NVIC_SetPriority(DMA2_Channel6_IRQn, 5, 0);
	//HAL_NVIC_EnableIRQ(DMA2_Channel6_IRQn);	
	
	/* DMA1_Channel5_IRQn interrupt configuration */
	//UART1 RX
//	HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 5, 0);
//	HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

	//HAL_NVIC_SetPriority(DMA2_Channel7_IRQn, 5, 0);
	//HAL_NVIC_EnableIRQ(DMA2_Channel7_IRQn);

  	//UART2 TX
	HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

	/* DMA1_Channel5_IRQn interrupt configuration */
	//UART2 RX
	HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);	
	
	/* DMA1_Channel2_IRQn interrupt configuration */
	//SPI1 RX
	HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
	/* DMA1_Channel3_IRQn interrupt configuration */
	//SPI1 TX
	HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);   
	/* DMA1_Channel4_IRQn interrupt configuration */

	  /* DMA2_Channel4_IRQn interrupt configuration */
  //SDMMC1_RX
  //HAL_NVIC_SetPriority(DMA2_Channel4_IRQn, 5, 0);
  //HAL_NVIC_EnableIRQ(DMA2_Channel4_IRQn);

  /* DMA2_Channel5_IRQn interrupt configuration */
  //SDMMC1_TX
  //HAL_NVIC_SetPriority(DMA2_Channel5_IRQn, 5, 0);
  //HAL_NVIC_EnableIRQ(DMA2_Channel5_IRQn);
   
   
   // ACCELEROMETER DMA Channels
   
	//SPI2 RX
//	HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 5, 0);
//	HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
//	/* DMA1_Channel5_IRQn interrupt configuration */
//	//SPI2 TX
//	HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 5, 0);
//	HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

  return BSP_RESULT_OK;
}

static BSP_RESULT_E MX_SDMMC1_SD_Init(void)
{

  hsd1.Instance = SDMMC1;
  hsd1.Init.ClockEdge = SDMMC_CLOCK_EDGE_RISING;
  hsd1.Init.ClockBypass = SDMMC_CLOCK_BYPASS_DISABLE;
  hsd1.Init.ClockPowerSave = SDMMC_CLOCK_POWER_SAVE_ENABLE;//SDMMC_CLOCK_POWER_SAVE_DISABLE;
  hsd1.Init.BusWide = SDMMC_BUS_WIDE_1B;
  hsd1.Init.HardwareFlowControl = SDMMC_HARDWARE_FLOW_CONTROL_ENABLE;//SDMMC_HARDWARE_FLOW_CONTROL_DISABLE;
  hsd1.Init.ClockDiv = 1;//4;
  
  return BSP_RESULT_OK;
}

static BSP_RESULT_E MX_RTC_Init(void)
{

    /**Initialize RTC Only 
    */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
	  return BSP_RESULT_ERROR;
  }
	  return BSP_RESULT_OK;

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/

static BSP_RESULT_E MX_GPIO_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__GPIOA_CLK_ENABLE();
	__GPIOB_CLK_ENABLE();
	__GPIOC_CLK_ENABLE();  
	__GPIOD_CLK_ENABLE();
	__GPIOE_CLK_ENABLE();
	__GPIOF_CLK_ENABLE();
        __GPIOH_CLK_ENABLE();

//  GPIO_InitStruct.Pin = GPIO_PIN_All;
//  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
//  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
//  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
//  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	/*Configure GPIO pins : PE12 ECG1_CS */
	GPIO_InitStruct.Pin = ECG1_CS_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(ECG1_CS_PORT, &GPIO_InitStruct);

	bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);	
	
	/*Configure GPIO pins : PA4 ECG2_CS */
	GPIO_InitStruct.Pin = ECG2_CS_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(ECG2_CS_PORT, &GPIO_InitStruct);  

	bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_SET);
	
	/*Configure GPIO pins : PE9 ECG1_RESET */
	GPIO_InitStruct.Pin = ECG1_RESET_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(ECG1_RESET_PORT, &GPIO_InitStruct);

	/*Configure GPIO pins : PE10 ECG2_RESET */
	GPIO_InitStruct.Pin = ECG2_RESET_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(ECG2_RESET_PORT, &GPIO_InitStruct);  

	/*Configure GPIO pins : PE11 ECG_START*/
	GPIO_InitStruct.Pin = ECG_START_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(ECG_START_PORT, &GPIO_InitStruct);

	/*Configure GPIO pin : PE7 ECG1_DATA_READY*/
	GPIO_InitStruct.Pin = ECG1_DRDY_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(ECG1_DRDY_PORT, &GPIO_InitStruct);

	/*Configure GPIO pin : PE8 ECG2_DATA_READY*/
//	GPIO_InitStruct.Pin = ECG2_DRDY_PIN;
//	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
//	GPIO_InitStruct.Pull = GPIO_PULLUP;
//	HAL_GPIO_Init(ECG2_DRDY_PORT, &GPIO_InitStruct);	
//	
	/*Configure GPIO pins : PC0 LED_RED */
	GPIO_InitStruct.Pin = LED_RED_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(LED_RED_PORT, &GPIO_InitStruct);  

	/*Configure GPIO pins : PC1 LED_BLUE */
	GPIO_InitStruct.Pin = LED_BLUE_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(LED_BLUE_PORT, &GPIO_InitStruct);  

	/*Configure GPIO pin : PB0 BLE_RESET*/
	GPIO_InitStruct.Pin = BLE_RESET_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(BLE_RESET_PORT, &GPIO_InitStruct);	

	/*Configure GPIO pin : PB1 BLE_UART_EN*/
	GPIO_InitStruct.Pin = BLE_UART_EN_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(BLE_UART_EN_PORT, &GPIO_InitStruct);	

	/*Configure GPIO pin : PB2 BLE_AT_COMM_EN*/
	GPIO_InitStruct.Pin = BLE_AT_COMM_EN_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(BLE_AT_COMM_EN_PORT, &GPIO_InitStruct);

	/*Configure GPIO pin : PC4 DTM_BLE_RX*/
	GPIO_InitStruct.Pin = DTM_BLE_RX_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(DTM_BLE_RX_PORT, &GPIO_InitStruct);
	
	/*Configure GPIO pin : PC5 DTM_BLE_TX*/
	GPIO_InitStruct.Pin = DTM_BLE_TX_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(DTM_BLE_TX_PORT, &GPIO_InitStruct);	
//#ifndef USB_MODE
	/*Configure GPIO pin : PA11 ACCEL_INT1*/
	/*GPIO_InitStruct.Pin = ACCEL_INT1_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ACCEL_INT1_PORT, &GPIO_InitStruct);	

	/*Configure GPIO pin : PA12 ACCEL_INT2*/
	/*GPIO_InitStruct.Pin = ACCEL_INT2_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ACCEL_INT2_PORT, &GPIO_InitStruct);	

	/*Configure GPIO pin : PB12 ACCEL_CS*/
	/*GPIO_InitStruct.Pin = ACCEL_CS_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ACCEL_CS_PORT, &GPIO_InitStruct);	

	  /*Configure GPIO pin : PB12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	bsp_write_pin(BSP_PIN_TYPE_ACCEL_CS, BSP_PIN_SET);
//#endif
	/*Configure GPIO pin : PC6 BUTTON_INT*/
	GPIO_InitStruct.Pin = BUTTON_INT_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(BUTTON_INT_PORT, &GPIO_InitStruct);	

	/*Configure GPIO pins : PE1 SD_PWR_ON */
	GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);

	bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_RESET);
	
	/*Configure GPIO pin : PD3 SDMMC1_EN*/
	GPIO_InitStruct.Pin = SDMMC1_EN_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SDMMC1_EN_PORT, &GPIO_InitStruct);	
	HAL_GPIO_WritePin(SDMMC1_EN_PORT, SDMMC1_EN_PIN, GPIO_PIN_SET);
	
	/*Configure GPIO pins : PC13 BLE_STATUS */
	GPIO_InitStruct.Pin = BLE_STATUS_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;//GPIO_MODE_IT_RISING;	//GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;//GPIO_PULLDOWN; 	//GPIO_PULLUP;
	HAL_GPIO_Init(BLE_STATUS_PORT, &GPIO_InitStruct);

	/*Configure GPIO pin : PD3 BAT_LOAD_EN*/
	GPIO_InitStruct.Pin = BAT_LOAD_EN_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BAT_LOAD_EN_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin(BAT_LOAD_EN_PORT, BAT_LOAD_EN_PIN, GPIO_PIN_RESET);

	//HAL_GPIO_WritePin(BLE_RST_N_PORT, BLE_RST_N_PIN, GPIO_PIN_RESET);

	/*Configure GPIO pins : PC9 DEBUG PIN */
	GPIO_InitStruct.Pin = GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);	
	
	/*Configure GPIO pins : PB* DEBUG PIN */
	GPIO_InitStruct.Pin = GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);		
	
	/*Configure GPIO pins : PD10 PM power off pin */
	GPIO_InitStruct.Pin = PM_OFF_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(PM_OFF_PORT, &GPIO_InitStruct);
	bsp_write_pin(BSP_PIN_TYPE_PM_OFF, BSP_PIN_RESET);

//	/*Configure GPIO pins : PE1 SD_PWR_ON */
//	GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
//	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//	GPIO_InitStruct.Pull = GPIO_NOPULL;
//	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
//	HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);

//	bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_RESET);
	
	
#ifdef TP_ENABLED
	/*Configure GPIO pins : test points pins */
	GPIO_InitStruct.Pin = TP1_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(TP1_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = TP2_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(TP2_PORT, &GPIO_InitStruct);
#endif


	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);  

	return BSP_RESULT_OK;

}

/* COMP2 init function */
static BSP_RESULT_E MX_COMP2_Init(void)
{

	hcomp2.Instance = COMP2;
	hcomp2.Init.InvertingInput = COMP_INPUT_MINUS_DAC1_CH1;
	hcomp2.Init.NonInvertingInput = COMP_INPUT_PLUS_IO2;
	hcomp2.Init.OutputPol = COMP_OUTPUTPOL_NONINVERTED;
	hcomp2.Init.Hysteresis = COMP_HYSTERESIS_NONE;
	hcomp2.Init.BlankingSrce = COMP_BLANKINGSRC_NONE;
	hcomp2.Init.Mode = COMP_POWERMODE_ULTRALOWPOWER;
	hcomp2.Init.WindowMode = COMP_WINDOWMODE_DISABLE;
	hcomp2.Init.TriggerMode = COMP_TRIGGERMODE_IT_FALLING;//COMP_TRIGGERMODE_IT_RISING_FALLING;

	if (HAL_COMP_Init(&hcomp2) != HAL_OK)
		return BSP_RESULT_ERROR;
	
//	if (HAL_COMP_Start(&hcomp2) != HAL_OK)
//		return BSP_RESULT_ERROR;
	
	return BSP_RESULT_OK;

}

/* DAC1 init function */
static BSP_RESULT_E MX_DAC1_Init(void)
{

	DAC_ChannelConfTypeDef sConfig;

	/**DAC Initialization 
	*/
	hdac1.Instance = DAC1;
	if (HAL_DAC_Init(&hdac1) != HAL_OK)
	{
		return BSP_RESULT_ERROR;
	}

	/**DAC channel OUT1 config 
	*/
	sConfig.DAC_SampleAndHold = DAC_SAMPLEANDHOLD_ENABLE;
	sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_DISABLE;
	sConfig.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_ENABLE;
	sConfig.DAC_UserTrimming = DAC_TRIMMING_FACTORY;
	
	sConfig.DAC_SampleAndHoldConfig.DAC_HoldTime = 1000;
	sConfig.DAC_SampleAndHoldConfig.DAC_RefreshTime = 6;
	sConfig.DAC_SampleAndHoldConfig.DAC_SampleTime = 18;
	
	HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_1, DAC_ALIGN_12B_R, 2035);//1980 - 2.7);//2055 - 2.8V);//2130 - 2.9V);//2209 - 3V);
	
	if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
		return BSP_RESULT_ERROR;
		
	if (HAL_DAC_Start(&hdac1, DAC_CHANNEL_1) != HAL_OK)
		return BSP_RESULT_ERROR;
	
	return BSP_RESULT_OK;

}

#ifdef IWDG_ENABLE
/* IWDG init function */
static BSP_RESULT_E MX_IWDG_Init(uint32_t prescaler)
{
	IwdgHandle.Instance = IWDG;
	IwdgHandle.Init.Prescaler = prescaler;//IWDG_PRESCALER_64;//IWDG_PRESCALER_32;
	IwdgHandle.Init.Window = 0x0FFF;
	IwdgHandle.Init.Reload = 0x0FFF;	
	if (HAL_IWDG_Init(&IwdgHandle) != HAL_OK)
		return BSP_RESULT_ERROR;
	else
		return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bspSetWdTime
 * Description: Set Watchdog time
 * Parameters : time - time to set the watchdog
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspSetWdTime(BSP_WD_TIME_E time)
{
	
	switch (time)
	{
		case BSP_WD_TIME_4_SEC:
		{
			MX_IWDG_Init(IWDG_PRESCALER_32);
		}
		break;

		case BSP_WD_TIME_8_SEC:
		{
			MX_IWDG_Init(IWDG_PRESCALER_64);
		}
		break;
		
		default:
		{
			return BSP_RESULT_ERROR;
		}
		break;
	}
	
	return BSP_RESULT_OK;
}

void wdgTask(void const * argument)
{
	//HAL_StatusTypeDef res;
	IWDG_HandleTypeDef   * hiwdg = &IwdgHandle;

	while (1)
	{
		osDelay(1000);
		//DEBUG_PRINT(DEBUG_LEVEL_1, "\r\n\r\n\r\nWatchdog refresh, flag = %d\r\n", watchdog_reset_flag);
		if (watchdog_reset_flag)
		{
//			LOG("WD Not Set\r\n");
		}
		else
		{
			HAL_IWDG_Refresh(hiwdg);
		}
		watchdoc_count++;
	}
}
/***********************************************************************
 * Function name: bspWdInit
 * Description: Init watchdog task
 * Parameters : 
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspWdInit(void)
{
	osThreadDef(wdgTask, wdgTask, osPriorityLow , 0, WD_STACK_SIZE);
	wdgTaskHandle = osThreadCreate(osThread(wdgTask), NULL);
	
	if (wdgTaskHandle != NULL)
		return BSP_RESULT_OK;
	else
		return BSP_RESULT_ERROR;
}
#else	//IWDG_ENABLE
static BSP_RESULT_E MX_IWDG_Init(uint32_t prescaler)
{
	return BSP_RESULT_OK;
}
#endif

/***********************************************************************
 * Function name: bsp_enable_button
 * Description: enable button's interrupt
 * Parameters :
 *		enable - enable/disable to button interrupt
 *
 * Returns :
 *		BSP_RESULT_s
OK - on success
 ***********************************************************************/
BSP_RESULT_E bsp_enable_button(BOOL enable)
{
	if (enable == TRUE)
	{
		HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
		HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
	}
	else
		HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);

	return BSP_RESULT_OK;
}

BOOL button_pressed = FALSE;

/***********************************************************************
 * Function name: bsp_button_pressed_callback
 * Description: callback that need implementation
 * set parameter :"button_pressed" to true
 * Parameters :
 *		enable -
  * Returns :
 *		BSP_RESULT_OK - on success
 ***********************************************************************/
BSP_RESULT_E bsp_button_pressed_callback(IN BOOL enable)
{
	//bspButtonEvent();
	button_pressed = TRUE;

	return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bsp_check_button
 * Description: check status of parameter "button_pressed" and if true set to false
 * Parameters :
 *		None
 * Returns :
 *		BSP_RESULT_OK - on success
 ***********************************************************************/
BSP_RESULT_E bsp_check_button(void)
{
	if (button_pressed == TRUE)
	{
		//bspButtonEvent();
		button_pressed = FALSE;
	}

	return BSP_RESULT_OK;
}


// mutex protected call for i2c (common for few peripherals)
/***********************************************************************
 * Function name: bsp_i2c_mem_read
 * Description: Read an amount of data in blocking mode from a specific memory address
 * Synchronized function (mutex)
 * Parameters :
 * 		i2c_addr - Target device address
 * 		reg_addr - Internal memory address
 * 		reg_add_size - Size of internal memory address
 * 		buff - Pointer to data buffer
 * 		len - Amount of data to be sent *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - length 0 or data is NULL
 *		BSP_RESULT_ERROR_ALREADY_RUNNING - Mutex is locked
 *		BSP_RESULT_ERROR - Mutex isn't released or message not received
 ***********************************************************************/
BSP_RESULT_E bsp_i2c_mem_read(
		IN uint8_t 		i2c_addr,
		IN uint8_t 		reg_addr,
		IN uint8_t 		reg_add_size,
		IN uint16_t 	len,
		OUT uint8_t* 	buff)
{
	if (buff == NULL || len == 0)
		return BSP_RESULT_BAD_COMMAND;

	if (osMutexWait(i2cMutex,osWaitForever) != osOK)
		return BSP_RESULT_ERROR_ALREADY_RUNNING;

	HAL_StatusTypeDef res = HAL_I2C_Mem_Read(&hi2c1, i2c_addr, reg_addr, reg_add_size, buff, len, 100);

	if (osMutexRelease(i2cMutex) != osOK)
		return BSP_RESULT_ERROR;

	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bsp_i2c_mem_write
 * Description: Write an amount of data in blocking mode to a specific memory address.
 * Synchronized function (mutex)
 * Parameters :
 * 		i2c_addr - Target device address
 * 		reg_addr - Internal memory address
 * 		reg_add_size - Size of internal memory address
 * 		buff - Pointer to data buffer
 * 		len - Amount of data to be sent
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - length 0 or data is NULL
 *		BSP_RESULT_ERROR_ALREADY_RUNNING - Mutex is locked
 *		BSP_RESULT_ERROR - Mutex isn't released or message not sent
 ***********************************************************************/
BSP_RESULT_E bsp_i2c_mem_write(
		IN uint8_t 		i2c_addr,
		IN uint8_t 		reg_addr,
		IN uint8_t 		reg_add_size,
		IN uint8_t* 	buff,
		IN uint16_t 	len)
{
	if (buff == NULL || len == 0)
		return BSP_RESULT_BAD_COMMAND;

	if (osMutexWait(i2cMutex,osWaitForever) != osOK)
		return BSP_RESULT_ERROR_ALREADY_RUNNING;

	HAL_StatusTypeDef res = HAL_I2C_Mem_Write(&hi2c1, i2c_addr, reg_addr, reg_add_size, buff, len, 100);

	if (osMutexRelease(i2cMutex) != osOK)
		return BSP_RESULT_ERROR;

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bsp_i2c_rx
 * Description: I2C RX
 * Synchronized function (mutex)
 * Parameters :
 *		i2c_addr - I2C address, buff - buffer to read into,
 *		len - data length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - length 0 or data is NULL
 *		BSP_RESULT_ERROR_ALREADY_RUNNING - Mutex is locked
 *		BSP_RESULT_ERROR - Mutex isn't released or message not received
 ***********************************************************************/
BSP_RESULT_E bsp_i2c_rx(
		IN uint8_t 		i2c_addr,
		IN uint8_t 		len,
 		OUT uint8_t* 	buff)
{
	if (buff == NULL || len == 0)
		return BSP_RESULT_BAD_COMMAND;

	if (osMutexWait(i2cMutex,osWaitForever) != osOK)
		return BSP_RESULT_ERROR_ALREADY_RUNNING;

	HAL_StatusTypeDef res = HAL_I2C_Master_Receive(&hi2c1, i2c_addr, buff, len, 100);

	if (osMutexRelease(i2cMutex) != osOK)
		return BSP_RESULT_ERROR;

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bsp_i2c_tx
 * Description: I2C TX
 * Synchronized function (mutex)
 * Parameters :
 *		i2c_addr - I2C address, buff - buffer to send,
 *		len - data length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - length 0 or data is NULL
 *		BSP_RESULT_ERROR_ALREADY_RUNNING - Mutex is locked
 *		BSP_RESULT_ERROR - Mutex isn't released or message not sent
 ***********************************************************************/
BSP_RESULT_E bsp_i2c_tx(
		IN uint8_t 		i2c_addr,
		IN uint8_t* 	buff,
		IN uint8_t 		len)
{
	if (buff == NULL || len == 0)
		return BSP_RESULT_BAD_COMMAND;

	if (osMutexWait(i2cMutex,osWaitForever) != osOK)
		return BSP_RESULT_ERROR_ALREADY_RUNNING;

	HAL_StatusTypeDef res = HAL_I2C_Master_Transmit(&hi2c1, i2c_addr, buff, len, 100) ;

	if (osMutexRelease(i2cMutex) != osOK)
		return BSP_RESULT_ERROR;

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bsp_enter_sleep_mode
 * Description: Put device in sleep/standby mode
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *
 ***********************************************************************/
BSP_RESULT_E bsp_enter_mode(uint8_t mode)
{
	//__HAL_RCC_PWR_CLK_ENABLE();
	//HAL_PWR_EnableBkUpAccess();
    //HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFE);
	//HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);
	//bsp_GPIO_Deinit();
	//HAL_I2C_DeInit(&hi2c1);
	//HAL_SPI_DeInit(&hspi_ppg);
	//HAL_SPI_DeInit(&hspi_ecg);
	//HAL_DeInit();
	HAL_PWR_EnterSTANDBYMode();
	//HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_STOPENTRY_WFI);
    return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bsp_write_pin
 * Description: Write state to a specific pin
 * Parameters :
 *		gpio - port and pin (BSP_PIN_TYPE_E),
 *		pin_state - high/low
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_INVALID_PIN - if pin doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_write_pin(
		IN BSP_PIN_TYPE_E 	gpio,
		IN BSP_PIN_STATE_E 	pin_state)
{
	if (gpio >= BSP_PIN_TYPE_MAX_USED)
		return BSP_RESULT_BAD_COMMAND;
	
	if (gpio != (BSP_PIN_TYPE_E)BSP_GPIO_ARRAY[gpio][0])
		return BSP_RESULT_ERROR_GPIO_ARRAY_MISMATCH;

	HAL_GPIO_WritePin((GPIO_TypeDef*)BSP_GPIO_ARRAY[gpio][1], BSP_GPIO_ARRAY[gpio][2], (GPIO_PinState)pin_state);
	return BSP_RESULT_OK;
}

///***********************************************************************
// * Function name: bsp_set_pin_as_input
// * Description: Set pin as input
// * Parameters :
// *		gpio - port and pin (BSP_PIN_TYPE_E),
// *
// * Returns :
// *		BSP_RESULT_OK - on success
// *		BSP_RESULT_ERROR_GPIO_ARRAY_MISMATCH - if pin doesn't exist
// *
// ***********************************************************************/
//BSP_RESULT_E bsp_set_pin_as_input(IN BSP_PIN_TYPE_E 	gpio)
//{
//	GPIO_InitTypeDef GPIO_InitStruct;
//	
//	if (gpio >= BSP_PIN_TYPE_MAX_USED)
//		return BSP_RESULT_ERROR_GPIO_ARRAY_MISMATCH;
//	
//	if (gpio != (BSP_PIN_TYPE_E)BSP_GPIO_ARRAY[gpio][0])
//		return BSP_RESULT_ERROR_GPIO_ARRAY_MISMATCH;
//
//	GPIO_InitStruct.Pin = BSP_GPIO_ARRAY[gpio][2];
//	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//	GPIO_InitStruct.Pull = GPIO_NOPULL;
//	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
//	HAL_GPIO_Init((GPIO_TypeDef*)BSP_GPIO_ARRAY[gpio][1], &GPIO_InitStruct);	
//	
//	return BSP_RESULT_OK;
//}
//
//void bspSd(uint8_t status)
//{
//	GPIO_InitTypeDef GPIO_InitStruct;
//	
//	if (status)
//	{
//		GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
//		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//		GPIO_InitStruct.Pull = GPIO_NOPULL;
//		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
//		HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);
//
//		bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_RESET);		
//		osDelay(100);
//		bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_SET);	
//	}
//	else
//	{
//		GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
//		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//		GPIO_InitStruct.Pull = GPIO_NOPULL;
//		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
//		HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);	
//		
//		bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_RESET);		
//		
//	}
//	
//	osDelay(100);
//}

/***********************************************************************
 * Function name: bsp_toogle_pin
 * Description: Toogle state of a specific pin
 * Parameters :
 *		gpio - port and pin (BSP_PIN_TYPE_E),
 *
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_INVALID_PIN - if pin doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_toogle_pin(
		IN BSP_PIN_TYPE_E 	gpio)
{
	if (gpio >= BSP_PIN_TYPE_MAX_USED)
		return BSP_RESULT_BAD_COMMAND;

	if (gpio != (BSP_PIN_TYPE_E)BSP_GPIO_ARRAY[gpio][0])
		return BSP_RESULT_ERROR_GPIO_ARRAY_MISMATCH;

	HAL_GPIO_TogglePin((GPIO_TypeDef*)BSP_GPIO_ARRAY[gpio][1], BSP_GPIO_ARRAY[gpio][2]);
	return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bspReadPin
 * Description: Read state of a specific pin
 * Parameters :
 *		gpio - port and pin (BSP_PIN_TYPE_E),
 *		pin_state - high/low
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_INVALID_PIN - if pin doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bspReadPin(
		IN BSP_PIN_TYPE_E 		gpio,
		OUT BSP_PIN_STATE_E* 	pin_state)
{
	if ( (gpio >= BSP_PIN_TYPE_MAX_USED) || (pin_state == NULL) )
		return BSP_RESULT_BAD_COMMAND;

	if (gpio != (BSP_PIN_TYPE_E)BSP_GPIO_ARRAY[gpio][0])
		return BSP_RESULT_ERROR_GPIO_ARRAY_MISMATCH;

	*pin_state = (BSP_PIN_STATE_E)HAL_GPIO_ReadPin((GPIO_TypeDef*)BSP_GPIO_ARRAY[gpio][1], BSP_GPIO_ARRAY[gpio][2]);
	
	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bsp_set_irq_mode
 * Description: enable/disable a specific irq
 * Parameters :
 *		irq - IRQ number (BSP_IRQ_E)
 *		mode - enable/disable
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_WRONG_IRQ - if irq number invalid
 *
 ***********************************************************************/
BSP_RESULT_E bsp_set_irq_mode(
		IN BSP_IRQ_E irq,
		IN BOOL mode)
{
	if (irq == BSP_IRQ_BLE_MCU_WKUP)
		HAL_NVIC_SetPriority((IRQn_Type)irq, 5, 0);
	
	if (mode)
		HAL_NVIC_EnableIRQ((IRQn_Type)irq);
	else
		HAL_NVIC_DisableIRQ((IRQn_Type)irq);

	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bsp_set_irq_priority
 * Description: enable/disable a specific irq
 * Parameters :
 *		irq - IRQ number (BSP_IRQ_E)
 *		preempt_priority - Main priority
 *		sub_priority - Sub priority
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_WRONG_IRQ - if irq number invalid
 *
 ***********************************************************************/
BSP_RESULT_E bsp_set_irq_priority(
		IN BSP_IRQ_E irq,
		IN uint32_t preempt_priority, 
		IN uint32_t sub_priority)
{
	HAL_NVIC_SetPriority((IRQn_Type)irq, preempt_priority, sub_priority);

	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bsp_spi_transmit
 * Description: SPI tx
 * Parameters :
 *		device_type - SPI type (BSP_SPI_DEVICE_TYPE_E), pData - data in,
 *		size - data length, timeout - timeout
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_SPI_TX_FAILED - SPI TX failed
 *		BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST - SPI doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_spi_transmit(
		IN BSP_SPI_DEVICE_TYPE_E device_type,
		IN uint8_t*		pData,
		IN uint16_t 	size,
		IN uint32_t 	timeout)
{
	HAL_StatusTypeDef res;
	switch (device_type)
	{
		case BSP_SPI_ECG1:
		{
			bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
			res = HAL_SPI_Transmit(&hspi_ecg, pData, size, timeout);
			bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
		}
		break;
		
		case BSP_SPI_ECG2:
		{
			bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_RESET);
			res = HAL_SPI_Transmit(&hspi_ecg, pData, size, timeout);
			bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_SET);
		}
		break;
		
		case BSP_SPI_ACCEL:
		{
			bsp_write_pin(BSP_PIN_TYPE_ACCEL_CS, BSP_PIN_RESET);
			res = HAL_SPI_Transmit(&hspi_accel, pData, size, timeout);
			bsp_write_pin(BSP_PIN_TYPE_ACCEL_CS, BSP_PIN_SET);
		}
		break;
		
//	case BSP_SPI_RAM:
//		{
//			bsp_write_pin(BSP_PIN_TYPE_TP2, BSP_PIN_RESET);
//			res = HAL_SPI_Transmit(&hspi_accel, pData, size, timeout);
//			bsp_write_pin(BSP_PIN_TYPE_TP2, BSP_PIN_SET);		
//		}
//		break;
		default:
		{
			return BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST;
		}
		break;
	}
	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR_SPI_TX_FAILED);
}

/***********************************************************************
 * Function name: bsp_spi_transmit_receive
 * Description: SPI tx and rx Blocking
 * Parameters :
 *		device_type - SPI type (BSP_SPI_DEVICE_TYPE_E), pTxData - tx data,
 *		pRxData - rx data,  size - data rx length, timeout - timeout
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_SPI_TX_FAILED - SPI TX failed
 *		BSP_RESULT_ERROR_SPI_RX_FAILED - SPI RX failed
 *		BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST - SPI doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_spi_transmit_receive(
		IN BSP_SPI_DEVICE_TYPE_E device_type,
		IN uint8_t *pTxData,
		IN uint16_t size,
		IN uint32_t timeout,
		OUT uint8_t *pRxData)
{
	HAL_StatusTypeDef res;
	
	
	switch (device_type)
	{
		case BSP_SPI_ECG1:
		{
			bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
			res = HAL_SPI_TransmitReceive(&hspi_ecg, pTxData, pRxData, size, timeout);
			bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
		}
		break;
		
		case BSP_SPI_ECG2:
		{
			bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_RESET);
			res = HAL_SPI_TransmitReceive(&hspi_ecg, pTxData, pRxData, size, timeout);
			bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_SET);
		}
		break;
		
		case BSP_SPI_ACCEL:
		{
			bsp_write_pin(BSP_PIN_TYPE_ACCEL_CS, BSP_PIN_RESET);
			res = HAL_SPI_TransmitReceive(&hspi_accel, pTxData, pRxData, size, timeout);
			bsp_write_pin(BSP_PIN_TYPE_ACCEL_CS, BSP_PIN_SET);
		}
		break;
//		case BSP_SPI_RAM:
//		{
//			bsp_write_pin(BSP_PIN_TYPE_TP2, BSP_PIN_RESET);
//			res = HAL_SPI_TransmitReceive(&hspi_accel, pTxData, pRxData, size, timeout);
//			bsp_write_pin(BSP_PIN_TYPE_TP2, BSP_PIN_SET);
//		}
//		break;				
		default:
		{
			return BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST;
		}
		break;
	}	
	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR_SPI_TX_FAILED);
	
}

/***********************************************************************
 * Function name: bsp_spi_transmit_receive_dma
 * Description: SPI tx and rx with DMA ASynchronized
 * Parameters :
 *		device_type - SPI type (BSP_SPI_DEVICE_TYPE_E), pTxData - tx data,
 *		pRxData - rx data,  size - data rx length,
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_SPI_TX_FAILED - SPI TX failed
 *		BSP_RESULT_ERROR_SPI_RX_FAILED - SPI RX failed
 *		BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST - SPI doesn't exist
 *
 ***********************************************************************/
BSP_RESULT_E bsp_spi_transmit_receive_dma(
		IN BSP_SPI_DEVICE_TYPE_E device_type,
		IN uint8_t*		pTxData,
		IN uint16_t 	size,
		OUT uint8_t*	pRxData)
{
	HAL_StatusTypeDef res;

	switch (device_type)
	{
		case BSP_SPI_ECG1:
		{
			bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
			res = HAL_SPI_TransmitReceive_DMA(&hspi_ecg, pTxData, pRxData, size);	
		}
		break;
		
		case BSP_SPI_ECG2:
		{
			bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_RESET);
			res = HAL_SPI_TransmitReceive_DMA(&hspi_ecg, pTxData, pRxData, size);
		}
		break;
		
		case BSP_SPI_ACCEL:
		{
			bsp_write_pin(BSP_PIN_TYPE_ACCEL_CS, BSP_PIN_RESET);
			res = HAL_SPI_TransmitReceive_DMA(&hspi_accel, pTxData, pRxData, size);
		}
		break;
		
		default:
		{
			return BSP_RESULT_ERROR_SPI_DOES_NOT_EXIST;
		}
		break;
	}	
	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR_SPI_TX_FAILED);	
}
/***********************************************************************
 * Function name: bspSpiDmaStop
 * Description: stop SPI DMA last transaction
 * Parameters :
 *		device_type - SPI type (BSP_SPI_DEVICE_TYPE_E)
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - wrong type
 *		BSP_RESULT_ERROR - HAL error escalation
 *
 ***********************************************************************/
BSP_RESULT_E bspSpiDmaStop(IN BSP_SPI_DEVICE_TYPE_E device_type)
{
	SPI_HandleTypeDef *hspi = NULL;
	switch (device_type)
	{
		case BSP_SPI_ECG1:
		case BSP_SPI_ECG2:
		{
			hspi = &hspi_ecg;
		}
		break;
		case BSP_SPI_ACCEL:
			hspi = &hspi_accel;
			break;
		default:
			break;
	}
	if (hspi == NULL)
		return BSP_RESULT_BAD_COMMAND;
	if (HAL_SPI_DMAStop(hspi) == HAL_OK)
		return BSP_RESULT_OK;
	else
		return BSP_RESULT_ERROR;
}

/***********************************************************************
 * Function name: bsp_uart_tx_dma
 * Description: UART TX
 * Parameters :
 *		uart - uart handle , pTxData - tx data,
 *		size - data tx length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_UART_TX_FAILED -  TX failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_dma(
		IN BSP_UART_E uart,
		IN uint8_t*		pTxData,
		IN uint16_t 	size)
{
	HAL_StatusTypeDef res;
	if (uart == BSP_UART_DEBUG)
		res = HAL_UART_Transmit_DMA(&huart1, pTxData, size);
	else if (uart == BSP_UART_BLE)
		res = HAL_UART_Transmit_DMA(&huart2, pTxData, size);
	else
		return BSP_RESULT_ERROR;
	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR); 
}


/***********************************************************************
 * Function name: bsp_uart_tx_dma_serial_comm
 * Description: UART TX
 * Parameters :
 *		pTxData - tx data,
 *		size - data tx length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_UART_TX_FAILED -  TX failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_dma_serial_comm(
		IN uint8_t*		pTxData,
		IN uint16_t 	size)
{
	HAL_StatusTypeDef res;
	res = HAL_UART_Transmit_DMA(&huart1, pTxData, size);

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bsp_uart_tx_dma_ble
 * Description: UART TX
 * Parameters :
 *		pTxData - tx data,
 *		size - data tx length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_UART_TX_FAILED -  TX failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_dma_ble(
		IN uint8_t*		pTxData,
		IN uint16_t 	size)
{
	HAL_StatusTypeDef res;
	res = HAL_UART_Transmit_DMA(&huart2, pTxData, size);

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bsp_uart_tx_it
 * Description: UART TX
 * Parameters :
 *		uart - uart handle , pTxData - tx data,
 *		size - data tx length
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_UART_TX_FAILED -  TX failed
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_tx_it(
		IN BSP_UART_E uart,
		IN uint8_t*		pTxData,
		IN uint16_t 	size)
{
	HAL_StatusTypeDef res;

	if (uart == BSP_UART_DEBUG)
		res = HAL_UART_Transmit_IT(&huart1, pTxData, size);
	else if (uart == BSP_UART_BLE)
		res = HAL_UART_Transmit_IT(&huart2, pTxData, size);
	else
		return BSP_RESULT_ERROR;
	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR); 
}
/***********************************************************************
 * Function name: bsp_uart_receive_it
 * Description: UART receive via interrupt
 * Parameters :
 *		uart - uart handle , rx_buffer - rx data,
 *		size - Amount of data to be received
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_receive_it(
		IN BSP_UART_E uart,
		OUT uint8_t*		rx_buffer,
		IN uint16_t 	size)
{
	HAL_StatusTypeDef res = HAL_ERROR;
//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET);
	if (uart == BSP_UART_DEBUG)
		res = HAL_UART_Receive_IT(&huart1, rx_buffer, size);
	else if (uart == BSP_UART_BLE)
		res = HAL_UART_Receive_IT(&huart2, rx_buffer, size);
//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR); 
}

/***********************************************************************
 * Function name: bsp_uart_receive
 * Description: UART receive
 * Parameters :
 *		uart - uart handle , rx_buffer - rx data,
 *		size - Amount of data to be received, timeout: Timeout duration
 *		size_rx - Amount of data received
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_receive(
		IN BSP_UART_E uart,
		IN uint8_t*		rx_buffer,
		IN uint16_t 	size,
		IN uint16_t		timeout,
		OUT uint16_t*	size_rx)
{
	HAL_StatusTypeDef res = HAL_ERROR;
//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET);
	if (uart == BSP_UART_DEBUG)
	{
		res = HAL_UART_Receive(&huart1, rx_buffer, size, timeout);
		*size_rx = huart2.RxXferCount;
	}
	else if (uart == BSP_UART_BLE)
	{
		res = HAL_UART_Receive(&huart2, rx_buffer, size, timeout);
		*size_rx = huart1.RxXferCount;
	}
//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);	
	if (res == HAL_TIMEOUT)
		return BSP_RESULT_ERROR_TIMEOUT;
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR); 
}

/***********************************************************************
 * Function name: bsp_uart_receive_dma
 * Description: UART receive via dma
 * Parameters :
 *		uart - uart handle , rx_buffer - rx data,
 *		size - Amount of data to be received
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_receive_dma(
		IN BSP_UART_E 	uart,
		OUT uint8_t*	rx_buffer,
		IN uint16_t 	size)
{
	HAL_StatusTypeDef res = HAL_ERROR;

	if (uart == BSP_UART_DEBUG)
		res = HAL_UART_Receive_DMA(&huart1, rx_buffer, size);
	else if (uart == BSP_UART_BLE)
		res = HAL_UART_Receive_DMA(&huart2, rx_buffer, size);
	if (res != HAL_OK)
	   return BSP_RESULT_ERROR;
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR); 
}
/***********************************************************************
 * Function name: bsp_uart_stop_dma
 * Description: Stop UART dma
 * Parameters :
 *		uart - uart handle
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_uart_stop_dma(IN BSP_UART_E uart)
{
	HAL_StatusTypeDef res = HAL_ERROR;
	if (uart == BSP_UART_DEBUG)
		res = HAL_UART_DMAStop(&huart1);
	else if (uart == BSP_UART_BLE)
		res = HAL_UART_DMAStop(&huart2);
	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR); 
}

/***********************************************************************
 * Function name: bspUartDmaDataLeft
 * Description: Number of bytes to read in dma register
 * Parameters :
 *		uart - uart handle
 *		length - Number of bytes remaining
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspUartDmaDataLeft(
		IN BSP_UART_E uart, 
		OUT uint16_t* length)
{
	if (uart == BSP_UART_DEBUG)
		*length = huart1.hdmarx->Instance->CNDTR;
	else if (uart == BSP_UART_BLE)
		*length = huart2.hdmarx->Instance->CNDTR;

	return BSP_RESULT_OK;
}


/***********************************************************************
 * Function name: bsp_wd_init
 * Description: Watchdog init
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
//BSP_RESULT_E bsp_wd_init(void)
//{
//	IwdgHandle.Instance = IWDG;
//	IwdgHandle.Init.Prescaler = IWDG_PRESCALER_32;
//	IwdgHandle.Init.Reload = 3000;
//	HAL_IWDG_Init(&IwdgHandle);
//}

/***********************************************************************
 * Function name: bsp_led
 * Description: Turn On/Off specific Led
 * Parameters : 
 * led - led type, led_state - On/Off
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_LED_INVALID - Led not valid
 *
 ***********************************************************************/
BSP_RESULT_E bsp_led(BSP_LEDS_E led, BSP_LED_STATE_E led_state)
{
	BSP_RESULT_E res;
	
	switch (led)
	{
		case BSP_LED_RED:
			res = bsp_write_pin(BSP_PIN_TYPE_LED_RED, (BSP_PIN_STATE_E)led_state);
		break;
		case BSP_LED_BLUE:
			res = bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, (BSP_PIN_STATE_E)led_state);
		break;
		default:
			return BSP_RESULT_ERROR_LED_INVALID;
		break;
	}
	
	return res;
}
/***********************************************************************
 * Function name: enable_peripherals
 * Description: Peripherals Enable/Disable
 * Parameters : status - TRUE or FALSE
 *				test - type of test
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E enable_peripherals(IN BSP_TESTS_TYPE_E test, 
								IN uint8_t status)
{
//	static uint8_t	bit_mask = 0;
//	GPIO_InitTypeDef GPIO_InitStruct;
//
//	
//	if (status)
//	{
//		if (test == BSP_TEST_TYPE_ECG)
//		{
//			/*Configure GPIO pins : PA9 POWER_DOWN_RESET-ADS */
//			GPIO_InitStruct.Pin = ECG_RESET_PIN;
//			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//			GPIO_InitStruct.Pull = GPIO_NOPULL;
//			GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
//			HAL_GPIO_Init(ECG_RESET_PORT, &GPIO_InitStruct);
//
//			/*Configure GPIO pins : PA4 CS_SPI2-ADS */
//			GPIO_InitStruct.Pin = ECG_CS_PIN;
//			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//			GPIO_InitStruct.Pull = GPIO_NOPULL;
//			GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
//			HAL_GPIO_Init(ECG_CS_PORT, &GPIO_InitStruct);			
//		}
////		else if (test == BSP_TEST_TYPE_PPG)
////		{
////			/*SpO2 Start*/
////			HAL_SPI_Init(&hspi_ppg);
////			/*Configure GPIO pins : PB2 AFE_POWER_DOWN */
////			GPIO_InitStruct.Pin = PPG_PWRDN_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
////			HAL_GPIO_Init(PPG_PWRDN_PORT, &GPIO_InitStruct);
////
////			/*Configure GPIO pins : PB1 AFE_RESET */
////			GPIO_InitStruct.Pin = PPG_RESET_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
////			HAL_GPIO_Init(PPG_RESET_PORT, &GPIO_InitStruct);
////
////			/*Configure GPIO pin : PB12 SPI1_CS- AFE */
////			GPIO_InitStruct.Pin = PPG_CS_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
////			HAL_GPIO_Init(PPG_CS_PORT, &GPIO_InitStruct);		
////			/*SpO2 End*/		
////		}
//		bit_mask |= test;
//		HAL_GPIO_WritePin(PER_PWR_OFF_PORT, PER_PWR_OFF_PIN, GPIO_PIN_RESET);
//	}
//	else
//	{
//		bit_mask &= ~test;
//		if (bit_mask == 0)
//		{
//			/*SpO2 Start*/
////			HAL_SPI_DeInit(&hspi_ppg);
////			GPIO_InitStruct.Pin = SPI2_SCLK_PIN|SPI2_MISO_PIN|SPI2_MOSI_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			HAL_GPIO_Init(SPI2_PORT, &GPIO_InitStruct);			
////			
////			/*Configure GPIO pins : PB2 AFE_POWER_DOWN */
////			GPIO_InitStruct.Pin = PPG_PWRDN_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			HAL_GPIO_Init(PPG_PWRDN_PORT, &GPIO_InitStruct);
////
////			/*Configure GPIO pins : PB1 AFE_RESET */
////			GPIO_InitStruct.Pin = PPG_RESET_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			HAL_GPIO_Init(PPG_RESET_PORT, &GPIO_InitStruct);
////
////			/*Configure GPIO pin : PB12 SPI1_CS- AFE */
////			GPIO_InitStruct.Pin = PPG_CS_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			HAL_GPIO_Init(PPG_CS_PORT, &GPIO_InitStruct);
//			/*SpO2 End*/
//			
//			/*ECG Start*/			
////			GPIO_InitStruct.Pin = SPI1_SCLK_PIN | SPI1_MISO_PIN | SPI1_MOSI_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//
//			/*Configure GPIO pins : PA4 CS_SPI2-ADS */
//			GPIO_InitStruct.Pin = ECG_CS_PIN;
//			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//			GPIO_InitStruct.Pull = GPIO_NOPULL;
//			HAL_GPIO_Init(ECG_CS_PORT, &GPIO_InitStruct);
////
////			/*Configure GPIO pins : PB0 START-ADS*/
////			GPIO_InitStruct.Pin = ECG_START_PIN;
////			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
////			GPIO_InitStruct.Pull = GPIO_NOPULL;
////			HAL_GPIO_Init(ECG_START_PORT, &GPIO_InitStruct);
//
//			/*Configure GPIO pins : PA9 POWER_DOWN_RESET-ADS */
//			GPIO_InitStruct.Pin = ECG_RESET_PIN;
//			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//			GPIO_InitStruct.Pull = GPIO_NOPULL;
//			HAL_GPIO_Init(ECG_RESET_PORT, &GPIO_InitStruct);			
//			
//			/*ECG End*/
//			HAL_GPIO_WritePin(PER_PWR_OFF_PORT, PER_PWR_OFF_PIN, GPIO_PIN_SET);
//		}
//	}
////	
////	if (status)
////		HAL_GPIO_WritePin(PER_PWR_OFF_PORT, PER_PWR_OFF_PIN, GPIO_PIN_RESET);
////	else
////		HAL_GPIO_WritePin(PER_PWR_OFF_PORT, PER_PWR_OFF_PIN, GPIO_PIN_SET);
//
	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bsp_system_reset
 * Description: Restart System 
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_system_reset(void)
{
    
    HAL_NVIC_SystemReset();
    
    return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bsp_ble_config
 * Description: BLE Config - set UART1 to input, set ble config/test to '0', 
 * toggle ble reset pin
 * Parameters : source - from which interface the ble will be configured
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_ble_config(IN BSP_BLE_CONFIG_SOURCE_E source)
{
//	GPIO_InitTypeDef 		GPIO_InitStruct;
	


	return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bsp_ble_reset
 * Description: BLE Reset 
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_ble_reset(void)
{
	
//	HAL_GPIO_WritePin(BLE_RST_N_PORT, BLE_RST_N_PIN, GPIO_PIN_RESET);
//	osDelay(5);
//	HAL_GPIO_WritePin(BLE_RST_N_PORT, BLE_RST_N_PIN, GPIO_PIN_SET);
//	osDelay(10);
	return BSP_RESULT_OK;
	
}
/***********************************************************************
 * Function name: bsp_restart_sensors
 * Description: Restart all sensors 
 * Parameters :
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bsp_restart_sensors(void)
{
//	
//	HAL_GPIO_WritePin(PER_PWR_OFF_PORT, PER_PWR_OFF_PIN, GPIO_PIN_SET);
//	osDelay(800);
//	HAL_GPIO_WritePin(PER_PWR_OFF_PORT, PER_PWR_OFF_PIN, GPIO_PIN_RESET);
	
	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bspReadAdc
 * Description: Read ADC Channel
 * Parameters : adc - ADC channel
 * 				adc_value - Value from ADC
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *		BSP_RESULT_ERROR_ADC_DOES_NOT_EXIST - ADC not valid
 *
 ***********************************************************************/
BSP_RESULT_E bspReadAdc(IN BSP_ADC_E adc, 
						OUT uint32_t* adc_value)
{

  ADC_ChannelConfTypeDef 		adc_ch = {.OffsetNumber = ADC_OFFSET_NONE}; //= {0};
	ADC_HandleTypeDef* 			hadc;
	BSP_RESULT_E				bsp_res = BSP_RESULT_OK;
	
	//memset(&adc_ch, 0, sizeof(ADC_ChannelConfTypeDef));
	osStatus os_res = osMutexWait(bspAdcMutex, ADC_MUTEX_TIMEOUT);
	if (os_res != osOK)
	   return BSP_RESULT_ERROR_MUTEX_ERROR;
	
	switch (adc)
	{
		case BSP_ADC_HW_IND:
		{
			MX_ADC3_Init();
			hadc = &hadc3;
    		adc_ch.Channel = ADC_CHANNEL_7;
			adc_ch.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
		}
		break;
		
		case BSP_ADC_BATTERY_LEVEL:
		{
			MX_ADC1_Init();
			hadc = &hadc1;
			adc_ch.Channel = ADC_CHANNEL_18;
			adc_ch.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
		}
		break;
		
		case BSP_ADC_TEMPERATURE:
		{
			MX_ADC3_Init();
			hadc = &hadc3;
    		adc_ch.Channel = ADC_CHANNEL_TEMPSENSOR;
			adc_ch.SamplingTime = ADC_SAMPLETIME_640CYCLES_5;
		}
		break;
		
		case BSP_ADC_IMPEDANCE:
		{
			MX_ADC1_Init();
			hadc = &hadc1;
			adc_ch.Channel = ADC_CHANNEL_11;
			adc_ch.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
		}
		break;

		default:
		{
			osMutexRelease(bspAdcMutex);
			return BSP_RESULT_ERROR_ADC_DOES_NOT_EXIST;
		}
		break;		
	}

    adc_ch.Rank = 1;

    adc_ch.Offset = 0;
	
	HAL_StatusTypeDef hal_res;
    
	hal_res = HAL_ADC_ConfigChannel(hadc, &adc_ch);

    hal_res |= HAL_ADC_Start(hadc);

	hal_res |= HAL_ADC_PollForConversion(hadc, 2000);
	
    if (hal_res == HAL_OK)	// 2000
    {
        *adc_value = HAL_ADC_GetValue(hadc);
    }
    else
	{
        bsp_res = BSP_RESULT_ERROR;
	}
	
    HAL_ADC_DeInit(hadc);
	osMutexRelease(bspAdcMutex);
    return bsp_res;
}

static BSP_ADC_E adc_curr = BSP_ADC_MAX;

/***********************************************************************
 * Function name: bspReadAdcIt
 * Description: Read ADC Channel
 * Parameters : adc - ADC channel
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *		BSP_RESULT_ERROR_ADC_DOES_NOT_EXIST - ADC not valid
 *
 ***********************************************************************/
BSP_RESULT_E bspReadAdcIt(IN BSP_ADC_E adc)
{

    ADC_ChannelConfTypeDef 		adc_ch;
	ADC_HandleTypeDef* 			hadc;

	if (adc >= BSP_ADC_MAX)
		return BSP_RESULT_BAD_COMMAND;
	


	osStatus os_res = osMutexWait(bspAdcMutex, ADC_MUTEX_TIMEOUT);
	if (os_res != osOK)
	   return BSP_RESULT_ERROR_MUTEX_ERROR;
	
	switch (adc)
	{
		case BSP_ADC_HW_IND:
		{
			MX_ADC3_Init();
			hadc = &hadc3;
    		adc_ch.Channel = ADC_CHANNEL_7;
			adc_ch.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
		}
		break;

		case BSP_ADC_BATTERY_LEVEL:
		{
			MX_ADC1_Init();
			hadc = &hadc1;
			adc_ch.Channel = ADC_CHANNEL_VBAT; //ADC_CHANNEL_18;
			adc_ch.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
		}
		break;

		case BSP_ADC_TEMPERATURE:
		{
			MX_ADC3_Init();
			hadc = &hadc3;
    		adc_ch.Channel = ADC_CHANNEL_TEMPSENSOR;
			adc_ch.SamplingTime = ADC_SAMPLETIME_640CYCLES_5;
		}
		break;

		case BSP_ADC_IMPEDANCE:
		{
			MX_ADC1_Init();
			hadc = &hadc1;
			adc_ch.Channel = ADC_CHANNEL_11;
			adc_ch.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
		}
		break;

		default:
		{
			osMutexRelease(bspAdcMutex);
			return BSP_RESULT_ERROR_ADC_DOES_NOT_EXIST;
		}
		break;
	}

	adc_curr = adc;

    adc_ch.Rank = 1;

    adc_ch.Offset = 0;

    HAL_StatusTypeDef hal_res = HAL_ADC_ConfigChannel(hadc, &adc_ch);

    hal_res |= HAL_ADC_Start_IT(hadc);

    if (hal_res != HAL_OK)
    	return BSP_RESULT_ERROR_ADC_ERROR;

    return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bspReadComp
 * Description: Read Comparator
 * Parameters : value - value from comparator
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspReadComp(INOUT uint32_t *value)
{
	 
	*value = HAL_COMP_GetOutputLevel(&hcomp2);
	
	return BSP_RESULT_OK;
}

// Override
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	uint32_t adc_value;
	adc_value = HAL_ADC_GetValue(hadc);
	HAL_ADC_Stop_IT(hadc);
	HAL_ADC_DeInit(hadc);

	switch (adc_curr)
	{
	case BSP_ADC_IMPEDANCE:
		bspAdcReadCompleteCallbackImpedance(adc_value);
		break;
	case BSP_ADC_TEMPERATURE:
		bspAdcReadCompleteCallbackTemperature(adc_value);
		break;
	case BSP_ADC_BATTERY_LEVEL:
		bspAdcReadCompleteCallbackBattery(adc_value);
		break;
	default:
		//ERR_LOG("HAL_ADC_ConvCpltCallback unknown source %d", adc_curr);
		break;
	}

	adc_curr = BSP_ADC_MAX;	// Reset for next time

	osMutexRelease(bspAdcMutex);
}

// Override
void HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp)
{
	bspCompBatteryLowCallback();
}

void HAL_ADC_IRQHandler_Callback()
{
   	switch (adc_curr)
	{
	case BSP_ADC_IMPEDANCE:
	case BSP_ADC_BATTERY_LEVEL:
   		HAL_ADC_IRQHandler(&hadc1);
		break;
	case BSP_ADC_TEMPERATURE:
	case BSP_ADC_HW_IND:
  		HAL_ADC_IRQHandler(&hadc3);
		break;
	}
   
}

void HAL_COMP_IRQHandler_Callback()
{
	HAL_COMP_IRQHandler(&hcomp2);
}

/***********************************************************************
 * Function name: bspHwVerGet
 * Description: Gets HW revision from BSP
 * Parameters : hw_ver - HW version
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspHwVerGet(OUT uint8_t* hw_ver)
{
	*hw_ver = bsp_hw_ver;
	
	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: prvBspSetHwRev
 * Description: Gets HW revision via ADC and BSP HW revision variable
 * Parameters : 
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
static BSP_RESULT_E prvBspSetHwRev(void)
{
	uint32_t hw_ver = 0;
	if (bspReadAdc(BSP_ADC_HW_IND, &hw_ver) != BSP_RESULT_OK)
		return BSP_RESULT_ERROR;
	else
		bsp_hw_ver = hw_ver / BSP_HW_VOLTAGE;
	
	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bspReadTemp
 * Description: Read ADC1 10
 * Parameters : temperature - MCU temperature
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspReadTemp(IN float32_t* temperature)
{

	uint32_t 					temp = 0;
    uint16_t					ts_cal1 = 0, ts_cal2 = 0;
	
	memcpy((uint8_t*)&ts_cal1, (void*)ADC_TEMP_SENSOR_CAL1_30_ADDR, 2);
	memcpy((uint8_t*)&ts_cal2, (void*)ADC_TEMP_SENSOR_CAL2_110_ADDR, 2);
	
	if (bspReadAdc(BSP_ADC_TEMPERATURE, &temp) != BSP_RESULT_OK)
		return BSP_RESULT_ERROR;
	
//	temp *= 3300;
//	temp /= (1 << 12);
	
	*temperature = (float32_t)temp - ts_cal1;
	*temperature *= ((float32_t)(ADC_TEMP_SENSOR_110 - ADC_TEMP_SENSOR_30) / (ts_cal2 - ts_cal1));
	
	*temperature += 30;
	
//		temperature = adc_value;// * 2800;//TEMPERATURE_3_3_MV;
//		*temperature /= TEMPERATURE_12_BIT; //Reading in mV
//		*temperature /= 1000.0; //Reading in Volts
//		*temperature -= TEMPERATURE_REFERENCE_VOLTAGE_AT_25C; // Subtract the reference voltage at 25\B0C
//		*temperature /= TEMPERATURE_AVERAGE_SLOPE_VOLTAGE; // Divide by slope 2.5mV
//		*temperature += TEMPERATURE_OFFSET_VOLTAGE; // Add the 25\B0C
		
		
//	if ((*temperature < -10) || (*temperature > 100))//Wrong value
//		return BSP_RESULT_ERROR;



    return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bspRtcTimeGet
 * Description: Reads RTC time and Date
 * Parameters : BSP_TIME_STRUCT* time, BSP_DATE_STRUCT* date
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeGet(BSP_TIME_STRUCT* time, BSP_DATE_STRUCT* date)
{
	HAL_StatusTypeDef		res;
	RTC_TimeTypeDef 		time_type;
	RTC_DateTypeDef 		date_type;

	RV30287_rtc_time rv30287_rtc_time;
	res = RV3028_Read_Time(&hi2c2, &rv30287_rtc_time);

	/*time->seconds = rv30287_rtc_time.tm_sec;
	time->hours = rv30287_rtc_time.tm_hour;
	time->minutes = rv30287_rtc_time.tm_min;
	time->msec = 0;
	
	date->date = rv30287_rtc_time.tm_mday;
	date->month = rv30287_rtc_time.tm_mon;
	date->year = rv30287_rtc_time.tm_year;*/

	/*res = HAL_RTC_GetTime(&hrtc, &time_type, RTC_FORMAT_BCD);*/

	time->seconds = (((rv30287_rtc_time.tm_sec & 0xF0) >> 4) * 10) + (rv30287_rtc_time.tm_sec & 0x0F);
	time->hours = (((rv30287_rtc_time.tm_hour & 0xF0) >> 4) * 10) + (rv30287_rtc_time.tm_hour & 0x0F);
	time->minutes = (((rv30287_rtc_time.tm_min & 0xF0) >> 4) * 10) + (rv30287_rtc_time.tm_min & 0x0F);
	time->msec = 0;//1000*(time_type.SecondFraction-time_type.SubSeconds) / (time_type.SecondFraction+1);

	//HAL_RTC_GetDate(&hrtc, &date_type, RTC_FORMAT_BCD);
	
	date->date = (((rv30287_rtc_time.tm_mday & 0xF0) >> 4) * 10) + (rv30287_rtc_time.tm_mday & 0x0F);
	date->month = (((rv30287_rtc_time.tm_mon & 0xF0) >> 4) * 10) + (rv30287_rtc_time.tm_mon & 0x0F);
	date->year = (((rv30287_rtc_time.tm_year & 0xF0) >> 4) * 10) + (rv30287_rtc_time.tm_year & 0x0F);
	
	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bspRtcTimeDiff
 * Description: Calculates difference between 2 dates
 * Parameters : BSP_TIME_DATE_STRUCT* time, BSP_TIME_DATE_STRUCT* date
 *				uint32_t * out_sec - result of time difference [seconds]
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeDiff(IN BSP_TIME_DATE_STRUCT* start, IN BSP_TIME_DATE_STRUCT * end, OUT uint32_t * out_sec)
{
//	HAL_StatusTypeDef		res;

	if ( (start->date.year != end->date.year) || (start->date.month != end->date.month) )
		return BSP_RESULT_ERROR_NOT_IMPLEMENTED;	// not implemented
	if (start->date.date > end->date.date)
		return BSP_RESULT_BAD_COMMAND;

	uint32_t days_count =  end->date.date - start->date.date;

	BSP_TIME_STRUCT	diff;

	if(end->time.seconds < start->time.seconds)
	{
		--end->time.minutes;
		end->time.seconds += 60;
	}

	diff.seconds = end->time.seconds - start->time.seconds;

	if(end->time.minutes < start->time.minutes){
		--end->time.hours;
		end->time.minutes += 60;
	}

	diff.minutes = end->time.minutes - start->time.minutes;
	diff.hours = end->time.hours - start->time.hours;

	* out_sec = days_count * SEC_PER_DAY + diff.hours *SEC_PER_HOUR + diff.minutes * SEC_PER_MIN + diff.seconds;

	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bspRtcTimeSet
 * Description: Sets RTC time
 * Parameters : BSP_TIME_STRUCT
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeSet(BSP_TIME_STRUCT* time, BSP_DATE_STRUCT* date)
{
	HAL_StatusTypeDef		res;
	RV30287_rtc_time rv30287_rtc_time;

	/*time_type.Hours = (time->hours % 10) | ((time->hours / 10) << 4);
	time_type.Minutes = (time->minutes % 10) | ((time->minutes / 10) << 4);
	time_type.Seconds = (time->seconds % 10) | ((time->seconds / 10) << 4);*/
	
	rv30287_rtc_time.tm_hour = (time->hours % 10) | ((time->hours / 10) << 4);
	rv30287_rtc_time.tm_min = (time->minutes % 10) | ((time->minutes / 10) << 4);
	rv30287_rtc_time.tm_sec = (time->seconds % 10) | ((time->seconds / 10) << 4);

	rv30287_rtc_time.tm_mday = (date->date % 10) | ((date->date / 10) << 4);
	rv30287_rtc_time.tm_mon = (date->month % 10) | ((date->month / 10) << 4);
	rv30287_rtc_time.tm_year = (date->year % 10) | ((date->year / 10) << 4);

	//res = HAL_RTC_SetTime(&hrtc, &time_type, RTC_FORMAT_BCD);
	res = RV3028_Write_Time(&hi2c2, &rv30287_rtc_time);

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}


/***********************************************************************
 * Function name: bspRtcTimeGetBCD
 * Description: Reads RTC time and Date in BCD format
 * Parameters : BSP_RTC_TIMESTAMP * bcd
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_BAD_COMMAND - on wrong input (*bcd)
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeGetBCD(BSP_RTC_TIMESTAMP * bcd)
{
	HAL_StatusTypeDef		res;
	RV30287_rtc_time rv30287_rtc_time;
	RTC_TimeTypeDef 		time_type;
	//RTC_DateTypeDef 		date_type;
	if (bcd == NULL)
		return BSP_RESULT_BAD_COMMAND;

	res = RV3028_Read_Time(&hi2c2, &rv30287_rtc_time);

	bcd->field.seconds = rv30287_rtc_time.tm_sec;
	bcd->field.hours = rv30287_rtc_time.tm_hour;
	bcd->field.minutes = rv30287_rtc_time.tm_min;
	//bcd->field.msec = 0;

	bcd->field.date = rv30287_rtc_time.tm_mday;
	bcd->field.month = rv30287_rtc_time.tm_mon;
	bcd->field.year = rv30287_rtc_time.tm_year;

	res = HAL_RTC_GetTime(&hrtc, &time_type, RTC_FORMAT_BCD);

	/*bcd->field.seconds = time_type.Seconds;
	bcd->field.hours = time_type.Hours;
	bcd->field.minutes = time_type.Minutes;*/

	uint16_t msec = 1000*(time_type.SecondFraction-time_type.SubSeconds) / (time_type.SecondFraction+1);

	bcd->field.msec = (msec % 10) | ( ( (msec / 10) % 10) << 4 ) | ( ( (msec / 100) % 10) << 8 );

	/*HAL_RTC_GetDate(&hrtc, &date_type, RTC_FORMAT_BCD);

	bcd->field.date = date_type.Date;
	bcd->field.month = date_type.Month;
	bcd->field.year = date_type.Year;*/

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bspRtcTimeSet
 * Description: Sets RTC time
 * Parameters : BSP_TIME_STRUCT
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimestampToBCD(BSP_DATE_STRUCT	* date, BSP_TIME_STRUCT	* time, OUT BSP_RTC_TIMESTAMP * bcd)
{
	if ( (time == NULL) || (date == NULL) ||(bcd == NULL) )
		return BSP_RESULT_BAD_COMMAND;

	bcd->field.hours = (time->hours % 10) | ((time->hours / 10) << 4);
	bcd->field.minutes = (time->minutes % 10) | ((time->minutes / 10) << 4);
	bcd->field.seconds = (time->seconds % 10) | ((time->seconds / 10) << 4);
	bcd->field.msec = 1000*(255-time->msec) / 256;


	bcd->field.date = (date->date % 10) | ((date->date / 10) << 4);
	bcd->field.month = (date->month % 10) | ((date->month / 10) << 4);
	bcd->field.year = (date->year % 10) | ((date->year / 10) << 4);

	return BSP_RESULT_OK;
}


/***********************************************************************
 * Function name: bspRtcTimeSetBCD
 * Description: Sets RTC time using BCD string format
 * Parameters : BSP_RTC_TIMESTAMP
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcTimeSetBCD(BSP_RTC_TIMESTAMP * time)
{
	HAL_StatusTypeDef		res;
	RTC_TimeTypeDef 		time_type;
	RTC_DateTypeDef 		date_type;

	time_type.Hours = time->field.hours;
	time_type.Minutes = time->field.minutes;
	time_type.Seconds = time->field.seconds;
	time_type.SubSeconds = 0;  //(-1) * time->field.msec * (time_type.SecondFraction+1) / 1000 + time_type.SecondFraction;
   time_type.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
   time_type.StoreOperation = RTC_STOREOPERATION_RESET;
   
	//time->msec = 1000*(time_type.SecondFraction-time_type.SubSeconds) / (time_type.SecondFraction+1);

	res = HAL_RTC_SetTime(&hrtc, &time_type, RTC_FORMAT_BCD);

	date_type.Date = time->field.date;
	date_type.Month =time->field.month;
	date_type.Year = time->field.year;
   date_type.WeekDay = RTC_WEEKDAY_SUNDAY;

	res |= HAL_RTC_SetDate(&hrtc, &date_type, RTC_FORMAT_BCD);

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bspRtcDateSet
 * Description: Sets RTC Date
 * Parameters : BSP_DATE_STRUCT* date
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspRtcDateSet(BSP_DATE_STRUCT* date)
{
	HAL_StatusTypeDef		res;
	RTC_DateTypeDef 		date_type;

	date_type.Date = (date->date % 10) | ((date->date / 10) << 4);//date->date;
	date_type.Month = (date->month % 10) | ((date->month / 10) << 4);// date->month;
	date_type.Year = (date->year % 10) | ((date->year / 10) << 4);//date->year;
	
	res = HAL_RTC_SetDate(&hrtc, &date_type, RTC_FORMAT_BCD);

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bspCheckWdReset
 * Description: Check if GMP reseted by watchdog and clear flags
 *				If reseted by WD close device
 * Parameters : 
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspCheckWdReset(void)
{
	uint8_t flag = 0;
#ifndef MENNEN_ALGO
	flag = __HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST);
	__HAL_RCC_CLEAR_RESET_FLAGS();
	
	if (flag == TRUE)
	{
		return BSP_RESULT_ERROR;
	}
#endif
	return BSP_RESULT_OK;
}
/***********************************************************************
 * Function name: bspLockOptionBytes
 * Description: Lock option bytes, disable flash reading
 *				
 * Parameters : 
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspLockOptionBytes(void)
{
	HAL_StatusTypeDef 				status;
	FLASH_OBProgramInitTypeDef 		ob_Init;
	
	memset((uint8_t*)&ob_Init, 0, sizeof(ob_Init));
	
	ob_Init.OptionType = OPTIONBYTE_RDP;
	ob_Init.RDPLevel = OB_RDP_LEVEL_1;

	bspFlashUnlock();
	
	HAL_FLASH_OB_Unlock();
	
	status = HAL_FLASHEx_OBProgram(&ob_Init);
	
//	status |= HAL_FLASH_OB_Launch();
	
	HAL_FLASH_OB_Lock();
	
	bspFlashLock();
	
	return ((status == HAL_OK) ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bspFlashUnlock
 * Description: Flash unlock
 *				
 * Parameters : 
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspFlashUnlock(void)
{
	HAL_StatusTypeDef res;
	
	res = HAL_FLASH_Unlock();
	
	return ((res == HAL_OK) ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bspFlashLock
 * Description: Flash unlock
 *				
 * Parameters : 
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BSP_RESULT_E bspFlashLock(void)
{
	HAL_StatusTypeDef res;
	
	res = HAL_FLASH_Lock();
	
	return ((res == HAL_OK) ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bspFlashProgramByte
 * Description: Program flash in form of 1 byte
 *				
 * Parameters : address - flash address to write in
 *				data - address of data to write
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspFlashProgramByte(uint32_t address, uint8_t data)
{
	HAL_StatusTypeDef res;
	uint8_t data_to_write[8] = {0};
	
	osMutexWait(bspFlashMutex,osWaitForever);
	memset(data_to_write, 0xFF, sizeof(data_to_write));
	data_to_write[0] = data;
	
	res = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, address, *((uint64_t*)&data_to_write));

	osMutexRelease(bspFlashMutex);
	
	return ((res == HAL_OK) ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bspFlashProgramDw
 * Description: Program flash in form of double word
 *				
 * Parameters : address - flash address to write in
 *				data - address of data to write
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspFlashProgramDw(uint32_t address, uint64_t data)
{
	HAL_StatusTypeDef res;
	
	osMutexWait(bspFlashMutex,osWaitForever);
	
	res = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, address, data);

	osMutexRelease(bspFlashMutex);
	
	return ((res == HAL_OK) ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}
/***********************************************************************
 * Function name: bspFlashPagesErase
 * Description: Erase pages in flash
 *				
 * Parameters : page_num - start page number to erase
 *				num_of_pages - number of pages to delete from start page number
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR - on failure
 ***********************************************************************/
BSP_RESULT_E bspFlashPagesErase(uint32_t page_num, uint32_t num_of_pages)
{
	HAL_StatusTypeDef 		res;
	uint32_t 				page_error;
	FLASH_EraseInitTypeDef 	eraseDef = {
							.TypeErase = FLASH_TYPEERASE_PAGES, 
							.Banks = FLASH_BANK_BOTH,
							.Page = page_num,
							.NbPages = num_of_pages};


	osMutexWait(bspFlashMutex,osWaitForever);
	
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGAERR);
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGSERR);
	
	res = HAL_FLASHEx_Erase(&eraseDef, &page_error);

	osMutexRelease(bspFlashMutex);
	
	return ((res == HAL_OK) ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bspGpioSet
 * Description: set gpio
 *				
 * Parameters : gpio - gpio to set, state - state for gpio
 *
 * Returns :
 *		BSP_RESULT_OK - on success
 *		BSP_RESULT_ERROR_INVALID_PIN_STATE - on invalid pin state
 *		BSP_RESULT_ERROR_INVALID_PIN - on invalid pin
 ***********************************************************************/
BSP_RESULT_E bspGpioSet(char* gpio, char* state)
{
	uint8_t 			pin_state;
	uint8_t 			pin = 0;
	GPIO_TypeDef*		pin_port;
	GPIO_InitTypeDef 	GPIO_InitStruct;
	
	pin_state = atoi(state);
	
	if ((pin_state < 0) || (pin_state > 2))
		return BSP_RESULT_ERROR_INVALID_PIN_STATE;
	
	if (strlen(gpio) != 4)
		return BSP_RESULT_ERROR_INVALID_PIN;
	
	pin = atoi(&gpio[2]);
	
	if ((pin < 0) || (pin > 15))
		return BSP_RESULT_ERROR_INVALID_PIN;
	
	
	switch (gpio[1])
	{
		case 'a':
		case 'A':
			pin_port = GPIOA;
		break;
		case 'b':
		case 'B':
			pin_port = GPIOB;
		break;
		case 'c':
		case 'C':
			pin_port = GPIOC;
		break;
		case 'd':
		case 'D':
			pin_port = GPIOD;
		break;
		case 'e':
		case 'E':
			pin_port = GPIOE;
		break;
		case 'f':
		case 'F':
			pin_port = GPIOF;
		break;
		default:
		return BSP_RESULT_ERROR_INVALID_PIN;
	}

	GPIO_InitStruct.Pin = (1 << pin);
	if ((pin_state == 1) || (pin_state == 0))
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	else
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(pin_port, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(pin_port, (1 << pin), (GPIO_PinState)pin_state);
		
	return BSP_RESULT_OK;
}

/* ADC1 init function */
static BSP_RESULT_E MX_ADC1_Init(void)
{

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.NbrOfDiscConversion = 1;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure the ADC multi-mode 
    */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_VBAT; //ADC_CHANNEL_TEMPSENSOR;//
  sConfig.Rank = ADC_REGULAR_RANK_1;//1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
     return BSP_RESULT_ERROR;
  }

//	if (HAL_ADC_PollForConversion(&hadc1, 2000) == HAL_OK)
//	{
//		uint32_t tmp = HAL_ADC_GetValue(&hadc1);
//	}
	return BSP_RESULT_OK;
}

/* ADC3 init function */
static BSP_RESULT_E MX_ADC3_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc3.Instance = ADC3;
  hadc3.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc3.Init.Resolution = ADC_RESOLUTION_12B;
  hadc3.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc3.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc3.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc3.Init.LowPowerAutoWait = DISABLE;
  hadc3.Init.ContinuousConvMode = DISABLE;
  hadc3.Init.NbrOfConversion = 1;
  hadc3.Init.DiscontinuousConvMode = DISABLE;
  hadc3.Init.NbrOfDiscConversion = 1;
  hadc3.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc3.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc3.Init.DMAContinuousRequests = DISABLE;
  hadc3.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc3.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc3) != HAL_OK)
  {
	 return BSP_RESULT_ERROR;
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = ADC_REGULAR_RANK_1;//1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
 sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  
  if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
  {
    return BSP_RESULT_ERROR;
  }
  return BSP_RESULT_OK;

}



 /***********************************************************************
 * Function name: bsp_PMcaptureInit
 * Description:  configure and start pacemaker detection
 * Parameters : none
 *
 * Returns :
 *		BSP_RESULT_OK - operation success
 *
 *
 ***********************************************************************/
BSP_RESULT_E bspPmCaptureInit(void)
{
	// Timer Input Capture Configuration Structure
	TIM_IC_InitTypeDef       sConfig;

   __HAL_RCC_TIM2_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();


    // GPIO
    GPIO_InitTypeDef GPIO_InitStruct;
 	/*Configure GPIO pin :  BSP_PIN_TYPE_PM*/
	GPIO_InitStruct.Pin = PM_TIM2_CH4_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
	HAL_GPIO_Init(PM_TIM2_CH4_PORT, &GPIO_InitStruct);

    /* Input Capture timer Init */
    timer_handler_pace_maker.Instance = TIM2;
    // Clock source
    TIM_ClockConfigTypeDef sClockSourceConfig;
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if(HAL_TIM_ConfigClockSource(&timer_handler_pace_maker, &sClockSourceConfig)!= HAL_OK)
    {
        return BSP_RESULT_ERROR_PM;
    }

    timer_handler_pace_maker.Init.Period = PM_TIMER_MAX;
    timer_handler_pace_maker.Init.CounterMode = TIM_COUNTERMODE_UP;
    timer_handler_pace_maker.Init.Prescaler = 0;	//(uint32_t)(SYS_CLOCK_FREQ/TIM_CLOCK_FREQ) - 1; [MARK changed]
    timer_handler_pace_maker.Init.ClockDivision = 0;
    if (HAL_TIM_IC_Init(&timer_handler_pace_maker) != HAL_OK)
    {
        return BSP_RESULT_ERROR_PM;
    }

     // Configure the Input Capture channel
    sConfig.ICPrescaler = TIM_ICPSC_DIV1;
    sConfig.ICFilter = 0;
    sConfig.ICPolarity = TIM_ICPOLARITY_FALLING;
	sConfig.ICSelection = TIM_ICSELECTION_DIRECTTI;
	if (HAL_TIM_IC_ConfigChannel(&timer_handler_pace_maker, &sConfig, TIM_CHANNEL_4) != HAL_OK)
	{
		return BSP_RESULT_ERROR_PM;
	}

    // Interrupt enable
    HAL_NVIC_SetPriority(TIM2_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(TIM2_IRQn);
    return BSP_RESULT_OK;
}


// /***********************************************************************
// * Function name: HAL_TIM_IC_CaptureCallback
// * Description:  Input Capture callback in non blocking mode for PM calculation.
// * Parameters :  TIM IC handle - timer handle struct
// *
// * Returns : none
// *
// *
// *
// ***********************************************************************/
//
//void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)		//pm detector
//{
//
//	static uint32_t pm1stPulseTime;
//
//
//  	uint32_t pm2ndPulseTime = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
//
//
//  	uint32_t width;
//    bsp_pm_event_s		event;
//
// 	if(pm2ndPulseTime >= pm1stPulseTime)
// 		width = pm2ndPulseTime - pm1stPulseTime;
// 	else
// 		width = pm2ndPulseTime + PM_TIMER_MAX - pm1stPulseTime;
// 	if((width < PM_WIDTH_MIN_TICKS) || (width > PM_WIDTH_MAX_TICKS))
// 	{
// 		pm1stPulseTime = pm2ndPulseTime;
// 		return;
// 	}
// 	//PM detected
//    event.event_type = BSP_PM_EVENT_PM_DETECTED;
//    pmEvent(&event);
//   	TP1_TOOGLE
//}
/***********************************************************************
* Function name: HAL_TIM_IC_CaptureCallback
* Description:  Input Capture callback in non blocking mode for PM calculation.
* Parameters :  TIM IC handle - timer handle struct
*
* Returns : none
*
*
*
***********************************************************************/
#define PM_1st_EDGE_NOT_ASSIGENED       0xFFFFFFFF
#define PM_SHORT_PULSE_LIMIT            20
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)                  //pm detector
{

	static uint32_t pmNoiseCount = 0;
	static uint32_t pm1stPulseTime = PM_1st_EDGE_NOT_ASSIGENED;

	uint32_t pm2ndPulseTime = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
	if (pm1stPulseTime == PM_1st_EDGE_NOT_ASSIGENED)
	{
		pm1stPulseTime = pm2ndPulseTime;
		return;
	}

	uint32_t width;

	bsp_pm_event_s event;
//	TP2_TOOGLE
	if (pm2ndPulseTime >= pm1stPulseTime)
		width = pm2ndPulseTime - pm1stPulseTime;
	else
		width = pm2ndPulseTime + PM_TIMER_MAX - pm1stPulseTime;

	if (width < PM_WIDTH_MIN_TICKS)
	{
		if (++pmNoiseCount >= PM_SHORT_PULSE_LIMIT)
		{
			pm1stPulseTime = PM_1st_EDGE_NOT_ASSIGENED;
			pmNoiseCount = 0;
			event.event_type = BSP_PM_EVENT_NOISE_STATE;
			pmEvent(&event);
		}
		return;
	}
	if (width > PM_WIDTH_MAX_TICKS)
	{
		pm1stPulseTime = PM_1st_EDGE_NOT_ASSIGENED;
		pmNoiseCount = 0;
		event.event_type = BSP_PM_EVENT_NOISE_STATE;
		pmEvent(&event);
		return;
	}
	//PM detected
	pm1stPulseTime = PM_1st_EDGE_NOT_ASSIGENED;
	pmNoiseCount = 0;
	event.event_type = BSP_PM_EVENT_PM_DETECTED;
	pmEvent(&event);
	TP1_TOOGLE
}

/***********************************************************************
 * Function name: bsp_PMcaptureStart
 * Description:  stop PM capture
 * Parameters : none
 *
 * Returns :
 *		BSP_RESULT_OK - operation success
 *
 *
 ***********************************************************************/
BSP_RESULT_E bspPmCaptureStart(void)
{
	//enable PM pin event
    //Start the Input Capture in interrupt mode
    if (HAL_TIM_IC_Start_IT(&timer_handler_pace_maker, TIM_CHANNEL_4) != HAL_OK)
    {
        return BSP_RESULT_ERROR_PM;
    }
 /*   if (HAL_TIM_IC_Start_IT(&TimHandle, TIM_CHANNEL_3) != HAL_OK)
    {
        Error_Handler();
        return BSP_RESULT_ERROR_PM;
    } */
	return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bsp_PMcaptureStop
 * Description:  temporary stop PM capture
 * Parameters : none
 *
 * Returns :
 *		BSP_RESULT_OK - operation success
 *
 *
 ***********************************************************************/
BSP_RESULT_E bspPmCaptureStop(void)
{
	//disnable PM pin event
   	if (HAL_TIM_IC_Stop_IT(&timer_handler_pace_maker, TIM_CHANNEL_4) != HAL_OK)
    {
        return BSP_RESULT_ERROR_PM;
    }
   /* if (HAL_TIM_IC_Stop_IT(&TimHandle, TIM_CHANNEL_3) != HAL_OK)
    {
        Error_Handler();
        return BSP_RESULT_ERROR_PM;
    }*/
	return BSP_RESULT_OK;
}

/***********************************************************************
 * Function name: bsp_PM_power_switch
 * Description: power/depower picemaker resources
 * Parameters :
 * mode - 	BSP_POWER_DOWN_MODE
			BSP_POWER_UP_MODE
 *
 * Returns :
 *		BSP_RESULT_OK - switching success
 *		BSP_RESULT_ERROR - switching failed
 *
 ***********************************************************************/
BSP_RESULT_E bspPmPowerSwitch(BSP_POWER_MODE_E mode)
{
	if(mode == BSP_POWER_DOWN_MODE)
	{
		bsp_write_pin(BSP_PIN_TYPE_PM_OFF, BSP_PIN_RESET);
		return BSP_RESULT_OK;
	}
	else if(mode == BSP_POWER_UP_MODE)
	{
		bsp_write_pin(BSP_PIN_TYPE_PM_OFF, BSP_PIN_SET);
		return BSP_RESULT_OK;
	}
	return BSP_RESULT_ERROR_PM;
}

// overriding HAL interrupts methods (@ stm32f4xx_it.c)

void DMA1_Stream12_IRQHandler_callback()
{//SPI1 ECG RX
	HAL_DMA_IRQHandler(&hdma_spi_ecg_rx);
}
void DMA1_Stream13_IRQHandler_callback()
{//SPI1 ECG TX
	HAL_DMA_IRQHandler(&hdma_spi_ecg_tx);
}
void DMA2_Stream6_IRQHandler_callback()
{
	HAL_DMA_IRQHandler(&hdma_usart1_tx);
}
void DMA1_Stream7_IRQHandler_callback()
{
	HAL_DMA_IRQHandler(&hdma_usart2_tx);
}
void DMA2_Stream7_IRQHandler_callback()
{
	HAL_DMA_IRQHandler(&hdma_usart1_rx);
}
void DMA1_Stream6_IRQHandler_callback()
{
	HAL_DMA_IRQHandler(&hdma_usart2_rx);
}


void USART1_IRQHandler_callback()
{
#ifdef SERIAL_PROTOCOL_FAST
	if((USART1->ISR & USART_ISR_IDLE) != RESET)
	    {
	        USART1->ICR = UART_CLEAR_IDLEF;
	        /* Start DMA timer */
	        dma_uart_rx.timer = DMA_TIMEOUT_MS;
	    }
#endif 	//SERIAL_PROTOCOL_FAST
	HAL_UART_IRQHandler(&huart1);
}

void SysTick_Handler_callback()
{
#ifdef SERIAL_PROTOCOL_FAST
	/* DMA timer */
    if(dma_uart_rx.timer == 1)
    {
        /* DMA Timeout event: set Timeout Flag and call DMA Rx Complete Callback */
        dma_uart_rx.flag = 1;
        hdma_usart1_rx.XferCpltCallback(&hdma_usart1_rx);
    }
    if(dma_uart_rx.timer) { --dma_uart_rx.timer; }
#endif	//SERIAL_PROTOCOL_FAST
}



/** DMA Rx Complete AND DMA Rx Timeout function
 * Timeout event: generated after UART IDLE IT + DMA Timeout value
 * Scenarios:
 *  - Timeout event when previous event was DMA Rx Complete --> new data is from buffer beginning till (MAX-currentCNDTR)
 *  - Timeout event when previous event was Timeout event   --> buffer contains old data, new data is in the "middle": from (MAX-previousCNDTR) till (MAX-currentCNDTR)
 *  - DMA Rx Complete event when previous event was DMA Rx Complete --> entire buffer holds new data
 *  - DMA Rx Complete event when previous event was Timeout event   --> buffer entirely filled but contains old data, new data is from (MAX-previousCNDTR) till MAX
 * Remarks:
 *  - If there is no following data after DMA Rx Complete, the generated IDLE Timeout has to be ignored!
 *  - When buffer overflow occurs, the following has to be performed in order not to lose data:
 *      (1): DMA Rx Complete event occurs, process first part of new data till buffer MAX.
 *      (2): In this case, the currentCNDTR is already decreased because of overflow.
 *              However, previousCNDTR has to be set to MAX in order to signal for upcoming Timeout event that new data has to be processed from buffer beginning.
 *      (3): When many overflows occur, simply process DMA Rx Complete events (process entire DMA buffer) until Timeout event occurs.
 *      (4): When there is no more overflow, Timeout event occurs, process last part of data from buffer beginning till currentCNDTR.
*/
void HAL_UART_RxCpltCallback_1(UART_HandleTypeDef *huart)
{
    uint16_t i, pos, start, length;
    uint16_t currCNDTR = __HAL_DMA_GET_COUNTER(huart->hdmarx);

    /* Ignore IDLE Timeout when the received characters exactly filled up the DMA buffer and DMA Rx Complete IT is generated, but there is no new character during timeout */
    if(dma_uart_rx.flag && currCNDTR == DMA_BUF_SIZE)
    {
//        dma_uart_rx.flag = 0;
//       return;
	   __NOP();
    }

    /* Determine start position in DMA buffer based on previous CNDTR value */
    start = (dma_uart_rx.prevCNDTR < DMA_BUF_SIZE) ? (DMA_BUF_SIZE - dma_uart_rx.prevCNDTR) : 0;

    if(dma_uart_rx.flag)    /* Timeout event */
    {
        /* Determine new data length based on previous DMA_CNDTR value:
         *  If previous CNDTR is less than DMA buffer size: there is old data in DMA buffer (from previous timeout) that has to be ignored.
         *  If CNDTR == DMA buffer size: entire buffer content is new and has to be processed.
        */
        length = (dma_uart_rx.prevCNDTR < DMA_BUF_SIZE) ? (dma_uart_rx.prevCNDTR - currCNDTR) : (DMA_BUF_SIZE - currCNDTR);
		dma_uart_rx.prevCNDTR = currCNDTR;
        dma_uart_rx.flag = 0;
    }
    else                /* DMA Rx Complete event */
    {
        length = DMA_BUF_SIZE - start;
        dma_uart_rx.prevCNDTR = DMA_BUF_SIZE;
    }

	if (length > DMA_BUF_SIZE)
	   length = DMA_BUF_SIZE;	// Limit to max to prevent stack overflow
    
	/* Copy and Process new data */
    for(i=0,pos=start; i<length; ++i,++pos)
    {
        data[i] = dma_rx_buf[pos];
    }

    /* Send received data over USB */
//    CDC_Transmit_FS(data, length);
    bspSerialCallback(data, length);
}




BSP_RESULT_E bspUartDmaCyclicSerialStart()
{
	HAL_StatusTypeDef res = HAL_ERROR;

	SET_BIT(USART1->CR1, USART_CR1_IDLEIE);	//IDLE Interrupt Configuration
	HAL_NVIC_SetPriority(USART1_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(USART1_IRQn);

	res = HAL_UART_Receive_DMA(&huart1, dma_rx_buf, DMA_BUF_SIZE);
	__HAL_DMA_DISABLE_IT(huart1.hdmarx, DMA_IT_HT);

	return (res == HAL_OK ? BSP_RESULT_OK : BSP_RESULT_ERROR);
}






void USART2_IRQHandler_callback()
{
	HAL_UART_IRQHandler(&huart2);
}



void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart1)
		bsp_uart_debug_tx_cplt_callback();
	else
		bspUartBleTxCpltCallback();

}
		
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart1)
#ifdef SERIAL_PROTOCOL_FAST
		HAL_UART_RxCpltCallback_1(huart);
#else // SERIAL_PROTOCOL_FAST
	bsp_uart_debug_rx_cplt_callback();
#endif	//SERIAL_PROTOCOL_FAST
	else if (huart == &huart2)
		bspUartBleRxCpltCallback();
}


void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
	if (UartHandle == &huart2)
		bspUartBleErrorCallback(UartHandle->ErrorCode);
	else
		bspUartErrorCallback(UartHandle->ErrorCode);

}


void EXTI11_IRQHandler_callback()
{
	
	
}

void EXTI12_IRQHandler_callback()
{
	
	
}

void EXTI13_IRQHandler_callback()
{
	GPIO_PinState pin_state = HAL_GPIO_ReadPin(BLE_STATUS_PORT, BLE_STATUS_PIN);
	
	if (pin_state)
		bspBleStatus(TRUE);
	else
		bspBleStatus(FALSE);
}

void EXTI6_IRQHandler_callback()
{
	GPIO_PinState pin_state = HAL_GPIO_ReadPin(BUTTON_INT_PORT, BUTTON_INT_PIN);
//
	if (pin_state)
	{
		bspButtonEvent(BSP_BUTTON_EVENT_RELEASED);//Button Released
	}
	else
	{
//		prvBspButtonPressedDebouncer(0);
		bspButtonEvent(BSP_BUTTON_EVENT_PRESSED);
	}
}


//static BSP_RESULT_E prvBspButtonPressedDebouncer(uint8_t dc_in)
//{
//	osTimerStart(bsp_button_timer, BSP_BUTTON_PRESSED_DEBOUNCE_TIME_MSEC);
//	return BSP_RESULT_OK;
//}

//static void prvBspButtonPressedDebouncerCallback(void const *n)
//{
//	// verify button was released, long press will reset by HW
//	// BUTTON pull
//	if (HAL_GPIO_ReadPin(BUTTON_INT_PORT, BUTTON_INT_PIN) == GPIO_PIN_SET)
//		bspButtonEvent(BSP_BUTTON_EVENT_BOOKMARK);//Button pressed and released
//}


void EXTI0_IRQHandler_callback()
{
	bsp_uart_ble_rx_ind();	
}


//override board interrupt handler functions
void EXTI7_IRQHandler_callback()
{
	//call weak function for ecg1 handler
	bsp_ads1_irq_callback();
}

//override board interrupt handler functions
void EXTI8_IRQHandler_callback()
{
	//call weak function for ecg2 handler
	//bsp_ads2_irq_callback();
}

void SPI1_complete__callback(uint8_t * rxData, uint16_t size)//SPI 1 read complete  (ADS finished)
{
#ifdef G_UNIT_TEST_ENABLE
	if (bsp_unit_test_enable && bspAdsSpiCmpltCallbackRedirect(rxData, size))
		return;
#endif //G_UNIT_TEST_ENABLE
	//call weak function for ads handler
//	if (!HAL_GPIO_ReadPin(ECG1_CS_PORT, ECG1_CS_PIN))
//		bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
//	else if (!HAL_GPIO_ReadPin(ECG2_CS_PORT, ECG2_CS_PIN))
//		bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_SET);
	bspAdsSpiCmpltCallback(rxData, size);
}

void TIM2_IRQHandler_callback(void)   //Pacemaker capture
{
  HAL_TIM_IRQHandler(&timer_handler_pace_maker);
  //bsp_PMcapture_timer_callback(&timer_handler_pace_maker);
}

// dummy implementation of "weak" functions 
void HAL_TIMEx_CommutationCallback(TIM_HandleTypeDef *htim)
{
}
void HAL_TIMEx_BreakCallback(TIM_HandleTypeDef *htim)
{
}

void SPI2_complete__callback(uint8_t * rxData, uint16_t size)//SPI 2 read complete  (Accelerometer finished)
{
#ifdef G_UNIT_TEST_ENABLE
//	if (bsp_unit_test_enable && bspPpgSpiCmpltCallbackRedirect(rxData, size))
//		return;
#endif //G_UNIT_TEST_ENABLE
	
	//call weak function for ppg handler
//	bsp_ppg_spi_cmplt_callback(rxData, size);
//	bsp_write_pin(BSP_PIN_TYPE_ACCEL_CS, BSP_PIN_SET);
	
//	bspAccelSpiCmplt(rxData, size);
}

#ifdef G_UNIT_TEST_ENABLE
BSP_RESULT_E bsp_set_unit_test(uint8_t enable)
{
	bsp_unit_test_enable = enable;
	return BSP_RESULT_OK;
}
#endif //G_UNIT_TEST_ENABLE


/***********************************************************************
* Function name: prvBspImpedanceIOinit
* Description:  configure impedance measure pins
* Parameters : none
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
BSP_RESULT_E prvBspImpedanceIOinit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	/*Configure GPIO pins : PD5 impedance switch LL pin */
	GPIO_InitStruct.Pin = IMP_LL_ON_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(IMP_LL_ON_PORT, &GPIO_InitStruct);
	bsp_write_pin(BSP_PIN_TYPE_IMP_LL_SWITCH, BSP_PIN_RESET);

	/*Configure GPIO pins : PD6 impedance switch RA pin */
	GPIO_InitStruct.Pin = IMP_RA_ON_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(IMP_RA_ON_PORT, &GPIO_InitStruct);
	bsp_write_pin(BSP_PIN_TYPE_IMP_RA_SWITCH, BSP_PIN_RESET);

	/*Configure GPIO pins : PD7 impedance switch LA pin */
	GPIO_InitStruct.Pin = IMP_LA_ON_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(IMP_LA_ON_PORT, &GPIO_InitStruct);
	bsp_write_pin(BSP_PIN_TYPE_IMP_LA_SWITCH, BSP_PIN_RESET);

	/*Configure GPIO pins : PD13 impedance PWM output pin */
	GPIO_InitStruct.Pin = IMP_PWM_TIM4_CH2_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
	HAL_GPIO_Init(IMP_PWM_TIM4_CH2_PORT, &GPIO_InitStruct);

	/*Configure GPIO pins : PA6 impedance result analog input pin */
	GPIO_InitStruct.Pin = IMP_ADC12_IN11_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(IMP_ADC12_IN11_PORT, &GPIO_InitStruct);

	return BSP_RESULT_OK;

}

/***********************************************************************
* Function name: bspImpedanceHWinit
* Description:  configure impedance measure
* Parameters : pwm_period, pwm_pulse
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
BSP_RESULT_E bspImpedanceHWinit(uint32_t pwm_period, uint32_t pwm_pulse)
{
	// Timer output compare Configuration Structure
	TIM_OC_InitTypeDef       sConfig;
	TIM_ClockConfigTypeDef sClockSourceConfig;

	__HAL_RCC_TIM4_CLK_ENABLE();

	prvBspImpedanceIOinit();

	// impedance timer Init
	timer_handler_impedance.Instance = TIM4;

	// Clock source
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	sClockSourceConfig.ClockPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
	sClockSourceConfig.ClockPrescaler = TIM_ETRPRESCALER_DIV1;
	//ClockSourceConfig.ClockFilter = ;
	if(HAL_TIM_ConfigClockSource(&timer_handler_impedance, &sClockSourceConfig)!= HAL_OK)
	{
		return BSP_RESULT_ERROR_IMPEDANCE_INIT_FAILED;
	}

	//timer
	timer_handler_impedance.Init.Period = pwm_period;
	timer_handler_impedance.Init.CounterMode = TIM_COUNTERMODE_UP;
	timer_handler_impedance.Init.Prescaler = 0;
	timer_handler_impedance.Init.ClockDivision = 0;
	timer_handler_impedance.Init.RepetitionCounter = 0;
	if (HAL_TIM_PWM_Init(&timer_handler_impedance) != HAL_OK)
	{
		return BSP_RESULT_ERROR_IMPEDANCE_INIT_FAILED;
	}

	// Configure the PWM channel
	sConfig.OCMode       = TIM_OCMODE_PWM1;
	sConfig.OCPolarity   = TIM_OCPOLARITY_LOW;
	sConfig.OCFastMode   = TIM_OCFAST_DISABLE;
	sConfig.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
	sConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	sConfig.OCIdleState  = TIM_OCIDLESTATE_RESET;
	sConfig.Pulse        = pwm_pulse;

	if (HAL_TIM_PWM_ConfigChannel(&timer_handler_impedance, &sConfig, TIM_CHANNEL_2) != HAL_OK)
	{
		return BSP_RESULT_ERROR_IMPEDANCE_INIT_FAILED;
	}
	/* Start channel 2 */
	/* Enable the Capture compare channel */
	TIM_CCxChannelCmd(TIM4, TIM_CHANNEL_2,TIM_CCx_ENABLE);
	// FTO __HAL_TIM_MOE_ENABLE(&timer_handler_impedance);
	// FTO __HAL_TIM_ENABLE(&timer_handler_impedance);

	if(HAL_TIM_Base_Start_IT(&timer_handler_impedance)!= HAL_OK)
	{
		/* PWM Generation Error */
	}

	// Interrupt enable
	HAL_NVIC_SetPriority(TIM4_IRQn, 5, 0);
//	HAL_NVIC_EnableIRQ(TIM4_IRQn);
	return BSP_RESULT_OK;
}

/***********************************************************************
* Function name: bspImpedancePwmStart
* Description:  start or stop impedance measure PWM timer (sine generator)
* Parameters : 	mode (start /stop)
* 				pwm_val (value to start with)
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
BSP_RESULT_E bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_E mode, uint32_t pwm_val)
{
	switch (mode)
	{
		case BSP_IMPEDANCE_PWM_MODE_START:
			HAL_NVIC_EnableIRQ(TIM4_IRQn);
			TIM4->CCR2 = pwm_val;
			__HAL_RCC_TIM4_CLK_ENABLE();
			break;
		case BSP_IMPEDANCE_PWM_MODE_STOP:
			__HAL_RCC_TIM4_CLK_DISABLE();
			HAL_NVIC_DisableIRQ(TIM4_IRQn);
			break;
		default:
			return BSP_RESULT_BAD_COMMAND;
	}
	return BSP_RESULT_OK;
}

/***********************************************************************
* Function name: bspImpedanceSetPwmWidth
* Description:  sets PWM width
* Parameters : 	width
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
BSP_RESULT_E bspImpedanceSetPwmWidth(uint32_t width)
{
	TIM4->CCR2 = width;
	return BSP_RESULT_OK;
}

/***********************************************************************
* Function name: prvBspEnableInterrupts
* Description:  enable ADC1, ADC3 and COMP interrupts
* Parameters : 	width
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
static BSP_RESULT_E prvBspEnableInterrupts(void)
{
	
	HAL_NVIC_SetPriority(ADC1_2_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(ADC1_2_IRQn);
	HAL_NVIC_SetPriority(ADC3_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(ADC3_IRQn);
	HAL_NVIC_SetPriority(COMP_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(COMP_IRQn);

	return BSP_RESULT_OK;	
}	

/***********************************************************************
* Function name: bspCompModeSet
* Description:  Turn on or off the comparator
* Parameters : 	mode - ON/OFF
*
* Returns :
*		BSP_RESULT_OK - operation success
*		BSP_RESULT_ERROR_INVALID_ENUM - enum does not exist
***********************************************************************/
BSP_RESULT_E bspCompModeSet(IN BSP_MODE_SET_E mode)
{
	
	if (mode == BSP_MODE_SET_ON)
		HAL_COMP_Start(&hcomp2);
	else if (mode == BSP_MODE_SET_OFF)
		HAL_COMP_Stop(&hcomp2);
	else
		return BSP_RESULT_ERROR_INVALID_ENUM;
	
	return BSP_RESULT_OK;
}
/***********************************************************************
* Function name: bspSdModeSet
* Description:  Turn on or off the SD card
* Parameters : 	mode - ON/OFF
*
* Returns :
*		BSP_RESULT_OK - operation success
*		BSP_RESULT_ERROR_INVALID_ENUM - enum does not exist
***********************************************************************/
BSP_RESULT_E bspSdModeSet(IN BSP_MODE_SET_E mode)
{
//	GPIO_InitTypeDef GPIO_InitStruct;
//	
//	switch(mode)
//	{
//		case BSP_MODE_SET_ON:
//		{
//			GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
//			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//			GPIO_InitStruct.Pull = GPIO_NOPULL;
//			GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
//			HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);
//
//			bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_RESET);
//			osDelay(15);
//			bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_SET);
//			osDelay(15);
//		}
//		break;
//		
//		case BSP_MODE_SET_OFF:
//		{
//			bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_RESET);
//			osDelay(30);
//			
//			GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
//			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//			GPIO_InitStruct.Pull = GPIO_NOPULL;
//			GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
//			HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);	
//		}
//		break;
//		
//		default:
//		{
//			return BSP_RESULT_ERROR_INVALID_ENUM;
//		}
//		break;
//	}

	return BSP_RESULT_OK;
}

void TIM4_IRQHandler_callback(void)   //impedance
{

	if(__HAL_TIM_GET_FLAG(&timer_handler_impedance, TIM_FLAG_UPDATE) != SET)
  		return;

  	if(__HAL_TIM_GET_IT_SOURCE(&timer_handler_impedance, TIM_IT_UPDATE) !=SET)
		return;

	__HAL_TIM_CLEAR_IT(&timer_handler_impedance, TIM_IT_UPDATE);

	bspImpedanceAdcResultReadyCallback();
}




void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(huart->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspInit 0 */

  /* USER CODE END USART1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();
  
    /**USART1 GPIO Configuration    
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* Peripheral DMA init*/
  
//    hdma_usart1_rx.Instance = DMA1_Channel5;
	hdma_usart1_rx.Instance = DMA2_Channel7;
    hdma_usart1_rx.Init.Request = DMA_REQUEST_2;
    hdma_usart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_usart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart1_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
#ifdef SERIAL_PROTOCOL_FAST
    hdma_usart1_rx.Init.Mode = DMA_CIRCULAR;
    hdma_usart1_rx.Init.Priority = DMA_PRIORITY_VERY_HIGH;
#else	//SERIAL_PROTOCOL_FAST
    hdma_usart1_rx.Init.Mode = DMA_NORMAL;
    hdma_usart1_rx.Init.Priority = DMA_PRIORITY_LOW;
#endif	//SERIAL_PROTOCOL_FAST
    if (HAL_DMA_Init(&hdma_usart1_rx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(huart,hdmarx,hdma_usart1_rx);

//    hdma_usart1_tx.Instance = DMA1_Channel4;
	hdma_usart1_tx.Instance = DMA2_Channel6;
    hdma_usart1_tx.Init.Request = DMA_REQUEST_2;
    hdma_usart1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_usart1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart1_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart1_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart1_tx.Init.Mode = DMA_NORMAL;
    hdma_usart1_tx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_usart1_tx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(huart,hdmatx,hdma_usart1_tx);

    /* Peripheral interrupt init */
//  SET_BIT(USART1->CR1, USART_CR1_IDLEIE);	//IDLE Interrupt Configuration
#if defined(MENNEN_ALGO) || !defined(SERIAL_PROTOCOL_FAST)
    HAL_NVIC_SetPriority(USART1_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
#endif
  /* USER CODE BEGIN USART1_MspInit 1 */

  /* USER CODE END USART1_MspInit 1 */
  }
  else if(huart->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspInit 0 */

  /* USER CODE END USART2_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_USART2_CLK_ENABLE();
  
    /**USART2 GPIO Configuration    
    PA2     ------> USART2_TX
    PA0     ------> USART2_CTS
    PA3     ------> USART2_RX
    PA1     ------> USART2_RTS 
    */
	
    GPIO_InitStruct.Pin = USART_TX_Pin|USART_RX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

//If RTS/CTS enabled
	
//    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
//    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
//    GPIO_InitStruct.Pull = GPIO_NOPULL;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
//    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);	

    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;//GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;//GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_1;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);	
	
    /* Peripheral DMA init*/
  
    hdma_usart2_rx.Instance = DMA1_Channel6;
    hdma_usart2_rx.Init.Request = DMA_REQUEST_2;
    hdma_usart2_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_usart2_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart2_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart2_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart2_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart2_rx.Init.Mode = DMA_NORMAL;
    hdma_usart2_rx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_usart2_rx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(huart,hdmarx,hdma_usart2_rx);

    hdma_usart2_tx.Instance = DMA1_Channel7;
    hdma_usart2_tx.Init.Request = DMA_REQUEST_2;
    hdma_usart2_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_usart2_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart2_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart2_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart2_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart2_tx.Init.Mode = DMA_NORMAL;
    hdma_usart2_tx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_usart2_tx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(huart,hdmatx,hdma_usart2_tx);

    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(USART2_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);	
	
  /* USER CODE BEGIN USART2_MspInit 1 */

  /* USER CODE END USART2_MspInit 1 */
  }

}

void HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{

  if(huart->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspDeInit 0 */

  /* USER CODE END USART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();
  
    /**USART1 GPIO Configuration    
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9|GPIO_PIN_10);

    /* Peripheral DMA DeInit*/
    HAL_DMA_DeInit(huart->hdmarx);
    HAL_DMA_DeInit(huart->hdmatx);

    /* Peripheral interrupt DeInit*/
    HAL_NVIC_DisableIRQ(USART1_IRQn);

  /* USER CODE BEGIN USART1_MspDeInit 1 */

  /* USER CODE END USART1_MspDeInit 1 */
  }
  else if(huart->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspDeInit 0 */

  /* USER CODE END USART2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART2_CLK_DISABLE();
  
    /**USART2 GPIO Configuration    
    PA2     ------> USART2_TX
    PA0     ------> USART2_CTS
    PA3     ------> USART2_RX
    PA1     ------> USART2_RTS 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2|GPIO_PIN_0|GPIO_PIN_3|GPIO_PIN_1);

    /* Peripheral DMA DeInit*/
    HAL_DMA_DeInit(huart->hdmarx);
    HAL_DMA_DeInit(huart->hdmatx);

    /* Peripheral interrupt DeInit*/
    HAL_NVIC_DisableIRQ(USART2_IRQn);

  /* USER CODE BEGIN USART2_MspDeInit 1 */

  /* USER CODE END USART2_MspDeInit 1 */
  }

}


void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hspi->Instance==SPI1)
  {
  /* USER CODE BEGIN SPI1_MspInit 0 */

  /* USER CODE END SPI1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_SPI1_CLK_ENABLE();
  
    /**SPI1 GPIO Configuration    
    PE13     ------> SPI1_SCK
    PE14     ------> SPI1_MISO
    PE15     ------> SPI1_MOSI 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

    /* Peripheral DMA init*/
  
    hdma_spi_ecg_rx.Instance = DMA1_Channel2;
    hdma_spi_ecg_rx.Init.Request = DMA_REQUEST_1;
    hdma_spi_ecg_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_spi_ecg_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_spi_ecg_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_spi_ecg_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_spi_ecg_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_spi_ecg_rx.Init.Mode = DMA_NORMAL;
    hdma_spi_ecg_rx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_spi_ecg_rx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(hspi,hdmarx,hdma_spi_ecg_rx);

    hdma_spi_ecg_tx.Instance = DMA1_Channel3;
    hdma_spi_ecg_tx.Init.Request = DMA_REQUEST_1;
    hdma_spi_ecg_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_spi_ecg_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_spi_ecg_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_spi_ecg_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_spi_ecg_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_spi_ecg_tx.Init.Mode = DMA_NORMAL;
    hdma_spi_ecg_tx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_spi_ecg_tx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(hspi,hdmatx,hdma_spi_ecg_tx);

    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(SPI1_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(SPI1_IRQn);
  /* USER CODE BEGIN SPI1_MspInit 1 */

  /* USER CODE END SPI1_MspInit 1 */
  }
  else if(hspi->Instance==SPI2)
  {
  /* USER CODE BEGIN SPI2_MspInit 0 */

  /* USER CODE END SPI2_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_SPI2_CLK_ENABLE();
  
    /**SPI2 GPIO Configuration    
    PB15     ------> SPI2_MOSI
    PB14     ------> SPI2_MISO
    PB13     ------> SPI2_SCK
    */
    GPIO_InitStruct.Pin = GPIO_PIN_15|GPIO_PIN_14|GPIO_PIN_13;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


    /* Peripheral DMA init*/
  
    hdma_spi_accel_rx.Instance = DMA1_Channel4;
    hdma_spi_accel_rx.Init.Request = DMA_REQUEST_1;
    hdma_spi_accel_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_spi_accel_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_spi_accel_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_spi_accel_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_spi_accel_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_spi_accel_rx.Init.Mode = DMA_NORMAL;
    hdma_spi_accel_rx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_spi_accel_rx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(hspi,hdmarx,hdma_spi_accel_rx);

    hdma_spi_accel_tx.Instance = DMA1_Channel5;
    hdma_spi_accel_rx.Init.Request = DMA_REQUEST_1;
    hdma_spi_accel_rx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_spi_accel_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_spi_accel_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_spi_accel_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_spi_accel_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_spi_accel_rx.Init.Mode = DMA_NORMAL;
    hdma_spi_accel_rx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_spi_accel_rx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(hspi,hdmatx,hdma_spi_accel_rx);

    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(SPI2_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(SPI2_IRQn);
  /* USER CODE BEGIN SPI2_MspInit 1 */

  /* USER CODE END SPI2_MspInit 1 */
  }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi)
{

  if(hspi->Instance==SPI1)
  {
  /* USER CODE BEGIN SPI1_MspDeInit 0 */

  /* USER CODE END SPI1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI1_CLK_DISABLE();
  
    /**SPI1 GPIO Configuration    
    PE13     ------> SPI1_SCK
    PE14     ------> SPI1_MISO
    PE15     ------> SPI1_MOSI 
    */
    HAL_GPIO_DeInit(GPIOE, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15);

    /* Peripheral DMA DeInit*/
    HAL_DMA_DeInit(hspi->hdmarx);
    HAL_DMA_DeInit(hspi->hdmatx);

    /* Peripheral interrupt DeInit*/
    HAL_NVIC_DisableIRQ(SPI1_IRQn);

  
  /* USER CODE BEGIN SPI1_MspDeInit 1 */

  /* USER CODE END SPI1_MspDeInit 1 */
  }
   else if(hspi->Instance==SPI2)
  {
  /* USER CODE BEGIN SPI2_MspDeInit 0 */

  /* USER CODE END SPI2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI2_CLK_DISABLE();
  
    /**SPI2 GPIO Configuration    
    PB15     ------> SPI2_MOSI
    PB14     ------> SPI2_MISO
    PB13     ------> SPI2_SCK
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_15|GPIO_PIN_14|GPIO_PIN_13);

    /* Peripheral DMA DeInit*/
    HAL_DMA_DeInit(hspi->hdmarx);
    HAL_DMA_DeInit(hspi->hdmatx);

    /* Peripheral interrupt DeInit*/
    HAL_NVIC_DisableIRQ(SPI2_IRQn);

  /* USER CODE BEGIN SPI2_MspDeInit 1 */

  /* USER CODE END SPI2_MspDeInit 1 */
  } 
}


void HAL_MspInit(void)
{
  /* USER CODE BEGIN MspInit 0 */

  /* USER CODE END MspInit 0 */

  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_RCC_PWR_CLK_ENABLE();

  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* System interrupt init*/
  /* MemoryManagement_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
  /* BusFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
  /* UsageFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
  /* SVCall_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SVCall_IRQn, 0, 0);
  /* DebugMonitor_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
  /* PendSV_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(PendSV_IRQn, 15, 0);
  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

  /* USER CODE BEGIN MspInit 1 */

  /* USER CODE END MspInit 1 */
}

static uint32_t HAL_RCC_ADC_CLK_ENABLED=0;

void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hadc->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspInit 0 */

  /* USER CODE END ADC1_MspInit 0 */
    /* Peripheral clock enable */
    HAL_RCC_ADC_CLK_ENABLED++;
    if(HAL_RCC_ADC_CLK_ENABLED==1){
      __HAL_RCC_ADC_CLK_ENABLE();
    }
  /* USER CODE BEGIN ADC1_MspInit 1 */

  /* USER CODE END ADC1_MspInit 1 */
  }
  else if(hadc->Instance==ADC3)
  {
  /* USER CODE BEGIN ADC3_MspInit 0 */

  /* USER CODE END ADC3_MspInit 0 */
    /* Peripheral clock enable */
    HAL_RCC_ADC_CLK_ENABLED++;
    if(HAL_RCC_ADC_CLK_ENABLED==1){
      __HAL_RCC_ADC_CLK_ENABLE();
    }
  
    /**ADC3 GPIO Configuration    
    PF4     ------> ADC3_IN7 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_4;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /* USER CODE BEGIN ADC3_MspInit 1 */

  /* USER CODE END ADC3_MspInit 1 */
  }

}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* hadc)
{

  if(hadc->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspDeInit 0 */

  /* USER CODE END ADC1_MspDeInit 0 */
    /* Peripheral clock disable */
    HAL_RCC_ADC_CLK_ENABLED--;
    if(HAL_RCC_ADC_CLK_ENABLED==0){
      __HAL_RCC_ADC_CLK_DISABLE();
    }
  /* USER CODE BEGIN ADC1_MspDeInit 1 */

  /* USER CODE END ADC1_MspDeInit 1 */
  }
  else if(hadc->Instance==ADC3)
  {
  /* USER CODE BEGIN ADC3_MspDeInit 0 */

  /* USER CODE END ADC3_MspDeInit 0 */
    /* Peripheral clock disable */
    HAL_RCC_ADC_CLK_ENABLED--;
    if(HAL_RCC_ADC_CLK_ENABLED==0){
      __HAL_RCC_ADC_CLK_DISABLE();
    }
  
    /**ADC3 GPIO Configuration    
    PF4     ------> ADC3_IN7 
    */
    HAL_GPIO_DeInit(GPIOF, GPIO_PIN_4);

  /* USER CODE BEGIN ADC3_MspDeInit 1 */

  /* USER CODE END ADC3_MspDeInit 1 */
  }

}

void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc)
{

  if(hrtc->Instance==RTC)
  {
  /* USER CODE BEGIN RTC_MspInit 0 */

  /* USER CODE END RTC_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_RTC_ENABLE();
  /* USER CODE BEGIN RTC_MspInit 1 */

  /* USER CODE END RTC_MspInit 1 */
  }

}

void HAL_SD_MspInit(SD_HandleTypeDef* hsd)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hsd->Instance==SDMMC1)
  {
  /* USER CODE BEGIN SDMMC1_MspInit 0 */

  /* USER CODE END SDMMC1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_SDMMC1_CLK_ENABLE();
  
    /**SDMMC1 GPIO Configuration    
    PC8     ------> SDMMC1_D0
    PC12     ------> SDMMC1_CK
    PD2     ------> SDMMC1_CMD 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_SDMMC1;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_SDMMC1;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* USER CODE BEGIN SDMMC1_MspInit 1 */

  /* USER CODE END SDMMC1_MspInit 1 */
  }

}

void HAL_SD_MspDeInit(SD_HandleTypeDef* hsd)
{

  if(hsd->Instance==SDMMC1)
  {
  /* USER CODE BEGIN SDMMC1_MspDeInit 0 */

  /* USER CODE END SDMMC1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SDMMC1_CLK_DISABLE();
  
    /**SDMMC1 GPIO Configuration    
    PC8     ------> SDMMC1_D0
    PC12     ------> SDMMC1_CK
    PD2     ------> SDMMC1_CMD 
    */
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_8|GPIO_PIN_12);

    HAL_GPIO_DeInit(GPIOD, GPIO_PIN_2);

  }
  /* USER CODE BEGIN SDMMC1_MspDeInit 1 */

  /* USER CODE END SDMMC1_MspDeInit 1 */

}

void HAL_COMP_MspInit(COMP_HandleTypeDef* hcomp)
{

	GPIO_InitTypeDef GPIO_InitStruct;
	if(hcomp->Instance==COMP2)
	{
		/**COMP2 GPIO Configuration    
		PB6     ------> COMP2_INP 
		*/
		GPIO_InitStruct.Pin = GPIO_PIN_6;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	}

}

void HAL_COMP_MspDeInit(COMP_HandleTypeDef* hcomp)
{

	if(hcomp->Instance==COMP2)
	{
		/**COMP2 GPIO Configuration    
		PB6     ------> COMP2_INP 
		*/
		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_6);
	}

}


void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac)
{

  if(hdac->Instance==DAC1)
  {
  /* USER CODE BEGIN DAC1_MspInit 0 */

  /* USER CODE END DAC1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_DAC1_CLK_ENABLE();
  /* USER CODE BEGIN DAC1_MspInit 1 */

  /* USER CODE END DAC1_MspInit 1 */
  }

}

void HAL_DAC_MspDeInit(DAC_HandleTypeDef* hdac)
{

  if(hdac->Instance==DAC1)
  {
  /* USER CODE BEGIN DAC1_MspDeInit 0 */

  /* USER CODE END DAC1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_DAC1_CLK_DISABLE();
  /* USER CODE BEGIN DAC1_MspDeInit 1 */

  /* USER CODE END DAC1_MspDeInit 1 */
  }

}

static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x00101319; //0x00000E14;
  hi2c2.Init.OwnAddress1 = 164;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

void DMA1_Channel2_IRQHandler_callback()
{
   HAL_DMA_IRQHandler(&hdma_spi_ecg_rx);
}
void DMA1_Channel3_IRQHandler_callback()
{
   HAL_DMA_IRQHandler(&hdma_spi_ecg_tx);
}
void DMA1_Channel4_IRQHandler_callback()
{
   HAL_DMA_IRQHandler(&hdma_spi_accel_rx);
}
void DMA1_Channel5_IRQHandler_callback()
{
   HAL_DMA_IRQHandler(&hdma_spi_accel_tx);
}



