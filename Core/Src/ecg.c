#include "cmsis_os.h"

#include "ads.h"
#include "ecg.h"
#include "HRV_Calculation.h"
#include "serial_protocol.h"
#include "ecg_mmi_api.h"
#include "resp_mmi_api.h"
//#include "timers.h"
#include "bsp.h"
#include "impedance.h"

__weak void ecg_event(ecg_event_s * event)
{

}


#define ECG_ADS_ALIVE_TIMEOUT_HOLTER	700
#define ECG_ADS_ALIVE_TIMEOUT_PATCH		300

#define ECG_ALGO_IMPEDANCE_RECOVERY_TIME_SEC	10
#define ECG_IMPEDANCE_RETRIES					2
#define PM_IDLE_TIME               		50	// [MARK changed, was 5] 10 msec after PM detection

#define ECG_SQUARE_WAVE_NUM_OF_TEMPLATES_250_HZ 4

// 1 lead status per each block ( each block 125 * 3 uint8_t size )
typedef struct ECG_PROCESS_BUFFER{
	uint16_t         				pmLocation_1;
    uint16_t         				pmLocation_2;
    uint16_t         				pm_count;
	ADS_LEAD_STATUS					leads_status;
	ECG_MEAS_PROGRESS_DATA_STRUCT  	ecg_lead1_samples;
	ECG_MEAS_PROGRESS_DATA_STRUCT  	ecg_lead2_samples;
	ECG_MEAS_PROGRESS_DATA_STRUCT  	resp_samples;
}ECG_PROCESS_BUFFER;

typedef struct PMattr_ {
 	BOOL 		PMdetected;
 	//uint8_t		PMsamplesCount;
}PMattr_s;





static void ecgAdsAliveTimerCallback(void const *n);
static void prvEcgImpTimerCallback(void const *n);
static void ecgTask(void const * argument);
static void ecgPmIdleTimerCallback(void const *n);
static ECG_RESULT prvEcgAdsKeepAliveTimerStart(uint8_t start);
static ECG_RESULT prvEcgStart();
static ECG_RESULT prvEcgSquareWaveTemplateGenerate();

static ECG_RESULT prvEcgPmInit();
static ECG_RESULT prvEcgPmStart();
static ECG_RESULT prvEcgPmStop();

static PMattr_s PMattr = {FALSE};


static osMessageQId 					ECGQueueHandle;
static osThreadId 						processECGBufTaHandle;

static uint32_t                         ecgBufferIndex = 0, ecgBufferSampleIndex = 0;
static uint8_t                          meas_250_flag_first = TRUE;
//static ECG_PARAMS_STRUCT				ecg_params;
static ECG_RESP_CONFIG_OBJECT_STRUCT	ecg_params;

static ECG_STATE 						state = ECG_STATE_UNINITIALIZED;

//static TimerHandle_t 					ecg_timer;
//static ECG_MEAS_COMP_STATUS				ecg_timer_flag = ECG_MEAS_COMP_STATUS_OK;

#pragma location = ".RAM2"
static ECG_PROCESS_BUFFER  ecgSampleBuffer[NUMBER_OF_BUFFERS]; //ECG double buffer
static uint32_t						    			num_of_samples;
static uint8_t										ecg_impedance_retries = 0;
static uint16_t ecg_ads_alive_timeout;
static float32_t ecg_buffer_time_msec;

static uint16_t		algo_impedance_recover_count = 0;	// counter for post-impedance measurement recovery (it takes algo ~3 seconds to re-recognize tachycardia after impedance)

//static const uint8_t holter_square_template[ECG_SQUARE_WAVE_NUM_OF_TEMPLATES_250_HZ][375] = {
//		{0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0},
//		{0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0},
//		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
//		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0x13, 0x05, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
//};

static const uint8_t holter_square_template[ECG_SQUARE_WAVE_NUM_OF_TEMPLATES_250_HZ][375] = {
		{0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0},
		{0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0x65, 0x33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};


// MATCH GAIN: algorithm always gets signal amplification x3
const uint8_t ecg_mennen_algo_match_gain_table[][2] = {
					{3,1}, //Gain 1
					{3,2}, //Gain 2
					{1,1}, //Gain 3
					{3,4}, //Gain 4
					{1,2}, //Gain 6
					{3,8}, //Gain 8
					{1,4} //Gain 12
};


static osTimerId				ecg_ads_alive_timer;
static osTimerId				ecg_impedance_periodic_measurement_timer;
static int16_t 					ecg_timer_debug_count;
static osTimerId        		ecg_timer_PM;

static uint32_t lead_on_count;


static ECG_TEMPERATURE_SET_E 	ecg_temp_state = ECG_TEMPERATURE_SET_MAX;
static ECG_STATE 				prv_impedance_save_last_state;
static ECG_ALGO_ARRYTHMIA_EVENT_HEADER_STRUCT arrhythmias_mask = 0;

#ifdef MENNEN_ALGO
static void mennenAlgoTask(void const * argument);
static osMessageQId MennenQueueHandle;
osThreadId mennenAlgoTaskHandle;

#endif

static ECG_RESULT ecgSampleBufferReset()
{
	ecgSampleBuffer[ecgBufferIndex].ecg_lead1_samples.buff_byte_length = 0;
	ecgSampleBuffer[ecgBufferIndex].ecg_lead2_samples.buff_byte_length = 0;
	ecgSampleBuffer[ecgBufferIndex].resp_samples.buff_byte_length = 0;
	ecgSampleBuffer[ecgBufferIndex].leads_status = ADS_LEAD_STATUS_ALL_ON;
	ecgSampleBuffer[ecgBufferIndex].pmLocation_1 = PM_POSITION_NOT_ASSIGNED;
    ecgSampleBuffer[ecgBufferIndex].pmLocation_2 = PM_POSITION_NOT_ASSIGNED;
    ecgSampleBuffer[ecgBufferIndex].pm_count = 0;
	return ECG_RESULT_OK;
}

static void ecgAdsAliveTimerCallback(void const *n)
{
	ecg_event_s			event;
	
	LOG_ERR("ADS_DRIVER_ERR, timer expired\r\n");
	
	event.event_type = ECG_HOLTER_EVENT_ERROR;
	ecg_event(&event);
}



osTimerDef(ecgAdsAliveTimer, ecgAdsAliveTimerCallback);
osTimerDef(ecg_imp_timer, prvEcgImpTimerCallback);
osTimerDef(ecg_pm_timer, ecgPmIdleTimerCallback);




static void prvEcgImpTimerCallback(void const *n)
{

	ecg_event_s			event;
	event.event_type = ECG_EVENT_IMPEDANCE_TIME;
	event.payloadPtr.impedance_results = NULL;
	event.test_params = &ecg_params;
	ecg_event(&event);
}



ECG_RESULT ecgInit(void)
{
	//osStatus ret;
	
	if (state != ECG_STATE_UNINITIALIZED)
		return ECG_RESULT_ERROR_ALREADY_INITIALIZED;


    //Init queue
    osMessageQDef(ECGQueue, 2, ECG_PROCESS_BUFFER *);
    ECGQueueHandle = osMessageCreate(osMessageQ(ECGQueue), NULL);
    if (!ECGQueueHandle)
    	return ECG_RESULT_ERROR_QUEUE;


    
    
#ifdef MENNEN_ALGO
	mennen_algo_init();
#else

	//Init task
    osThreadDef(ecgTask, ecgTask, osPriorityNormal, 0, ECG_STACK_SIZE);
    processECGBufTaHandle = osThreadCreate(osThread(ecgTask), NULL);
    if (!processECGBufTaHandle)
		return ECG_RESULT_ERROR_OS;

#ifdef MENNEN_LIB
	ECG_resetProc();
	ECG_initProc();
    ECG_initArrh(); 

    ECG_setFilter(ECG_FILT_MONITOR);  //DIAGNOSTIC
    ECG_setNotch(ECG_PARAMS_FILTER_DEFAULT);

    Resp_initProc();
    Resp_initCalc();

    Resp_setNotch(RESP_NOTCH_50HZ);
    Resp_setFilter(RESP_FILT_01_4HZ);
    Resp_setGain  (RESP_GAIN_AUTO);    //(RESP_GAIN_1_2);
    Resp_setApneaLimit(RESP_APN_10SEC);
#endif //MENNEN_LIB
#endif //MENNEN_ALGO

    return ECG_RESULT_OK;
}

uint8_t ecgCalcGain(ADS_PARAMS_GAIN_ENUM gain)
{

	switch (gain)
	{
		case ADS_PARAMS_GAIN_1:
			return 1;
		case ADS_PARAMS_GAIN_2:
			return 2;
		case ADS_PARAMS_GAIN_3:
			return 3;
		case ADS_PARAMS_GAIN_4:
			return 4;
		case ADS_PARAMS_GAIN_6:
			return 6;
		case ADS_PARAMS_GAIN_8:
			return 8;
		case ADS_PARAMS_GAIN_12:
			return 12;
		default:
			break;
	}
	return 0xFF;
}

ECG_RESULT ecgCalcLsbWeight(ECG_RESP_CONFIG_OBJECT_STRUCT* ecg_params_struct, float32_t* weight)
{
	if ((ecg_params_struct == NULL) || (weight == NULL))
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	if (ecg_params_struct->ads.gain >= ADS_PARAMS_GAIN_MAX)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;
//	uint8_t gain = 0;
	
//	uint8_t gain = ECG_MENNEN_ALGO_DEFAULT_GAIN;//ecg_calc_gain(ecg_params_struct->gain);
//
//	if ((ecg_params_struct->notch_filter == ENUM_ECG_PARAMS_FILTER_OFF) || (ecg_params_struct->bandpass_filter == ECG_PARAMS_BANDPASS_FILTER_OFF))
//		*weight = (float64_t)((ADS_VREF / (gain * ADS_UNITS))) * 1000000;
//	else
//    {
//		*weight = (float64_t)((ADS_VREF / (gain * ADS_UNITS)) * 1000000);
//		*weight *= 8;
//    }

	if (ecg_params_struct->ecg_output.output_filter == ECG_FILTER_STATUS_OFF)
		*weight = (float64_t)((ADS_VREF / (ecgCalcGain(ecg_params_struct->ads.gain) * ADS_UNITS))) * 1000000;
	else
    {
		*weight = (float64_t)((ADS_VREF / (ECG_MENNEN_ALGO_DEFAULT_GAIN * ADS_UNITS)) * 1000000);
		*weight *= 8;
            //*weight = 0.76;
    }
	return ECG_RESULT_OK;
}

void ecgDebugStart(uint8_t index)
{
	switch (index)
	{
	case 0:
		prvEcgSquareWaveTemplateGenerate();
		break;
	case 1:
	{
		ADS_PARAMS_STRUCT par = {0};
		ADS_RESULT_E res;
		par.init_type = ADS_PARAMS_INIT_TYPE_REGULAR;
		par.resp_chan_en = ADS_PARAMS_RESPIRATION_ENABLED_ON;
		par.sample_rate = ADS_SAMPLE_RATE_500_HZ;

		ecgStop();
		for (uint8_t i=0; i<100; i++)
		{
			res = adsStart(&par);
			LOG("ECG debug ADS i=%d, res %d", i, res);
			res = adsStop();
			LOG(", stop res %d", res);

			par.init_type = ADS_PARAMS_INIT_TYPE_IMPEDANCE;
			res = adsStart(&par);
			LOG(", imp res %d",res);
			res = adsStop();
			LOG(", stop res %d\r\n", res);

		}
	}
	break;

	}
	
}


//=======================================================================
#ifdef MENNEN_ALGO
void mennen_algo_init(void)
{
//-------------- VH -------------------------
    ECG_initProc(); 
    ECG_initArrh(); 
    
    ECG_setFilter(ECG_FILT_MONITOR);  //DIAGNOSTIC
    ECG_setNotch(ECG_NOTCH_50Hz);
    
    //for test:
//    ARRH_setRunLimit   (5);
//    ARRH_setBgmLimit   (4);
//    ARRH_setPauseLimit (6);
//    ARRH_setVtachLimit (3);
//    ARRH_setEbradLimit (51);
//    ARRH_setEtachLimit (101);
//    ARRH_setPatientType(PATIENT_INFANT);

    
   Resp_initProc();
   Resp_initCalc();
//	Resp_resetProc();
	Resp_resetCalc();

    Resp_setNotch(RESP_NOTCH_50HZ);
    Resp_setFilter(RESP_FILT_01_4HZ);
    Resp_setGain  (RESP_GAIN_AUTO);    //(RESP_GAIN_1_2);
    Resp_setApneaLimit(RESP_APN_10SEC);

//    ARRH_setDefaults
//-------------------------------------------
    //Init queue
    osMessageQDef(MennenAlgoQueue, 1, ECG_PROCESS_BUFFER *);
    MennenQueueHandle = osMessageCreate(osMessageQ(MennenAlgoQueue), NULL);
    if (!MennenQueueHandle)
    	return;

    //Init task
    osThreadDef(mennenAlgoTask, mennenAlgoTask, osPriorityNormal, 0, MENNEN_ALGO_STACK_SIZE);
    mennenAlgoTaskHandle = osThreadCreate(osThread(mennenAlgoTask), NULL);
    if (!mennenAlgoTaskHandle)
		return;	
	
}
#endif



ECG_RESULT ecgAlgoInit(IN ECG_RESP_CONFIG_OBJECT_STRUCT* ecg_params)
{
#ifdef MENNEN_LIB
	ECG_resetProc();
	ECG_initProc(); 
	ECG_initArrh(); 	

    ECG_setFilter(ecg_params->ecg_output.bandpass_filter);
    //ECG_setNotch(ecg_params->ecg_output.notch_filter);
	ECG_setNotch(ENUM_ECG_PARAMS_FILTER_OFF); //According to Alex supposed to be default without filter

    // MATCH GAIN: algorithm always gets signal amplification x3
	ECG_setGain(ecg_mennen_algo_match_gain_table[ecg_params->ads.gain][0],ecg_mennen_algo_match_gain_table[ecg_params->ads.gain][1]);
//	ECG_setGain(1,1);
	ARRH_setDefaults ();
	
	ARRH_setRunLimit (ecg_params->ecg_algorithm.limit_runs);
	ARRH_setBgmLimit (ecg_params->ecg_algorithm.limit_bigeminy);
	ARRH_setPauseLimit (ecg_params->ecg_algorithm.limit_pause);
	ARRH_setVtachLimit (ecg_params->ecg_algorithm.limit_vtach);
	ARRH_setEbradLimit (ecg_params->ecg_algorithm.limit_ebrad);
	ARRH_setEtachLimit (ecg_params->ecg_algorithm.limit_etach);
#endif //MENNEN_LIB
	return ECG_RESULT_OK;
}

ECG_RESULT ecgRespirationAlgoInit(IN ECG_RESP_CONFIG_OBJECT_STRUCT* ecg_params)
{
#ifdef MENNEN_LIB
	Resp_resetProc();
	Resp_resetCalc();

//    Resp_setNotch(RESP_NOTCH_50HZ);
	if (ecg_params->respiration_output.notch_filter < ENUM_ECG_PARAMS_FILTER_AUTO)
		Resp_setNotch(ecg_params->respiration_output.notch_filter);
//    Resp_setFilter(RESP_FILT_01_4HZ);
	Resp_setFilter(ecg_params->respiration_output.bandpass_filter);
//	Resp_setGain(RESP_GAIN_AUTO);
//	Resp_setFractGain(1,1);	// TODO: needed? seen in example (Mennen - ECG algorithm\Shared folder\Prizma_VH\Prizma_VH_Feb_17_2017\ecg.c)
    Resp_setGain  (ecg_params->respiration_output.algo_gain);    //(RESP_GAIN_1_2);

//    Resp_setApneaLimit(RESP_APN_10SEC);
    Resp_setApneaLimit(ecg_params->respiration_algorithm.limit_apnea);
#endif //MENNEN_LIB
	return ECG_RESULT_OK;
}

ECG_RESULT ecgGetConfig(OUT ECG_RESP_CONFIG_OBJECT_STRUCT * config_out)
{
	if (config_out == NULL)
	{
		return ECG_RESULT_ERROR_WRONG_PARAMETER;
	}

	memcpy(config_out, &ecg_params, sizeof(ECG_RESP_CONFIG_OBJECT_STRUCT));

	return ECG_RESULT_OK;

}

static ECG_RESULT ecgSetAdsPatchHolterConfig()
{
	if (ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY)
	{
		ecg_params.ads.sample_rate = ADS_SAMPLE_RATE_250_HZ;
		ecg_params.ads.resp_chan_en = ADS_PARAMS_RESPIRATION_ENABLED_OFF;
	}
	else if(ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH)
	{
		ecg_params.ads.sample_rate = ADS_SAMPLE_RATE_500_HZ;
		ecg_params.ads.resp_chan_en = ADS_PARAMS_RESPIRATION_ENABLED_ON;
	}

	return ECG_RESULT_OK;
}

ECG_RESULT ecgSetConfig(IN ECG_RESP_CONFIG_OBJECT_STRUCT * config_in)
{
	if (config_in == NULL)
	{
		return ECG_RESULT_ERROR_WRONG_PARAMETER;
	}

	memcpy(&ecg_params, config_in, sizeof(ECG_RESP_CONFIG_OBJECT_STRUCT));

//	ecgSetAdsPatchHolterConfig();

	return ECG_RESULT_OK;
}


static ECG_RESULT prvEcgInit()
{
//	ecg_timer_flag = ECG_MEAS_COMP_STATUS_OK;

//	square_pulse_width = ECG_SQUARE_WAVE_WIDTH_SEC * ecg_params.ads.sample_rate == ADS_SAMPLE_RATE_250_HZ ? 250 : 500;
	if (ecg_params.ecg_output.lsb_weight == 0)
		ecgCalcLsbWeight(&ecg_params, &ecg_params.ecg_output.lsb_weight);

	if (ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH)
	{
		// TODO: is enought test
		if (ecg_params.ecg_output.bandpass_filter != ECG_PARAMS_BANDPASS_FILTER_OFF)
		{
			ecgAlgoInit(&ecg_params);
		}


		switch (ecg_params.respiration_output.output_frequency)
		{
		case ECG_PARAMS_RESPIRATION_OUTPUT_500HZ_COMPRESSED:		// filter off / on (+notch)
			if (ecg_params.respiration_output.bandpass_filter != ECG_PARAMS_RESPIRATION_BANDPASS_FILTER_OFF)
			{
				ecgRespirationAlgoInit(&ecg_params);
			}
			break;
		case ECG_PARAMS_RESPIRATION_OUTPUT_50HZ_COMPRESSED:			// filter on (+notch)
		case ECG_PARAMS_RESPIRATION_OUTPUT_RESPIRATION_RATE_ONLY:	// filter on (+notch)
			if (ecg_params.respiration_output.bandpass_filter == ECG_PARAMS_RESPIRATION_BANDPASS_FILTER_OFF)
				return ECG_RESULT_ERROR_WRONG_PARAMETER;
			ecgRespirationAlgoInit(&ecg_params);
			break;
		case ECG_PARAMS_RESPIRATION_OUTPUT_OFF:						// nothing to do
		default:
			break;
		}


		state = ECG_STATE_TEST_CONTACT_DETECTION;

		ecg_ads_alive_timeout = ECG_ADS_ALIVE_TIMEOUT_PATCH;

		ecgArrythmiasTrackChanges(NULL);	// RESET arrythmias
	}
	else if(ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER)
	{
		ecg_ads_alive_timeout = ECG_ADS_ALIVE_TIMEOUT_PATCH;	// same timing for filling buffer with samples (125 samples / 500 sps)
		state = ECG_STATE_TEST_TESTER;
	}
	else
	{
		ecg_ads_alive_timeout = ECG_ADS_ALIVE_TIMEOUT_HOLTER;
	    state = ECG_HOLTER_STATE_CONTACT_DETECTION;
	}
	
	if (ecg_params.general_config.pace_maker_enable == ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_ON)
	{
		LOG("Pace Maker INIT\r\n");
		if (prvEcgPmInit() != ECG_RESULT_OK)
			LOG("PM init failed\r\n");
	}
	ecg_buffer_time_msec = (float32_t)ECG_BUFFER_LENGTH/(ecg_params.ads.sample_rate == ADS_SAMPLE_RATE_250_HZ ? 250 : 500);

	return ECG_RESULT_OK;
}

static ECG_RESULT prvEcgImpedancePeriodicStart()
{
	if ( (ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH) && (ecg_params.general_config.impedance_period_min > 0) )
	{
		BaseType_t is_active = xTimerIsTimerActive(ecg_impedance_periodic_measurement_timer);
		if (!is_active)
			osTimerStart(ecg_impedance_periodic_measurement_timer, 1000 * 60 * ecg_params.general_config.impedance_period_min);
	}
}

static ECG_RESULT prvEcgStart()
{	
   meas_250_flag_first = TRUE;	// relevant only for patch

    lead_on_count = 0;

    ecgBufferSampleIndex = 0;
	ecgSampleBufferReset();

	ADS_RESULT_E res  = adsStart(&ecg_params.ads);

	if (res == ADS_RESULT_OK)
	{
#ifndef MENNEN_ALGO
		prvEcgAdsKeepAliveTimerStart(TRUE);
		prvEcgImpedancePeriodicStart();
#endif
	}
	
	if (ecg_params.general_config.pace_maker_enable == ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_ON)
	{
		LOG("Pace Maker Start\r\n");
		if (prvEcgPmStart() != ECG_RESULT_OK)
			LOG("PM init failed\r\n");
	}
	
	return (res == ADS_RESULT_OK ? ECG_RESULT_OK : ECG_RESULT_ERROR_ADS_DRIVER);
}
ECG_RESULT ecgImpedanceIntervalSet(uint16_t minutes)
{
	if (ecg_params.general_config.device_mode != ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	if (minutes >ECG_IMPEDANCE_PERIOD_MINUTES_MAX_VALID)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	BaseType_t is_active = xTimerIsTimerActive(ecg_impedance_periodic_measurement_timer);
	if (is_active)
		osTimerStop(ecg_impedance_periodic_measurement_timer);

	ecg_params.general_config.impedance_period_min = minutes;

	if (ecg_params.general_config.impedance_period_min > 0)
		osTimerStart(ecg_impedance_periodic_measurement_timer, 1000 * 60 * ecg_params.general_config.impedance_period_min);

}
ECG_RESULT ecgStart()
{
	ECG_RESULT res = prvEcgInit();
	if (res != ECG_RESULT_OK)
	{
		LOG("prvEcgInit failed %d\r\n", res);
		return res;
	}

	num_of_samples = 0;	// reset upon first use, impedance restarts won't need sample count reset
	res = prvEcgStart();
	if (res != ECG_RESULT_OK)
	{
		LOG("prvEcgStart failed %d\r\n", res);
	}
	return res;
}


static ECG_RESULT ecgChangeState(ECG_STATE new_state)
{
	if (state == new_state)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	state = new_state;

	osStatus ret = osMessagePut (ECGQueueHandle, 0, 0);
	if (ret != osOK)      //TBD - handle error
	    LOG_ERR("ecgChangeState osMessagePut:%d\r\n",ret);

	return ECG_RESULT_OK;
}

ECG_RESULT ecgSetDefaultParams(ECG_GENERAL_CONFIG_DEVICE_MODE_E device_mode, ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_ENUM pace_maker_en)
{
	if (device_mode >= ECG_GENERAL_CONFIG_DEVICE_MODE_MAX)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;
	if (pace_maker_en >= ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_MAX)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

   ecg_params.general_config.device_mode = device_mode;
   ecg_params.general_config.pace_maker_enable = pace_maker_en;
   
	// common configurations
	ecg_params.ads.gain = ECG_PARAMS_GAIN_DEFAULT;
	ecg_params.ads.respiration_gain = ECG_PARAMS_RESPIRATION_GAIN_DEFAULT;
	ecg_params.ads.respiration_phase_shift = ECG_PARAMS_RESPIRATION_PHASE_SHIFT_DEFAULT;
	ecg_params.ads.respiration_clock_freq = ECG_PARAMS_RESPIRATION_CLOCK_FREQ_DEFAULT;
	ecg_params.ads.init_type = ADS_PARAMS_INIT_TYPE_REGULAR;

	ecg_params.general_config.on_lead_off_event = ECG_PARAMS_ON_LEAD_OFF_DEFAULT;
	ecg_params.general_config.compression = ECG_PARAMS_DEFAULT_COMPRESSEION;
	ecg_params.general_config.copy_to_memory = ECG_PARAMS_DEFAULT_RECORD_TO_MEMORY;
	ecg_params.general_config.progress_ind_flow_control = ECG_PARAMS_DEFAULT_PROGRESS_INDICATIONS_FLOW_CONTROL;
	ecg_params.general_config.measurement_enable = ECG_PARAMS_DEFAULT_MEASUREMENT_ENABLE;
	ecg_params.general_config.send_events = 0;	// TODO: not defined in PBS doc
	ecg_params.general_config.impedance_period_min = ECG_IMPEDANCE_DEFAULT_PERIOD_MINUTES;
	ecg_params.general_config.impedance_settings = ECG_IMPEDANCE_SETTING_DEFAULT;

	// patch or holter only configurations
	if (ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY)
	{
		ecg_params.ads.sample_rate = ADS_SAMPLE_RATE_250_HZ;
		ecg_params.ads.resp_chan_en = ADS_PARAMS_RESPIRATION_ENABLED_OFF;
	}
	else if(ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER)
	{
		ecg_params.ads.sample_rate = ADS_SAMPLE_RATE_500_HZ;
		ecg_params.ads.resp_chan_en = ADS_PARAMS_RESPIRATION_ENABLED_ON;
	}
	else
	{
		ecg_params.ads.sample_rate = ADS_SAMPLE_RATE_500_HZ;
		ecg_params.ads.resp_chan_en = ADS_PARAMS_RESPIRATION_ENABLED_ON;

		ecg_params.ecg_output.output_frequency = ECG_PARAMS_OUTPUT_FREQ_DEFAULT;
		ecg_params.ecg_output.notch_filter = ECG_PARAMS_FILTER_DEFAULT;
		ecg_params.ecg_output.bandpass_filter = ECG_PARAMS_BANDPASS_FILTER_DEFAULT;
		ecg_params.ecg_output.peaks_output = ECG_PARAMS_PEAKS_OUTPUT_DEFAULT;

		ecg_params.respiration_output.output_frequency = ECG_PARAMS_RESPIRATION_OUTPUT_DEFAULT;
		ecg_params.respiration_output.notch_filter = ECG_PARAMS_RESPIRATION_NOTCH_FILTER_DEFAULT;
		ecg_params.respiration_output.bandpass_filter = ECG_PARAMS_RESPIRATION_BANDPASS_FILTER_DEFAULT;
		ecg_params.respiration_output.algo_gain = ECG_PARAMS_RESPIRATION_ALGO_GAIN_DEFAULT;

		ecg_params.ecg_algorithm.limit_runs = ECG_PARAMS_ALGORITHM_DEFAULT_LIMIT_RUNS;
		ecg_params.ecg_algorithm.limit_bigeminy = ECG_PARAMS_ALGORITHM_DEFAULT_LIMIT_BIGEMINY;
		ecg_params.ecg_algorithm.limit_pause = ECG_PARAMS_ALGORITHM_DEFAULT_LIMIT_PAUSE;
		ecg_params.ecg_algorithm.limit_vtach = ECG_PARAMS_ALGORITHM_DEFAULT_LIMIT_VTACH;
		ecg_params.ecg_algorithm.limit_ebrad = ECG_PARAMS_ALGORITHM_DEFAULT_LIMIT_EBRAD;
		ecg_params.ecg_algorithm.limit_etach = ECG_PARAMS_ALGORITHM_DEFAULT_LIMIT_ETACH;

		ecg_params.respiration_algorithm.limit_apnea = ECG_PARAMS_RESPITAION_ALGORITHM_DEFAULT_LIMIT_APNEA;
		ecg_params.respiration_algorithm.respiration_rate_max = ECG_PARAMS_RESP_ALGO_RESP_RATE_MAX;
		ecg_params.respiration_algorithm.respiration_rate_min = ECG_PARAMS_RESP_ALGO_RESP_RATE_MIN;

		ecg_params.ecg_output.output_filter = ECG_PARAMS_OUTPUT_FILTER_DEFAULT;
	}

	return ECG_RESULT_OK;
}



ECG_RESULT ecgStop()
{
	prvEcgAdsKeepAliveTimerStart(FALSE);
	if (xTimerIsTimerActive(ecg_impedance_periodic_measurement_timer))
	{
		osTimerStop(ecg_impedance_periodic_measurement_timer);
	}
	ADS_RESULT_E res = adsStop();

	if (res != ADS_RESULT_OK)
		return ECG_RESULT_ERROR_ADS_DRIVER;

	return ECG_RESULT_OK;

}


void ecgTimerCallback(TimerHandle_t xTimer)
{
   if (ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH)
   {
      if ((state == ECG_STATE_TEST_PROGRESS) || (state == ECG_STATE_TEST_CONTACT_DETECTION) )
      {
   //		ecg_timer_flag = ECG_MEAS_COMP_STATUS_NO_RESP_SENSOR;
   //		ecgChangeState(ECG_STATE_ERROR_COMPLETED);
      }
      else
      {
         LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ecgTimerCallback out of measurement state");
      }
   }
   else	
      LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ecgTimerCallback out of measurement state");
   
}

uint32_t pm_count = 0;

void adsSampleReceived(int32_t ecg1_sample,int32_t ecg2_sample, int32_t resp_sample ,uint8_t lead)
{
	memcpy(&ecgSampleBuffer[ecgBufferIndex].ecg_lead1_samples.buff.raw[ecgBufferSampleIndex * ECG_SAMPLE_24_BIT_SIZE], &ecg1_sample, ECG_SAMPLE_24_BIT_SIZE);
	ecgSampleBuffer[ecgBufferIndex].ecg_lead1_samples.buff_byte_length += ECG_SAMPLE_24_BIT_SIZE;

	memcpy(&ecgSampleBuffer[ecgBufferIndex].ecg_lead2_samples.buff.raw[ecgBufferSampleIndex * ECG_SAMPLE_24_BIT_SIZE], &ecg2_sample, ECG_SAMPLE_24_BIT_SIZE);
	ecgSampleBuffer[ecgBufferIndex].ecg_lead2_samples.buff_byte_length += ECG_SAMPLE_24_BIT_SIZE;
	
   if ( (ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH) ||
		   (ecg_params.general_config.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER) )
   {	
      memcpy(&ecgSampleBuffer[ecgBufferIndex].resp_samples.buff.raw[ecgBufferSampleIndex * ECG_SAMPLE_24_BIT_SIZE], &resp_sample, ECG_SAMPLE_24_BIT_SIZE);
      ecgSampleBuffer[ecgBufferIndex].resp_samples.buff_byte_length += ECG_SAMPLE_24_BIT_SIZE;
   }

    if(PMattr.PMdetected ==TRUE)	
   	{
	   pm_count++;
//        TP2_TOOGLE
		PMattr.PMdetected = FALSE;
		if(ecgSampleBuffer[ecgBufferIndex].pmLocation_1 == PM_POSITION_NOT_ASSIGNED)
 			 ecgSampleBuffer[ecgBufferIndex].pmLocation_1 = ecgBufferSampleIndex;
//		else if(ecgSampleBuffer[ecgBufferIndex].pmLocation_2 == PM_POSITION_NOT_ASSIGNED)	// PMattr.PMdetected gives indication for the whole Pace detection
//          	 ecgSampleBuffer[ecgBufferIndex].pmLocation_2 = ecgBufferSampleIndex;
	}

//	if (lead == FALSE)
//		ecgSampleBuffer[ecgBufferIndex].leads_status = 0;
	//ecgSampleBuffer[ecgBufferIndex].leads_status = 0;//|= lead;	// For Respiration Recording
	ecgSampleBuffer[ecgBufferIndex].leads_status |= lead;	// accumulate errors

	//advance sample index
	ecgBufferSampleIndex++;

	//Buffer finished
	if (ecgBufferSampleIndex >= ECG_BUFFER_LENGTH)
	{
		ecgSampleBuffer[ecgBufferIndex].pm_count = pm_count;
		//send buffer to queue
#ifdef MENNEN_ALGO
		osStatus ret = osMessagePut (MennenQueueHandle, (uint32_t)&ecgSampleBuffer[ecgBufferIndex], 0);
#else
		osStatus ret = osMessagePut (ECGQueueHandle, (uint32_t)&ecgSampleBuffer[ecgBufferIndex], 0);
#endif
		if (ret != osOK)      //TBD - handle error
		    LOG_ERR("adsSampleReceived osMessagePut:%d\r\n",ret);

		//advance index
		ecgBufferSampleIndex = 0;
		ecgBufferIndex = (ecgBufferIndex + 1) % NUMBER_OF_BUFFERS;
		ecgSampleBufferReset();
		pm_count = 0;
	}

}


ECG_RESULT ecgCompressBufferLen(IN uint8_t* ptr_data_in,
							   IN uint16_t data_in_len,
							   IN uint8_t data_in_step_size,
							   OUT uint8_t* ptr_data_out,
							   OUT uint16_t* data_out_len,
							   OUT uint8_t* ptr_bit_map,
							   OUT uint16_t* bit_map_len)							   
{
	uint16_t 		data_in_index = 0;
	uint16_t 		data_out_index = 0;
	int32_t			full_sample = 0;
	int32_t			next_sample = 0;
	uint8_t			bit_map[30] = {0};
	uint16_t		bit_map_index = 0;
	uint8_t			bit_flag = 0x40;
	int32_t			difference = 0;
	uint16_t 		data_in_len_orig = data_in_len;
	if ( (data_in_step_size != ECG_DELTA_COMPRESSION_FILTERED_SAMPLE_SIZE) && (data_in_step_size != ECG_DELTA_COMPRESSION_RAW_SAMPLE_SIZE))
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	*bit_map_len = 1;

	if (ptr_data_out != ptr_data_in)
		memcpy((uint8_t*)&ptr_data_out[data_out_index], (uint8_t*)&ptr_data_in[data_in_index], data_in_step_size);
	memcpy((uint8_t*)&full_sample, (uint8_t*)ptr_data_in, data_in_step_size);
	memcpy((uint8_t*)&next_sample, (uint8_t*)&ptr_data_in[data_in_step_size], data_in_step_size);
	
	data_out_index += data_in_step_size;
	
	while(data_in_len_orig > data_in_index)
	{
		difference = next_sample - full_sample;
		
		*data_out_len = data_out_index;
		
		if ((difference <= 127) && (difference >= -128))
		{
			ptr_data_out[data_out_index++] = difference;
			bit_map[bit_map_index] |= bit_flag;			
		}
		else
		{
			if (data_out_index == data_in_len_orig)
			{
				*data_out_len = data_out_index;
				break;
			}			
			memcpy((uint8_t*)&ptr_data_out[data_out_index], (uint8_t*)&next_sample, data_in_step_size);
			data_out_index += data_in_step_size;
		}
		
		if (bit_flag == 0x01)
		{
			bit_map_index++;
			bit_flag = 0x80;
			*bit_map_len = bit_map_index + 1;
		}
		else
			bit_flag >>=1;
		
		data_in_index += data_in_step_size;
		
		memcpy((uint8_t*)&full_sample, (uint8_t*)&next_sample, data_in_step_size);
		memcpy((uint8_t*)&next_sample, (uint8_t*)&ptr_data_in[data_in_index + data_in_step_size], data_in_step_size);
		
	}
	
	memcpy(ptr_bit_map, bit_map, *bit_map_len);
	
	
	return ECG_RESULT_OK;
}



// Filter feed and storage: 24 bit sample in -> 16 bit sample out
static ECG_RESULT prvEcgFilter500Hz(IN uint16_t length, IN ECG_PROCESS_BUFFER * samples_in, IN BOOL is_store_output_inplcae, IN ECG_LEAD_STATUS_ENUM leads)
{
	int32_t tmp_samples[2], tmp_resp;
	int32_t i;
	int  pace_flag;
	BOOL PMinChunk;

	if (samples_in == NULL)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	uint8_t loff_bits = 0;
	switch (leads)
	{
	case ECG_LEAD_STATUS_LEAD_1_ON:
		loff_bits = LOFF_BIT1;
		break;
	case ECG_LEAD_STATUS_LEAD_2_ON:
		loff_bits = LOFF_BIT0;
		break;
	default:
		break;
	}

 	if((samples_in->pmLocation_1 == PM_POSITION_NOT_ASSIGNED) && (samples_in->pmLocation_2 == PM_POSITION_NOT_ASSIGNED))
    {
    	PMinChunk = FALSE;
        pace_flag   = 0;
    }
    else
    {
        if(samples_in->pmLocation_2 == PM_POSITION_NOT_ASSIGNED)       //pacemaker pulse length is one sample length
            samples_in->pmLocation_2 = samples_in->pmLocation_1;
    	LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "** PM start %d\tend %d\tcount %d\r\n", samples_in->pmLocation_1, samples_in->pmLocation_2, samples_in->pm_count);
       	PMinChunk = TRUE;
    }	
       	
#ifdef MENNEN_LIB
	ECG_resetPaceLocat(); //!!!
#endif //MENNEN_LIB
	for (i = 0;i < length;i++)
	{
        tmp_samples[0] = ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(&samples_in->ecg_lead1_samples, i);
        tmp_samples[1] = ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(&samples_in->ecg_lead2_samples, i);
        tmp_resp = ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(&samples_in->resp_samples, i);
#ifdef MENNEN_LIB
        Resp_procSample(&tmp_resp, 0);    // TODO: check
#endif //MENNEN_LIB
		if(PMinChunk)
		{
           	if((i >=  samples_in->pmLocation_1) && (i <= samples_in->pmLocation_2))
				pace_flag   = 1;
			else
				pace_flag   = 0;
		}
#ifdef MENNEN_LIB
		ECG_procSample(tmp_samples, loff_bits, pace_flag);
		//fixate pace position for ananlyis
		ECG_latchPaceLocat(pace_flag, i);
#endif //MENNEN_LIB
        if (is_store_output_inplcae == TRUE)
		{
			//copy samples to short format buffer
//			memcpy(&samples_out[i * sizeof(int16_t)], &tmp_samples[0], sizeof(int16_t));
//			*samples_out_length += sizeof(int16_t);
			samples_in->ecg_lead1_samples.buff.sh[i] = tmp_samples[0];
			samples_in->ecg_lead2_samples.buff.sh[i] = tmp_samples[1];
			samples_in->resp_samples.buff.sh[i] = tmp_resp;
		}
	}

	// fix length
    if (is_store_output_inplcae == TRUE)
	{
		samples_in->ecg_lead1_samples.buff_byte_length = length * ECG_FILTERED_SAMPLE_SIZE;
		samples_in->ecg_lead2_samples.buff_byte_length = length * ECG_FILTERED_SAMPLE_SIZE;
		samples_in->resp_samples.buff_byte_length = length * ECG_FILTERED_SAMPLE_SIZE;
	}

	return ECG_RESULT_OK;
}

static ECG_RESULT prvEcgParseBeatsLocations(uint16_t beats_count,uint32_t total_samples_count, IN ECG_BEAT * beats_struct, ECG_MEAS_PROGRESS_PEAKS_STRUCT * peaks)
{
	uint8_t i;

	if ((beats_struct == NULL) || (peaks == NULL) )
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	if (beats_count == 0)
	{
		peaks->peaks_len = 0;
		return ECG_RESULT_OK;
	}
	if (beats_count>ECG_PEAKS_MAX_LENGTH)
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2,"beats_count exceeds max -> limit to max\r\n");
		beats_count = ECG_PEAKS_MAX_LENGTH;
	}
	for(i = 0; i< beats_count; i++)
	{
		peaks->ecgPeakIndex[i] = (int32_t)total_samples_count + beats_struct[i].beatLocation;
		if (ecg_params.ecg_output.output_frequency == ECG_PARAMS_OUTPUT_FREQ_250)
			peaks->ecgPeakIndex[i] /= 2;
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "BEAT %d index %d\r\n", i, peaks->ecgPeakIndex[i] /*beats_struct[i].beatLocation*/);
		peaks->peaks_len = i + 1;
	}
	return ECG_RESULT_OK;
}


// for debug
static ECG_RESULT ecgGetBeatParameters(uint16_t beatsNumber, uint16_t hr, IN ECG_BEAT	* beatParametersStore)
{
#if 0
	uint8_t i;
	if (beatsNumber > ECG_PEAKS_MAX_LENGTH)
	{
		LOG("BEATS: Beats store overflow!!!!\r\n");
	}
	else if (beatsNumber == 0)
	{
		LOG("BEATS: none \r\n");
	}
	else
	{
		for (i = 0; i < beatsNumber; i++)
			LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "BEATS: Number = %d   R offset = %d R-R = %d HR = %d \r\n", i + 1,
					beatParametersStore[i].beatLocation, beatParametersStore[i].rrInterval, hr);
	}
#endif
	return ECG_RESULT_OK;
}

ECG_RESULT ecgGetArrh(ECG_ALGO_ARRYTHMIA_EVENT_HEADER_STRUCT* arrh_bitmask)
{
	
	*arrh_bitmask = arrhythmias_mask;
	
	return ECG_RESULT_OK;
}


static ECG_RESULT prvEcgParseArrithEvents(IN ECG_ARRH *Arrh, OUT ECG_ALGO_RESULTS_STRUCT * algoResults, OUT ECG_ALGO_ARRYTHMIA_EVENT_STRUCT * arrhythEvent)
{
	BOOL detected = FALSE;
    uint8_t i;
    
    if ( (Arrh == NULL) || (algoResults == NULL) || (arrhythEvent == NULL))
    	return ECG_RESULT_ERROR_WRONG_PARAMETER;

 	for(i=0;i< sizeof(ECG_ARRH); i++)
	{
		if(((uint8_t *)Arrh)[i] != 0)
        {
            detected = TRUE;
	 		break;
        }
	}

   if(!detected)
   	{
   		algoResults->arrhythmia = 0;
   		memset(arrhythEvent, 0, sizeof(ECG_ALGO_ARRYTHMIA_EVENT_STRUCT));
 		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2,"---- none\r\n");
        return ECG_RESULT_OK;
 	}

    arrhythEvent->events_flags.raw = 0; // TODO: clear ?

   	if(Arrh->asyst)   				// boolean flag
  	{		
  		arrhythEvent->events_flags.bitmap.ventricular_asystole = 1;
		arrhythmias_mask.bitmap.ventricular_asystole = 1;
   	}
   	if(Arrh->vfib)   				// boolean flag
   	{
   		arrhythEvent->events_flags.bitmap.ventricular_fibrillation = 1;
		arrhythmias_mask.bitmap.ventricular_fibrillation = 1;
   	}
    if(Arrh->vtach)   				// boolean flag
   	{
   		arrhythEvent->events_flags.bitmap.ventricular_tachycardia = 1;
		arrhythmias_mask.bitmap.ventricular_tachycardia = 1;
   	}
    if(Arrh->pause)   				// boolean flag
   	{
   		arrhythEvent->events_flags.bitmap.pause = 1;
   	}
    if(Arrh->ebrad)   				// bradicardia
   	{
   		arrhythEvent->events_flags.bitmap.bradicardia = 1;
		arrhythmias_mask.bitmap.bradicardia = 1;
   	}
    if(Arrh->etach)   				// bradicardia
   	{
   		arrhythEvent->events_flags.bitmap.tachycardia = 1;
		arrhythmias_mask.bitmap.tachycardia = 1;
   	}
    if(Arrh->irrythm)   			// irregular rhythm
   	{
   		arrhythEvent->events_flags.bitmap.irregular_rhythm = 1;
		arrhythmias_mask.bitmap.irregular_rhythm = 1;
   	} 
    if(Arrh->idrythm)   			// Ideoventricular rhythm 
   	{
   		arrhythEvent->events_flags.bitmap.ideoventricular_rhythm = 1;
		arrhythmias_mask.bitmap.ideoventricular_rhythm = 1;
   	}
    if(Arrh->svtach)   				// Supravent. Tachycardia
   	{
   		arrhythEvent->events_flags.bitmap.supravent_tachycardia = 1;
		arrhythmias_mask.bitmap.supravent_tachycardia = 1;
   	}

   	if(Arrh->runs)   				// runs per minute
   	{
   		arrhythEvent->runs = Arrh->runs;
		arrhythmias_mask.bitmap.runs = 1;
   	}
   	if(Arrh->pvc)   				// beats per minute
   	{
   		arrhythEvent->pvc = Arrh->pvc;
		arrhythmias_mask.bitmap.pvc = 1;
   	}
   	if(Arrh->bigem)   				// bigemeny complexes per minute
   	{
   		arrhythEvent->bigeminy = Arrh->bigem;
		arrhythmias_mask.bitmap.bigeminy = 1;
   	}
   	if(Arrh->couplet)   			// couplet of pvc's
   	{
   		arrhythEvent->couplet_pvcs = Arrh->couplet;
		arrhythmias_mask.bitmap.couplet_pvcs = 1;
   	}   	
   	if(Arrh->triplet)   			// triplet of pvc's
   	{
   		arrhythEvent->triplet_pvcs = Arrh->triplet;
		arrhythmias_mask.bitmap.triplet_pvcs = 1;
   	}
   	if(Arrh->trigem)   				// trigemeny
   	{
   		arrhythEvent->trigeminy = Arrh->trigem;
		arrhythmias_mask.bitmap.trigeminy = 1;
   	} 
    if(Arrh->ront)   				// R on T
   	{
   		arrhythEvent->r_on_t = Arrh->ront;
		arrhythmias_mask.bitmap.r_on_t = 1;
   	}	   	
    if(Arrh->pac)   				// premature atrial contraction
   	{
   		arrhythEvent->premature_atrial_contraction = Arrh->pac;
		arrhythmias_mask.bitmap.premature_atrial_contraction = 1;
   	}   	
    if(Arrh->mpvc)   				//multifocal pvc
   	{
   		arrhythEvent->multifocal_of_pvc = Arrh->mpvc;
		arrhythmias_mask.bitmap.multifocal_of_pvc = 1;
   	}
    if(Arrh->ipvc)   				//interpolated pvc's
   	{
   		arrhythEvent->interpolated_pvc = Arrh->ipvc;
		arrhythmias_mask.bitmap.interpolated_pvc = 1;
   	}


 	algoResults->arrhythmia = 1;
 	return ECG_RESULT_ARRYTHMIA_FOUND;
}
 


ECG_RESULT ecgArrythmiasTrackChanges(IN ECG_ALGO_ARRYTHMIA_EVENT_STRUCT * arrs_in)
{
	static ECG_ALGO_ARRYTHMIA_EVENT_STRUCT arrs_local;

	ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT change_out;
	ecg_event_s e;
	e.event_type = ECG_EVENT_ARRYTHMIA_CAHNGED;
	e.payloadPtr.arrythmia_change = &change_out;
	e.test_params = NULL;

	uint16_t dbg_cnt = 0;

	if (arrs_in == NULL) // RESET
	{
		memset(&arrs_local, 0, sizeof(arrs_local));
		return ECG_RESULT_OK;
	}

	if (((arrs_local.runs) && (!arrs_in->runs)) || ((!arrs_local.runs) && (arrs_in->runs)))
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.runs = 1;
		if (arrs_local.runs)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.runs = arrs_in->runs;
		dbg_cnt++;
	}
#ifndef GMP_DEMO_CHINA
//	if (arrs_local.pvc != arrs_in->pvc)
	if ((arrs_local.pvc && !arrs_in->pvc) || (!arrs_local.pvc && arrs_in->pvc))
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.pvc = 1;
		if (arrs_local.pvc)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.pvc = arrs_in->pvc;
		dbg_cnt++;
	}
#endif //GMP_DEMO_CHINA
//	if (arrs_local.bigeminy != arrs_in->bigeminy)
	if ((arrs_local.bigeminy && !arrs_in->bigeminy) || (!arrs_local.bigeminy && arrs_in->bigeminy))
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.bigeminy = 1;
		if (arrs_local.bigeminy)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.bigeminy = arrs_in->bigeminy;
		dbg_cnt++;
	}
//	if (arrs_local.couplet_pvcs != arrs_in->couplet_pvcs)
	if (((arrs_local.couplet_pvcs) && (!arrs_in->couplet_pvcs)) || ((!arrs_local.couplet_pvcs) && (arrs_in->couplet_pvcs)))
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.couplet_pvcs = 1;
		if (arrs_local.couplet_pvcs)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.couplet_pvcs = arrs_in->couplet_pvcs;
		dbg_cnt++;
	}
	if (arrs_local.triplet_pvcs != arrs_in->triplet_pvcs)
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.triplet_pvcs = 1;
		if (arrs_local.triplet_pvcs)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.triplet_pvcs = arrs_in->triplet_pvcs;
		dbg_cnt++;
	}
//	if (arrs_local.trigeminy != arrs_in->trigeminy)
	if ((arrs_local.trigeminy && !arrs_in->trigeminy) || (!arrs_local.trigeminy && arrs_in->trigeminy) )
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.trigeminy = 1;
		if (arrs_local.trigeminy)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.trigeminy = arrs_in->trigeminy;
		dbg_cnt++;
	}
	if (arrs_local.r_on_t != arrs_in->r_on_t)
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.r_on_t = 1;
		if (arrs_local.r_on_t)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.r_on_t = arrs_in->r_on_t;
	}
//	if (arrs_local.premature_atrial_contraction != arrs_in->premature_atrial_contraction)
	if ((arrs_local.premature_atrial_contraction && !arrs_in->premature_atrial_contraction) || (!arrs_local.premature_atrial_contraction && arrs_in->premature_atrial_contraction))
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.premature_atrial_contraction = 1;
		if (arrs_local.premature_atrial_contraction)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.premature_atrial_contraction = arrs_in->premature_atrial_contraction;
		dbg_cnt++;
	}
//	if (arrs_local.multifocal_of_pvc != arrs_in->multifocal_of_pvc)
	if ((arrs_local.multifocal_of_pvc && !arrs_in->multifocal_of_pvc) || (!arrs_local.multifocal_of_pvc && arrs_in->multifocal_of_pvc))
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.multifocal_of_pvc = 1;
		if (arrs_local.multifocal_of_pvc)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.multifocal_of_pvc = arrs_in->multifocal_of_pvc;
		dbg_cnt++;
	}
	if (arrs_local.interpolated_pvc != arrs_in->interpolated_pvc)
	{
		change_out.arr.raw = 0;
		change_out.arr.bitmap.interpolated_pvc = 1;
		if (arrs_local.interpolated_pvc)
			change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
		else
			change_out.change = ECG_ARRYTHMIA_CHANGE_START;
		ecg_event(&e);
		arrs_local.interpolated_pvc = arrs_in->interpolated_pvc;
		dbg_cnt++;
	}

	if (arrs_local.events_flags.raw != arrs_in->events_flags.raw)
	{
#ifdef DISABLE_FALSE_ARRHYTHMIAS
		if (arrs_local.events_flags.bitmap.ventricular_asystole != arrs_in->events_flags.bitmap.ventricular_asystole)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.ventricular_asystole = 1;
			if (arrs_local.events_flags.bitmap.ventricular_asystole)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.ventricular_asystole = arrs_in->events_flags.bitmap.ventricular_asystole;
			dbg_cnt++;
		}
#endif //DISABLE_FALSE_ARRHYTHMIAS
		if (arrs_local.events_flags.bitmap.ventricular_fibrillation != arrs_in->events_flags.bitmap.ventricular_fibrillation)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.ventricular_fibrillation = 1;
			if (arrs_local.events_flags.bitmap.ventricular_fibrillation)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.ventricular_fibrillation = arrs_in->events_flags.bitmap.ventricular_fibrillation;
			dbg_cnt++;
		}
		if (arrs_local.events_flags.bitmap.ventricular_tachycardia != arrs_in->events_flags.bitmap.ventricular_tachycardia)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.ventricular_tachycardia = 1;
			if (arrs_local.events_flags.bitmap.ventricular_tachycardia)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.ventricular_tachycardia = arrs_in->events_flags.bitmap.ventricular_tachycardia;
			dbg_cnt++;
		}
#ifdef DISABLE_FALSE_ARRHYTHMIAS1
		if (arrs_local.events_flags.bitmap.pause != arrs_in->events_flags.bitmap.pause)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.pause = 1;
			if (arrs_local.events_flags.bitmap.pause)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.pause = arrs_in->events_flags.bitmap.pause;
			dbg_cnt++;
		}
#endif //DISABLE_FALSE_ARRHYTHMIAS
		if (arrs_local.events_flags.bitmap.bradicardia != arrs_in->events_flags.bitmap.bradicardia)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.bradicardia = 1;
			if (arrs_local.events_flags.bitmap.bradicardia)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.bradicardia = arrs_in->events_flags.bitmap.bradicardia;
			dbg_cnt++;
		}
		if (arrs_local.events_flags.bitmap.tachycardia != arrs_in->events_flags.bitmap.tachycardia)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.tachycardia = 1;
			if (arrs_local.events_flags.bitmap.tachycardia)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.tachycardia = arrs_in->events_flags.bitmap.tachycardia;
			dbg_cnt++;
		}
		if (arrs_local.events_flags.bitmap.irregular_rhythm != arrs_in->events_flags.bitmap.irregular_rhythm)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.irregular_rhythm = 1;
			if (arrs_local.events_flags.bitmap.irregular_rhythm)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.irregular_rhythm = arrs_in->events_flags.bitmap.irregular_rhythm;
			dbg_cnt++;
		}
		if (arrs_local.events_flags.bitmap.ideoventricular_rhythm != arrs_in->events_flags.bitmap.ideoventricular_rhythm)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.ideoventricular_rhythm = 1;
			if (arrs_local.events_flags.bitmap.ideoventricular_rhythm)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.ideoventricular_rhythm = arrs_in->events_flags.bitmap.ideoventricular_rhythm;
			dbg_cnt++;
		}
		if (arrs_local.events_flags.bitmap.supravent_tachycardia != arrs_in->events_flags.bitmap.supravent_tachycardia)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.supravent_tachycardia = 1;
			if (arrs_local.events_flags.bitmap.supravent_tachycardia)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.supravent_tachycardia = arrs_in->events_flags.bitmap.supravent_tachycardia;
			dbg_cnt++;
		}
		if (arrs_local.events_flags.bitmap.resp_ecg_coincidence != arrs_in->events_flags.bitmap.resp_ecg_coincidence)
		{
			change_out.arr.raw = 0;
			change_out.arr.bitmap.resp_ecg_coincidence = 1;
			if (arrs_local.events_flags.bitmap.resp_ecg_coincidence)
				change_out.change = ECG_ARRYTHMIA_CHANGE_STOP;
			else
				change_out.change = ECG_ARRYTHMIA_CHANGE_START;
			ecg_event(&e);
			arrs_local.events_flags.bitmap.resp_ecg_coincidence = arrs_in->events_flags.bitmap.resp_ecg_coincidence;
			dbg_cnt++;
		}
	}

	LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "\r\necgArrythmiasTrackChanges count=%d\r\n", dbg_cnt);

	return ECG_RESULT_OK;
}


ECG_RESULT ecgAccumulateArrythmias(IN ECG_ALGO_ARRYTHMIA_EVENT_STRUCT * arrs_in, OUT ECG_ALGO_ARRYTHMIA_EVENT_STRUCT * arrs_out)
{
	if ( (arrs_in == NULL) || (arrs_out == NULL) )
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	arrs_out->events_flags.raw |= arrs_in->events_flags.raw;

	if (arrs_in->runs != 0) arrs_out->runs = arrs_in->runs ;
  	if (arrs_in->pvc != 0) arrs_out->pvc = arrs_in->pvc;
  	if (arrs_in->bigeminy != 0) arrs_out->bigeminy = arrs_in->bigeminy;
  	if (arrs_in->couplet_pvcs != 0) arrs_out->couplet_pvcs = arrs_in->couplet_pvcs;
  	if (arrs_in->triplet_pvcs != 0) arrs_out->triplet_pvcs = arrs_in->triplet_pvcs;
  	if (arrs_in->trigeminy != 0) arrs_out->trigeminy = arrs_in->trigeminy;
  	if (arrs_in->r_on_t != 0) arrs_out->r_on_t = arrs_in->r_on_t;
  	if (arrs_in->premature_atrial_contraction != 0) arrs_out->premature_atrial_contraction = arrs_in->premature_atrial_contraction;
  	if (arrs_in->multifocal_of_pvc != 0) arrs_out->multifocal_of_pvc = arrs_in->multifocal_of_pvc;
  	if (arrs_in->interpolated_pvc != 0) arrs_out->interpolated_pvc = arrs_in->interpolated_pvc;

	return ECG_RESULT_OK;
}


static ECG_RESULT prvEcgRunECGAlgorithm(IN ECG_PROCESS_BUFFER * samples, IN uint32_t total_samples_count, OUT ECG_ALGO_RESULTS_STRUCT * algo_results, OUT ECG_ALGO_ARRYTHMIA_EVENT_STRUCT * arrythmias)
{
#ifdef MENNEN_LIB
	uint8_t 		heart_rate_found, beats_count;
	ECG_BEAT		beats[ECG_PEAKS_MAX_LENGTH];
	ECG_ARRH 		arrhResult;
	ECG_STAT		algo_stat;

	if ( (samples == NULL) || (algo_results == NULL) /*|| (arrythmais == NULL)*/ )
		return ECG_RESULT_ERROR_WRONG_PARAMETER;
	if ((samples->leads_status != ADS_LEAD_STATUS_LL) &&  (samples->leads_status != ADS_LEAD_STATUS_LA))
		ECG_execArrh(samples->ecg_lead1_samples.buff.sh, samples->ecg_lead2_samples.buff.sh, 0, 0);

    heart_rate_found = ECG_getHRate(&algo_results->heart_rate);

    LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "algo_results->heart_rate = %d\r\n", algo_results->heart_rate);
    if (heart_rate_found == FALSE)
    	algo_results->heart_rate = ECG_INVALID_VALUE;

    // TODO: get arrythmias and peaks
    beats_count = ECG_getBeat(beats);
    prvEcgParseBeatsLocations(beats_count, total_samples_count, beats, &algo_results->peaks);
    //ecgGetBeatParameters(beats_count, beats);
    

    ECG_getStat(&algo_stat);
	LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2,"algo_stat: learn %d, hr_valid %d, artifact %d, low amp %d, unstable %d\r\n",
			algo_stat.learn_on, algo_stat.hr_valid, algo_stat.artifact, algo_stat.low_ampl ,algo_stat.unstable);
    if (algo_stat.learn_on || !algo_stat.hr_valid || algo_stat.artifact || algo_stat.low_ampl || algo_stat.unstable)
    {
    	algo_results->arrhythmia = 0;
   		memset(arrythmias, 0, sizeof(ECG_ALGO_ARRYTHMIA_EVENT_STRUCT));

    	return ECG_RESULT_ALGORITHM_NOT_READY;
    }

    ECG_getArrh(&arrhResult);

    if (arrythmias == NULL)	// skip parsing if not desired
    	return ECG_RESULT_OK;

    // prvEcgRunECGAlgorithm runs also in HR which currently doesn't give arrythmias
    prvEcgParseArrithEvents(&arrhResult, algo_results, arrythmias);
#endif //MENNEN_LIB
	return ECG_RESULT_OK;
}


static ECG_RESULT prvEcgRunRespirationAlgorithm(IN ECG_MEAS_PROGRESS_DATA_STRUCT * samples, OUT ECG_RESPIRATION_ALGO_RESULTS_STRUCT * algo_results)
{
#ifdef MENNEN_LIB
	
	uint8_t respiration_found, apnea_found;
	int16_t strob_pos;
	int16_t apnea;

	if ( (samples == NULL) || (algo_results == NULL) )
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	// Resp_execCalc should run at 500 sps
	strob_pos = Resp_execCalc(samples->buff.sh, ECG_BUFFER_LENGTH);
	if(strob_pos >=0) { strob_pos /=10; }	// match 50 hz display

	Resp_applyGain(samples->buff.sh, ECG_BUFFER_LENGTH);
    respiration_found = Resp_getBreathRate(&algo_results->respiration_rate);


    // TODO: get apeas coincidence
    /*algo_results->apnea_time*/ apnea_found = Resp_getApneaTime(&apnea);
    if (apnea_found)
    	algo_results->apnea_time = apnea;
    else
    	algo_results->apnea_time = ECG_INVALID_VALUE;

    LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "--RESP = %d, res = %d, apnea = %d, apnea_found=%d\r\n", algo_results->respiration_rate, respiration_found, apnea, apnea_found);
//	send_tx_data(pBytes, sizeof(short));
//	//pBytes += sizeof(short);
//
//	Resp_getStat((RESP_STAT*)pBytes);
//	send_tx_data(pBytes, sizeof(RESP_STAT));
//	//pBytes += sizeof(RESP_STAT);
//
//	Resp_getConf((RESP_CONF*)pBytes);
//	send_tx_data(pBytes, sizeof(RESP_CONF));
//	//pBytes += sizeof(RESP_CONF);
//
//	*(short*)pBytes = Resp_ECG_coincidence(breath_rate, heart_rate);
//	send_tx_data(pBytes, sizeof(short));

#endif //MENNEN_LIB
	return ECG_RESULT_OK;
}

/*
 * prvEcgGetAdsLeads
 *
 * ADS_LEAD_STATUS_ALL_ON	= 0,
	ADS_LEAD_STATUS_LA		= 0x01,
	ADS_LEAD_STATUS_RA		= 0x02,
	ADS_LEAD_STATUS_LL		= 0x04,
	ADS_LEAD_STATUS_RL		= 0x10
	*/
static ECG_RESULT prvEcgGetAdsLeads(IN ADS_LEAD_STATUS ads_in, OUT ECG_LEAD_STATUS_ENUM * out)
{
	if (out == NULL)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;
	if (ads_in == ADS_LEAD_STATUS_ALL_ON)
	{
		*out = ECG_LEAD_STATUS_LEAD_1_ON | ECG_LEAD_STATUS_LEAD_2_ON;
		return ECG_RESULT_OK;
	}

	*out = ECG_LEAD_STATUS_OFF;

	if ( ( (ads_in & ADS_LEAD_STATUS_LA) == 0) && ( (ads_in & ADS_LEAD_STATUS_RA) == 0) )
	{
		*out |= ECG_LEAD_STATUS_LEAD_1_ON;
	}
	if ( ( (ads_in & ADS_LEAD_STATUS_LL) == 0) && ((ads_in & ADS_LEAD_STATUS_RA) == 0) )
	{
		*out |= ECG_LEAD_STATUS_LEAD_2_ON;
	}

	return ECG_RESULT_OK;
}

static ECG_RESULT prvEcgAdsKeepAliveTimerStart(uint8_t start)
{
	if (ecg_ads_alive_timer == NULL)
	{
		LOG("ECG TIMER not initialized\r\n");
		return ECG_RESULT_ERROR;

	}

	BaseType_t is_active = xTimerIsTimerActive(ecg_ads_alive_timer);

	if (start && !is_active)
	{
		osTimerStart(ecg_ads_alive_timer, ecg_ads_alive_timeout);
		ecg_timer_debug_count++;
	}
	else if (!start && is_active)
	{
		osTimerStop(ecg_ads_alive_timer);
		ecg_timer_debug_count--;
	}
	else if (start == 1)	// both 1
	{
//		osTimerStop(ecg_ads_alive_timer);
		osTimerStart(ecg_ads_alive_timer, ecg_ads_alive_timeout);	   
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ECG TIMER restart timer!\r\n");
	}
	else
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ECG TIMER wrong command: start=%d, is_active=%d\r\n",start, is_active);
		return ECG_RESULT_ERROR;
	}
	return ECG_RESULT_OK;
}

ECG_RESULT ecgGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (processECGBufTaHandle == NULL) || (ret_val == NULL) )
		return ECG_RESULT_ERROR_WRONG_PARAMETER;
	*ret_val = uxTaskGetStackHighWaterMark( processECGBufTaHandle );
	return ECG_RESULT_OK;
}

ECG_RESULT ecgMennenGetHighWaterMark(UBaseType_t * ret_val)
{
#ifdef MENNEN_ALGO
if ( (mennenAlgoTaskHandle == NULL) || (ret_val == NULL) )
		return ECG_RESULT_ERROR_WRONG_PARAMETER;
	*ret_val = uxTaskGetStackHighWaterMark( mennenAlgoTaskHandle );
	return ECG_RESULT_OK;
#else	//MENNEN_ALGO
	return ECG_RESULT_ERROR_NOT_INITIALIZED;
#endif	//MENNEN_ALGO
}





#define ECG_SQUARE_LENGTH	(ECG_BUFFER_LENGTH * 2 / 5)	// 125 samples = 0.5 sec, spec for square is 200 msec
static ECG_RESULT prvEcgSquareWaveTemplateGenerate()
{


	// NOT NEEDED
//	return ECG_RESULT_OK;


	static ECG_MEAS_PROGRESS_DATA_STRUCT  	signal;
	ecgCalcLsbWeight(&ecg_params, &ecg_params.ecg_output.lsb_weight);
	uint16_t mv_1 = 1000 / ecg_params.ecg_output.lsb_weight ;
	uint16_t i;

	LOG("prvEcgSquareWaveTemplateGenerate first template\r\n");
	// set 200 msec as 1 mv
	for (i=0; i<ECG_SQUARE_LENGTH; i++)
	{
		memcpy(&signal.buff.raw[i * ECG_SAMPLE_24_BIT_SIZE], &mv_1, ECG_SAMPLE_24_BIT_SIZE);
	}

	// set 200 msec as 0 mv
	memset(&signal.buff.raw[i * ECG_SAMPLE_24_BIT_SIZE], 0, (ECG_SQUARE_LENGTH * ECG_SAMPLE_24_BIT_SIZE) );
	i += ECG_SQUARE_LENGTH;

	// set last 100 msec as 1 mv
	for (; i<ECG_BUFFER_LENGTH; i++)
	{
		memcpy(&signal.buff.raw[i * ECG_SAMPLE_24_BIT_SIZE], &mv_1, ECG_SAMPLE_24_BIT_SIZE);
	}





	ECG_MEAS_PROGRESS_INDICATION_BASE_STRUCT prog_ind_common;

	prog_ind_common.ecg_index = num_of_samples;
	prog_ind_common.lead_status = 0;
	prog_ind_common.pace_maker.index_start = PM_POSITION_NOT_ASSIGNED;
	prog_ind_common.pace_maker.index_stop = PM_POSITION_NOT_ASSIGNED;


	ECG_MEAS_PROGRESS_EVENT_STRUCT event_rt;
	event_rt.ecg_lead_1 = &signal;
	event_rt.ecg_lead_2 = &signal;
	event_rt.common = &prog_ind_common;

	ecg_event_s						    			event;
	event.event_type = ECG_HOLTER_EVENT_MEASURING;
	event.payloadPtr.real_time_ind = &event_rt;
	event.test_params = &ecg_params;
	ecg_event(&event);



	LOG("second template\r\n");
	// second template

	// set first 100 msec as 1 mv
	for (i=0; i< ECG_SQUARE_LENGTH/2; i++)
	{
		memcpy(&signal.buff.raw[i * ECG_SAMPLE_24_BIT_SIZE], &mv_1, ECG_SAMPLE_24_BIT_SIZE);
	}

	LOG(" ");
	// reset next 200 msec as 0 mv
	memset(&signal.buff.raw[i* ECG_SAMPLE_24_BIT_SIZE], 0, (ECG_SQUARE_LENGTH * ECG_SAMPLE_24_BIT_SIZE) );
	i += ECG_SQUARE_LENGTH;

	// set last 200 msec as 1 mv
	for (; i< ECG_BUFFER_LENGTH; i++)
	{
		memcpy(&signal.buff.raw[i * ECG_SAMPLE_24_BIT_SIZE], &mv_1, ECG_SAMPLE_24_BIT_SIZE);
	}

	event.event_type = ECG_HOLTER_EVENT_MEASURING;
	event.payloadPtr.real_time_ind = &event_rt;
	event.test_params = &ecg_params;
	ecg_event(&event);

	return ECG_RESULT_OK;
}



// TODO: started and stashed a more complex implementation for different sample rates...

//
//#define ECG_SQUARE_WAVE_WIDTH_SEC		(0.2)	//200 msec (standard)
//
//typedef enum {
//	ECG_SQUARE_STAGE_LOW,
//	ECG_SQUARE_STAGE_HIGH
//}ECG_SQUARE_STAGE_E;
//
//static uint8_t square_pulse_width;
//static ECG_RESULT prvEcgSetSignalSquare(ECG_PROCESS_BUFFER * buff, uint16_t num_of_samples, uint8_t is_reset)
//{
//	if ( (buff == NULL) || (num_of_samples == 0) || (num_of_samples >= ECG_BUFFER_LENGTH))
//		return ECG_RESULT_ERROR_WRONG_PARAMETER;
//
//	static uint16_t sample_count;
//	static ECG_SQUARE_STAGE_E stage = ECG_SQUARE_STAGE_HIGH;
//
//	if (is_reset)
//		sample_count = 0;
//
//	uint16_t i = 0;
//	while(i < (num_of_samples * ECG_SAMPLE_24_BIT_SIZE) )
//	{
//		buff->ecg_lead1_samples.buff.raw[i] = (stage == ECG_SQUARE_STAGE_LOW ? 0 : 1000);
//		buff->ecg_lead2_samples.buff.raw[i] = (stage == ECG_SQUARE_STAGE_LOW ? 0 : 1000);
//
//		i++;
//
//		if ( (i % ECG_SAMPLE_24_BIT_SIZE) == 0)
//			sample_count++;
//
//		if (sample_count > square_pulse_width)
//		{
//			sample_count = 0;
//			if (stage == ECG_SQUARE_STAGE_LOW)
//				stage = ECG_SQUARE_STAGE_HIGH;
//			else
//				stage = ECG_SQUARE_STAGE_LOW;
//		}
//	}
//
//	return ECG_RESULT_OK;
//}




static ECG_RESULT prvEcgSetSignalSquare(ECG_PROCESS_BUFFER * buff, ECG_LEAD_STATUS_ENUM leads_status)
{
	static uint8_t template_index = 0;

	if (buff == NULL)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;


	if (leads_status == ECG_LEAD_STATUS_LEAD_2_ON)
		memcpy(buff->ecg_lead1_samples.buff.raw, holter_square_template[template_index], sizeof(buff->ecg_lead1_samples.buff.raw));
	else if(leads_status == ECG_LEAD_STATUS_LEAD_1_ON)
		memcpy(buff->ecg_lead2_samples.buff.raw, holter_square_template[template_index], sizeof(buff->ecg_lead2_samples.buff.raw));
	else
	{
		memcpy(buff->ecg_lead1_samples.buff.raw, holter_square_template[template_index], sizeof(buff->ecg_lead1_samples.buff.raw));
		memcpy(buff->ecg_lead2_samples.buff.raw, holter_square_template[template_index], sizeof(buff->ecg_lead2_samples.buff.raw));
	}

	template_index =  (template_index + 1) % ECG_SQUARE_WAVE_NUM_OF_TEMPLATES_250_HZ;

	return ECG_RESULT_OK;
}

static void ecgTask(void const * argument)
{
	osEvent  						    			ret;
	ECG_PROCESS_BUFFER 				    			*bufPtr;
	ecg_event_s						    			event;
	uint32_t						    			lead_off_count;
	uint8_t											first_hr_value = TRUE;
//	int16_t 						    			n_beats;
//	static ECG_MEAS_PROGRESS_EVENT_STRUCT	    			ecg_meas_prog;
	ECG_ALGO_RESULTS_STRUCT 						algo_results = {0};
	ECG_RESPIRATION_ALGO_RESULTS_STRUCT				respiration_algo_results;
//	ECG_MEAS_PROGRESS_EVENT_STRUCT 					prog_event_data;


	static int16_t                         			last_sample_250_hz_1, last_sample_250_hz_2;
//	static int16_t                         			last_resp_sample_50_hz;
	static int16_t 									respiration_buff_500hz_leftovers[5];
	static ECG_ALGO_ARRYTHMIA_EVENT_STRUCT 			arrhythmias;

   static ECG_MEAS_PROGRESS_EVENT_STRUCT event_rt;

	ecg_ads_alive_timer = osTimerCreate(osTimer(ecgAdsAliveTimer), osTimerOnce, (void *)0);
	ecg_impedance_periodic_measurement_timer	= osTimerCreate(osTimer(ecg_imp_timer), osTimerPeriodic, (void *)0);

	ECG_EVENT_INIT_STRUCT bit;
	ADS_PARAMS_STRUCT par_bit = {.init_type = ADS_PARAMS_INIT_TYPE_BIT};
	bit.bit_resuls = ECG_RESP_BIT_EVENT_SUCCEED;
	ADS_RESULT_E bit_res = adsStart(&par_bit);
	if (bit_res != ADS_RESULT_OK)
		bit.bit_resuls = ECG_RESP_BIT_EVENT_FAILED;
	event.event_type = ECG_EVENT_INIT;
	event.payloadPtr.bit_result = &bit;
	ecg_event(&event);





//	enable_peripherals(BSP_TEST_TYPE_ECG, FALSE);
	UBaseType_t uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
	
//	ecg_timer = xTimerCreate("ECG Timer",pdMS_TO_TICKS(300), pdFALSE, ( void * ) 0, ecgTimerCallback );





	for(;;)
	{
		ret = osMessageGet(ECGQueueHandle, osWaitForever);
		uint32_t time = osKernelSysTick();

		if (adsCheckSpiDummyBuff() != ADS_RESULT_OK)
		{
		  LOG("ADS SPI BUFFER OVERRIDEN!!!\r\n");
		}
		if (ret.status == osEventMessage)
		{

		}
		else
		{
			LOG("ret.status != osEventMessage\r\n");
		}

		prvEcgAdsKeepAliveTimerStart(FALSE);
		bufPtr = (ECG_PROCESS_BUFFER*) ret.value.v;
		if (bufPtr == NULL)
		{
			LOG("ECG task error: bufPtr == NULL\r\n");
			continue;
		}

		num_of_samples += ECG_BUFFER_LENGTH;	// increase number of samples in any case
//		LOG("ECG    Sample Index: %ld\r\n", num_of_samples);

		switch (state)
		{
		case ECG_HOLTER_STATE_CONTACT_DETECTION:
		{
			if (ecg_temp_state == ECG_TEMPERATURE_SET_OUT_OF_RANGE)
			{
				LOG("ECG_HOLTER_STATE_CONTACT_DETECTION, ECG_TEMPERATURE_SET_OUT_OF_RANGE \r\n");
				continue;
			}

			ECG_MEAS_CONTACT_DETECTIION_RESULTS cont;

//			ECG_LEAD_STATUS_ENUM lead_stat;

			prvEcgGetAdsLeads(bufPtr->leads_status, &cont.lead_status);

			if (bufPtr->leads_status != ADS_LEAD_STATUS_ALL_ON)
			{
				cont.leads_state = ECG_LEADS_OFF;
				lead_off_count++;
				lead_on_count = 0;
//				num_of_samples = 0;
				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ECG_STATE_CONTACT_DETECTION, LEADS OFF ADS: %02X -> ECG %02X\r\n", bufPtr->leads_status, cont.lead_status);
			}
			else
			{
				cont.leads_state = ECG_LEADS_ON;
				lead_on_count++;
				lead_off_count = 0;
				//				num_of_samples += ECG_BUFFER_LENGTH;
				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ECG_STATE_CONTACT_DETECTION, LEADS ON\r\n");
			}

			if ((lead_on_count * ecg_buffer_time_msec) >= ECG_CONTACT_DETECTION_TIME)
			{
				cont.leads_state = ECG_LEADS_ON_START;

				state = ECG_HOLTER_STATE_PROGRESS;
//				num_of_samples = 0;
			}

			event.event_type = ECG_HOLTER_EVENT_CONTACT_DETECTION;
			event.payloadPtr.contact_det_leads = &cont;
			event.test_params = &ecg_params;
			ecg_event(&event);


			// override signal with box card square wave
			ECG_MEAS_PROGRESS_INDICATION_BASE_STRUCT prog_ind_common;

			prog_ind_common.ecg_index = num_of_samples;
			prvEcgGetAdsLeads(bufPtr->leads_status, &prog_ind_common.lead_status);
			prog_ind_common.pace_maker.index_start = PM_POSITION_NOT_ASSIGNED;
			prog_ind_common.pace_maker.index_stop = PM_POSITION_NOT_ASSIGNED;

			prvEcgSetSignalSquare(bufPtr, ECG_LEAD_STATUS_OFF);

			event_rt.ecg_lead_1 = &bufPtr->ecg_lead1_samples;
			event_rt.ecg_lead_2 = &bufPtr->ecg_lead2_samples;
			event_rt.common = &prog_ind_common;

			event.event_type = ECG_HOLTER_EVENT_MEASURING;
			event.payloadPtr.real_time_ind = &event_rt;
			event.test_params = &ecg_params;
			ecg_event(&event);


			prvEcgAdsKeepAliveTimerStart(TRUE);
		}
			break;
		case ECG_HOLTER_STATE_PROGRESS:
		{

			ECG_MEAS_PROGRESS_INDICATION_BASE_STRUCT prog_ind_common;

			if ((bufPtr->leads_status & (ADS_LEAD_STATUS_RA | ADS_LEAD_STATUS_RL)) != 0)
			{
				lead_off_count++;

				// override signal with box card square wave
				prvEcgSetSignalSquare(bufPtr, ECG_LEAD_STATUS_OFF);	// TODO: check which lead is off
			}
			else
			{
				lead_off_count = 0;
			}

			if ((lead_off_count * ecg_buffer_time_msec) >= ECG_LEAD_OFF_TIME)
			{
				state = ECG_HOLTER_STATE_CONTACT_DETECTION;
				lead_on_count = 0;
//				num_of_samples = 0;
				break;
			}
			
			if((bufPtr->pmLocation_1 == PM_POSITION_NOT_ASSIGNED) && (bufPtr->pmLocation_2 == PM_POSITION_NOT_ASSIGNED))
			{
//				PMinChunk = FALSE;
//				pace_flag   = 0;
			}
			else
			{
//				if(bufPtr->pmLocation_2 == PM_POSITION_NOT_ASSIGNED)       //pacemaker pulse length is one sample length
//					bufPtr->pmLocation_2 = bufPtr->pmLocation_1;
		    	LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "** PM start %d\tend %d\tcount %d\r\n", (int16_t)bufPtr->pmLocation_1, (int16_t)bufPtr->pmLocation_2, bufPtr->pm_count);
//				PMinChunk = TRUE;
			}	

			prog_ind_common.ecg_index = num_of_samples;
			prvEcgGetAdsLeads(bufPtr->leads_status, &prog_ind_common.lead_status);
			prog_ind_common.pace_maker.index_start = bufPtr->pmLocation_1;
			prog_ind_common.pace_maker.index_stop = bufPtr->pmLocation_2;

			event_rt.ecg_lead_1 = &bufPtr->ecg_lead1_samples;
			event_rt.ecg_lead_2 = &bufPtr->ecg_lead2_samples;
			event_rt.common = &prog_ind_common;

			event.event_type = ECG_HOLTER_EVENT_MEASURING;
			event.payloadPtr.real_time_ind = &event_rt;
			event.test_params = &ecg_params;
			ecg_event(&event);

			prvEcgAdsKeepAliveTimerStart(TRUE);
		}
			break;
		case ECG_HOLTER_STATE_ERROR:
		{

		}
			break;
		case ECG_STATE_TEST_CONTACT_DETECTION:
		{
			if (ecg_temp_state == ECG_TEMPERATURE_SET_OUT_OF_RANGE)
			{
				LOG("ECG_STATE_CONTACT_DETECTION, ECG_TEMPERATURE_SET_OUT_OF_RANGE \r\n");
				continue;
			}
			//				PRINT("\r\n ECG_STATE_CONTACT_DETECTION %d\r\n", osKernelSysTick());
//			bufPtr = (ECG_PROCESS_BUFFER*) ret.value.v;
			ECG_MEAS_PROGRESS_INDICATION_BASE_STRUCT prog_ind_common;

			prvEcgGetAdsLeads(bufPtr->leads_status, &prog_ind_common.lead_status);
			prog_ind_common.ecg_index = 0;

			event.event_type = ECG_EVENT_CONTACT_DETECTION;
			event_rt.common = &prog_ind_common;
			event.payloadPtr.real_time_ind = &event_rt;
			event.test_params = &ecg_params;
			ecg_event(&event);

			if (bufPtr->leads_status != ADS_LEAD_STATUS_ALL_ON)
			{
				lead_off_count++;
				lead_on_count = 0;
//				num_of_samples = 0;
				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ECG_STATE_CONTACT_DETECTION, LEADS OFF ADS: %02X -> ECG %02X\r\n", bufPtr->leads_status,
						prog_ind_common.lead_status);
			}
			else
			{
				lead_on_count++;
				lead_off_count = 0;
//				num_of_samples += ECG_BUFFER_LENGTH;
				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ECG_STATE_CONTACT_DETECTION, LEADS ON\r\n");
			}

			if ((lead_on_count * ecg_buffer_time_msec) >= ECG_CONTACT_DETECTION_TIME)
			{
#ifdef MENNEN_LIB
				ECG_initProc();
				ECG_initArrh();
#endif //MENNEN_LIB
				state = ECG_STATE_TEST_PROGRESS;

				if ( (ecg_params.general_config.impedance_settings & ECG_IMPEDANCE_SETTINGS_AFTER_CONTACT_DETECTION) == ECG_IMPEDANCE_SETTINGS_AFTER_CONTACT_DETECTION)
				{
					if (ecgImpedanceStart() != ECG_RESULT_OK)
					{
						ECG_RESULT res = prvEcgStart();

						if (res == ECG_RESULT_OK)
						{
	//						LOG("\tOK -> restarted ECG\r\n");
						}
						else
						{
							LOG_ERR("\tOK -> restart ECG failed %d\r\n", res);
						}
					}
					break;

				}
//				num_of_samples = 0;
			}
			else
			{
			}
			   prvEcgAdsKeepAliveTimerStart(TRUE);
		}
			break;

		case ECG_STATE_TEST_PROGRESS:
		{
			uint16_t respiration_50hz_length;
			int32_t tmp_samples[2];
			uint8_t i;

			ECG_MEAS_PROGRESS_INDICATION_BASE_STRUCT prog_ind_common;

			prvEcgGetAdsLeads(bufPtr->leads_status, &prog_ind_common.lead_status);

			if (prog_ind_common.lead_status == 3)
			{
				for (i = 0;i < 125;i++)
				{
					tmp_samples[0] = ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(&bufPtr->ecg_lead1_samples, i);
					tmp_samples[1] = ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(&bufPtr->ecg_lead2_samples, i);
					if (((tmp_samples[0] * 0.096) > 20000) || ((tmp_samples[1] * 0.096) > 20000))
						prog_ind_common.lead_status = ECG_LEAD_STATUS_OFF;
						//LOG("!");
				}
			}
			if ((prog_ind_common.lead_status == ECG_LEAD_STATUS_OFF) || (prog_ind_common.lead_status == ECG_LEAD_STATUS_LEAD_1_ON) || (prog_ind_common.lead_status == ECG_LEAD_STATUS_LEAD_2_ON))
//			if (prog_ind_common.lead_status == ECG_LEAD_STATUS_OFF)
			{
				lead_off_count++;
				if ((lead_off_count * ecg_buffer_time_msec) >= ECG_LEAD_OFF_TIME)
				{
	//				if (ecg_params.general_config.on_lead_off_event != ECG_RESP_GENERAL_CONFIG_ON_LEAD_OFF_EVENT_IGNORE)
					state = ECG_STATE_TEST_CONTACT_DETECTION;
					lead_on_count = 0;

					// TODO: send event ?

					prvEcgAdsKeepAliveTimerStart(TRUE);

					break;
				}
			}
			else
			{
				lead_off_count = 0;
			}

			prog_ind_common.algo_results = &algo_results;
			prog_ind_common.arrythmias = &arrhythmias;
			prog_ind_common.ecg_index = num_of_samples;
			prvEcgGetAdsLeads(bufPtr->leads_status, &prog_ind_common.lead_status);
//			prog_ind_common.lead_status = bufPtr->leads_status;
			prog_ind_common.respiration_algo_results = &respiration_algo_results;

			// fix length
			bufPtr->ecg_lead1_samples.buff_byte_length = ECG_BUFFER_LENGTH * ECG_SAMPLE_24_BIT_SIZE;
			bufPtr->ecg_lead2_samples.buff_byte_length = ECG_BUFFER_LENGTH * ECG_SAMPLE_24_BIT_SIZE;
			bufPtr->resp_samples.buff_byte_length = ECG_BUFFER_LENGTH * ECG_SAMPLE_24_BIT_SIZE;

			event_rt.common = &prog_ind_common;
			event_rt.ecg_lead_1 = &bufPtr->ecg_lead1_samples;
			event_rt.ecg_lead_2 = &bufPtr->ecg_lead2_samples;
			event_rt.respiration = &bufPtr->resp_samples;

			event.event_type = ECG_EVENT_REAL_TIME_PROGRESS_RAW;
			event.payloadPtr.real_time_ind = &event_rt;
			event.test_params = &ecg_params;
			ecg_event(&event);
         
			ECG_RESULT ecg_res = prvEcgFilter500Hz(ECG_BUFFER_LENGTH, bufPtr, TRUE, prog_ind_common.lead_status);

			event.event_type = ECG_EVENT_REAL_TIME_PROGRESS_FILTERED;
			event.payloadPtr.real_time_ind = &event_rt;
			ecg_event(&event);

			// pmLocation_2 is copied from pmLocation_1 if was empty (prvEcgFilter500Hz)
			prog_ind_common.pace_maker.index_start = bufPtr->pmLocation_1;
			prog_ind_common.pace_maker.index_stop = bufPtr->pmLocation_2;
			if ( (bufPtr->pmLocation_1 != PM_POSITION_NOT_ASSIGNED) && (ecg_params.ads.sample_rate == ADS_SAMPLE_RATE_500_HZ) && (ecg_params.ecg_output.output_frequency == ECG_PARAMS_OUTPUT_FREQ_250) )
			{
				prog_ind_common.pace_maker.index_start /= 2;
			}
			if ( (bufPtr->pmLocation_2 != PM_POSITION_NOT_ASSIGNED) && (ecg_params.ads.sample_rate == ADS_SAMPLE_RATE_500_HZ) && (ecg_params.ecg_output.output_frequency == ECG_PARAMS_OUTPUT_FREQ_250) )
			{
				prog_ind_common.pace_maker.index_stop /= 2;
			}

			ecg_res = prvEcgRunECGAlgorithm(bufPtr, num_of_samples, &algo_results, &arrhythmias);
			LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "prvEcgRunECGAlgorithm result %d\r\n", ecg_res);
			ecg_res |= prvEcgRunRespirationAlgorithm(&bufPtr->resp_samples, &respiration_algo_results);
			LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "prvEcgRunRespirationAlgorithm result %d\r\n", ecg_res);

			if (ecg_res == ECG_RESULT_OK)
			{
			   if ( (algo_impedance_recover_count * ecg_buffer_time_msec) < ECG_ALGO_IMPEDANCE_RECOVERY_TIME_SEC)
			   {
				  algo_impedance_recover_count++;
				  LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "algo_impedance_recover_count = %d, TrackChanges waiting\r\n", algo_impedance_recover_count);
			   }
			   else
			   {
				  ecgArrythmiasTrackChanges(&arrhythmias);
				  LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "algo_impedance_recover_count = %d, TrackChanges running\r\n", algo_impedance_recover_count);
			   }
			}
			

			if (algo_results.arrhythmia != 0)
			{
				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "arrhythmia found\r\n");
				event.event_type = ECG_EVENT_ARRYTHMIAS_FOUND;
				event.payloadPtr.arrythmias = &arrhythmias;
				ecg_event(&event);
			}

			// TODO: down sample (decimate) signals
			if (meas_250_flag_first)
			{
				// ------------------------------- ECG LEAD 1---------------------------------------------
#ifdef MENNEN_LIB				
				ECG_downSample_500_250(bufPtr->ecg_lead1_samples.buff.sh, ECG_BUFFER_LENGTH - 1, bufPtr->ecg_lead1_samples.buff.sh); //Down sample 124 samples at 500 Hz to 62 samples at 250Hz
#endif //MENNEN_LIB				
				last_sample_250_hz_1 = bufPtr->ecg_lead1_samples.buff.sh[(ECG_BUFFER_LENGTH - 1)]; //Save last sample of the 125 samples

				bufPtr->ecg_lead1_samples.buff_byte_length = ((ECG_BUFFER_LENGTH - 1) * sizeof(int16_t)) / 2; // TODO: ECG_downSample_500_250 result is 0

				// ------------------------------- ECG LEAD 2---------------------------------------------
#ifdef MENNEN_LIB
				ECG_downSample_500_250(bufPtr->ecg_lead2_samples.buff.sh, ECG_BUFFER_LENGTH - 1, bufPtr->ecg_lead2_samples.buff.sh); //Down sample 124 samples at 500 Hz to 62 samples at 250Hz
#endif //MENNEN_LIB
				last_sample_250_hz_2 = bufPtr->ecg_lead2_samples.buff.sh[(ECG_BUFFER_LENGTH - 1)]; //Save last sample of the 125 samples

				bufPtr->ecg_lead2_samples.buff_byte_length = ((ECG_BUFFER_LENGTH - 1) * sizeof(int16_t)) / 2; // TODO: ECG_downSample_500_250 result is 0

				// ------------------------------- Respiration ---------------------------------------------
				memcpy(respiration_buff_500hz_leftovers,
						&bufPtr->resp_samples.buff.sh[(ECG_BUFFER_LENGTH - ECG_RESPIRATION_DOWNSAMPLED_50HZ_LEFTOVERS_LENGTH)],
						sizeof(respiration_buff_500hz_leftovers));		//Save last 5 samples of the 125 samples

				respiration_50hz_length = (ECG_BUFFER_LENGTH - ECG_RESPIRATION_DOWNSAMPLED_50HZ_LEFTOVERS_LENGTH);
#ifdef MENNEN_LIB
				respiration_50hz_length = Resp_downSample_500_50(bufPtr->resp_samples.buff.sh, respiration_50hz_length,
						bufPtr->resp_samples.buff.sh); //Down sample 120 samples at 500 Hz to 12 samples at 50Hz
#endif //MENNEN_LIB
				// set flag for next time
				meas_250_flag_first = FALSE;
			}
			else
			{
				// ------------------------------- ECG LEAD 1---------------------------------------------
				memmove(&bufPtr->ecg_lead1_samples.buff.sh[1], bufPtr->ecg_lead1_samples.buff.sh, (ECG_BUFFER_LENGTH * sizeof(int16_t))); // make room for saved sample
				bufPtr->ecg_lead1_samples.buff.sh[0] = last_sample_250_hz_1;	// place saved sample
#ifdef MENNEN_LIB
				ECG_downSample_500_250(bufPtr->ecg_lead1_samples.buff.sh, ECG_BUFFER_LENGTH + 1, bufPtr->ecg_lead1_samples.buff.sh); //Down sample 126 samples at 500 Hz to 63 samples at 250Hz
#endif //MENNEN_LIB
				bufPtr->ecg_lead1_samples.buff_byte_length = ((ECG_BUFFER_LENGTH + 1) * sizeof(int16_t)) / 2; // 500 to 250 hz, TODO: ECG_downSample_500_250 result is 0

				// ------------------------------- ECG LEAD 2---------------------------------------------
				memmove(&bufPtr->ecg_lead2_samples.buff.sh[1], bufPtr->ecg_lead2_samples.buff.sh, (ECG_BUFFER_LENGTH * sizeof(int16_t))); // make room for saved sample
				bufPtr->ecg_lead2_samples.buff.sh[0] = last_sample_250_hz_2;	// place saved sample
#ifdef MENNEN_LIB
				ECG_downSample_500_250(bufPtr->ecg_lead2_samples.buff.sh, ECG_BUFFER_LENGTH + 1, bufPtr->ecg_lead2_samples.buff.sh); //Down sample 126 samples at 500 Hz to 63 samples at 250Hz
#endif //MENNEN_LIB
				bufPtr->ecg_lead2_samples.buff_byte_length = ((ECG_BUFFER_LENGTH + 1) * sizeof(int16_t)) / 2; // 500 to 250 hz, TODO: ECG_downSample_500_250 result is 0

				// ------------------------------- Respiration ---------------------------------------------
				memmove(bufPtr->resp_samples.buff.raw + sizeof(respiration_buff_500hz_leftovers), bufPtr->resp_samples.buff.raw,
						(ECG_BUFFER_LENGTH * sizeof(int16_t)));	// move new data 5 samples forward
				memcpy(bufPtr->resp_samples.buff.raw, respiration_buff_500hz_leftovers, sizeof(respiration_buff_500hz_leftovers));// copy saved 5 samples
				respiration_50hz_length = (ECG_BUFFER_LENGTH + ECG_RESPIRATION_DOWNSAMPLED_50HZ_LEFTOVERS_LENGTH);
#ifdef MENNEN_LIB
				respiration_50hz_length = Resp_downSample_500_50(bufPtr->resp_samples.buff.sh, respiration_50hz_length,
						bufPtr->resp_samples.buff.sh); //Down sample 126 samples at 500 Hz to 63 samples at 250Hz
#endif //MENNEN_LIB

				// set flag for next time
				meas_250_flag_first = TRUE;
			}

			bufPtr->resp_samples.buff_byte_length = (sizeof(int16_t) * respiration_50hz_length);

			prog_ind_common.ecg_index = num_of_samples/2;	//fix downsampling ecg index

			event.event_type = ECG_EVENT_REAL_TIME_PROGRESS_DECIMATED_WITH_RESULTS;
			event.payloadPtr.real_time_ind = &event_rt;
			ecg_event(&event);

			prvEcgAdsKeepAliveTimerStart(TRUE);

		}
			break;

		case ECG_STATE_TEST_TESTER:
		{
			// IMPLEMENT: upon demand (PBS command), send N samples @ 500 hz / respiration filtered @ 50 hz

		}
			break;

		case ECG_STATE_TEST_ERROR:
		{

		}
			break;
		default:
		{
			LOG_ERR("ECG Unknown State:%d\r\n", state);
			ecgStop();
		}
			break;
		}
		uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
		//DEBUG_PRINT(DEBUG_LEVEL_2, "SERIAL uxHighWaterMark = %d \r\n", uxHighWaterMark);
#ifdef PRINT_STACK_WATER_MARK
		LOG("*ECG uxHighWaterMark = %d \r\n", uxHighWaterMark);
#endif
//   LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ecg time %d\r\n", osKernelSysTick() - time);
	}
}
 

//-----------------------------------------------------------------

static void ecgPmIdleTimerCallback(void const *n)		//end of PM idle time
//void ecgPmIdleTimerCallback(TimerHandle_t xTimer)
{
	if (bspPmCaptureStart() != BSP_RESULT_OK)
		LOG_ERR("*** ecgPmIdleTimerCallback Error\r\n");
}




void pmEvent(bsp_pm_event_s * event)
{
	switch (event->event_type)
	{
		case BSP_PM_EVENT_PM_DETECTED:
		{
			bspPmCaptureStop();
			PMattr.PMdetected = TRUE;
			osTimerStart(ecg_timer_PM, PM_IDLE_TIME);
 		}
		break;
		case BSP_PM_EVENT_PM_NOT_DETECTED:
		{

		}
		break;
		case BSP_PM_EVENT_NOISE_STATE:
		{
			bspPmCaptureStop();		
			osTimerStart(ecg_timer_PM, PM_IDLE_TIME);
		}
		break;
		case BSP_PM_EVENT_PM_IDLE_STATE:
		{

		}
		break;
	}
}

static ECG_RESULT prvEcgPmInit(void)
{
	if (ecg_timer_PM == NULL)
	    ecg_timer_PM = osTimerCreate(osTimer(ecg_pm_timer), osTimerOnce, (void *)0);
	if(bspPmCaptureInit() != BSP_RESULT_OK)  
   		return ECG_RESULT_ERROR_PM;  
	return ECG_RESULT_OK;  
}

static ECG_RESULT prvEcgPmStart()
{
	PMattr.PMdetected = FALSE;
   	if(bspPmCaptureStart() != BSP_RESULT_OK)  
   		return ECG_RESULT_ERROR_PM;			
	
	if(bspPmPowerSwitch(BSP_POWER_UP_MODE) != BSP_RESULT_OK) 
   		return ECG_RESULT_ERROR_PM;			

    return ECG_RESULT_OK;     
}

static ECG_RESULT prvEcgPmStop()
{
   if(xTimerIsTimerActive(ecg_timer_PM))
   {
	  osTimerStop(ecg_timer_PM);
   }
   if (bspPmCaptureStop() != BSP_RESULT_OK)
	  LOG("Pace maker stop error\r\n");
	if(bspPmPowerSwitch(BSP_POWER_UP_MODE) != BSP_RESULT_OK) 
	  LOG("Pace maker stop switch error\r\n");
   
   return ECG_RESULT_OK;
}

//-----------------------------------------------------------------


// Used for external usage (e.g. from CLI), regular use (initialized on startup is splitted: init first, start only after ADS is started too)
ECG_RESULT ecgPmInitStart(uint8_t enable)
{
   ECG_RESULT res;
   if (enable == 1)
   {
		res = prvEcgPmInit();
		if (res != ECG_RESULT_OK)
		   return res;
		
		res = prvEcgPmStart();
		if (res != ECG_RESULT_OK)
		   return res;
   }
   else if (enable == 0)
   {
	  prvEcgPmStop();
   }
   else
	  return ECG_RESULT_ERROR_WRONG_PARAMETER;
   
   return ECG_RESULT_OK;  
}



// ------------------------ IMPEDANCE ---------------------------------------
ECG_RESULT ecgImpedanceStartWithRecovery()
{
	if ( (state == ECG_STATE_TEST_IMPEDANCE) || (state == ECG_STATE_UNINITIALIZED) )
		return ECG_RESULT_ERROR_IMPEDANCE_ALREADY_RUNNING;

	prvEcgAdsKeepAliveTimerStart(FALSE);

	if (ecgImpedanceStart() == ECG_RESULT_OK)
		return ECG_RESULT_OK;

	ECG_RESULT res = prvEcgStart();

	if (res == ECG_RESULT_OK)
	{
			LOG("\tOK -> restarted ECG\r\n");
	}
	else
	{
		LOG_ERR("\tOK -> restart ECG failed %d\r\n", res);
	}
	return ECG_RESULT_ERROR_IMPEDANCE_DRIVER;

}

ECG_RESULT ecgImpedanceStart()
{
	//return ECG_RESULT_OK; //For Respiration Recording
	adsStop();
	if (ecg_params.general_config.pace_maker_enable == ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_ON)
		prvEcgPmStop();
	prv_impedance_save_last_state = state;
	state = ECG_STATE_TEST_IMPEDANCE;
	ecgStop();

	ADS_PARAMS_STRUCT par = {.init_type = ADS_PARAMS_INIT_TYPE_IMPEDANCE};

	ADS_RESULT_E ads_res = adsStart(&par);

	if (ads_res != ADS_RESULT_OK)
	{
		LOG("ecgImpedanceStart ads fail %d\r\n", ads_res);
		return ECG_RESULT_ERROR_ADS_DRIVER;
	}

	IMPEDANCE_RESULT_E imp_res = impedanceStart();
	if (imp_res != IMPEDANCE_RESULT_OK)
	{
		LOG("ecgImpedanceStart imp fail %d\r\n", imp_res);
		return ECG_RESULT_ERROR_IMPEDANCE_DRIVER;
	}
	return ECG_RESULT_OK;
}


void impedanceEventCallback(IMPEDANCE_EVENT_STRUCT * data)
{
	ecg_event_s			event;
	
	switch (data->event_type)
	{
	case IMPEDANCE_EVENT_INIT_FAILED:
		break;
	case IMPEDANCE_EVENT_RESULT_READY:
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "impedanceEventCallback event IMPEDANCE_EVENT_RESULT_READY, LEADS = %d\r\n", data->leads_status);
//		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "Impedance kOhm:RA = %d\tLA = %d\tLL = %d\r\n", data->measurements.ra_kohm, data->measurements.la_kohm, data->measurements.ll_kohm);
		LOG("Impedance kOhm:RA = %d\tLA = %d\tLL = %d\r\n", data->measurements.ra_kohm, data->measurements.la_kohm, data->measurements.ll_kohm);
//    	LOG("Impedance LEAD_STATUS=%d\r\n", leads_status);
//		if (data->leads_status == IMPEDANCE_CHECK_RESULT_OK)

		event.payloadPtr.impedance_results = data;
		event.test_params = &ecg_params;		
		
		if (data->leads_status != IMPEDANCE_CHECK_RESULT_OK)
		{//Impedance too high
			if (++ecg_impedance_retries < ECG_IMPEDANCE_RETRIES)
			{
				impedanceStart();
			}
			else
			{
				event.event_type = ECG_EVENT_IMPEDANCE_TOO_HIGH;
				ecg_event(&event);
				ecg_impedance_retries = 0;
			}
		}
		else
		{
			event.event_type = ECG_EVENT_IMPEDANCE_RESULTS;
			ecg_event(&event);
#ifdef MENNEN_LIB			
			ECG_resetProc();
			ECG_initProc();
			ECG_initArrh();
#endif //MENNEN_LIB
			algo_impedance_recover_count = 0;
			ECG_RESULT res = prvEcgStart();
			state = prv_impedance_save_last_state;

			if (res == ECG_RESULT_OK)
			{
				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "\tOK -> restarted ECG\r\n");
			}
			else
			{
				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "\tOK -> restart ECG failed %d\r\n", res);
			}
			ecg_impedance_retries = 0;
		}

//		if (1)	// TODO: handle properly
//		{
////			state = ECG_STATE_TEST_CONTACT_DETECTION;
//
//			ECG_resetProc();
//			ECG_initProc();
//			ECG_initArrh();
//			algo_impedance_recover_count = 0;
//			ECG_RESULT res = prvEcgStart();
//			state = prv_impedance_save_last_state;
//
//			if (res == ECG_RESULT_OK)
//			{
//				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "\tOK -> restarted ECG\r\n");
//			}
//			else
//			{
//				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "\tOK -> restart ECG failed %d\r\n", res);
//			}
//
//		}
//		else
//		{
//			LOG("impedanceEventCallback leads_status ERROR -> won't restart ECG\r\n");
//		}
	}
	break;
	case IMPEDANCE_EVENT_MEASUREMENT_FAILED:
	{
#ifdef MENNEN_LIB
		ECG_resetProc();
		ECG_initProc();
		ECG_initArrh();
#endif //MENNEN_LIB
		algo_impedance_recover_count = 0;
		ECG_RESULT res = prvEcgStart();

		if (res == ECG_RESULT_OK)
		{
			LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "\tOK -> restarted ECG\r\n");
		}
		else
		{
			LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "\tOK -> restart ECG failed %d\r\n", res);
		}
	}
		break;
	default:
		LOG("impedanceEventCallback event unhandled %d\r\n", data->event_type);
		break;
	}
}




// ---------------------------------- Temperature Handling -------------------------------------


ECG_RESULT ecgTemperatureSet(ECG_TEMPERATURE_SET_E temp)
{
	if (temp >= ECG_TEMPERATURE_SET_MAX)
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	if (ecg_temp_state == temp)
		return ECG_RESULT_OK;

	ecg_temp_state = temp;
	
   lead_on_count = 0;		// reset 3 sec lead on time
	
   if (temp == ECG_TEMPERATURE_SET_OK)
	{
		return ECG_RESULT_OK;	// Will be referred on contact detection state
	}

	// Otherwise -> ECG_TEMPERATURE_SET_OUT_OF_RANGE
	if (state == ECG_HOLTER_STATE_PROGRESS)
	{
		ecgChangeState(ECG_HOLTER_STATE_CONTACT_DETECTION);
	}
	else if (state == ECG_STATE_TEST_PROGRESS)
	{
		ecgChangeState(ECG_STATE_TEST_CONTACT_DETECTION);
	}

	return ECG_RESULT_OK;
}


ECG_RESULT ecgDownsampleRaw500to250(IN ECG_MEAS_PROGRESS_DATA_STRUCT * data_in, IN ECG_MEAS_PROGRESS_DATA_STRUCT * data_in_2,
						OUT ECG_MEAS_PROGRESS_SIGNAL_UNION * data_out, OUT ECG_MEAS_PROGRESS_SIGNAL_UNION * data_out_2)
{
	if ( (data_in == NULL) || (data_out == NULL) )
		return ECG_RESULT_ERROR_WRONG_PARAMETER;

	static uint8_t parity = 0;

	uint16_t last_index = ECG_BUFFER_LENGTH/2 - parity/ECG_SAMPLE_24_BIT_SIZE;

	for (uint16_t i = 0; i <= last_index; i++)
	{
		memcpy(&data_out->raw.buff.raw[i * ECG_SAMPLE_24_BIT_SIZE], &data_in->buff.raw[i * ECG_SAMPLE_24_BIT_SIZE * 2 + parity], ECG_SAMPLE_24_BIT_SIZE);
		if (data_in_2 && data_out_2)
			memcpy(&data_out_2->raw.buff.raw[i * ECG_SAMPLE_24_BIT_SIZE], &data_in_2->buff.raw[i * ECG_SAMPLE_24_BIT_SIZE * 2 + parity], ECG_SAMPLE_24_BIT_SIZE);
	}

	if (parity == 0)
	{
		parity = 3;
	}
	else
	{
		parity = 0;
	}
	data_out->raw.buff_byte_length = (last_index + 1) * ECG_SAMPLE_24_BIT_SIZE - 1;	// compensations: 1 for 0 index start, 2 - for last sample size, -1 fix for offset 0
	if (data_in_2 && data_out_2)
		data_out_2->raw.buff_byte_length = data_out->raw.buff_byte_length;
	return ECG_RESULT_OK;
}



//===========================================================================
#ifdef MENNEN_ALGO

static void mennenAlgoTask(void const * argument)
{

	osEvent ret;
	uint16_t i; //,j,Len;
	uint8_t lead1 = 0, lead2 = 0;

	//ecg_arrh:
	long lsamples[2];
	int pace_flag;
	unsigned char loff_bits = 0;	// Always 0 otherwise will skip buffer
	int16_t ecg_sbuf[2][ECG_BUFFER_LENGTH];
	uint8_t *pBytes;
	int16_t n_beats;
	int num_samples;
	//resp_anal
	long lsample;
	int16_t resp_sbuf[ECG_BUFFER_LENGTH];
	int n_samp;
	int strob_pos;
	int heart_rate, breath_rate;
   int16_t resp_rate;
	ECG_PROCESS_BUFFER * bufPtr;
	ECG_LEAD_STATUS_ENUM leads;
	//   int32_t
	BOOL PMinChunk;

	ecgSetDefaultParams(ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH, ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_OFF);
	osDelay(1000);
	prvEcgStart();
	//   ECG_bindWformBuf(ecg_sbuf, ECG_PROCESS_BUFFER, 1);

	while (1)
	{
		ret = osMessageGet(MennenQueueHandle, osWaitForever);
		{
			bufPtr = (ECG_PROCESS_BUFFER*) ret.value.v;

//			if (bufPtr->leads_status != ADS_LEAD_STATUS_ALL_ON)
//				continue;

			prvEcgGetAdsLeads(bufPtr->leads_status, &leads);
			loff_bits = 0;
//			if (leads == ECG_LEAD_STATUS_OFF)
			switch (leads)
			{
			case ECG_LEAD_STATUS_LEAD_1_ON:
				loff_bits = LOFF_BIT1;
				break;
			case ECG_LEAD_STATUS_LEAD_2_ON:
				loff_bits = LOFF_BIT0;
				break;
			case ECG_LEAD_STATUS_OFF:
				continue;
				break;
			default:
				// TODO: error
				break;
			}
			if ((bufPtr->pmLocation_1 == PM_POSITION_NOT_ASSIGNED) && (bufPtr->pmLocation_2 == PM_POSITION_NOT_ASSIGNED))
			{
				PMinChunk = FALSE;
				pace_flag = 0;
			}
			else
			{
				PMinChunk = TRUE;
				if (bufPtr->pmLocation_1 == PM_POSITION_NOT_ASSIGNED) 	//PM started in the previous chunk
					bufPtr->pmLocation_1 = 0;
			}
#ifdef MENNEN_LIB
			ECG_resetPaceLocat(); //!!!
#endif //MENNEN_LIB
			for (i = 0; i < ECG_BUFFER_LENGTH; i++)
			{
				if(PMinChunk)
				{
					if((i >=  bufPtr->pmLocation_1) && (i <=  bufPtr->pmLocation_2))
						pace_flag   = 1;
					else
						pace_flag   = 0;
				}
				lsamples[0] = ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(&bufPtr->ecg_lead1_samples, i); //RA-LA
				lsamples[1] = ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(&bufPtr->ecg_lead2_samples, i); //RA-LL
				//sample processing - signal conditioning
#ifdef MENNEN_LIB
				ECG_procSample(lsamples, loff_bits, pace_flag); //result is saved in lsamples
#endif //MENNEN_LIB
				//copy samples to short format buffer
				ecg_sbuf[0][i] = (short) lsamples[0];
				ecg_sbuf[1][i] = (short) lsamples[1];
				//fixate pace position for ananlyis
				ECG_latchPaceLocat(pace_flag, i);
#if 1
				//---------- respiration ---------------
				lsample = ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(&bufPtr->resp_samples, i);
				//lead_off = bufPtr->resp_samples[i].lead_on;
#ifdef MENNEN_LIB
				Resp_procSample((long*)&lsample, 0);
#endif //MENNEN_LIB
				resp_sbuf[i] = (short)lsample;
#endif
			}

//			lead1 = (lead1 == ECG_LEAD_STATUS_OFF);
//			lead2 = (lead2 == ECG_LEAD_STATUS_OFF);

			//------------ ecg analysis ----------------

			//run arrthythmia detection
			ECG_execArrh(ecg_sbuf[0], ecg_sbuf[1], lead1, lead2); //, pace_flg);

			//transmit ecg data header
			send_tx_data("ecg", 3);

			//downsample lead1 signal
			num_samples = ECG_downSample_500_250(ecg_sbuf[0], ECG_BUFFER_LENGTH, ecg_sbuf[0]);
			//transmit lead1 signal
			send_tx_data((uint8_t*) ecg_sbuf[0], num_samples * sizeof(short));

			//downsample lead2 signal
			num_samples = ECG_downSample_500_250(ecg_sbuf[1], ECG_BUFFER_LENGTH, ecg_sbuf[1]);
			//transmit lead2 signal
			send_tx_data((uint8_t*) ecg_sbuf[1], num_samples * sizeof(short));

			//warning: ecg_buf is used to transmit all data
			pBytes = (uint8_t*) ecg_sbuf;

			ECG_getPaceLocat_250((ECG_PACE_LOC*) pBytes);
			send_tx_data(pBytes, sizeof(ECG_PACE_LOC));
			//pBytes += sizeof(ECG_PACE_LOC);

			n_beats = ECG_isBeat();
			send_tx_data((uint8_t*) &n_beats, sizeof(short));
			//pBytes += sizeof(short);

			ECG_getHRate((short*) pBytes);
			send_tx_data(pBytes, sizeof(short));
			//pBytes += sizeof(short);
			heart_rate = *(short*) pBytes;

			ECG_getArrh((ECG_ARRH*) pBytes);
			send_tx_data(pBytes, sizeof(ECG_ARRH));
			//pBytes += sizeof(ECG_ARRH);

			n_beats = ECG_getBeat((ECG_BEAT*) pBytes);
			send_tx_data(pBytes, 2 * sizeof(ECG_BEAT)); // n_beats*sizeof(ECG_BEAT));
			//pBytes += sizeof(ECG_BEAT);

			ECG_getStat((ECG_STAT*) pBytes);
			send_tx_data(pBytes, sizeof(ECG_STAT));
			//pBytes += sizeof(ECG_STAT);

			ECG_getConf((ECG_CONF*) pBytes);
			send_tx_data(pBytes, sizeof(ECG_CONF));
			//pBytes += sizeof(ECG_CONF);

			ECG_getVers((char*) pBytes);
			send_tx_data(pBytes, sizeof(ECG_VERS));

			Arrh_getVers((char*) pBytes);
			send_tx_data(pBytes, sizeof(ECG_VERS));
#if 1
			//---------- respiration analysis ---------------
			send_tx_data("rsp", 3); //resp. data header

			pBytes = (uint8_t*)resp_sbuf;

			//transmit waveform 500sps
//			send_tx_data(pBytes, ECG_BUFFER_LENGTH*sizeof(short));
			//pBytes += ECG_BUFFER_LENGTH*sizeof(short);

			//downsample waveform 500 to 320sps, result is saved in resp_buf
//			n_samp = Resp_downSample_500_320(resp_sbuf, ECG_BUFFER_LENGTH);
			//run respiration vsigns calculations on 320sps
//			strob_pos = Resp_execCalc_320sps(resp_sbuf, n_samp);

			n_samp = ECG_BUFFER_LENGTH;
			//run respiration vsigns calculations on 500sps
			strob_pos = Resp_execCalc(resp_sbuf, n_samp);

			//scale waveform signal
			Resp_applyGain(resp_sbuf, n_samp);

			breath_rate = Resp_getBreathRate(&resp_rate);
			//downsample waveform 500 to 50sps, result is saved in resp_buf
#ifdef MENNEN_LIB
			n_samp = Resp_downSample_500_50(resp_sbuf, ECG_BUFFER_LENGTH, resp_sbuf);
#endif //MENNEN_LIB
			if(strob_pos >=0) { strob_pos /=10; }

			send_tx_data((uint8_t*)&n_samp, sizeof(short));

			//transmit waveform
			send_tx_data(pBytes, n_samp*sizeof(short));//(ECG_BUFFER_LENGTH*32/50)*sizeof(short));
			//pBytes += ECG_BUFFER_LENGTH*32/50*sizeof(short);

			send_tx_data((uint8_t*)&strob_pos, sizeof(short));
			//pBytes += sizeof(short);

			pBytes = (uint8_t*)&resp_rate;
			send_tx_data(pBytes, sizeof(short));
			//pBytes += sizeof(short);

			Resp_getApneaTime((short*)pBytes);
			send_tx_data(pBytes, sizeof(short));
			//pBytes += sizeof(short);

			Resp_getStat((RESP_STAT*)pBytes);
			send_tx_data(pBytes, sizeof(RESP_STAT));
			//pBytes += sizeof(RESP_STAT);

			Resp_getConf((RESP_CONF*)pBytes);
			send_tx_data(pBytes, sizeof(RESP_CONF));
			//pBytes += sizeof(RESP_CONF);

			*(short*)pBytes = Resp_ECG_coincidence(breath_rate, heart_rate);
			send_tx_data(pBytes, sizeof(short));

			Resp_getVers((char*)pBytes);
			send_tx_data(pBytes, sizeof(RESP_VERS));
#endif
		}
	}	         //while
}

void serial_protocol_ecg(uint8_t* buff, uint8_t raw_length)
{
	//Vladimir Rx
}

#endif
//===================================================================


