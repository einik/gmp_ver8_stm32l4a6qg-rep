#include "ads.h"
#include "serial_protocol.h"
#include "global_defs.h"
#include "common.h"
#include "BSP.h"

#ifdef G_ECG_EMULATE_DATA 
  #include "ECG\testECG.h"
#endif

#define NUMBER_OF_BUFFERS 2

#define ADS_OPCODE_READ					0x20
#define ADS_OPCODE_WRITE				0x40

#define ADS_REG_ADDR_CONFIG_1			0x01
#define ADS_REG_ADDR_ID                         0
#define ADS_VERIFY_REGISTERS_LENGTH		5	// LOFF_STAT register is READ_ONLY, leads state dependent -> can't be verified (last register)

#define ADS_IN1P_MASK 					0x080
#define ADS_IN1N_MASK 					0x100
#define ADS_IN2P_MASK 					0x200
#define ADS_IN2N_MASK 					0x400
#define ADS_RLD_MASK 					0x800


typedef enum{
	ADS_SRC_ECG,
	ADS_SRC_RESP	
}ADS_SRC_E;

ADS_RESULT_E prvAdsSetForImpedance(void);
ADS_RESULT_E prvAdsSetForImpedanceResp(void);


static ADS_SRC_E 								ads_src = ADS_SRC_ECG;
static uint8_t 									aSpiDummyTxBuff[ADS_DMA_RX_DATA_LEN] = {0}; // Dummy buffer
static uint8_t 									rxBuffer[ADS_DMA_RX_DATA_LEN];
static 	ADS_PARAMS_RESPIRATION_ENABLED_E		resp_chan_en;
static osTimerId 								ads_sampler_timer;

static void adsSamplerCallback(void const *n);


osTimerDef(ads_sampler_timer, adsSamplerCallback);


ADS_RESULT_E adsFirstInit(void)
{
	if (ads_sampler_timer == NULL)
	    ads_sampler_timer = osTimerCreate(osTimer(ads_sampler_timer), osTimerPeriodic, (void *)0);
	
	if (ads_sampler_timer == NULL)
		return ADS_RESULT_ERROR_INIT_FAILED;
	
	return ADS_RESULT_OK;
}

static void adsSamplerCallback(void const *n)
{//ADS start

	static uint8_t 		temp = FALSE;
	BSP_PIN_TYPE_E 		gpio;
	BSP_PIN_STATE_E 	pin_state = BSP_PIN_TOOGLE;
	
	bspReadPin(BSP_PIN_TYPE_ECG_START,	&pin_state);
	
//	if (temp)
		bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET);
//	else
	{
		__HAL_GPIO_EXTI_CLEAR_IT(ECG1_DRDY_PIN);	
		bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_SET);
	}
	
	temp = ~temp;
}

ADS_RESULT_E prvAdsSendStartReadDataContinuouesly(uint8_t ads_bitmap)
{
	BSP_RESULT_E 	bsp_res;
	uint8_t  rdatac = 0x10;
  	
	if ( (ads_bitmap & 0x1) == 0x1)
	{
	   	bsp_res = bsp_spi_transmit(BSP_SPI_ECG1, &rdatac, 1, 5000);
  		if (bsp_res != BSP_RESULT_OK)
			return ADS_RESULT_ERROR_TX_FAILED;
	}
	
	if ( (ads_bitmap & 0x2) == 0x2)
	{
  		bsp_res |= bsp_spi_transmit(BSP_SPI_ECG2, &rdatac, 1, 5000);
  		if (bsp_res != BSP_RESULT_OK)
			return ADS_RESULT_ERROR_TX_FAILED;
	}
	
	return ADS_RESULT_OK;
}

ADS_RESULT_E adsStop(void)
{
//	if (ads_state != ADS_STATE_RUNNING)
//		return ADS_RESULT_ERROR_NOT_RUNNING;
	////bsp_set_irq_mode(BSP_IRQ_ADS, FALSE);

	bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET); // stop
	//ads_read_reg();
	bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_RESET); // Reset leg held LOW = Sleep
	
//	ads_state = ADS_STATE_IDLE;
		
//	enable_peripherals(BSP_TEST_TYPE_ECG, FALSE); //Disable Peripherals
	return ADS_RESULT_OK;
}


static uint32_t int_count;
static uint32_t start_time;

ADS_RESULT_E adsPrintIntCount(void)
{
   LOG("\r\nADS ints %d, time %d", int_count, osKernelSysTick() - start_time);
   
   return ADS_RESULT_OK;
}

ADS_RESULT_E adsStart(ADS_PARAMS_STRUCT* ads_params_struct)
{
	ads_src = ADS_SRC_ECG;
	ADS_RESULT_E res;
	BSP_RESULT_E bsp_res;
   
   resp_chan_en = ads_params_struct->resp_chan_en; // copy to local (for 
   
	switch(ads_params_struct->init_type)
	{
	case ADS_PARAMS_INIT_TYPE_TEST_SIGNAL_ENABLED:
		res =  ads_square_init(ads_params_struct);
		bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_SET);
		break;
	case ADS_PARAMS_INIT_TYPE_REGULAR:
		res =  adsInit(ads_params_struct);

		if (resp_chan_en == ADS_PARAMS_RESPIRATION_ENABLED_ON)
		{
  			osDelay(500);
			res |= ads_init_resp(ads_params_struct);
			
			prvAdsSendStartReadDataContinuouesly(0x3);
		}
		else
		{
		    // turn off 2nd ADS
			bsp_write_pin(BSP_PIN_TYPE_ECG2_RESET, BSP_PIN_RESET);
			
			prvAdsSendStartReadDataContinuouesly(0x1);
			
		}
		//osTimerStart(ads_sampler_timer, 5);
		break;
	case ADS_PARAMS_INIT_TYPE_BIT:
		bsp_res = bsp_write_pin(BSP_PIN_TYPE_ECG2_RESET, BSP_PIN_RESET);
		if (bsp_res != BSP_RESULT_OK)
			LOG_ERR("adsStart -> BIT -> bsp_write_pin fail\r\n");
		res = ads_bit();
		res |= adsStop();
		return res;
		break;
	case ADS_PARAMS_INIT_TYPE_IMPEDANCE:
		bsp_res = bsp_write_pin(BSP_PIN_TYPE_ECG2_RESET, BSP_PIN_RESET);
		if (bsp_res != BSP_RESULT_OK)
			LOG_ERR("adsStart -> Imp -> bsp_write_pin fail\r\n");
		res = prvAdsSetForImpedance();
//		res |= prvAdsSetForImpedanceResp();	// TODO: [Mark] no needed
		break;
	default:
		return ADS_RESULT_ERROR_WRONG_PARAM;
		break;
	}

	if (res != ADS_RESULT_OK)
	{
		bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET);
	}
	else
	{
		__HAL_GPIO_EXTI_CLEAR_IT(ECG1_DRDY_PIN);
   		bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_SET);
	}


	int_count = 0;
	start_time = osKernelSysTick();


	return res;
}

ADS_RESULT_E ads_calc_gain_register(uint8_t gain, uint8_t* reg)
{
	if (reg == NULL)
		return ADS_RESULT_ERROR_WRONG_PARAM;

	*reg &= ~0x70;	// clear gain bits from register
		
	switch (gain)
	{
		case ADS_PARAMS_GAIN_1:
			*reg |= 0x10;
		break;
		case ADS_PARAMS_GAIN_2:
			*reg |= 0x20;
		break;
		case ADS_PARAMS_GAIN_3:
			*reg |= 0x30;
		break;
		case ADS_PARAMS_GAIN_4:
			*reg |= 0x40;
		break;
		case ADS_PARAMS_GAIN_6:
			*reg |= 0;
		break;
		case ADS_PARAMS_GAIN_8:
			*reg |= 0x50;
		break;
		case ADS_PARAMS_GAIN_12:
			*reg |= 0x60;
		break;

		
	}
	return ADS_RESULT_OK;
}

ADS_RESULT_E adsInit(ADS_PARAMS_STRUCT* ads_params_struct)
{
	BSP_RESULT_E 	bsp_res;
	ADS_RESULT_E	ads_res;

	uint8_t ads_reg_cha2set = 0x30;
	uint8_t ads_reg_cha1set = 0x60;
	ads_res = ads_calc_gain_register(ads_params_struct->respiration_gain, &ads_reg_cha1set);
	ads_res |= ads_calc_gain_register(ads_params_struct->gain, &ads_reg_cha2set);
	
	if (ads_res != ADS_RESULT_OK)
		return ADS_RESULT_ERROR_WRONG_PARAM;
  // ADS Configuration: Using WREG Opcode, Starting at Reg Address 0x01 (Config1)

  uint8_t ADSregSet[] = {
      ADS_OPCODE_WRITE | ADS_REG_ADDR_CONFIG_1,//command
      10,         							//data length
	  (ads_params_struct->sample_rate == ADS_SAMPLE_RATE_500_HZ ? 0x02 : 0x01),
	  0xE8,                     			                                                              // CONFIG2
      0xF0,                     			// LOFF 
	  ads_reg_cha2set,	//0x30,                     			// CH1SET: gain 3
	  ads_reg_cha2set,	//0x30,                     			// CH2SET: gain 3
      0xBF,	//0x3F,                     			// RLD_SENS
      0x0D,                     			// LOFF_SENS
      0x00,                     			// LOFF_STAT    
      0x02,                     			// RESP1: 22.5 degrees, MOD, DEMOD enable, Internal clock
      0x07,                     			// RESP2: no calibration, 32 Khz, RLDREF internal = (AVDD-AVSS)/2
      0x00                      			    // GPIO - set output
};
	
  // Flow: CS high (Inactive), Reset Low Pulse, CS Low (Active), 
  //       Send SDATAC opCode (Stop Data Cont., for config)
  //       Send Config (Opcode+addr, Len-1, Registers config)
  //       Start pin High, send RDATAC opCode
  
  //Opcodes
  uint8_t  sdatac = 0x11;
  uint8_t  rdatac = 0x10;
  //uint8_t  len = sizeof(ADSregSet) - 1; //Number of registers - 1

  uint8_t read_verify_cmd[sizeof(ADSregSet) + 2] = {0};
  uint8_t read_verify_reply[sizeof(ADSregSet) + 2] = {0};
  read_verify_cmd[0] = ADS_OPCODE_READ | ADS_REG_ADDR_CONFIG_1;
  read_verify_cmd[1] = sizeof(ADSregSet) - 1;


//  if (queueHandle!= NULL)
//	  ECGQueueHandle = queueHandle; // Save queue for output
  
  bsp_res = bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET); // stop PE13
	
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET); //PE09
  osDelay(1000);
  //CS High;
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
  osDelay(100);
  //Reset Pulse;
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_RESET); //PE09
  osDelay(100);
  
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET); //PE09

  osDelay(50); // Assumption: it takes the ADS time to be ready for configurations after board power up


  //CS Low (Activate)
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
  osDelay(20);

  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)&sdatac, 1, 5000);
  osDelay(20);

  bsp_res |= bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  //config
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)ADSregSet, sizeof(ADSregSet), 5000);

  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;

  osDelay(50);
  bsp_res = bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_RX_FAILED;


  for (int i = 2; i<ADS_VERIFY_REGISTERS_LENGTH; i++)
	  if ( read_verify_reply[i] != ADSregSet[i]) {
		  LOG_ERR("ADS CONFIGURATION FAILED @ index %d, got 0x%x\r\n", i, read_verify_reply[i]);
		  //TODO:Asher why comment
		  return ADS_RESULT_ERROR_VERIFICATION_FAILED;
	  }

  //ads_read_reg();
  
//  bsp_res = bsp_spi_transmit(BSP_SPI_ECG1, &rdatac, 1, 5000);
//  if (bsp_res != BSP_RESULT_OK)
//	  return ADS_RESULT_ERROR_TX_FAILED;

//  bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_SET);
  osDelay(150);	// delay: T_SETTLE(17msec) + T_DR(4msec), where Tsettle = 2052 (for 250 Hz) x T_MOD, T_MOD = 4 T_CLK (512Khz), T_DR = 1/250 Hz

//  ads_state = ADS_STATE_IDLE;
  
  return ADS_RESULT_OK;
}



static ADS_RESULT_E prvAdsCalcRespContRegs(IN ADS_PARAMS_STRUCT* params, OUT uint8_t* reg1, OUT uint8_t * reg2)
{
	if ((params->respiration_phase_shift >= ADS_PARAMS_RESPIRATION_PHASE_SHIFT_MAX) || (params->respiration_clock_freq >= ADS_PARAMS_RESPIRATION_CLOCK_FREQ_MAX))
		return ADS_RESULT_ERROR_WRONG_PARAM;

	*reg1 &= ~0x3C;	// clear phase bits from register
	*reg2 &= ~0x04;

	if (params->respiration_clock_freq == ADS_PARAMS_RESPIRATION_CLOCK_FREQ_32KHZ)
	{
		*reg2 |= 0<<2;	// RESP_FREQ bit on
		switch (params->respiration_phase_shift)
		{
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_0:
			*reg1 |= 0<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_11_25:
			*reg1 |= 1<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_22_5:
			*reg1 |= 2<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_33_75:
			*reg1 |= 3<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_45:
			*reg1 |= 4<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_56_25:
			*reg1 |= 5<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_67_5:
			*reg1 |= 6<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_78_75:
			*reg1 |= 7<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_90:
			*reg1 |= 8<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_101_25:
			*reg1 |= 9<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_112_5:
			*reg1 |= 10<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_123_75:
			*reg1 |= 11<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_135:
			*reg1 |= 12<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_146_25:
			*reg1 |= 13<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_157_5:
			*reg1 |= 14<<2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_168_75:
			*reg1 |= 15<<2;
			break;
		default:
			return ADS_RESULT_ERROR_WRONG_PARAM;
			break;
		}
	}
	else
	{
		*reg2 |= 1<<2;	// RESP_FREQ bit off
		switch (params->respiration_phase_shift)
		{
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_0:
			*reg1 |= 0 << 2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_22_5:
			*reg1 |= 1 << 2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_45:
			*reg1 |= 2 << 2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_67_5:
			*reg1 |= 3 << 2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_90:
			*reg1 |= 4 << 2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_112_5:
			*reg1 |= 5 << 2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_135:
			*reg1 |= 6 << 2;
			break;
		case ADS_PARAMS_RESPIRATION_PHASE_SHIFT_157_5:
			*reg1 |= 7 << 2;
			break;
		default:
			return ADS_RESULT_ERROR_WRONG_PARAM;
		}
	}

	return ADS_RESULT_OK;
}
//TODO:Asher it's a temporary name
ADS_RESULT_E ads_init_resp(ADS_PARAMS_STRUCT* ads_params_struct)
{
	BSP_RESULT_E 	bsp_res;
	ADS_RESULT_E	ads_res;
	uint8_t ads_reg_cha2set = 0x30;
	uint8_t ads_reg_cha1set = 0x30;//0x60;
	
	ads_res = ads_calc_gain_register(ads_params_struct->respiration_gain, &ads_reg_cha1set);
	ads_res |= ads_calc_gain_register(ads_params_struct->gain, &ads_reg_cha2set);
	
	uint8_t ads_reg_resp_1 = 0xC2;
	uint8_t ads_reg_resp_2 = 0x83;//0xF3;

	ads_res |= prvAdsCalcRespContRegs(ads_params_struct, &ads_reg_resp_1, &ads_reg_resp_2);
	if (ads_res != ADS_RESULT_OK)
		return ADS_RESULT_ERROR_WRONG_PARAM;

  // ADS Configuration: Using WREG Opcode, Starting at Reg Address 0x01 (Config1)

	uint8_t ADSregSet[] = {
		ADS_OPCODE_WRITE | ADS_REG_ADDR_CONFIG_1,//command
		10,				        //data lenght
		0x02,          			// CONFIG1: 0x01 for 250Hz, 0x02 for 500Hz
		0xE0,						// CONFIG2
		0xF0,          			// LOFF
		ads_reg_cha1set,	//0x60,           			// CH1SET: normal 10 (gain 1), normal 00 (gain 6), test:05
		0xB0,                     // CH2SET: normal 10 (gain 1), normal 00 (gain 6), test:05
		0x00,                     // RLD_SENS
		0x00,                     // LOFF_SENS
		0x00,	    				// LOFF_STAT
		ads_reg_resp_1,           // RESP1: 22.5 degrees, MOD, DEMOD enable, Internal clock
		ads_reg_resp_2,           // RESP2: no calibration, 32 Khz, RLDREF internal = (AVDD-AVSS)/2
		0x00						// GPIO - set output
	};



	  // Flow: CS high (Inactive), Reset Low Pulse, CS Low (Active),
  //       Send SDATAC opCode (Stop Data Cont., for config)
  //       Send Config (Opcode+addr, Len-1, Registers config)
  //       Start pin High, send RDATAC opCode
  
  //Opcodes
  uint8_t  sdatac = 0x11;
  uint8_t  rdatac = 0x10;
  //uint8_t  len = sizeof(ADSregSet) - 1; //Number of registers - 1

  uint8_t read_verify_cmd[sizeof(ADSregSet) + 2] = {0};
  uint8_t read_verify_reply[sizeof(ADSregSet) + 2] = {0};
  read_verify_cmd[0] = ADS_OPCODE_READ | ADS_REG_ADDR_CONFIG_1;
  read_verify_cmd[1] = sizeof(ADSregSet) - 1;


//  if (queueHandle!= NULL)
//	  ECGQueueHandle = queueHandle; // Save queue for output
  
  bsp_res = bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET); // stop
	
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG2_RESET, BSP_PIN_SET);
  osDelay(1000);
  //CS High;
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
  osDelay(100);
  //Reset Pulse;
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG2_RESET, BSP_PIN_RESET);
  osDelay(100);
  
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG2_RESET, BSP_PIN_SET);

  osDelay(50); // Assumption: it takes the ADS time to be ready for configurations after board power up

  //CS Low (Activate)
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
  osDelay(20);

  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG2, (uint8_t*)&sdatac, 1, 5000);
  osDelay(20);

  bsp_res |= bsp_spi_transmit_receive(BSP_SPI_ECG2, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  //config
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG2, (uint8_t*)ADSregSet, sizeof(ADSregSet), 5000);

  osDelay(50);
  bsp_res |= bsp_spi_transmit_receive(BSP_SPI_ECG2, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);
  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;


  for (int i = 2; i<ADS_VERIFY_REGISTERS_LENGTH; i++)
	  if ( read_verify_reply[i] != ADSregSet[i]) {
		  LOG_ERR("ADS RESP CONFIGURATION FAILED @ index %d, got 0x%x\r\n", i, read_verify_reply[i]);
		  //TODO:Asher why comment
		  return ADS_RESULT_ERROR_VERIFICATION_FAILED;
	  }

  //ads_read_reg();
//  
//  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG2, &rdatac, 1, 5000);
//  if (bsp_res != BSP_RESULT_OK)
//	  return ADS_RESULT_ERROR_TX_FAILED;

  //bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_SET);
  osDelay(150);	// delay: T_SETTLE(17msec) + T_DR(4msec), where Tsettle = 2052 (for 250 Hz) x T_MOD, T_MOD = 4 T_CLK (512Khz), T_DR = 1/250 Hz

//  ads_state = ADS_STATE_IDLE;
  
  return ADS_RESULT_OK;
}

ADS_RESULT_E ads_bit(void)
{
	BSP_RESULT_E 	bsp_res;
	ADS_RESULT_E	ads_res;
//	uint8_t ads_reg_config2;
//	uint8_t ads_reg_cha1set;
	uint8_t ads_reg_cha2set = 0x30;
	
	ads_res = ads_calc_gain_register(ADS_PARAMS_GAIN_3, &ads_reg_cha2set);
	if (ads_res != ADS_RESULT_OK)
		return ADS_RESULT_ERROR_WRONG_PARAM;
  // ADS Configuration: Using WREG Opcode, Starting at Reg Address 0x01 (Config1)

  uint8_t ADSregSet[] = {
		  ADS_OPCODE_WRITE | ADS_REG_ADDR_CONFIG_1,
		  10,			//ID
      0x02,           	//CONFIG1: 0x01 for 250Hz, 0x02 for 500Hz
      0xE0,				//CONFIG2: normal 0xE0, test:0xA3 for 1HZ SQR of 4.033V, test:A2 for DC of 4.033V,
      0xF0,				//LOFF	
      0x60,				//CH1SET: normal 10 (gain 1), normal 00 (gain 6), test:05
	  ads_reg_cha2set,	//0xC1,           //CH2SET: normal 0xC1, test 0x85	
	  0x2C,           	//RLD_SENS
      0x0F,				//LOFF_SENS
      0x0C,				//LOFF_STAT	
      0x2A,				//RESP1
      0x03,				//RESP2
      	0x00,				//GPIO
  }; 

  // Flow: CS high (Inactive), Reset Low Pulse, CS Low (Active), 
  //       Send SDATAC opCode (Stop Data Cont., for config)
  //       Send Config (Opcode+addr, Len-1, Registers config)
  //       Start pin High, send RDATAC opCode
  
  //Opcodes
  uint8_t  sdatac = 0x11;
  uint8_t  rdatac = 0x10;
  //uint8_t  len = sizeof(ADSregSet) - 1; //Number of registers - 1

  uint8_t read_verify_cmd[sizeof(ADSregSet) + 2] = {0}; // 13 + 2
  uint8_t read_verify_reply[sizeof(ADSregSet) + 2] = {0}; // 13 + 2
  read_verify_cmd[0] = ADS_OPCODE_READ | ADS_REG_ADDR_CONFIG_1;
  read_verify_cmd[1] = sizeof(ADSregSet) - 1; // 13 - 1


//  if (queueHandle!= NULL)
//	  ECGQueueHandle = queueHandle; // Save queue for output
  
  bsp_res = bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET); // stop
	
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);
  osDelay(1000);
  //CS High;
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
  osDelay(100);
  //Reset Pulse;
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_RESET);
  osDelay(100);
  
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);

  osDelay(50); // Assumption: it takes the ADS time to be ready for configurations after board power up


  //CS Low (Activate)
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
  osDelay(20);
  //SendADScmd(SDATAC);	//Stop Read Data Continuously mode
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)&sdatac, 1, 5000);
  osDelay(20);

  bsp_res |= bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  //config
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)ADSregSet, sizeof(ADSregSet), 5000);

  osDelay(50);
  bsp_res |= bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);
  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;

  for (int i = 2; i<ADS_VERIFY_REGISTERS_LENGTH; i++)
	  if ( read_verify_reply[i] != ADSregSet[i]) {
		  LOG_ERR("ADS CONFIGURATION FAILED @ index %d\r\n", i);
		  return ADS_RESULT_ERROR_VERIFICATION_FAILED;
	  }

  //ads_read_reg();
  
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, &rdatac, 1, 5000);
  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;


  //bsp_write_pin(BSP_PIN_TYPE_ADS_START, BSP_PIN_SET);
  osDelay(150);	// delay: T_SETTLE(17msec) + T_DR(4msec), where Tsettle = 2052 (for 250 Hz) x T_MOD, T_MOD = 4 T_CLK (512Khz), T_DR = 1/250 Hz

//  ads_state = ADS_STATE_IDLE;
  
  return ADS_RESULT_OK;
}

ADS_RESULT_E ads_read_reg(void)
{

	uint8_t read_verify_cmd[13 + 2] = {0};
	uint8_t read_verify_reply[13 + 2] = {0};
	read_verify_cmd[0] = ADS_OPCODE_READ | ADS_REG_ADDR_CONFIG_1;
	read_verify_cmd[1] = 13 - 1;	
	//uint8_t  sdatac = 0x11;
	
	

	
	osDelay(50);
	bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);
	osDelay(50);
	print_buffer("ADS Registers", read_verify_reply, sizeof(read_verify_reply));
	
//	for (int i = 2; i<ADS_VERIFY_REGISTERS_LENGTH; i++)
//	  if ( read_verify_reply[i] != ADSregSet[i]) {
//		  ERROR_PRINT("ADS CONFIGURATION FAILED @ index %d", i);
//		  //TODO:Asher why comment
//		  //return RESULT_ERROR;
//	  }	
	
	return ADS_RESULT_OK;
}

ADS_RESULT_E ads_square_init(ADS_PARAMS_STRUCT* ads_params_struct)
{
	BSP_RESULT_E bsp_res;
			
  // ADS Configuration: Using WREG Opcode, Starting at Reg Address 0x01 (Config1)

  uint8_t ADSregSet[] = {
		  ADS_OPCODE_WRITE | ADS_REG_ADDR_CONFIG_1,
		  10,			//ID
      0x02,           //CONFIG1: 0x01 for 250Hz, 0x02 for 500Hz
      0xA3,//ads_reg_config2,//CONFIG2: normal 0xE0, test:0xA3 for 1HZ SQR of 4.033V, test:A2 for DC of 4.033V,
      0xF0,           //LOFF	
      0x80,//ads_reg_cha1set,//CH1SET: normal 10 (gain 1), normal 00 (gain 6), test:05
      0x65,//0xC1,           //CH2SET: normal 0xC1, test 0x85	
      //0x33,           //RLD_SENS	
	  0x00,           //RLD_SENS - RLD disabled//0x2C
      0x0C,//0x0F,//0x03,           //LOFF_SENS
	  //0x00,           //LOFF_SENS - Lead off detection disabled
      0x13,//0x00,	    	//LOFF_STAT	
      0x2A,//0x02,           //RESP1
      0x03,//0x07,           //RESP2
      0x00,//0x00			//GPIO
  }; 

  //Opcodes
  uint8_t  sdatac = 0x11;
  uint8_t  rdatac = 0x10;
  //uint8_t  len = sizeof(ADSregSet) - 1; //Number of registers - 1

  uint8_t read_verify_cmd[sizeof(ADSregSet) + 2] = {0};
  uint8_t read_verify_reply[sizeof(ADSregSet) + 2] = {0};
  read_verify_cmd[0] = ADS_OPCODE_READ | ADS_REG_ADDR_CONFIG_1;
  read_verify_cmd[1] = sizeof(ADSregSet) - 1;


//  if (queueHandle!= NULL)
//	  ECGQueueHandle = queueHandle; // Save queue for output
  
  bsp_res = bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);
  osDelay(1000);
  //CS High;
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
  osDelay(100);
  //Reset Pulse;
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_RESET);
  osDelay(100);
  
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);

  osDelay(50); // Assumption: it takes the ADS time to be ready for configurations after board power up


  //CS Low (Activate)
  bsp_res |= bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
  osDelay(20);
  //SendADScmd(SDATAC);	//Stop Read Data Continuously mode
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)&sdatac, 1, 5000);
  osDelay(20);

  bsp_res |= bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  //config
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)ADSregSet, sizeof(ADSregSet), 5000);

  osDelay(50);
  bsp_res |= bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);
  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;


  for (int i = 2; i<ADS_VERIFY_REGISTERS_LENGTH; i++)
	  if ( read_verify_reply[i] != ADSregSet[i]) {
		  LOG_ERR("ADS CONFIGURATION FAILED @ index %d\r\n", i);
		  //TODO:Asher why comment
		  return ADS_RESULT_ERROR_VERIFICATION_FAILED;
	  }

  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, &rdatac, 1, 5000);
  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;


//  bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_SET);
  osDelay(150);	// delay: T_SETTLE(17msec) + T_DR(4msec), where Tsettle = 2052 (for 250 Hz) x T_MOD, T_MOD = 4 T_CLK (512Khz), T_DR = 1/250 Hz

//  ads_state = ADS_STATE_IDLE;
  
  return ADS_RESULT_OK;
}

ADS_RESULT_E ads_input_shorted_init(ADS_PARAMS_STRUCT* ads_params_struct)
{
	BSP_RESULT_E bsp_res;
				
  // ADS Configuration: Using WREG Opcode, Starting at Reg Address 0x01 (Config1)

  uint8_t ADSregSet[] = {
		  ADS_OPCODE_WRITE | ADS_REG_ADDR_CONFIG_1,
		  10,			//ID
      0x02,           //CONFIG1: 0x01 for 250Hz, 0x02 for 500Hz
      0xA0,//ads_reg_config2,//CONFIG2: normal 0xE0, test:0xA3 for 1HZ SQR of 4.033V, test:A2 for DC of 4.033V,
      0xF0,           //LOFF	
      0x80,//ads_reg_cha1set,//CH1SET: normal 10 (gain 1), normal 00 (gain 6), test:05
      0x61,//0xC1,           //CH2SET: normal 0xC1, test 0x85	
      //0x33,           //RLD_SENS	
	  0x00,           //RLD_SENS - RLD disabled//0x2C
      0x0C,//0x0F,//0x03,           //LOFF_SENS
	  //0x00,           //LOFF_SENS - Lead off detection disabled
      0x13,//0x00,	    	//LOFF_STAT	
      0x2A,//0x02,           //RESP1
      0x03,//0x07,           //RESP2
      0x00,//0x00			//GPIO
  }; 

  //Opcodes
  uint8_t  sdatac = 0x11;
  uint8_t  rdatac = 0x10;
  //uint8_t  len = sizeof(ADSregSet) - 1; //Number of registers - 1

  uint8_t read_verify_cmd[sizeof(ADSregSet) + 2] = {0};
  uint8_t read_verify_reply[sizeof(ADSregSet) + 2] = {0};
  read_verify_cmd[0] = ADS_OPCODE_READ | ADS_REG_ADDR_CONFIG_1;
  read_verify_cmd[1] = sizeof(ADSregSet) - 1;


//  if (queueHandle!= NULL)
//	  ECGQueueHandle = queueHandle; // Save queue for output
  
  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);
  osDelay(1000);
  //CS High;
  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
  osDelay(100);
  //Reset Pulse;
  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_RESET);
  osDelay(100);
  
  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);

  osDelay(50); // Assumption: it takes the ADS time to be ready for configurations after board power up


  //CS Low (Activate)
  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
  osDelay(20);
  //SendADScmd(SDATAC);	//Stop Read Data Continuously mode
  bsp_res = bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)&sdatac, 1, 5000);
  osDelay(20);

  bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  //config
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)ADSregSet, sizeof(ADSregSet), 5000);

  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;

  osDelay(50);
  bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  for (int i = 2; i<ADS_VERIFY_REGISTERS_LENGTH; i++)
	  if ( read_verify_reply[i] != ADSregSet[i]) {
		  LOG_ERR("ADS CONFIGURATION FAILED @ index %d\r\n", i);
		  //TODO:Asher why comment
		  //return RESULT_ERROR;
	  }

  bsp_spi_transmit(BSP_SPI_ECG1, &rdatac, 1, 5000);

  bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_SET);
  osDelay(150);	// delay: T_SETTLE(17msec) + T_DR(4msec), where Tsettle = 2052 (for 250 Hz) x T_MOD, T_MOD = 4 T_CLK (512Khz), T_DR = 1/250 Hz

//  ads_state = ADS_STATE_IDLE;
  
  return ADS_RESULT_OK;
}

/*
  uint8_t ADSregSet[] = {
		  ADS_OPCODE_WRITE | ADS_REG_ADDR_CONFIG_1,
		  10,			//ID
      0x02,           //CONFIG1: 0x01 for 250Hz, 0x02 for 500Hz
      ads_reg_config2,//CONFIG2: normal 0xE0, test:0xA3 for 1HZ SQR of 4.033V, test:A2 for DC of 4.033V,
      0xF0,           //LOFF	
      ads_reg_cha1set,//CH1SET: normal 10 (gain 1), normal 00 (gain 6), test:05
      ads_reg_cha1set,//0xC1,           //CH2SET: normal 0xC1, test 0x85	
      //0x33,           //RLD_SENS	
	  0x00,           //RLD_SENS - RLD disabled
      0x03,           //LOFF_SENS
	  //0x00,           //LOFF_SENS - Lead off detection disabled
      0x00,	    	//LOFF_STAT	
      0x02,           //RESP1
      0x07,           //RESP2
      0x00			//GPIO
  }; 
*/

//static void ECG_Toggle_Buffers()
//{
//  u8buffIndx^=1;
//  iSpiECGindex = 0;
//}

ADS_RESULT_E adsCheckSpiDummyBuff()
{
	if ( (aSpiDummyTxBuff[0] == 0) && (aSpiDummyTxBuff[ADS_DMA_RX_DATA_LEN-1]==0) )
		return ADS_RESULT_OK;
	return ADS_RESULT_ERROR_SPI_DUMMY_BUFF;
}

//Override BSP functions
//static uint8_t bufNum = 0;
void bsp_ads1_irq_callback()
{
	BSP_RESULT_E bsp_res;


	int_count++;
//	start_time = osKernelSysTick();
	ads_src = ADS_SRC_ECG;		

	bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
//	ERROR_PRINT("bsp_ads1_irq_callback\r\n");
	
//    memset(aSpiDummyTxBuff, 0, sizeof(aSpiDummyTxBuff));
        
	bsp_res = bsp_spi_transmit_receive_dma(BSP_SPI_ECG1, aSpiDummyTxBuff, ADS_DMA_RX_DATA_LEN, rxBuffer);

	if (bsp_res == BSP_RESULT_OK)
		return;

	LOG_ERR("ADS 1 DRDY Driver Error DMA failed: %d\r\n",bsp_res);
	if ( (bsp_res == BSP_RESULT_ERROR_SPI_TX_FAILED) || (bsp_res == BSP_RESULT_ERROR_SPI_RX_FAILED) )
	{
		bspSpiDmaStop(BSP_SPI_ECG1);

		// TODO: retry
		bsp_res = bsp_spi_transmit_receive_dma(BSP_SPI_ECG2, aSpiDummyTxBuff, ADS_DMA_RX_DATA_LEN, rxBuffer);
		if (bsp_res != BSP_RESULT_OK)
		{
			LOG_ERR("ADS 1 retry DRDY Driver Error DMA failed: %d\r\n",bsp_res);
		}
	}


}

void bsp_ads2_irq_callback()
{

	BSP_RESULT_E bsp_res;
    
	ads_src = ADS_SRC_RESP;

	bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_SET);
//	ERROR_PRINT("bsp_ads2_irq_callback\r\n");

//    memset(aSpiDummyTxBuff, 0, sizeof(aSpiDummyTxBuff));
        
	bsp_res = bsp_spi_transmit_receive_dma(BSP_SPI_ECG2, aSpiDummyTxBuff, ADS_DMA_RX_DATA_LEN, rxBuffer);

	if (bsp_res != BSP_RESULT_OK)
	{
		LOG_ERR("ADS 2 DRDY Driver Error DMA failed: %d\r\n",bsp_res);
		if ( (bsp_res == BSP_RESULT_ERROR_SPI_TX_FAILED) || (bsp_res == BSP_RESULT_ERROR_SPI_RX_FAILED) )
		{
			bspSpiDmaStop(BSP_SPI_ECG1);

			// TODO: retry
			bsp_res = bsp_spi_transmit_receive_dma(BSP_SPI_ECG2, aSpiDummyTxBuff, ADS_DMA_RX_DATA_LEN, rxBuffer);
			if (bsp_res != BSP_RESULT_OK)
			{
				LOG_ERR("ADS 2 retry DRDY Driver Error DMA failed: %d\r\n",bsp_res);
			}
		}
	}

}



ADS_RESULT_E prvAdsCalcLeadDetection(int32_t lead_status, uint8_t* lead)
{
	if (lead == NULL)
		return ADS_RESULT_ERROR_WRONG_PARAM;

	if ( lead_status & ADS_IN1P_MASK )
		*lead = ADS_LEAD_STATUS_LA;    // LA
	
	if ( lead_status & ADS_IN1N_MASK )
		*lead |= ADS_LEAD_STATUS_RA;   // RA
	
	if ( lead_status & ADS_IN2P_MASK )
		*lead |= ADS_LEAD_STATUS_LL;   // LL
	// ,
	if ( lead_status & ADS_IN2N_MASK )
		*lead |= ADS_LEAD_STATUS_RA;   // TODO

	if ( lead_status & ADS_RLD_MASK )
		*lead |= ADS_LEAD_STATUS_RL;   // LL

	
	return ADS_RESULT_OK;
}



void bspAdsSpiCmpltCallback(uint8_t * rxData, uint16_t size)
{
	static int32_t ecg_lead1_sample;
	static int32_t ecg_lead2_sample;
	static int32_t resp_sample;
	static uint32_t lead_status = 0;
	static uint8_t lead = 0;

	if (size != ADS_DMA_RX_DATA_LEN)
		LOG_ERR("bspAdsSpiCmpltCallback size mismatch:%d\r\n", (size-ADS_DMA_RX_DATA_LEN) );
	if((rxData[0] & 0xF0) != ADS_DATA_START_TOKEN)		//ADS format error
	{
		LOG_ERR("ADS format error %x\r\n", rxData[0]);
		return;
	}
	
	if (ads_src == ADS_SRC_ECG)
	{
		bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
		// reset for next time
		lead = 0;
		lead_status = 0;
	
		//process data
//		lead_status = (rxData[0] << 16) |  (rxData[1] << 8) | rxData[2];
		lead_status = ( (rxData[0] & 0x0F) << 8) |  (rxData[1] & 0x80);
		ecg_lead1_sample = (rxData[3] << 16) |  (rxData[4] << 8) | rxData[5];
		ecg_lead2_sample = (rxData[6] << 16) |  (rxData[7] << 8) | rxData[8];
		
		if ((rxData[3] & 0x80) != 0)
			ecg_lead1_sample |= (0xFF << 24);
		
		if ((rxData[6] & 0x80) != 0)
			ecg_lead2_sample |= (0xFF << 24);

		ecg_lead1_sample = (ecg_lead1_sample * 105) / 100;
		
		ecg_lead2_sample = (ecg_lead2_sample * 105) / 100;
		
		prvAdsCalcLeadDetection(lead_status, &lead);

		if (resp_chan_en == ADS_PARAMS_RESPIRATION_ENABLED_OFF)  // "Holter only" mode
		{
         adsSampleReceived(ecg_lead1_sample, ecg_lead2_sample, resp_sample, lead);
      }
      else
		{
         bsp_ads2_irq_callback();
      }
	}
	else if (ads_src == ADS_SRC_RESP)
	{
		bsp_write_pin(BSP_PIN_TYPE_ECG2_CS, BSP_PIN_SET);
		//process data
		resp_sample = (rxData[3] << 16) |  (rxData[4] << 8) | rxData[5];
		
		if ((rxData[3] & 0x80) != 0)
			resp_sample |= (0xFF << 24);

		
		adsSampleReceived(ecg_lead1_sample, ecg_lead2_sample, resp_sample, lead);
		
	}
	else
	{
		LOG_ERR("ADS wrong source\r\n");
	}
}


// ---------------------------------- IMPEDANCE -------------------------------------------


ADS_RESULT_E prvAdsSetForImpedanceResp(void)
{
	BSP_RESULT_E bsp_res;

  uint8_t ADSregSet[] = {
      ADS_OPCODE_WRITE | ADS_REG_ADDR_CONFIG_1,//command
      10,         							//data length
      0x07,                     			// CONFIG1: 0x01 for 250Hz, 0x02 for 500Hz
      0xE8,                     			//CONFIG2:                                                             // CONFIG2
      0xF0,                     			// LOFF
	  0x30,                     			// CH1SET: gain 3
	  0x30,                     			// CH2SET: gain 3
      0x30,                     			// RLD_SENS
      0x00,                     			// LOFF_SENS
      0x00,                     			// LOFF_STAT
      0x00,                     			// RESP1: 22.5 degrees, MOD, DEMOD enable, Internal clock
      0x00,                     			// RESP2: no calibration, 32 Khz, RLDREF internal = (AVDD-AVSS)/2
      0x00                      			    // GPIO - set output
	};

  // Flow: CS high (Inactive), Reset Low Pulse, CS Low (Active),
  //       Send SDATAC opCode (Stop Data Cont., for config)
  //       Send Config (Opcode+addr, Len-1, Registers config)
  //       Start pin High, send RDATAC opCode

  //Opcodes
  uint8_t  sdatac = 0x11;
  uint8_t  rdatac = 0x10;

  uint8_t read_verify_cmd[sizeof(ADSregSet) + 2] = {0};
  uint8_t read_verify_reply[sizeof(ADSregSet) + 2] = {0};
  read_verify_cmd[0] = ADS_OPCODE_READ | ADS_REG_ADDR_CONFIG_1;
  read_verify_cmd[1] = sizeof(ADSregSet) - 1;

  bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET); // stop

  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);
  osDelay(1000);
  //CS High;
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
  osDelay(100);
  //Reset Pulse;
  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_RESET);
  osDelay(100);

  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);

  osDelay(50); // Assumption: it takes the ADS time to be ready for configurations after board power up


  //CS Low (Activate)
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
  osDelay(20);

  bsp_res = bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)&sdatac, 1, 5000);
  osDelay(20);

  bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  //config
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)ADSregSet, sizeof(ADSregSet), 5000);

  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;

  osDelay(50);
  bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  for (int i = 2; i<ADS_VERIFY_REGISTERS_LENGTH; i++)
	  if ( read_verify_reply[i] != ADSregSet[i]) {
		  LOG_ERR("ADS IMP CONFIGURATION FAILED @ index %d, got 0x%x instead of 0x%x\r\n", i, read_verify_reply[i], ADSregSet[i]);
		  //TODO:Asher why comment
		  return ADS_RESULT_ERROR;
	  }

  //ads_read_reg();
  bsp_spi_transmit(BSP_SPI_ECG1, &rdatac, 1, 5000);
  osDelay(150);	// delay: T_SETTLE(17msec) + T_DR(4msec), where Tsettle = 2052 (for 250 Hz) x T_MOD, T_MOD = 4 T_CLK (512Khz), T_DR = 1/250 Hz
  return ADS_RESULT_OK;
}



ADS_RESULT_E prvAdsSetForImpedance(void)
{
	BSP_RESULT_E bsp_res;

  uint8_t ADSregSet[] = {
      ADS_OPCODE_WRITE | ADS_REG_ADDR_CONFIG_1,//command
      10,         							//data length
      0x07, //02                     			// CONFIG1: 0x01 for 250Hz, 0x02 for 500Hz
      0x20, //E8                     			//CONFIG2:                                                              // CONFIG2
      0x00,//F0,                     			// LOFF
	  0x80, //30,                     			// CH1SET: gain 3
	  0x80,//30,                     			// CH2SET: gain 3
      0x30,                     			// RLD_SENS
      0x0D,                     			// LOFF_SENS
      0x00,                     			// LOFF_STAT
      0x02,                     			// RESP1: 22.5 degrees, MOD, DEMOD enable, Internal clock
      0x07,                     			// RESP2: no calibration, 32 Khz, RLDREF internal = (AVDD-AVSS)/2
      0x00                      			    // GPIO - set output
	};

  // Flow: CS high (Inactive), Reset Low Pulse, CS Low (Active),
  //       Send SDATAC opCode (Stop Data Cont., for config)
  //       Send Config (Opcode+addr, Len-1, Registers config)
  //       Start pin High, send RDATAC opCode

  //Opcodes
  uint8_t  sdatac = 0x11;
  uint8_t  rdatac = 0x10;

  uint8_t read_verify_cmd[sizeof(ADSregSet) + 2] = {0};
  uint8_t read_verify_reply[sizeof(ADSregSet) + 2] = {0};
  read_verify_cmd[0] = ADS_OPCODE_READ | ADS_REG_ADDR_CONFIG_1;
  read_verify_cmd[1] = sizeof(ADSregSet) - 1;
  
  bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET); // stop

  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);
  osDelay(1000);
  //CS High;
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
  osDelay(100);
  //Reset Pulse;
  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_RESET);
  osDelay(100);
  
  bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET);

  osDelay(50); // Assumption: it takes the ADS time to be ready for configurations after board power up


  //CS Low (Activate)
//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_RESET);
  osDelay(20);

  bsp_res = bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)&sdatac, 1, 5000);
  osDelay(20);

  bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);

  //config
  bsp_res |= bsp_spi_transmit(BSP_SPI_ECG1, (uint8_t*)ADSregSet, sizeof(ADSregSet), 5000);

  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;

  osDelay(50);
  bsp_res = bsp_spi_transmit_receive(BSP_SPI_ECG1, read_verify_cmd, sizeof(read_verify_cmd), 5000, read_verify_reply);
  if (bsp_res != BSP_RESULT_OK)
	  return ADS_RESULT_ERROR_TX_FAILED;


  for (int i = 2; i<ADS_VERIFY_REGISTERS_LENGTH; i++)
	  if ( read_verify_reply[i] != ADSregSet[i]) {
		  LOG_ERR("ADS IMP CONFIGURATION FAILED @ index %d, got 0x%x instead of 0x%x\r\n", i, read_verify_reply[i], ADSregSet[i]);
		  //TODO:Asher why comment
		  return ADS_RESULT_ERROR;
	  }

  //ads_read_reg();
  bsp_spi_transmit(BSP_SPI_ECG1, &rdatac, 1, 5000);
  osDelay(150);	// delay: T_SETTLE(17msec) + T_DR(4msec), where Tsettle = 2052 (for 250 Hz) x T_MOD, T_MOD = 4 T_CLK (512Khz), T_DR = 1/250 Hz
  return ADS_RESULT_OK;
}
