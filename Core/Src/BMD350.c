#include "cmsis_os.h"
#include "common.h"

#include "serial_protocol.h"
#include "BSP.h"

#include "BMD350.h"


/****************************************************************/
/*						Local Definitions	 					*/
#define BMD_AT_COMMAND_MAX_LENGTH							30
#define BMD_AT_COMMAND_CR									0x0D
#define BMD_AT_COMMAND_SPACE								0x20


/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/

/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
static BMD_MODE_E 						bmd_mode = BMD_MODE_NORMAL;
//static osMutexId 						transmitUart2Mutex;
static osSemaphoreId					transmitUart2Mutex;
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/

/****************************************************************/

/****************************************************************/
/*						Weak Functions							*/
__weak void bmdRxCptCallback(void){};
__weak void bmdEvent(BMD_EVENT_E event){};
/****************************************************************/


/***********************************************************************
 * Function name: bmdInit
 * Description: Init BMD350 BLE module
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdInit(void)
{  

	//Init Tx Mutex
//	osMutexDef(transmitUart2Mutex);
	osSemaphoreDef(transmitUart2Mutex);
//	transmitUart2Mutex = osMutexCreate (osMutex(transmitUart2Mutex));
	transmitUart2Mutex = osSemaphoreCreate (osSemaphore(transmitUart2Mutex), 1);
	if (!transmitUart2Mutex)
		return BMD_RESULT_ERROR_INIT_FAILED;	
	
	
	return BMD_RESULT_OK;
}

/***********************************************************************
 * Function name: bmdAtCommand
 * Description: Set mode 
 * Parameters : data1 - pointer to first data
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR_COMMAND_TOO_LONG - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdAtCommandSend(	IN uint8_t* data1, 
						  		IN uint8_t* data2)
{
	
	BSP_RESULT_E	ret;
	uint16_t 		length1 = 0, length2 = 0;;
	uint8_t			i;
	uint8_t			at_command[BMD_AT_COMMAND_MAX_LENGTH] = {0};
	
	length1 = strnlen((char*)data1, BMD_AT_COMMAND_MAX_LENGTH);
	length2 = strnlen((char*)data2, BMD_AT_COMMAND_MAX_LENGTH);

	if (data2 == NULL)
		length2 = 0;
	
	if ((length1 + length2) >= BMD_AT_COMMAND_MAX_LENGTH) 
	{
		return BMD_RESULT_ERROR_COMMAND_TOO_LONG;
	}
	LOG("%s %s\r\n", data1, data2);
	
	memcpy(at_command, data1, length1);
	at_command[length1] = BMD_AT_COMMAND_SPACE; //Space charactrer
	if (length2 != 0)
		memcpy(&at_command[length1 + 1], data2, length2);
	
	at_command[length1 + 1 + length2] = BMD_AT_COMMAND_CR; //Carriage return
	
	for(i = 0;i < (length1 + 1 + length2 + 1);i++)
	{
		osDelay(50);
//		osMutexWait(transmitUart2Mutex, 2000);
		osSemaphoreWait(transmitUart2Mutex, 2000);
		ret = bsp_uart_tx_dma_ble(&at_command[i], 1);
		if (ret != BSP_RESULT_OK)
		{
			LOG_ERR("AT command failed\r\n");
//			osMutexRelease(transmitUart2Mutex);
			osSemaphoreRelease(transmitUart2Mutex);
			return BMD_RESULT_ERROR_AT_COMMAND_FAILED;
		}
	}

//	PRINT("BLE TX: ");
//	DEBUG_PRINT(DEBUG_LEVEL_10, (char*)data, length);
//	PRINT("\r\n");	
	
	return BMD_RESULT_OK;
}

/***********************************************************************
 * Function name: bmdReset
 * Description: Reset BMD350 module
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdReset(void)
{
	bsp_write_pin(BSP_PIN_TYPE_BLE_RESET, BSP_PIN_RESET);
	osDelay(100);
	bsp_write_pin(BSP_PIN_TYPE_BLE_RESET, BSP_PIN_SET);	
	osDelay(50);
	return BMD_RESULT_OK;
}

/***********************************************************************
 * Function name: bmdAtCommandMode
 * Description: Set BMD350 module in or out of AT COMMANDS MODE
 * Parameters : BMD_OPERATION_MODE mode - AT command mode
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdAtCommandMode(IN BMD_MODE_E mode)
{
	bmd_mode = mode;
	
	if (bmd_mode == BMD_MODE_AT_COMMAND)
	{
		bsp_write_pin(BSP_PIN_TYPE_BLE_AT_COMM_EN, BSP_PIN_RESET);
		bspUart2Uninit();
		osDelay(50);
		bspUart2Init(G_UART_BAUD_RATE_AT_COMMAND_BLE, FALSE);
	}
	else if (mode == BMD_MODE_NORMAL)
	{
		bsp_write_pin(BSP_PIN_TYPE_BLE_AT_COMM_EN, BSP_PIN_SET);
		bspUart2Uninit();
		osDelay(50);
		bspUart2Init(G_UART_BAUD_RATE_BLE, TRUE);	
	}
	
	osDelay(50);
	
	bmdReset();
	
//	bsp_uart_receive_dma(BSP_UART_BLE, bleRxBuffer, 1);//TODO
		
	return BMD_RESULT_OK;
}

/***********************************************************************
 * Function name: bmdGetDataFromBle
 * Description: Configure UART DMA to receive data asyncorinos
 * Parameters : data - pointer to data, length - length of data to receive
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdGetDataFromBle(INOUT uint8_t* data, 
							   IN uint16_t length)
{
	
	BSP_RESULT_E res = bsp_uart_receive_dma(BSP_UART_BLE, data, length);
	
	return (res == BSP_RESULT_OK ? BMD_RESULT_OK : BMD_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bmdStopReadingDataFromBle
 * Description: Stop UART DMA 
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdStopReadingDataFromBle(void)
{
	BSP_RESULT_E res = bsp_uart_stop_dma(BSP_UART_BLE);
	
	return (res == BSP_RESULT_OK ? BMD_RESULT_OK : BMD_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bmdGetDataLength
 * Description: Get length of data buffer size left 
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdGetDataLength(OUT uint16_t* length)
{
	BSP_RESULT_E res = bspUartDmaDataLeft(BSP_UART_BLE, length);
	
	return (res == BSP_RESULT_OK ? BMD_RESULT_OK : BMD_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bmdModeGet
 * Description: Get BMD350 mode
 * Parameters : mode - pointer to BMD mode
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdModeGet(OUT BMD_MODE_E* mode)
{
	*mode = bmd_mode;
	return BMD_RESULT_OK;
}

/***********************************************************************
 * Function name: bmdSendDataToBle
 * Description: Send data to BLE
 * Parameters : ptr - pointer for data to send, length - length of data to send
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdSendDataToBle(IN uint8_t *ptr, 
							  IN uint16_t length)
{
	if ((ptr == NULL) || (length == 0))
		return BMD_RESULT_ERROR_WRONG_PARAM;

//	osMutexWait(transmitUart2Mutex, 100);
	osSemaphoreWait(transmitUart2Mutex, 100);
	
	BSP_RESULT_E ret = bsp_uart_tx_it(BSP_UART_BLE, ptr, length);
		
	if (ret != BSP_RESULT_OK)
//		osMutexRelease(transmitUart2Mutex);
		osSemaphoreRelease(transmitUart2Mutex);
	
	return (ret == BSP_RESULT_OK ? BMD_RESULT_OK : BMD_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bmdBleCommSet
 * Description: Set UART mode enable/disable
 * Parameters : mode - enable/disable UART
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdBleCommSet(IN BMD_COMM_MODE_E mode)
{
	BSP_RESULT_E ret;

	if (mode == BMD_COMM_MODE_ENABLE)
		ret = bsp_write_pin(BSP_PIN_TYPE_BLE_UART_EN, BSP_PIN_SET);
	else
		ret = bsp_write_pin(BSP_PIN_TYPE_BLE_UART_EN, BSP_PIN_RESET);
	
	return (ret == BSP_RESULT_OK ? BMD_RESULT_OK : BMD_RESULT_ERROR);
}

/***********************************************************************
 * Function name: bmdSetRigadoParams
 * Description: Set Rigado module params
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdSetRigadoParams(void)
{
//	BSP_RESULT_E ret;
	
	bmdAtCommandMode(BMD_MODE_AT_COMMAND);
	
	
	
//	return (ret == BSP_RESULT_OK ? BMD_RESULT_OK : BMD_RESULT_ERROR);
	return BMD_RESULT_OK;
}

/***********************************************************************
 * Function name: bmdPowerSet
 * Description: Set Rigado power on/off
 * Parameters : 
 *
 * Returns :
 *		BMD_RESULT_OK - on success
 *		BMD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
BMD_RESULT_E bmdPowerSet(BMD_POWER_E power)
{
	BSP_RESULT_E ret;
		
	if (power == BMD_POWER_OFF)
		ret = bsp_write_pin(BSP_PIN_TYPE_BLE_RESET, BSP_PIN_RESET);
	else if (power == BMD_POWER_ON)
		ret = bsp_write_pin(BSP_PIN_TYPE_BLE_RESET, BSP_PIN_SET);
	else
		ret = BSP_RESULT_ERROR;		
	
	return (ret == BSP_RESULT_OK ? BMD_RESULT_OK : BMD_RESULT_ERROR);
}

void bspUartBleRxCpltCallback(void)
{
	bmdRxCptCallback();
}

void bspUartBleTxCpltCallback()
{
//	if (osMutexRelease(transmitUart2Mutex) != osOK)
	if (osSemaphoreRelease(transmitUart2Mutex) != osOK)
		LOG_ERR("failed to release mutex transmitUart2Mutex\r\n");
}

void bspBleStatus(uint8_t ble_status)
{
	if (ble_status)
		bmdEvent(BMD_EVENT_BLE_CONNECTED);
	else
		bmdEvent(BMD_EVENT_BLE_DISCONNECTED);
}

void bspUartBleErrorCallback(uint32_t hal_err_code)
{
	LOG_ERR("bspUartBleErrorCallback hal_err_code %d\r\n", hal_err_code);
}
