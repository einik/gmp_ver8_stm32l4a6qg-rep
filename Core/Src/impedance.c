
/*
	Electrodes to body connection impedance measurement

	Per each electrode, a sine wave is generated (using PWM) and sampled by ADC
	Per each measurement, 4 sine waves are triggered but only the last one is sampled and taken into account for calculation

*/
#include "cmsis_os.h"
#include "impedance.h"
#include "common.h"
#include "BSP.h"
#include "serial_protocol.h"
#include <stdlib.h>
#include "imp_sin_table.h"

#define IMPEDANCE_NUM_READS_PER_LEAD	3

//limits for positive impedance
#define LO_LIMIT	0.19
#define HI_LIMIT	0.9

// Impedance is calculated along with 2 resistors to be subtracted: r1, r2
#define R_1     (24.9)
#define R_2     (54.9)
#define K_SCHEM	(90.91)	// coefficient
//----------------------------------------------------------------------------------------------------

typedef enum {
	IMPEDANCE_STATE_UNINITIALIZED,
	IMPEDANCE_STATE_IDLE,
	IMPEDANCE_STATE_STARTED,
	IMPEDANCE_STATE_RA_CHECKED,
	IMPEDANCE_STATE_LA_CHECKED,
	IMPEDANCE_STATE_LL_CHECKED,
	IMPEDANCE_STATE_REF_CHECKED,
	IMPEDANCE_STATE_RESULT_READY,
	IMPEDANCE_STATE_ERROR,
	IMPEDANCE_STATE_MAX
} IMPEDANCE_STATE_E;



typedef enum {
	IMPEDANCE_MEASURE_STAGE_LA,
	IMPEDANCE_MEASURE_STAGE_RA,
	IMPEDANCE_MEASURE_STAGE_LL,
	IMPEDANCE_MEASURE_STAGE_REFERENCE,
	IMPEDANCE_MEASURE_STAGE_FINISH             //common ECG mode
}IMPEDANCE_MEASURE_STAGE_E;

typedef struct {
	uint16_t imp_read[IMPEDANCE_NUM_READS_PER_LEAD];
}IMPEDANCE_LEAD_READ_STRUCT;

typedef struct {
	IMPEDANCE_LEAD_READ_STRUCT 	ra;
	IMPEDANCE_LEAD_READ_STRUCT	la;
	IMPEDANCE_LEAD_READ_STRUCT	ll;
	IMPEDANCE_LEAD_READ_STRUCT	ref;
}IMPEDANCE_READ_STRUCT;


static IMPEDANCE_RESULT_E prvImpedanceCalculateLeadsImpedance(IN IMPEDANCE_READ_STRUCT * reads_in, OUT IMPEDANCE_RESULTS_STRUCT * results_kohm);
static IMPEDANCE_RESULT_E prvImpedanceEvaluateImpedanceResults(IN IMPEDANCE_RESULTS_STRUCT * results_kohm, OUT IMPEDANCE_CHECK_RESULT_E * leads_status);


__weak void impedanceEventCallback(IMPEDANCE_EVENT_STRUCT * data){}



static uint8_t 						sine_table_index;
static uint8_t 						sine_cycles_count;
static uint32_t 					imp_adc_result[3];					//3 points x (3leads+reference)   RA,LA,LL, Reference
static IMPEDANCE_LEAD_READ_STRUCT * curr_lead;
static uint8_t 						imp_adc_result_idx;
static osThreadId 					impedance_task_handle;
static osMessageQId 				impedance_queue_handle;
static 	IMPEDANCE_STATE_E 			state = IMPEDANCE_STATE_UNINITIALIZED;
static IMPEDANCE_READ_STRUCT		read_data;
/***********************************************************************
* Function name: prvImpedanceSetMeasuredLead
* Description:  control impedance measure pins
* Parameters : none
*
* Returns :
*		BSP_RESULT_OK - operation success
*
*
***********************************************************************/
IMPEDANCE_RESULT_E prvImpedanceSetMeasuredLead(IMPEDANCE_MEASURE_STAGE_E stage)
{
	switch(stage)
	{
       case IMPEDANCE_MEASURE_STAGE_LA:
			bsp_write_pin(BSP_PIN_TYPE_IMP_RA_SWITCH, BSP_PIN_RESET);
			bsp_write_pin(BSP_PIN_TYPE_IMP_LL_SWITCH, BSP_PIN_RESET);
			bsp_write_pin(BSP_PIN_TYPE_IMP_LA_SWITCH, BSP_PIN_SET);
			break;
       case IMPEDANCE_MEASURE_STAGE_RA:
			bsp_write_pin(BSP_PIN_TYPE_IMP_LA_SWITCH, BSP_PIN_RESET);
			bsp_write_pin(BSP_PIN_TYPE_IMP_LL_SWITCH, BSP_PIN_RESET);
			bsp_write_pin(BSP_PIN_TYPE_IMP_RA_SWITCH, BSP_PIN_SET);
			break;
       case IMPEDANCE_MEASURE_STAGE_LL:
			bsp_write_pin(BSP_PIN_TYPE_IMP_RA_SWITCH, BSP_PIN_RESET);
			bsp_write_pin(BSP_PIN_TYPE_IMP_LA_SWITCH, BSP_PIN_RESET);
			bsp_write_pin(BSP_PIN_TYPE_IMP_LL_SWITCH, BSP_PIN_SET);
			break;
       case IMPEDANCE_MEASURE_STAGE_REFERENCE:
       case IMPEDANCE_MEASURE_STAGE_FINISH:
			bsp_write_pin(BSP_PIN_TYPE_IMP_RA_SWITCH, BSP_PIN_RESET);
			bsp_write_pin(BSP_PIN_TYPE_IMP_LL_SWITCH, BSP_PIN_RESET);
			bsp_write_pin(BSP_PIN_TYPE_IMP_LA_SWITCH, BSP_PIN_RESET);
			break;

	}
	return IMPEDANCE_RESULT_OK;
}

// override BSP
void bspImpedanceAdcResultReadyCallback()
{
	osStatus os_res;
	if(!(sine_table_index & 0x3F))							//index = 0,64,128,192
	{
		if(!sine_table_index)									//start of cycle, index = 0
		{
			if(sine_cycles_count == MEASURING_WAVES_NUMBER)
			{
				bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_STOP, 0);
				// send measure end message
				if (curr_lead != NULL)
				{
					curr_lead->imp_read[0] = imp_adc_result[0];
					curr_lead->imp_read[1] = imp_adc_result[1];
					curr_lead->imp_read[2] = imp_adc_result[2];
					os_res = osMessagePut(impedance_queue_handle, 0, 0);
					if (os_res != osOK)
					{
						LOG_ERR("bspImpedanceAdcResultReadyCallback: osMessagePut error %d", os_res);
					}
				}

				return;
			}
			sine_cycles_count++;
			imp_adc_result_idx = 0;							//prepare index for measure
		}
		else if(sine_cycles_count == MEASURING_WAVES_NUMBER)	//end wave and one from measuring points(64,128,192)
		{
				//start and read ADC
			BSP_RESULT_E res = bspReadAdcIt(BSP_ADC_IMPEDANCE);
			if(BSP_RESULT_OK != res)
			{
				LOG_ERR("Impedance: ADC result error \r\n");
				state = IMPEDANCE_STATE_ERROR;

				IMPEDANCE_EVENT_STRUCT e;

				e.event_type = IMPEDANCE_EVENT_MEASUREMENT_FAILED;
            	e.leads_status = (IMPEDANCE_CHECK_RESULT_ERROR_LL | IMPEDANCE_CHECK_RESULT_ERROR_LA | IMPEDANCE_CHECK_RESULT_ERROR_RA) ;
            	e.measurements.ra_kohm = e.measurements.la_kohm = e.measurements.ll_kohm = DISCONNECTED_VAL;
            	impedanceEventCallback(&e);

				return;
			}
			for (uint32_t i=0; i<0xff; i++);
			return;	// Break operation so index & PWM will start when ADC read is finsished
		}
	}
//	for (uint32_t i=0; i<0xff; i++);

	sine_table_index++;

	bspImpedanceSetPwmWidth(imp_sin_table[sine_table_index]);  //next value from sine table

}
void bspAdcReadCompleteCallbackImpedance(uint32_t val)
{
	imp_adc_result[imp_adc_result_idx++] = val;
	sine_table_index++;
	bspImpedanceSetPwmWidth(imp_sin_table[sine_table_index]);  //next value from sine table
}

static void impedanceTask(void const * argument)
{
	osEvent  	ret;
	IMPEDANCE_RESULTS_STRUCT results_kohm = {0};
	for(;;)
	{
		ret = osMessageGet(impedance_queue_handle, osWaitForever);
		if (ret.status != osEventMessage)
		{
			LOG("impedanceTask: got wrong message\r\n");
			continue;
		}

	  	switch (state)
		{
			case IMPEDANCE_STATE_STARTED:
				//RA check start
//				impADCbufferIdx = 0;
				curr_lead = &read_data.ra;
				sine_table_index = 0;
				sine_cycles_count = 0;
				imp_adc_result_idx = 0;
				prvImpedanceSetMeasuredLead(IMPEDANCE_MEASURE_STAGE_RA);
				state = IMPEDANCE_STATE_RA_CHECKED;
				bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_START, PWM_MIDDLE);
				break;
			case IMPEDANCE_STATE_RA_CHECKED:
				//LA check start
				curr_lead = &read_data.la;
				sine_table_index = 0;
				sine_cycles_count = 0;
				prvImpedanceSetMeasuredLead(IMPEDANCE_MEASURE_STAGE_LA);
				state = IMPEDANCE_STATE_LA_CHECKED;
				bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_START, PWM_MIDDLE);
				break;
			case IMPEDANCE_STATE_LA_CHECKED:
				//LL check start
				curr_lead = &read_data.ll;
				sine_table_index = 0;
				sine_cycles_count = 0;
				prvImpedanceSetMeasuredLead(IMPEDANCE_MEASURE_STAGE_LL);
				state = IMPEDANCE_STATE_LL_CHECKED;
				bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_START, PWM_MIDDLE);
				break;
			case IMPEDANCE_STATE_LL_CHECKED:
				//REF check start
				curr_lead = &read_data.ref;
				sine_table_index = 0;
				sine_cycles_count = 0;
				prvImpedanceSetMeasuredLead(IMPEDANCE_MEASURE_STAGE_REFERENCE);
				state = IMPEDANCE_STATE_REF_CHECKED;
				bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_START, PWM_MIDDLE);
				break;
			case IMPEDANCE_STATE_REF_CHECKED:
				//return to common ECG stste
				prvImpedanceSetMeasuredLead(IMPEDANCE_MEASURE_STAGE_FINISH);
//				state = IMPEDANCE_STATE_RESULT_READY;
				bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_STOP, 0);
//				break;
//			case IMPEDANCE_STATE_RESULT_READY:
				IMPEDANCE_CHECK_RESULT_E leads_status;
				IMPEDANCE_EVENT_STRUCT e;
                prvImpedanceCalculateLeadsImpedance(&read_data, &e.measurements);
            	prvImpedanceEvaluateImpedanceResults(&e.measurements, &leads_status);

            	e.event_type = IMPEDANCE_EVENT_RESULT_READY;
            	e.leads_status = leads_status;
//            	memcpy(&e.measurements, &results_kohm, sizeof(e.measurements));

				state = IMPEDANCE_STATE_IDLE;
				
            	impedanceEventCallback(&e);
				//calculate and send impedance data
				//return to ECG
				// event data ready
				
				break;
			case IMPEDANCE_STATE_IDLE:
				//wait for period end (for periodic measure
				//wait for command
				break;
			case IMPEDANCE_STATE_ERROR:


				LOG_ERR("\r\n Impedance measure error\r\n");
//				state = IMPEDANCE_STATE_IDLE;
				break;
        }
	}
}


IMPEDANCE_RESULT_E impedanceInitTask(void)
{
	if (state != IMPEDANCE_STATE_UNINITIALIZED)
    {
        return IMPEDANCE_RESULT_ERROR_ALREADY_INITIALIZED;
    }

	osMessageQDef(impedanceQueue, 1, uint8_t );	// no real declaration, just release queue from waiting
    impedance_queue_handle = osMessageCreate(osMessageQ(impedanceQueue), NULL);
    if (!impedance_queue_handle)
    	return IMPEDANCE_RESULT_ERROR_QUEUE;

   	osThreadDef(processImpedanceTask, impedanceTask, osPriorityNormal, 0, IMPEDANCE_STACK_SIZE);
    impedance_task_handle = osThreadCreate(osThread(processImpedanceTask), NULL);
    if (!impedance_task_handle)
    	return IMPEDANCE_RESULT_ERROR_TASK_CREATE_FAILED;

	//init ADC, PWM timer
//	BSP_RESULT_E res = bspImpedanceHWinit(PWM_MAX, PWM_MIDDLE);
//	if(BSP_RESULT_OK != res)
//	{
//		LOG_ERR("Failed to init impedance:  %d\r\n", res);
//		return IMPEDANCE_RESULT_ERROR_BSP;
//	}

	state = IMPEDANCE_STATE_IDLE;

	return IMPEDANCE_RESULT_OK;
}


IMPEDANCE_RESULT_E impedanceStart(void)
{
	if (state != IMPEDANCE_STATE_IDLE)
    {
        return IMPEDANCE_RESULT_ERROR_ALREADY_RUNNING;
    }
	//init ADC, PWM timer
	BSP_RESULT_E res = bspImpedanceHWinit(PWM_MAX, PWM_MIDDLE);
	if(BSP_RESULT_OK != res)
	{
		LOG_ERR("Failed to start impedance:  %d\r\n", res);
		return IMPEDANCE_RESULT_ERROR_BSP;
	}

	state = IMPEDANCE_STATE_STARTED;

	osStatus os_res = osMessagePut(impedance_queue_handle, 0, 0);
	if (os_res != osOK)
	{
		LOG_ERR("impedanceStart: osMessagePut error %d\r\n", os_res);
	}

	return IMPEDANCE_RESULT_OK;
}


IMPEDANCE_RESULT_E impedanceStop(void)
{
	if (state == IMPEDANCE_STATE_IDLE)
    {
        return IMPEDANCE_RESULT_ERROR_NOT_RUNNING;
    }

	// TODO: is BSP DE-INIT NEEDED?

	prvImpedanceSetMeasuredLead(IMPEDANCE_MEASURE_STAGE_FINISH);

	bspImpedancePwmStart(BSP_IMPEDANCE_PWM_MODE_STOP, 0);

	return IMPEDANCE_RESULT_OK;
}


static IMPEDANCE_RESULT_E prvImpedanceCalculateLeadAmplitude(IMPEDANCE_LEAD_READ_STRUCT * reads_in, OUT float64_t * amplitude_out)
{
	if ( (reads_in == NULL) || (amplitude_out == NULL) )
		return IMPEDANCE_RESULT_ERROR_WRONG_PARAM;

	float64_t y1,y2;
	float64_t temp;

	y1 = (float64_t)reads_in->imp_read[0] - (float64_t)reads_in->imp_read[1];
	y2 = (float64_t)reads_in->imp_read[1] - (float64_t)reads_in->imp_read[2];
	temp = (y1 - y2)/(y1 + y2);			//tangens
	temp *= temp;						//tg^2
	temp += 1.0;
	*amplitude_out = ((y1+y2)/2)*sqrt(temp);
	return IMPEDANCE_RESULT_OK;
}

// returns impedance value (kOhm)
static IMPEDANCE_RESULT_E prvImpedanceCalculateImpedanceValue(float64_t LeadAmpl,float64_t ReferenceAmpl, uint16_t * impedance_out)
{
	if (impedance_out == NULL)
		return IMPEDANCE_RESULT_ERROR_WRONG_PARAM;

	float64_t rate,amplitude,impedance;

    amplitude = abs((int16_t)LeadAmpl);		//get absolute value
	rate = amplitude/ReferenceAmpl;

	if(rate < LO_LIMIT)
		rate = LO_LIMIT;

	if(rate >= HI_LIMIT)
	{
		*impedance_out = DISCONNECTED_VAL;
	    return IMPEDANCE_RESULT_OK;
	}

    impedance = ( ( K_SCHEM * rate )/( 1 - rate ) ) - R_1 - R_2;

    if(impedance < 0)
    	*impedance_out = 0;
    else
    	*impedance_out = (uint16_t)(impedance + 0.5);   // round up

    return IMPEDANCE_RESULT_OK;
}

static IMPEDANCE_RESULT_E prvImpedanceCalculateLeadsImpedance(IN IMPEDANCE_READ_STRUCT * reads_in, OUT IMPEDANCE_RESULTS_STRUCT * results_kohm)
{
	if ( (reads_in == NULL) || (results_kohm == NULL) )
		return IMPEDANCE_RESULT_ERROR_WRONG_PARAM;

	float64_t amp_ref, amp_ra, amp_la, amp_ll;
	IMPEDANCE_RESULT_E res;

	res = prvImpedanceCalculateLeadAmplitude(&reads_in->ra, &amp_ra);
	res |= prvImpedanceCalculateLeadAmplitude(&reads_in->la, &amp_la);
	res |= prvImpedanceCalculateLeadAmplitude(&reads_in->ll, &amp_ll);
	res |= prvImpedanceCalculateLeadAmplitude(&reads_in->ref, &amp_ref);

	res |= prvImpedanceCalculateImpedanceValue(amp_ra,amp_ref, &results_kohm->ra_kohm);
	res |= prvImpedanceCalculateImpedanceValue(amp_la,amp_ref, &results_kohm->la_kohm);
	res |= prvImpedanceCalculateImpedanceValue(amp_ll,amp_ref, &results_kohm->ll_kohm);

	if (res != IMPEDANCE_RESULT_OK)
		return IMPEDANCE_RESULT_ERROR;

	return IMPEDANCE_RESULT_OK;
}

static IMPEDANCE_RESULT_E prvImpedanceEvaluateImpedanceResults(IN IMPEDANCE_RESULTS_STRUCT * results_kohm, OUT IMPEDANCE_CHECK_RESULT_E * leads_status)
{
	if ( (results_kohm == NULL) || (leads_status == NULL) )
		return IMPEDANCE_RESULT_ERROR_WRONG_PARAM;

	*leads_status = IMPEDANCE_CHECK_RESULT_OK;	// init

	// TODO: define thresholds (instead of DISCONNECTED_VAL)
	if (results_kohm->ra_kohm >= DISCONNECTED_VAL)
		*leads_status |= IMPEDANCE_CHECK_RESULT_ERROR_RA;

	if (results_kohm->la_kohm >= DISCONNECTED_VAL)
		*leads_status |= IMPEDANCE_CHECK_RESULT_ERROR_LA;

	if (results_kohm->ll_kohm >= DISCONNECTED_VAL)
		*leads_status |= IMPEDANCE_CHECK_RESULT_ERROR_LL;

	return IMPEDANCE_RESULT_OK;
}

IMPEDANCE_RESULT_E impedanceGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (impedance_task_handle == NULL) || (ret_val == NULL) )
		return IMPEDANCE_RESULT_ERROR_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark( impedance_task_handle );
	return IMPEDANCE_RESULT_OK;
}
