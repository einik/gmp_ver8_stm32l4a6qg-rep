#include "serial_protocol.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <stdlib.h>
#include "common.h"
#include "BSP.h"
#include "timers.h"


/****************************************************************/
/*						Local Definitions						*/
#define SERIAL_PROTOCOL_PRINT_TIMEOUT 			osWaitForever//0 //1000
#define SERIAL_PROTOCOL_FLUSH_TIMEOUT 			100	//osWaitForever//0 //1000
#define UART_TX_DATA_SIZE  						129//128//64 //4
#define COMMAND_MAX_SIZE  						150//20
#define UART_NUM_OF_TX_BUFFERS 					2
#define MAX_ERROR_MSG_SIZE 						100
#define ERROR_STRING 							"\r\nError: "
#define UART_RX_MAX_BUFFER  					50
#define SERIAL_PROTOCOL_RX_STRUCT_ARRAY_LENGTH	4

#define 	SERIAL_PROTOCOL_LOG_HEX DEBUG_LEVEL_4


typedef enum{
	SERIAL_PROTOCOL_INIT_STATUS_UNINITIALIZED,
	SERIAL_PROTOCOL_INIT_STATUS_INITIALIZED,
	SERIAL_PROTOCOL_INIT_STATUS_HALTED,
}SERIAL_PROTOCOL_INIT_STATUS;

typedef enum {
	SERIAL_PROTOCOL_ECHO_OFF,
	SERIAL_PROTOCOL_ECHO_ON,
	SERIAL_PROTOCOL_ECHO_MAX
}SERIAL_PROTOCOL_ECHO_E;

typedef struct {
	uint8_t		data[UART_RX_MAX_BUFFER];
	uint16_t 	length;
}SERIAL_PROTOCOL_RX_STRUCT;
/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
static uint8_t 							uartTxBuffer[UART_NUM_OF_TX_BUFFERS][UART_TX_DATA_SIZE];// Tx double buffer
static uint8_t 							lastErrorMsg[MAX_ERROR_MSG_SIZE] = ERROR_STRING;
#ifdef 	SERIAL_PROTOCOL_FAST
#else	// SERIAL_PROTOCOL_FAST
static uint8_t                             uartRxBuffer[UART_RX_MAX_BUFFER];
#endif //SERIAL_PROTOCOL_FAST
static uint8_t 							timer_flag = FALSE;
osThreadId 								debugUartRxTaskHandle;
osMessageQId 							debugUartRxQueueHandle;
static osMutexId 						printMutex;
static osSemaphoreId					transmitMutex;
static uint16_t 						buffer_index = 0; //index of tx buffer
static uint16_t 						print_counter = 0;
static const command_desc * 			commandList = NULL;
static int 								command_list_max;
static command_handler 					handlerCallback = null;
static SERIAL_PROTOCOL_INIT_STATUS		serial_protcol_init_state = SERIAL_PROTOCOL_INIT_STATUS_UNINITIALIZED;
static SERIAL_PROTOCOL_ECHO_E			echo_state = SERIAL_PROTOCOL_ECHO_ON;
#ifdef MENNEN_ALGO
static TimerHandle_t					uart_debug_timer;
#endif
static uint32_t							prv_current_debug_set[DEBUG_MODULE_MAX];
static SERIAL_PROTOCOL_RX_STRUCT 		rx_s[SERIAL_PROTOCOL_RX_STRUCT_ARRAY_LENGTH];
static uint8_t							rx_s_index;
static DEBUG_LEVEL 						currentDebugLevel;
static uint32_t 						flush_error_count;
//Retargeting stdout for printf use
//FILE __stdout;
osStatus os_res;
/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/
static void 							debugUartRxTask(void const * argument);
static void 							prvSerialProtocolFlushBuffer();
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/

/****************************************************************/

/****************************************************************/
/*						Weak Functions							*/
__weak void serial_protocol_ecg(uint8_t* buff, uint8_t length){}
__weak void serialProtocolExternalProtocolInterfaceTx(uint8_t *buff, uint16_t length){}
/****************************************************************/


RESULT_E serialProtocolSetCommands(command_handler rxCallbackFunc, const command_desc * cmd_list, int cmd_list_max)
{
	if (cmd_list == NULL)
		  return RESULT_WRONG_PARAMTER;
	if (cmd_list_max == 0)
		  return RESULT_WRONG_PARAMTER;
	if (rxCallbackFunc == NULL)
		  return RESULT_WRONG_PARAMTER;

	handlerCallback = rxCallbackFunc;
	commandList = cmd_list;
	command_list_max = cmd_list_max;

	return RESULT_OK;

}


static void prvSerialProtocolPrintMenu()
{
  uint32_t i=0;
  
  LOG("\r\n\r\nCommand list:\r\n");
    
  while (commandList[i].commandType !=command_list_max)
  {
	LOG("%s\t%s\r\n", commandList[i].command_string, commandList[i].description);
    i++;
  }
  LOG("\r\n");
}

//DEBUG

void setDebugLevel(DEBUG_LEVEL dbgLvl)
{
	currentDebugLevel = (DEBUG_LEVEL)(dbgLvl | DEBUG_LEVEL_ERROR | DEBUG_LEVEL_ALWAYS); // always print errors
}


static void prvSerialProtocolFlushBuffer()
{
	BSP_RESULT_E bsp_res;
	if (print_counter == 0)
		return;
  
//	osStatus os_res = osMutexWait(transmitMutex,SERIAL_PROTOCOL_FLUSH_TIMEOUT);
 	osStatus os_res = osSemaphoreWait(transmitMutex,SERIAL_PROTOCOL_FLUSH_TIMEOUT);
  if (os_res != osOK)
   {
      flush_error_count++;
   }
	bsp_res = bsp_uart_tx_dma_serial_comm(&uartTxBuffer[buffer_index][0], print_counter);
	
	if (bsp_res != BSP_RESULT_OK)
	{
//      PRINT("\t\nflush_buffer failed");
		if (osSemaphoreRelease(transmitMutex) != osOK)
		{	
//         PRINT(", transmitMutex failed flush\r\n");		
      }
	}
		
	print_counter = 0;
	buffer_index ^=1;  
} 

void bsp_uart_debug_tx_cplt_callback()
{
	os_res = osSemaphoreRelease(transmitMutex);
	if (os_res != osOK)
	{
		LOG_ERR("transmitMutex failed cb\r\n");
	}
}

//Retargeting stdout for printf use - start
int _write(int file, char *data, int len)
{
	int bytes_written;
	for (bytes_written = 0; bytes_written < len; bytes_written++) {
		uartTxBuffer[buffer_index][print_counter++] = *(data + bytes_written);

		if (print_counter >= UART_TX_DATA_SIZE)
			prvSerialProtocolFlushBuffer();
	}
	return bytes_written;
}


int fputc(int ch, FILE *f) {
    uartTxBuffer[buffer_index][print_counter++] = ch;

    if (print_counter >= UART_TX_DATA_SIZE)
      prvSerialProtocolFlushBuffer();

    return ch;
}

int fputhex(int ch, FILE *f) {
	
	
	if (print_counter >= UART_TX_DATA_SIZE)
      prvSerialProtocolFlushBuffer();	
	
	sprintf((char*)&uartTxBuffer[buffer_index][print_counter], "%02X ", ch);
    //uartTxBuffer[buffer_index][print_counter++] = ch;
	print_counter += 3;
	
    

    return ch;
}
//Retargeting stdout for printf use - end




RESULT_E serialProtocolInit()
{  
#ifndef USB_MODE
	if (bspUart1Valid() == BSP_RESULT_OK)
	{
		if (bspUart1Init() != BSP_RESULT_OK)
			return RESULT_ERROR;
		serial_protcol_init_state = SERIAL_PROTOCOL_INIT_STATUS_INITIALIZED;
	}
	else

		serial_protcol_init_state = SERIAL_PROTOCOL_INIT_STATUS_UNINITIALIZED;
	
	setvbuf( stdout, 0, _IONBF, 0 ); //Coocox
	osMutexDef(printMutex);
//	osMutexDef(transmitMutex);
	osSemaphoreDef(transmitMutex);

	//  setDebugLevel(DEBUG_LEVEL_ERROR);
	  setDebugLevel((DEBUG_LEVEL)SERIAL_PROTOCOL_DEFAULT_DEBUG_LEVEL);
	printMutex = osMutexCreate (osMutex(printMutex));
	if (!printMutex)
		return RESULT_ERROR;

//	transmitMutex = osMutexCreate (osMutex(transmitMutex));
	transmitMutex = osSemaphoreCreate (osSemaphore(transmitMutex), 1);
	if (!transmitMutex)
		return RESULT_ERROR;

	//init queue
#ifdef SERIAL_PROTOCOL_FAST
	osMessageQDef(debugUartRxQueue, SERIAL_PROTOCOL_RX_STRUCT_ARRAY_LENGTH, SERIAL_PROTOCOL_RX_STRUCT * );
#else	//SERIAL_PROTOCOL_FAST
    osMessageQDef(debugUartRxQueue, 3, uint8_t );
#endif	//SERIAL_PROTOCOL_FAST
	debugUartRxQueueHandle = osMessageCreate(osMessageQ(debugUartRxQueue), NULL);
	if (!debugUartRxQueueHandle)
		return RESULT_ERROR;

	//Init task
	osThreadDef(debugUartRxTask, debugUartRxTask, osPriorityNormal, 0, SERIAL_PROTOCOL_STACK_SIZE);
	debugUartRxTaskHandle = osThreadCreate(osThread(debugUartRxTask), NULL);
	if (!debugUartRxTaskHandle)
		return RESULT_ERROR;
#endif
	return RESULT_OK;
}

SERIAL_PROTOCOL_RESULT_E serialProtocolGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (debugUartRxTaskHandle == NULL) || (ret_val == NULL) )
		return SERIAL_PROTOCOL_RESULT_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark( debugUartRxTaskHandle );
	return SERIAL_PROTOCOL_RESULT_OK;
}






SERIAL_PROTOCOL_RESULT_E serialProtocolSetModuleLogLevel(DEBUG_MODULE_E module, DEBUG_LEVEL level)
{
	if ( (module == DEBUG_MODULE_MAX) || (level >= DEBUG_LEVEL_5) )
		return SERIAL_PROTOCOL_RESULT_WRONG_PARAM;

	prv_current_debug_set[module] = level | DEBUG_LEVEL_ERROR | DEBUG_LEVEL_ALWAYS;;

	if (level & SERIAL_PROTOCOL_LOG_HEX)
		prv_current_debug_set[module] |= DEBUG_LEVEL_HEX;
	else
		prv_current_debug_set[module] &= ~DEBUG_LEVEL_HEX;


	return SERIAL_PROTOCOL_RESULT_OK;
}



SERIAL_PROTOCOL_RESULT_E serialProtocolModuleLog(DEBUG_MODULE_E module, DEBUG_LEVEL dbg_level, const char* format, ...)
{

#ifdef MENNEN_ALGO
  return SERIAL_PROTOCOL_RESULT_OK;
#endif

  static uint8_t error_exists = 0;
  int res = 0;
  osStatus res_mutex;
  va_list arglist;

  if (serial_protcol_init_state != SERIAL_PROTOCOL_INIT_STATUS_INITIALIZED || module >= DEBUG_MODULE_MAX)
	  return SERIAL_PROTOCOL_RESULT_WRONG_PARAM;

  va_start( arglist, format );

  //If in interrupt handler
  if (__get_IPSR())
  {
	  //save message only if it is an error and last error was already printed
	  if ((dbg_level == DEBUG_LEVEL_ERROR) && (!error_exists))
	  {
		  vsprintf((char*)lastErrorMsg + sizeof(ERROR_STRING)-1, format, arglist);
		  error_exists = 1;
	  }
  }
  //if called from task
  else
  {
	  //print error if exists
	  if (error_exists)
	  {
		  res_mutex = osMutexWait(printMutex,SERIAL_PROTOCOL_PRINT_TIMEOUT);
		  res = printf((char*)lastErrorMsg);
		  error_exists = 0;
		  prvSerialProtocolFlushBuffer();
		  res_mutex = osMutexRelease(printMutex);
	  }

	  //print message
	  if ( (prv_current_debug_set[module] & dbg_level) || (module == DEBUG_MODULE_MASTER) )
	  {
		  if (dbg_level & DEBUG_LEVEL_HEX)
		  {
			  res_mutex = osMutexWait(printMutex,SERIAL_PROTOCOL_PRINT_TIMEOUT);
			  //if (dbgLvl == DEBUG_LEVEL_ERROR)
				//  printf(ERROR_STRING);
			  //res = vprintf( format, arglist );
			  uint32_t a = va_arg(arglist, uint32_t);

			  for (int i=0; i < a; i++)
				fputhex(format[i], NULL);

			  prvSerialProtocolFlushBuffer();
			  //bsp_uart_tx_dma(BSP_UART_DEBUG, format, a);
			  res_mutex = osMutexRelease(printMutex);

		  }
		  else
		  {
			  res_mutex = osMutexWait(printMutex,SERIAL_PROTOCOL_PRINT_TIMEOUT);
			  if (dbg_level == DEBUG_LEVEL_ERROR)
				  printf(ERROR_STRING);
			  res = vprintf( format, arglist );
			  prvSerialProtocolFlushBuffer();
			  res_mutex = osMutexRelease(printMutex);
		  }
	  }
//	  else if (dbg_level == DEBUG_LEVEL_RAW)
//	  {
//		  res_mutex = osMutexWait(printMutex,SERIAL_PROTOCOL_PRINT_TIMEOUT);
//		  //if (dbgLvl == DEBUG_LEVEL_ERROR)
//			//  printf(ERROR_STRING);
//		  //res = vprintf( format, arglist );
//		  uint32_t a = va_arg(arglist, uint32_t);
//
//		  for (int i=0; i < a; i++)
//			fputc(format[i], NULL);
//
//		  flush_buffer();
//		  //bsp_uart_tx_dma(BSP_UART_DEBUG, format, a);
//		  res_mutex = osMutexRelease(printMutex);
//
//	  }

  }

  va_end(arglist);
  //resMutex = resMutex; //TODO:
  return SERIAL_PROTOCOL_RESULT_OK;
}
void bsp_uart_debug_rx_cplt_callback()
{

	uint16_t length = 0;
	
	if (!timer_flag)
	{
		//BSP_RESULT_E res= bsp_uart_receive_dma(BSP_UART_DEBUG, &uartRxBuffer[1], UART_RX_MAX_BUFFER-1);
		if (osMessagePut (debugUartRxQueueHandle, 1, 0) != osOK)
			LOG_ERR("bsp_uart_debug_rx_cplt_callback\r\n");
	}
	else
	{
		
		timer_flag = FALSE;
		bspUartDmaDataLeft(BSP_UART_DEBUG, &length);
		length = UART_RX_MAX_BUFFER - length;
		if ((length > 0) && ((length <= UART_RX_MAX_BUFFER)))
			if (osMessagePut (debugUartRxQueueHandle, length, 0) != osOK)
				LOG_ERR("bsp_uart_debug_rx_cplt_callback\r\n");
	}	
}

/* processECGBufTask function */

void bspUartErrorCallback(uint32_t hal_err_code)
{
	LOG_ERR("bspUartErrorCallback hal_err_code %d\r\n", hal_err_code);
}
#ifdef MENNEN_ALGO

static TimerHandle_t                     uart_debug_timer;
//static uint8_t							uartRxBuffer[1];
void uart_debug_timer_callback(TimerHandle_t xTimer)
{
    //HAL_UART_DMAStop(&huart1);
    bsp_uart_stop_dma(BSP_UART_DEBUG);
       bsp_uart_receive_it(BSP_UART_DEBUG, uartRxBuffer, 1);
}


void debugUartRxTask(void const * argument)
{
	osEvent  ret;
	uint8_t nextChar, commandIndex=0;
	char commandBuf[COMMAND_MAX_SIZE+1];
	uint32_t i=0;
	char * pch;
	RESULT_E result = RESULT_OK;
	command_param param;
	//bsp_uart_receive_dma(BSP_UART_DEBUG, uartRxBuffer, 1);
	bsp_uart_receive_it(BSP_UART_DEBUG, uartRxBuffer, 1);
	
	UBaseType_t uxHighWaterMark;
	uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
//	uart_debug_timer = xTimerCreate("UART Debug Timer",pdMS_TO_TICKS(20), pdFALSE, ( void * ) 0, uart_debug_timer_callback );
	
	while(1)
	{
		ret =  osMessageGet (debugUartRxQueueHandle, osWaitForever );
    	if (ret.status == osEventMessage)
    	{
		
			if (ret.value.v == 0xFF)
			{
				xTimerStart( uart_debug_timer, 0);
				timer_flag = TRUE;
			}
			else
			{
				errno = 0;
				//uartRxBuffer		//Vladimir: Buffer
				//ret.value.v		//Vladimir: Buffer length
				serial_protocol_ecg(uartRxBuffer, ret.value.v);
			}
    	}
        //HAL_UART_Receive_DMA(&huart1, &uartRxBuff, 1);
		//bsp_uart_receive_dma(BSP_UART_DEBUG, uartRxBuffer, 1);
		bsp_uart_receive_it(BSP_UART_DEBUG, uartRxBuffer, 1);

  	} 
}


void print_buffer(char* string, uint8_t* ptr_data, uint16_t length)
{
	uint16_t 			i;
	
	LOG("%s: ",string);
	for (i = 0;i < length;i++)
		LOG("%02X ",ptr_data[i]);
	LOG("\r\n");

}

//---------------------- VH -------------------------------------
int send_tx_data(uint8_t data[], int length)
{
	if ( (length == 0) || (data == NULL) )
		return -1;
//	return 0;
//	bsp_uart_tx_dma_serial_comm(data, length);
    int i;
//    osStatus resMutex;

    osMutexWait(printMutex,SERIAL_PROTOCOL_PRINT_TIMEOUT);

    for(i=0; i<length;i++)
    {
        uartTxBuffer[buffer_index][print_counter++] = data[i];
        if (print_counter >= UART_TX_DATA_SIZE)
            prvSerialProtocolFlushBuffer();
    }
    if (print_counter)
        prvSerialProtocolFlushBuffer();
    
    osMutexRelease(printMutex);
  
  return length;
}

#else	//MENNEN_ALGO





void bspSerialCallback(uint8_t * ptr, uint16_t length)
{
//	uartRxBuffer[0] = ptr[0];
	if ( (length >= UART_RX_MAX_BUFFER) || (ptr == NULL) )
	{
		LOG_ERR("bspSerialCallback error length %d, ptr %x\r\n", length, ptr);
		return;
	}
	memcpy(rx_s[rx_s_index].data, ptr, length);
	rx_s[rx_s_index].length = length;

	if (osMessagePut (debugUartRxQueueHandle, (uint32_t)&rx_s[rx_s_index], 0) != osOK)
		LOG_ERR("bsp_uart_debug_rx_cplt_callback\r\n");

	rx_s_index = (rx_s_index + 1) % SERIAL_PROTOCOL_RX_STRUCT_ARRAY_LENGTH;
}


static SERIAL_PROTOCOL_RESULT_E prvSerialProtocolHandleChar(uint8_t nextChar)
{

	static uint8_t commandIndex=0;
	static char commandBuf[COMMAND_MAX_SIZE+1];
	uint32_t i=0;
	char * pch;
	RESULT_E result = RESULT_OK;
	command_param param;

	if (echo_state == SERIAL_PROTOCOL_ECHO_ON)
		LOG("%c",nextChar);//echo
	//Command exceeded size
	if (commandIndex >= COMMAND_MAX_SIZE)
		commandIndex = 0;

	//save to command buffer
	switch (nextChar)
	{
	case '\b':
		if (commandIndex > 0 )
		{
			LOG(" \b");//delete char
			commandIndex--;
		}
	  break;
	case '\t':
		prvSerialProtocolPrintMenu();
		commandIndex = 0;
	break;
	default:
		commandBuf[commandIndex++] = nextChar;
		break;
	case ' ':
		if ((commandIndex != 0) && (commandBuf[commandIndex-1] != ' '))
		  commandBuf[commandIndex++] = nextChar;
	break;
	case '\r':
		LOG("\n");
		commandBuf[commandIndex] = null;
		if ((commandIndex > 0) && (handlerCallback != null))
		  {
			result = RESULT_BAD_COMMAND;
			pch = strtok (commandBuf," ");
			if (pch != null)
			{
			//find command in list
			  for (i=0;commandList[i].commandType !=command_list_max;i++)
			  {
				  if (strcmp(pch, commandList[i].command_string) == 0)
				  {
					//check param
					pch = strtok (null, " ");
					//command has no params
					if (commandList[i].command_param == COMMAND_PARAM_NONE)
					{
					  if (pch != null)
						break;
					}
					else //command has a param
					{
					  if (pch == null)
						break;

					  if (commandList[i].command_param == COMMAND_PARAM_INT)
					  {
						errno = 0;
						param.i = strtol (pch,null,0);
						// if (errno == EINVAL)
						if (errno != 0)
						  break;
					  }
					  else if (commandList[i].command_param == COMMAND_PARAM_STR)
					  {
						param.str = pch;
					  }
					  else if (commandList[i].command_param == COMMAND_PARAM_STR_2)
					  {
						param.str = pch;
						pch = strtok (null," ");
						if (pch !=NULL)
							param.str2 = pch;
						else
							param.str2 = NULL;
					  }
					  else if (commandList[i].command_param == COMMAND_PARAM_CHAR)
					  {
						if (*(pch+1)!=null)
						  break;
						param.c = *pch;
					  }

					  //check that there are no other params
					  if (strtok (null, " ") != null)
						break;
					}


					//execute command
					result = handlerCallback(&commandList[i],  &param);
					break;
				  }
			  }
			}

			switch (result)
			{
				case RESULT_OK:
					LOG("%s\r\n",commandList[i].ack);
					break;
				case RESULT_ERROR:
					LOG("An error has occured\r\n");
					break;
				case RESULT_ERROR_ALREADY_RUNNING:
					LOG("Already running\r\n");
					break;
				case RESULT_BAD_COMMAND:
					if (i == command_list_max) //command not found
						LOG("%s\r\n",commandList[i].ack);
					else
						LOG("bad parameter\r\n");
					break;
			}
		  }
		  commandIndex = 0;

	  break;
	}
	return SERIAL_PROTOCOL_RESULT_OK;
}

#ifdef SERIAL_PROTOCOL_FAST
void debugUartRxTask(void const * argument)
{
	osEvent  ret;

	bspUartDmaCyclicSerialStart();
	

	while(1)
	{
		ret =  osMessageGet (debugUartRxQueueHandle, osWaitForever );
		if (ret.status == osEventMessage)
    	{
			SERIAL_PROTOCOL_RX_STRUCT * rx = (SERIAL_PROTOCOL_RX_STRUCT *)ret.value.p;

			if (rx->length == 0)
			{
				bsp_uart_stop_dma(BSP_UART_DEBUG);
				bspUartDmaCyclicSerialStart();
				continue;
			}

			for (uint8_t i = 0; i < rx->length; i++)
			{
				prvSerialProtocolHandleChar(rx->data[i]);
			}
    	}
	}
}

#else	//SERIAL_PROTOCOL_FAST
void debugUartRxTask(void const * argument)
{
	osEvent  ret;
	char commandBuf[COMMAND_MAX_SIZE+1];
	uint32_t i=0;
	char * pch;
	RESULT_E result = RESULT_OK;
	command_param param;
	uint8_t nextChar, commandIndex = 0;


	// TODO: Error with DMA interrupts - for some reason gets more tx complete than tx starts
	if (bsp_uart_receive_it(BSP_UART_DEBUG, uartRxBuffer, 1) != BSP_RESULT_OK)
	{
		LOG("debugUartRxTask error calling bsp_uart_receive_dma\r\n");
	}

	while(1)
	{
		ret =  osMessageGet (debugUartRxQueueHandle, osWaitForever );

		if (ret.status == osEventMessage)
    	{
			prvSerialProtocolHandleChar(uartRxBuffer[0]);

    	}
//      if (bsp_uart_receive_dma(BSP_UART_DEBUG, uartRxBuffer, 1) != BSP_RESULT_OK)
    	// TODO: Error with DMA interrupts - for some reason gets more tx complete than tx starts
      if (bsp_uart_receive_it(BSP_UART_DEBUG, uartRxBuffer, 1) != BSP_RESULT_OK)
      {
         LOG("debugUartRxTask error calling bsp_uart_receive_dma\r\n");
      }
//		bsp_uart_receive_dma_serial();

	}
}

#endif	//SERIAL_PROTOCOL_FAST

SERIAL_PROTOCOL_RESULT_E serialProtocolLogsEnable(uint8_t en)
{
	serial_protcol_init_state = (en == 0 ? SERIAL_PROTOCOL_INIT_STATUS_HALTED : SERIAL_PROTOCOL_INIT_STATUS_INITIALIZED);
	return SERIAL_PROTOCOL_RESULT_OK;
}

SERIAL_PROTOCOL_RESULT_E serialProtocolEchoEnable(uint8_t en)
{
	if (en >= SERIAL_PROTOCOL_ECHO_MAX)
		return SERIAL_PROTOCOL_RESULT_WRONG_PARAM;

	if (en == 0)
		echo_state = SERIAL_PROTOCOL_ECHO_OFF;
	else
		echo_state = SERIAL_PROTOCOL_ECHO_ON;
	return SERIAL_PROTOCOL_RESULT_OK;
}
#endif //MENNEN_ALGO

