#include "pbs.h"
#include "ecg.h"
#include "cmsis_os.h"
#include "serial_protocol.h"
#include "versions.h"
#include "flash_if.h"
#include "math.h"
#include "string.h"
#include "global_defs.h"
#include "device.h"
#include "leds.h"
#include "timers.h"
#include "BLE.h"
#include "batmon.h"
#include "common.h"
#include "SD.h"
#include "BSP.h"
#include "jsmn.h"
#include "deviceConfig.h"
#include "impedance.h"
#include "accel.h"
#include "utils.h"
#include <stdarg.h>

#ifdef G_UNIT_TEST_ENABLE
#include "UT_main.h"
#endif //G_UNIT_TEST_ENABLE

/*
 *	Device = main GMP's logic.
 *
 * 	module is responsible for PBS interface (GApp via USB), start/stop of peripheral modules and modules and peripherals event handling
 *  it holds current states, MCU serial number, p-token (for login), versions (HW, FW) and guest_mode flag (to skip results saving)
 *
 * 	USB PBS interface is overriden with pbs_rx_message_handler() and message types are handled according to login state
 * 	peripheral modules events are accumulated into a united handler device_event_handler()
 *
 *
 * 	*note: serial protocol (currently!) bypasses "device", modules can be started / stopped by serial protocol regardless to GApp
 */

/********************************************************************************************************************/
/*	    					                    Local Definitions                                                     */

#define DEVICE_FLASH_PAGE_CHECKSUM			254
#define DEVICE_FLASH_PAGE_ECG_RESP_CONFIG 	255
#define DEVICE_MODE_TESTER					(device_mode_conf.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER)	// disgusting macro

#define DEVICE_MODE_PATCH					(device_mode_conf.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH)

#define DEVICE_MODE_HOLTER					(device_mode_conf.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY)
#define DEVICE_TEMPERATURE_DEFAULT_TIME		4000
#define DEVICE_PANIC_BUTTON_DEFAULT_TIME	90000 // 1.5 minutes * 60 * 1000 milli-seconds
#define DEVICE_ECG_FIRST_RUN_DEFAULT_TIME	30000
#define DEVICE_INFO_FILE_NAME				"CONFIG/device_info.txt"
#define DEVICE_FORMAT_SD_FILE_NAME			"reset.json"//"prepare_sd.gmp"
#define DEVICE_INFO_JSON_FILE_NAME			"device_info.json"//"CONFIG/device_info.json"
#define DEVICE_PATIENT_INFO_JSON_FILE_NAME	"patient_info.json"//"CONFIG/patient_info.json"
#define DEVICE_FW_FILE_NAME					"FW/GMP.bin"//"CONFIG/patient_info.json"
#define DEVICE_CONFIG_DIRECTORY_NAME        "CONFIG"
#define DEVICE_ECG_DIRECTORY_NAME        	"ECG"
#define DEVICE_LOG_FILE_NAME				"LOG/log.txt"
#define DEVICE_SN_LENGTH					13
#define DEVICE_SN_ADDRESS					0
#define DEVICE_TASK_QUEUE_SIZE				5
#define MINUTES_TO_TICKS(minutes)			minutes * 1000 * 60
#define SECONEDS_TO_TICKS(seconds)			seconds * 1000
#define BLE_HOLTER_TIMER_PERIOD_MIN			5
#define BLE_HOLTER_TIMER_PERIOD				MINUTES_TO_TICKS(BLE_HOLTER_TIMER_PERIOD_MIN) // 5 minutes
#define BLE_HOLTER_DOUBLE_CLICK_PERIOD		2
#define BLE_DOUBLE_CLICK_HOLTER_PERIOD		MINUTES_TO_TICKS(BLE_HOLTER_DOUBLE_CLICK_PERIOD) // 5 minutes
#define BLE_HOURLY_MINUTES_TO_SEND			10
#define BLE_PANIC_BUTTON_MINUTES_TO_SEND	3
#define BLE_HOURLY_MINUTES_PERIOD			60
#define BLE_HOURLY_TIMER_PERIOD				MINUTES_TO_TICKS(BLE_HOURLY_MINUTES_PERIOD) //1 hour = 60min*60second*1000ms  // 1 hour

#define DEVICE_SD_CARD_MIN_SD_CAPACITY		6000000
#define DEVICE_SD_CARD_MIN_FREE_SPACE		3000000
#define DEVICE_LEAD_ON_LED_MINUTES			10
#define DEVICE_LEAD_ON_LED_DRUATION			(DEVICE_LEAD_ON_LED_MINUTES * 60 * 1000)
#define DEVICE_ECG_BATTERY_WAIT_TIME_SEC	15
#define DEVICE_LOG_FILE_STR_MAX_LEN			300
#define DEVICE_MANUAL_EVENT_DRUATION		1000
#define DEVICE_DOUBLE_CLICK_DRUATION		4000



typedef enum {
	DEVICE_STATE_BOOTLOADER = -1,	// TODO: define in PBS
	DEVICE_STATE_IDLE = 0,
	DEVICE_STATE_RECORDING =1,
	DEVICE_STATE_ERROR = 2,
	DEVICE_STATE_CONTACT_DETECTION = 3,
	DEVICE_STATE_MAX
}DEVICE_STATE_TYPE;


typedef enum {
	DEVICE_CONNECTION_CONNECTED,
	DEVICE_CONNECTION_DISCONNECTED,
	DEVICE_CONNECTION_MAX
}DEVICE_CONNECTION_STATE;


typedef enum {
	DEVICE_BUTTON_STATE_IDLE,
	DEVICE_BUTTON_STATE_PRESSED,
	DEVICE_BUTTON_STATE_MAX
}DEVICE_BUTTON_STATE_ENUM;


typedef enum {
	DEVICE_TIMING_ACCURACY_FLAG_NONE = 0,
	DEVICE_TIMING_ACCURACY_FLAG_RESET = 1,
	DEVICE_TIMING_ACCURACY_FLAG_PRINT_STAT = 2
}DEVICE_TIMING_ACCURACY_FLAG_E;

typedef enum COMMAND_TYPE{
  COMMAND_ECG_START,
  COMMAND_STRESS,
  COMMAND_ECG_STOP,
  COMMAND_STOP,
  COMMAND_DEBUG_MODE,
  COMMAND_DEVICE_TEST_START,
  COMMAND_RESET,
  COMMAND_SLEEP,
  COMMAND_GET_STATS,
  COMMAND_ECG_BIT_START,
  COMMAND_ECG_BIT_STOP,
  COMMAND_BLE_RESET,
  COMMAND_ADS_COMMANDS,
  COMMAND_ECG_COMMANDS,
  COMMAND_HR_COMMANDS,
  COMMAND_TEMPERATURE_COMMANDS,
  COMMAND_TEST_WD,
  COMMAND_FLASH_UNIT_TEST,
  COMMAND_LOCK_OPTION_BYTES,
  COMMAND_PBS_COMMAND,
#ifdef G_UNIT_TEST_ENABLE
  COMMAND_UNIT_TEST,
#endif //G_UNIT_TEST_ENABLE
  COMMAND_LEDS,
  COMMAND_BLE_WRITE_CONFIG,
  COMMAND_BLE_WRITE_DEVICE_NAME,
  COMMAND_WRITE_BL,
  COMMAND_CREATE_FILE,
  COMMAND_READ_RTC,
  COMMAND_WRITE_RTC,
  COMMAND_WRITE_RTC_BCD,
  COMMAND_WRITE_DATE_RTC,
  COMMAND_AT_COMMAND,
  COMMAND_AT_MODE,
  COMMAND_BLE_TEST,
  COMMAND_RESET_TO_DEFAULTS,
  COMMAND_PBS_TEST,
  COMMAND_DIR,
  COMMAND_SD_WRITE,
  COMMAND_SD_READ,
  COMMAND_SD_DELETE,
  COMMAND_SD_FORMAT,
  COMMAND_SD_TEST,
  COMMAND_SD_CD,
  COMMAND_SD_MAKE_DIR,
  COMMAND_CLOCK_CHANGE,
  COMMAND_TIMING_ACCURICY_RESET,
  COMMAND_TIMING_ACCURICY_STAT,
  COMMAND_DEBUG_MODE_MENU,
  COMMAND_PARSE_JSON,
  COMMAND_PRINT_STACKS,
  COMMAND_ADS_INT_COUNT,
  COMMAND_CHECK_IMPEDANCE,
  COMMAND_CHECK_IMPEDANCE_INTERVAL,
  COMMAND_PBS_REMOTE_EVENT,
  COMMAND_ECG_TEMP_SET,
  COMMAND_BAT_MON_EN,
  COMMAND_LOG_SET,
  COMMAND_LOG_PRINT,
  COMMAND_SD_RENAME_FILE,
  COMMAND_WRITE_DEVICE_SN,
  COMMAND_ERROR_EVENT,
  COMMAND_SD_GET_INFO,
  COMMAND_START_BATTERY_TRAINING,
  COMMAND_START_BLE_CONFIG,
  COMMAND_MODE_SET,
  COMMAND_ECHO_SET,
  COMMAND_ACCEL_REG_WRITE,
  COMMAND_CLEAR_ECG_DATA_FROM_SD,
  COMMAND_PM_TEST,
  COMMAND_SET_CHECKSUM,
  COMMAND_ACCEL_REG_READ,
  COMMAND_SET_BIT_FAILURE,
  COMMAND_COMPARATOR_READ,
  COMMAND_SET_FILE_DUARTION_LENGTH,
  COMMAND_GPIO_SET,
  COMMAND_SPI_WRITE,
  COMMAND_SPI_READ,
//  COMMAND_SRAM_WRITE,
//  COMMAND_SRAM_READ,
//  COMMAND_SRAM_TEST,
  COMMAND_SET_BLE_ADV_NAME,
//  COMMAND_SLEEP,
  COMMAND_MAX
}COMMAND_TYPE;

typedef enum{
	TEMPERATURE_OK,
	TEMPERATURE_TOO_LOW,
	TEMPERATURE_TOO_HIGH
}TEMPERATURE_STATE_E;

/********************************************************************************************************************/
__weak RESULT_E deviceGetMainStack(UBaseType_t * ret_val){return RESULT_OK;}
/********************************************************************************************************************/
/*	    					                    Local Functions                                                     */
static void prvDeviceEventHandler(EVENT_SOURCE_TYPE source, device_event event);

void temperatureTimerCallback(TimerHandle_t xTimer);
void panicButtonTimerCallback(TimerHandle_t xTimer);
void shutdownTimertemperatureCallback(TimerHandle_t xTimer);
void bleHolterTimerCallback(void const *n);
void ecgFirstRunTimerCallback(void const *n);
//void sendBleEveryHourTimerCallback(void const *n);
void ledTimerCallback(void const *n);
void manualEventTimerCallback(void const *n);
void doubleClickTimerCallback(void const *n);
void deviceTask(void const * argument);
void deviceHourlyTask(void const * argument);

void readEgcFromSdToBleHourlyCallback(void const *n); // read Egc data from sdcard every hour last x minutes
static RESULT_E prvDeviceRxMessageHandlerLoggedIn(uint16_t type, PBS_RX_PAYLOAD_STRUCT payload);
static RESULT_E prvDeviceMcuIdGet(void);

static RESULT_E prvDevicePrintInfo(void);
static DEVICE_RESULT_ENUM prvDeviceStopMeasurement(PBS_RX_PAYLOAD_STRUCT payload);
//static DEVICE_RESULT_ENUM prvDeviceErrorEvent(ENUM_PBS_ERRORS error_event);
static RESULT_E prvDeviceSetDeviceStates(DEVICE_CONNECTION_STATE new_conn_state, DEVICE_STATE_TYPE new_state);
static RESULT_E deviceCommandHandler(const command_desc *commDesc, command_param * param);
//static DEVICE_RESULT_ENUM prvDeviceWriteBootloader(void);
static RESULT_E prvDevicePowerOnLeds(void);
static RESULT_E prvDeviceSetState(DEVICE_STATE_TYPE new_state);
static DEVICE_RESULT_ENUM prvDevicePrintStacks();
static RESULT_E prvDeviceSnSet(uint8_t* data);
static DEVICE_RESULT_ENUM prvDeviceIdGet(uint8_t *device_id_num);
static DEVICE_RESULT_ENUM prvDeviceSetBleName(char* ble_name);

// REVIEW: PBS

static DEVICE_RESULT_ENUM prvDeviceEcgArrythmiasCollectionReset();
static DEVICE_RESULT_ENUM prvDeviceEcgStart();


static DEVICE_RESULT_ENUM prvDeviceGetDeviceInfo();
static RESULT_E prvDeviceGetInfo(PBS_RX_PAYLOAD_STRUCT payload);
static RESULT_E prvDeviceGetStatus();
static DEVICE_RESULT_ENUM prvDeviceClearSdEcgData();

static RESULT_E prvDeviceControl(PBS_RX_PAYLOAD_STRUCT payload);
static RESULT_E prvDeviceConfigSet(PBS_RX_PAYLOAD_STRUCT payload);
static RESULT_E prvDeviceEcgSetRealTime(PBS_RX_PAYLOAD_STRUCT payload);
static RESULT_E prvDeviceEcgProgressIndicationResponse(PBS_RX_PAYLOAD_STRUCT payload);
static RESULT_E prvDeviceEcgRemoteEventCmd(PBS_RX_PAYLOAD_STRUCT payload);
static RESULT_E prvDeviceEcgRemoteEventHolterCmd(PBS_RX_PAYLOAD_STRUCT payload);
static DEVICE_RESULT_ENUM prvDeviceEcgEventIndicationResponse(PBS_RX_PAYLOAD_STRUCT payload);
static DEVICE_RESULT_ENUM prvDeviceEcgContactInfoGetCmd(PBS_RX_PAYLOAD_STRUCT payload);
static DEVICE_RESULT_ENUM prvDeviceEcgConfigSet(PBS_RX_PAYLOAD_STRUCT payload);
static DEVICE_RESULT_ENUM prvDeviceEcgConfigGet();
static DEVICE_RESULT_ENUM prvDeviceLoadEcgConfig();
static DEVICE_RESULT_ENUM prvDeviceEcgConfigResetToDefaults();
static DEVICE_RESULT_ENUM prvDeviceConfiguration(void);
static DEVICE_RESULT_ENUM prvDeviceEcgAlgorithmConfigGet();
static DEVICE_RESULT_ENUM prvDeviceEcgAlgorithmConfigSet(PBS_RX_PAYLOAD_STRUCT payload);
static DEVICE_RESULT_ENUM prvDeviceStartBatteryTraining(void);
static DEVICE_RESULT_ENUM prvDevicePrintTime(void);
static DEVICE_RESULT_ENUM prvDeviceCheckSdFreeSpace(uint32_t min_free_space);
static DEVICE_RESULT_ENUM prvDeviceSetChecksum(void);
//static DEVICE_RESULT_ENUM prvDeviceFlashTest(void);
DEVICE_RESULT_ENUM	prvDevicePrintMode(DEVICE_CONFIG_STRUCT * mode);
static DEVICE_RESULT_ENUM deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ENUM err);
static DEVICE_RESULT_ENUM prvDeviceAccelConfigSet(PBS_RX_PAYLOAD_STRUCT payload);
static DEVICE_RESULT_ENUM prvDeviceLogSd(char* format, ...);
static DEVICE_RESULT_ENUM prvDeviceTimersInit(void);
static DEVICE_RESULT_ENUM prvDeviceGetRespLastFileName(void);
/********************************************************************************************************************/

/********************************************************************************************************************/
/*					                    	    Local Parameters		                            				*/
static DEVICE_STATE_TYPE                    				device_state = DEVICE_STATE_MAX;//DEVICE_STATE_CONTACT_DETECTION;	// TODO: set in init phase
static DEVICE_CONNECTION_STATE								device_conn_state = DEVICE_CONNECTION_DISCONNECTED;
static DEVICE_BUTTON_STATE_ENUM								device_button;
static uint8_t                              				serial_number[STM32_UUID_LENGTH];
static uint8_t                              				hw_ver[] = {0, 0, 0, 0};
#pragma location = FLASH_VERSION_ADDRESS//".VER"
const uint8_t                              					fw_ver[] = {FW_MAJOR_VERSION, FW_MINOR_VERSION, FW_PATCH_VERSION, FW_BUILD_VERSION};
static uint8_t                              				bl_fw_ver[] = {BL_FW_MAJOR_VERSION, BL_FW_MINOR_VERSION, BL_FW_PATCH_VERSION, BL_FW_BUILD_VERSION};
static uint8_t                              				ble_mac[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
//static uint8_t                            				  pbs_ver[] = {PBS_MAJOR_VERSION, PBS_MINOR_VERSION, PBS_PATCH_VERSION, PBS_BUILD_NUMBER_VERSION};
static TimerHandle_t                        				temperature_timer;
static TimerHandle_t                        				panic_button_timer;
//static osTimerId 											send_ble_every_hour_timer;
static osTimerId 											ble_holter_timer;
static osTimerId 											ecg_first_run_timer;
static osTimerId 											led_timer;
static osTimerId 											manual_event_timer;
static osTimerId 											double_click_timer;
static osTimerId 											read_egc_from_sd_to_ble_hourly_timer;
static bool 												doubleClickDone = false;

static float32_t											current_temperature = 0;
static TEMPERATURE_STATE_E									temp_state = TEMPERATURE_OK;
osThreadId 											deviceTaskHandle;
osThreadId                                                                                      deviceHourlyTaskHandle;
osMessageQId                                                                                      deviceQueueHourlyHandle;
osMessageQId 											deviceQueueHandle;
static LEDS_BATTERY_E										leds_state_battery = DEVICE_LEDS_BATTERY_IDLE;
#pragma location = ".RAM2"
static ECG_MEAS_PROGRESS_SIGNAL_UNION 						signal_ecg_1, signal_ecg_2, signal_resp;
static ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT 		ecg_rt_ind;
static ECG_MEAS_EVENT_INDICATION_STRUCT						ecg_event_ind;
static ECG_RESPIRATION_REAL_TIME_MEASUREMENT_STRUCT 		ecg_real_time_config = {.real_time_enable = ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_DISABLE, .reset_buffer = ECG_MEASUREMENT_REALTIME_ENABLE_RESET_BUFFER_DO_NOT_RESET};
static uint32_t 											device_time_last_sent_ecg_contact_ind;
static ECG_ALGO_ARRYTHMIA_EVENT_STRUCT						arrythmias_curr;//, arrythmias_acc;
static ECG_RESPIRATION_ALGO_RESULTS_STRUCT					respiration_algo_curr;//, respiration_algo_acc;
//static uint32_t												ecg_measure_time;
static PBS_DEVICE_INFO_STRUCT								device_info;
static PBS_DEVICE_CONFIG_OBJECT								device_config;
static DEVICE_CONFIG_STRUCT 								device_mode_conf = {.device_mode = ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY, .pace_maker_enable = ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_OFF};
static uint32_t												timing_accuracy_ostime;
static BSP_TIME_DATE_STRUCT									timing_accuracy_rtc_date_time;
static uint32_t												timing_accuracy_ecg_index;
static DEVICE_TIMING_ACCURACY_FLAG_E						timing_accuracy_flag = DEVICE_TIMING_ACCURACY_FLAG_RESET;
static PBS_DEVICE_STATUS_BIT_ERROR_ENUM						device_bit = PBS_DEVICE_STATUS_BIT_NO_ERROR;
static 	osMutexId 											deviceSyncSdLedMutex;

static ECG_LEAD_STATUS_ENUM									curr_lead_status = ECG_LEAD_STATUS_OFF;
static uint8_t												device_id[DEVICE_SN_LENGTH + 1] = {0};
static IMPEDANCE_EVENT_STRUCT								impedance_results = {.event_type = IMPEDANCE_EVENT_MAX, .measurements.ra_kohm = DISCONNECTED_VAL,
																	.measurements.la_kohm = DISCONNECTED_VAL, .measurements.ll_kohm = DISCONNECTED_VAL};

static uint16_t												hr_avg_acc, hr_min, hr_max, hr_avg_cnt;
static uint8_t												device_1st_ecg_run_flag = TRUE;
static uint8_t												device_double_click_counter = 0;

EventGroupHandle_t 											panicButton;

//static uint8_t 												device_current_resp_file_num = 1;
//static uint8_t 												device_current_resp_file[15] = {0};

static const command_desc device_cmd_list[] = {
{COMMAND_ECG_START 					,"s"    		,"Activate ECG"         			,"ECG activated"    			,COMMAND_PARAM_NONE},
{COMMAND_ECG_STOP  				   	,"t"    		,"Stop ECG/ECG BIT"     			,"ECG stopped"      			,COMMAND_PARAM_NONE},
{COMMAND_STOP      					,"x"   			,"Stop all"             			,"Stopped"          			,COMMAND_PARAM_NONE},
{COMMAND_RESET   					, "rst"   		,"reset CPU"             			,"Reset CPU"        			,COMMAND_PARAM_NONE},
{COMMAND_GET_STATS					, "info"   		,"Get system information" 			,""      		 				,COMMAND_PARAM_NONE},
{COMMAND_BLE_RESET 					,"rst_ble"		,"BLE Reset"   						, "BLE Reset"    				,COMMAND_PARAM_NONE},
{COMMAND_ECG_COMMANDS 				,"ecg"     		,"ecg test commands"   				,"ECG TEST"    				,      COMMAND_PARAM_INT},
{COMMAND_TEMPERATURE_COMMANDS 		,"temp"     	,"CPU Temperature read"   			, ""    						,COMMAND_PARAM_NONE},
{COMMAND_TEST_WD 					,"wd"     		,"Watch Dog test"   				, "WD Test" 					,COMMAND_PARAM_NONE},
{COMMAND_FLASH_UNIT_TEST			,"ft" 			,"Flash Test"   					, "" 							,COMMAND_PARAM_NONE},
{COMMAND_LOCK_OPTION_BYTES			,"lock"     	,"Lock Option Bytes"				, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_PBS_COMMAND				,"pbs"     		,"Send PBS command"					, "						"    	,COMMAND_PARAM_STR},
{COMMAND_CREATE_FILE				,"f_name" 		,"Create File Name"					, "						"    	,COMMAND_PARAM_STR},
{COMMAND_READ_RTC					,"rrtc" 		,"Read RTC"							, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_WRITE_RTC					,"wrtc" 		,"Write RTC"						, "						"    	,COMMAND_PARAM_STR_2},
{COMMAND_WRITE_RTC_BCD				,"wrtc_bcd"		,"Write RTC from bcd format"		, "						"    	,COMMAND_PARAM_STR},
{COMMAND_WRITE_DATE_RTC				,"wdrtc" 		,"Write Date RTC"					, "						"    	,COMMAND_PARAM_STR},
{COMMAND_AT_COMMAND					,"at"     		,"Send AT command to BLE"			, "						"    	,COMMAND_PARAM_STR_2},
{COMMAND_AT_MODE					,"at_mode"     	,"AT command mode 0 - OFF, 1 - ON"	, "						"    	,COMMAND_PARAM_INT},
{COMMAND_BLE_TEST					,"ble"     		,"BLE test"							, "						"    	,COMMAND_PARAM_INT},
{COMMAND_RESET_TO_DEFAULTS			,"defaults"		,"reset to defaults"				, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_PBS_TEST					,"tx"			,"pbs test: 0 clear, 1-dequeue, 2-ack + fc, 3-send RT", "		"    	,COMMAND_PARAM_STR_2},
{COMMAND_DIR						,"dir"			,"Read all files in root directory"	, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_SD_WRITE					,"sdw"     		,"Write sd file with text"			, "						"    	,COMMAND_PARAM_STR_2},
{COMMAND_SD_READ					,"sdr"     		,"Read sd file"						, "						"    	,COMMAND_PARAM_STR},
{COMMAND_SD_DELETE					,"sdd"     		,"Delete sd file"					, "						"    	,COMMAND_PARAM_STR},
{COMMAND_SD_TEST					,"sdtest"		,"Test SD card"						, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_SD_CD						,"cd"			,"Change Directory"					, "						"    	,COMMAND_PARAM_STR},
{COMMAND_SD_MAKE_DIR				,"md"			,"Make Directory"					, "						"    	,COMMAND_PARAM_STR},
{COMMAND_CLOCK_CHANGE				,"clock"		,"change clock 0-40M, 1-5M [hz]"	, "						"    	,COMMAND_PARAM_INT},
{COMMAND_TIMING_ACCURICY_RESET		,"tareset"		,"timing accuracy reset vars"		, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_TIMING_ACCURICY_STAT		,"tastat"		,"timing accuracy show stat"		, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_PARSE_JSON					,"json"			,"Read JSON file"					, "						"    	,COMMAND_PARAM_STR},
{COMMAND_PRINT_STACKS				,"stack"		,"print stacks status"				, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_ADS_INT_COUNT				,"ads"			,"print ads int count"				, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_CHECK_IMPEDANCE			,"imp"			,"run impedance test"				, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_CHECK_IMPEDANCE_INTERVAL	,"imp_int"		,"set impedance interval"			, "						"    	,COMMAND_PARAM_INT},
{COMMAND_PBS_REMOTE_EVENT			,"txr"			,"send remote event"				, "						"    	,COMMAND_PARAM_INT},
{COMMAND_ECG_TEMP_SET				,"ecg_t"		,"set ecg temp 0-ok, 1-out of range", "						"    	,COMMAND_PARAM_INT},
{COMMAND_BAT_MON_EN					,"bat_en"		,"activate battery reads 0-no, 1-yes", "					"    	,COMMAND_PARAM_INT},
{COMMAND_LOG_SET					,"log"			,"log MODULE LEVEL (or ? for help)"	, "						"    	,COMMAND_PARAM_STR_2},
{COMMAND_LOG_PRINT					,"log_test"		,"test log debug"					, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_SD_RENAME_FILE				,"rename"		,"rename file"						, "						"    	,COMMAND_PARAM_STR_2},
{COMMAND_WRITE_DEVICE_SN			,"device_sn" 	,"Set device serial number"			, "						"    	,COMMAND_PARAM_STR},
{COMMAND_ERROR_EVENT				,"error"		,"error event"						, "						"    	,COMMAND_PARAM_INT},
{COMMAND_SD_GET_INFO				,"sd_info"		,"Get SD card info"					, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_START_BATTERY_TRAINING		,"bat_train"	,"Start battery training"			, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_START_BLE_CONFIG			,"ble_config"	,"Start BLE configuration"			, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_ACCEL_REG_WRITE			,"accel_set"	,"Write accelerometer registry"		, "						"    	,COMMAND_PARAM_STR_2},
{COMMAND_MODE_SET					,"mode_set"		,"<MODE> (? for help)"				, "						"    	,COMMAND_PARAM_STR_2},
{COMMAND_ECHO_SET					,"echo"			,"echo 1=on / 0=off"				, "						"    	,COMMAND_PARAM_INT},
{COMMAND_CLEAR_ECG_DATA_FROM_SD		,"ecg_del"		,"delete ecg files"					, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_PM_TEST					,"pm_start"		,"Pace maker start 1 / stop 0"		, "						"    	,COMMAND_PARAM_INT},
{COMMAND_SET_CHECKSUM				,"checksum"		,"write checksum in flash"			, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_ACCEL_REG_READ				,"accel_get"	,"Read accelerometer registry"		, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_SET_BIT_FAILURE			,"bit"			,"Set BIT failure"					, "						"    	,COMMAND_PARAM_INT},
{COMMAND_COMPARATOR_READ			,"comp"			,"Read comparator voltage"			, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_SLEEP						,"sleep"		,"Set the device in sleep mode"		, "						"    	,COMMAND_PARAM_INT},
{COMMAND_SET_FILE_DUARTION_LENGTH	,"file_len"		,"Set file duration length in hours", "						"    	,COMMAND_PARAM_INT},
{COMMAND_GPIO_SET					,"gpio"			,"Set gpio output mode"				, "						"    	,COMMAND_PARAM_STR_2},
{COMMAND_SPI_WRITE					,"spiw"			,"write data via spi"				, "						"    	,COMMAND_PARAM_STR},
{COMMAND_SPI_READ					,"spir"			,"read data via spi"				, "						"    	,COMMAND_PARAM_STR_2},
//{COMMAND_SRAM_WRITE					,"sramw"		,"write data to SRAM"				, "						"    	,COMMAND_PARAM_NONE},
//{COMMAND_SRAM_READ					,"sramr"		,"read data from SRAM"				, "						"    	,COMMAND_PARAM_NONE},
//{COMMAND_SRAM_TEST					,"sram_test"	,"Test the SRAM"					, "						"    	,COMMAND_PARAM_NONE},
{COMMAND_SET_BLE_ADV_NAME			,"set_ble_name"	,"Set BLE advertisement name"		, "						"    	,COMMAND_PARAM_STR},
//last command
{COMMAND_MAX       					,""      		,"", 								"Unknown command or illegal parameter"  ,       COMMAND_PARAM_NONE},
};

#ifdef G_UNIT_TEST_ENABLE
static BOOL is_unit_test = FALSE;
#endif //G_UNIT_TEST_ENABLE

/********************************************************************************************************************/

/********************************************************************************************************************/
/*					                        	External Parameters			                                        */

/********************************************************************************************************************/

//Override WEAK functions of each module
void ecg_event(ecg_event_s * event)
{
	device_event e;
	e.ecg = event;
	prvDeviceEventHandler(EVENT_SOURCE_ECG, e);
}

LEDS_RESULT ledsSync(void)
{//Checks if Mutex is available
	if (osSemaphoreGetCount(deviceSyncSdLedMutex))
		return LEDS_RESULT_OK;
	else
		return LEDS_RESULT_ERROR_SYNC;

}

SD_RESULT_E sdSync(SD_MUTEX_COMMAND_E command)
{
	osStatus 				ret = osErrorOS;

	bsp_led(BSP_LED_RED, BSP_LED_OFF);
	bsp_led(BSP_LED_BLUE, BSP_LED_OFF);


	if (command == SD_MUTEX_COMMAND_WAIT)
		ret = osMutexWait(deviceSyncSdLedMutex, osWaitForever); //TODO: Constant
	else if (command == SD_MUTEX_COMMAND_RELEASE)
		ret = osMutexRelease(deviceSyncSdLedMutex);

	return (ret == osOK ? SD_RESULT_OK:SD_RESULT_ERROR);
}

SD_RESULT_E sdGetTime(uint32_t* time)
{
	BSP_TIME_STRUCT 	rtc_time;
	BSP_DATE_STRUCT 	rtc_date;
	SD_TIME_E			sd_time;

	bspRtcTimeGet(&rtc_time, &rtc_date);

	sd_time.bitmap.second = rtc_time.seconds / 2;
	sd_time.bitmap.minute = rtc_time.minutes;
	sd_time.bitmap.hour = rtc_time.hours;
	sd_time.bitmap.day = rtc_date.date;
	sd_time.bitmap.month = rtc_date.month;
	sd_time.bitmap.year = rtc_date.year + 20;

	*time = sd_time.time;

	return SD_RESULT_OK;
}

void bat_mon_event(bat_mon_event_s * event)
{


	switch (event->event_type)
	{
//		case BAT_MON_EVENT_LOW_BATTERY:
//		{
//			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_LOW_BATTERY, 0);
//		}
//		break;

		case BAT_MON_EVENT_CRIT_LOW_BATTERY:
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_CRIT_LOW_BATTERY, 0);
		}
		break;

		case BAT_MON_EVENT_IDLE_BATTERY:
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_IDLE_BATTERY, 0);
		}
		break;


		default:
			LOG("bat_mon_event wrong event type: %d\r\n", event->event_type);
		break;

	}


}

void bleCopyMac(uint8_t* mac)
{
	memcpy(ble_mac, mac, sizeof(ble_mac));
}
void ble_event(BLE_EVENT_E event)
{
//	if (device_mode_conf.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY)
//		return;
	switch (event)
	{
		case BLE_EVENT_GMP_CONNECTED:
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_GMP_CONNECTED, 1000);
		}
		break;

		case BLE_EVENT_GMP_DISCONNECTED:
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_GMP_DISCONNECTED, 1000);
		}
		break;

		case BLE_EVENT_BLE_BIT_FAILED:
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_BLE_BIT_FAILED, 1000);
		}
		break;
	}
}

void bspButtonEvent(BSP_BUTTON_EVENT_E button_status)
{
	switch (button_status)
	{
		case BSP_BUTTON_EVENT_PRESSED:
		{
			osTimerStart(manual_event_timer, DEVICE_MANUAL_EVENT_DRUATION);
			if (device_double_click_counter == 0)
				osTimerStart(double_click_timer, DEVICE_DOUBLE_CLICK_DRUATION);
		}
		break;

		case BSP_BUTTON_EVENT_RELEASED:
		{
			osTimerStop(manual_event_timer);
			if (device_double_click_counter == 0)
				device_double_click_counter++;
			else
			{
				osTimerStop(double_click_timer);
				device_double_click_counter = 0;
				//Trigger double click event
				osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_DOUBLE_CLICK, 1000);
			}
		}
		break;

		case BSP_BUTTON_EVENT_BOOKMARK:
		{
//		osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_BUTTON_PRESSED, 1000);
		}
		break;
	}
//	if (button_status)
//		osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_BUTTON_PRESSED, 1000);
//	else
//		osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_BUTTON_RELEASED, 1000);
}

DEVICE_RESULT_ENUM prvDeviceGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (deviceTaskHandle == NULL) || (ret_val == NULL) )
		return DEVICE_RESULT_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark( deviceTaskHandle );
	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDevicePrintStacks()
{
	UBaseType_t high_water_mark;
	uint32_t total_stack = 0, total_stack_use = 0;
	LOG("\r\nStacks Check\tOrig\tLeft");
	if (deviceGetMainStack(&high_water_mark) == RESULT_OK)
	{
		LOG("\r\nMAIN\t\t%d\t%d", 4 * MAIN_STACK_SIZE, 4 * high_water_mark);
		total_stack += MAIN_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (ecgGetHighWaterMark(&high_water_mark) == ECG_RESULT_OK)
	{
		LOG("\r\nECG\t\t%d\t%d", 4 * ECG_STACK_SIZE, 4 * high_water_mark);
		total_stack += ECG_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (bleGetHighWaterMark(&high_water_mark) == BLE_RESULT_OK)
	{
		LOG("\r\nBLE\t\t%d\t%d", 4 * BLE_STACK_SIZE, 4 * high_water_mark);
		total_stack += BLE_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (batMonGetHighWaterMark(&high_water_mark) == BAT_MON_RESULT_OK)
	{
		LOG("\r\nBatmon\t\t%d\t%d", 4 * BAT_MON_STACK_SIZE, 4 * high_water_mark);
		total_stack += BAT_MON_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (prvDeviceGetHighWaterMark(&high_water_mark) == DEVICE_RESULT_OK)
	{
		LOG("\r\nDevice\t\t%d\t%d", 4 * DEVICE_STACK_SIZE, 4 * high_water_mark);
		total_stack += DEVICE_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (serialProtocolGetHighWaterMark(&high_water_mark) == SERIAL_PROTOCOL_RESULT_OK)
	{
		LOG("\r\nSerial\t\t%d\t%d", 4 * SERIAL_PROTOCOL_STACK_SIZE, 4 * high_water_mark);
		total_stack += SERIAL_PROTOCOL_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (pbsGetHighWaterMark(&high_water_mark) == PBS_RESULT_OK)
	{
		LOG("\r\nPBS\t\t%d\t%d", 4 * PBS_STACK_SIZE, 4 * high_water_mark);
		total_stack += PBS_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (pbsTxGetHighWaterMark(&high_water_mark) == PBS_RESULT_OK)
	{
		LOG("\r\nPBS_Tx\t\t%d\t%d", 4 * PBS_TX_STACK_SIZE, 4 * high_water_mark);
		total_stack += PBS_TX_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (sdGetHighWaterMark(&high_water_mark) == SD_RESULT_OK)
	{
		LOG("\r\nSD\t\t%d\t%d", 4 * SD_STACK_SIZE, 4 * high_water_mark);
		total_stack += SD_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (ecgMennenGetHighWaterMark(&high_water_mark) == ECG_RESULT_OK)
	{
		LOG("\r\nMennen\t\t%d\t%d", 4 * MENNEN_ALGO_STACK_SIZE, 4 * high_water_mark);
		total_stack += MENNEN_ALGO_STACK_SIZE;
		total_stack_use += high_water_mark;
	}
	if (impedanceGetHighWaterMark(&high_water_mark) == IMPEDANCE_RESULT_OK)
	{
		LOG("\r\nImpedance\t%d\t%d", 4 * IMPEDANCE_STACK_SIZE, 4 * high_water_mark);
		total_stack += IMPEDANCE_STACK_SIZE;
		total_stack_use += high_water_mark;
	}

	if (accelGetHighWaterMark(&high_water_mark) == ACCEL_RESULT_OK)
	{
		LOG("\r\nAccelerometer\t%d\t%d", 4 * ACCEL_STACK_SIZE, 4 * high_water_mark);
		total_stack += ACCEL_STACK_SIZE;
		total_stack_use += high_water_mark;
	}

	LOG("\r\n-----------------------------");
	LOG("\r\nTotal\t\t%d\t%d", 4 * total_stack, 4 * total_stack_use);
	return DEVICE_RESULT_OK;
}


static DEVICE_RESULT_ENUM prvDeviceEcgStart()
{
	uint8_t		i = 0;
	memset (&arrythmias_curr, 0, sizeof(arrythmias_curr));
	memset (&respiration_algo_curr, 0, sizeof(respiration_algo_curr));

	while(device_state != DEVICE_STATE_IDLE)
	{//wait for battery training
		osDelay(1000);
		++i;

		if (i > DEVICE_ECG_BATTERY_WAIT_TIME_SEC)
			//return DEVICE_RESULT_ECG_ERROR;
			break;
	}

	prvDeviceEcgArrythmiasCollectionReset();

	device_time_last_sent_ecg_contact_ind = 0;

	ECG_RESULT result = ecgStart();
	LOG("ECG start result = %d\r\n", result);

	if (result != ECG_RESULT_OK)
	{
		if (device_mode_conf.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY) // TODO: was ifdef holter only.. is needed?
		{
			prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_ECG");
			deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ECG);
		}
		return DEVICE_RESULT_ECG_ERROR;
	}

	return DEVICE_RESULT_OK;

}
//static uint8_t buff_read[1000] = {0};
//static uint16_t buff_read_length = 0;
/*
			uint8_t sram_data_write[] = {0x02,  //Write data
									0x00, 0x12, 0x34, //Address
			0x99, 0x20, 0x79, 0x6f, 0x00, 0x08, 0x15, 0x70, 0x00, 0x08, 0x19, 0x70, 0x08, 0x39, 0x70, 0xab,
			0x00, 0x08, 0x1d, 0x70, 0x00, 0x08, 0x21, 0x70, 0x00, 0x08, 0x25, 0x70, 0x00, 0x08, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xcd, 0x64,
			0x00, 0x08, 0x29, 0x70, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x69, 0x64, 0x00, 0x08, 0x5f, 0x6a,
			0x00, 0x08, 0x2d, 0x70, 0x00, 0x08, 0x31, 0x70, 0x00, 0x08, 0x35, 0x70, 0x00, 0x08, 0x39, 0x70,
			0x00, 0x08, 0x3d, 0x70, 0x00, 0x08, 0x41, 0x70, 0x00, 0x08, 0xb5, 0x6a, 0x00, 0x08, 0xbb, 0x6a,
			0x00, 0x08, 0xcb, 0x6a, 0x00, 0x08, 0xdb, 0x6a, 0x00, 0x08, 0x45, 0x70, 0x00, 0x08, 0x49, 0x70,
			0x00, 0x08, 0x4d, 0x70, 0x00, 0x08, 0x51, 0x70, 0x00, 0x08, 0x57, 0x6a, 0x00, 0x08, 0x6d, 0x6a,
			0x00, 0x08, 0x85, 0x6a, 0x00, 0x08, 0x7d, 0x6a, 0x00, 0x08, 0x55, 0x70, 0x00, 0x08, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xa5, 0x6a,
			0x00, 0x08, 0x59, 0x70, 0x00, 0x08, 0x5d, 0x70, 0x00, 0x08, 0x61, 0x70, 0x00, 0x08, 0x65, 0x70,
			0x00, 0x08, 0x69, 0x70, 0x00, 0x08, 0x6d, 0x70, 0x00, 0x08, 0x71, 0x70, 0x00, 0x08, 0x75, 0x70,
			0x00, 0x08, 0x79, 0x70, 0x00, 0x08, 0x7d, 0x70, 0x00, 0x08, 0x81, 0x70, 0x00, 0x08, 0x85, 0x70,
			0x00, 0x08, 0x89, 0x70, 0x00, 0x08, 0x8d, 0x6a, 0x00, 0x08, 0x95, 0x6a, 0x00, 0x08, 0x00, 0x00,
			0x00, 0x00, 0x9d, 0x6a, 0x00, 0x08, 0x8d, 0x70, 0x00, 0x08, 0x91, 0x70, 0x00, 0x08, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x95, 0x70,
			0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x4f, 0x6a, 0x00, 0x08, 0xa5, 0x70, 0x00, 0x08, 0xeb, 0x6a, 0x00, 0x08, 0xa9, 0x70,
			0x00, 0x08, 0xad, 0x70, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x08, 0xf3, 0x6a, 0x00, 0x08, 0xb5, 0x70, 0x00, 0x08, 0x75, 0x6a, 0x00, 0x08, 0xb9, 0x70};
*/
static RESULT_E deviceCommandHandler(const command_desc *commDesc, command_param * param)
{
	RESULT_E result = RESULT_OK;
	float32_t dummy;

	switch (commDesc->commandType)
	{
	case COMMAND_ECG_START:
		{
			ECG_RESP_CONFIG_OBJECT_STRUCT ecg_params_struct;

			ecgStop();

			ecgSetDefaultParams(ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY, ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_OFF);
			ecgGetConfig(&ecg_params_struct);
			ecg_real_time_config.real_time_enable = ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_ENABLE;

//			ecg_params_struct.test_time = ECG_PARAMS_TEST_TIME_UNLIMITED;	//30;
//			ecg_params_struct.ecg_output.peaks_output = ECG_PARAM_PEAKS_OUTPUT_ON;
			ecg_params_struct.ecg_output.output_frequency = ECG_PARAMS_OUTPUT_FREQ_500;
			ecg_params_struct.ecg_output.bandpass_filter = ECG_PARAMS_BANDPASS_FILTER_OFF;

			ecg_params_struct.respiration_output.output_frequency = ECG_PARAMS_RESPIRATION_OUTPUT_500HZ_COMPRESSED;
			ecg_params_struct.respiration_output.bandpass_filter = ECG_PARAMS_RESPIRATION_BANDPASS_FILTER_0_1__4HZ;
//			ecg_params_struct.max_hr = ECG_PARAMS_HR_UNLIMITED;
//			ecg_params_struct.min_hr = ECG_PARAMS_HR_UNLIMITED;

//			ecg_params_struct.ads.gain = ECG_PARAMS_GAIN_1;
			ecgCalcLsbWeight(&ecg_params_struct, &ecg_params_struct.ecg_output.lsb_weight);
//
//			ecg_params_struct.ads.respiration_gain = ECG_PARAMS_GAIN_12;
			LOG("\r\nCOMMAND_ECG_START, gain = %d, weight = %f\r\n", ecg_params_struct.ads.gain, ecg_params_struct.ecg_output.lsb_weight);
			ecgSetConfig(&ecg_params_struct);

			if (prvDeviceEcgStart() != DEVICE_RESULT_OK)
				result = RESULT_ERROR;


//			device_state = DEVICE_STATE_IDLE;	// force idle (connected)
//	        PBS_RX_PAYLOAD_STRUCT payload;
//	        payload.tlv_list = NULL;
//	        device_ecg_meas_start(payload);

		}
		break;
	case COMMAND_STRESS:
	{
//        PBS_RX_PAYLOAD_STRUCT payload;
//        payload.tlv_list = NULL;
//        device_stress_meas_start(payload);
	}
	break;
	case COMMAND_ECG_STOP:
	{
		PBS_RX_PAYLOAD_STRUCT payload;
        payload.tlv_list = NULL;
        prvDeviceStopMeasurement(payload);
		//		result = ecgStop();
		//HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
		////bsp_set_irq_mode(BSP_IRQ_ADS, FALSE);
		//ecgPbsStop();
	}
		break;
	case COMMAND_STOP:
		result |= ecgStop();
		HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
		//ecgPbsStop();
		break;
	case COMMAND_DEBUG_MODE:
		setDebugLevel((DEBUG_LEVEL)param->i);
		break;
	case COMMAND_DEVICE_TEST_START:
		//bsp_enable_button(TRUE);
		//device_test();
		//crypro_test();
        	//crypto_test();
		break;
	case COMMAND_RESET:
//		if (HAL_IWDG_GetState(&IwdgHandle) == HAL_IWDG_STATE_RESET)
			NVIC_SystemReset();
//		else
//			watchdog_reset_flag = 1;
		break;
//	case COMMAND_SLEEP:
//		LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_1, "\r\n\r\nEnter Sleep Mode\r\n");
//		result = bsp_enter_mode(BSP_SLEEP_MODE);
//		break;
	case COMMAND_GET_STATS:
		prvDevicePrintInfo();
		prvDevicePrintMode(&device_mode_conf);
//		device_print_status();
		break;
	case COMMAND_ECG_BIT_START:
		//adsInit(/*NULL, */TRUE);
		//result = ecgStart(TRUE);
		//HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
		//bsp_set_irq_mode(BSP_IRQ_ADS, TRUE);
		break;
	case COMMAND_BLE_RESET:
		bleReset();
		break;
	case COMMAND_ECG_COMMANDS:
        ecgDebugStart(param->i);
		break;
	case COMMAND_HR_COMMANDS:
//        hrDebugStart(param->i);
		break;
	case COMMAND_TEMPERATURE_COMMANDS:
		bspReadTemp(&dummy);
		LOG("Current Temperature: %.02f\r\n", dummy);
		break;
	case COMMAND_TEST_WD:
		while(1);
		break;
	case COMMAND_FLASH_UNIT_TEST:
//		flashTest();
		break;
	case COMMAND_LOCK_OPTION_BYTES:
		if (bspLockOptionBytes() == BSP_RESULT_OK)
			LOG("Read Protection Enabled\r\n");
		else
			LOG("Read Protection Failed\r\n");
		break;

	case COMMAND_PBS_COMMAND:
		  pbsSendMsgToQueue((uint8_t*)param->str);
		break;
#ifdef G_UNIT_TEST_ENABLE
	case COMMAND_UNIT_TEST:
		is_unit_test = TRUE;
		bsp_set_unit_test(TRUE);
		result = 10;	// TODO: Switch back to device serial commands, then Ack's it table
		unitTestSetSeriaCommands();
		break;
#endif //G_UNIT_TEST_ENABLE
	case COMMAND_LEDS:
		{
			static uint8_t led_mode = TRUE;
			if (led_mode)
				ledsDeviceControl((LEDS_BITMAP_E)param->i, LEDS_MODE_ON);
			else
				ledsDeviceControl((LEDS_BITMAP_E)param->i, LEDS_MODE_OFF);
			led_mode ^= 1;
		}
		break;
	case COMMAND_BLE_WRITE_CONFIG:
//		bleStartWriteConfig();
		break;
	case COMMAND_BLE_WRITE_DEVICE_NAME:
//		if (bleDeviceNameSet((char*)param->str) == BLE_RESULT_OK)
//			bleStartWriteConfig();
		break;
	case COMMAND_WRITE_BL:
//		prvDeviceWriteBootloader();
		break;
	case COMMAND_CREATE_FILE:
		if (sdOpenFile((uint8_t*)param->str) == SD_RESULT_OK)
			LOG("File created successfully\r\n");
		else
			LOG("File creation failed\r\n");
//		MX_SDMMC1_SD_Init();
		break;
	case COMMAND_READ_RTC:
	{
		prvDevicePrintTime();
	}
		break;
	case COMMAND_WRITE_RTC:
	{
		BSP_TIME_STRUCT time;
		BSP_DATE_STRUCT date;
		//if (sscanf((char*)param->str, "%da%da%d", &time.hours, &time.minutes, &time.seconds) == 3)
		if (strlen((char*)param->str) == 8 && strlen((char*)param->str2) == 8)
		{
			time.hours = ((param->str[0] & 0x0F) * 10 + (param->str[1] & 0x0F));
			time.minutes = ((param->str[3] & 0x0F) * 10 + (param->str[4] & 0x0F));
			time.seconds = ((param->str[6] & 0x0F) * 10 + (param->str[7] & 0x0F));

			date.date = ((param->str2[0] & 0x0F) * 10 + (param->str2[1] & 0x0F));
			date.month = ((param->str2[3] & 0x0F) * 10 + (param->str2[4] & 0x0F));
			date.year = ((param->str2[6] & 0x0F) * 10 + (param->str2[7] & 0x0F));
//		if (sscanf(tmp_str, "%d:%d:%d", &time.hours, &time.minutes, &time.seconds) == 3)
			if (bspRtcTimeSet(&time, &date) != BSP_RESULT_OK)
				LOG("RTC set failed\r\n");
		}
		else
			LOG("RTC set failed\r\n");
	}
		break;
	case COMMAND_WRITE_RTC_BCD:
	{
		BSP_RTC_TIMESTAMP bcd;
		BSP_TIME_DATE_STRUCT timestamp;

		BSP_TIME_STRUCT time;
		BSP_DATE_STRUCT date;

		timestamp.date.date = 15;
		timestamp.date.month = 10;
		timestamp.date.year = 17;

		timestamp.time.hours = 10;
		timestamp.time.minutes = 46;
		timestamp.time.seconds = 30;
		timestamp.time.msec = 120;

		bspRtcTimestampToBCD(&timestamp.date, &timestamp.time, &bcd);

		bspRtcTimeSetBCD(&bcd);



		bspRtcTimeGet(&time, &date);
		LOG("%02d:%02d:%02d\r\n", time.hours, time.minutes, time.seconds);
		LOG("%02d/%02d/%02d\r\n", date.date, date.month, date.year);

	}
		break;
	case COMMAND_WRITE_DATE_RTC:
	{
		BSP_DATE_STRUCT date;
		if (strlen((char*)param->str) == 8)
		{
			date.date = ((param->str[0] & 0x0F) * 10 + (param->str[1] & 0x0F));
			date.month = ((param->str[3] & 0x0F) * 10 + (param->str[4] & 0x0F));
			date.year = ((param->str[6] & 0x0F) * 10 + (param->str[7] & 0x0F));

			if (bspRtcDateSet(&date) != BSP_RESULT_OK)
				LOG("RTC Date set failed\r\n");
		}
		else
			LOG("RTC Date set failed\r\n");
	}
		break;
	case COMMAND_AT_COMMAND:
		bleAtCommand((uint8_t*)param->str, (uint8_t*)param->str2);
		break;
	case COMMAND_AT_MODE:
		if (param->i == 0)
			bleAtCommandMode(BLE_MODE_NORMAL);
		else
			bleAtCommandMode(BLE_MODE_AT_COMMAND);
		break;
	case COMMAND_BLE_TEST:
		bleTest(atoi((char*)param->str));
		break;
	case COMMAND_RESET_TO_DEFAULTS:
		if (prvDeviceEcgConfigResetToDefaults() == DEVICE_RESULT_OK)
			LOG("prvDeviceEcgConfigResetToDefaults OK\r\n");
		else
			LOG("prvDeviceEcgConfigResetToDefaults Failed\r\n");

		break;
	case COMMAND_PBS_TEST:
	{
		int8_t a;
      int32_t b;
		a = atoi(param->str);
		b = atoi(param->str2);
		pbsTxTaskTest(a,b);
	}
	break;
	case COMMAND_DIR:
			sdDir();
		break;
	case COMMAND_SD_WRITE:
	{
		sdWriteDataToFile((uint8_t*)param->str, (uint8_t*)param->str2, strlen(param->str2), NULL);
	}
		break;
	case COMMAND_SD_READ:
	{
		uint8_t sd_read[50] = {0};
		uint16_t sd_read_len = 0;
		uint16_t sd_index = 0;
		do{
			memset(sd_read, 0, sizeof(sd_read));
			if (sdReadDataFromFile((uint8_t*)param->str, 50, sd_index, sd_read,&sd_read_len) != SD_RESULT_OK)
				break;
			LOG("%s", sd_read);
			sd_index += 50;

		}while(sd_read_len == 50);
		LOG("\r\n");
	}
		break;
	case COMMAND_SD_DELETE:
		sdDeleteFile((uint8_t*)param->str);
		break;
//	case COMMAND_SD_FORMAT:
//		sdFormat();
//		break;
	case COMMAND_SD_TEST:
		sdTestSdCard();
		break;
	case COMMAND_SD_CD:
		sdChangeDir((uint8_t*)param->str);
		break;
	case COMMAND_SD_MAKE_DIR:
		sdMakeDir((uint8_t*)param->str);
		break;
	case COMMAND_CLOCK_CHANGE:
	{
//		HAL_RCC_DeInit();
		BSP_RESULT_E bsp_res = BSP_RESULT_OK;
		if (param->i == 0)
			bsp_res = SystemClock_Config_40Mhz();
		else if (param->i == 1)
			bsp_res = SystemClock_Config_5Mhz();
		LOG("COMMAND_CLOCK_CHANGE bsp_res=%d\r\n", bsp_res);
	}
		break;

	case COMMAND_TIMING_ACCURICY_RESET:
		timing_accuracy_flag = DEVICE_TIMING_ACCURACY_FLAG_RESET;
		break;
	case COMMAND_TIMING_ACCURICY_STAT:
		timing_accuracy_flag = DEVICE_TIMING_ACCURACY_FLAG_PRINT_STAT;
		break;
	case COMMAND_DEBUG_MODE_MENU:
		LOG("Debug Menu:\r\n");
		LOG("SD\t%d\r\n", DEBUG_LEVEL_9);
		LOG("BLE\t%d\r\n", DEBUG_LEVEL_10);
		LOG("Temperature\t%d\r\n", DEBUG_LEVEL_11);
		break;
	case COMMAND_PARSE_JSON:
//		static const char *JSON_STRING =
//			"{\"user\": \"johndoe\", \"admin\": false, \"uid\": 1000,\n  "
//			"\"groups\": [\"users\", \"wheel\", \"audio\", \"video\"]}";
//		static jsmn_parser j_parser = {0};
//		static jsmntok_t jsmn_tok[20] = {0};
//		sdReadDataFromFile("json.txt", 1000, 0, buff_read, &buff_read_length);
//		jsmn_parse(&j_parser, buff_read, buff_read_length, jsmn_tok, 20);
//		int ret = jsmn_parse(&j_parser, JSON_STRING, strlen(JSON_STRING), jsmn_tok, sizeof(jsmn_tok)/sizeof(jsmn_tok[0]));
		break;
	case COMMAND_PRINT_STACKS:
		prvDevicePrintStacks();
		break;
   case COMMAND_ADS_INT_COUNT:
      {
         adsPrintIntCount();
      }
      break;
   case COMMAND_CHECK_IMPEDANCE:
//	   ecgImpedanceStart();
	   osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_IMPEDANCE_TIME, 0);

	   break;
   case COMMAND_CHECK_IMPEDANCE_INTERVAL:
	   ecgImpedanceIntervalSet(param->i);
	   break;
   case COMMAND_PBS_REMOTE_EVENT:
	   {
			PBS_RX_PAYLOAD_STRUCT payload;
			PBS_TLV_LIST_STRUCT li;
			uint8_t val = param->i == 1 ? PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENABLE : PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_DISABLE;
			li.subtype = PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT;
			li.value = &val;
			li.length = 1;
         li.next = NULL;
	        payload.tlv_list = &li;

		   prvDeviceEcgRemoteEventCmd(payload);
	   }
	   break;
   case COMMAND_ECG_TEMP_SET:
   {
	   if (param->i == 0)
	   {
		   ecgTemperatureSet(ECG_TEMPERATURE_SET_OUT_OF_RANGE);
	   }
	   else
	   {
		   ecgTemperatureSet(ECG_TEMPERATURE_SET_OK);
	   }

   }
	   break;
   case COMMAND_BAT_MON_EN:
	   batMonReadEnable(param->i);
	   break;
   case COMMAND_LOG_SET:
   {
	   SERIAL_PROTOCOL_RESULT_E ser_res;
	   if (strcmp(param->str, "?") == 0)
	   {
		   LOG("\r\nSet Module Log Level, usage: \"log [MODULE] [LEVEL]\"\r\n");
		   LOG("[MODULE]:\tdevice, ecg, sd, accel, batt, ble, pbs (case sensitive)\r\n");
		   LOG("[LEVEL]:\t1, 2, 4, 8  (or any combination) \r\n");
		   LOG("use \"log clear\" to remove all logs\r\n");
		   LOG("Description:\r\n");
		   LOG("BLE\tlevel 8 = hex (rx/tx buffers)\r\n");
		   LOG("ECG\tlevel 8 = raw, level 4 = filtered, level 2 = algorithm results, level 1 = logic (contacts etc)\r\n");
		   LOG("PBS\tlevel 8 = tlv shit, level 4 = rx messages & timings, level 2 = tx messages\r\n");
		   LOG("SD\tlevel 8 = r/w timings\r\n");
		   LOG("DEVICE\tlevel 8 = hex, level 4 = events/ temp reads, level 2 = rx handling, level 1 = business logic\r\n");
		   LOG("Battery\tlevel 1 = reads\r\n");
		   LOG("Accel\tlevel 1 = accelerometer algorithm position\r\n");
		   LOG("\r\noff - DISABLE LOGS, on - ENABLE\r\n");
		   break;
	   }
	   DEBUG_MODULE_E module = DEBUG_MODULE_MAX;
	   if (strcmp(param->str, "device") == 0)
		   module = DEBUG_MODULE_DEVICE;
	   else if(strcmp(param->str, "ecg") == 0)
		   module = DEBUG_MODULE_ECG;
	   else if(strcmp(param->str, "sd") == 0)
		   module = DEBUG_MODULE_SD;
	   else if(strcmp(param->str, "accel") == 0)
		   module = DEBUG_MODULE_ACCEL;
	   else if(strcmp(param->str, "batt") == 0)
		   module = DEBUG_MODULE_BATTERY;
	   else if(strcmp(param->str, "ble") == 0)
		   module = DEBUG_MODULE_BLE;
	   else if(strcmp(param->str, "pbs") == 0)
		   module = DEBUG_MODULE_PBS;
	   else if(strcmp(param->str, "clear") == 0)
	   {
		   ser_res = serialProtocolSetModuleLogLevel(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_NONE);
		   ser_res |= serialProtocolSetModuleLogLevel(DEBUG_MODULE_ECG, DEBUG_LEVEL_NONE);
		   ser_res |= serialProtocolSetModuleLogLevel(DEBUG_MODULE_SD, DEBUG_LEVEL_NONE);
		   ser_res |= serialProtocolSetModuleLogLevel(DEBUG_MODULE_ACCEL, DEBUG_LEVEL_NONE);
		   ser_res |= serialProtocolSetModuleLogLevel(DEBUG_MODULE_BATTERY, DEBUG_LEVEL_NONE);
		   ser_res |= serialProtocolSetModuleLogLevel(DEBUG_MODULE_BLE, DEBUG_LEVEL_NONE);
		   ser_res |= serialProtocolSetModuleLogLevel(DEBUG_MODULE_PBS, DEBUG_LEVEL_NONE);
		   LOG("\r\nLog level clear %s", (ser_res == SERIAL_PROTOCOL_RESULT_OK ? "ok" : "failed, use \"log ?\" for help"));
		   break;
	   }
	   else if (strcmp(param->str, "off") == 0)
	   {
		   LOG("Logs disabled\r\n");
		   serialProtocolLogsEnable(0);
		   break;
	   }
	   else if (strcmp(param->str, "on") == 0)
	   {
		   serialProtocolLogsEnable(1);
		   LOG("Logs enabled\r\n");
		   break;
	   }

	   uint8_t level = strtol(param->str2, NULL, 0);

	   ser_res = serialProtocolSetModuleLogLevel(module, (DEBUG_LEVEL)level);
	   LOG("\r\nLog level set %s", (ser_res == SERIAL_PROTOCOL_RESULT_OK ? "ok" : "failed, use \"log ?\" for help\r\n"));

   }
	   break;
   case COMMAND_LOG_PRINT:
   {
	   uint8_t hex[] = "1234";
	   LOG("*Log Levels*\r\n");

	   LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_NONE, "Device none\r\n");
	   LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_1, "Device 1\r\n");
	   LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_2, "Device 2\r\n");
	   LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_3, "Device 4\r\n");
	   LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_4, "Device 8 \r\n");
	   LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_HEX, (char*)hex, sizeof(hex));

	   LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_NONE, "ECG none\r\n");
	   LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "ECG 1\r\n");
	   LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "ECG 2\r\n");
	   LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_3, "ECG 4\r\n");
	   LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_4, "ECG 8\r\n");

	   LOG_D(DEBUG_MODULE_SD, DEBUG_LEVEL_NONE, "SD none\r\n");
	   LOG_D(DEBUG_MODULE_SD, DEBUG_LEVEL_1, "SD 1\r\n");
	   LOG_D(DEBUG_MODULE_SD, DEBUG_LEVEL_2, "SD 2\r\n");
	   LOG_D(DEBUG_MODULE_SD, DEBUG_LEVEL_3, "SD 4\r\n");
	   LOG_D(DEBUG_MODULE_SD, DEBUG_LEVEL_4, "SD 8\r\n");

	   LOG_D(DEBUG_MODULE_ACCEL, DEBUG_LEVEL_NONE, "Accel none\r\n");
	   LOG_D(DEBUG_MODULE_ACCEL, DEBUG_LEVEL_1, "ACCEL 1\r\n");
	   LOG_D(DEBUG_MODULE_ACCEL, DEBUG_LEVEL_2, "ACCEL 2\r\n");
	   LOG_D(DEBUG_MODULE_ACCEL, DEBUG_LEVEL_3, "ACCEL 4\r\n");
	   LOG_D(DEBUG_MODULE_ACCEL, DEBUG_LEVEL_4, "ACCEL 8\r\n");

	   LOG_D(DEBUG_MODULE_BATTERY, DEBUG_LEVEL_NONE, "Battery none\r\n");
	   LOG_D(DEBUG_MODULE_BATTERY, DEBUG_LEVEL_1, "Battery 1\r\n");
	   LOG_D(DEBUG_MODULE_BATTERY, DEBUG_LEVEL_2, "Battery 2\r\n");
	   LOG_D(DEBUG_MODULE_BATTERY, DEBUG_LEVEL_3, "Battery 4\r\n");
	   LOG_D(DEBUG_MODULE_BATTERY, DEBUG_LEVEL_4, "Battery 8\r\n");

	   LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_NONE, "BLE none\r\n");
	   LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_1, "BLE 1\r\n");
	   LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_2, "BLE 2\r\n");
	   LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_3, "BLE 4\r\n");
	   LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_4, "BLE 8 \r\n");
	   LOG_D(DEBUG_MODULE_BLE, DEBUG_LEVEL_HEX, (char*)hex, sizeof(hex));

	   LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_NONE, "PBS none\r\n");
	   LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_1, "PBS 1\r\n");
	   LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "PBS 2\r\n");
	   LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "PBS 4\r\n");
	   LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_4, "PBS 8\r\n");


	   LOG_ERR("*Error Message*\r\n");
   }
		break;
		case COMMAND_SD_RENAME_FILE:
		{
			if (sdRenameFile((uint8_t*)param->str, (uint8_t*)param->str2) == SD_RESULT_OK)
				LOG("File renamed successfully\r\n");
			else
				LOG("File rename failed\r\n");
		}
		break;

		case COMMAND_WRITE_DEVICE_SN:
		{
			if (strlen(param->str) > sizeof(device_id))
				LOG("Device ID too long");
			else
			{
				strcpy(device_id, (uint8_t*)param->str);
				osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_WRITE_DEVICE_ID, 1000);
			}
//			prvDeviceSnSet((uint8_t*)param->str);
		}
		break;

		case COMMAND_ERROR_EVENT:
		{
			pbsSendErrorEvent((PBS_GENERAL_ERRORS_EVENTS_ENUM)param->i);
		}
		break;

		case COMMAND_SD_GET_INFO:
		{
			uint32_t sd_total_size = 0, sd_free_size = 0;
			sdGetSdInfo(&sd_total_size, &sd_free_size);

			LOG("SD capacity: %dKB\r\n", sd_total_size);
			LOG("SD free space: %dKB\r\n", sd_free_size);
			LOG("SD used space: %dKB\r\n", (sd_total_size - sd_free_size));

		}
		break;

		case COMMAND_START_BATTERY_TRAINING:
		{
			prvDeviceStartBatteryTraining();
		}
		break;

		case COMMAND_START_BLE_CONFIG:
		{
			bleStartConfig();
		}
		break;

		case COMMAND_MODE_SET:
		{
			DEVICE_CONFIG_STRUCT conf = {0};
			if (strcmp(param->str, "?") == 0)
			{
				LOG("usage: mode_set <MODE> <PM_MODE>\r\n");
				LOG("where <MODE> is holter, patch or tester\r\n");
				LOG("where <PM_MODE> is pace maker enable flag 1=on, 0=off");
				break;
			}
			else if (strcmp(param->str, "holter") == 0)
			{
				conf.device_mode = ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY;
			}
			else if (strcmp(param->str, "patch") == 0)
			{
				conf.device_mode = ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH;
			}
			else if (strcmp(param->str, "tester") == 0)
			{
				conf.device_mode = ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER;
			}
			else
			{
				LOG("<MODE> bad param, for usage: mode_set ?\r\n");
				break;
			}
			if (strcmp(param->str2, "1") == 0)
			{
				conf.pace_maker_enable = ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_ON;
			}
			else if (strcmp(param->str2, "0") == 0)
			{
				conf.pace_maker_enable = ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_OFF;
			}
			else
			{
				LOG("<PM_MODE> bad param, for usage: mode_set ?\r\n");
				break;
			}

			DEVICE_CONFIG_RESULT_E conf_res = deviceConfigChangeMode(&conf);
			LOG("MODE SET %s\r\n", (conf_res == DEVICE_CONFIG_RESULT_OK ? "succeed, Please Restart System (rst)" : "failed"));
			if (conf_res != DEVICE_CONFIG_RESULT_OK)
				LOG("error code %d", conf_res);
		}
		break;

		case COMMAND_ECHO_SET:
			SERIAL_PROTOCOL_RESULT_E ser_res = serialProtocolEchoEnable(param->i);
			LOG("echo set %d, result %s\r\n", param->i, ser_res == SERIAL_PROTOCOL_RESULT_OK ? "succeed" : "failed");
			break;
		case COMMAND_ACCEL_REG_WRITE:
		{
			accelWriteLis2hReg(ascii2Hex(param->str[0], param->str[1]), ascii2Hex(param->str2[0], param->str2[1]));
		}
		break;

		case COMMAND_CLEAR_ECG_DATA_FROM_SD:
		{
        	DEVICE_RESULT_ENUM dev_res = prvDeviceClearSdEcgData();
            LOG("COMMAND_CLEAR_ECG_DATA_FROM_SD: ECG directory content deleted %s %d\r\n", (dev_res == DEVICE_RESULT_OK ? "ok" : "failed"), dev_res);
			if (dev_res == DEVICE_RESULT_OK)
			{
				osDelay(100);
				bsp_system_reset();
			}
		}
		break;

		case COMMAND_PM_TEST:
		{
			ECG_RESULT ecg_res = ecgPmInitStart(param->i);
			LOG("COMMAND_PM_TEST start res: ecg %d", ecg_res);

		}
		break;

		case COMMAND_SET_CHECKSUM:
		{
			prvDeviceSetChecksum();
		}
		break;

		case COMMAND_ACCEL_REG_READ:
		{
			accelPrintRegisters();
		}
		break;

		case COMMAND_SET_BIT_FAILURE:
		{
			if (param->i > 0x17F)
				LOG("BIT error value is too high: %d", param->i);
			else
				device_bit = (PBS_DEVICE_STATUS_BIT_ERROR_ENUM)param->i;
		}
		break;

		case COMMAND_COMPARATOR_READ:
		{
			uint32_t tmp = 0;
			bspReadComp(&tmp);
		}
		break;

		case COMMAND_SLEEP:
		{
			/*
			GPIO_InitTypeDef GPIO_InitStruct;
//			bsp_write_pin(BSP_PIN_TYPE_BLE_RESET, BSP_PIN_RESET);
//			accelSleep();
			switch (param->i)
			{
				case 1:
//					bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_SET);
					GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
					GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
					GPIO_InitStruct.Pull = GPIO_NOPULL;
					GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
					HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);

					bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_RESET);
				break;

				case 2:
//					bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_SET);
					GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
					GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
					GPIO_InitStruct.Pull = GPIO_NOPULL;
					GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
					HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);

					bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_SET);
				break;

				case 3:
					GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
					GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
					GPIO_InitStruct.Pull = GPIO_NOPULL;
					GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
					HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);
					bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_RESET);
					bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_RESET);
				break;

				case 4:
					GPIO_InitStruct.Pin = SD_PWR_ON_PIN;
					GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
					GPIO_InitStruct.Pull = GPIO_NOPULL;
					GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
					HAL_GPIO_Init(SD_PWR_ON_PORT, &GPIO_InitStruct);
					bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_RESET);
					bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_RESET);
				break;

				case 5:
					HAL_PWREx_EnterSTOP1Mode(PWR_STOPENTRY_WFE);
				break;

				case 6:
					HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFE);
				break;

				case 7:
					HAL_PWREx_EnterSHUTDOWNMode();
				break;
			}

			//ecgStop();
			*/
//			bsp_write_pin(BSP_PIN_TYPE_SD_PWR_ON, BSP_PIN_SET);
//			bsp_write_pin(BSP_PIN_TYPE_SDMMC1_EN, BSP_PIN_RESET);

//			HAL_PWREx_EnterSHUTDOWNMode();
//			HAL_PWREx_EnterSTOP1Mode(PWR_STOPENTRY_WFE);
			//PWR->CR1 |= PWR_CR1_LPMS_SHUTDOWN;
//			HAL_PWREx_EnterSTOP1Mode(PWR_STOPENTRY_WFI);
		}
		break;

		case COMMAND_SET_FILE_DUARTION_LENGTH:
		{
			if (pbsSetFileDurationLen(param->i) == PBS_RESULT_OK)
				LOG("ECG file duration set to %d\r\n", param->i);
			else
				LOG("ECG file duration failed\r\n", param->i);
		}
		break;

		case COMMAND_GPIO_SET:
		{
			bspGpioSet(param->str, param->str2);
		}
		break;

		case COMMAND_SPI_WRITE:
		{

//			bsp_write_pin(BSP_PIN_TYPE_ECG_START, BSP_PIN_RESET); // stop PE13
//
//			bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET); //PE09
//			osDelay(1000);
//			//CS High;
//			//  bsp_write_pin(BSP_PIN_TYPE_ECG1_CS, BSP_PIN_SET);
//			osDelay(100);
//			//Reset Pulse;
//			bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_RESET); //PE09
//			osDelay(100);
//
//			bsp_write_pin(BSP_PIN_TYPE_ECG1_RESET, BSP_PIN_SET); //PE09

			int a;
			BSP_RESULT_E bsp_res;
			uint8_t i = 0;
			uint8_t buffer[10] = {0};
			uint8_t str1_len = strlen(param->str);
			if (str1_len % 2 != 0)//Must be even
				break;
			for (i = 0; i < (str1_len / 2); i++)
				buffer[i] = ascii2Hex(param->str[i * 2], param->str[i * 2+ 1]);

			//bsp_res = bsp_spi_transmit(BSP_SPI_RAM, buffer, i, 5000);
			LOG("bsp_res: %d\r\n", bsp_res);
		}
		break;

		case COMMAND_SPI_READ:
		{
			BSP_RESULT_E bsp_res;
			uint8_t buffer_write[15] = {0};
			uint8_t buffer_write2[15] = {0};
			static uint8_t buffer_read[20] = {0};
			memset(buffer_read, 0, sizeof(buffer_read));
			buffer_write[0] = ascii2Hex(param->str[0], param->str[1]);
			//buffer_write[0] |= 0x80;
//			buffer_write[1] = atoi(param->str2);

			//bsp_res = bsp_spi_transmit_receive(BSP_SPI_RAM, buffer_write, atoi(param->str2) + 1, 5000, buffer_read);
			//bsp_res = bsp_spi_transmit_receive(BSP_SPI_RAM, buffer_write2, 1, 5000, buffer_read);

			LOG("bsp_res: %d\r\n", bsp_res);


			LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_HEX, (char*)buffer_read, sizeof(buffer_read));

		}
		break;
/*
		case COMMAND_SRAM_WRITE:
		{
			BSP_RESULT_E bsp_res;
			uint8_t sram_mode_seq[] = {0x01, //Write mode register
										0x40}; //Sequential mode
			uint8_t sram_mode_byte[] = {0x01, //Write mode register
										0x00}; //Byte mode
			uint8_t sram_mode_page[] = {0x01, //Write mode register
										0x80}; //Page mode

//			uint8_t sram_data_write[] = {0x02,  //Write data
//									0x00, 0x12, 0x34, //Address
//			0x99, 0x20, 0x79, 0x6f, 0x00, 0x08, 0x15, 0x70, 0x00, 0x08, 0x19, 0x70, 0x08, 0x39, 0x70, 0xab,
//			0x00, 0x08, 0x1d, 0x70, 0x00, 0x08, 0x21, 0x70, 0x00, 0x08, 0x25, 0x70, 0x00, 0x08, 0x00, 0x00,
//			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xcd, 0x64,
//			0x00, 0x08, 0x29, 0x70, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x69, 0x64, 0x00, 0x08, 0x5f, 0x6a,
//			0x00, 0x08, 0x2d, 0x70, 0x00, 0x08, 0x31, 0x70, 0x00, 0x08, 0x35, 0x70, 0x00, 0x08, 0x39, 0x70,
//			0x00, 0x08, 0x3d, 0x70, 0x00, 0x08, 0x41, 0x70, 0x00, 0x08, 0xb5, 0x6a, 0x00, 0x08, 0xbb, 0x6a,
//			0x00, 0x08, 0xcb, 0x6a, 0x00, 0x08, 0xdb, 0x6a, 0x00, 0x08, 0x45, 0x70, 0x00, 0x08, 0x49, 0x70,
//			0x00, 0x08, 0x4d, 0x70, 0x00, 0x08, 0x51, 0x70, 0x00, 0x08, 0x57, 0x6a, 0x00, 0x08, 0x6d, 0x6a,
//			0x00, 0x08, 0x85, 0x6a, 0x00, 0x08, 0x7d, 0x6a, 0x00, 0x08, 0x55, 0x70, 0x00, 0x08, 0x00, 0x00,
//			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xa5, 0x6a,
//			0x00, 0x08, 0x59, 0x70, 0x00, 0x08, 0x5d, 0x70, 0x00, 0x08, 0x61, 0x70, 0x00, 0x08, 0x65, 0x70,
//			0x00, 0x08, 0x69, 0x70, 0x00, 0x08, 0x6d, 0x70, 0x00, 0x08, 0x71, 0x70, 0x00, 0x08, 0x75, 0x70,
//			0x00, 0x08, 0x79, 0x70, 0x00, 0x08, 0x7d, 0x70, 0x00, 0x08, 0x81, 0x70, 0x00, 0x08, 0x85, 0x70,
//			0x00, 0x08, 0x89, 0x70, 0x00, 0x08, 0x8d, 0x6a, 0x00, 0x08, 0x95, 0x6a, 0x00, 0x08, 0x00, 0x00,
//			0x00, 0x00, 0x9d, 0x6a, 0x00, 0x08, 0x8d, 0x70, 0x00, 0x08, 0x91, 0x70, 0x00, 0x08, 0x00, 0x00,
//			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x95, 0x70,
//			0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//			0x00, 0x00, 0x4f, 0x6a, 0x00, 0x08, 0xa5, 0x70, 0x00, 0x08, 0xeb, 0x6a, 0x00, 0x08, 0xa9, 0x70,
//			0x00, 0x08, 0xad, 0x70, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//			0x00, 0x08, 0xf3, 0x6a, 0x00, 0x08, 0xb5, 0x70, 0x00, 0x08, 0x75, 0x6a, 0x00, 0x08, 0xb9, 0x70};

			bsp_res = bsp_spi_transmit(BSP_SPI_RAM, sram_mode_seq, sizeof(sram_mode_seq), 5000);
			LOG("SRAM register mode, bsp_res: %d\r\n", bsp_res);

			bsp_res = bsp_spi_transmit(BSP_SPI_RAM, sram_data_write, sizeof(sram_data_write), 5000);
			LOG("SRAM data write, bsp_res: %d\r\n", bsp_res);


		}
		break;

		case COMMAND_SRAM_READ:
		{
			BSP_RESULT_E bsp_res;
			uint8_t sram_read[] = {0x03,  //Read data
									0x00, 0x12, 0x34}; //Address
			uint8_t sram_data_read[320 + sizeof(sram_read)] = {0}; //Read data buffer

			bsp_res = bsp_spi_transmit_receive(BSP_SPI_RAM, sram_read, sizeof(sram_read) + sizeof(sram_data_read), 5000, sram_data_read);

			LOG("SRAM Read, bsp_res: %d\r\n", bsp_res);
			LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_HEX, (char*)&sram_data_read[4], sizeof(sram_data_read) - sizeof(sram_read));
			if (memcmp((uint8_t*)&sram_data_read[4], &sram_data_write[4], sizeof(sram_data_read) - sizeof(sram_read)) == 0)
				LOG("Data matches %d\r\n");
			else
				LOG("\r\nData not matches %d\r\n");
		}
		break;

		case COMMAND_SRAM_TEST:
		{
			BSP_RESULT_E bsp_res;
			uint8_t sram_mode_seq[] = {0x01, //Write mode register
										0x40}; //Sequential mode

			uint8_t sram_read[] = {0x03,  //Read data
									0x00, 0x12, 0x34}; //Address
			uint8_t sram_data_read[320 + sizeof(sram_read)] = {0}; //Read data buffer


			bsp_res = bsp_spi_transmit(BSP_SPI_RAM, sram_mode_seq, sizeof(sram_mode_seq), 5000);
			LOG("SRAM register mode, bsp_res: %d\r\n", bsp_res);

			do{
				bsp_res = bsp_spi_transmit(BSP_SPI_RAM, sram_data_write, sizeof(sram_data_write), 5000);
				//LOG("SRAM data write, bsp_res: %d\r\n", bsp_res);

				bsp_res |= bsp_spi_transmit_receive(BSP_SPI_RAM, sram_read, sizeof(sram_read) + sizeof(sram_data_read), 5000, sram_data_read);
				if (memcmp((uint8_t*)&sram_data_read[4], &sram_data_write[4], sizeof(sram_data_read) - sizeof(sram_read)) != 0)
					break;
				memset(sram_data_read, 0, sizeof(sram_data_read));
			}while(bsp_res == BSP_RESULT_OK);
		}
		break;
*/
		case COMMAND_SET_BLE_ADV_NAME:
		{
			prvDeviceSetBleName(param->str);
		}
		break;

	default:
		break;

	}

	return result;
}


RESULT_E deviceSetSerialCommands()
{
	return serialProtocolSetCommands(&deviceCommandHandler, device_cmd_list, COMMAND_MAX);
}

#ifdef G_UNIT_TEST_ENABLE
//override UT_main
void finishUnitTesting()
{
	deviceSetSerialCommands();
	is_unit_test = FALSE;
	bsp_set_unit_test(FALSE);
}
#endif //G_UNIT_TEST_ENABLE



static DEVICE_RESULT_ENUM prvDeviceInfoSave(PBS_DEVICE_INFO_STRUCT * info)
{
	SD_RESULT_E sd_res;
	uint8_t		write_info[50];

	sd_res = sdFileExist(DEVICE_INFO_FILE_NAME);

	if (sd_res == SD_RESULT_OK)	// file exists
		sdDeleteFile(DEVICE_INFO_FILE_NAME);
	else if (sd_res != SD_RESULT_ERROR_FILE_DOES_NOT_EXIST)
		return DEVICE_RESULT_SD_ERROR;

	sprintf((char*)write_info, "FW_VERSION=%d.%d.%d.%d\r\nSN=", info->fw.major, info->fw.minor, info->fw.patch, info->fw.build);
	for (uint8_t i=0; i<sizeof(info->device_sn); i++)
		sprintf((char*)write_info, "%s%c", write_info, info->device_sn[i]);
	sprintf((char*)write_info, "%s\r\n", write_info);

	sd_res = sdWriteDataToFile(DEVICE_INFO_FILE_NAME, write_info, strlen((char*)write_info), NULL);
	if (sd_res != SD_RESULT_OK)
	{
		LOG("Failed to write file, error: %d\r\n", sd_res);
		return DEVICE_RESULT_SD_ERROR;
	}

	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceInfoJsonSave(PBS_DEVICE_INFO_STRUCT * info)
{
	SD_RESULT_E 	sd_res;
	uint8_t			write_info[180] = {0};

	sd_res = sdFileExist(DEVICE_INFO_FILE_NAME);

	if (sd_res == SD_RESULT_OK)	// file exists
		sdDeleteFile(DEVICE_INFO_JSON_FILE_NAME);
	else if (sd_res != SD_RESULT_ERROR_FILE_DOES_NOT_EXIST)
		return DEVICE_RESULT_SD_ERROR;

	sprintf((char*)write_info, "{\r\n\"Make\": \"G-Medical Innovations\",\r\n\"Model\": \"GMP\",\r\n\"FW_VERSION\":\"%d.%d.%d.%d\",\r\n\"SN\":\"", info->fw.major, info->fw.minor, info->fw.patch, info->fw.build);
	for (uint8_t i=0; i<sizeof(info->device_sn); i++)
		sprintf((char*)write_info, "%s%c", write_info, info->device_sn[i]);
	sprintf((char*)write_info, "%s\"\r\n}", write_info);

	sd_res = sdWriteDataToFile(DEVICE_INFO_JSON_FILE_NAME, write_info, strlen((char*)write_info), NULL);
	if (sd_res != SD_RESULT_OK)
	{
		LOG("Failed to write file, error: %d\r\n", sd_res);
		return DEVICE_RESULT_SD_ERROR;
	}

	return DEVICE_RESULT_OK;
}

DEVICE_RESULT_ENUM prvDeviceSetDefaultLogLevels()
{
	serialProtocolSetModuleLogLevel(DEBUG_MODULE_DEVICE, (DEBUG_LEVEL)((DEBUG_LEVEL_4  | DEBUG_LEVEL_1)) );
//	serialProtocolSetModuleLogLevel(DEBUG_MODULE_BLE, DEBUG_LEVEL_4);

	return DEVICE_RESULT_OK;
}
uint8_t 						temp_read[300] = {0};
uint16_t 						temp_len = 0;
char* 							ptr_sn;
#define DEVICE_SN_JSON_KEY		"\"SN\":\""
static DEVICE_RESULT_ENUM prvDeviceConfiguration(void)
{
	SD_RESULT_E 	sd_res;
	uint8_t			gmp_id[DEVICE_SN_LENGTH + 1] = {0};
	char			serial_numer_json[DEVICE_SN_LENGTH + 1] = {0};

	// get device mode configuration: holter only / patch, pace maker en/disable
	DEVICE_CONFIG_RESULT_E dev_conf_res = deviceConfigGet(&device_mode_conf);

	switch (dev_conf_res)
	{
		case DEVICE_CONFIG_RESULT_NEW_FILE_CREATED_WITH_DEFAULTS:
		case DEVICE_CONFIG_RESULT_OK:
			// everything is ok
			break;
		case DEVICE_CONFIG_RESULT_SD_ERROR:
			{
				LOG_ERR("sd read/ write FAILED\r\n");
				prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
				deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
			}
			break;
		case DEVICE_CONFIG_RESULT_WRONG_PARAM:
		default:
			{
				LOG_ERR("sd read/ write FAILED\r\n");
				prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
				deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
			}
			break;
	}

	ECG_RESULT ecg_res = ecgSetDefaultParams(device_mode_conf.device_mode, device_mode_conf.pace_maker_enable);
	if (ecg_res != ECG_RESULT_OK)
	{
		LOG("prvDeviceConfiguration ECG ecgSetDefaultParams after BIT failed %d \r\n", ecg_res);
	}

	if (device_mode_conf.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY)
	{
		//set clock to 5 Mhz
		//HAL_RCC_DeInit();
		//SystemClock_Config_4Mhz(); //Check power consumtion!!!
		SystemClock_Config_16Mhz();
		serialProtocolInit();
		prvDevicePrintInfo();
		if (bleInit() != BLE_RESULT_OK)
			LOG("BLE init failed\r\n");
		LOG("\r\n\r\n~~(__^�>  Device Mode: Holter  <�^__)~~  \r\n");
		//Create BLE expiration timer
		//Currently BLE should work all the time for the clinical trial 12.6.18
//		osTimerDef(ble_holter_timer, bleHolterTimerCallback);
//		ble_holter_timer = osTimerCreate(osTimer(ble_holter_timer), osTimerOnce, ( void * ) 0);
//		osTimerStart(ble_holter_timer, BLE_HOLTER_TIMER_PERIOD);

		sd_res = sdFileExist(DEVICE_FORMAT_SD_FILE_NAME);
		if (sd_res == SD_RESULT_OK)
		{//if "reset.json" file exist
			sd_res = sdFileExist(DEVICE_INFO_JSON_FILE_NAME);
//			if (sd_res != SD_RESULT_OK)
//			{//Check if GMP serial number the same as the one in the "device_info.json"
//				deviceSetErrorState();
//				LOG("SD card does not contain \"device_info.json\" file, error %d\r\n",sd_res);
//				return DEVICE_RESULT_ERROR;
//			}
			prvDeviceIdGet(gmp_id);
			sd_res = sdReadDataFromFile(DEVICE_FORMAT_SD_FILE_NAME, sizeof(temp_read), 0, (uint8_t *)temp_read, &temp_len);
			if (sd_res != SD_RESULT_OK)
				LOG("Failed to read file, error: %d\r\n", sd_res);
			ptr_sn = strstr(temp_read,"\"SN\":\"");
			if (ptr_sn == NULL)
			{
				prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
				deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
				LOG("No serial number inside reset.json file\r\n");
				return DEVICE_RESULT_ERROR;
			}
			strncpy(serial_numer_json, (ptr_sn + 6), DEVICE_SN_LENGTH);
			if (strncmp(gmp_id, serial_numer_json, DEVICE_SN_LENGTH) != 0)
			{
				prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
				deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
				LOG("Wrong GMP ID inside \"device_info.json\" file\r\n");
				return DEVICE_RESULT_ERROR;
			}

			sd_res = sdFileExist(DEVICE_PATIENT_INFO_JSON_FILE_NAME);
			if (sd_res == SD_RESULT_OK)
			{//if patient_info.json exists we need to delete it before we renaming reset.json
				sd_res = sdDeleteFile(DEVICE_PATIENT_INFO_JSON_FILE_NAME);
				if (sd_res != SD_RESULT_OK)
				{
					prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
					deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
					LOG("Could not delete \"patient_info.json\" file from SD card, error %d\r\n",sd_res);
					return DEVICE_RESULT_ERROR;
				}
			}
			sd_res = sdDeleteDirContect(DEVICE_ECG_DIRECTORY_NAME);
			if (sd_res == SD_RESULT_OK)
				LOG("ECG directory content deleted successfully\r\n");
			else//TODO:Add error led
				LOG("ECG directory content could not be deleted, error %d\r\n",sd_res);

			sd_res = sdRenameFile(DEVICE_FORMAT_SD_FILE_NAME, DEVICE_PATIENT_INFO_JSON_FILE_NAME);
			if (sd_res != SD_RESULT_OK)
			{
				prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
				deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
				LOG("SD card could not write \"patient_info.json\" file, error %d\r\n",sd_res);
				return DEVICE_RESULT_ERROR;
			}
		}
	}
	else if(device_mode_conf.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER)
	{
		SystemClock_Config_40Mhz();

		LOG("\r\n\r\n><(((('>  Device Mode: Tester  <'))))>< \r\n");
	}
	else
	{
		// reset clock to 40 Mhz
		SystemClock_Config_40Mhz();

		LOG("\r\n\r\n_/\__/\__0>  Device Mode: Patchale  <0__/\__/\_\r\n");
	}

	//Delete FW file if exists
	if (sdFileExist(DEVICE_FW_FILE_NAME) == SD_RESULT_OK)
	{
		if (sdDeleteFile(DEVICE_FW_FILE_NAME) != SD_RESULT_OK)
			prvDeviceLogSd("Firmware file delete failed");
		else
			prvDeviceLogSd("Firmware file delete succeeded");
	}

#ifdef BLE_ADVERTISEMENT_ON
	pbsSetOutputMode(PBS_OUTPUT_TYPE_SD_BLE);
#else
	pbsSetOutputMode(PBS_OUTPUT_TYPE_SD_ONLY);
#endif //BLE_ADVERTISEMENT_ON

	return RESULT_OK;
}

DEVICE_RESULT_ENUM	prvDevicePrintMode(DEVICE_CONFIG_STRUCT * mode)
{
	switch (mode->device_mode)
	{
	case ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER:
		LOG("\r\n\r\n><(((('>  Device Mode: Tester  <'))))>< \r\n");
		break;
	case ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY:
		LOG("\r\n\r\n~~(__^�>  Device Mode: Holter  <�^__)~~  \r\n");
		break;
	case ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH:
		LOG("\r\n\r\n_/\__/\__0>  Device Mode: Patchale  <0__/\__/\_\r\n");
		break;
	default:
		LOG("\r\n\r\n()___)____________) Device Mode: UNKNOWN  (____________(___()\r\n");
	}
	LOG("Pace maker O%s", (mode->pace_maker_enable == ECG_RESP_GENERAL_CONFIG_PACE_MAKER_ENABLED_ON ? "N" : "FF"));
	return DEVICE_RESULT_OK;
}

RESULT_E deviceInit(void)
{
//    uint8_t    hw_ver = 0;
#ifdef MENNEN_ALGO

//	HAL_RCC_DeInit();
//	SystemClock_Config_80Mhz();	//SystemClock_Config_40Mhz();

	/* Init serial protocol */
	if (serialProtocolInit() != RESULT_OK)
		LOG_ERR("\r\ninit_serial_protocol failed");

	/* Init ECG */
	if (ecgInit() != ECG_RESULT_OK)
	{
		LOG_ERR("\r\necgInit FAILED");
		deviceSetErrorState();
	}
#else	//MENNEN_ALGO


#ifdef IWDG_ENABLE	// @ BSP.h

	/*if (bspWdInit() != BSP_RESULT_OK) // Inite Watch Dog
	{
		LOG_ERR("WD Init FAILED\r\n!!");
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_WATCH_DOG");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_WATCH_DOG);
	   	return RESULT_ERROR;
	}*/

#endif	//IWDG_ENABLE

	panicButton = xEventGroupCreate(); // used for panic button priority
    if( panicButton == NULL )
    {
		LOG_ERR("\r\npanicButton Event Group creation failed\r\n!!");
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_OS");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_OS);
	   	return RESULT_ERROR;
    }

	//Init task
	osThreadDef(deviceTask, deviceTask, osPriorityNormal, 0, DEVICE_STACK_SIZE);
	deviceTaskHandle = osThreadCreate(osThread(deviceTask), NULL);
	if (!deviceTaskHandle)
	{
		LOG_ERR("\r\nDevice Task Init FAILED\r\n!!");
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_OS");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_OS);
	   	return RESULT_ERROR;
	}

     	osThreadDef(deviceHourlyTask, deviceHourlyTask, osPriorityNormal, 0, DEVICE_STACK_SIZE);
	deviceHourlyTaskHandle = osThreadCreate(osThread(deviceHourlyTask), NULL);
	if (!deviceHourlyTaskHandle)
	{
		LOG_ERR("\r\n DeviceHourlyTask  FAILED\r\n!!");
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_OS");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_OS);
	   	return RESULT_ERROR;
	}



#endif // MENNEN_ALGO
	return RESULT_OK;
}


//static DEVICE_RESULT_ENUM prvDeviceFlashTest(void)
//{
//	FLASH_RES 	res = 0;
//
//	res |= flashEraseData(DEVICE_FLASH_PAGE_ECG_RESP_CONFIG, 1);
//
//
//
//	return DEVICE_RESULT_OK;
//}

static DEVICE_RESULT_ENUM prvDeviceSaveEcgConfig(ECG_RESP_CONFIG_OBJECT_STRUCT *ecg_conf)
{
	if (ecg_conf == NULL)
		return DEVICE_RESULT_WRONG_PARAM;

	FLASH_RES 	res;
	res = flashEraseData(DEVICE_FLASH_PAGE_ECG_RESP_CONFIG, 1);
	if (res != FLASH_OK)
	{
		LOG("\r\nprvDeviceSaveEcgConfig error erasing flash page %d", res);
		return DEVICE_RESULT_FLASH_ERROR;
	}
	res = flashWriteData(DEVICE_FLASH_PAGE_ECG_RESP_CONFIG, sizeof(ECG_RESP_CONFIG_OBJECT_STRUCT), (uint8_t *)ecg_conf);
	if (res != FLASH_OK)
	{
		LOG("\r\nprvDeviceSaveEcgConfig error writing to Flash %d", res);
		return DEVICE_RESULT_FLASH_ERROR;
	}

	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceEcgConfigResetToDefaults()
{
	ECG_RESP_CONFIG_OBJECT_STRUCT ecg_conf;

	DEVICE_RESULT_ENUM ret_val = DEVICE_RESULT_OK;

	if (ecgGetConfig(&ecg_conf) != ECG_RESULT_OK)
		return DEVICE_RESULT_ECG_ERROR;

	ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_ENUM meas_en = ecg_conf.general_config.measurement_enable;

	if (meas_en == ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_ON)
		ecgStop();

	// device_mode_conf is loaded from SD card upon power up
	ecgSetDefaultParams(device_mode_conf.device_mode, device_mode_conf.pace_maker_enable);

	if (ecgGetConfig(&ecg_conf) != ECG_RESULT_OK)
		return DEVICE_RESULT_ECG_ERROR;

	if (prvDeviceSaveEcgConfig(&ecg_conf) != DEVICE_RESULT_OK)
		ret_val = DEVICE_RESULT_FLASH_ERROR;

	if (ecg_conf.general_config.measurement_enable == ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_ON)
	{
		if (prvDeviceEcgStart() != DEVICE_RESULT_OK)
		{
			ret_val =  DEVICE_RESULT_ECG_ERROR;
		}
	}

	return ret_val;
}

static DEVICE_RESULT_ENUM prvDeviceLoadEcgConfig()
{
	ECG_RESP_CONFIG_OBJECT_STRUCT ecg_conf;

	FLASH_RES res = flashReadData(DEVICE_FLASH_PAGE_ECG_RESP_CONFIG, sizeof(ecg_conf), (uint8_t *)&ecg_conf);

	if (res != FLASH_OK)
	{
		return DEVICE_RESULT_FLASH_ERROR;
	}

	// TODO: change object validation
	if ( ecg_conf.general_config.compression > ECG_RESP_GENERAL_CONFIG_SIGNAL_COMPRESSION_MAX )
	{
		DEVICE_RESULT_ENUM dev_res = prvDeviceEcgConfigResetToDefaults();
		if (dev_res != DEVICE_RESULT_OK)
			return dev_res;
	}
	else
	{
		ecgSetConfig(&ecg_conf);
	}

	if (ecg_conf.general_config.measurement_enable == ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_ON)
	{
		if (prvDeviceEcgStart() != DEVICE_RESULT_OK)
		{
			return DEVICE_RESULT_ECG_ERROR;
		}
	}

	return DEVICE_RESULT_OK;
}

void deviceHourlyTask(void const * argument)
{
    osEvent             	ret;
    DEVICE_RESULT_ENUM 		dev_res;
    RESULT_E res;

    while (1)
    {
        ret = osMessageGet(deviceQueueHourlyHandle, osWaitForever);

        if (ret.status == osEventMessage)
        {
            switch (ret.value.v)//Event
            {
                case TRY_TO_CONNECT_BLE_FOR_SEND_HOURLY:
                {
                  if(PBS_CONNECTION_STATE_CONNECTED == pbsGetBleConnectionState())
                  {
                     osMessagePut(deviceQueueHourlyHandle, (uint32_t)DEVICE_EVENT_START_READ_LAST_10_MIN, 0);
					 //goes on only after connection established
                  }
                  else
                  {
                    //TODO other tasks run while this task sleep:
					LOG("BLE is not yet connected\r\n");
                    osDelay(2000);
                    osMessagePut(deviceQueueHourlyHandle, (uint32_t)TRY_TO_CONNECT_BLE_FOR_SEND_HOURLY, 0);
                    //count 1 min and finsih
                  }
                }
                break;
                case DEVICE_EVENT_START_READ_LAST_10_MIN:
                {
                    //read last 10 min:
					osTimerStop(ble_holter_timer);
                    res = prvPbsReadLastMinutes(BLE_HOURLY_MINUTES_TO_SEND, false);
                }
                break;
				case TRY_TO_CONNECT_BLE_FOR_SEND_PANIC:
                {
                  if(PBS_CONNECTION_STATE_CONNECTED == pbsGetBleConnectionState())
                  {
                     osMessagePut(deviceQueueHourlyHandle, (uint32_t)DEVICE_EVENT_START_READ_PANIC_INFO, 0);
					 //goes on only after connection established
                  }
                  else
                  {
                    //TODO other tasks run while this task sleep:
					LOG("BLE is not yet connected\r\n");
                    osDelay(2000);
                    osMessagePut(deviceQueueHourlyHandle, (uint32_t)TRY_TO_CONNECT_BLE_FOR_SEND_PANIC, 0);
                    //count 1 min and finsih
                  }
                }
                break;
                case DEVICE_EVENT_START_READ_PANIC_INFO:
                {
                    //read last 3 min:
					osTimerStop(ble_holter_timer);
                    res = prvPbsReadLastMinutes(BLE_PANIC_BUTTON_MINUTES_TO_SEND, true);
					xEventGroupClearBits( panicButton, BIT_0 );
                }
                break;
				case TRY_TO_CONNECT_BLE_FOR_DOUBLE_CLICK_SEND:
                {
                  if(PBS_CONNECTION_STATE_CONNECTED == pbsGetBleConnectionState())
                  {
                     osMessagePut(deviceQueueHourlyHandle, (uint32_t)DEVICE_EVENT_START_DOUBLE_CLICK_SEND, 0);
					 //goes on only after connection established
                  }
                  else
                  {
                    //TODO other tasks run while this task sleep:
					LOG("BLE is not yet connected\r\n");
                    osDelay(2000);
                    osMessagePut(deviceQueueHourlyHandle, (uint32_t)TRY_TO_CONNECT_BLE_FOR_DOUBLE_CLICK_SEND, 0);
                    //count 1 min and finsih
                  }
                }
                break;
                case DEVICE_EVENT_START_DOUBLE_CLICK_SEND:
                {
					doubleClickDone = true;
					osTimerStart(ble_holter_timer, BLE_DOUBLE_CLICK_HOLTER_PERIOD); // timer for ble to turn off ()
					pbsSetOutputMode(PBS_OUTPUT_TYPE_SD_BLE);
                }
                break;
            }
        }
    }
}



void deviceTask(void const * argument)
{
	osEvent             	ret;
	DEVICE_RESULT_ENUM 		dev_res;

	//---------------------------------------------
	bsp_write_pin(BSP_PIN_TYPE_BLE_UART_EN, BSP_PIN_RESET);
	prvDevicePowerOnLeds();

    temperature_timer = xTimerCreate("Temperature Timer", pdMS_TO_TICKS(DEVICE_TEMPERATURE_DEFAULT_TIME), pdTRUE, ( void * ) 0, temperatureTimerCallback );
	panic_button_timer = xTimerCreate("Panic Click Timer", pdMS_TO_TICKS(DEVICE_PANIC_BUTTON_DEFAULT_TIME), pdFALSE, ( void * ) 1, panicButtonTimerCallback );
	
	xTimerStart( temperature_timer, 0);

	prvDeviceSetDefaultLogLevels();

	//Mutex to sync between SD writing and Leds
	osMutexDef(deviceSyncSdLedMutex);
	deviceSyncSdLedMutex = osMutexCreate(osMutex(deviceSyncSdLedMutex));
	if (deviceSyncSdLedMutex == NULL)
		LOG_ERR("deviceSyncSdLedMutex init failed\r\n");


    //Init queue
    osMessageQDef(deviceQueue, DEVICE_TASK_QUEUE_SIZE, uint8_t);
    deviceQueueHandle = osMessageCreate(osMessageQ(deviceQueue), NULL);
	if (deviceQueueHandle == NULL)
		LOG_ERR("deviceQueueHandle init failed\r\n");

    osMessageQDef(deviceHourly, DEVICE_TASK_QUEUE_SIZE, uint8_t);
    deviceQueueHourlyHandle = osMessageCreate(osMessageQ(deviceHourly), NULL);
	if (deviceQueueHourlyHandle == NULL)
		LOG_ERR("deviceQueueHourlyHandle init failed\r\n");

	RESULT_E res = deviceSetSerialCommands();
	if (res != RESULT_OK)
		LOG_ERR("deviceSetSerialCommands init failed\r\n");


    //Init Leds
	if (ledsInit() != LEDS_RESULT_OK)
		LOG_ERR("leds_init failed\r\n");

	/* Init serial protocol */
//	if (serialProtocolInit() != RESULT_OK)
//		LOG_ERR("init_serial_protocol failed\r\n");
//#ifndef GMP_DEMO_CHINA

#ifndef ACCEL_ENABLED
	osDelay(200);//TEST
	//accelSleep();
	if (batMonInit() != BAT_MON_RESULT_OK)
	{
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_BATTERY_MONITOR");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_BATTERY_MONITOR);
		LOG_ERR("bat mon init FAILED\r\n");
	}

	//Init BLE
//	if (bleInit() != BLE_RESULT_OK)
//		LOG_ERR("BLE init failed\r\n");


//	osDelay(1000);//TEST
	/*	Init SD Card*/
	if (sdInit() != SD_RESULT_OK)
	{
		LOG_ERR("sdInit FAILED\r\n");
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
	}
//	uint32_t						sd_total_size, sd_free_size;
//	prvDevicePrintTime();
//	sdGetSdInfo(&sd_total_size, &sd_free_size);
//	prvDevicePrintTime();

//	prvDevicePrintTime();
//	prvDevicePrintInfo();

	if (prvDeviceCheckSdFreeSpace(DEVICE_SD_CARD_MIN_FREE_SPACE) != DEVICE_RESULT_OK)
	{
		LOG_ERR("Not enough free space on SD card\r\n");
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD_NOT_ENOUGH_FREE_SPACE");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD_NOT_ENOUGH_FREE_SPACE);
	}

//	if (bspSetWdTime(BSP_WD_TIME_4_SEC) != BSP_RESULT_OK)
//	{
//		LOG_ERR("Could not set WD time to 4 seconds\r\n");
//		deviceSetErrorState();
//	}

	if (prvDeviceConfiguration() != DEVICE_RESULT_OK)
	{
		LOG_ERR("prvDeviceConfiguration FAILED\r\n");//TODO: remove error state from inside the function
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_OS");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_OS);
	}

#if !defined (GMP_DEMO_CHINA) && !defined (SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME)
	if (bspCheckWdReset() != BSP_RESULT_OK)
	{
		LOG("\r\n!!!!!WD was set on previous startup!!!!!\r\n");
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_WATCH_DOG");
//		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_WATCH_DOG);
//	   	return RESULT_ERROR;
	}
#endif //GMP_DEMO_CHINA

//	BLE_RESULT ble_res;
//	BLE_CONFIG_STATUS ble_status = BLE_CONFIG_STATUS_INIT;
//	while (ble_status == BLE_CONFIG_STATUS_INIT)
//	{
//		bleConfigStatusGet(&ble_status);
//		osDelay(100);
//	}



	/* Init PBS*/
	if (DEVICE_MODE_HOLTER)
	{
		if (pbsInit(PBS_TX_TASK_MODE_OFF) != PBS_RESULT_OK)
			LOG_ERR("pbsInit FAILED\r\n");
	}
	else
	{
		if (pbsInit(PBS_TX_TASK_MODE_ON) != PBS_RESULT_OK)
			LOG_ERR("pbsInit FAILED\r\n");
	}

    //Init Flash
	if (flashInit() != FLASH_OK)
		LOG_ERR("flash init FAILED\r\n");

//	if (batMonInit() != BAT_MON_RESULT_OK)
//		LOG_ERR("bat mon init FAILED\r\n");

//	if (impedanceInitTask() != IMPEDANCE_RESULT_OK)
//		LOG_ERR("bat mon init FAILED\r\n");

	/* Init ECG */
	if (ecgInit() != ECG_RESULT_OK)
	{
		LOG_ERR("ecgInit FAILED\r\n");
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_ECG");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ECG);
	}
	//---------------------------------------------

//	prvDevicePrintTime();
//	prvDevicePrintInfo();
	memset(&device_info, 0, sizeof(device_info));

	prvDeviceGetDeviceInfo();

	dev_res = prvDeviceInfoSave(&device_info);
	if (dev_res != DEVICE_RESULT_OK)
	{
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
	}
	LOG("%s write %s\r\n", DEVICE_INFO_FILE_NAME, dev_res != DEVICE_RESULT_OK ? "failed" : "succeed");

	if (DEVICE_MODE_HOLTER)
	{
		dev_res = prvDeviceInfoJsonSave(&device_info);//TODO: Just in holter
		if (dev_res != DEVICE_RESULT_OK)
		{
			prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
			deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
		}
		LOG("%s write %s\r\n", DEVICE_INFO_JSON_FILE_NAME, dev_res != DEVICE_RESULT_OK ? "failed" : "succeed");
	}

#ifndef __RELEASE
	prvDeviceSetChecksum();
#endif //not __RELEASE

#else //ACCEL_ENABLED

	if (flashInit() != FLASH_OK)
		LOG_ERR("flash init FAILED\r\n");

	/* Init Accelerometer*/
	if (accelInit() != ACCEL_RESULT_OK)
		LOG_ERR("accelInit FAILED");

	prvDeviceSetChecksum();
#endif //ACCEL_ENABLED

	prvDeviceTimersInit();

	prvDeviceLogSd("VSMS Startup");

//	bsp_write_pin(BSP_PIN_TYPE_BLE_RESET, BSP_PIN_RESET);
//	accelSleep();

	//prvDeviceGetRespLastFileName();

	while (1)
	{
		ret = osMessageGet(deviceQueueHandle, osWaitForever);
		//LOG("!!!!deviceTask!!!!\r\n");
		if (ret.status == osEventMessage)
		{
			switch (ret.value.v)//Event
			{

				case DEVICE_EVENT_BUTTON_PRESSED:
				{
					LOG("Button Pressed\r\n");


					if (DEVICE_MODE_PATCH)
					{
						device_button = DEVICE_BUTTON_STATE_PRESSED;
						ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT changes = {0};

						changes.change = ECG_ARRYTHMIA_CHANGE_BUTTON;
						PBS_TIMESTAMP_STRUCT timestamp;
						BSP_RTC_TIMESTAMP time;
						BSP_RESULT_E bsp_res = bspRtcTimeGetBCD(&time);
						if (bsp_res == BSP_RESULT_OK)
						{
							memcpy(timestamp.data, time.raw, sizeof(timestamp.data));
						}
						else
						{
							memset(timestamp.data, PBS_TIMESTAMP_INVALID_PATTERN, sizeof(timestamp.data));
						}

						pbsSaveEventEntryToSdCard(&changes, 0, &timestamp);
					}
					else
					{
						osTimerStart(manual_event_timer, DEVICE_MANUAL_EVENT_DRUATION);
					}
					pbsSendErrorEvent(PBS_GENERAL_ERRORS_EVENTS_BUTTON_PRESSED);
				}
				break;

				case DEVICE_EVENT_DOUBLE_CLICK:
                case DEVICE_EVENT_SEND_BLE_EVERY_HOUR:
				{
					if((DEVICE_EVENT_DOUBLE_CLICK == ret.value.v) && (doubleClickDone == true))
					{
						LOG("Double click can only be enabled once.\r\n");
						break;
					}

					BLE_CONFIG_STATUS ble_config_status;
					bleConfigStatusGet(&ble_config_status);

					// BLE may be enable by double click or hourly periodic event:
					if (ble_config_status == BLE_CONFIG_STATUS_WORKING_ADV_OFF)
					{
						bleAdvMode(BLE_ADV_ON); //turn on ble advertise

						if(DEVICE_EVENT_SEND_BLE_EVERY_HOUR == ret.value.v)
						{
							//send event to hourly task:
							LOG("send EGC last 10 min to BLE periodic started\r\n");
							//send event to start ble
							osTimerStart(ble_holter_timer,BLE_HOLTER_TIMER_PERIOD); // timer for ble to turn off ()
							osMessagePut(deviceQueueHourlyHandle, (uint32_t)TRY_TO_CONNECT_BLE_FOR_SEND_HOURLY, 0);
						}
						else
						{
							LOG("Connecting to BLE for first and only double click event\r\n");
							osMessagePut(deviceQueueHourlyHandle, (uint32_t)TRY_TO_CONNECT_BLE_FOR_DOUBLE_CLICK_SEND, 0);
						}
					}
					else // if BLE is bussy, do not interfere
					{
						LOG("BLE already enabled\r\n");
					}
				}
				break;

				case DEVICE_EVENT_BLE_TRUN_OFF:
				{
					pbsSetOutputMode(PBS_OUTPUT_TYPE_SD_ONLY);

					bleAdvMode(BLE_ADV_OFF);

					LOG("BLE time out, shuting down\r\n");
				}
				break;

//				case DEVICE_EVENT_BUTTON_RELEASED:
//				{
//					LOG("Button Released\r\n");
//					pbsSendErrorEvent(PBS_ERROR_EVENT_BUTTON_RELEASED);
//				}
//				break;

//				case DEVICE_EVENT_LOW_BATTERY:
//				{
//					//LOG("Low Battery\r\n");
////					prvDeviceSetDeviceStates(DEVICE_CONNECTION_MAX, DEVICE_STATE_ERROR);
//					leds_set_state(LEDS_STATE_LOW_BATTERY, LEDS_DEBUG_MODE_ON);
//					leds_state_battery = DEVICE_LEDS_BATTERY_LOW_BATTERY;
//					prvDeviceLogSd("PBS_GENERAL_ERRORS_EVENTS_LOW_BATTERY");
//					pbsSendErrorEvent(PBS_GENERAL_ERRORS_EVENTS_LOW_BATTERY);
//				}
//				break;

				case DEVICE_EVENT_CRIT_LOW_BATTERY:
				{
//					LOG("Critically Low Battery\r\n");
					prvDeviceLogSd("PBS_GENERAL_ERRORS_EVENTS_CRITICALLY_LOW_BATTERY");
					prvDeviceSetDeviceStates(DEVICE_CONNECTION_MAX, DEVICE_STATE_ERROR);
					leds_set_state(LEDS_STATE_CRITICALLY_LOW_BATTERY, LEDS_DEBUG_MODE_ON);
					pbsSendErrorEvent(PBS_GENERAL_ERRORS_EVENTS_CRITICALLY_LOW_BATTERY);
					osDelay(1000);
				}
				break;

				case DEVICE_EVENT_IDLE_BATTERY:
				{
					prvDeviceSetState(DEVICE_STATE_IDLE);
				}
				break;

				case DEVICE_EVENT_TEMPERATURE_OK:
//					LOG("Temperature: %.02fC\r\n", current_temperature);
					ecgTemperatureSet(ECG_TEMPERATURE_SET_OK);
					break;

				case DEVICE_EVENT_TEMPERATURE_HIGH_SHUT_DOWN:
				{//Device state need to be set to Contact Detection
					ecgTemperatureSet(ECG_TEMPERATURE_SET_OUT_OF_RANGE);
					LOG("Temperature HIGH: %.02fC\r\n", current_temperature);
					prvDeviceLogSd("PBS_GENERAL_ERRORS_EVENTS_DEVICE_TEMPERATURE_TOO_HIGH");
					pbsSendErrorEvent(PBS_GENERAL_ERRORS_EVENTS_DEVICE_TEMPERATURE_TOO_HIGH);
					leds_set_state(LEDS_STATE_RECORDING, LEDS_DEBUG_MODE_OFF);
					leds_set_state(LEDS_STATE_CONTACT_DETECTION_PHASE, LEDS_DEBUG_MODE_ON);
					//Stop ECG if in measurement
				}
				break;

				case DEVICE_EVENT_TEMPERATURE_LOW_SHUT_DOWN:
				{//Device state need to be set to Contact Detection
					ecgTemperatureSet(ECG_TEMPERATURE_SET_OUT_OF_RANGE);
					LOG("Temperature LOW: %.02fC\r\n", current_temperature);
					prvDeviceLogSd("PBS_GENERAL_ERRORS_EVENTS_DEVICE_TEMPERATURE_TOO_LOW");
					pbsSendErrorEvent(PBS_GENERAL_ERRORS_EVENTS_DEVICE_TEMPERATURE_TOO_LOW);
					//Stop ECG if in measurement
				}
				break;

				case DEVICE_EVENT_GMP_CONNECTED:
				{
					BLE_CONFIG_STATUS ble_config_status;
					bleConfigStatusGet(&ble_config_status);
					if ((ble_config_status == BLE_CONFIG_STATUS_WORKING_ADV_ON) || (ble_config_status == BLE_CONFIG_STATUS_WORKING_ADV_OFF))
					{
						LOG("GMP Connected\r\n");
	//					//Before this Led need to check if Recording or in Contact detection phase
						prvDeviceSetDeviceStates(DEVICE_CONNECTION_CONNECTED, DEVICE_STATE_MAX);

						pbsSetConnectionState(PBS_CONNECTION_STATE_CONNECTED);
#ifdef VSMS_EXTENDED_HOLTER_MOCKUP
						pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, (ECG_LEAD_STATUS_ENUM)0x03, &impedance_results);
#else
						pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, curr_lead_status, &impedance_results);
#endif //VSMS_EXTENDED_HOLTER_MOCKUP
					}
				}
				break;

				case DEVICE_EVENT_GMP_DISCONNECTED:
				{
					BLE_CONFIG_STATUS ble_config_status;
					bleConfigStatusGet(&ble_config_status);
					if ((ble_config_status == BLE_CONFIG_STATUS_WORKING_ADV_ON) || (ble_config_status == BLE_CONFIG_STATUS_WORKING_ADV_ON))
					{
						LOG("GMP Disconnected\r\n");
						prvDeviceSetDeviceStates(DEVICE_CONNECTION_DISCONNECTED, DEVICE_STATE_MAX);
						pbsSetConnectionState(PBS_CONNECTION_STATE_DISCONNECTED);
					}
//					ecg_real_time_config.real_time_enable = ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_DISABLE;   // commented out for testings
				}
				break;

				case DEVICE_EVENT_WRITE_DEVICE_ID:
				{
					char ble_name[BLE_DEVICE_NAME_MAX_LENGTH +1] = {0};
					if (prvDeviceSnSet(device_id) == RESULT_OK)
					{
						strncpy(ble_name, (char*)&device_id[DEVICE_SN_LENGTH - BLE_DEVICE_NAME_MAX_LENGTH], BLE_DEVICE_NAME_MAX_LENGTH);
						if (prvDeviceSetBleName(ble_name) != DEVICE_RESULT_OK)
							LOG("BLE name writing failed\r\n");
						else
							LOG("BLE name writing succeeded\r\n");
					}

				}
				break;

				case DEVICE_EVENT_BLE_BIT_FAILED:
				{
					prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_BLE");
					deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_BLE);
				}
				break;

				case DEVICE_EVENT_IMPEDANCE_TIME:
				{
					ecgImpedanceStartWithRecovery();
				}
				break;

				case DEVICE_EVENT_CHECK_ARRYTHMIAS_STATUS:
				{
					ECG_ALGO_ARRYTHMIA_EVENT_HEADER_STRUCT arrh_bitmap = {.raw = 0};
					ecgGetArrh(&arrh_bitmap);
					pbsCheckArrhythmia(arrh_bitmap);
				}
				break;

				case DEVICE_EVENT_ECG_BIT_FAILED:
				{
					prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_ECG");
					deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ECG);
				}
				break;

				default:
					LOG("Device Task Unknown event type %d", ret.value.v);
				break;
			}
		}
	}

}

void temperatureTimerCallback(TimerHandle_t xTimer)
{
	if (bspReadTemp(&current_temperature) == BSP_RESULT_OK)
	{
		if ((temp_state != TEMPERATURE_TOO_HIGH) && (current_temperature > GMP_TEMPERATURE_MAX) && (current_temperature < 300))
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_TEMPERATURE_HIGH_SHUT_DOWN, 0);
			temp_state = TEMPERATURE_TOO_HIGH;
		}
		else if ((temp_state != TEMPERATURE_TOO_LOW) && (current_temperature < GMP_TEMPERATURE_MIN) && (current_temperature < 300))
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_TEMPERATURE_LOW_SHUT_DOWN, 0);
			temp_state = TEMPERATURE_TOO_LOW;
		}
		else if ((temp_state != TEMPERATURE_OK) &&(current_temperature > GMP_TEMPERATURE_MIN) && (current_temperature < GMP_TEMPERATURE_MAX))
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_TEMPERATURE_OK, 0);
			temp_state = TEMPERATURE_OK;
		}
		LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_3,"Temperature: %.02fC\r\n", current_temperature);
	}
	else
		LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_3,"Temperature is not available yet\r\n");

}

void shutdownTimertemperatureCallback(TimerHandle_t xTimer)
{

}

void bleHolterTimerCallback(void const *n)
{
//	pbsSetOutputMode(PBS_OUTPUT_TYPE_SD_ONLY);
//
////	blePowerSet(BLE_POWER_OFF);
//	bleAdvMode(BLE_ADV_OFF);
//
//	LOG("BLE time out, shuting down\r\n");

	osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_BLE_TRUN_OFF, 1000);
}

// read Egc data from sdcard every hour last x minutes:
void readEgcFromSdToBleHourlyCallback(void const *n)
{
        //send event to start ble
        osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_SEND_BLE_EVERY_HOUR, 0);
}

void ecgFirstRunTimerCallback(void const *n)
{
	device_1st_ecg_run_flag = FALSE;

	osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_CHECK_ARRYTHMIAS_STATUS, 0);

	LOG("First ECG run completed\r\n");
}

void ledTimerCallback(void const *n)
{
	ledsSetLedStateInterval(LEDS_STATE_RECORDING, LEDS_TIME_SUPER_SLOW);
	//leds_set_state(LEDS_STATE_RECORDING, LEDS_DEBUG_MODE_ON);
	leds_set_state(LEDS_STATE_OFF, LEDS_DEBUG_MODE_ON);
	LOG("LED timer completed, switching to slow blink in lead on\r\n");
}

void manualEventTimerCallback(void const *n)
{
	uint8_t free_space;
	if (sdGetSdWriteBufferFree(&free_space) == SD_RESULT_OK)
	{
		if (free_space > 40)
			device_button = DEVICE_BUTTON_STATE_PRESSED;
		else
			osTimerStart(manual_event_timer, DEVICE_MANUAL_EVENT_DRUATION);
	}
	else
	{
		//TODO: Maybe error state?
	}


}

void doubleClickTimerCallback(void const *n)
{
	device_double_click_counter = 0;
}

void panicButtonTimerCallback(TimerHandle_t xTimer)
{	
	LOG("entered panicButtonTimerCallback\r\n");
	osTimerStart(ble_holter_timer,BLE_HOLTER_TIMER_PERIOD); // timer for ble to turn off ()
	osMessagePut(deviceQueueHourlyHandle, (uint32_t)TRY_TO_CONNECT_BLE_FOR_SEND_PANIC, 0);
}

static RESULT_E prvDeviceSetConnectionState(DEVICE_CONNECTION_STATE new_state)
{
	if (new_state >= DEVICE_CONNECTION_MAX)
		return RESULT_WRONG_PARAMTER;

	if (device_conn_state == new_state)
		return RESULT_OK;

	device_conn_state = new_state;

	return RESULT_OK;
}
static RESULT_E prvDeviceSetState(DEVICE_STATE_TYPE new_state)
{
	if (new_state >= DEVICE_STATE_MAX)
		return RESULT_WRONG_PARAMTER;

	if (device_state == new_state)
		return RESULT_OK;


	switch (device_state)
	{
	case DEVICE_STATE_MAX:
	case DEVICE_STATE_IDLE:
	case DEVICE_STATE_CONTACT_DETECTION:
	case DEVICE_STATE_RECORDING:
		device_state = new_state;
		break;
	case DEVICE_STATE_ERROR:
		// CAN't GET OUT OF ERROR STATE
		LOG("\r\nCAN't GET OUT OF ERROR STATE\r\n");
		//ecgStop();
		break;
	default:
		return RESULT_ERROR;
	}

	return RESULT_OK;
}


/*
 * prvDeviceSetDeviceStates
 *
 * DEVICE_STATE_MAX & DEVICE_CONNECTION_MAX - indicate don't set
 *
 * */
static RESULT_E prvDeviceSetDeviceStates(DEVICE_CONNECTION_STATE new_conn_state, DEVICE_STATE_TYPE new_state)
{
	if ( (device_state == new_state) && (device_conn_state == new_conn_state) )
		return RESULT_OK;
	if ( (device_state == new_state) && (new_conn_state == DEVICE_CONNECTION_MAX) )
		return RESULT_OK;
	if ( (device_state == DEVICE_STATE_MAX) && (device_conn_state == new_conn_state) )
		return RESULT_OK;

	RESULT_E res = RESULT_OK;

	if (new_state != DEVICE_STATE_MAX)
	{
		res = prvDeviceSetState(new_state);
		if (res != RESULT_OK)
			return res;
	}

	if (new_conn_state != DEVICE_CONNECTION_MAX)
	{
		res = prvDeviceSetConnectionState(new_conn_state);
		if (res != RESULT_OK)
			return res;
	}

	if (device_state == DEVICE_STATE_ERROR)
	{
		// indicate error
		LOG("\r\n-----IN ERROR STATE-----\r\n");
		leds_set_state(LEDS_STATE_ERROR_STATE, LEDS_DEBUG_MODE_ON);
		ecgStop();
	}
	else
	{

		if ((DEVICE_MODE_PATCH || DEVICE_MODE_TESTER) && (device_conn_state == DEVICE_CONNECTION_DISCONNECTED))
		{
			leds_set_state(LEDS_STATE_BLE_ADVERTISEMENT, LEDS_DEBUG_MODE_ON);
		}
		else if (((device_conn_state == DEVICE_CONNECTION_CONNECTED) || DEVICE_MODE_HOLTER) && (leds_state_battery != DEVICE_LEDS_BATTERY_LOW_BATTERY))
		{
			switch (device_state)
			{
//			case DEVICE_STATE_IDLE:
//				leds_set_state(LEDS_STATE_POWER_ON, LEDS_DEBUG_MODE_ON);
//				break;
			case DEVICE_STATE_CONTACT_DETECTION:
				leds_set_state(LEDS_STATE_RECORDING, LEDS_DEBUG_MODE_OFF);
				leds_set_state(LEDS_STATE_CONTACT_DETECTION_PHASE, LEDS_DEBUG_MODE_ON);
				osTimerStop(led_timer);

				static uint8_t led_first_run = TRUE;
				if (led_first_run)
				{
					ledsSetLedStateInterval(LEDS_STATE_RECORDING, LEDS_TIME_HIGH);
					led_first_run = FALSE;
				}
				else
					ledsSetLedStateInterval(LEDS_STATE_RECORDING, LEDS_TIME_MEDIUM);

				break;
			case DEVICE_STATE_RECORDING:
				leds_set_state(LEDS_STATE_CONTACT_DETECTION_PHASE, LEDS_DEBUG_MODE_OFF);
				leds_set_state(LEDS_STATE_RECORDING, LEDS_DEBUG_MODE_ON);
#ifndef VSMS_EXTENDED_HOLTER_MOCKUP
				osTimerStart(led_timer, DEVICE_LEAD_ON_LED_DRUATION);
#endif //VSMS_EXTENDED_HOLTER_MOCKUP
				break;
			case DEVICE_STATE_ERROR: // already covered, can't get here
			default:
				return RESULT_ERROR;
			}
		}
	}
	return RESULT_OK;
}




static void prvDeviceEcgDebugPrintArrythmias(ECG_ALGO_ARRYTHMIA_EVENT_STRUCT* e_arrs)
{
   if (e_arrs == NULL)
   {
      LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "\r\nprvDeviceEcgDebugPrintArrythmias error param\r\n");
      return;
   }
	// DEBUG PRINTS
	if (e_arrs->events_flags.bitmap.ventricular_asystole)		// boolean flag
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "1 : Ventricular asystole  \r\n");
	}
	if (e_arrs->events_flags.bitmap.ventricular_fibrillation)		// boolean flag
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "2 : ARR: Ventricular fibrillation \r\n");
	}
	if (e_arrs->events_flags.bitmap.ventricular_tachycardia)		// boolean flag
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "3 : Ventricular tachycardia \r\n");
	}
	if (e_arrs->runs)		// runs per minute
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "4 : Runs (complexes per minute) = %d\r\n", e_arrs->runs);
	}
	if (e_arrs->pvc)		// beats per minute
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "5 : PVC beats per minute  = %d\r\n", e_arrs->pvc);
	}
	if (e_arrs->events_flags.bitmap.pause)		// boolean flag
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "6 : Pause in ECG beats \r\n");
	}
	if (e_arrs->bigeminy)		// bigemeny complexes per minute
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "7 : Bigemeny complexes per minute  = %d\r\n", e_arrs->bigeminy);
	}
	if (e_arrs->events_flags.bitmap.bradicardia)		// bradicardia
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "8 : Bradicardia \r\n");
	}
	if (e_arrs->events_flags.bitmap.tachycardia)		// tachycardia
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "9 : Tachicardia \r\n");
	}
	if (e_arrs->events_flags.bitmap.irregular_rhythm)		// irregular rhythm
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "10: Irregular rhythm  \r\n");
	}
	if (e_arrs->events_flags.bitmap.ideoventricular_rhythm)		// Ideoventricular rhythm
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "11: Ideoventricular rhythm   \r\n");
	}
	if (e_arrs->couplet_pvcs)		// couplet of pvc's
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "12: Couplet of pvc's (complexes per minute)  = %d\r\n", e_arrs->couplet_pvcs);
	}
	if (e_arrs->triplet_pvcs)		// triplet of pvc's
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "13: Triplet of pvc's (complexes per minute)  = %d\r\n", e_arrs->triplet_pvcs);
	}
	if (e_arrs->trigeminy)		// trigemeny
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "14: Trigemeny (complexes per minute)  = %d\r\n", e_arrs->trigeminy);
	}
	if (e_arrs->r_on_t)		// R on T
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "15: R on T (complexes per minute) = %d\r\n", e_arrs->r_on_t);
	}
	if (e_arrs->premature_atrial_contraction)		// premature atrial contraction
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "16: Premature atrial contraction (compl. per minute) = %d\r\n", e_arrs->premature_atrial_contraction);
	}
	if (e_arrs->events_flags.bitmap.supravent_tachycardia)		// Supravent. Tachycardia
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "17: Supravent. Tachycardia    \r\n");
	}
	if (e_arrs->multifocal_of_pvc)		//multifocal pvc
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "18: Multifocal pvc's (complexes per minute) = %d\r\n", e_arrs->multifocal_of_pvc);
	}
	if (e_arrs->interpolated_pvc)		//interpolated pvc's
	{
		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_2, "19: Interpolated pvc's (complexes per minute) = %d\r\n", e_arrs->interpolated_pvc);
	}
}

static DEVICE_RESULT_ENUM prvDeviceEcgArrythmiasCollectionReset()
{
//	ecg_measure_time = osKernelSysTick();
	memset (&arrythmias_curr, 0, sizeof(arrythmias_curr));
	memset (&respiration_algo_curr, 0, sizeof(respiration_algo_curr));
	hr_avg_acc = hr_min = hr_max = hr_avg_cnt = ECG_INVALID_VALUE;

	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceHRAddResult(uint16_t hr)
{
	if (hr < ECG_PARAMS_HR_RESULT_MIN || hr > ECG_PARAMS_HR_RESULT_MAX)
		return DEVICE_RESULT_WRONG_PARAM;

	if (hr_min == ECG_INVALID_VALUE)
	{
		// first value after reset
		hr_avg_acc = hr;
		hr_min = hr;
		hr_max = hr;
		hr_avg_cnt++;
		return DEVICE_RESULT_OK;
	}

	hr_avg_acc += hr;
	hr_avg_cnt++;

	hr_min = min(hr, hr_min);
	hr_max = max(hr, hr_max);

	return DEVICE_RESULT_OK;
}

//static uint8_t buff_sd[2000] = {0};
//static uint32_t resp_sd_index = 0;
// Function Implementations :
static void prvDeviceEventHandler(EVENT_SOURCE_TYPE source, device_event event)
{
#ifdef G_UNIT_TEST_ENABLE
	if (is_unit_test == TRUE)
	{
		unitTestEvent(source, event);
		return;
	}
#endif //G_UNIT_TEST_ENABLE


	LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_3, "\r\nDEVICE::device_event_handler()");
	switch (source)
	{

	case EVENT_SOURCE_ECG:
	{
		LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_3, " EVENT_SOURCE_ECG: ");

		switch (event.ecg->event_type)
		{
		case ECG_EVENT_IMPEDANCE_TIME:
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_IMPEDANCE_TIME, 0);
		}
		break;
        case ECG_EVENT_INIT:
		{
            LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_3, " ECG_EVENT_INIT ");
            //device_status_bitmap.ecg_init = (event.ecg->payload.ch == RESULT_OK ? DEVICE_STATE_PASS : DEVICE_STATE_FAIL);
            //if (device_status_bitmap.ecg_init == DEVICE_STATE_FAIL)
			if (event.ecg->payloadPtr.bit_result->bit_resuls == ECG_RESP_BIT_EVENT_SUCCEED)
			{
            DEVICE_RESULT_ENUM dev_res;
				if (device_mode_conf.device_mode == ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY)
				{
					if (device_bit == PBS_DEVICE_STATUS_BIT_NO_ERROR)
					{
						dev_res = prvDeviceEcgStart();
						if (dev_res != DEVICE_RESULT_OK)
						{
							LOG("Device ECG start after BIT failed %d \r\n", dev_res);
							prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_ECG");
							deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ECG);
						}
					}
				}
				else
				{
//					dev_res = prvDeviceLoadEcgConfig();
//					if (dev_res != DEVICE_RESULT_OK)
//					{
//						LOG("Device ECG load config after BIT failed %d \r\n", dev_res);
//					}

					// TODO: until we fix RT mode, save / load of configuration object isn't needed

					dev_res = prvDeviceEcgStart();
	               	if (dev_res != DEVICE_RESULT_OK)
					{
						LOG("Device ECG start after BIT failed %d \r\n", dev_res);
					}

				}
			}
			else
			{
#ifndef MENNEN_ALGO
				LOG("ECG BIT failed\r\n");
				prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_ECG");
				deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ECG);
#endif
			}
		}
		break;


		case ECG_HOLTER_EVENT_CONTACT_DETECTION:
		{
			ECG_MEAS_CONTACT_DETECTIION_RESULTS	 * msg = event.ecg->payloadPtr.contact_det_leads;

			switch (msg->leads_state)
			{
			case ECG_LEADS_ON:
			case ECG_LEADS_OFF:
#ifndef VSMS_EXTENDED_HOLTER_MOCKUP
				prvDeviceSetDeviceStates(DEVICE_CONNECTION_MAX, DEVICE_STATE_CONTACT_DETECTION);
				break;
#endif //VSMS_EXTENDED_HOLTER_MOCKUP
			case ECG_LEADS_ON_START:
				prvDeviceSetDeviceStates(DEVICE_CONNECTION_MAX, DEVICE_STATE_RECORDING);
				break;
			}
			if ( curr_lead_status != msg->lead_status)
			{
				prvDeviceLogSd("Lead Status, previous: %d, current: %d", curr_lead_status, msg->lead_status);
				curr_lead_status = msg->lead_status;
#ifdef VSMS_EXTENDED_HOLTER_MOCKUP
				pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, (ECG_LEAD_STATUS_ENUM)0x03, &impedance_results);
#else
				pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, curr_lead_status, &impedance_results);

//				if (curr_lead_status == ECG_LEAD_STATUS_OFF)
//					prvDeviceLogSd("Contact Detection Off");
//				else if (curr_lead_status == (ECG_LEAD_STATUS_LEAD_1_ON | ECG_LEAD_STATUS_LEAD_2_ON))
//					prvDeviceLogSd("Contact Detection On");
#endif //VSMS_EXTENDED_HOLTER_MOCKUP
			}

		}
		break;
		case ECG_HOLTER_EVENT_MEASURING:
		{

			ECG_MEAS_PROGRESS_EVENT_STRUCT * msg = event.ecg->payloadPtr.real_time_ind;
			ECG_RESP_CONFIG_OBJECT_STRUCT * tp = event.ecg->test_params;

			if ( (timing_accuracy_flag == DEVICE_TIMING_ACCURACY_FLAG_RESET)  && (msg->common->ecg_index % 250 == 0) )
			{
				timing_accuracy_flag = DEVICE_TIMING_ACCURACY_FLAG_NONE;
				timing_accuracy_ostime = osKernelSysTick();
				timing_accuracy_ecg_index = msg->common->ecg_index;
				bspRtcTimeGet(&timing_accuracy_rtc_date_time.time, &timing_accuracy_rtc_date_time.date);
			}
			else if ( (timing_accuracy_flag == DEVICE_TIMING_ACCURACY_FLAG_PRINT_STAT) && (msg->common->ecg_index % 250 == 0) )
			{
				timing_accuracy_flag = DEVICE_TIMING_ACCURACY_FLAG_NONE;


				BSP_TIME_DATE_STRUCT tmp;
				uint32_t sec_count;
				bspRtcTimeGet(&tmp.time, &tmp.date);
				bspRtcTimeDiff(&timing_accuracy_rtc_date_time, &tmp, &sec_count);

				LOG("\r\nTiming accuracy: os time diff=%d [msec], rtc sec_cout=%d [sec], index time diff = %f [sec]",
						(osKernelSysTick() - timing_accuracy_ostime), sec_count,
						(float32_t)(msg->common->ecg_index - timing_accuracy_ecg_index)/250 );	// 250 = ecg Rs
			}

			if ( curr_lead_status != msg->common->lead_status)
			{
				prvDeviceLogSd("Lead Status, previous: %d, current: %d", curr_lead_status, msg->common->lead_status);
				curr_lead_status = msg->common->lead_status;
#ifdef VSMS_EXTENDED_HOLTER_MOCKUP
				pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, (ECG_LEAD_STATUS_ENUM)0x03, &impedance_results);
#else
				pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, curr_lead_status, &impedance_results);

//				if (curr_lead_status == ECG_LEAD_STATUS_OFF)
//					prvDeviceLogSd("Contact Detection Off");
//				else if (curr_lead_status == (ECG_LEAD_STATUS_LEAD_1_ON | ECG_LEAD_STATUS_LEAD_2_ON))
//					prvDeviceLogSd("Contact Detection On");
#endif //VSMS_EXTENDED_HOLTER_MOCKUP
			}



			if (tp->general_config.compression == ECG_RESP_GENERAL_CONFIG_SIGNAL_COMPRESSION_OFF)
			{
				memcpy(&signal_ecg_1, msg->ecg_lead_1, sizeof(signal_ecg_1));
				memcpy(&signal_ecg_2, msg->ecg_lead_2, sizeof(signal_ecg_2));
				ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_RAW;
			}
			else
			{
				memset(&signal_ecg_1.raw_comp, 0, sizeof(signal_ecg_1.raw_comp));
				memset(&signal_ecg_2.raw_comp, 0, sizeof(signal_ecg_2.raw_comp));

				ecgCompressBufferLen(msg->ecg_lead_1->buff.raw, msg->ecg_lead_1->buff_byte_length, ECG_SAMPLE_24_BIT_SIZE,
						signal_ecg_1.raw_comp.samples, &signal_ecg_1.raw_comp.sample_len, signal_ecg_1.raw_comp.bit_map,
						&signal_ecg_1.raw_comp.bit_map_len);

				ecgCompressBufferLen(msg->ecg_lead_2->buff.raw, msg->ecg_lead_2->buff_byte_length, ECG_SAMPLE_24_BIT_SIZE,
						signal_ecg_2.raw_comp.samples, &signal_ecg_2.raw_comp.sample_len, signal_ecg_2.raw_comp.bit_map,
						&signal_ecg_2.raw_comp.bit_map_len);

				ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_RAW_COMPRESSED;
			}

			ecg_rt_ind.ecg_lead_1 = &signal_ecg_1;
			ecg_rt_ind.ecg_lead_2 = &signal_ecg_2;

			memcpy(&ecg_rt_ind.common, msg->common, sizeof(ecg_rt_ind.common) );

			ecg_rt_ind.ecg_status = ECG_REALTIME_STATUS_RECORDING_IN_PROGRESS;

			if (device_button == DEVICE_BUTTON_STATE_PRESSED)
			{
				xEventGroupSetBits( panicButton, BIT_0 );
				BLE_CONFIG_STATUS ble_config_status;
				bleConfigStatusGet(&ble_config_status);
				osTimerStart(ble_holter_timer,BLE_HOLTER_TIMER_PERIOD); // timer for ble to turn off ()
				if (ble_config_status == BLE_CONFIG_STATUS_WORKING_ADV_OFF)
				{ 
					bleAdvMode(BLE_ADV_ON); //turn on ble advertise
				}
				xTimerStart( panic_button_timer, 0 ); // start timer for 1.5 minutes
				pbsSetOutputMode(PBS_OUTPUT_TYPE_SD_ONLY);
				ecg_rt_ind.event_type = ECG_MEAS_EVENT_TYPE_MANUAL;
				device_button = DEVICE_BUTTON_STATE_IDLE;	// clear for next time
				ledsBlink(LEDS_BITMAP_BLUE, 100);
				LOG("Manual Event saved\r\n");
				prvDeviceLogSd("Manual Event saved");
                                //set ble event for 1.5minut:...
			}

			BSP_RTC_TIMESTAMP bcd;
			BSP_RESULT_E bsp_res = bspRtcTimeGetBCD(&bcd);
			if (bsp_res == BSP_RESULT_OK)
				memcpy(ecg_rt_ind.base_timestamp, bcd.raw, sizeof(ecg_rt_ind.base_timestamp) );
			else// in case of BSP RTC error
				memset(ecg_rt_ind.base_timestamp, 0xff, sizeof(ecg_rt_ind.base_timestamp) );

//			LOG("Device Sample Index: %ld, %02X:%02X:%02X:%d%02d %02X/%02X/%02X\r\n", ecg_rt_ind.common.ecg_index,
//				ecg_rt_ind.base_timestamp[3],ecg_rt_ind.base_timestamp[4],ecg_rt_ind.base_timestamp[5],
//				ecg_rt_ind.base_timestamp[7],ecg_rt_ind.base_timestamp[6],
//				ecg_rt_ind.base_timestamp[2],ecg_rt_ind.base_timestamp[1],ecg_rt_ind.base_timestamp[0]);
			pbsEcgHolterMeasurementRealtimeProgressInd(&ecg_rt_ind);

			ecg_rt_ind.event_type = ECG_MEAS_EVENT_TYPE_MAX;	// clear until next button press

			for (uint16_t i = 0; i < ECG_BUFFER_LENGTH; i++)
			{
				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_4, "\r\n%d\t%d\t%02X", ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->ecg_lead_1,i),
						ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->ecg_lead_2, i), msg->common->lead_status);
				if (msg->common->pace_maker.index_start != PM_POSITION_NOT_ASSIGNED)
				{
					LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_4, ", pace maker start: %d", msg->common->pace_maker.index_start);
					if (msg->common->pace_maker.index_stop != PM_POSITION_NOT_ASSIGNED)
					{
						LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_4, ", stop: %d", msg->common->pace_maker.index_stop);
					}
				}
			}

		}
			break;
		case ECG_HOLTER_EVENT_ERROR:
		{
			osMessagePut(deviceQueueHandle, (uint32_t)DEVICE_EVENT_ECG_BIT_FAILED, 0);
		}
		break;
      case ECG_EVENT_CONTACT_DETECTION:
            {
				prvDeviceSetDeviceStates(DEVICE_CONNECTION_MAX, DEVICE_STATE_CONTACT_DETECTION);

                LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, " \r\nECG_EVENT_CONTACT_DETECTION");
//                	pbs_snd_ecg_contact_status_ind(PBS_IND_MEASURE_CONTACT_STATUS_CONTACT_LEADS_OFF);
                ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT tmp;
				ECG_MEAS_PROGRESS_EVENT_STRUCT * msg = event.ecg->payloadPtr.real_time_ind;	//ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT
				ECG_RESP_CONFIG_OBJECT_STRUCT * tp = event.ecg->test_params;


				// restart timer for arrythmias
				if (msg->common->lead_status == (ECG_LEAD_STATUS_LEAD_1_ON | ECG_LEAD_STATUS_LEAD_2_ON) )
				{
					prvDeviceEcgArrythmiasCollectionReset();
				}

				if (tp->general_config.on_lead_off_event == ECG_RESP_GENERAL_CONFIG_ON_LEAD_OFF_EVENT_IGNORE)
				{
	                LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, ":\tON_LEAD_OFF_EVENT_IGNORE");
					return;
				}
				if ( (osKernelSysTick() - device_time_last_sent_ecg_contact_ind) > ECG_LEAD_OFF_IND_TIME_DIFF_MSEC)
				{
					device_time_last_sent_ecg_contact_ind = osKernelSysTick();
					memcpy(&tmp.common, msg->common, sizeof(tmp.common) );
					tmp.ecg_status = ECG_REALTIME_STATUS_LEAD_DETECTION;
					if (ecg_real_time_config.real_time_enable == ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_ENABLE)
					{
						pbsSendECGMeasurementRealtimeProgressInd(&tmp, 0);
					}
//					else
//					{
//						curr_lead_status = msg->common->lead_status;
//						pbsSendECGMeasurementContactInd(curr_lead_status);
//					}
				}
				else if (curr_lead_status != msg->common->lead_status)
				{
					prvDeviceLogSd("Lead Status, previous: %d, current: %d", curr_lead_status, msg->common->lead_status);
					curr_lead_status = msg->common->lead_status;
					pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, curr_lead_status, &impedance_results);
				}

				if (xTimerIsTimerActive(ecg_first_run_timer) && (device_1st_ecg_run_flag))
					osTimerStop(ecg_first_run_timer);

            }
            break;


            // TODO:ECG processing
            // Patch ECG implementation
            //		RAW: save (/compress) data if required,
            // 		Filtered: save (/compress) data if required
            //		dec_with_results:
            //				send data via PBS
            //			and/or
            //				save to memory card
            case ECG_EVENT_REAL_TIME_PROGRESS_RAW:	// 500 Hz
            {
            	ECG_MEAS_PROGRESS_EVENT_STRUCT * msg = event.ecg->payloadPtr.real_time_ind;
            	ECG_RESP_CONFIG_OBJECT_STRUCT	* tp = event.ecg->test_params;

				prvDeviceSetDeviceStates(DEVICE_CONNECTION_MAX, DEVICE_STATE_RECORDING);

    			for (uint16_t i = 0; i < ECG_BUFFER_LENGTH; i++)
    			{
      				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_4, "\r\n%d\t%d\t%d\t%02X", ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->ecg_lead_1,i),
    						ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->ecg_lead_2, i),
    						ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->respiration, i), msg->common->lead_status);
    			}
//				uint16_t index = 0;
//    			for (uint16_t i = 0; i < ECG_BUFFER_LENGTH; i++)
//    			{
//      				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_4, "\r\n%d\t%d\t%d\t%02X", ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->ecg_lead_1,i),
//    						ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->ecg_lead_2, i),
//    						ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->respiration, i), msg->common->lead_status);
//					sprintf(&buff_sd[index], "%07d\r\n", ECG_MEAS_PROGRESS_STRUCT_GET_24BIT_SAMPLE(msg->respiration, i));
//					index += 9;
//
//
//    			}
//
//				sdWriteDataToFile(device_current_resp_file, buff_sd, strlen(buff_sd), &resp_sd_index);
//				//resp_sd_index += strlen(buff_sd);
    			// added functionality when Mennen filters were not working within regulations spec.
    			if ( (tp->ecg_output.output_frequency == ECG_PARAMS_OUTPUT_FREQ_250) && (tp->ecg_output.output_filter == ECG_FILTER_STATUS_OFF) )
    			{
    				ecgDownsampleRaw500to250(msg->ecg_lead_1, msg->ecg_lead_2, &signal_ecg_1, &signal_ecg_2);
//    				ecgDownsampleRaw500to250(msg->ecg_lead_2, &signal_ecg_2);

					ecgCompressBufferLen(signal_ecg_1.raw.buff.raw, signal_ecg_1.raw.buff_byte_length, ECG_SAMPLE_24_BIT_SIZE,
							signal_ecg_1.raw_comp.samples, &signal_ecg_1.raw_comp.sample_len,
							signal_ecg_1.raw_comp.bit_map, &signal_ecg_1.raw_comp.bit_map_len);

					ecgCompressBufferLen(signal_ecg_2.raw.buff.raw, signal_ecg_2.raw.buff_byte_length, ECG_SAMPLE_24_BIT_SIZE,
							signal_ecg_2.raw_comp.samples, &signal_ecg_2.raw_comp.sample_len,
							signal_ecg_2.raw_comp.bit_map, &signal_ecg_2.raw_comp.bit_map_len);

					ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_RAW_COMPRESSED;
    			}
//				memset(buff_sd, 0, sizeof(buff_sd));
				if (ecg_real_time_config.real_time_enable == ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_DISABLE)
            		return;

            	if ( (tp->ecg_output.output_frequency == ECG_PARAMS_OUTPUT_FREQ_500) && (tp->ecg_output.output_filter == ECG_FILTER_STATUS_OFF) )
            	{
            		if (tp->general_config.compression == ECG_RESP_GENERAL_CONFIG_SIGNAL_COMPRESSION_OFF)
            		{
            			memcpy(&signal_ecg_1, msg->ecg_lead_1, sizeof(signal_ecg_1));
            			memcpy(&signal_ecg_2, msg->ecg_lead_2, sizeof(signal_ecg_2));
            			ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_RAW;
					}
            		else
            		{
						memset(&signal_ecg_1.raw_comp, 0, sizeof(signal_ecg_1.raw_comp));
						memset(&signal_ecg_2.raw_comp, 0, sizeof(signal_ecg_2.raw_comp));

						ecgCompressBufferLen(msg->ecg_lead_1->buff.raw, msg->ecg_lead_1->buff_byte_length, ECG_SAMPLE_24_BIT_SIZE,
								signal_ecg_1.raw_comp.samples, &signal_ecg_1.raw_comp.sample_len, signal_ecg_1.raw_comp.bit_map,
								&signal_ecg_1.raw_comp.bit_map_len);

						ecgCompressBufferLen(msg->ecg_lead_2->buff.raw, msg->ecg_lead_2->buff_byte_length, ECG_SAMPLE_24_BIT_SIZE,
								signal_ecg_2.raw_comp.samples, &signal_ecg_2.raw_comp.sample_len, signal_ecg_2.raw_comp.bit_map,
								&signal_ecg_2.raw_comp.bit_map_len);

						ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_RAW_COMPRESSED;
					}
            	}

            	if ( (tp->respiration_output.output_frequency == ECG_PARAMS_RESPIRATION_OUTPUT_500HZ_COMPRESSED) &&
					(tp->respiration_output.bandpass_filter == ECG_PARAMS_RESPIRATION_BANDPASS_FILTER_OFF) )
            	{
					memset(&signal_resp.raw_comp, 0, sizeof(signal_resp.raw_comp));

					ecgCompressBufferLen(msg->respiration->buff.raw, msg->respiration->buff_byte_length, ECG_SAMPLE_24_BIT_SIZE,
							signal_resp.raw_comp.samples, &signal_resp.raw_comp.sample_len, signal_resp.raw_comp.bit_map,
							&signal_resp.raw_comp.bit_map_len);
					ecg_rt_ind.resp_type = ECG_MEAS_RESPIRATION_SIGNAL_500HZ_RAW_COMPRESSED;
				}
				// signal_ecg_1, signal_ecg_2, signal_resp - to be used for compression / saving


            }
            break;

            case ECG_EVENT_REAL_TIME_PROGRESS_FILTERED:	// 500 Hz
            {
            	ECG_MEAS_PROGRESS_EVENT_STRUCT * msg = event.ecg->payloadPtr.real_time_ind;
            	ECG_RESP_CONFIG_OBJECT_STRUCT	* tp = event.ecg->test_params;

    			for (uint16_t i = 0; i < ECG_BUFFER_LENGTH; i++)
    			{
    				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_3, "%d\t%d\t%d\r\n", i, msg->ecg_lead_1->buff.sh[i], msg->ecg_lead_2->buff.sh[i], msg->respiration->buff.sh[i]);
//    				LOG("%6d\r\n", msg->ecg_lead_2->buff.sh[i] );
//    				LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_3, "%6d\r\n", msg->ecg_lead_1->buff.sh[i]);
    			}

				if (ecg_real_time_config.real_time_enable == ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_DISABLE)
            		return;

            	if ( (tp->ecg_output.output_frequency == ECG_PARAMS_OUTPUT_FREQ_500) && (tp->ecg_output.bandpass_filter != ECG_PARAMS_BANDPASS_FILTER_OFF) )
            	{
            		if (tp->general_config.compression == ECG_RESP_GENERAL_CONFIG_SIGNAL_COMPRESSION_OFF)
            		{
            			memcpy(&signal_ecg_1, msg->ecg_lead_1, sizeof(signal_ecg_1));
            			memcpy(&signal_ecg_2, msg->ecg_lead_2, sizeof(signal_ecg_2));
            			ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_FILTERED;
					}
            		else
            		{
						memset(&signal_ecg_1.filtered_comp, 0, sizeof(signal_ecg_1.filtered_comp));
						memset(&signal_ecg_2.filtered_comp, 0, sizeof(signal_ecg_2.filtered_comp));

						ecgCompressBufferLen((uint8_t *) msg->ecg_lead_1->buff.sh, msg->ecg_lead_1->buff_byte_length, ECG_FILTERED_SAMPLE_SIZE,
								signal_ecg_1.filtered_comp.samples, &signal_ecg_1.filtered_comp.sample_len, signal_ecg_1.filtered_comp.bit_map,
								&signal_ecg_1.filtered_comp.bit_map_len);

						ecgCompressBufferLen((uint8_t *) msg->ecg_lead_2->buff.sh, msg->ecg_lead_2->buff_byte_length, ECG_FILTERED_SAMPLE_SIZE,
								signal_ecg_2.filtered_comp.samples, &signal_ecg_2.filtered_comp.sample_len, signal_ecg_2.filtered_comp.bit_map,
								&signal_ecg_2.filtered_comp.bit_map_len);

						ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_500HZ_FILTERED_COMPRESSED;
					}
            	}

				if ( (tp->respiration_output.output_frequency == ECG_PARAMS_RESPIRATION_OUTPUT_500HZ_COMPRESSED) &&
					(tp->respiration_output.bandpass_filter != ECG_PARAMS_RESPIRATION_BANDPASS_FILTER_OFF))
				{
					memset(&signal_resp.filtered_comp, 0, sizeof(signal_resp.filtered_comp));

					ecgCompressBufferLen((uint8_t *)msg->respiration->buff.sh, msg->respiration->buff_byte_length, ECG_FILTERED_SAMPLE_SIZE,
							signal_resp.filtered_comp.samples, &signal_resp.filtered_comp.sample_len, signal_resp.filtered_comp.bit_map,
							&signal_resp.filtered_comp.bit_map_len);
					ecg_rt_ind.resp_type = ECG_MEAS_RESPIRATION_SIGNAL_50HZ_FILTERED_COMPRESSED;
				}


           }
            break;

            case ECG_EVENT_REAL_TIME_PROGRESS_DECIMATED_WITH_RESULTS:
			{
				ECG_MEAS_PROGRESS_EVENT_STRUCT * msg = event.ecg->payloadPtr.real_time_ind;	//ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT
				ECG_RESP_CONFIG_OBJECT_STRUCT * tp = event.ecg->test_params;

				if ( curr_lead_status != msg->common->lead_status)
				{
					prvDeviceLogSd("Lead Status, previous: %d, current: %d", curr_lead_status, msg->common->lead_status);
					curr_lead_status = msg->common->lead_status;
					pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, curr_lead_status, &impedance_results);
				}


				//uint8_t send_arrythmias = 0;

				ecg_rt_ind.ecg_lead_1 = &signal_ecg_1;
				ecg_rt_ind.ecg_lead_2 = &signal_ecg_2;
				ecg_rt_ind.respiration = &signal_resp;

				memcpy(&ecg_rt_ind.common, msg->common, sizeof(ecg_rt_ind.common) );

				ecg_rt_ind.ecg_status = ECG_REALTIME_STATUS_RECORDING_IN_PROGRESS;

				if ( (tp->ecg_output.output_frequency == ECG_PARAMS_OUTPUT_FREQ_250) && (tp->ecg_output.output_filter == ECG_FILTER_STATUS_ON) )
				{
					if (tp->general_config.compression == ECG_RESP_GENERAL_CONFIG_SIGNAL_COMPRESSION_OFF)
					{
						memcpy(&signal_ecg_1, msg->ecg_lead_1, sizeof(signal_ecg_1));
						memcpy(&signal_ecg_2, msg->ecg_lead_2, sizeof(signal_ecg_2));
						ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_FILTERED;
					}
					else
					{
						memset(&signal_ecg_1.filtered_comp, 0, sizeof(signal_ecg_1.filtered_comp));
						memset(&signal_ecg_2.filtered_comp, 0, sizeof(signal_ecg_2.filtered_comp));

						ecgCompressBufferLen(msg->ecg_lead_1->buff.raw, msg->ecg_lead_1->buff_byte_length, ECG_FILTERED_SAMPLE_SIZE,
								signal_ecg_1.filtered_comp.samples, &signal_ecg_1.filtered_comp.sample_len, signal_ecg_1.filtered_comp.bit_map,
								&signal_ecg_1.filtered_comp.bit_map_len);

						ecgCompressBufferLen(msg->ecg_lead_2->buff.raw, msg->ecg_lead_2->buff_byte_length, ECG_FILTERED_SAMPLE_SIZE,
								signal_ecg_2.filtered_comp.samples, &signal_ecg_2.filtered_comp.sample_len, signal_ecg_2.filtered_comp.bit_map,
								&signal_ecg_2.filtered_comp.bit_map_len);

						ecg_rt_ind.ecg_type = ECG_MEAS_SIGNAL_250HZ_FILTERED_COMPRESSED;
					}

				}

				if ( (tp->respiration_output.output_frequency == ECG_PARAMS_RESPIRATION_OUTPUT_50HZ_COMPRESSED) ||
						(ecg_real_time_config.real_time_enable == ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_DISABLE) )
				{
					memset(&signal_resp.filtered_comp, 0, sizeof(signal_resp.filtered_comp));

					ecgCompressBufferLen(msg->respiration->buff.raw, msg->respiration->buff_byte_length, ECG_FILTERED_SAMPLE_SIZE,
							signal_resp.filtered_comp.samples, &signal_resp.filtered_comp.sample_len, signal_resp.filtered_comp.bit_map,
							&signal_resp.filtered_comp.bit_map_len);
					ecg_rt_ind.resp_type = ECG_MEAS_RESPIRATION_SIGNAL_50HZ_FILTERED_COMPRESSED;
				}

//				ECG_ARRYTHMIA_CHANGE_E change = ECG_ARRYTHMIA_CHANGE_NONE;
            	ECG_ALGO_ARRYTHMIA_EVENT_STRUCT	*	e_arrs = msg->common->arrythmias;

//				if ( ecgArrythmiasTrackChanges(e_arrs, &change) != ECG_RESULT_OK)
//				{
//					LOG("\r\nError Tracking Arrythmias");
//				}

             	// Collect ECG algorithm (arrythmias) data
             	if (msg->common->algo_results->arrhythmia  )
             	{
//                	ecgAccumulateArrythmias(e_arrs, &arrythmias_acc);
//
//                	if (tp->ecg_algorithm->algorithm_params.report_type != ECG_PARAMS_ALGORITHM_REPORT_TYPE_SUMMARY_ONLY)
                		ecgAccumulateArrythmias(e_arrs, &arrythmias_curr);

                	// DEBUG PRINTS
                	prvDeviceEcgDebugPrintArrythmias(e_arrs);
             	}

             	prvDeviceHRAddResult(msg->common->algo_results->heart_rate);

             	// Collect Respiration algorithm data
         		respiration_algo_curr.respiration_rate = msg->common->respiration_algo_results->respiration_rate;
             	if (msg->common->respiration_algo_results->apnea_time)	// bigger than 0, apnea found
             	{
             		respiration_algo_curr.apnea_time = msg->common->respiration_algo_results->apnea_time;
//                 		memcpy(&respiration_algo_curr, data->respiration_algo_results, sizeof(respiration_algo_curr));

//             		if (data->respiration_algo_results->apnea_time > respiration_algo_acc.apnea_time)
//             			respiration_algo_acc.apnea_time = data->respiration_algo_results->apnea_time;
             	}



            	// Timout check for "arrythmias time period"
//             	if ( (osKernelSysTick() - ecg_measure_time) >= ECG_ALGORITHM_REPORT_SUMMARY_PERIOD_MSEC)
//             	{
//             		send_arrythmias = 1;
//             		DEBUG_PRINT(DEBUG_LEVEL_6, "\r\nsend_arrythmias\r\n");
//             	}


				if (ecg_real_time_config.real_time_enable == ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_ENABLE)
				{
					pbsSendECGMeasurementRealtimeProgressInd(&ecg_rt_ind, 0);
				}
				else /*if (send_arrythmias)*/
				{
					ecg_event_ind.event_type = ECG_MEAS_EVENT_TYPE_MAX;
					if (msg->common->algo_results->arrhythmia)
					{
						ecg_event_ind.event_type = ECG_MEAS_EVENT_TYPE_AUTOMATIC;
					}
					else if (device_button == DEVICE_BUTTON_STATE_PRESSED)
					{
						ecg_event_ind.event_type = ECG_MEAS_EVENT_TYPE_MANUAL;
						device_button = DEVICE_BUTTON_STATE_IDLE;	// clear for next time
					}
					BSP_RTC_TIMESTAMP bcd;
					BSP_RESULT_E bsp_res = bspRtcTimeGetBCD(&bcd);
					if (bsp_res == BSP_RESULT_OK)
					{
						memcpy(ecg_event_ind.base_timestamp, bcd.raw, sizeof(ecg_event_ind.base_timestamp) );
					}
					else
					{
						// in case of BSP RTC error
						memset(ecg_event_ind.base_timestamp, 0xff, sizeof(ecg_event_ind.base_timestamp) );
					}
					memcpy(&ecg_event_ind.common, msg->common, sizeof(ecg_event_ind.common) );

//					ecg_event_ind.filt_comp_lead_1 = &signal_ecg_1.filtered_comp;
//					ecg_event_ind.filt_comp_lead_2 = &signal_ecg_2.filtered_comp;
					ecg_event_ind.lead_1 = &signal_ecg_1.raw_comp;
					ecg_event_ind.lead_2 = &signal_ecg_2.raw_comp;
					ecg_event_ind.filt_comp_resp = &signal_resp.filtered_comp;
					ecg_event_ind.packet_type = ECG_MEAS_EVENT_PACKET_TYPE_BODY;	// always saved to flash as body, will be changed

					if (impedance_results.event_type == IMPEDANCE_EVENT_RESULT_READY)
					{
						pbsSendECGMeasurementEventInd(&ecg_event_ind, &impedance_results);
						impedance_results.event_type = IMPEDANCE_EVENT_MAX;	// reset for next time
					}
					else
					{
						pbsSendECGMeasurementEventInd(&ecg_event_ind, NULL);	// send impedance results only when new results available
					}

//					pbsFlushSdEvent(ecg_event_ind.event_type, change);

				}

//             	// Reset for next cycle
//             	if (send_arrythmias)
//             	{
//					prvDeviceEcgArrythmiasCollectionReset();
//             	}


			}
            break;
            case ECG_EVENT_ARRYTHMIA_CAHNGED:
            {
            	ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT * changes = event.ecg->payloadPtr.arrythmia_change;
            	PBS_TIMESTAMP_STRUCT timestamp;
            	BSP_RTC_TIMESTAMP time;
            	BSP_RESULT_E bsp_res = bspRtcTimeGetBCD(&time);
            	if (bsp_res == BSP_RESULT_OK)
            	{
            		memcpy(timestamp.data, time.raw, sizeof(timestamp.data));
            	}
            	else
            	{
            		memset(timestamp.data, PBS_TIMESTAMP_INVALID_PATTERN, sizeof(timestamp.data));
            	}

//				pbsFlushSdEvent(changes, 0, &timestamp);
            	pbsSaveEventEntryToSdCard(changes, 0, &timestamp);
            }
            break;
            case ECG_EVENT_IMPEDANCE_RESULTS:
            {
            	memcpy(&impedance_results, event.ecg->payloadPtr.impedance_results, sizeof(impedance_results));

				pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, curr_lead_status, &impedance_results);
				if (device_1st_ecg_run_flag)
				{
					osTimerStart(ecg_first_run_timer, DEVICE_ECG_FIRST_RUN_DEFAULT_TIME);

				}
            }
           	break;
            case ECG_EVENT_ARRYTHMIAS_FOUND:
            {
//            	prvDeviceEcgDebugPrintArrythmias(event.ecg->payloadPtr.arrythmias);
            }
            break;
			case ECG_EVENT_IMPEDANCE_TOO_HIGH:
			{
				pbsSendErrorEvent(PBS_GENERAL_ERRORS_EVENTS_IMPEDANCE_TOO_HIGH);
				prvDeviceLogSd("PBS_GENERAL_ERRORS_EVENTS_IMPEDANCE_TOO_HIGH");
				deviceSetErrorState(PBS_DEVICE_STATUS_BIT_NO_ERROR);
			}
			break;


		}
//		LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_3, " \r\n");
	}
	break;

	default:
	{
		LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_3, "DEVICE::device_event_handler() ->> UNHANDLED \r\n");
		return;
	}
	break;
	}


	return;

}



//void device_print_status(void)
//{
////	uint16_t volt = 0;
////	uint8_t volt_precn = 0;
//
////	PRINT("\r\nDEVICE STATE %x (0-Bootloader, 1-Idle, 2-Standby, 3-In Measurement, 4-Disconnected, 5-error) \r\n", device_state);
//	if (bspReadTemp(&current_temperature) == BSP_RESULT_OK)
//		LOG("Current Temperature: %f\r\n", current_temperature);
//	else
//		LOG("Current Temperature is not available yet\r\n");
//	//bat_mon_get_bat_level(&volt);
////	if (bat_mon_calc_batt_level(&volt, &volt_precn) == BAT_MON_RESULT_OK)
////		PRINT("Battery: %dV, %d%\r\n", volt, volt_precn);
////	else
////		PRINT("Cannot read battery\r\n");
//}




// @Override
RESULT_E pbsRxMessageHandler(uint16_t type, PBS_RX_PAYLOAD_STRUCT payload)
{
	return prvDeviceRxMessageHandlerLoggedIn(type, payload);
}



// Private handlers

static RESULT_E prvDeviceRxMessageHandlerLoggedIn(uint16_t type, PBS_RX_PAYLOAD_STRUCT payload)
{
	LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_2,"DEVICE:: rx_message_handler_logged_in type %x \r\n", type);


	switch (type)
	{
	// --------------------- COMMAND_GENERAL ----------------------------------
	case PBS_COMMAND_GENERAL_GET_INFO:
		prvDeviceGetInfo(payload);
		break;
	case PBS_COMMAND_GENERAL_GET_STATUS:
		prvDeviceGetStatus();
		break;
	case PBS_COMMAND_GENERAL_CONTROL:
		prvDeviceControl(payload);
		break;
	case PBS_COMMAND_DEVICE_CONFIGURATION_SET:
		prvDeviceConfigSet(payload);
		break;
	case PBS_COMMAND_DEVICE_CONFIGURATION_GET:
	{
		BSP_RTC_TIMESTAMP time;
		bspRtcTimeGetBCD(&time);
		memcpy(device_config.timestamp, time.raw, sizeof(device_config.timestamp));
		pbsSendDeviceConfigGetResp(&device_config);
	}
	break;


	// ------------------- COMMAND_ECG ----------------------------------------
	case PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE:
		prvDeviceEcgSetRealTime(payload);
		break;
	case PBS_COMMAND_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESP:
		prvDeviceEcgProgressIndicationResponse(payload);
		break;
	case PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT:
		if (DEVICE_MODE_PATCH)
      		prvDeviceEcgRemoteEventCmd(payload);
		else
            //mode holter: enter when user press logo:
			prvDeviceEcgRemoteEventHolterCmd(payload);
		break;
	case PBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_RESP:
		prvDeviceEcgEventIndicationResponse(payload);
		break;
	case PBS_COMMAND_ECG_MEASUREMENT_CONFIG_SET:
		prvDeviceEcgConfigSet(payload);
		break;
	case PBS_COMMAND_ECG_MEASUREMENT_CONFIG_GET:
		prvDeviceEcgConfigGet();
		break;
	case PBS_COMMAND_ECG_ALGORITHM_CONFIG_SET:
		prvDeviceEcgAlgorithmConfigSet(payload);
		break;
	case PBS_COMMAND_ECG_ALGORITHM_CONFIG_GET:
		prvDeviceEcgAlgorithmConfigGet();
		break;
	case PBS_COMMAND_ECG_MEASUREMENT_INFO_GET:
		prvDeviceEcgContactInfoGetCmd(payload);
		break;
	case PBS_COMMAND_ECG_STATISTICS_IND_RESP:
		break;

	// ------------------- COMMAND_ACCELEROMETER ------------------------------
	case PBS_COMMAND_ACCELEROMETER_CONFIG_SET:
		prvDeviceAccelConfigSet(payload);
		break;
	case PBS_COMMAND_ACCELEROMETER_CONFIG_GET:
		break;
	case PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER:
		break;

	}

	return RESULT_OK;
}


// --------------------------------RX message handlers helpers--------------------------------------
// Note: these functions touch static members of Device Module (current file)

/*
 * handshake()
 *
 * saves GApp version and replies with HW & FW versions, and serial numbers of MCU and Atmel cryptography peripheral
 * */

static RESULT_E prvDeviceGetInfo(PBS_RX_PAYLOAD_STRUCT payload)
{
	PBS_DEVICE_INFO_STRUCT dummy;
    memset(&dummy, 0, sizeof(dummy));

    // update if necessary
	if (memcmp(&device_info, &dummy, sizeof(device_info)) == 0)
	{
		prvDeviceGetDeviceInfo();
	}

	// send response
	pbsSendGetDeviceInfoResp(&device_info);

	return RESULT_OK;
}
static DEVICE_RESULT_ENUM prvDeviceGetDeviceInfo()
{
//	static SYS_INFO_STRUCT 				sys_info;
	uint8_t								gmp_id[DEVICE_SN_LENGTH + 1] = {0};

	device_info.device_type = DEVICE_TYPE_PATCH;

	prvDeviceMcuIdGet();
	memcpy(&device_info.mcu_sn, serial_number, sizeof(device_info.mcu_sn) );

	bspHwVerGet(&hw_ver[3]);

//	char tmp[] = {"GPR1111700001"};//TEST
	//memcpy(&device_info.device_sn, serial_number, sizeof(device_info.device_sn) );
	prvDeviceIdGet(gmp_id);
	memcpy(&device_info.device_sn, gmp_id, sizeof(device_info.device_sn));
//	memcpy(&device_info.device_sn, tmp, sizeof(device_info.device_sn) );
	memcpy(&device_info.fw, fw_ver, sizeof(device_info.fw) );
	memcpy(&device_info.hw, hw_ver, sizeof(device_info.hw) );
	memcpy(&device_info.ble_mac, ble_mac, sizeof(device_info.ble_mac) );
	memcpy(&device_info.bootloader, bl_fw_ver, sizeof(device_info.bootloader) );

	return DEVICE_RESULT_OK;
}

/*
 * prvDeviceMcuIdGet()
 *
 * get mcu id from flash and store it in static member (serial_number)
 * */
static RESULT_E prvDeviceMcuIdGet(void)
{//Move to BSP
	memcpy (serial_number, STM32_UUID, sizeof(serial_number));

	return RESULT_OK;
}

// TODO: match types (according to PBS)
static RESULT_E prvDeviceGetStatus()
{
	PBS_DEVICE_STATUS_STRUCT status;
	uint16_t				volt = 0;
	float32_t				temperature = 0;
	BSP_PIN_STATE_E			pin_state;


	if (bspReadTemp(&temperature) != BSP_RESULT_OK)
		temperature = -1;


	status.state = (PBS_DEVICE_STATUS_STATE_ENUM)device_state;
	status.error = PBS_GENERAL_ERRORS_EVENTS_OK; // NOT IMPLEMENTED
	status.bit_status = device_bit;
	if (leds_state_battery == DEVICE_LEDS_BATTERY_IDLE)
		status.bat_level = PBS_DEVICE_STATUS_BATTERY_LEVEL_OK;
	else
		status.bat_level = PBS_DEVICE_STATUS_BATTERY_LEVEL_LOW;
	status.temperature = temperature;
	status.owned = PBS_DEVICE_STATUS_OWNED;
	if (batMonVoltGet(&volt) != BAT_MON_RESULT_OK)
		volt = -1;
	status.voltage = volt;

	bspReadPin(BSP_PIN_TYPE_BUTTON, &pin_state);
	if (pin_state)
		status.button = PBS_DEVICE_STATUS_BUTTON_NOT_PRESSED;
	else
		status.button = PBS_DEVICE_STATUS_BUTTON_PRESSED;

	pbsSendGetStatusResp(&status);

	return RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceClearSdEcgData()
{
	ecgStop();
	osDelay(1000);
	SD_RESULT_E sd_res = sdDeleteDirContect(DEVICE_ECG_DIRECTORY_NAME);
	if (sdFileExist(PBS_SD_EVENT_MAP_FILE_NAME) == SD_RESULT_OK)
	{
		sd_res |= sdDeleteFile(PBS_SD_EVENT_MAP_FILE_NAME);
	}

	if (sd_res == SD_RESULT_OK)
		return DEVICE_RESULT_OK;

	return DEVICE_RESULT_SD_ERROR;
}


static RESULT_E prvDeviceControl(PBS_RX_PAYLOAD_STRUCT payload)
{
	if (payload.tlv_list == NULL)
		return RESULT_WRONG_PARAMTER;

	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;

	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case PBS_COMMAND_DEVICE_CONTROL_RESET_INTO_DEFAULTS:
//			pbsTxEmptyQueue(); // TODO: debug
			if (prvDeviceEcgConfigResetToDefaults() == DEVICE_RESULT_OK)
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_OK);
			else
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_GENERAL_ERROR);	// TODO: fix error code
			break;
		case PBS_COMMAND_DEVICE_CONTROL_RESET_EXTERNAL_MEMEORY:
        {
        	if ( (curr_tlv->length != sizeof(uint8_t)) || (curr_tlv->value[0] != PBS_COMMAND_DEVICE_CONTROL_RESET_EXTERNAL_MEMEORY_RESET_MEMORY) )
        	{
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_GENERAL_ERROR);	// TODO: fix error code
				return RESULT_WRONG_PARAMTER;
        	}

#ifdef 	SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
            LOG("prvDeviceControl -> PBS_COMMAND_DEVICE_CONTROL_RESET_EXTERNAL_MEMEORY: Skipped");
			pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_OK);
			osDelay(100);
			bsp_system_reset();
#else	// SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
        	DEVICE_RESULT_ENUM dev_res = prvDeviceClearSdEcgData();
            LOG("prvDeviceControl -> PBS_COMMAND_DEVICE_CONTROL_RESET_EXTERNAL_MEMEORY: ECG directory content deleted %s %d\r\n", (dev_res == DEVICE_RESULT_OK ? "ok" : "failed"), dev_res);
			if (dev_res == DEVICE_RESULT_OK)
			{
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_OK);
				osDelay(100);
				bsp_system_reset();
			}
			else
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_SD_CARD_RESET_MEMORY_FAILED);
#endif	// SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
        }
			break;
		case PBS_COMMAND_DEVICE_CONTROL_SET_DEVICE_NAME:
			pbsSendDeviceControlResp(PBS_GENERAL_ERRORS_EVENTS_UNKNOWN_ERROR);	// NOT IMPLEMENTED
			return RESULT_WRONG_PARAMTER;
			break;
		case PBS_COMMAND_DEVICE_CONTROL_UPDATE_BOOTLOADER:
			pbsSendDeviceControlResp(PBS_GENERAL_ERRORS_EVENTS_UNKNOWN_ERROR);	// NOT IMPLEMENTED
			return RESULT_WRONG_PARAMTER;
			break;
		case PBS_COMMAND_DEVICE_CONTROL_SEND_ECG_DATA_FROM_SD_CARD:
			pbsSendDeviceControlResp(PBS_GENERAL_ERRORS_EVENTS_UNKNOWN_ERROR);	// NOT IMPLEMENTED
			return RESULT_WRONG_PARAMTER;
		   break;
		case PBS_COMMAND_DEVICE_CONTROL_SERIAL_INTERFACE_ECHO:
			if (curr_tlv->length != sizeof(uint8_t))
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_GENERAL_ERROR);
			if (serialProtocolEchoEnable(curr_tlv->value[0]) == SERIAL_PROTOCOL_RESULT_OK)
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_OK);
			else
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_SET_SERIAL_ECHO_ON_OFF_FAILED);	// TODO: fix error code
		   break;
		case PBS_COMMAND_DEVICE_CONTROL_CHANGE_DEVICE_MODE:
			if (curr_tlv->length != sizeof(ECG_GENERAL_CONFIG_DEVICE_MODE_E))
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_GENERAL_ERROR);

			DEVICE_CONFIG_STRUCT conf = {0};
			conf.device_mode = (ECG_GENERAL_CONFIG_DEVICE_MODE_E)curr_tlv->value[0];

			switch (conf.device_mode)
			{
			case ECG_GENERAL_CONFIG_DEVICE_MODE_HOLTER_ONLY:
			case ECG_GENERAL_CONFIG_DEVICE_MODE_TESTER:
			case ECG_GENERAL_CONFIG_DEVICE_MODE_PATCH:
				break;
			default:
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_GENERAL_ERROR);
				return RESULT_WRONG_PARAMTER;
			}

			DEVICE_CONFIG_RESULT_E conf_res = deviceConfigChangeMode(&conf);
			if (conf_res == DEVICE_CONFIG_RESULT_OK)
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_OK);
			else
				pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_CHANGE_DEVICE_MODE_FAILED);

		   break;
		case PBS_COMMAND_DEVICE_CONTROL_RUN_SD_CARD_TEST:
			pbsSendDeviceControlResp(PBS_GENERAL_ERRORS_EVENTS_UNKNOWN_ERROR);	// NOT IMPLEMENTED
			return RESULT_WRONG_PARAMTER;
		   break;

		default:
			pbsSendDeviceControlResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);	// wrong parameter
			return RESULT_WRONG_PARAMTER;
			break;
		}
		curr_tlv = curr_tlv->next;
	}
	return RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceConfigSetTimestamp(uint8_t * timestamp, uint8_t length)
{
	if ( (length != PBS_TIMESTAMP_LENGTH) || (timestamp == NULL) )
		return DEVICE_RESULT_WRONG_PARAM;

	BSP_RTC_TIMESTAMP * tmp = (BSP_RTC_TIMESTAMP *)timestamp;

	if (bspRtcTimeSetBCD(tmp) != BSP_RESULT_OK)
		return DEVICE_RESULT_RTC_ERROR;

	return DEVICE_RESULT_OK;
}

static RESULT_E prvDeviceConfigSet(PBS_RX_PAYLOAD_STRUCT payload)
{
	if (payload.tlv_list == NULL)
		return RESULT_WRONG_PARAMTER;

	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;

	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_TIME:
			if (prvDeviceConfigSetTimestamp(curr_tlv->value, curr_tlv->length) != DEVICE_RESULT_OK)
			{
				pbsSendDeviceConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return RESULT_WRONG_PARAMTER;
			}
			break;
		case PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LED:
			if (curr_tlv->length != sizeof(device_config.leds_state) || (curr_tlv->value[0] >= PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LEDS_BITMAP_MAX))
			{
				pbsSendDeviceConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return RESULT_WRONG_PARAMTER;
			}

			device_config.leds_state = curr_tlv->value[0];
			// TODO: send command to leds

			break;
		case PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_DEVICE_NAME:
			if (curr_tlv->length > sizeof(device_config.device_name))
			{
				pbsSendDeviceConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return RESULT_WRONG_PARAMTER;
			}

			memcpy(device_config.device_name, curr_tlv->value, curr_tlv->length );
			device_config.device_name_length = curr_tlv->length;

			break;
		default:
			pbsSendDeviceConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
			return RESULT_WRONG_PARAMTER;
			break;
		}
		curr_tlv = curr_tlv->next;
	}

	pbsSendDeviceConfigSetResp(PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_STATUS_OK);

	prvDeviceLogSd("Configuration Set");

	return RESULT_OK;
}


static RESULT_E prvDeviceEcgSetRealTime(PBS_RX_PAYLOAD_STRUCT payload)
{
	if (payload.tlv_list == NULL)
		return RESULT_WRONG_PARAMTER;

	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;

	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME:
			if ( (curr_tlv->length != sizeof(ecg_real_time_config.real_time_enable) )
					|| (curr_tlv->value[0] >= ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_MAX))
			{
				// TODO: 2nd param is time
				pbsSendECGMeasurementRealtimeEnableResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, 0);
				return RESULT_WRONG_PARAMTER;
			}

			ecg_real_time_config.real_time_enable = (ECG_MEASUREMENT_REALTIME_ENABLE_REAL_TIME_ENUM)curr_tlv->value[0];

			break;
		case PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESET_BUFFER:
			if ( (curr_tlv->length != sizeof(ecg_real_time_config.reset_buffer) )
					|| (curr_tlv->value[0] >= ECG_MEASUREMENT_REALTIME_ENABLE_RESET_BUFFER_MAX))
			{
				// TODO: 2nd param is time
				pbsSendECGMeasurementRealtimeEnableResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, 0);
				return RESULT_WRONG_PARAMTER;
			}

			ecg_real_time_config.reset_buffer = (ECG_MEASUREMENT_REALTIME_ENABLE_RESET_BUFFER_ENUM)curr_tlv->value[0];

			break;
		default:
			pbsSendECGMeasurementRealtimeEnableResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, 0);
			return RESULT_WRONG_PARAMTER;
			break;
		}
		curr_tlv = curr_tlv->next;
	}

	// TODO:
	//		activate according to changed ecg_real_time_config?
	//		2nd param is time
	//
	// 		ADD RTC
	pbsSendECGMeasurementRealtimeEnableResp(PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_OK, 0);

	return RESULT_OK;

}

static RESULT_E prvDeviceEcgProgressIndicationResponse(PBS_RX_PAYLOAD_STRUCT payload)
{
	if (payload.tlv_list == NULL)
		return RESULT_WRONG_PARAMTER;

	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;

	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case ECG_MEASUREMENT_REALTIME_PROGRESS_IND_RESP_SW_FLOW_CONTROL_INDEX:
		{
			if ( (curr_tlv->length != sizeof(uint8_t) ) || ( (curr_tlv->value[0] > PBS_FLOW_CONTROL_MAX) && (curr_tlv->value[0] != (uint8_t)PBS_ECG_EVENT_IND_HEADER_FC) ) )
			{
				// TODO: handle wrong index / wrong message
				LOG("\r\nprvDeviceEcgProgressIndicationResponse WRONG index");
				return RESULT_WRONG_PARAMTER;
			}
			uint8_t fc_index = curr_tlv->value[0];
			if (pbsMessageAckRecieved(fc_index) != PBS_RESULT_OK)
			{
//				LOG("\r\nprvDeviceEcgProgressIndicationResponse Ack failed to remove index %d", fc_index);
			}
		}
			break;
		default:
			LOG("\r\nprvDeviceEcgProgressIndicationResponse WRONG PARAMETER");
			return RESULT_WRONG_PARAMTER;
			break;
		}
		curr_tlv = curr_tlv->next;
	}

	return RESULT_OK;
}

static RESULT_E prvDeviceEcgRemoteEventCmd(PBS_RX_PAYLOAD_STRUCT payload)
{
	if (payload.tlv_list == NULL)
		return RESULT_WRONG_PARAMTER;

	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;
	PBS_TIMESTAMP_STRUCT timestamp;
	memset(timestamp.data, PBS_TIMESTAMP_INVALID_PATTERN, sizeof(timestamp.data));

	if (device_state == DEVICE_STATE_ERROR)
	{
		pbsSendECGMeasurementRemoteEventResp(PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_DEVICE_IN_ERROR_STATE, NULL);
		return DEVICE_RESULT_ERROR_STATE;
	}

	ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT changes = {.change = ECG_ARRYTHMIA_CHANGE_MAX, .arr.raw =0};
	uint16_t duration = 0;
	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT:
			if (curr_tlv->length == sizeof(uint8_t))
			{
				if (curr_tlv->value[0] == PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENABLE)
					changes.change = ECG_ARRYTHMIA_CHANGE_REMOTE;
				else if(curr_tlv->value[0] == PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_DISABLE)
					changes.change = ECG_ARRYTHMIA_CHANGE_REMOTE_STOP;
				else
				{
					pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
					return RESULT_WRONG_PARAMTER;
				}
			}
			else
			{
				pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
				return RESULT_WRONG_PARAMTER;
			}
			break;
		case PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_DURATION:
			if (curr_tlv->length == sizeof(duration))
			{
				memcpy(&duration, curr_tlv->value, sizeof(duration));
				if ( (duration < PBS_ECG_REMOTE_EVENT_DURATION_SEC_MIN) && (duration > PBS_ECG_REMOTE_EVENT_DURATION_SEC_MAX) )
				{
					pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
					return RESULT_WRONG_PARAMTER;
				}
			}
			else
			{
				pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
				return RESULT_WRONG_PARAMTER;
			}
			break;
		default:
			LOG("\r\nprvDeviceEcgRemoteEventCmd WRONG PARAMETER");
			pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
			return RESULT_WRONG_PARAMTER;
			break;
		}
		curr_tlv = curr_tlv->next;
	}
	BSP_RTC_TIMESTAMP time;
	BSP_RESULT_E bsp_res = bspRtcTimeGetBCD(&time);
	if (bsp_res == BSP_RESULT_OK)
	{
		memcpy(timestamp.data, time.raw, sizeof(timestamp.data));
	}

	PBS_RESULT_ENUM pbs_res = pbsSaveEventEntryToSdCard(&changes, duration, &timestamp);
	if (pbs_res == PBS_RESULT_OK)
	{
		pbsSendECGMeasurementRemoteEventResp(PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_OK, &timestamp);
	}
	else
	{
		pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
	}
	return RESULT_OK;
}

static RESULT_E prvDeviceEcgRemoteEventHolterCmd(PBS_RX_PAYLOAD_STRUCT payload)
{
	PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENUM state = PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_DISABLE;

	if (payload.tlv_list == NULL)
		return RESULT_WRONG_PARAMTER;

	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;
	PBS_TIMESTAMP_STRUCT timestamp;
	memset(timestamp.data, PBS_TIMESTAMP_INVALID_PATTERN, sizeof(timestamp.data));

	if (device_state == DEVICE_STATE_ERROR)
	{
		pbsSendECGMeasurementRemoteEventResp(PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_DEVICE_IN_ERROR_STATE, NULL);
		return DEVICE_RESULT_ERROR_STATE;
	}

//	ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT changes = {.change = ECG_ARRYTHMIA_CHANGE_MAX, .arr.raw =0};
	uint16_t duration = 0;
	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT:
			if (curr_tlv->length == sizeof(uint8_t))
			{
				if (curr_tlv->value[0] == PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENABLE)
					state = PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENABLE;
				else if(curr_tlv->value[0] == PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_DISABLE)
					state = PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_DISABLE;
				else
				{
					pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
					return RESULT_WRONG_PARAMTER;
				}
			}
			else
			{
				pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
				return RESULT_WRONG_PARAMTER;
			}
			break;
		case PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_DURATION:
			if (curr_tlv->length == sizeof(duration))
			{
				memcpy(&duration, curr_tlv->value, sizeof(duration));
				if ( (duration < PBS_ECG_REMOTE_EVENT_DURATION_SEC_MIN) && (duration > PBS_ECG_REMOTE_EVENT_DURATION_SEC_MAX) )
				{
					pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
					return RESULT_WRONG_PARAMTER;
				}
			}
			else
			{
				pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
				return RESULT_WRONG_PARAMTER;
			}
			break;
		default:
			LOG("\r\nprvDeviceEcgRemoteEventCmd WRONG PARAMETER");
			pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
			return RESULT_WRONG_PARAMTER;
			break;
		}
		curr_tlv = curr_tlv->next;
	}
	BSP_RTC_TIMESTAMP time;
	BSP_RESULT_E bsp_res = bspRtcTimeGetBCD(&time);
	if (bsp_res == BSP_RESULT_OK)
	{
		memcpy(timestamp.data, time.raw, sizeof(timestamp.data));
	}

	PBS_RESULT_ENUM pbs_res = pbsHolterRemoteEvent(state, duration);//pbsSaveEventEntryToSdCard(&changes, duration, &timestamp);
	if (pbs_res == PBS_RESULT_OK)
		pbsSendECGMeasurementRemoteEventResp(PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_OK, &timestamp);
	else
		pbsSendECGMeasurementRemoteEventResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, &timestamp);
	return RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceEcgEventIndicationResponse(PBS_RX_PAYLOAD_STRUCT payload)
{
	if (payload.tlv_list == NULL)
		return DEVICE_RESULT_WRONG_PARAM;

	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;

	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case PBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_RESP_SW_FC_INDEX:
		{
			if ( (curr_tlv->length != sizeof(uint8_t) ) || ( (curr_tlv->value[0] > PBS_FLOW_CONTROL_MAX) && (curr_tlv->value[0] != (uint8_t)PBS_ECG_EVENT_IND_HEADER_FC) ) )
			{
				// TODO: handle wrong index / wrong message
				LOG("prvDeviceEcgProgressIndicationResponse WRONG index\r\n");
				return DEVICE_RESULT_WRONG_PARAM;
			}
			uint8_t fc_index = curr_tlv->value[0];
			if (pbsMessageAckRecieved(fc_index) != PBS_RESULT_OK)
			{
//				LOG("\r\nprvDeviceEcgProgressIndicationResponse Ack failed to remove index %d", fc_index);
			}
		}
			break;
		case PBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_BASE_TIMESTAMP:
		{
			LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_2, "\r\nPBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_BASE_TIMESTAMP");
		}
		break;
		default:
			LOG("prvDeviceEcgProgressIndicationResponse WRONG PARAMETER\r\n");
			return DEVICE_RESULT_WRONG_PARAM;
			break;
		}
		curr_tlv = curr_tlv->next;
	}

	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceEcgContactInfoGetCmd(PBS_RX_PAYLOAD_STRUCT payload)
{
	if (payload.tlv_list == NULL)
		return DEVICE_RESULT_WRONG_PARAM;

	PBS_RESULT_ENUM		pbs_res;
	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;

	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
			case PBS_ECG_CONTACT_INFO_CMD_REQUEST:
			{
				if ( (curr_tlv->length == sizeof(uint8_t)) &&  (curr_tlv->value[0] <  PBS_ECG_GET_INFO_MAX))
				{
					switch (curr_tlv->value[0])
					{
						case PBS_ECG_GET_INFO_IMPEDANCE:
						{
							if (ecgImpedanceStart() != ECG_RESULT_OK)
								pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_IMPEDANCE_FAILED, curr_lead_status, &impedance_results);
							else
								pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, curr_lead_status, NULL);
						}
						break;

						case PBS_ECG_GET_INFO_LEAD_CONTACT_STATUS:
						{
							pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_OK, curr_lead_status, &impedance_results);
						}
						break;
						case PBS_ECG_GET_INFO_STATISTICS:
						{
							PBS_ECG_GET_INFO_STATUS_E status;
							pbs_res = pbsFlushStatisticsStart();
							switch (pbs_res)
							{
							case PBS_RESULT_OK:
								status = PBS_ECG_GET_INFO_STATUS_OK;
								break;
							case PBS_RESULT_ERROR_STATISTICS_FILE_DOES_NOT_EXISTS:
								status = PBS_ECG_GET_INFO_STATUS_NO_STATISTICS_FILE;
								break;
							case PBS_RESULT_ERROR_STATISTICS_FILE_ERROR:
								status = PBS_ECG_GET_INFO_STATUS_STATISTICS_FILE_ERROR;
								break;
							default:
								pbsSendECGContactInfoInd(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, curr_lead_status, &impedance_results);
								return DEVICE_RESULT_WRONG_PARAM;
							}
							pbsSendECGContactInfoInd(status, curr_lead_status, &impedance_results);

						}
							break;
						default:
							pbsSendECGContactInfoInd(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, curr_lead_status, &impedance_results);
							return DEVICE_RESULT_OK;
							break;
					}
				}
				else
					pbsSendECGContactInfoInd(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, curr_lead_status, &impedance_results);
				return DEVICE_RESULT_OK;
			}
			break;


		default:
			LOG("prvDeviceEcgContactInfoGetCmd WRONG PARAMETER\r\n");
			return DEVICE_RESULT_WRONG_PARAM;
			break;
		}
		curr_tlv = curr_tlv->next;
	}
	pbsSendECGContactInfoInd(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER, curr_lead_status, &impedance_results);
	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceEcgConfigSet(PBS_RX_PAYLOAD_STRUCT payload)
{

//	ECG_RESULT 				res = ECG_RESULT_OK;
	ECG_RESP_CONFIG_OBJECT_STRUCT		ecg_params_struct;

//	if (device_state != DEVICE_STATE_IDLE)
//	{
//		if (device_state == DEVICE_STATE_IN_MEASUREMENT)
//		{
//            pbs_snd_ecg_meas_start_resp(PBS_ERROR_EVENT_IN_MEASUREMENT, 0);
//			DEBUG_PRINT(DEBUG_LEVEL_1, "DEVICE::PBS_MEASURE_COMMAND_START - PBS_ERROR_EVENT_IN_MEASUREMENT \r\n");
//			return RESULT_ERROR_ALREADY_RUNNING;
//		}
////		else if (device_state == DEVICE_STATE_CHARGING)
//		else
//		{
//            pbs_snd_ecg_meas_start_resp(PBS_ACK_MEASURE_START_GENERAL_ERROR, 0);
//			DEBUG_PRINT(DEBUG_LEVEL_1, "DEVICE::PBS_MEASURE_COMMAND_START - IN ERROR\r\n");
//			return RESULT_ERROR;
//		}
//	}
//
//	if (charging_status == DEVICE_CHARGING_STATUS_ON)
//	{
//		pbs_snd_ecg_meas_start_resp(PBS_ERROR_EVENT_DEVICE_IS_CHARGING, 0);
//		DEBUG_PRINT(DEBUG_LEVEL_1, "DEVICE::PBS_MEASURE_COMMAND_START - PBS_ERROR_EVENT_DEVICE_IS_CHARGING \r\n");
//		return RESULT_ERROR_DEVICE_CHARGING;
//	}

	// Get a copy of current configuration
	ecgGetConfig(&ecg_params_struct);


	PBS_TLV_LIST_STRUCT * curr_tlv = payload.tlv_list;



	// TODO: validate configuration ability while running /  turn off

	ecgStop();


//	if (ecg_params_struct.general_config.measurement_enable == ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_ON)
//	{
//		BOOL can_config = FALSE;
//
//		while (curr_tlv != NULL)
//		{
//
//			switch (curr_tlv->subtype)
//			{
//				// TODO: excluded types that CAN be set "on-the-fly"
//				case PBS_ECG_RESPIRATION_CONFIG_OBJECT_MEASUREMENT_ENABLE:
//				{
//					if (curr_tlv->length ==sizeof(ecg_params_struct.general_config.measurement_enable) && (curr_tlv->value[0] == ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_OFF))
//					{
//						can_config = TRUE;
//					}
//				}
//				break;
//
//				default:
//				{
//
//				}
//				break;
//			}
//			curr_tlv = curr_tlv->next;
//		}
//
//		if (can_config == FALSE)
//		{
//			pbsSendECGConfigSetResp(PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_CANNOT_SET_WHILE_RECORDING);
//			return RESULT_WRONG_PARAMTER;
//		}
//		ecgStop();
//
//	}

	// reset for parsing
	curr_tlv = payload.tlv_list;

	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_MEASUREMENT_ENABLE:
			{
				if (curr_tlv->length != sizeof(ecg_params_struct.general_config.measurement_enable)
						|| (curr_tlv->value[0] >= ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_MAX))
				{
					pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
					return DEVICE_RESULT_WRONG_PARAM;
				}

				ecg_params_struct.general_config.measurement_enable = (ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_ENUM)curr_tlv->value[0];
			}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_COMPRESSED_SIGNAL:
			{
				if (curr_tlv->length != sizeof(ecg_params_struct.general_config.compression)
						|| (curr_tlv->value[0] >= ECG_RESP_GENERAL_CONFIG_SIGNAL_COMPRESSION_MAX))
				{
					pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
					return DEVICE_RESULT_WRONG_PARAM;
				}

				ecg_params_struct.general_config.compression = (ECG_RESP_GENERAL_CONFIG_SIGNAL_COMPRESSION_ENUM)curr_tlv->value[0];
			}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_RECORD_TO_MEMORY:
			{
				if (curr_tlv->length != sizeof(ecg_params_struct.general_config.copy_to_memory)
						|| (curr_tlv->value[0] >= ECG_RESP_GENERAL_CONFIG_RECORD_TO_MEMORY_MAX))
				{
					pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
					return DEVICE_RESULT_WRONG_PARAM;
				}

				ecg_params_struct.general_config.copy_to_memory = (ECG_RESP_GENERAL_CONFIG_RECORD_TO_MEMORY_ENUM)curr_tlv->value[0];
			}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_SEND_EVENTS:
			{
				if (curr_tlv->length != sizeof(ecg_params_struct.general_config.send_events)
						|| (curr_tlv->value[0] >= ECG_RESP_GENERAL_CONFIG_SEND_EVENTS_MAX))
				{
					pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
					return DEVICE_RESULT_WRONG_PARAM;
				}

				ecg_params_struct.general_config.send_events = (ECG_RESP_GENERAL_CONFIG_SEND_EVENTS_ENUM)curr_tlv->value[0];
			}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_PROGRESS_INDICATIONS_FLOW_CONTROL:
			{
				if (curr_tlv->length != sizeof(ecg_params_struct.general_config.progress_ind_flow_control)
						|| (curr_tlv->value[0] >= ECG_RESP_GENERAL_CONFIG_PROGRESS_INDICATIONS_FLOW_CONTROL_MAX))
				{
					pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
					return DEVICE_RESULT_WRONG_PARAM;
				}

				ecg_params_struct.general_config.progress_ind_flow_control = (ECG_RESP_GENERAL_CONFIG_PROGRESS_INDICATIONS_FLOW_CONTROL_ENUM)curr_tlv->value[0];
			}
			break;



		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_OUTPUT_FREQUENCY:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_output.output_frequency))
					&& (curr_tlv->value[0] < ECG_PARAMS_OUTPUT_FREQ_MAX))
				ecg_params_struct.ecg_output.output_frequency = (ENUM_ECG_PARAMS_OUTPUT_FREQ) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_50_60HZ_FILTER:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_output.notch_filter))
					&& (curr_tlv->value[0] < ENUM_ECG_PARAMS_FILTER_MAX))
				ecg_params_struct.ecg_output.notch_filter = (ENUM_ECG_PARAMS_NOTCH_FILTER) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
			if (ecg_params_struct.ecg_output.notch_filter == ENUM_ECG_PARAMS_FILTER_AUTO) ecg_params_struct.ecg_output.notch_filter =
					ENUM_ECG_PARAMS_FILTER_50;
		}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_ADS_GAIN_ECG:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ads.gain)) && (curr_tlv->value[0] < ADS_PARAMS_GAIN_MAX))
				ecg_params_struct.ads.gain = (ADS_PARAMS_GAIN_ENUM) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_BANDPASS_FILTER:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_output.bandpass_filter))
					&& (curr_tlv->value[0] < ECG_PARAMS_BANDPASS_FILTER_MAX))
				ecg_params_struct.ecg_output.bandpass_filter = (ENUM_ECG_PARAMS_BANDPASS_FILTER) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_ON_LEAD_OFF_EVENT:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.general_config.on_lead_off_event))
					&& (curr_tlv->value[0] < (ECG_PARAMS_ON_LEAD_OFF_MAX - 1)))
				ecg_params_struct.general_config.on_lead_off_event = (ECG_RESP_GENERAL_CONFIG_ON_LEAD_OFF_EVENT_ENUM) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;

		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_PEAKS_OUTPUT:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_output.peaks_output))
					&& (curr_tlv->value[0] < ECG_PARAM_PEAKS_OUTPUT_MAX))
				ecg_params_struct.ecg_output.peaks_output = (ENUM_ECG_PARAMS_PEAKS_OUTPUT) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;

		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_OUTPUT_FILTER:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_output.output_filter))
					&& (curr_tlv->value[0] < ECG_FILTER_STATUS_MAX))
				ecg_params_struct.ecg_output.output_filter = (ECG_FILTER_STATUS_E) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;

		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_OUTPUT:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.respiration_output.output_frequency))
					&& (curr_tlv->value[0] < ECG_PARAMS_RESPIRATION_OUTPUT_MAX))
				ecg_params_struct.respiration_output.output_frequency = (ENUM_ECG_PARAMS_RESPIRATION_OUTPUT) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_50_60HZ_FILTER:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.respiration_output.notch_filter))
					&& (curr_tlv->value[0] < ENUM_ECG_PARAMS_FILTER_MAX))
				ecg_params_struct.respiration_output.notch_filter = (ENUM_ECG_PARAMS_NOTCH_FILTER) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
			if (ecg_params_struct.respiration_output.notch_filter == ENUM_ECG_PARAMS_FILTER_AUTO)
				ecg_params_struct.respiration_output.notch_filter = ENUM_ECG_PARAMS_FILTER_50;
		}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_BANDPASS_FILTER:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.respiration_output.bandpass_filter))
					&& (curr_tlv->value[0] < ECG_PARAMS_RESPIRATION_BANDPASS_FILTER_MAX))
				ecg_params_struct.respiration_output.bandpass_filter = (ENUM_ECG_PARAMS_RESPIRATION_BANDPASS_FILTER) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;

		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_ADS_GAIN_RESPIRATION:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ads.respiration_gain)) && (curr_tlv->value[0] < ADS_PARAMS_GAIN_MAX))
				ecg_params_struct.ads.respiration_gain = (ADS_PARAMS_GAIN_ENUM) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;


		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_CLOCK:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ads.respiration_clock_freq)) && (curr_tlv->value[0] < ADS_PARAMS_RESPIRATION_CLOCK_FREQ_MAX))
				ecg_params_struct.ads.respiration_clock_freq = (ADS_PARAMS_RESPIRATION_CLOCK_FREQ_ENUM) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_PHASE_SHIFT:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ads.respiration_phase_shift)) && (curr_tlv->value[0] < ADS_PARAMS_RESPIRATION_PHASE_SHIFT_MAX))
				ecg_params_struct.ads.respiration_phase_shift = (ADS_PARAMS_RESPIRATION_PHASE_SHIFT_ENUM) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;




		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_ALGORITHM_GAIN:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.respiration_output.algo_gain))
					&& (curr_tlv->value[0] < ECG_PARAMS_RESPIRATION_GAIN_MAX))
				ecg_params_struct.respiration_output.algo_gain = (ENUM_ECG_PARAMS_RESPIRATION_ALGO_GAIN) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;



//		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_PACE_MAKER_ENABLE:
//			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_IMPEDANCE_MEASUREMENT_SETTINGS:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.general_config.impedance_settings))
					&& (curr_tlv->value[0] < ECG_IMPEDANCE_SETTINGS_MAX))
				ecg_params_struct.respiration_output.algo_gain = (ECG_IMPEDANCE_SETTINGS_E) curr_tlv->value[0];
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_CONFIG_OBJECT_IMPEDANCE_MEASUREMENT_INTERVAL:
			if (curr_tlv->length == sizeof(ecg_params_struct.general_config.impedance_period_min))
			{
				memcpy(&ecg_params_struct.general_config.impedance_period_min, curr_tlv->value, sizeof(ecg_params_struct.general_config.impedance_period_min));
				if (ecg_params_struct.general_config.impedance_period_min > ECG_IMPEDANCE_PERIOD_MINUTES_MAX_VALID)
				{
					pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
					return DEVICE_RESULT_WRONG_PARAM;
				}
			}
			else
			{
				pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
			break;

		default:
		{
			pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
			return DEVICE_RESULT_WRONG_PARAM;

		}
			break;
		}

		curr_tlv = curr_tlv->next;
	}

    if ((ecg_params_struct.ecg_output.bandpass_filter == ECG_PARAMS_BANDPASS_FILTER_OFF) && (ecg_params_struct.ecg_output.output_frequency == ECG_PARAMS_OUTPUT_FREQ_250) )
    {
        LOG("Wrong Parameters\r\n");
		pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
        return DEVICE_RESULT_WRONG_PARAM;
    }



    // Set Flow Control accordingly
	if (ecg_params_struct.general_config.progress_ind_flow_control == ECG_RESP_GENERAL_CONFIG_PROGRESS_INDICATIONS_FLOW_CONTROL_ACK_PER_EACH_PACKET)
		pbsFlowControlEnable(SW_FLOW_CONTROL_ON);
	else if(ecg_params_struct.general_config.progress_ind_flow_control == ECG_RESP_GENERAL_CONFIG_PROGRESS_INDICATIONS_FLOW_CONTROL_NO_ACK_REQUIRED)
		pbsFlowControlEnable(SW_FLOW_CONTROL_OFF);

	// TODO: fix commented out

//    if ((ecg_params_struct.respiration_output.bandpass_filter == ECG_PARAMS_RESPIRATION_BANDPASS_FILTER_OFF) && ((ecg_params_struct.respiration_output.notch_filter == ENUM_ECG_PARAMS_FILTER_50) || (ecg_params_struct.respiration_output.notch_filter == ENUM_ECG_PARAMS_FILTER_60)))
//    {
//        LOG("Wrong Parameters\r\n");
//		pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
//        return RESULT_OK;
//    }

    LOG_D(DEBUG_MODULE_DEVICE, DEBUG_LEVEL_2, "DEVICE::PBS_MEASURE_COMMAND_START:: PBS_TEST_ECG\r\n");



	ecgCalcLsbWeight(&ecg_params_struct, &ecg_params_struct.ecg_output.lsb_weight);
	LOG("Weight %f\r\n", ecg_params_struct.ecg_output.lsb_weight);

	// Save new configuration
	ecgSetConfig(&ecg_params_struct);
	DEVICE_RESULT_ENUM device_res = prvDeviceSaveEcgConfig(&ecg_params_struct);

	if (device_res != DEVICE_RESULT_OK)
	{
		LOG("\r\nprvDeviceEcgConfigSet error writing to Flash");
	}


	if (ecg_params_struct.general_config.measurement_enable == ECG_RESP_GENERAL_CONFIG_MEASUREMENT_ENABLE_ON)
	{
		device_time_last_sent_ecg_contact_ind = 0;
		device_res = prvDeviceEcgStart();
		if (device_res != DEVICE_RESULT_OK)
			pbsSendECGConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_ADS_FAILURE);	//TODO: was PBS_ACK_MEASURE_START_BIT_FAILED
		else
		{
			pbsSendECGConfigSetResp(PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_OK);
			prvDeviceSetDeviceStates(DEVICE_CONNECTION_MAX, DEVICE_STATE_CONTACT_DETECTION);
		}
	}
	else
	{
		// TODO: set device state?
		pbsSendECGConfigSetResp(PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_OK);
	}

	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceEcgConfigGet()
{
	ECG_RESP_CONFIG_OBJECT_STRUCT		ecg_params_struct;

	ecgGetConfig(&ecg_params_struct);

	if (pbsSendECGConfigGetResp(&ecg_params_struct) != PBS_RESULT_OK)
		return DEVICE_RESULT_ERROR;

	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceEcgAlgorithmConfigGet()
{
	ECG_RESP_CONFIG_OBJECT_STRUCT		ecg_params_struct;

	ecgGetConfig(&ecg_params_struct);

	if (pbsSendECGAlgoConfigGetResp(&ecg_params_struct) != PBS_RESULT_OK)
		return DEVICE_RESULT_ERROR;

	return RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceEcgAlgorithmConfigSet(PBS_RX_PAYLOAD_STRUCT payload)
{
	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;

	ECG_RESP_CONFIG_OBJECT_STRUCT ecg_params_struct;

	ecgGetConfig(&ecg_params_struct);

	while (curr_tlv != NULL)
	{
		switch (curr_tlv->subtype)
		{
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_RUNS_LIMIT:
		{
			if (curr_tlv->length == sizeof(ecg_params_struct.ecg_algorithm.limit_runs) )
				memcpy(&ecg_params_struct.ecg_algorithm.limit_runs, &curr_tlv->value[0],
						sizeof(ecg_params_struct.ecg_algorithm.limit_runs));
			else
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
			if ( (ecg_params_struct.ecg_algorithm.limit_runs > ECG_PARAMS_ALGORITHM_VALID_VALUE_MAX) || (ecg_params_struct.ecg_algorithm.limit_runs < ECG_PARAMS_ALGORITHM_VALID_VALUE_MIN) )
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_BIGEMINY_LIMIT:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_algorithm.limit_bigeminy))
					&& (curr_tlv->value[0] < ECG_PARAMS_ALGORITHM_VALID_VALUE_MAX)
					&& (curr_tlv->value[0] > ECG_PARAMS_ALGORITHM_VALID_VALUE_MIN))
				memcpy(&ecg_params_struct.ecg_algorithm.limit_bigeminy, &curr_tlv->value[0],
						sizeof(ecg_params_struct.ecg_algorithm.limit_bigeminy));
			else
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_PAUSE_LIMIT:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_algorithm.limit_pause))
					&& (curr_tlv->value[0] < ECG_PARAMS_ALGORITHM_VALID_VALUE_MAX)
					&& (curr_tlv->value[0] > ECG_PARAMS_ALGORITHM_VALID_VALUE_MIN))
				memcpy(&ecg_params_struct.ecg_algorithm.limit_pause, &curr_tlv->value[0],
						sizeof(ecg_params_struct.ecg_algorithm.limit_pause));
			else
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_VTACH_LIMIT_1:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_algorithm.limit_vtach))
					&& (curr_tlv->value[0] < ECG_PARAMS_ALGORITHM_VALID_VALUE_MAX)
					&& (curr_tlv->value[0] > ECG_PARAMS_ALGORITHM_VALID_VALUE_MIN))
				memcpy(&ecg_params_struct.ecg_algorithm.limit_vtach, &curr_tlv->value[0],
						sizeof(ecg_params_struct.ecg_algorithm.limit_vtach));
			else
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_EBRAD_LIMIT_1:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_algorithm.limit_ebrad))
					&& (curr_tlv->value[0] < ECG_PARAMS_ALGORITHM_VALID_VALUE_MAX)
					&& (curr_tlv->value[0] > ECG_PARAMS_ALGORITHM_VALID_VALUE_MIN))
				memcpy(&ecg_params_struct.ecg_algorithm.limit_ebrad, &curr_tlv->value[0],
						sizeof(ecg_params_struct.ecg_algorithm.limit_ebrad));
			else
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_ETACH_LIMIT_1:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.ecg_algorithm.limit_etach))
					&& (curr_tlv->value[0] < ECG_PARAMS_ALGORITHM_VALID_VALUE_MAX)
					&& (curr_tlv->value[0] > ECG_PARAMS_ALGORITHM_VALID_VALUE_MIN))
				memcpy(&ecg_params_struct.ecg_algorithm.limit_etach, &curr_tlv->value[0],
						sizeof(ecg_params_struct.ecg_algorithm.limit_etach));
			else
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_APNEA_DETECTION_LIMIT:
		{
			if ((curr_tlv->length == sizeof(ecg_params_struct.respiration_algorithm.limit_apnea))
					&& (curr_tlv->value[0] < ECG_RESPIRATION_APNEA_DURATION_MAX))
				ecg_params_struct.respiration_algorithm.limit_apnea = (ECG_RESPIRATION_APNEA_DURATION_E) curr_tlv->value[0];
			else
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_MAXIMUM_RESPIRATION_RATE:
		{
			if (curr_tlv->length != sizeof(ecg_params_struct.respiration_algorithm.respiration_rate_max))
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
			memcpy(&ecg_params_struct.respiration_algorithm.respiration_rate_max, &curr_tlv->value[0],
					sizeof(ecg_params_struct.respiration_algorithm.respiration_rate_max));
			if ( (ecg_params_struct.respiration_algorithm.respiration_rate_max > ECG_PARAMS_RESP_ALGO_RESP_RATE_MAX) ||
					(ecg_params_struct.respiration_algorithm.respiration_rate_max < ECG_PARAMS_RESP_ALGO_RESP_RATE_MIN) )
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
		}
			break;
		case PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_MINIMUM_RESPIRATION_RATE:
		{
			if (curr_tlv->length != sizeof(ecg_params_struct.respiration_algorithm.respiration_rate_min))
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}
			memcpy(&ecg_params_struct.respiration_algorithm.respiration_rate_min, &curr_tlv->value[0],
					sizeof(ecg_params_struct.respiration_algorithm.respiration_rate_min));
			if ( (ecg_params_struct.respiration_algorithm.respiration_rate_min > ECG_PARAMS_RESP_ALGO_RESP_RATE_MAX) ||
					(ecg_params_struct.respiration_algorithm.respiration_rate_min < ECG_PARAMS_RESP_ALGO_RESP_RATE_MIN) )
			{
				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
				return DEVICE_RESULT_WRONG_PARAM;
			}


		}
			break;
		default:
			pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
			return DEVICE_RESULT_WRONG_PARAM;
			break;

		}
		curr_tlv = curr_tlv->next;
	}

	ECG_RESULT ecg_res = ecgSetConfig(&ecg_params_struct);
	if (ecg_res != ECG_RESULT_OK)
	{
		pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
		return DEVICE_RESULT_WRONG_PARAM;
	}
	// TODO: save to flash ?
//	DEVICE_RESULT_ENUM device_res = prvDeviceSaveEcgConfig(&ecg_params_struct);
//
//	if (device_res != DEVICE_RESULT_OK)
//	{
//		LOG("\r\nprvDeviceEcgConfigSet error writing to Flash");
//	}

	pbsSendECGAlgoConfigSetResp(PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_STATUS_OK);

	return DEVICE_RESULT_OK;
}


static DEVICE_RESULT_ENUM prvDeviceStopMeasurement(PBS_RX_PAYLOAD_STRUCT payload)
{//TODO: is this function in use?
    if (device_state == DEVICE_STATE_RECORDING || device_state == DEVICE_STATE_CONTACT_DETECTION )
    {
        ecgStop();
//		prvDeviceSetDeviceStates(DEVICE_CONNECTION_MAX, DEVICE_STATE_IDLE);
    }
 	return DEVICE_RESULT_OK;
}

//static DEVICE_RESULT_ENUM prvDeviceWriteBootloader(void)
//{
//
////	bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, BSP_PIN_SET);
////	osDelay(1);
////	bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, BSP_PIN_RESET);
//
//	if (flash_if_erase_hw_sector_spec(FLASH_BOOTLOADER_SECTOR_1) != FLASH_OK)
//		return RESULT_ERROR;
//	if (flash_if_erase_hw_sector_spec(FLASH_BOOTLOADER_SECTOR_2) != FLASH_OK)
//		return RESULT_ERROR;
//
////	bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, BSP_PIN_SET);
////	osDelay(1);
////	bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, BSP_PIN_RESET);
//
//	if (flash_if_write_sync(device_bootloader_address, sizeof(device_bootloader_fw), (uint8_t*)device_bootloader_fw)!= FLASH_OK)
//		return RESULT_ERROR;
//
////	bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, BSP_PIN_SET);
////	osDelay(1);
////	bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, BSP_PIN_RESET);
//
//	return DEVICE_RESULT_OK;
//}

static RESULT_E prvDevicePrintInfo(void)
{
	uint8_t				hw_ver = 0;
	uint16_t 			volt;
	uint8_t				gmp_id[DEVICE_SN_LENGTH + 1] = {0};
	char				ble_device_name[BLE_DEVICE_NAME_MAX_LENGTH + 1] = {0};

	LOG("\r\n****************** GMP ******************\r\n");
//	LOG("\r\nSD card 20 seconds version, ECG samples prints\r\n");
#ifdef VSMS_EXTENDED_HOLTER_MOCKUP
	LOG("\r\nVSMS mockup version for demo only, sends ECG data which is saved on SD card\r\n");
#endif //VSMS_EXTENDED_HOLTER_MOCKUP
	LOG("FW build time and date: %s %s\r\n", __TIME__, __DATE__);

	LOG("Free heap size: %d / %d\r\n", xPortGetFreeHeapSize(), configTOTAL_HEAP_SIZE);

	LOG("Firmware version %d.%d.%d.%d\r\n", FW_MAJOR_VERSION, FW_MINOR_VERSION, FW_PATCH_VERSION, FW_BUILD_VERSION);

//	LOG("\r\n watchdoc_count = %d\r\n", watchdoc_count);

	uint32_t sys_time = osKernelSysTick();
	LOG("System time = %d milisec\r\n", sys_time);

//	BAT_MON_RESULT bm_res = batMonBatLevelGet(&volt);
//	if (bm_res != BAT_MON_RESULT_OK)
//		LOG("Couldn't read battery, res = %d\r\n", bm_res);
//	else
//		LOG("Battery: %dmV\r\n", volt);

	float32_t temperature = 0;
	if (bspReadTemp(&temperature) == BSP_RESULT_OK)
		LOG("Current Temperature: %.02fC\r\n", temperature);
	else
		LOG("Temperature is not available yet\r\n");

//	if (bspReadAdc(BSP_ADC_HW_IND, &adc_value) != BSP_RESULT_OK)
	if (bspHwVerGet(&hw_ver) != BSP_RESULT_OK)
		LOG("Couldn't read HW revision\r\n");
	else
		LOG("HW: %d\r\n", hw_ver);

	prvDeviceMcuIdGet();

	LOG("MCU ID: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\r\n", serial_number[0],
					serial_number[1], serial_number[2], serial_number[3], serial_number[4], serial_number[5],
					serial_number[6], serial_number[7], serial_number[8], serial_number[9], serial_number[10], serial_number[11]);
	if (prvDeviceIdGet(gmp_id) == DEVICE_RESULT_OK)
		LOG("Device ID: %s\r\n", gmp_id);
	else
		LOG("No Device ID\r\n");

	if (bleDeviceNameGet(ble_device_name) == BLE_RESULT_OK)
		LOG("BLE Device Name: %s\r\n", ble_device_name);
	else
		LOG("No BLE Device Name\r\n");

	LOG("Device State: %d\r\n", device_state);

	prvDevicePrintTime();

	return RESULT_OK;
}

static RESULT_E prvDevicePowerOnLeds(void)
{

	bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, BSP_PIN_SET);
	osDelay(500);
	bsp_write_pin(BSP_PIN_TYPE_LED_BLUE, BSP_PIN_RESET);
	bsp_write_pin(BSP_PIN_TYPE_LED_RED, BSP_PIN_SET);
	osDelay(500);
	bsp_write_pin(BSP_PIN_TYPE_LED_RED, BSP_PIN_RESET);

	return RESULT_OK;
}


static DEVICE_RESULT_ENUM deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ENUM err)
{
	PBS_RX_PAYLOAD_STRUCT payload;
	payload.tlv_list = NULL;

	device_bit |= err;	// accumulate any event, to be sent upon get status command

	leds_set_state(LEDS_STATE_CONTACT_DETECTION_PHASE, LEDS_DEBUG_MODE_OFF);
	leds_set_state(LEDS_STATE_ERROR_STATE, LEDS_DEBUG_MODE_ON);
	prvDeviceStopMeasurement(payload);
	prvDeviceSetState(DEVICE_STATE_ERROR);

	return DEVICE_RESULT_OK;
}

DEVICE_RESULT_ENUM pbsSetDeviceErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ENUM error)
{
	return deviceSetErrorState(error);
}

static RESULT_E prvDeviceSnSet(uint8_t* data)
{
	char 			device_sn_read[DEVICE_SN_LENGTH + 1] = {0};
	uint8_t			empty_sn[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

	if (flashReadOtp(DEVICE_SN_ADDRESS, (uint8_t*)device_sn_read, DEVICE_SN_LENGTH) != FLASH_OK)
	{
		LOG("Device Serial Number write failed\r\n");
		return RESULT_ERROR;
	}

	if (memcmp(device_sn_read, empty_sn, sizeof(DEVICE_SN_LENGTH)) != 0)
	{
		LOG("Device Serial Number already written\r\n");
		return RESULT_ERROR;
	}
	memset(device_sn_read, 0, DEVICE_SN_LENGTH);

	if (strlen((char*)data) != DEVICE_SN_LENGTH)
	{
		LOG("Length of serial number is wrong\r\n");
		return RESULT_ERROR;
	}

	if (flashWriteOtp(DEVICE_SN_ADDRESS, data, DEVICE_SN_LENGTH) != FLASH_OK)
	{
		LOG("Device Serial Number write failed\r\n");
		return RESULT_ERROR;
	}

	if (flashReadOtp(DEVICE_SN_ADDRESS, (uint8_t*)device_sn_read, DEVICE_SN_LENGTH) != FLASH_OK)
	{
		LOG("Device Serial Number write failed\r\n");
		return RESULT_ERROR;
	}

	if (strcmp((char*)data, device_sn_read) != 0)
	{
		LOG("Device Serial Number write failed\r\n");
		return RESULT_ERROR;
	}

	LOG("Device Serial Number write succeeded\r\n");
	return RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceIdGet(uint8_t *device_id_num)
{
	if (flashReadOtp(DEVICE_SN_ADDRESS, (uint8_t*)device_id_num, DEVICE_SN_LENGTH) == FLASH_OK)
		return DEVICE_RESULT_OK;
	else
		return DEVICE_RESULT_ERROR;
}

static DEVICE_RESULT_ENUM prvDeviceStartBatteryTraining(void)
{

	BAT_MON_RESULT res = batMonStartBatteryTraining();

	return (res == BAT_MON_RESULT_OK ? DEVICE_RESULT_OK : DEVICE_RESULT_ERROR);
}


static DEVICE_RESULT_ENUM prvDevicePrintTime(void)
{
	BSP_TIME_STRUCT time;
	BSP_DATE_STRUCT date;

	bspRtcTimeGet(&time, &date);
	LOG("%02d:%02d:%02d\r\n", time.hours, time.minutes, time.seconds);
	LOG("%02d/%02d/%02d\r\n", date.date, date.month, date.year);

	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceCheckSdFreeSpace(uint32_t min_free_space)
{
	uint32_t		sd_total_size, sd_free_size;

//	prvDevicePrintTime();
	LOG("Checking SD card free space...\r\n");
	sdGetSdInfo(&sd_total_size, &sd_free_size);
	if (sd_total_size < DEVICE_SD_CARD_MIN_SD_CAPACITY)
	{
		LOG_ERR("SD card is too small: %d\r\n", sd_total_size);
		return DEVICE_RESULT_ERROR;
	}

	if (sd_free_size < min_free_space)
	{
		LOG_ERR("There is not enough space on SD card: %d\r\n", sd_free_size);
		return DEVICE_RESULT_ERROR;
	}

	LOG("SD capacity: %dKB\r\n", sd_total_size);
	LOG("SD free space: %dKB\r\n", sd_free_size);
	LOG("SD used space: %dKB\r\n", (sd_total_size - sd_free_size));


//	prvDevicePrintTime();
	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceWriteChecksum(uint32_t checksum)
{
	FLASH_RES 	res;

	res = flashEraseData(DEVICE_FLASH_PAGE_CHECKSUM, 1);
	if (res != FLASH_OK)
		return DEVICE_RESULT_FLASH_ERROR;

	res = flashWriteData(DEVICE_FLASH_PAGE_CHECKSUM, sizeof(uint32_t), (uint8_t *)&checksum);//Address 0x807F000
	if (res != FLASH_OK)
		return DEVICE_RESULT_FLASH_ERROR;

	if (memcmp((uint8_t*)&checksum, (uint8_t*)FLASH_CHECKSUM_ADDRESS, sizeof(uint32_t)) != 0)
		return DEVICE_RESULT_FLASH_ERROR;

	return DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceSetChecksum(void)
{
	uint32_t checksum = 0;

	if (flashCalcFwChecksum(FLASH_APP_MAX_SIZE, &checksum) != FLASH_OK)
		return DEVICE_RESULT_ERROR;

	if (prvDeviceWriteChecksum(checksum) != DEVICE_RESULT_OK)
		return DEVICE_RESULT_ERROR;

	return DEVICE_RESULT_OK;
}

typedef struct {
	uint8_t code;
	char * str;
}DEVICE_SYSTEM_LOG_EVENT_STRINGS_MAP;

typedef enum {
	DEVICE_SYSTEM_LOG_EVENT_CODE_START_UP,
	DEVICE_SYSTEM_LOG_EVENT_CODE_CONNECTED,
	DEVICE_SYSTEM_LOG_EVENT_CODE_DISCONNECTED,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_ECG,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_BLE,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_SD,		// probably won't be written anyway..
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_RTC,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_DEVICE,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_SERIAL_PROTOCOL,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_ACCELEROMETER,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_FLASH,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_BATTERY_MONITOR,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_LEDS,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_PBS,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_PBS_TX,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_IMPEDANCE,
	DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_PACE_MAKER,
	DEVICE_SYSTEM_LOG_EVENT_CODE_MAX
}DEVICE_SYSTEM_LOG_EVENT_CODE_E;


DEVICE_SYSTEM_LOG_EVENT_STRINGS_MAP sys_log_strings[] = {
	{DEVICE_SYSTEM_LOG_EVENT_CODE_START_UP             			, "System Initialized"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_CONNECTED             		, "GW connected"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_DISCONNECTED             		, "GW disconnected"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_ECG             	, "ECG init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_BLE             	, "BLE init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_SD		        , "SD init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_RTC             	, "RTC init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_DEVICE            , "Device init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_SERIAL_PROTOCOL   , "Serial protocol init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_ACCELEROMETER     , "Accelerometer init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_FLASH             , "Flash init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_BATTERY_MONITOR   , "Battery monitor init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_LEDS             	, "LEDs init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_PBS             	, "PBS init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_PBS_TX            , "PBS Tx task init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_IMPEDANCE         , "Impedance init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_INIT_FAILED_PACE_MAKER        , "Pace maker init failed"},
	{DEVICE_SYSTEM_LOG_EVENT_CODE_MAX        					, ""},
};

#define SYSTEM_LOG_START_DELIM				"."
#define SYSTEM_LOG_ENTRY_LENGTH				(BSP_TIMESTAMP_LENGTH + sizeof(SYSTEM_LOG_START_DELIM) + sizeof(uint16_t))
#define SYSTEM_LOG_FILE_NAME				"syslog.txt"
#define SYSTEM_LOG_LEGEND_LINE_MAX_LENGTH	30

static DEVICE_RESULT_ENUM prvDeviceSystemLogLegend()
{
	uint8_t i, line_length;
	uint8_t log_payload[SYSTEM_LOG_LEGEND_LINE_MAX_LENGTH];
	line_length = snprintf(log_payload,SYSTEM_LOG_LEGEND_LINE_MAX_LENGTH, "Log legend:[code]=[error]\r\n");
	sdWriteDataToFile(SYSTEM_LOG_FILE_NAME, log_payload, line_length, NULL);

	for (i = 0; i<(uint8_t)DEVICE_SYSTEM_LOG_EVENT_CODE_MAX; i++)
	{
		line_length = snprintf(log_payload, SYSTEM_LOG_LEGEND_LINE_MAX_LENGTH, "%d=%s\r\n", sys_log_strings[i].code, sys_log_strings[i].str);
		sdWriteDataToFile(SYSTEM_LOG_FILE_NAME, log_payload, line_length, NULL);
	}
}

static DEVICE_RESULT_ENUM prvDeviceSetBleName(char* ble_name)
{

	if (strlen(ble_name) != BLE_DEVICE_NAME_MAX_LENGTH)
	{
		LOG("BLE name must be 8 characters\r\n");
		return DEVICE_RESULT_WRONG_PARAM;
	}

	if (bleAtCommandMode(BLE_MODE_AT_COMMAND) != BLE_RESULT_OK)
		return DEVICE_RESULT_ERROR;

	osDelay(2500);

	if (bleAtCommand("at$name", ble_name) != BLE_RESULT_OK)
		return DEVICE_RESULT_ERROR;

	osDelay(2000);

	if (bleAtCommandMode(BLE_MODE_NORMAL) != BLE_RESULT_OK)
		return DEVICE_RESULT_ERROR;

	return DEVICE_RESULT_OK;
}

//static DEVICE_RESULT_ENUM prvDeviceSystemLog(DEVICE_SYSTEM_LOG_EVENT_CODE_E code)
//{
//	if (code >= DEVICE_SYSTEM_LOG_EVENT_CODE_MAX)
//		DEVICE_RESULT_WRONG_PARAM;
//
//	uint8_t log_payload[SYSTEM_LOG_ENTRY_LENGTH] = {0};
//
//	uint16_t code_sh = code;
//
//	BSP_RTC_TIMESTAMP bcd;
//
//	bspRtcTimeGetBCD(&bcd);
//
//	log_payload[0] = (uint8_t)SYSTEM_LOG_START_DELIM;
//	memcpy(log_payload + sizeof(SYSTEM_LOG_START_DELIM), &bcd, sizeof(bcd));
//	memcpy(log_payload +sizeof(bcd) + sizeof(SYSTEM_LOG_START_DELIM), &code_sh, sizeof(code_sh));
//	sdSendMsgToQueue(SYSTEM_LOG_FILE_NAME, log_payload, sizeof(log_payload));
//	return DEVICE_RESULT_OK;
//}


static DEVICE_RESULT_ENUM prvDeviceAccelConfigSet(PBS_RX_PAYLOAD_STRUCT payload)
{
	PBS_TLV_LIST_STRUCT *curr_tlv = payload.tlv_list;

//	while (curr_tlv != NULL)
//	{
//		switch (curr_tlv->subtype)
//		{
//		case PBS_ACCELEROMETER_CONFIG_OBJECT_ACCELEROMETER_ENABLE:
//		{
//			if (curr_tlv->length == sizeof(ecg_params_struct.ecg_algorithm.limit_runs) )
//				memcpy(&ecg_params_struct.ecg_algorithm.limit_runs, &curr_tlv->value[0],
//						sizeof(ecg_params_struct.ecg_algorithm.limit_runs));
//			else
//			{
//				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
//				return DEVICE_RESULT_WRONG_PARAM;
//			}
//			if ( (ecg_params_struct.ecg_algorithm.limit_runs > ECG_PARAMS_ALGORITHM_VALID_VALUE_MAX) || (ecg_params_struct.ecg_algorithm.limit_runs < ECG_PARAMS_ALGORITHM_VALID_VALUE_MIN) )
//			{
//				pbsSendECGAlgoConfigSetResp(PBS_GENERAL_ERRORS_EVENTS_WRONG_PARAMETER);
//				return DEVICE_RESULT_WRONG_PARAM;
//			}
//		}
//			break;
//
//		curr_tlv = curr_tlv->next;
//	}

	//Send response

	return DEVICE_RESULT_OK;
}

void sdFailure(void)
{
	prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_SD");
	deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
}

void batMonBatteryValidState(BATT_MON_STATE_E state)
{
	if (state == BATT_MON_STATE_BATTERY_NOT_READY)
	{
		prvDeviceLogSd("PBS_DEVICE_STATUS_BIT_ERROR_BATTERY_MONITOR");
		deviceSetErrorState(PBS_DEVICE_STATUS_BIT_ERROR_BATTERY_MONITOR);
	}
	else if (state == BATT_MON_STATE_BATTERY_READY)
		prvDeviceSetState(DEVICE_STATE_IDLE);
	else
		LOG("Battery Monitor not recgonized: %d\r\n", state);
}
//static DEVICE_RESULT_ENUM prvDeviceLogSd(char *log_str)
static DEVICE_RESULT_ENUM prvDeviceLogSd(char* format, ...)
{
	va_list 					arg_list;
	BSP_RTC_TIMESTAMP 			time = {.field.hours = 0, .field.minutes = 0, .field.seconds = 0,	.field.date = 0, .field.month = 0, .field.year = 0};
	uint8_t						string_buffer[DEVICE_LOG_FILE_STR_MAX_LEN + 30] = {0};
	SD_RESULT_E					res;
	uint32_t					string_length = 0;

	va_start( arg_list, format );

	bspRtcTimeGetBCD(&time);

	sprintf((char*)string_buffer, "%02X:%02X:%02X %02X/%02X/%02X: ",
			time.field.hours, time.field.minutes, time.field.seconds,
			time.field.date, time.field.month, time.field.year);


	string_length = vsnprintf(&string_buffer[strlen(string_buffer)], DEVICE_LOG_FILE_STR_MAX_LEN, format, arg_list);


	sprintf((char*)&string_buffer[strlen(string_buffer)], "\r\n");

	res = sdWriteDataToFile(DEVICE_LOG_FILE_NAME, string_buffer, strlen((char*)string_buffer), NULL);

	if (res != SD_RESULT_OK)
		return 	DEVICE_RESULT_SD_ERROR;

	va_end(arg_list);

	return 	DEVICE_RESULT_OK;
}

static DEVICE_RESULT_ENUM prvDeviceTimersInit(void)
{

        //create timer for send sdcard data to ble every hour:
        osTimerDef(read_egc_from_sd_to_ble_hourly_timer, readEgcFromSdToBleHourlyCallback);
        read_egc_from_sd_to_ble_hourly_timer = osTimerCreate(osTimer(read_egc_from_sd_to_ble_hourly_timer), osTimerPeriodic, ( void * ) 0);

	osTimerDef(ecg_first_run_timer, ecgFirstRunTimerCallback);
	ecg_first_run_timer = osTimerCreate(osTimer(ecg_first_run_timer), osTimerOnce, ( void * ) 0);

	osTimerDef(led_timer, ledTimerCallback);
	led_timer = osTimerCreate(osTimer(led_timer), osTimerOnce, ( void * ) 0);

	osTimerDef(manual_event_timer, manualEventTimerCallback);
	manual_event_timer = osTimerCreate(osTimer(manual_event_timer), osTimerOnce, ( void * ) 0);

	osTimerDef(double_click_timer, doubleClickTimerCallback);
	double_click_timer = osTimerCreate(osTimer(double_click_timer), osTimerOnce, ( void * ) 0);

	osTimerDef(ble_holter_timer, bleHolterTimerCallback);
	ble_holter_timer = osTimerCreate(osTimer(ble_holter_timer), osTimerOnce, ( void * ) 0);

        //start hourly timer: every hour send last 10 minutes:
        osTimerStart(read_egc_from_sd_to_ble_hourly_timer, BLE_HOURLY_TIMER_PERIOD);

	return 	DEVICE_RESULT_OK;
}

EventGroupHandle_t getPanicButton(void)
{
	return panicButton;
}

osStatus turn_off_BLE_after_delay(uint8_t delay_in_seconds)
{
	return osTimerStart(ble_holter_timer, SECONEDS_TO_TICKS(delay_in_seconds));
}
/*
static DEVICE_RESULT_ENUM prvDeviceGetRespLastFileName(void)
{
	uint8_t 	i;
	uint8_t		file[15] = {0};//TODO:Constant
	uint32_t 	file_size = 0;
	SD_RESULT_E	sd_res = SD_RESULT_OK;

//	if (sdDirExist(PBS_SD_ECG_FILES_DIRECTORY) != SD_RESULT_OK)
//	{
//		if (sdMakeDir(PBS_SD_ECG_FILES_DIRECTORY)!=  SD_RESULT_OK)
//		{
//			LOG("Couldn't create ECG directory\r\n");
//			pbsSetDeviceErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
//			return PBS_RESULT_ERROR_SD;
//		}
//	}

	for(i = 1; i <= 200; i++)
	{
		sprintf((char*)file, "resp%03d.txt", i);
		if (sdFileExist(file) == SD_RESULT_ERROR_FILE_DOES_NOT_EXIST)
		{
			device_current_resp_file_num = i - 1;
			sprintf((char*)file, "resp%03d.txt", device_current_resp_file_num);
			sd_res = sdGetFileSize(file, &file_size);
			if ((sd_res == SD_RESULT_OK) && (file_size < 163840))
			{//if last file is smaller than 2048 Bytes delete it
				sdDeleteFile(file);
				strcpy((char*)device_current_resp_file, (char*)file);
				device_current_resp_file_num--;
				break;
			}
			sprintf((char*)file, "resp%03d.txt", i);
			strcpy((char*)device_current_resp_file, (char*)file);
			break;
		}
	}

	if (i > 200)
		return DEVICE_RESULT_ERROR;

	LOG("%d ECG files\r\n", (i - 1));

	sprintf((char*)device_current_resp_file, "resp%03d.txt", ++device_current_resp_file_num);

	return DEVICE_RESULT_OK;
}
*/