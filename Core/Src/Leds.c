//#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
//#include "timers.h"

#include "serial_protocol.h"
#include "Leds.h"
#include "BSP.h"

/****************************************************************/
/*						Local Definitions	 					*/

#define LEDS_NUM_OF_LEDS				3
#define LEDS_CONSTANT_VALUE        	 	0xFF

typedef enum{
	LEDS_OFFSET_BLUE 	= 0,
	LEDS_OFFSET_RED 	= 6
}LEDS_OFFSET_E;

/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/
void ledsTimerCallback(void const *n);
//static LEDS_RESULT leds_set_state(LEDS_STATES_E state);
LEDS_RESULT prvLedsControlDebug(LEDS_BITMAP_E led_bitmap, LEDS_DEBUG_MODE_E led_debug_mode);
/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
//static TimerHandle_t        leds_timer;
static osTimerId 			leds_timer;
//static uint8_t              timer_flag = FALSE;
//static BSP_LEDS_E           led;
static uint8_t           	led;
static uint32_t              led_counter;
//static uint8_t              led_current_counter = 0;
static uint32_t              led_current_counters[LEDS_NUM_OF_LEDS] = {0};
static LEDS_CONTROL_E		leds_control = LEDS_CONTROL_DEVICE;
static LEDS_TIME_E			leds_state_speed_recording = LEDS_TIME_HIGH;
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/

/****************************************************************/

/****************************************************************/
/*						Weak Functions							*/
__weak LEDS_RESULT ledsSync(void){return LEDS_RESULT_OK;}
/****************************************************************/

LEDS_RESULT ledsInit(void)
{
    osTimerDef(leds_timer, ledsTimerCallback);
	leds_timer = osTimerCreate(osTimer(leds_timer), osTimerPeriodic, ( void * ) 0);
	osTimerStart(leds_timer, pdMS_TO_TICKS(LEDS_DEFAULT_TIME));
	
    return LEDS_RESULT_OK;
}

LEDS_RESULT leds_bit(void)
{
	BSP_RESULT_E res = BSP_RESULT_OK;
	
	res |= bsp_led(BSP_LED_RED, BSP_LED_ON);
	osDelay(333);
	res |= bsp_led(BSP_LED_RED, BSP_LED_OFF);
	res |= bsp_led(BSP_LED_BLUE, BSP_LED_ON);
	osDelay(333);
	res |= bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
	
	return (res == BSP_RESULT_OK ? LEDS_RESULT_OK:LEDS_RESULT_ERROR);
}


void ledsTimerCallback(void const *n)
{
	uint8_t led_active = TRUE;
	uint8_t counter_active = TRUE;	
	
	if (ledsSync() != LEDS_RESULT_OK)
		led_active = FALSE;

	if (led & LEDS_BITMAP_RED)
	{
		if (led_current_counters[BSP_LED_RED] != LEDS_CONSTANT_VALUE)
		{
			if ((led_counter % led_current_counters[BSP_LED_RED]) == LEDS_OFFSET_RED)
			{
				if (led_active)
					bsp_led(BSP_LED_RED, BSP_LED_ON);
				else
					counter_active = FALSE;
			}
			else if ((led_counter % led_current_counters[BSP_LED_RED]) == (LEDS_OFFSET_RED + 1))
				bsp_led(BSP_LED_RED, BSP_LED_OFF);
		}
	}

	if (led & LEDS_BITMAP_BLUE)
	{
		if (led_current_counters[BSP_LED_BLUE] != LEDS_CONSTANT_VALUE)
		{
			if ((led_counter % led_current_counters[BSP_LED_BLUE]) == LEDS_OFFSET_BLUE)
			{
				if (led_active)
					bsp_led(BSP_LED_BLUE, BSP_LED_ON);
				else
					counter_active = FALSE;
			}
			else if ((led_counter % led_current_counters[BSP_LED_BLUE]) == (LEDS_OFFSET_BLUE + 1))
				bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
		}
	}
	
	

	if (counter_active)
		led_counter = (led_counter + 1) % LEDS_TIME_SUPER_SLOW;	//LEDS_TIME_MEDIUM;
		

//	ledsSync(LEDS_MUTEX_COMMAND_RELEASE);
}

LEDS_RESULT leds_set_state(LEDS_STATES_E state, LEDS_DEBUG_MODE_E leds_mode)
{
    
	if (leds_control == LEDS_CONTROL_DEBUG)
		return LEDS_RESULT_ERROR_LEDS_IN_DEBUG_MODE;	
	
    switch (state)
    {
        case LEDS_STATE_ERROR_STATE:
//        {
//			osTimerStop( leds_timer);
//			bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
//			bsp_led(BSP_LED_RED, BSP_LED_ON);
//        }
        {
			if (leds_mode)
			{
				led |= LEDS_BITMAP_RED;
				led_current_counters[BSP_LED_RED] = LEDS_TIME_SUPER_HIGH;
			}
			else
			{
				bsp_led(BSP_LED_RED, BSP_LED_OFF);
				led &= ~LEDS_BITMAP_RED;
			}
        }
		break;

//        case LEDS_STATE_LOW_BATTERY:
//        {
//			if (leds_mode)
//			{
//				led |= LEDS_BITMAP_RED;
//				led_current_counters[BSP_LED_RED] = LEDS_TIME_HIGH;
//			}
//			else
//			{
//				bsp_led(BSP_LED_RED, BSP_LED_OFF);
//				led &= ~LEDS_BITMAP_RED;
//			}
//        }
//        break;        
		case LEDS_STATE_CRITICALLY_LOW_BATTERY:
		{
			osTimerStop( leds_timer);
			bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
			bsp_led(BSP_LED_RED, BSP_LED_ON);				
		}
		break;
		
//        case LEDS_STATE_CRITICALLY_LOW_BATTERY:
//        {
//			if (leds_mode)
//			{
//				led |= LEDS_BITMAP_RED;
//				led_current_counters[BSP_LED_RED] = LEDS_TIME_MEDIUM;
//			}
//			else
//			{
//				bsp_led(BSP_LED_RED, BSP_LED_OFF);
//				led &= ~LEDS_BITMAP_RED;
//			}
//        }
//        break;		
		
        case LEDS_STATE_POWER_ON:	// TODO: validate it's ok (no "idle" in SRS)
		{
			
		}
		break;
		
//        case LEDS_STATE_BLE_ADVERTISEMENT:
//        {
// 			if (leds_mode)
//			{
//				led |= LEDS_BITMAP_BLUE;
//				led_current_counters[BSP_LED_BLUE] = LEDS_TIME_HIGH;
//			}
//			else
//			{
//				bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
//				led &= ~LEDS_BITMAP_BLUE;
//			}
//        }
//        break;

        case LEDS_STATE_CONTACT_DETECTION_PHASE:
        {
			if (leds_mode)
			{
				led |= LEDS_BITMAP_ALL;
				led_current_counters[BSP_LED_BLUE] = LEDS_TIME_MEDIUM;
				led_current_counters[BSP_LED_RED] = LEDS_TIME_MEDIUM;
			}
			else
			{
				bsp_led(BSP_LED_RED, BSP_LED_OFF);
				bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
				led &= ~LEDS_BITMAP_ALL;
			}
        }
        break;        
		
        case LEDS_STATE_RECORDING:
        {
			if (leds_mode)
			{
				led |= LEDS_BITMAP_BLUE;
				led_current_counters[BSP_LED_BLUE] = leds_state_speed_recording;//LEDS_TIME_SUPER_SLOW;
			}
			else
			{
				bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
				led &= ~LEDS_BITMAP_BLUE;
			}
        }
        break;        
        
        case LEDS_STATE_OFF:
        {
            led = LEDS_BITMAP_OFF;
			bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
			bsp_led(BSP_LED_RED, BSP_LED_OFF);
            //led_counter = 0xFF;
        }
        break;
		
		
		
        default:
            LOG("Error: state %d does not exist\r\n", state);
        break;
    }
    
    //xTimerStart( leds_timer, 0);
    
    return LEDS_RESULT_OK;
}


LEDS_RESULT prvLedsControlDebug(LEDS_BITMAP_E led_bitmap, LEDS_DEBUG_MODE_E led_debug_mode)
{
	uint8_t mask = 1;

	for (uint8_t i = 0;i < 3;i++)	
	{
		if (led_bitmap & mask)
		{
			if (led_debug_mode == LEDS_DEBUG_MODE_ON)
				bsp_led((BSP_LEDS_E)i, BSP_LED_ON);
			else if (led_debug_mode == LEDS_DEBUG_MODE_OFF)
				bsp_led((BSP_LEDS_E)i, BSP_LED_OFF);
		}
		
		mask <<= 1;
	}
	
	return LEDS_RESULT_OK;
}

LEDS_RESULT ledsDeviceControl(LEDS_BITMAP_E led_bitmap, LEDS_MODE_E led_mode)
{
	static uint8_t first_debug = TRUE;
	
	if (led_mode == LEDS_MODE_AUTOMATIC)
	{
		prvLedsControlDebug(LEDS_BITMAP_ALL, LEDS_DEBUG_MODE_OFF);
		leds_control = LEDS_CONTROL_DEVICE;
		first_debug = TRUE;
		return LEDS_RESULT_OK;
	}
	else
	{
		if (first_debug)
		{
			leds_set_state(LEDS_STATE_OFF, LEDS_DEBUG_MODE_OFF);
			prvLedsControlDebug(LEDS_BITMAP_ALL, LEDS_DEBUG_MODE_OFF);
			leds_control = LEDS_CONTROL_DEBUG;
			first_debug = FALSE;
		}
	}
	if (led_mode == LEDS_MODE_ON)
		prvLedsControlDebug(led_bitmap, LEDS_DEBUG_MODE_ON);
	else
		prvLedsControlDebug(led_bitmap, LEDS_DEBUG_MODE_OFF);
	
	return LEDS_RESULT_OK;
}

LEDS_RESULT ledsSetLedStateInterval(IN LEDS_STATES_E state, IN LEDS_TIME_E interval)
{
	
	switch (state)
	{
		case LEDS_STATE_RECORDING:
		{
			leds_state_speed_recording = interval;
		}
		break;
		
		default:
		{
			LOG("State %d interval cannot be set\r\n", state);
			return LEDS_RESULT_ERROR_STATE_DOES_NOT_EXIST;
		}
		break;
	}
	
	return LEDS_RESULT_OK;
}

LEDS_RESULT ledsBlink(LEDS_BITMAP_E leds, uint32_t duration)
{
	if (duration > 2000)
		return LEDS_RESULT_ERROR_DUARATION_TOO_LONG;
	
	switch (leds)
	{
		case LEDS_BITMAP_RED:
		{
			bsp_led(BSP_LED_RED, BSP_LED_ON);
			osDelay(duration);
			bsp_led(BSP_LED_RED, BSP_LED_OFF);
		}
		break;

		case LEDS_BITMAP_BLUE:
		{
			bsp_led(BSP_LED_BLUE, BSP_LED_ON);
			osDelay(duration);
			bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
		}
		break;
		
		case LEDS_BITMAP_ALL:
		{
			bsp_led(BSP_LED_RED, BSP_LED_ON);
			bsp_led(BSP_LED_BLUE, BSP_LED_ON);
			osDelay(duration);
			bsp_led(BSP_LED_RED, BSP_LED_OFF);
			bsp_led(BSP_LED_BLUE, BSP_LED_OFF);
		}
		break;		
		
		default:
		{
			return LEDS_RESULT_ERROR_WRONG_LED_INDEX;
		}
		break;
		
	}
	
	return LEDS_RESULT_OK;
}