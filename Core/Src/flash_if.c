#include "flash_if.h"
#include "serial_protocol.h"
#include "cmsis_os.h"
#include "BSP.h"


// HW FS definitions
#define FLASH_HW_BOOTLOADER_BASE_ADDRESS 			0x08000000

//OTP
#define FLASH_OTP_START_ADDR						0x1FFF7000
//#define FLASH_OTP_LOCK_ADDR							0x1FFF7A00
#define FLASH_OTP_BLOCKS							128
#define FLASH_OTP_BYTES_IN_BLOCK					sizeof(long long)
#define FLASH_OTP_SIZE								(FLASH_OTP_BLOCKS * FLASH_OTP_BYTES_IN_BLOCK)


static osMutexId flashMutex = null;

//initializes flash file system
FLASH_RES flashInit(void){
	FLASH_RES res = FLASH_OK;

	//return if already initialized
//	if (used_file_table != FLASH_UNUSED_FAT)
//		return FLASH_OK;

	//init mutex
	if (flashMutex == null)
	{
		osMutexDef(flashMutex);
		flashMutex = osMutexCreate(osMutex(flashMutex));
		if (flashMutex == null)
			return FLASH_ERROR_COULD_NOT_CREATE_MUTEX;
	}
//	osMutexWait(flashMutex, osWaitForever);

//	osMutexRelease(flashMutex);

//	flash_if_set_debug_flag();
	
	return res;
}


FLASH_RES flashWriteOtp(uint32_t offset, uint8_t* data, uint16_t len)
{
	BSP_RESULT_E			res;
	uint32_t 				byte_written = 0, address;
	static uint8_t			write_buff[8] = {0};
	
	
	if ((offset % FLASH_OTP_BYTES_IN_BLOCK) != 0)
		return FLASH_ERROR_ADDRESS_NOT_VALID;
	
	if ((len > (FLASH_OTP_SIZE - offset)) && (len != 0))
	{
		return FLASH_ERROR_LENGTH_TOO_LONG;
	}	
	
	osMutexWait(flashMutex, osWaitForever);
	
	if (bspFlashUnlock() != BSP_RESULT_OK)
	{
		osMutexRelease(flashMutex);
		return FLASH_ERROR_COULD_NOT_UNLOCK_FLASH;
	}

	memset(write_buff, 0xFF, sizeof(write_buff));
	
	address = FLASH_OTP_START_ADDR + offset;
	
	while (byte_written < len)
	{
		if ((len - byte_written)>= 8)
		{
			res = bspFlashProgramDw(address + byte_written, *(uint64_t*)(data + byte_written));
			byte_written += sizeof(uint64_t);
		}
		else
		{
			memcpy(write_buff, (uint8_t*)(data + byte_written), len - byte_written);
			res = bspFlashProgramDw(address + byte_written, *(uint64_t*)(write_buff));

			byte_written += (len - byte_written);
		}

		if (res != BSP_RESULT_OK)
		{
			bspFlashLock();
			osMutexRelease(flashMutex);
			return FLASH_ERROR_WRITE_TO_FLASH;
		}		

	}
	
	if (bspFlashLock() != BSP_RESULT_OK)
	{
		osMutexRelease(flashMutex);
		return FLASH_ERROR_COULD_NOT_LOCK_FLASH;	
	}
	
	osMutexRelease(flashMutex);	
	
	return FLASH_OK;
}

FLASH_RES flashReadOtp(uint32_t offset, uint8_t* data, uint8_t length) 
{

	uint16_t length_left = length;
	uint16_t length_read = 0;
	
	uint8_t* address = (uint8_t*)(FLASH_OTP_START_ADDR + offset);

	while (length_left > 0)
	{
		if (length_left >= FLASH_OTP_BYTES_IN_BLOCK)
		{
			memcpy(&data[length_read], address, FLASH_OTP_BYTES_IN_BLOCK);
			length_left -= FLASH_OTP_BYTES_IN_BLOCK;
			length_read += FLASH_OTP_BYTES_IN_BLOCK;
			address += FLASH_OTP_BYTES_IN_BLOCK;
		}
		else
		{
			memcpy(&data[length_read], address, length_left);
			length_left -= length_left;
		}
		
	}
	
	return FLASH_OK;
}

FLASH_RES flashWriteData(uint32_t page_num, uint32_t len,  uint8_t* data)
{
	BSP_RESULT_E			res;
	uint32_t 				byte_written = 0, address;
	static uint8_t			write_buff[8] = {0};

	if (flashMutex == NULL)
		return FLASH_ERROR_NOT_INITIALIZED;
	
	if (page_num > FLASH_NUM_OF_PAGES) 
		return FLASH_ERROR_ADDRESS_OUT_OF_BOUNDS;

	if (len > FLASH_IF_PAGE_SIZE)
		return FLASH_ERROR_LENGTH_TOO_LONG;
	
	osMutexWait(flashMutex, osWaitForever);	
	
	memset(write_buff, 0xFF, sizeof(write_buff));
	
	address = FLASH_HW_BOOTLOADER_BASE_ADDRESS + (page_num * FLASH_IF_PAGE_SIZE);
	
	res = bspFlashUnlock();
	if (res != BSP_RESULT_OK)
	{
		osMutexRelease(flashMutex);
		return FLASH_ERROR_WRITE_TO_FLASH;	
	}
	

	while (byte_written < len)
	{
		//if (((int32_t)(byte_written - len) >= 8) && (((address + byte_written) & 0x7) == 0))
		if ((len - byte_written)>= 8)
		{
			res = bspFlashProgramDw(address + byte_written, *(uint64_t*)(data + byte_written));
			byte_written += sizeof(uint64_t);
		}
		else
		{
			memcpy(write_buff, (uint8_t*)(data + byte_written), len - byte_written);
			res = bspFlashProgramDw(address + byte_written, *(uint64_t*)(write_buff));
			
//			res = bspFlashProgramByte(address + byte_written, *(data + byte_written));
			byte_written += (len - byte_written);
		}

		if (res != BSP_RESULT_OK)
		{
			bspFlashLock();
			osMutexRelease(flashMutex);
			return FLASH_ERROR_WRITE_TO_FLASH;
		}		

	}	
	
	res = bspFlashLock();
	osMutexRelease(flashMutex);

	if (res != BSP_RESULT_OK)
	{
		return FLASH_ERROR_WRITE_TO_FLASH;	
	}

	
	return FLASH_OK;
}

FLASH_RES flashReadData(uint32_t page_num, uint32_t len,  uint8_t* data)
{
	uint32_t 				address;
	
	if (flashMutex == NULL)
		return FLASH_ERROR_NOT_INITIALIZED;

	if (page_num > FLASH_NUM_OF_PAGES) 
		return FLASH_ERROR_ADDRESS_OUT_OF_BOUNDS;

	if (len > FLASH_IF_PAGE_SIZE)
		return FLASH_ERROR_LENGTH_TOO_LONG;	
	
	osMutexWait(flashMutex, osWaitForever);

	address = FLASH_HW_BOOTLOADER_BASE_ADDRESS + (page_num * FLASH_IF_PAGE_SIZE);
	
	memcpy(data, (uint8_t*)address, len);
	
	osMutexRelease(flashMutex);

	return FLASH_OK;
}

FLASH_RES flashEraseData(uint32_t page_num, uint8_t num_of_pages)
{
	BSP_RESULT_E			res;
	
	if (num_of_pages > FLASH_NUM_OF_PAGES) 
		return FLASH_ERROR_ADDRESS_OUT_OF_BOUNDS;

	osMutexWait(flashMutex, osWaitForever);	
	
	res = bspFlashUnlock();
	if (res != BSP_RESULT_OK)
	{
		osMutexRelease(flashMutex);
		return FLASH_ERROR;	
	}
	

	res = bspFlashPagesErase(page_num, num_of_pages);
	if (res != BSP_RESULT_OK)
	{
		bspFlashLock();
		osMutexRelease(flashMutex);
		return FLASH_ERROR;
	}		
	
	
	res = bspFlashLock();

	osMutexRelease(flashMutex);

	if (res != BSP_RESULT_OK)
	{
		return FLASH_ERROR;	
	}
	
	return FLASH_OK;
}

FLASH_RES flashCalcFwChecksum(uint32_t size, uint32_t* checksum)
{
  uint32_t i;
  uint32_t end_addr = FLASH_BASE_ADDRESS + FLASH_APPLICATION_OFFSET + (size & 0xFFFFFFFC);
  
  for (i = FLASH_BASE_ADDRESS + FLASH_APPLICATION_OFFSET ; i < end_addr ; i = i + 4)
    *checksum ^= *(uint32_t *)i;
  
  return FLASH_OK;
}


//FLASH_RES flashTest(void)
//{
//	FLASH_RES 		res = 0;
//	uint8_t			i = 0;
//	static uint8_t	tmp_buff_write[200] = {0};
//	static uint8_t	tmp_buff_read[200] = {0};
//	
//	memset(tmp_buff_read, 0xFF, sizeof(tmp_buff_read));
//	
//	for(i = 0;i < 100; i++)
//	{
//		res |= flashEraseData(150 , 1);
//		memset(tmp_buff_read, 0xFF, sizeof(tmp_buff_read));
//		tmp_buff_write[0] = i;
//		tmp_buff_write[sizeof(tmp_buff_write) - 1] = i;
//		res |= flashWriteData(150 ,sizeof(tmp_buff_write), tmp_buff_write);
//		res |= flashReadData(150 , sizeof(tmp_buff_read), tmp_buff_read);
//		if (memcmp(tmp_buff_read, tmp_buff_write, sizeof(tmp_buff_write)) == 0)
//			PRINT("Flash %d succeeded\r\n", i);
//		else
//			PRINT("Flash %d failed\r\n", i);
//	}
//
//	res |= flashEraseData(150 , 100);
//	
//	return FLASH_OK;
//}


//void print_fs(BOOL verbose)
//{
//	FILE_DESC * fileDesc;
//	uint32_t space, size = 0, sizeAligned = 0;
//	int i, fileNum = 0;
//	//check initialized
//	if (used_file_table == FLASH_UNUSED_FAT)
//	{
//		PRINT("\r\nError: File system not initialized");
//		return;
//	}
//
//	PRINT("\r\nFile system information:");
//	PRINT("\r\nVersion : %d", file_table[used_file_table]->ver);
//	PRINT("\r\nTable number: %d", used_file_table);
//	PRINT("\r\nTable address: 0x%08X", file_table[used_file_table]);
//	PRINT("\r\nTable index:%d\r\n", file_table[used_file_table]->index);
//
//	PRINT("\r\nFile list:");
//	for (i=0;i<= last_used_file; i++)
//	{
//		fileDesc = FILE_GET_DESCRIPTOR(i);
//		if (fileDesc->state == FILE_STATE_OK)
//			fileNum++;
//		else if (verbose == FALSE)
//			continue;
//		size += fileDesc->size;
//		sizeAligned += (fileDesc->size + FLASH_FILE_ALIGN - 1) & ~ (FLASH_FILE_ALIGN - 1);
//		PRINT("\r\n%d\t%s\t- %dB\tAddress:0x%08X\tstate:%s",i,fileDesc->name, fileDesc->size, (uint32_t)fileDesc->filePtr, (fileDesc->state == FILE_STATE_OK)?"OK": ((fileDesc->state == FILE_STATE_IN_WRITING)?"Open":"Invalid"));
//
//	}
//
//	flash_if_get_free_space(&space);
//	PRINT("\r\n\r\n%d files on flash fs", fileNum);
//	PRINT("\r\n%d/ %dB free\r\n", space, FLASH_HW_SECTOR_SIZE);
//	PRINT("\r\nFat size: %dB", FLASH_FILE_TABLE_HEADER_SIZE + ((last_used_file< 0) ? 0: last_used_file) *  FLASH_FILE_DESCRIPTOR_SIZE);
//	PRINT("\r\nFiles size: %dB\r\nFiles aligned size:%dB", size, sizeAligned);
//	PRINT("\r\n%d used descriptors",last_used_file + 1);
//
//}





