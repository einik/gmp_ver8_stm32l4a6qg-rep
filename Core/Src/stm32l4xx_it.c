/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32l4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//#include "cmsis_os.h"
//#include "stm32l4xx.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
extern COMP_HandleTypeDef hcomp2;
extern DMA_HandleTypeDef hdma_sdmmc1_rx;
extern DMA_HandleTypeDef hdma_sdmmc1_tx;
extern SD_HandleTypeDef hsd1;
//extern DMA_HandleTypeDef hdma_spi1_rx;
//extern DMA_HandleTypeDef hdma_spi1_tx;
extern SPI_HandleTypeDef hspi1;
//extern DMA_HandleTypeDef hdma_usart2_rx;
//extern DMA_HandleTypeDef hdma_usart2_tx;
extern UART_HandleTypeDef huart2;
extern TIM_HandleTypeDef htim1;

/* USER CODE BEGIN EV */
__weak void DMA2_Stream7_IRQHandler_callback() {} //HAL_DMA_IRQHandler(&hdma_usart1_rx);
__weak void DMA1_Stream7_IRQHandler_callback() {} //  HAL_DMA_IRQHandler(&hdma_usart2_tx);
__weak void DMA2_Stream6_IRQHandler_callback(){} //HAL_DMA_IRQHandler(&hdma_usart1_tx);
__weak void DMA1_Stream6_IRQHandler_callback(){}	//HAL_DMA_IRQHandler(&hdma_usart2_rx);

__weak void DMA1_Channel2_IRQHandler_callback(){}	//  HAL_DMA_IRQHandler(&hdma_spi_ecg_rx);
__weak void DMA1_Channel3_IRQHandler_callback(){} //HAL_DMA_IRQHandler(&hdma_spi_ecg_tx);

__weak void DMA1_Channel4_IRQHandler_callback(){} //HAL_DMA_IRQHandler(&hdma_spi_accel_rx);
__weak void DMA1_Channel5_IRQHandler_callback(){} //HAL_DMA_IRQHandler(&hdma_spi_accel_tx);

__weak void USART1_IRQHandler_callback(){}	//HAL_UART_IRQHandler(&huart1);
__weak void USART2_IRQHandler_callback(){}	//HAL_UART_IRQHandler(&huart2);

__weak void EXTI6_IRQHandler_callback(){} //External IRQ 6  (Button pressed Interrupt)
__weak void EXTI7_IRQHandler_callback(){} //External IRQ 7  (ECG1 Data Ready)
__weak void EXTI8_IRQHandler_callback(){} //External IRQ 8  (ECG2 Data Ready)
__weak void EXTI11_IRQHandler_callback(){} //External IRQ 11  (Accelerometer INT 1)
__weak void EXTI12_IRQHandler_callback(){} //External IRQ 12  (Accelerometer INT 2)
__weak void EXTI13_IRQHandler_callback(){} //External IRQ 13  (BLE status indication)
__weak void EXTI_PVD_IRQHandler_callback(){} //External IRQ PVD  (If PVD threshold is passed)

__weak void SPI1_complete__callback(uint8_t * rxData, uint16_t size){ } //SPI 1 read complete  (ADS finished)
__weak void SPI2_complete__callback(uint8_t * rxData, uint16_t size){ } //SPI 2 read complete  (Accelerometer finished)

__weak void TIM2_IRQHandler_callback() {}   //Pacemaker capture
__weak void TIM4_IRQHandler_callback() {}   //impedance

__weak void SysTick_Handler_callback() {}   //impedance
__weak void HAL_ADC_IRQHandler_Callback() {} 
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/******************************************************************************/
/* STM32L4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32l4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles DMA1 channel2 global interrupt.
  */
void DMA1_Channel2_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel2_IRQn 0 */

  /* USER CODE END DMA1_Channel2_IRQn 0 */
  //HAL_DMA_IRQHandler(&hdma_spi1_rx);
  DMA1_Channel2_IRQHandler_callback();
  /* USER CODE BEGIN DMA1_Channel2_IRQn 1 */

  /* USER CODE END DMA1_Channel2_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel3 global interrupt.
  */
void DMA1_Channel3_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel3_IRQn 0 */

  /* USER CODE END DMA1_Channel3_IRQn 0 */
  //HAL_DMA_IRQHandler(&hdma_spi1_tx);
  DMA1_Channel3_IRQHandler_callback();
  /* USER CODE BEGIN DMA1_Channel3_IRQn 1 */

  /* USER CODE END DMA1_Channel3_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel6 global interrupt.
  */
void DMA1_Channel6_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel6_IRQn 0 */

  /* USER CODE END DMA1_Channel6_IRQn 0 */
  //HAL_DMA_IRQHandler(&hdma_usart2_rx);
  DMA1_Stream6_IRQHandler_callback();
  /* USER CODE BEGIN DMA1_Channel6_IRQn 1 */

  /* USER CODE END DMA1_Channel6_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel7 global interrupt.
  */
void DMA1_Channel7_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel7_IRQn 0 */

  /* USER CODE END DMA1_Channel7_IRQn 0 */
  //HAL_DMA_IRQHandler(&hdma_usart2_tx);
  DMA1_Stream7_IRQHandler_callback();
  /* USER CODE BEGIN DMA1_Channel7_IRQn 1 */

  /* USER CODE END DMA1_Channel7_IRQn 1 */
}

/**
  * @brief This function handles TIM1 update interrupt and TIM16 global interrupt.
  */
void TIM1_UP_TIM16_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_TIM16_IRQn 0 */

  /* USER CODE END TIM1_UP_TIM16_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_UP_TIM16_IRQn 1 */

  /* USER CODE END TIM1_UP_TIM16_IRQn 1 */
}

/**
  * @brief This function handles SPI1 global interrupt.
  */
void SPI1_IRQHandler(void)
{
  /* USER CODE BEGIN SPI1_IRQn 0 */

  /* USER CODE END SPI1_IRQn 0 */
  HAL_SPI_IRQHandler(&hspi1);
  //SPI1_complete__callback(&(&hspi1->pRxBuffPtr), &hspi1->RxXferSize);//ads_spi_complete_irq_callback();
  /* USER CODE BEGIN SPI1_IRQn 1 */

  /* USER CODE END SPI1_IRQn 1 */
}
#ifdef 0
/**
  * @brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */

  /* USER CODE END USART2_IRQn 0 */
  HAL_UART_IRQHandler(&huart2);
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}
#endif
/**
  * @brief This function handles SDMMC1 global interrupt.
  */
void SDMMC1_IRQHandler(void)
{
  /* USER CODE BEGIN SDMMC1_IRQn 0 */

  /* USER CODE END SDMMC1_IRQn 0 */
  HAL_SD_IRQHandler(&hsd1);
  /* USER CODE BEGIN SDMMC1_IRQn 1 */

  /* USER CODE END SDMMC1_IRQn 1 */
}
#ifdef 0
/**
  * @brief This function handles DMA2 channel4 global interrupt.
  */
void DMA2_Channel4_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Channel4_IRQn 0 */

  /* USER CODE END DMA2_Channel4_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_sdmmc1_rx);
  /* USER CODE BEGIN DMA2_Channel4_IRQn 1 */

  /* USER CODE END DMA2_Channel4_IRQn 1 */
}

/**
  * @brief This function handles DMA2 channel5 global interrupt.
  */
void DMA2_Channel5_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Channel5_IRQn 0 */

  /* USER CODE END DMA2_Channel5_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_sdmmc1_tx);
  /* USER CODE BEGIN DMA2_Channel5_IRQn 1 */

  /* USER CODE END DMA2_Channel5_IRQn 1 */
}
#endif
/**
  * @brief This function handles COMP1 and COMP2 interrupts through EXTI lines 21 and 22.
  */
void COMP_IRQHandler(void)
{
  /* USER CODE BEGIN COMP_IRQn 0 */

  /* USER CODE END COMP_IRQn 0 */
  //HAL_COMP_IRQHandler(&hcomp2);
  HAL_COMP_IRQHandler_Callback();
  /* USER CODE BEGIN COMP_IRQn 1 */

  /* USER CODE END COMP_IRQn 1 */
}

/**
  * @brief This function handles USB OTG FS global interrupt.
  */
void OTG_FS_IRQHandler(void)
{
  /* USER CODE BEGIN OTG_FS_IRQn 0 */

  /* USER CODE END OTG_FS_IRQn 0 */
  HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
  /* USER CODE BEGIN OTG_FS_IRQn 1 */

  /* USER CODE END OTG_FS_IRQn 1 */
}

/* USER CODE BEGIN 1 */
void ADC1_2_IRQHandler(void)
{
   HAL_ADC_IRQHandler_Callback();
}

void ADC3_IRQHandler(void)
{
   HAL_ADC_IRQHandler_Callback();
}
// Impedance PWM timer interrupt
void TIM4_IRQHandler(void)
{
	TIM4_IRQHandler_callback();
}

// Pacemaker capture interrupt 
void TIM2_IRQHandler(void)
{
	TIM2_IRQHandler_callback();
}

void EXTI9_5_IRQHandler(void)
{
	if (__HAL_GPIO_EXTI_GET_IT(BUTTON_INT_PIN) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(BUTTON_INT_PIN);
		EXTI6_IRQHandler_callback();
		HAL_GPIO_EXTI_Callback(BUTTON_INT_PIN);
	}
	else if(__HAL_GPIO_EXTI_GET_IT(ECG1_DRDY_PIN) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(ECG1_DRDY_PIN);
		EXTI7_IRQHandler_callback();
		HAL_GPIO_EXTI_Callback(ECG1_DRDY_PIN);
	}
	else if(__HAL_GPIO_EXTI_GET_IT(ECG2_DRDY_PIN) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(ECG2_DRDY_PIN);
		EXTI8_IRQHandler_callback();
		HAL_GPIO_EXTI_Callback(ECG2_DRDY_PIN);
	}
}

void EXTI15_10_IRQHandler(void)
{

	if(__HAL_GPIO_EXTI_GET_IT(ACCEL_INT1_PIN) != RESET)
	{
		EXTI11_IRQHandler_callback();
		__HAL_GPIO_EXTI_CLEAR_IT(ACCEL_INT1_PIN);
		HAL_GPIO_EXTI_Callback(ACCEL_INT1_PIN);
	}	
	else if(__HAL_GPIO_EXTI_GET_IT(ACCEL_INT2_PIN) != RESET)
	{
		EXTI12_IRQHandler_callback();
		__HAL_GPIO_EXTI_CLEAR_IT(ACCEL_INT2_PIN);
		HAL_GPIO_EXTI_Callback(ACCEL_INT2_PIN);
	}	
	else if(__HAL_GPIO_EXTI_GET_IT(BLE_STATUS_PIN) != RESET)
	{
		EXTI13_IRQHandler_callback();
		__HAL_GPIO_EXTI_CLEAR_IT(BLE_STATUS_PIN);
		HAL_GPIO_EXTI_Callback(BLE_STATUS_PIN);
	}
}

#ifdef 0
  /**
* @brief This function handles System tick timer.
*/
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */

	osSystickHandler();
	HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */
	SysTick_Handler_callback();
  /* USER CODE END SysTick_IRQn 1 */
}
#endif

/**
* @brief This function handles USART1 global interrupt.
*/
//void USART1_IRQHandler(void)
//{
  /* USER CODE BEGIN USART1_IRQn 0 */

  /* USER CODE END USART1_IRQn 0 */
	//USART1_IRQHandler_callback();	//HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */

  /* USER CODE END USART1_IRQn 1 */
//}

void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */

  /* USER CODE END USART2_IRQn 0 */
	USART2_IRQHandler_callback();
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if (hspi->Instance == SPI1)
		SPI1_complete__callback(hspi->pRxBuffPtr, hspi->RxXferSize);//ads_spi_complete_irq_callback();
	//if (hspi->Instance == SPI2)
	//	SPI2_complete__callback(hspi->pRxBuffPtr, hspi->RxXferSize);//vAFE_SpiCmpltCallback();//TODO: Asher, change to BSP function
}



/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
