//SPDX
/*
 * RTC driver for the Micro Crystal RV3028
 *
 * Copyright (C) 2018 Micro Crystal SA
 *
 * Alexandre Belloni <alexandre.belloni@bootlin.com>
 *
 */

/*#include <linux/bcd.h>
#include <linux/bitops.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/log2.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/regmap.h>
#include <linux/rtc.h>*/
#include <stdint.h>
#include "rtc-rv3028.h"

#include "serial_protocol.h"


//#define RV3028_I2C			hi2c2
#define RV3028_SLAVE_ADDRESS      0x52<<1 //The slave RTC address
#define RV3028_TIMEOUT              200  // MS
#define RV3028_ATTEMPTS             1

#define RV3028_SEC			0x00
#define RV3028_MIN			0x01
#define RV3028_HOUR			0x02
#define RV3028_WDAY			0x03
#define RV3028_DAY			0x04
#define RV3028_MONTH			0x05
#define RV3028_YEAR			0x06
#define RV3028_ALARM_MIN		0x07
#define RV3028_ALARM_HOUR		0x08
#define RV3028_ALARM_DAY		0x09
#define RV3028_STATUS			0x0E
#define RV3028_CTRL1			0x0F
#define RV3028_CTRL2			0x10
#define RV3028_EVT_CTRL			0x13
#define RV3028_TS_COUNT			0x14
#define RV3028_TS_SEC			0x15
#define RV3028_RAM1			0x1F
#define RV3028_EEPROM_ADDR		0x25
#define RV3028_EEPROM_DATA		0x26
#define RV3028_EEPROM_CMD		0x27
#define RV3028_CLKOUT			0x35
#define RV3028_OFFSET			0x36
#define RV3028_BACKUP			0x37

#define RV3028_STATUS_PORF		BIT(0)
#define RV3028_STATUS_EVF		BIT(1)
#define RV3028_STATUS_AF		BIT(2)
#define RV3028_STATUS_TF		BIT(3)
#define RV3028_STATUS_UF		BIT(4)
#define RV3028_STATUS_BSF		BIT(5)
#define RV3028_STATUS_CLKF		BIT(6)
#define RV3028_STATUS_EEBUSY		BIT(7)

#define RV3028_CTRL1_EERD		BIT(3)
#define RV3028_CTRL1_WADA		BIT(5)

#define RV3028_CTRL2_RESET		BIT(0)
#define RV3028_CTRL2_12_24		BIT(1)
#define RV3028_CTRL2_EIE		BIT(2)
#define RV3028_CTRL2_AIE		BIT(3)
#define RV3028_CTRL2_TIE		BIT(4)
#define RV3028_CTRL2_UIE		BIT(5)
#define RV3028_CTRL2_TSE		BIT(7)

#define RV3028_EVT_CTRL_TSR		BIT(2)

#define RV3028_EEPROM_CMD_WRITE		0x21
#define RV3028_EEPROM_CMD_READ		0x22

#define RV3028_EEBUSY_POLL		10000
#define RV3028_EEBUSY_TIMEOUT		100000

#define RV3028_BACKUP_TCE		BIT(5)
#define RV3028_BACKUP_TCR_MASK		GENMASK(1,0)

#define OFFSET_STEP_PPT			953674

#define	BACKUP_REGISTER_VALUE	188 //10111100 - bit 5 = enabled backup battery charging
									//		   - bit 2-3 = LMS mode

enum rv3028_type {
	rv_3028,
};

static bool RV3028_Initialized = false;
static uint8_t backupRegister = BACKUP_REGISTER_VALUE;

/*******************************************************************************
** @Function   : RV3028_Write_Time
** @Description: This function writes time to RV3028 module
** @Inputs     :
** @Outputs    :
** @Return     :
*******************************************************************************/
RV3028_Errors RV3028_Write_Time(I2C_HandleTypeDef* hi2c2,RV30287_rtc_time *tm)
{
	HAL_StatusTypeDef status;
	uint8_t date[7];

	date[RV3028_SEC]   = tm->tm_sec;//bin2bcd(tm->tm_sec);
	date[RV3028_MIN]   = tm->tm_min;//bin2bcd(tm->tm_min);
	date[RV3028_HOUR]  = tm->tm_hour;//bin2bcd(tm->tm_hour);
	date[RV3028_WDAY]  = 1 << (tm->tm_wday);
	date[RV3028_DAY]   = tm->tm_mday;//bin2bcd(tm->tm_mday);
	date[RV3028_MONTH] = tm->tm_mon;//bin2bcd(tm->tm_mon + 1);
	date[RV3028_YEAR]  = tm->tm_year;//bin2bcd(tm->tm_year - 100);

	status = HAL_I2C_Mem_Write( hi2c2, RV3028_SLAVE_ADDRESS, RV3028_SEC, I2C_MEMADD_SIZE_8BIT, date, sizeof(date), RV3028_TIMEOUT );
	if( status == HAL_OK )
	{
		return RV3028_OK;
	}
	else if( status == HAL_TIMEOUT )
	{
		LOG("");
		return RV3028_TIMEOUT_ERR;
	}
	else
	{
		LOG("");
		return RV3028_ERR;
	}

}

/*******************************************************************************
** @Function   : RV3028_Read_Time
** @Description: This function is writing register
** @Inputs     :
** @Outputs    :
** @Return     :
*******************************************************************************/
RV3028_Errors RV3028_Read_Time(I2C_HandleTypeDef* hi2c2, RV30287_rtc_time *tm)
{
   HAL_StatusTypeDef status;
   uint8_t date[7];
   uint8_t attempts/*, statusMap*/;

   if(!RV3028_Initialized)
   {
	   HAL_I2C_Mem_Write( hi2c2, RV3028_SLAVE_ADDRESS, RV3028_BACKUP, I2C_MEMADD_SIZE_8BIT, &backupRegister, sizeof(backupRegister), RV3028_TIMEOUT );
   }
   
   /*status = HAL_I2C_Mem_Read( hi2c2, RV3028_SLAVE_ADDRESS, RV3028_STATUS, I2C_MEMADD_SIZE_8BIT, &statusMap, 1, RV3028_TIMEOUT );
	if( status == HAL_OK )
   {
      return RV3028_OK;
   }
   else if( status == HAL_TIMEOUT )
   {
       LOG("");
      return RV3028_TIMEOUT_ERR;
   }
   else
   {
       LOG("");
      return RV3028_ERR;
   }

	if (statusMap & RV3028_STATUS_PORF) {
		LOG("Voltage low, data is invalid.\n");
		return RV3028_ERR;
	}*/

   for( attempts = 0; attempts < RV3028_ATTEMPTS; attempts++)
   {
      status = HAL_I2C_Mem_Read( hi2c2, RV3028_SLAVE_ADDRESS, RV3028_SEC, I2C_MEMADD_SIZE_8BIT, date, sizeof(date), RV3028_TIMEOUT );
      // if success break the loop
      if( status == HAL_OK ) break;
   }
   if( status == HAL_OK )
   {
	tm->tm_sec  = date[RV3028_SEC] & 0x7f;
	tm->tm_min  = date[RV3028_MIN] & 0x7f;
	tm->tm_hour = date[RV3028_HOUR] & 0x3f;
	tm->tm_wday = date[RV3028_WDAY] & 0x7f;
	tm->tm_mday = date[RV3028_DAY] & 0x3f;
	tm->tm_mon  = date[RV3028_MONTH] & 0x1f;
	tm->tm_year = date[RV3028_YEAR];
	//LOG("tm->tm_sec %d,tm->tm_min %d,tm->tm_hour %d,tm->tm_wday %d,tm->tm_wday %d,tm->tm_mday %d,tm->tm_mon %d,tm->tm_year %d\r\n",tm->tm_sec,tm->tm_min,tm->tm_hour,tm->tm_wday,tm->tm_wday,tm->tm_mday,tm->tm_mon,tm->tm_year);
	return RV3028_OK;
   }
   else if( status == HAL_TIMEOUT )
   {
       LOG("status: %d\r\n", status);
      return RV3028_TIMEOUT_ERR;
   }
   else
   {
       LOG("status: %d\r\n", status);
      return RV3028_ERR;
   }
}
/*
static int rv3028_get_time(struct device *dev, struct rtc_time *tm)
{
	struct rv3028_data *rv3028 = dev_get_drvdata(dev);
	u8 date[7];
	int ret, status;

	ret = regmap_read(rv3028->regmap, RV3028_STATUS, &status);
	if (ret < 0)
		return ret;

	if (status & RV3028_STATUS_PORF) {
		dev_warn(dev, "Voltage low, data is invalid.\n");
		return -EINVAL;
	}

	ret = regmap_bulk_read(rv3028->regmap, RV3028_SEC, date, sizeof(date));
	if (ret)
		return ret;

	tm->tm_sec  = bcd2bin(date[RV3028_SEC] & 0x7f);
	tm->tm_min  = bcd2bin(date[RV3028_MIN] & 0x7f);
	tm->tm_hour = bcd2bin(date[RV3028_HOUR] & 0x3f);
	tm->tm_wday = ilog2(date[RV3028_WDAY] & 0x7f);
	tm->tm_mday = bcd2bin(date[RV3028_DAY] & 0x3f);
	tm->tm_mon  = bcd2bin(date[RV3028_MONTH] & 0x1f) - 1;
	tm->tm_year = bcd2bin(date[RV3028_YEAR]) + 100;

	return 0;
}


static int rv3028_set_time(struct device *dev, struct rtc_time *tm)
{
	struct rv3028_data *rv3028 = dev_get_drvdata(dev);
	u8 date[7];
	int ret;

	date[RV3028_SEC]   = bin2bcd(tm->tm_sec);
	date[RV3028_MIN]   = bin2bcd(tm->tm_min);
	date[RV3028_HOUR]  = bin2bcd(tm->tm_hour);
	date[RV3028_WDAY]  = 1 << (tm->tm_wday);
	date[RV3028_DAY]   = bin2bcd(tm->tm_mday);
	date[RV3028_MONTH] = bin2bcd(tm->tm_mon + 1);
	date[RV3028_YEAR]  = bin2bcd(tm->tm_year - 100);*/

	/*
	 * Writing to the Seconds register has the same effect as setting RESET
	 * bit to 1
	 */
	/*ret = regmap_bulk_write(rv3028->regmap, RV3028_SEC, date,
				sizeof(date));
	if (ret)
		return ret;

	ret = regmap_update_bits(rv3028->regmap, RV3028_STATUS,
				 RV3028_STATUS_PORF, 0);

	return ret;
}*/