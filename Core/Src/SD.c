#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
#include "serial_protocol.h"
#include "common.h"
#include "SD.h"
#include "fatfs.h"
#include "ff_gen_drv.h"
/****************************************************************/
/*						Local Definitions						*/
#define MAX_NUM_OF_RETRIES							5
#define SD_QUEUE_SIZE								4 //5//1
#define SD_WRITE_RETRIES							7//5
#define MSG_SIZE_MAX_LENGTH							1000
#define FILE_NAME_MAX_LENGTH						30
#define SD_MUTEX_TIME_OUT							osWaitForever  //1000
#define SD_WRITE_INDEX_INVALID						(uint32_t)(-1)
#define SD_QUEUE_MUTEX_TIMEOUT						1000
#define SD_WRITE_BUFFER_LENGTH						17000//5000
#define SD_WRITE_BUFFER_MIN							800

typedef struct{
	uint8_t		file_name[SD_FILE_NAME_MAX_LENGTH];
	uint16_t	msg_length;
	uint8_t		msg[MSG_SIZE_MAX_LENGTH];
}SD_QUEUE_MSG_STRUCT;
/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
static 	osMutexId 							sdMutex;
static 	osMutexId 							sd_queue_mutex;
osMessageQId 								sdQueueHandle;
osThreadId 									processSdTaskHandle;
#pragma location = ".RAM2"
static FATFS 								sd_fatfs;
static FIL 									my_file_read, my_file_write, my_file_replace, my_file_open, my_file_exist, my_file_size;     /* File object */
static char 								sd_path[4];
static uint8_t								current_dir[FILE_NAME_MAX_LENGTH + 1] = {0};
//#pragma location = ".RAM2"
static uint8_t 								sd_write_buff[SD_WRITE_BUFFER_LENGTH] = {0};
static uint32_t 							sd_write_buff_length = 0;
/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/
static SD_RESULT_E prvSdMountSd(SD_STATUS_E mount);
static void sdTask(void const * argument);
/****************************************************************/

/****************************************************************/
/*						External Parameters						*/
extern Diskio_drvTypeDef 					SD_Driver;  /* File system object for SD card logical drive */
/****************************************************************/

/****************************************************************/
/*						Weak Functions							*/
__weak SD_RESULT_E sdSync(SD_MUTEX_COMMAND_E command){return SD_RESULT_OK;}
__weak SD_RESULT_E sdGetTime(uint32_t* time){return SD_RESULT_OK;}
__weak void sdWriteEndedEvent(uint8_t * file_name, uint32_t write_index){}
__weak void sdFailure(void){}
/****************************************************************/

/***********************************************************************
 * Function name: sdInit
 * Description: Init SD card
 * Parameters : 
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR_INIT_FAILED - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdInit(void)
{
	
	osMutexDef(sdMutex);
	sdMutex = osMutexCreate(osMutex(sdMutex));
	if (!sdMutex)
		return SD_RESULT_ERROR_MUTEX_INIT_FAILED;
	
	osMutexDef(sd_queue_mutex);
	sd_queue_mutex = osMutexCreate(osMutex(sd_queue_mutex));
	if (!sd_queue_mutex)
		return SD_RESULT_ERROR_MUTEX_INIT_FAILED;

	//Init Queue
	osMessageQDef(sdQueue, SD_QUEUE_SIZE, SD_QUEUE_MSG_STRUCT*);
	sdQueueHandle = osMessageCreate(osMessageQ(sdQueue), NULL);
	if (!sdQueueHandle)
		return SD_RESULT_ERROR_QUEUE_CREATE_FAILED;		
	
	//Init task
	osThreadDef(sdTask, sdTask, osPriorityNormal, 0, SD_STACK_SIZE);
	processSdTaskHandle = osThreadCreate(osThread(sdTask), NULL);
	if (!processSdTaskHandle)
		return SD_RESULT_ERROR_TASK_CREATE_FAILED;
	
	if (FATFS_LinkDriver(&SD_Driver, sd_path) != 0)
		return SD_RESULT_ERROR_MOUNT_FAILED;
	
	//Check if SD card inserted
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
		return SD_RESULT_ERROR_MOUNT_FAILED;
	
	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
		return SD_RESULT_ERROR_MOUNT_FAILED;
	
	
	return SD_RESULT_OK;
}

SD_RESULT_E sdGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (processSdTaskHandle == NULL) || (ret_val == NULL) )
		return SD_RESULT_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark( processSdTaskHandle );
	return SD_RESULT_OK;
}

//static uint8_t		num_of_packets_in_buffer = 0;
//static uint16_t		lengths_array[20] = {0};
//static uint32_t		lengths_total = 0;

static void sdTask(void const * argument)
{
	osEvent 						ret;
	static SD_QUEUE_MSG_STRUCT *	ptr_sd_msg;
	SD_RESULT_E						sd_res;
	uint8_t							write_index;
	uint16_t						errors = 0, totals = 0;
	uint32_t 						sd_file_write_index;
//	uint8_t							read_test[10];
//	uint16_t						read_test_length;
//	uint8_t							i = 0;
	
	while(1)
	{
		write_index = 0;
		sd_res = SD_RESULT_ERROR;
		ret = osMessageGet(sdQueueHandle, osWaitForever);
		if (ret.status == osEventMessage)
		{
			ptr_sd_msg = (SD_QUEUE_MSG_STRUCT*)ret.value.v;
			
			memcpy(&sd_write_buff[sd_write_buff_length], ptr_sd_msg->msg, ptr_sd_msg->msg_length);
			sd_write_buff_length += ptr_sd_msg->msg_length;
//			//TEST
//			lengths_array[num_of_packets_in_buffer] = ptr_sd_msg->msg_length;
//			num_of_packets_in_buffer++;
//			//TEST
			
			if ((sizeof(sd_write_buff) - sd_write_buff_length) > SD_WRITE_BUFFER_MIN)
			{
				memset(ptr_sd_msg, 0, sizeof(SD_QUEUE_MSG_STRUCT));
				continue;//dont write to SD card - wait for buffer to fill. 
                                // will take  20 seconds to fill buffer.
			}
			
                        // burn to sdcard:
			while ((write_index++ < SD_WRITE_RETRIES) && (sd_res != SD_RESULT_OK))
			{
//				for (i = 0;i < num_of_packets_in_buffer;i++)
//					lengths_total += lengths_array[i];
//
//				if (lengths_total != sd_write_buff_length)
//				{
//					for (i = 0;i < num_of_packets_in_buffer;i++)
//						LOG("%d ", lengths_array[i]);
//					LOG("\r\n");
//					LOG("%d\r\n", lengths_total);
//					
//					LOG("lengths_total != sd_write_buff_length\r\n");
//					LOG("sd_write_buff_length: %d\r\n", sd_write_buff_length);
//				}
//				SystemClock_Config_5Mhz();
//				osDelay(10);
//				if (bspUart1Valid() == BSP_RESULT_OK)
//				{
//					bspUart1Uninit();
//					bspUart1Init();					
//				}
				sd_res = sdWriteDataToFile(ptr_sd_msg->file_name, sd_write_buff, sd_write_buff_length, &sd_file_write_index);
				if ((sd_res == SD_RESULT_OK) && (sd_file_write_index != SD_WRITE_INDEX_INVALID ))
				{
					sdWriteEndedEvent(ptr_sd_msg->file_name, sd_file_write_index);
					
					memset(ptr_sd_msg, 0, sizeof(SD_QUEUE_MSG_STRUCT));
					memset(sd_write_buff, 0, sizeof(sd_write_buff));
					sd_write_buff_length = 0;
					
//					SystemClock_Config_2Mhz();
//					if (bspUart1Valid() == BSP_RESULT_OK)
//					{
//						bspUart1Uninit();
//						bspUart1Init();
//					}
//					//TEST
//					memset(lengths_array, 0,  sizeof(lengths_array) * sizeof(uint16_t));
//					num_of_packets_in_buffer = 0;
//					lengths_total = 0;
//					//TEST
//					sdReadDataFromFile(ptr_sd_msg->file_name, sizeof(read_test), sd_file_write_index, read_test, &read_test_length);
//					if (memcmp(read_test, ptr_sd_msg->msg, read_test_length) != 0)
//					{
//						PRINT("\r\nsdTast Write/ Read mismatch");
//					}
				}
				else
					LOG("Failed to write file, error: %d\r\n", sd_res);
				osDelay(2);
			}

			//memset(ptr_sd_msg, 0, sizeof(SD_QUEUE_MSG_STRUCT));
			
			++totals;
			if ((sd_res != SD_RESULT_OK) || (write_index > 2))
				LOG("SD Error write: %d/%d\tr:%d\r\n", ++errors, totals, write_index - 2);
			
			if (write_index > SD_WRITE_RETRIES)
				sdFailure();//Set the device in Error State
		}
		else
			osDelay(1);
	}
	
}

//Temporary function for testing
uint8_t bufferContainsHeader(uint8_t* buff, uint16_t length)
{
	uint16_t i = 0;
	
	for (i = 0;i < length;i++)
	{
		if ((buff[i] == 0xAA) && (buff[i + 1] == 0xAA))
			return TRUE;		
	}
	
	return FALSE;	
}
/***********************************************************************
 * Function name: sdSendMsgToQueue
 * Description: Send message to SD card queue
 * Parameters : file_name - file name to write 
 * 				buff - pointer to buffer, length - data length
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdSendMsgToQueue(uint8_t* file_name, uint8_t* buff, uint16_t length)
{
	static SD_QUEUE_MSG_STRUCT					sd_msg[SD_QUEUE_SIZE];
	static uint8_t 								sd_msg_index = 0;

	SD_RESULT_E res = SD_RESULT_OK;

	if (osMutexWait(sd_queue_mutex, SD_QUEUE_MUTEX_TIMEOUT) != osOK)
		return SD_RESULT_ERROR; 

	if (bufferContainsHeader(buff, length) != TRUE)
	{
		LOG("No Header in Packet!!!!\r\n");
		return SD_RESULT_ERROR_WRITE_FAILED_LENGTH_MISMATCH;
	}
		
	strcpy((char*)sd_msg[sd_msg_index].file_name, (char*)file_name);
	memcpy(sd_msg[sd_msg_index].msg, buff, length);
	sd_msg[sd_msg_index].msg_length = length;
	
	if (osMessagePut (sdQueueHandle, (uint32_t)&sd_msg[sd_msg_index], 0) != osOK)
	{
		res = SD_RESULT_ERROR;
	}

	sd_msg_index = (sd_msg_index + 1) % SD_QUEUE_SIZE;

	osMutexRelease(sd_queue_mutex);
	
	return res;
}
//extern SD_HandleTypeDef  							hsd1;

/***********************************************************************
 * Function name: prvSdMountSd
 * Description: Mount/Unmount SD card
 * Parameters : mount - mount/unmount param
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
static SD_RESULT_E prvSdMountSd(SD_STATUS_E mount)
{
	FRESULT 	ff_res = FR_DISK_ERR;
	uint8_t		fs_res = 0;
	uint8_t 	i = 0;
	
	
	
	if (mount == SD_STATUS_MOUNT)
	{
		sdSync(SD_MUTEX_COMMAND_WAIT);
	  /* Enable SDMMC Clock */
//	  __HAL_SD_ENABLE(&hsd1);
		//bspSdModeSet(BSP_MODE_SET_ON);
	  /* Required power up waiting time before starting the SD initialization sequence */
	  osDelay(2U);		
		while (ff_res != FR_OK)
		{
			ff_res = f_mount(&sd_fatfs, (TCHAR const*)sd_path, 1);
			if (ff_res != FR_OK)
			{
				fs_res = FATFS_UnLinkDriver(sd_path);
				fs_res |= FATFS_LinkDriver(&SD_Driver, sd_path);
				
				if (fs_res != 0)
				{
					sdSync(SD_MUTEX_COMMAND_RELEASE);
					//bspSdModeSet(BSP_MODE_SET_OFF);
					return SD_RESULT_ERROR_MOUNT_FAILED;
				}
			}
			osDelay(1);
			if (i++ > MAX_NUM_OF_RETRIES)
			{
				sdSync(SD_MUTEX_COMMAND_RELEASE);
				//bspSdModeSet(BSP_MODE_SET_OFF);
				break;
			}
		}
	}
	else
	{
		sdSync(SD_MUTEX_COMMAND_RELEASE);
		ff_res = f_mount(0, (TCHAR const*)sd_path, 1);
		/* Disable SDMMC Clock */
//		__HAL_SD_DISABLE(&hsd1);
		//bspSdModeSet(BSP_MODE_SET_OFF);
	}
	
	if (ff_res != FR_OK)
		LOG_ERR("Failed to mount, error: %d\r\n", ff_res);
	
	return ((ff_res == FR_OK) ? SD_RESULT_OK : SD_RESULT_ERROR_MOUNT_FAILED);
}

/***********************************************************************
 * Function name: sdOpenFile
 * Description: Create file
 * Parameters : file_name - File name
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdOpenFile(uint8_t* file_name)
{
	FRESULT 	ff_res = FR_DISK_ERR;
	char 		wtext[40] = {0};
	uint32_t	byteswritten = 0;
//	uint8_t 	i = 0;
	
	
//	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
//		return SD_RESULT_ERROR_MOUNT_FAILED;
	
	ff_res = f_open(&my_file_open, (const TCHAR*) file_name, FA_CREATE_NEW | FA_WRITE);
	
	if (ff_res != FR_OK)
	{
		f_mount(0, "", 1);
		return SD_RESULT_ERROR_FILE_CREATE;
	}
	f_close(&my_file_open);

	
	ff_res = f_open(&my_file_open, (const TCHAR*) file_name, FA_WRITE);

	if (ff_res != FR_OK)
	{
		f_mount(0, "", 1);
		return SD_RESULT_ERROR_WRITE;
	}
	
	ff_res = f_lseek(&my_file_open, f_size(&my_file_open));
	if (ff_res != FR_OK)
	{
		f_mount(0, "", 1);
		return SD_RESULT_ERROR_WRITE;
	}
	
	memcpy((uint8_t*)wtext, (uint8_t*)file_name, sizeof(wtext));
	
	ff_res = f_write(&my_file_open, wtext, sizeof(wtext), (void *) &byteswritten);
	if (ff_res != FR_OK)
	{
		f_mount(0, "", 1);
		return SD_RESULT_ERROR_WRITE;
	}
	
	f_close(&my_file_open);
	
	
//	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
//		return SD_RESULT_ERROR_MOUNT_FAILED;
	
	return SD_RESULT_OK;
}

/***********************************************************************
 * Function name: sdDir
 * Description: Prints all files in root directory
 * Parameters : 
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_FAILED_TO_OPEN_DIR
 *
 ***********************************************************************/
SD_RESULT_E sdDir(void)
{
	DIR 			dir;
	FRESULT 		res;
	FILINFO 		fno;
	char* 			fn;
	SD_TIME_E		time;
	
#if _USE_LFN
	static char 	lfn[_MAX_LFN + 1];   /* Buffer to store the LFN */
#endif //_USE_LFN

	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;
	
#if _USE_LFN	
	fno.lfname = lfn;
	fno.lfsize = sizeof(lfn);
#endif //_USE_LFN
	
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	
	if (f_opendir(&dir, (char*)current_dir) != FR_OK)
	{
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_FAILED_TO_OPEN_DIR;
	}
	
	while ((res = f_readdir(&dir, &fno)) == FR_OK && fno.fname[0] != 0) 
	{
		/* Ignore files */
		if (fno.fname[0] == '.')
			continue;

#if _USE_LFN
		fn = *fno.lfname ? fno.lfname : fno.fname;
#else
		fn = fno.fname;
#endif	//_USE_LFN	
		
		time.bitmap_word.time = fno.ftime;
		time.bitmap_word.date = fno.fdate;
		
//		if (fno.fattrib & AM_DIR)
//			LOG("%030s <DIR>\r\n", fn, fno.fsize);
//		else
//			LOG("%030s %d \t %02d:%02d:%02d %02d:%02d:%02d\r\n", fn, fno.fsize, 
//					time.bitmap.hour, time.bitmap.minute, time.bitmap.second, time.bitmap.day, time.bitmap.month, (time.bitmap.year - 20));

		if (fno.fattrib & AM_DIR)
			LOG("%02d:%02d:%02d %02d/%02d/%02d \t %030s <DIR>\r\n", time.bitmap.hour, time.bitmap.minute, (time.bitmap.second * 2), time.bitmap.day, time.bitmap.month, (time.bitmap.year - 20), fn);
		else
			LOG("%02d:%02d:%02d %02d/%02d/%02d \t %030s %d\r\n", time.bitmap.hour, time.bitmap.minute, (time.bitmap.second * 2), time.bitmap.day, time.bitmap.month, (time.bitmap.year - 20), fn, fno.fsize);
		
		
		
	}	

	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);
	
	return SD_RESULT_OK;
}

/***********************************************************************
 * Function name: sdMakeDir
 * Description: Create directory
 * Parameters : 
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_CREATE_DIRECTORY_FAILED - Directory creation failed
 *		SD_RESULT_ERROR_MOUNT_FAILED - mount or unmount failed
 ***********************************************************************/
SD_RESULT_E sdMakeDir(IN uint8_t* dir_name)
{
	FRESULT 	ff_res;

	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;
	
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	ff_res = f_mkdir((char*)dir_name);

	if (ff_res != FR_OK)
	{//Creat Directory
		LOG_ERR("Failed create directory, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_CREATE_DIRECTORY_FAILED;
	}	

	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);
	
	return SD_RESULT_OK;
}

/***********************************************************************
 * Function name: sdWriteDataToFile
 * Description: Write data to file
 * Parameters : file_name - File name to write in
 *				data - pointer to data, length - data length
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_OPEN_FILE - error openning file
 *		SD_RESULT_ERROR_FILE_CREATE - error creating file
 *		SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE - error placing pointer at end of file
 *		SD_RESULT_ERROR_WRITE - error writing to file
 *		SD_RESULT_ERROR_MOUNT_FAILED - mount SD failed
 ***********************************************************************/
SD_RESULT_E sdWriteDataToFile(IN uint8_t* file_name, 
							  IN uint8_t* data, 
							  IN uint32_t length,
							  OUT uint32_t * write_index)
{
	FRESULT 		ff_res;
	uint32_t		bytes_written = 0;
	uint32_t 		tmp_write_index = 0;

	if ( (file_name == NULL) || (data == NULL) || (length == 0))
		return SD_RESULT_WRONG_PARAM;

	if (write_index != NULL)
		*write_index = SD_WRITE_INDEX_INVALID;

	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK) 	
		return SD_RESULT_ERROR;
	uint32_t time = osKernelSysTick();
	uint32_t time1 = osKernelSysTick();
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
//	PRINT("0: %d\t%d\r\n", length ,osKernelSysTick() - time );	
	time = osKernelSysTick();
	ff_res = f_open(&my_file_write, (const TCHAR*) file_name, FA_CREATE_NEW | FA_WRITE);
//	LOG("1: %d\t%d\t%d\r\n", length ,osKernelSysTick() - time , ff_res);	
	
	if ((ff_res != FR_OK) && (ff_res != FR_EXIST))
	{//Check if file exist, if not then create
		LOG_ERR("Failed to create file, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_FILE_CREATE;
	}
	f_close(&my_file_write);
	time = osKernelSysTick();
	ff_res = f_open(&my_file_write, (const TCHAR*) file_name, FA_WRITE);
//	LOG("2: %d\t%d\r\n", length ,osKernelSysTick() - time );	
	if (ff_res != FR_OK)
	{//Open file for write
//		LOG_ERR("Failed to open file, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_OPEN_FILE;
	}	
	time = osKernelSysTick();
	tmp_write_index = f_size(&my_file_write);
	ff_res = f_lseek(&my_file_write, tmp_write_index);
//	LOG("3: %d\t%d\r\n", length ,osKernelSysTick() - time );	
	if (ff_res != FR_OK)
	{//Move write pointer to end of file
		LOG_ERR("Failed to move pointer to end of data in sdWriteDataToFile, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE;
	}	
	time = osKernelSysTick();
	ff_res = f_write(&my_file_write, data, length, (void *) &bytes_written);
//	LOG("4: %d\t%d\r\n", length ,osKernelSysTick() - time );
//	if ((osKernelSysTick() - time) > 100)
	if (ff_res != FR_OK)
	{//Write data to end of file
		LOG_ERR("Failed to write data in file, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_WRITE;
	}
	if (bytes_written != length)
	{
		LOG_ERR("SD bytes_written %d != length \r\n", bytes_written, length);
		// TODO: ret val = error
	}
//	time = osKernelSysTick();
	f_close(&my_file_write);
//	LOG("5: %d\t%d\r\n", length ,osKernelSysTick() - time );
	
	time = osKernelSysTick();
	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);

	LOG_D(DEBUG_MODULE_SD, DEBUG_LEVEL_1, "%s\tw:%d\t%d\r\n", file_name, length ,osKernelSysTick() - time1 );
	if ((osKernelSysTick() - time1) > 150)
		time++;
	if (write_index != NULL)
		*write_index = tmp_write_index;
	return SD_RESULT_OK;
}


/***********************************************************************
 * Function name: sdReplaceDataInFile
 * Description: Replace data on file
 * Parameters : file_name - File name to write in
 *				data - pointer to data
 *				length - data length
 *				write_index - offset of file for data replacement
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_OPEN_FILE - error openning file
 *		SD_RESULT_ERROR_FILE_CREATE - error creating file
 *		SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE - error placing pointer at end of file
 *		SD_RESULT_ERROR_WRITE - error writing to file
 *		SD_RESULT_ERROR_MOUNT_FAILED - mount SD failed
 *		SD_RESULT_ERROR_REPLACE_INDEX_OUT_OF_BOUNDS - requested index exceeds file size
 ***********************************************************************/
SD_RESULT_E sdReplaceDataInFile(IN uint8_t* file_name,
							  IN uint8_t* data,
							  IN uint16_t length,
							  IN uint32_t write_index)
{
	FRESULT 		ff_res;
	uint16_t		bytes_written = 0;
	uint32_t 		file_orig_size = 0;
	SD_RESULT_E		sd_res, ret_val = SD_RESULT_OK;

	if ( (file_name == NULL) || (data == NULL) || (length == 0))
		return SD_RESULT_WRONG_PARAM;

	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;

	uint32_t time = osKernelSysTick();

	sd_res = prvSdMountSd(SD_STATUS_MOUNT);
	if (sd_res != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;
	}

	do
	{
		ff_res = f_open(&my_file_replace, (const TCHAR*) file_name, FA_WRITE);
		if (ff_res != FR_OK)
		{//Open file for write
//			LOG_ERR("Failed to open file, error: %d\r\n", ff_res);
			ret_val = SD_RESULT_ERROR_OPEN_FILE;
			break;
		}

		file_orig_size = f_size(&my_file_replace);

		if (write_index >= file_orig_size)
		{
			LOG_ERR("write_index (%d) >= file_orig_size (%d)\r\n", write_index, file_orig_size);
			ret_val = SD_RESULT_ERROR_REPLACE_INDEX_OUT_OF_BOUNDS;
			break;
		}

		ff_res = f_lseek(&my_file_replace, write_index);
		if (ff_res != FR_OK)
		{//Move write pointer to end of file
			LOG_ERR("Failed to move pointer to end of data in sdWriteDataToFile, error: %d\r\n", ff_res);
			ret_val =  SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE;
			break;
		}

		ff_res = f_write(&my_file_replace, data, length, (void *) &bytes_written);
		if (ff_res != FR_OK)
		{//Write data to end of file
			LOG_ERR("Failed to write data in file, error: %d\r\n", ff_res);
			ret_val = SD_RESULT_ERROR_WRITE;
			break;
		}
		if (bytes_written != length)
		{
			LOG_ERR("SD bytes_written %d != length \r\n", bytes_written, length);
			ret_val =  SD_RESULT_ERROR_WRITE_FAILED_LENGTH_MISMATCH;
		}
		f_close(&my_file_replace);

	} while(0);


	sd_res = prvSdMountSd(SD_STATUS_UNMOUNT);

	osMutexRelease(sdMutex);

	LOG_D(DEBUG_MODULE_SD, DEBUG_LEVEL_1, "w:%d\t%d\r\n", length ,osKernelSysTick() - time );


	if (ret_val != SD_RESULT_OK)
		return ret_val;
	return sd_res;
}

/***********************************************************************
 * Function name: sdReadDataFromFile
 * Description: Read data from file
 * Parameters : file_name - File name to read from, data_index - data index to read from
 *				length - data length, data - pointer to data, 
 *				data_length - length of read data
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 * 		SD_RESULT_ERROR_READ - error reading
 * 		SD_RESULT_ERROR_MOUNT_FAILED - SD mount failed
 * 		SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE - error moving pointer to data
 *
 ***********************************************************************/
SD_RESULT_E sdReadDataFromFile(IN uint8_t* file_name, 
							   IN uint16_t length,
							   IN uint32_t data_index,
							   OUT uint8_t* data,
							   OUT uint16_t* data_length)
{
	FRESULT 		ff_res;
	
	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;
	
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	uint32_t time = osKernelSysTick();

	ff_res = f_open(&my_file_read, (const TCHAR*) file_name, FA_READ);
	
	if (ff_res != FR_OK)
	{//Open file for read
//		LOG_ERR("Failed to open file, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_OPEN_FILE;
	}	

	ff_res = f_lseek(&my_file_read, data_index);
					 
	if (ff_res != FR_OK)
	{//Move pointer of data to read
		LOG_ERR("Failed to move pointer, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_LOCATING_DATA_POINTER_IN_FILE;
	}		
	
	
	ff_res = f_read(&my_file_read, data, length, (void *) data_length);

	if (ff_res != FR_OK)
	{//Read data from end of file
		LOG_ERR("Failed to read data from file, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_READ;
	}	
	
	
	f_close(&my_file_read);
	
	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	LOG_D(DEBUG_MODULE_SD, DEBUG_LEVEL_1, "r:%d\t%d\r\n", length ,osKernelSysTick() - time );

	osMutexRelease(sdMutex);	
	
	return SD_RESULT_OK;
}
/***********************************************************************
 * Function name: sdDeleteFile
 * Description: Delete specific file in SD card
 * Parameters : file_name - File name to delete
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_MOUNT_FAILED - SD mount failed
 * 		SD_RESULT_ERROR_DELETE_FAILED - file delete failed
 ***********************************************************************/
SD_RESULT_E sdDeleteFile(IN uint8_t* file_name)
{
	FRESULT 		ff_res;
	
	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;
	
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	ff_res = f_unlink((const TCHAR*) file_name);
					 
	if (ff_res != FR_OK)
	{//Delete file
		LOG_ERR("Failed to delete file, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_DELETE_FAILED;
	}
	
	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);	
	
	return SD_RESULT_OK;
}
/***********************************************************************
 * Function name: sdFileExist
 * Description: Checks if file exist
 * Parameters : file_name - File name to check
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_MOUNT_FAILED - SD mount failed
 *		SD_RESULT_ERROR_FILE_DOES_NOT_EXIST - file doesn't exist
 ***********************************************************************/
SD_RESULT_E sdFileExist(IN uint8_t* file_name)
{
	FRESULT 		ff_res;
	
	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;
	
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	ff_res = f_open(&my_file_exist, (const TCHAR*) file_name, FA_READ);
	
	if (ff_res != FR_OK)
	{//Open file for read
//		LOG_ERR("Failed to open file, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_FILE_DOES_NOT_EXIST;
	}	
	
	f_close(&my_file_exist);

	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);	
	
	return SD_RESULT_OK;
}
/***********************************************************************
 * Function name: sdDirExist
 * Description: Checks if directory exist
 * Parameters : 
 *
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_DIRECTORY_DOES_NOT_EXIST - directory doesn't exist
 * 		SD_RESULT_ERROR_MOUNT_FAILED - SD mount failed
 ***********************************************************************/
SD_RESULT_E sdDirExist(uint8_t* dir_name)
{
	DIR 			dir;
	FRESULT 		res;
	
	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;

	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	res = f_opendir(&dir, (char*)dir_name);
	
	if (res != FR_OK)
	{
		LOG_ERR("Failed to open directory, error: %d\r\n", res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_DIRECTORY_DOES_NOT_EXIST;		
	}

	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);
	
	return SD_RESULT_OK;
}
/***********************************************************************
 * Function name: sdChangeDir
 * Description: Set current directory
 * Parameters : 
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *
 ***********************************************************************/
SD_RESULT_E sdChangeDir(IN uint8_t* dir_name)
{
	if (strlen((char*)dir_name) > FILE_NAME_MAX_LENGTH)
		return SD_RESULT_ERROR;
	
	if (strcmp((char*)dir_name, "..") == 0)
		memset(current_dir, 0, sizeof(current_dir));
	else
		strcpy((char*)current_dir, (char*)dir_name);
	
	return SD_RESULT_OK;
}
/***********************************************************************
 * Function name: sdRenameFile
 * Description: Rename file name
 * Parameters : file_name_old - old file name, file_name_new - new file name
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_FAILED_TO_RENAME_FILE - failed to rename file
 *
 ***********************************************************************/
SD_RESULT_E sdRenameFile(IN uint8_t* file_name_old, 
						 IN uint8_t* file_name_new)
{

	FRESULT 		res;	
	
	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;

	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}	

	res = f_rename((char*)file_name_old, (char*)file_name_new);
	
	if (res != FR_OK)
	{
		LOG_ERR("Failed to rename file name, error: %d\r\n", res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_FAILED_TO_RENAME_FILE;		
	}	
	
	
	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);	
	
	return SD_RESULT_OK;
}
/***********************************************************************
 * Function name: sdDeleteDirContect
 * Description: Write data to file
 * Parameters : 
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_DELETE_FAILED - file delete failed
 *		SD_RESULT_ERROR_FAILED_TO_OPEN_DIR - failed to opern directory
 *		SD_RESULT_ERROR_MOUNT_FAILED - Fatfs mount failed
 *		SD_RESULT_ERROR_DIRECTORY_NAME_TOO_LONG - directory name too long
 ***********************************************************************/
SD_RESULT_E sdDeleteDirContect(IN uint8_t* dir_name_in)
{
	FRESULT 		ff_res = FR_OK;
//	DRESULT			d_res = RES_OK;
	uint8_t			dir_name[FILE_NAME_MAX_LENGTH + 1] = {0};
	DIR 			dir, dir2;
	FILINFO 		fno, fno2;
	char 			*fn, *fn2;
	static char 	lfn[_MAX_LFN + FILE_NAME_MAX_LENGTH + 1];   /* Buffer to store the LFN */
	static char 	lfn2[_MAX_LFN + FILE_NAME_MAX_LENGTH +1];   /* Buffer to store the LFN2 */
	char 			tmp[FILE_NAME_MAX_LENGTH + 1] = 0;
	char 			full_file_name[FILE_NAME_MAX_LENGTH * 2 + 1] = 0;
	
	
	if (strlen((char*)dir_name_in) > FILE_NAME_MAX_LENGTH)
		return SD_RESULT_ERROR_DIRECTORY_NAME_TOO_LONG;	
	else
		strcpy((char*)dir_name, (char*)dir_name_in);
		
	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;

	fno.lfname = lfn;
	fno2.lfname = lfn2;
	fno.lfsize = sizeof(lfn);
	fno2.lfsize = sizeof(lfn2);	
	
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	if (f_opendir(&dir, (char*)dir_name) != FR_OK)
	{
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_FAILED_TO_OPEN_DIR;
	}	
	
	while ((ff_res = f_readdir(&dir, &fno)) == FR_OK && fno.fname[0] != 0) 
	{
		/* Ignore files */
		if (fno.fname[0] == '.')
			continue;
		
		fn = *fno.lfname ? fno.lfname : fno.fname;
		memset(full_file_name, 0, sizeof(full_file_name));
		sprintf(full_file_name, "%s/%s", dir_name_in, fn);
		
		if (fno.fattrib & AM_DIR)
		{
			if (f_opendir(&dir2, full_file_name) != FR_OK)
			{
				prvSdMountSd(SD_STATUS_UNMOUNT);
				osMutexRelease(sdMutex);
				return SD_RESULT_ERROR_FAILED_TO_OPEN_DIR;
			}
			while ((ff_res = f_readdir(&dir2, &fno2)) == FR_OK && fno2.fname[0] != 0) 
			{
				/* Ignore files */
				if (fno.fname[0] == '.')
					continue;
				
				fn2 = *fno2.lfname ? fno2.lfname : fno2.fname;				
				
				if (!(fno2.fattrib & AM_DIR))
				{
					strcpy(tmp, fn2);
					sprintf(fno2.lfname, "%s/%s",full_file_name, tmp);
					ff_res = f_unlink((const TCHAR*) fno2.lfname);
					if (ff_res != FR_OK)
					{//Delete file
						LOG_ERR("Failed to delete file inside a directory, error: %d\r\n", ff_res);
						prvSdMountSd(SD_STATUS_UNMOUNT);
						osMutexRelease(sdMutex);
						return SD_RESULT_ERROR_DELETE_FAILED;				
					}
				}
			}
			f_closedir(&dir2);
			ff_res = f_unlink((const TCHAR*) full_file_name);
			if (ff_res != FR_OK)
			{//Delete file
				LOG_ERR("Failed to delete directory, error: %d\r\n", ff_res);
				prvSdMountSd(SD_STATUS_UNMOUNT);
				osMutexRelease(sdMutex);
				return SD_RESULT_ERROR_DELETE_FAILED;
			}	
		}
		else
		{
			ff_res = f_unlink((const TCHAR*) full_file_name);
			if (ff_res != FR_OK)
			{//Delete file
				LOG_ERR("Failed to delete file, error: %d\r\n", ff_res);
				prvSdMountSd(SD_STATUS_UNMOUNT);
				osMutexRelease(sdMutex);
				return SD_RESULT_ERROR_DELETE_FAILED;
			}			
		}
	}	
	
	
	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);	
	
	return SD_RESULT_OK;
}
//static char buffer[512];
/***********************************************************************
 * Function name: sdFormat
 * Description: Write data to file
 * Parameters : 
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_DELETE_FAILED - file delete failed
 *		SD_RESULT_ERROR_FAILED_TO_OPEN_DIR - failed to opern directory
 *		SD_RESULT_ERROR_MOUNT_FAILED - Fatfs mount failed
 ***********************************************************************/
SD_RESULT_E sdFormat(void)
{
	FRESULT 		ff_res = FR_OK;
//	DRESULT			d_res = RES_OK;
	uint8_t			dir_name[FILE_NAME_MAX_LENGTH + 1] = {0};
	DIR 			dir, dir2;
	FILINFO 		fno, fno2;
	char 			*fn, *fn2;
	static char 	lfn[_MAX_LFN + 1];   /* Buffer to store the LFN */
	static char 	lfn2[_MAX_LFN + 1];   /* Buffer to store the LFN2 */
	char 			tmp[30 + 1] = {0};
	
	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;

	fno.lfname = lfn;
	fno2.lfname = lfn2;
	fno.lfsize = sizeof(lfn);
	fno2.lfsize = sizeof(lfn2);	
	
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	if (f_opendir(&dir, (char*)dir_name) != FR_OK)
	{
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_FAILED_TO_OPEN_DIR;
	}	
	
	while ((ff_res = f_readdir(&dir, &fno)) == FR_OK && fno.fname[0] != 0) 
	{
		/* Ignore files */
		if (fno.fname[0] == '.')
			continue;
		
		fn = *fno.lfname ? fno.lfname : fno.fname;
		
		if (fno.fattrib & AM_DIR)
		{
			if (f_opendir(&dir2, fn) != FR_OK)
			{
				prvSdMountSd(SD_STATUS_UNMOUNT);
				osMutexRelease(sdMutex);
				return SD_RESULT_ERROR_FAILED_TO_OPEN_DIR;
			}
			while ((ff_res = f_readdir(&dir2, &fno2)) == FR_OK && fno2.fname[0] != 0) 
			{
				/* Ignore files */
				if (fno.fname[0] == '.')
					continue;
				
				fn2 = *fno2.lfname ? fno2.lfname : fno2.fname;				
				
				if (!(fno2.fattrib & AM_DIR))
				{
					strcpy(tmp, fn2);
					sprintf(fno2.lfname, "%s/%s",fn, tmp);
					ff_res = f_unlink((const TCHAR*) fno2.lfname);
					if (ff_res != FR_OK)
					{//Delete file
						LOG_ERR("Failed to delete file inside a directory, error: %d\r\n", ff_res);
						prvSdMountSd(SD_STATUS_UNMOUNT);
						osMutexRelease(sdMutex);
						return SD_RESULT_ERROR_DELETE_FAILED;				
					}
				}
			}
			f_closedir(&dir2);
			ff_res = f_unlink((const TCHAR*) fn);
			if (ff_res != FR_OK)
			{//Delete file
				LOG_ERR("Failed to delete directory, error: %d\r\n", ff_res);
				prvSdMountSd(SD_STATUS_UNMOUNT);
				osMutexRelease(sdMutex);
				return SD_RESULT_ERROR_DELETE_FAILED;
			}	
		}
		else
		{
			ff_res = f_unlink((const TCHAR*) fn);
			if (ff_res != FR_OK)
			{//Delete file
				LOG_ERR("Failed to delete file, error: %d\r\n", ff_res);
				prvSdMountSd(SD_STATUS_UNMOUNT);
				osMutexRelease(sdMutex);
				return SD_RESULT_ERROR_DELETE_FAILED;
			}			
		}
	}	
	
	
	
	
	
	
	
//	ff_res = f_mkfs("", 0, 0);
	

	
//	uint32_t i;
//	memset(buffer, 0, 512);
////	disk_initialize(0);
////	prvSdMountSd(SD_STATUS_UNMOUNT);
////	f_mount(0, (TCHAR const*)sd_path, 1);
//	for (i = sd_fatfs.fatbase; i < sd_fatfs.database; i++)
//		d_res = disk_write(0, buffer, i, 1);
//					 
//	if ((ff_res != FR_OK) || (d_res != RES_OK))
//	{//Format SD card
//		ERROR_PRINT("Failed to format SD card, error: %d\r\n", ff_res);
//		prvSdMountSd(SD_STATUS_UNMOUNT);
//		osMutexRelease(sdMutex);
//		return SD_RESULT_ERROR_WRITE;
//	}
	
	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);	
	
	return SD_RESULT_OK;
}

//uint8_t 		write_buffer[1000] = {0};
//uint8_t 		read_buffer[1000] = {0};

SD_RESULT_E sdTestSdCard(void)
{
//	SD_RESULT_E		res;
////	uint8_t 		write_buffer_hex[50] = {0};
//	uint16_t		data_read_length = 0;
//	uint32_t 		i = 0;
//	uint32_t 		fail = 0, total = 500000;
//	uint8_t			retry = 3, retries = 0;
//	
//	memset(write_buffer, 0x41, sizeof(write_buffer));
////	memset(write_buffer_hex, 0xAA, sizeof(write_buffer_hex));
//	
//	if (sdFileExist("test2.txt") == SD_RESULT_OK)
//	{
//		if (sdDeleteFile("test2.txt") != SD_RESULT_OK)
//			ERROR_PRINT("Failed to delete file\r\n");
//	}
//	
//	for (i = 0;i < total;i++)
//	{
//		sdSendMsgToQueue("test2.txt", write_buffer, sizeof(write_buffer));
//		osDelay(30);
//	}
//	
//	for (i = 0;i < total;i++)
//	{
//		memset(read_buffer, 0, sizeof(read_buffer));
//		data_read_length = 0;
//		res = SD_RESULT_ERROR;
//		retry = 3;
//		
//		while ((res != SD_RESULT_OK) && (retry-- != 0))
//		{
//			res = sdReadDataFromFile("test2.txt", 
//									sizeof(read_buffer), 
//									i * (sizeof(read_buffer)), 
//									read_buffer, 
//									&data_read_length);
//			if (memcmp(read_buffer, write_buffer, sizeof(write_buffer)) != 0)
//			{
//				retries++;
//				PRINT("Wrong read from file, index: %d, result:%d \r\n", i, retry);
//				if (retry == 0)
//					fail++;
//			}
//			osDelay(50);
//		}
//	}	
//	PRINT("Summary: %d out of %d failed, number of retries: %d\r\n", fail, total, retries);
//	
	return SD_RESULT_OK;
}

/***********************************************************************
 * Function name: sdGetDriveInfo
 * Description: Write data to file
 * Parameters : 
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR - on failure
 *		SD_RESULT_ERROR_DELETE_FAILED - file delete failed
 *		SD_RESULT_ERROR_FAILED_TO_OPEN_DIR - failed to opern directory
 *		SD_RESULT_ERROR_MOUNT_FAILED - Fatfs mount failed
 ***********************************************************************/
SD_RESULT_E sdGetSdInfo(uint32_t *total_size, uint32_t *free_size) 
{
	FATFS 			*fs;
    DWORD 			fre_clust;
	FRESULT 		res;

	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;

	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
    /* Get volume information and free clusters of drive */
    res = f_getfree((TCHAR const*)sd_path, &fre_clust, &fs);
    if (res != FR_OK) 
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_READING_SD_INFO;
	}

	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);	
	
	
    /* Get total sectors and free sectors */
    *total_size = (uint32_t)((fs->n_fatent - 2) * fs->csize * 0.5);
    *free_size = (uint32_t)(fre_clust * fs->csize * 0.5);
//	
//	LOG("SD capacity: %dKB\r\n", *total_size);
//	LOG("SD free space: %dKB\r\n", *free_size);
//	LOG("SD used space: %dKB\r\n", (*total_size - *free_size));
	
	/* Return OK */
	return SD_RESULT_OK;
}

uint8_t fatfsGetTime(DWORD* time)
{
	if (sdGetTime(time) == SD_RESULT_OK)
		return 0;
	else
		return 1;
}

/***********************************************************************
 * Function name: sdGetFileSize
 * Description: Read file size
 * Parameters : file_name - file name to read, file_size - file size
 * Returns :
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR_MOUNT_FAILED - Fatfs mount failed
 *		SD_RESULT_ERROR_FILE_DOES_NOT_EXIST - file doesn't exist
 ***********************************************************************/
SD_RESULT_E sdGetFileSize(IN uint8_t* file_name, 
						  OUT uint32_t *file_size)
{

	FRESULT 		ff_res;
	
	if (osMutexWait(sdMutex, SD_MUTEX_TIME_OUT) != osOK)
		return SD_RESULT_ERROR;
	
	if (prvSdMountSd(SD_STATUS_MOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	ff_res = f_open(&my_file_size, (const TCHAR*) file_name, FA_READ);
	
	if (ff_res != FR_OK)
	{//Open file for read
//		LOG_ERR("Failed to open file, error: %d\r\n", ff_res);
		prvSdMountSd(SD_STATUS_UNMOUNT);
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_FILE_DOES_NOT_EXIST;
	}	
	
	*file_size = my_file_size.fsize;
	
	f_close(&my_file_size);

	if (prvSdMountSd(SD_STATUS_UNMOUNT) != SD_RESULT_OK)
	{
		osMutexRelease(sdMutex);
		return SD_RESULT_ERROR_MOUNT_FAILED;	
	}
	
	osMutexRelease(sdMutex);	
	
	return SD_RESULT_OK;
}

/***********************************************************************
 * Function name: sdGetSdWriteBufferFree
 * Description: Get size of free space in buffer
 * Parameters : free_space - free space in precentage
 * Returns :
 *
 *		SD_RESULT_OK - on success
 *		SD_RESULT_ERROR_WRONG_PARAMETER - wrong parameter
 ***********************************************************************/
SD_RESULT_E sdGetSdWriteBufferFree(OUT uint8_t* free_space)
{
	if (free_space == NULL)
		return SD_RESULT_ERROR_WRONG_PARAMETER;
	
	*free_space = ((float32_t)(SD_WRITE_BUFFER_LENGTH - sd_write_buff_length) / SD_WRITE_BUFFER_LENGTH) * 100;
	
	return SD_RESULT_OK;
}