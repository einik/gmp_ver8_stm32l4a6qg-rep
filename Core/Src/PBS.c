#include "stm32l4xx_hal.h"
#include "cmsis_os.h"
//#include "global_defs.h"

#include "PBS.h"
//#include "BSP.h"

#include "ecg.h" //TBD - delete
#include "serial_protocol.h"
#include "versions.h"
#include "BLE.h"
#include "common.h"
#include "SD.h"
#include "device.h"
/****************************************************************/
/*						Local Definitions						*/
#define PBS_FLOW_CONTROL_TYPE					0xFFFF
#define PBS_RX_MSG_TIMEOUT						1000
#define PBS_TX_MSG_TIMEOUT						10
#define PBS_TX_QUEUE_SIZE						10
#define PBS_MAX_NUMBER_OF_FILES					200//100//50
#define PBS_CRC16 0x8005
#define PBS_SD_FLUSH_LENGTH						60	//120 // how much time back need to send
#define PBS_SD_FLUSH_NUM_OF_FILES				2
#ifdef GMP_DEMO_CHINA
#define PBS_SD_FLUSH_EVENT_TIME_OFFSET_START	15//30//60
#else
#define PBS_SD_FLUSH_EVENT_TIME_OFFSET_START	60
#endif //GMP_DEMO_CHINA
//#define PBS_SD_FLUSH_EVENT_TIME_OFFSET_END		60	//120
#define PBS_SD_ECG_FILES_DIRECTORY				"ECG"
#define PBS_SD_ECG_FILE_FORMAT					"ECG/%03d.tlv"
#define PBS_UNKNOWN_TIME_DIFF					20
#define PBS_SD_EVENT_START_DELIMITER			':'
#define PBS_SD_EVENT_END_DELIMITER				';'
#define PBS_SD_EVENT_LENGTH						100
#define PBS_TX_TASK_DELAY_MS					1000
#define PBS_SD_FILE_SIZE_MIN					163840//81920//2048
#define PBS_SD_FILE_LENGTH_HOURS				4
#define PBS_SD_FILE_DURATION_MAX 					23
#define PBS_SD_FILE_DURATION_MIN 					1
//#define PBS_SD_ECG_EVENTS_MAP_FILE_NAME				"events/1.txt"
#define PBS_SD_ECG_EVENTS_MAP_FILE_CHUNK_SIZE	(25 * PBS_SD_EVENT_LENGTH)//4000
#define PBS_SD_EVENT_MAP_FILE_OFFSET_INVALID	0xFFFFFFFF
#define PBS_TX_FLUSH_TIMEOUT					osWaitForever
#define PBS_EVENT_IND_INIT_FC 					(-2)
#define PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT	4
#define PBS_EVENT_IND_FLUSH_INVALID_OFFSET		0xFFFFFFFF
#define PBS_EVENT_ARR_TABLE_SIZE				20
// for andrey for tests
#if defined (GMP_DEMO_CHINA) || defined (SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME)
#define SD_FLUSH_DURATION_DEFAULT				30//90
#define PBS_EVENT_IND_FLUSH_TX_TIMEOUT_MSEC		300 //500
#else
#define SD_FLUSH_DURATION_DEFAULT				90
#define PBS_EVENT_IND_FLUSH_TX_TIMEOUT_MSEC		500
#endif //GMP_DEMO_CHINA
#define SD_FLUSH_TIMEOUT_EXTENSION				200
#define PBS_SD_EVENT_DURATION_DEFAULT			30
#define PBS_SD_READ_BUFFER_LENGTH				1500
#define PBS_PACKETS_COUNTER_IN_HALF_SECONDS(minutes)     minutes * 60 * 2

typedef enum {
	PBS_SD_EVENT_IS_SENT_NO	= 0,
	PBS_SD_EVENT_IS_SENT_YES = 1,
	PBS_SD_EVENT_IS_SENT_FAILED = 2,
	PBS_SD_EVENT_IS_SENT_HALTED = 3,
	PBS_SD_EVENT_IS_SENT_MAX
}PBS_SD_EVENT_IS_SENT_ENUM;

typedef enum{
	PBS_RX_STATE_WAITING_FOR_MSG,
	PBS_RX_STATE_INCOMPLETE_MSG
}PBS_RX_STATE_E;

typedef enum
{
	NONE, TLV_LIST, DATA_UNION
} RX_PAYLOAD_TYPE;



typedef struct {
//	PBS_TX_MESSAGE_E 		type;
	uint8_t				tx_count;
	uint8_t 				flow_control;
	uint16_t				msg_len;
	uint8_t  			msg[0];
}PBS_TX_MESSAGE_STRUCT;


typedef struct {
	uint32_t			file_index;	//offset from start of file
	uint8_t				file_num;	// ref to pbs_sd_file
}PBS_SD_FLUSH_STRUCT;


typedef struct {
	ECG_MEAS_EVENT_TYPE_ENUM					event_type;
	ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT 	arr_change;
	uint16_t									event_duration;
	uint32_t									sd_file_offset;
	uint8_t										sd_file_name[SD_FILE_NAME_MAX_LENGTH];
	PBS_TIMESTAMP_STRUCT 						timestamp;
}PBS_SD_FLUSH_MSG;


typedef union {
	struct {
		int8_t										start_delim;
		PBS_SD_EVENT_IS_SENT_ENUM					is_sent;
		uint8_t										sd_file_name[SD_FILE_NAME_MAX_LENGTH];
		uint32_t									sd_file_offset;
		ECG_ALGO_ARRYTHMIA_EVENT_HEADER_STRUCT 		arr_type;
		ECG_ARRYTHMIA_CHANGE_E 						event_type;
		uint16_t									event_duration;
		PBS_TIMESTAMP_STRUCT 						timestamp;
		uint32_t									event_uuid;
		uint8_t										additional_sd_file_name[SD_FILE_NAME_MAX_LENGTH];	// TODO: reserved
		uint32_t									additional_sd_file_offset;
		PBS_TIMESTAMP_STRUCT 						onset_timestamp;	// Optional, only for for offset header message
		int8_t										end_delim;
	}fields;
	uint8_t raw[PBS_SD_EVENT_LENGTH];				// TODO: pad to a fix length of 100 bytes per record
}PBS_SD_EVENTS_MAP_ENTRY_STRUCT;

typedef enum{
	PBS_ECG_EVENT_STATUS_OFF,
	PBS_ECG_EVENT_STATUS_ON
}PBS_ECG_EVENT_STATUS_E;

typedef struct
{
	uint32_t 					uuid;
	PBS_TIMESTAMP_STRUCT		time_stamp;
	PBS_ECG_EVENT_STATUS_E		event_status;//
}PBS_ECG_EVENT_E;

/****************************************************************/

/****************************************************************/
/*						Local Parameters						*/
osThreadId 								pbs_task_handle;
osMessageQId 							pbs_rx_queue_handle;
osThreadId 								pbs_tx_task_handle;
osMessageQId 							pbs_tx_queue_handle;
static 	osMutexId 						pbs_tx_mutex;
static 	osMutexId 						pbs_flush_mutex;

static 	osMutexId 						pbs_tlv_build_mutex;

static SW_FLOW_CONTROL              	pbs_flow_control = {.enable = SW_FLOW_CONTROL_ON, .counter = 0};
static PBS_CONNECTION_STATE_ENUM    	pbs_conn_state = PBS_CONNECTION_STATE_DISCONNECTED;
static PBS_CONNECTION_STATE_ENUM    	pbsGetBleConnected = PBS_CONNECTION_STATE_DISCONNECTED;


static PBS_OUTPUT_TYPE_E            	pbs_output_mode = PBS_OUTPUT_TYPE_OFF;
static PBS_HOURLY_TYPE_E            	pbs_send_hourly_mode = PBS_OUTPUT_TYPE_HOURLY_OFF;





static uint8_t 							pbs_current_ecg_file[15] = {0};//TODO:Constant
static uint8_t 							pbs_current_ecg_file_num = 1;

#pragma location = ".RAM2"
static PBS_SD_FLUSH_STRUCT 				pbs_sd_flush[PBS_SD_FLUSH_LENGTH];	// cyclic array to hold previous 60 second
static uint8_t 							pbs_sd_flush_index;

static uint8_t 							pbs_sd_file[PBS_SD_FLUSH_NUM_OF_FILES][SD_FILE_NAME_MAX_LENGTH];
static uint8_t							pbs_sd_file_index;
static uint8_t 							pbs_file_duration = PBS_SD_FILE_LENGTH_HOURS;


static uint16_t 						tlv_send_index;
static uint32_t 						events_map_file_current_entry_offset = PBS_SD_EVENT_MAP_FILE_OFFSET_INVALID;

#pragma location = ".RAM2"
static uint8_t 		events_map_file_payload[PBS_SD_ECG_EVENTS_MAP_FILE_CHUNK_SIZE];	// local storage in RAM


static uint8_t duration_ext_for_test = SD_FLUSH_DURATION_DEFAULT;

static uint16_t flush_tx_delay_timeout = PBS_EVENT_IND_FLUSH_TX_TIMEOUT_MSEC;


static uint8_t 		pbs_event_ind_acked_fc = (uint8_t)PBS_EVENT_IND_INIT_FC;

static uint8_t		flag_halt_tx;
#pragma location = ".RAM2"
static uint8_t 		file_payload[PBS_SD_READ_BUFFER_LENGTH];

//static uint32_t 	event_uuid;

//static uint32_t							uuid_arr_table;//[PBS_EVENT_ARR_TABLE_SIZE];	// per 20 arrhythmias
//static PBS_TIMESTAMP_STRUCT				onset_timestamp_arr_table;//[PBS_EVENT_ARR_TABLE_SIZE];
static uint8_t 							new_event_pending_flag = FALSE;
static PBS_ECG_EVENT_E					pbs_ecg_events[PBS_EVENT_ARR_TABLE_SIZE] = {0};

static PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENUM		pbs_holter_remote_event = PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_DISABLE;
static uint16_t														pbs_holter_remote_event_packets = 0;

/****************************************************************/

/****************************************************************/
/*						Local Functions		 					*/
#pragma location = ".RAM2"
static PBS_BUFFER_U pbs_buffer[PBS_TX_NUM_OF_BUFFERS];
static uint8_t pbs_buffer_index = 0;
static void pbsTask(void const * argument);
static RESULT_E prvPbsTlvInit(TLV_STRUCT * tlv_in, uint16_t message_type, PBS_PAYLOAD_TYPE payload_type, PBS_DATA_U * data, uint16_t opt_byte_array_length);
static RESULT_E prvPbsAddSubTlvToTlv(TLV_STRUCT * tlv_in, TLV_STRUCT * tlv_sub, uint16_t sub_type, uint16_t sub_length, PBS_PAYLOAD_TYPE payload_type, PBS_DATA_U data);
static PBS_RESULT_ENUM prvPbsSendMessage(TLV_STRUCT * tlv, PBS_OUTPUT_TYPE_E mode);
static RESULT_E prvPbsRxMessageParser(uint16_t type, uint16_t length, uint8_t *value);
static RESULT_E prvPbsBufferToTlvList(uint16_t length, uint8_t *inBuff, PBS_TLV_LIST_STRUCT *tlv, uint8_t tlv_list_length);

static PBS_RESULT_ENUM prvPbsCrc16Calc(IN const uint8_t *data, IN uint16_t size, OUT uint16_t * crc);
static PBS_RESULT_ENUM prvPbsPreparsingCrcCheck(uint8_t * msg, uint16_t total_length);
static PBS_RESULT_ENUM pbsFillBufferWithTlv(TLV_STRUCT * tlv, uint8_t recur_depth);

//static PBS_RESULT_ENUM prvPbsSendControlledMessage(uint8_t * msg, uint16_t length, uint8_t flow_control);

static void pbsTxTask(void const * argument);
static PBS_RESULT_ENUM prvPbsIsTimestampOk(PBS_TIMESTAMP_STRUCT * timestamp);
static PBS_RESULT_ENUM prvPbsSearchHeaderInBufferMessageFromEnd(IN uint8_t *buff, IN uint16_t length, uint8_t ** header_loc);

/****************************************************************/
/*						External Parameters						*/
//extern osMessageQId *usbRxQueueHandle;
/****************************************************************/

/****************************************************************/
/*						Weak Functions							*/
__weak RESULT_E pbsRxMessageHandler(uint16_t type, PBS_RX_PAYLOAD_STRUCT payload){return RESULT_OK;}
__weak RESULT_E pbsSetDeviceErrorState(PBS_DEVICE_STATUS_BIT_ERROR_ENUM error){return RESULT_OK;}
/****************************************************************/

// ---------------------------------------- GMP INFRASTRUCTURE START ---------------------------------

PBS_RESULT_ENUM pbsInit(PBS_TX_TASK_MODE_E mode)
{

	osMutexDef(pbs_tx_mutex);
	osMutexDef(pbs_flush_mutex);
	osMutexDef(pbs_tlv_build_mutex);

	pbs_tx_mutex = osMutexCreate(osMutex(pbs_tx_mutex));
	if (!pbs_tx_mutex)
		return PBS_RESULT_ERROR_OS;
	pbs_flush_mutex = osMutexCreate(osMutex(pbs_flush_mutex));
	if (!pbs_flush_mutex)
		return PBS_RESULT_ERROR_OS;
	pbs_tlv_build_mutex = osMutexCreate(osMutex(pbs_tlv_build_mutex));
	if (!pbs_tlv_build_mutex)
		return PBS_RESULT_ERROR_OS;
	//Init queue
	osMessageQDef(pbsRxQueue, PBS_QUEUE_SIZE, PBS_QUEUE_MSG_STRUCT *);
	pbs_rx_queue_handle = osMessageCreate(osMessageQ(pbsRxQueue), NULL);
	if (!pbs_rx_queue_handle)
		return PBS_RESULT_ERROR_OS;

   	//Init task
	osThreadDef(pbsTask, pbsTask, osPriorityNormal, 0, PBS_STACK_SIZE);
	pbs_task_handle = osThreadCreate(osThread(pbsTask), NULL);
	if (!pbs_task_handle)
		return PBS_RESULT_ERROR_OS;

   if  ( (pbs_output_mode == PBS_OUTPUT_TYPE_OFF) || (pbs_output_mode == PBS_OUTPUT_TYPE_SD_ONLY) )
      return PBS_RESULT_OK;


//	osMessageQDef(pbs_tx_q, PBS_TX_QUEUE_SIZE, PBS_SD_FLUSH_MSG *);
//	pbs_tx_queue_handle = osMessageCreate(osMessageQ(pbs_tx_q), NULL);
//	if (!pbs_tx_queue_handle)
//		return PBS_RESULT_ERROR_OS;

        //if this mode will be enable may be conflict with file_payload locate in 10 minute task.
	if (mode == PBS_TX_TASK_MODE_ON)//in the past it handle 60 second last, 30 second forward.
	{
		//Init task
		osThreadDef(pbs_tx_task, pbsTxTask, osPriorityNormal, 0, PBS_TX_STACK_SIZE);
		pbs_tx_task_handle = osThreadCreate(osThread(pbs_tx_task), NULL);
		if (!pbs_task_handle)
			return PBS_RESULT_ERROR_OS;
	}
	memset(&pbs_sd_flush, 0xFF, sizeof(pbs_sd_flush) );
	return PBS_RESULT_OK;
}


// Tx Functions
static PBS_RESULT_ENUM prvPbsFlushTxBuffer(uint8_t *buff, uint16_t length)
{
	osStatus resMutex;
	PBS_RESULT_ENUM ret_val = PBS_RESULT_OK;
	if ((length == 0) || (buff == NULL))
		return PBS_RESULT_ERROR_WRONG_PARAM;

	resMutex = osMutexWait(pbs_flush_mutex, PBS_TX_FLUSH_TIMEOUT);

	if (resMutex != osOK)
		return PBS_RESULT_ERROR_OS;


	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_4, "pbs_flush_buffer starting address 0x%x\r\n", buff);


   if ( (pbs_output_mode == PBS_OUTPUT_TYPE_SD_BLE) || (pbs_output_mode == PBS_OUTPUT_TYPE_BLE_ONLY) || (pbs_send_hourly_mode == PBS_OUTPUT_TYPE_BLE_HOURLY))
	{
		BLE_RESULT res = BLE_RESULT_OK;

		//	DEBUG_PRINT(DEBUG_LEVEL_5, "\r\npbs_flush_buffer starting address 0x%x", buff);

		if ((pbs_conn_state == PBS_CONNECTION_STATE_CONNECTED) || (PBS_CONNECTION_STATE_CONNECTED == pbsGetBleConnectionState()))
                {
			res = bleSendMsg(buff, length);
                }
//		pbs_buffer_index ^= 1;
		if (res != BLE_RESULT_OK)
		{
			ret_val = PBS_RESULT_ERROR_BLE;
		}
	}
   else
   {
//      pbs_buffer_index ^= 1;
   }

	resMutex = osMutexRelease(pbs_flush_mutex);

	if (resMutex != osOK)
		ret_val = PBS_RESULT_ERROR_OS;


	return ret_val;
}
/* Initiate tlv_in with input parameters
 *
 * Output: modified *message
 * Input:  	tlv type,
 * 			payload_type,
 * 			data (all supported data types union),
 * 			opt_byte_array_length (optional, used for byte array only)
 * 			TLV Array: Init with null payload, use pbs_tlv_add_sub_tlv() to feed with sub TLV,
 * 			Nested TLV: init sub TLV with tlv_init, add TLV list payload with pbs_tlv_add_sub_tlv(), then add this TLV sub to initiated message
 */
static RESULT_E prvPbsTlvInit(TLV_STRUCT * tlv_in, uint16_t message_type, PBS_PAYLOAD_TYPE payload_type, PBS_DATA_U * data, uint16_t opt_byte_array_length)
{
	tlv_in->type = message_type;
	tlv_in->data.payload_type = payload_type;
	tlv_in->length = 0;	//PBS_TX_OVERHEAD;
	tlv_in->next = NULL;

	switch (payload_type)
	{
	case PBS_PAYLOAD_INTEGER:
		tlv_in->data.data.i = data->i;
		tlv_in->length += sizeof(int32_t);
		break;
	case PBS_PAYLOAD_FLOAT:
		tlv_in->length += sizeof(float32_t);
		tlv_in->data.data.f = data->f;
		break;
	case PBS_PAYLOAD_CHAR:
		tlv_in->length += sizeof(uint8_t);
		tlv_in->data.data.ch = data->ch;
		break;
	case PBS_PAYLOAD_SHORT:
		tlv_in->length += sizeof(uint16_t);
		tlv_in->data.data.sh = data->sh;
		break;
	case PBS_PAYLOAD_BYTE_ARRAY:
		tlv_in->length += opt_byte_array_length;
		tlv_in->data.data.byte_array = data->byte_array;
		break;
	case PBS_PAYLOAD_TLV_ARRAY:
		tlv_in->data.data.tlv_list = NULL;
		break;
	case PBS_PAYLOAD_NONE:
		tlv_in->data.data.byte_array = NULL;
		break;
	}
	return RESULT_OK;
}



static RESULT_E prvPbsAddSubTlvToTlv(TLV_STRUCT * tlv_in, TLV_STRUCT * tlv_sub, uint16_t sub_type, uint16_t sub_length, PBS_PAYLOAD_TYPE payload_type, PBS_DATA_U data)
{
	if (tlv_in->data.payload_type != PBS_PAYLOAD_TLV_ARRAY)
		return RESULT_BAD_COMMAND;

	osStatus resMutex = osMutexWait(pbs_tlv_build_mutex, PBS_TX_FLUSH_TIMEOUT);
	if (tlv_in->data.data.tlv_list == NULL)
		tlv_in->data.data.tlv_list = tlv_sub;
	else
	{
		TLV_STRUCT *tlv_ptr = tlv_in->data.data.tlv_list;
		while (tlv_ptr->next != NULL)
			tlv_ptr = tlv_ptr->next;
		tlv_ptr->next = tlv_sub;
	}

	tlv_sub->next = NULL;
	tlv_sub->type = sub_type;
	tlv_sub->data.payload_type = payload_type;

	if (payload_type != PBS_PAYLOAD_TLV_ARRAY)
		tlv_sub->length = 0;

	switch (payload_type)
	{
	case PBS_PAYLOAD_INTEGER:
		tlv_sub->length += sizeof(int32_t);
		tlv_sub->data.data.i = data.i;
		break;
	case PBS_PAYLOAD_FLOAT:
		tlv_sub->length += sizeof(float32_t);
		tlv_sub->data.data.f = data.f;
		break;
	case PBS_PAYLOAD_CHAR:
		tlv_sub->length += sizeof(uint8_t);
		tlv_sub->data.data.ch = data.ch;
		break;
	case PBS_PAYLOAD_SHORT:
		tlv_sub->length += sizeof(uint16_t);
		tlv_sub->data.data.sh = data.sh;
		break;
	case PBS_PAYLOAD_BYTE_ARRAY:
		tlv_sub->length += sub_length;
		tlv_sub->data.data.byte_array = data.byte_array;
		break;
	case PBS_PAYLOAD_TLV_ARRAY: //nested TLV support
		break;
	case PBS_PAYLOAD_NONE:
		break;
	}

	tlv_in->length += tlv_sub->length + PBS_TLV_VALUE_OFFSET;
	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_4, "pbs_tlv_add_sub_tlv: added type %d with length %d , new length = %d\r\n", sub_type, tlv_sub->length, tlv_in->length);
	resMutex = osMutexRelease(pbs_tlv_build_mutex);
	return RESULT_OK;

}
// ---------------------------------------- GMP INFRASTRUCTURE END ---------------------------------



//REVIEW
// --------------------------------------- PBS INTERFACE START --------------------------------------------------


PBS_RESULT_ENUM pbsSendGetDeviceInfoResp(PBS_DEVICE_INFO_STRUCT * info)
{
	if (info == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	TLV_STRUCT 			message, tlv_hw_ver, tlv_serial_num, tlv_fw_ver, tlv_ble_mac, tlv_device_sn, tlv_bl_ver, tlv_pbs_dev_type;
	PBS_DATA_U 		data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_GENERAL_GET_INFO, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.byte_array = info->mcu_sn;
	prvPbsAddSubTlvToTlv(&message, &tlv_serial_num, PBS_COMMAND_GENERAL_GET_INFO_RESP_MCU_SN, sizeof(info->mcu_sn), PBS_PAYLOAD_BYTE_ARRAY, data);
	data.byte_array = (uint8_t*)&info->fw;
	prvPbsAddSubTlvToTlv(&message, &tlv_fw_ver, PBS_COMMAND_GENERAL_GET_INFO_RESP_FW_VER, sizeof(info->fw), PBS_PAYLOAD_BYTE_ARRAY, data);
	data.byte_array = (uint8_t*)&info->hw;
	prvPbsAddSubTlvToTlv(&message, &tlv_hw_ver, PBS_COMMAND_GENERAL_GET_INFO_RESP_HW_VER, sizeof(info->hw), PBS_PAYLOAD_BYTE_ARRAY, data);
	data.byte_array = info->ble_mac;
	prvPbsAddSubTlvToTlv(&message, &tlv_ble_mac, PBS_COMMAND_GENERAL_GET_INFO_RESP_BLE_MAC, sizeof(info->ble_mac), PBS_PAYLOAD_BYTE_ARRAY, data);
	data.byte_array = (uint8_t*)&info->bootloader;
	prvPbsAddSubTlvToTlv(&message, &tlv_bl_ver, PBS_COMMAND_GENERAL_GET_INFO_RESP_BOOTLOADER_VER, sizeof(info->bootloader), PBS_PAYLOAD_BYTE_ARRAY, data);
	data.byte_array = info->device_sn;
	prvPbsAddSubTlvToTlv(&message, &tlv_device_sn, PBS_COMMAND_GENERAL_GET_INFO_RESP_DEVICE_S_N, sizeof(info->device_sn), PBS_PAYLOAD_BYTE_ARRAY, data);
	data.ch = info->device_type;
	prvPbsAddSubTlvToTlv(&message, &tlv_pbs_dev_type, PBS_COMMAND_GENERAL_GET_INFO_RESP_DEVICE_TYPE, 0, PBS_PAYLOAD_CHAR, data);


	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}

PBS_RESULT_ENUM pbsSendGetStatusResp(PBS_DEVICE_STATUS_STRUCT * status)
{
	if (status == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	TLV_STRUCT message, tlv_state, tlv_err, tlv_bit, tlv_batt_level, tlv_temperature, tlv_own, tlv_volt, tlv_button;
	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_GENERAL_GET_STATUS, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.ch = status->state;
	prvPbsAddSubTlvToTlv(&message, &tlv_state, PBS_COMMAND_GENERAL_GET_STATUS_RESP_STATE, 0, PBS_PAYLOAD_CHAR, data);

	if (status->state == PBS_DEVICE_STATUS_STATE_ERROR)
	{
		data.sh = status->error;
		prvPbsAddSubTlvToTlv(&message, &tlv_err, PBS_COMMAND_GENERAL_GET_STATUS_RESP_ERROR_TYPE, 0, PBS_PAYLOAD_SHORT, data);
	}

	if (status->bit_status)
	{
		data.sh = status->bit_status;
		prvPbsAddSubTlvToTlv(&message, &tlv_bit, PBS_COMMAND_GENERAL_GET_STATUS_RESP_BIT_ERROR, 0, PBS_PAYLOAD_SHORT, data);
	}
	data.ch = status->bat_level;
	prvPbsAddSubTlvToTlv(&message, &tlv_batt_level, PBS_COMMAND_GENERAL_GET_STATUS_RESP_BATTERY_LEVEL, 0, PBS_PAYLOAD_CHAR, data);
	data.f = status->temperature;
	prvPbsAddSubTlvToTlv(&message, &tlv_temperature, PBS_COMMAND_GENERAL_GET_STATUS_RESP_TEMPERATURE, 0, PBS_PAYLOAD_FLOAT, data);
	data.ch = status->owned;
	prvPbsAddSubTlvToTlv(&message, &tlv_own, PBS_COMMAND_GENERAL_GET_STATUS_RESP_OWNED, 0, PBS_PAYLOAD_CHAR, data);
	data.sh = status->voltage;
	prvPbsAddSubTlvToTlv(&message, &tlv_volt, PBS_COMMAND_GENERAL_GET_STATUS_RESP_VOLTAGE, 0, PBS_PAYLOAD_SHORT, data);

	if (status->button)
	{
		data.ch = status->button;
		prvPbsAddSubTlvToTlv(&message, &tlv_button, PBS_COMMAND_GENERAL_GET_STATUS_RESP_BUTTON, 0, PBS_PAYLOAD_CHAR, data);
	}
	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}

PBS_RESULT_ENUM pbsSendDeviceControlResp(PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS_ENUM status)
{
	TLV_STRUCT message, tlv_state;
	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_GENERAL_CONTROL, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.sh = status;
	prvPbsAddSubTlvToTlv(&message, &tlv_state, PBS_COMMAND_DEVICE_CONTROL_RESP_STATUS, 0, PBS_PAYLOAD_SHORT, data);

	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}

PBS_RESULT_ENUM pbsSendDeviceConfigSetResp(PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_STATUS_ENUM status)
{
	TLV_STRUCT message, tlv_state;
	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_DEVICE_CONFIGURATION_SET, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.sh = status;
	prvPbsAddSubTlvToTlv(&message, &tlv_state, PBS_COMMAND_DEVICE_CONFIGURATION_SET_RESP_STATUS, 0, PBS_PAYLOAD_SHORT, data);

	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}

PBS_RESULT_ENUM pbsSendDeviceConfigGetResp(PBS_DEVICE_CONFIG_OBJECT * conf)
{
	if (conf == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	TLV_STRUCT message, tlv_time, tlv_leds, tlv_name;
	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_DEVICE_CONFIGURATION_GET, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.byte_array = (uint8_t *)&conf->timestamp;	// TODO: check correct endianess
	prvPbsAddSubTlvToTlv(&message, &tlv_time, PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_TIME, sizeof(conf->timestamp), PBS_PAYLOAD_BYTE_ARRAY, data);

	data.ch = conf->leds_state;
	prvPbsAddSubTlvToTlv(&message, &tlv_leds, PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_SET_LED, 0, PBS_PAYLOAD_CHAR, data);

	data.byte_array = conf->device_name;	// TODO: if device_name contains null termination
	prvPbsAddSubTlvToTlv(&message, &tlv_name, PBS_COMMAND_DEVICE_CONFIGURATION_OBJECT_DEVICE_NAME, conf->device_name_length, PBS_PAYLOAD_BYTE_ARRAY, data);


	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}

PBS_RESULT_ENUM pbsSendECGMeasurementRealtimeEnableResp(PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS_ENUM status, PBS_TIMESTAMP_STRUCT * time)
{
	TLV_STRUCT message, tlv_time, tlv_status;
	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_ECG_MEASUREMENT_REALTIME_ENABLE, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.sh = status;
	prvPbsAddSubTlvToTlv(&message, &tlv_status, PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_STATUS, 0, PBS_PAYLOAD_SHORT, data);

	if (time != NULL)
	{
		data.byte_array = time->data;	// TODO: check correct endianess
		prvPbsAddSubTlvToTlv(&message, &tlv_time, PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE_RESP_BASE_TIMESTAMP, sizeof(PBS_TIMESTAMP_LENGTH), PBS_PAYLOAD_BYTE_ARRAY, data);
	}

	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}
PBS_RESULT_ENUM pbsSendECGMeasurementRemoteEventResp(PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS_ENUM status, PBS_TIMESTAMP_STRUCT * time)
{
	TLV_STRUCT message, tlv_time, tlv_status;
	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_ECG_MEASUREMENT_REMOTE_EVENT, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.sh = status;
	prvPbsAddSubTlvToTlv(&message, &tlv_status, PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_STATUS, 0, PBS_PAYLOAD_SHORT, data);

	if (time != NULL)
	{
		data.byte_array = time->data;	// TODO: check correct endianess
		prvPbsAddSubTlvToTlv(&message, &tlv_time, PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_RESP_BASE_TIMESTAMP, sizeof(time->data), PBS_PAYLOAD_BYTE_ARRAY, data);
	}
	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}


// to be called upon change from device
PBS_RESULT_ENUM pbsFlowControlEnable(SW_FLOW_CONTROL_ENUM en)
{
	if (en >  SW_FLOW_CONTROL_ON)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	pbs_flow_control.enable = en;

	return PBS_RESULT_OK;
}

// to be called on start from device
PBS_RESULT_ENUM prvPbsFlowControlReset()
{
	pbs_flow_control.counter = 0;
	return PBS_RESULT_OK;
}



static PBS_RESULT_ENUM prvPbsAttachFlowControlToMessage(TLV_STRUCT * msg, TLV_STRUCT * tlv_fc)
{
	if ( (msg == NULL) || (tlv_fc == NULL) )
		return PBS_RESULT_ERROR_WRONG_PARAM;

	if (pbs_flow_control.enable == SW_FLOW_CONTROL_OFF)
		return PBS_RESULT_OK;

	PBS_DATA_U 		data;

	data.ch = pbs_flow_control.counter;
	prvPbsAddSubTlvToTlv(msg, tlv_fc, PBS_FLOW_CONTROL_TYPE, 0, PBS_PAYLOAD_CHAR, data);
	pbs_flow_control.counter = (pbs_flow_control.counter + 1) % PBS_FLOW_CONTROL_MAX;

	return PBS_RESULT_OK;
}



PBS_RESULT_ENUM pbsEcgHolterMeasurementRealtimeProgressInd(ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT * msg)
{
	if (msg == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	TLV_STRUCT 		message = {0};
	PBS_DATA_U 		data = {0};
	TLV_STRUCT		tlv_ecg_index,  ecg_lead_status, tlv_pace_maker_start, tlv_pace_maker_end, tlv_timestamp;
	TLV_STRUCT 		tlv_samples_l1 = {0}, tlv_bit_map_l1 = {0}, tlv_samples_l2 = {0}, tlv_bit_map_l2 = {0}, tlv_flow_control = {0};
	TLV_STRUCT		tlv_event_type = {0};


	prvPbsTlvInit(&message, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_IND, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	// TODO: SW_FLOW_CONTROL * fc should be owned by PBS or Device
	prvPbsAttachFlowControlToMessage(&message, &tlv_flow_control);

	data.i = msg->common.ecg_index;
	prvPbsAddSubTlvToTlv(&message, &tlv_ecg_index, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ECG_INDEX, 0, PBS_PAYLOAD_INTEGER, data);

	data.ch = msg->common.lead_status;
	prvPbsAddSubTlvToTlv(&message, &ecg_lead_status, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_STATUS, 0, PBS_PAYLOAD_CHAR, data);

	data.byte_array = (uint8_t *)&msg->base_timestamp;
	prvPbsAddSubTlvToTlv(&message, &tlv_timestamp, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_TIMESTAMP, sizeof(msg->base_timestamp), PBS_PAYLOAD_BYTE_ARRAY, data);

	if (msg->event_type == ECG_MEAS_EVENT_TYPE_MANUAL)
	{
		data.ch = msg->event_type;
		prvPbsAddSubTlvToTlv(&message, &tlv_event_type, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_TYPE, 0, PBS_PAYLOAD_CHAR, data);
	}


	if (msg->common.pace_maker.index_start != (uint16_t)ECG_PACE_MAKER_INVALID)
	{
		data.sh = msg->common.pace_maker.index_start;
		prvPbsAddSubTlvToTlv(&message, &tlv_pace_maker_start, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_PACE_MAKER_START_INDEX, 0, PBS_PAYLOAD_SHORT, data);

		data.sh = msg->common.pace_maker.index_stop;
		prvPbsAddSubTlvToTlv(&message, &tlv_pace_maker_end, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_PACE_MAKER_STOP_INDEX, 0, PBS_PAYLOAD_SHORT, data);
	}


	switch (msg->ecg_type)
	{
	case ECG_MEAS_SIGNAL_RAW:
		// lead 1
		data.byte_array = msg->ecg_lead_1->raw.buff.raw;
		prvPbsAddSubTlvToTlv(&message, &tlv_samples_l1, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_RAW_ECG_DATA,
				msg->ecg_lead_1->raw.buff_byte_length, PBS_PAYLOAD_BYTE_ARRAY, data);

		// lead 2
		data.byte_array = msg->ecg_lead_2->raw.buff.raw;
		prvPbsAddSubTlvToTlv(&message, &tlv_samples_l2, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_RAW_ECG_DATA,
				msg->ecg_lead_1->raw.buff_byte_length, PBS_PAYLOAD_BYTE_ARRAY, data);
		break;

	case ECG_MEAS_SIGNAL_RAW_COMPRESSED:

		// lead 1
		data.byte_array = msg->ecg_lead_1->raw_comp.samples;
		prvPbsAddSubTlvToTlv(&message, &tlv_samples_l1, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_RAW_ECG_DATA,
				msg->ecg_lead_1->raw_comp.sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);

		data.byte_array = msg->ecg_lead_1->raw_comp.bit_map;
		prvPbsAddSubTlvToTlv(&message, &tlv_bit_map_l1,
				PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_RAW_ECG_DATA_MAP,
				msg->ecg_lead_1->raw_comp.bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);

		// lead 2
		data.byte_array = msg->ecg_lead_2->raw_comp.samples;
		prvPbsAddSubTlvToTlv(&message, &tlv_samples_l2, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_RAW_ECG_DATA,
				msg->ecg_lead_2->raw_comp.sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);

		data.byte_array = msg->ecg_lead_2->raw_comp.bit_map;
		prvPbsAddSubTlvToTlv(&message, &tlv_bit_map_l2,
				PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_RAW_ECG_DATA_MAP,
				msg->ecg_lead_2->raw_comp.bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);
		break;
	default:
		break;
	}


	PBS_RESULT_ENUM res;

	res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_SD_ONLY);//send to sdcard

	return res;
}


PBS_RESULT_ENUM pbsSendECGMeasurementRealtimeProgressInd(ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT * msg, uint8_t is_send_arrythmias)
{
	if (msg == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	TLV_STRUCT 		message;
	PBS_DATA_U 	data;
	TLV_STRUCT		tlv_ecg_index, tlv_ecg_status, ecg_lead_status, tlv_pace_maker_start, tlv_pace_maker_end;
	TLV_STRUCT 		tlv_samples_l1, tlv_bit_map_l1, tlv_samples_l2, tlv_bit_map_l2, tlv_flow_control;
	TLV_STRUCT 		tlv_resp_samples, tlv_resp_bit_map;
	TLV_STRUCT		tlv_respiration_rate, tlv_heart_rate, tlv_peaks;

	TLV_STRUCT 		tlv_sub, tlv_algo_repoty_type, /*tlv_time_period,*/ tlv_event_bitmap, tlv_runs, tlv_pvc, tlv_bigemeny;
	TLV_STRUCT 		tlv_couplet_pvcs, tlv_triplet_pvcs, tlv_trigemeny, tlv_r_on_t, tlv_premature_atrial_contraction;
	TLV_STRUCT 		tlv_multifocal_pvcs, tlv_interpolated_pvcs, tlv_resp_apnea_duration;

	prvPbsTlvInit(&message, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_IND, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	// TODO: SW_FLOW_CONTROL * fc should be owned by PBS or Device
	prvPbsAttachFlowControlToMessage(&message, &tlv_flow_control);

	data.i = msg->common.ecg_index;
	prvPbsAddSubTlvToTlv(&message, &tlv_ecg_index, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ECG_INDEX, 0, PBS_PAYLOAD_INTEGER, data);

	data.ch = msg->ecg_status;
	prvPbsAddSubTlvToTlv(&message, &tlv_ecg_status, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ECG_STATUS, 0, PBS_PAYLOAD_CHAR, data);

	data.ch = msg->common.lead_status;
	prvPbsAddSubTlvToTlv(&message, &ecg_lead_status, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_STATUS, 0, PBS_PAYLOAD_CHAR, data);




	// Only if both leads are on
	if (msg->common.lead_status == (ECG_LEAD_STATUS_LEAD_1_ON | ECG_LEAD_STATUS_LEAD_2_ON) )
	{

		// TODO: only if leads on? (PBS doc)
		if (msg->common.pace_maker.index_start != (uint16_t)ECG_PACE_MAKER_INVALID)
		{
			data.sh = msg->common.pace_maker.index_start;
			prvPbsAddSubTlvToTlv(&message, &tlv_pace_maker_start, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_PACE_MAKER_START_INDEX, 0, PBS_PAYLOAD_SHORT, data);

			data.sh = msg->common.pace_maker.index_stop;
			prvPbsAddSubTlvToTlv(&message, &tlv_pace_maker_end, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_PACE_MAKER_STOP_INDEX, 0, PBS_PAYLOAD_SHORT, data);
		}

		// attach ECG signal
		switch (msg->ecg_type)
		{
		case ECG_MEAS_SIGNAL_RAW:
			// lead 1
			data.byte_array = msg->ecg_lead_1->raw.buff.raw;
			prvPbsAddSubTlvToTlv(&message, &tlv_samples_l1, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_RAW_ECG_DATA,
					msg->ecg_lead_1->raw.buff_byte_length, PBS_PAYLOAD_BYTE_ARRAY, data);

			// lead 2
			data.byte_array = msg->ecg_lead_2->raw.buff.raw;
			prvPbsAddSubTlvToTlv(&message, &tlv_samples_l2, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_RAW_ECG_DATA,
					msg->ecg_lead_1->raw.buff_byte_length, PBS_PAYLOAD_BYTE_ARRAY, data);
			break;

		case ECG_MEAS_SIGNAL_RAW_COMPRESSED:

			// lead 1
			data.byte_array = msg->ecg_lead_1->raw_comp.samples;
			prvPbsAddSubTlvToTlv(&message, &tlv_samples_l1, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_RAW_ECG_DATA,
					msg->ecg_lead_1->raw_comp.sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);

			data.byte_array = msg->ecg_lead_1->raw_comp.bit_map;
			prvPbsAddSubTlvToTlv(&message, &tlv_bit_map_l1,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_RAW_ECG_DATA_MAP,
					msg->ecg_lead_1->raw_comp.bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);

			// lead 2
			data.byte_array = msg->ecg_lead_2->raw_comp.samples;
			prvPbsAddSubTlvToTlv(&message, &tlv_samples_l2, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_RAW_ECG_DATA,
					msg->ecg_lead_2->raw_comp.sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);

			data.byte_array = msg->ecg_lead_2->raw_comp.bit_map;
			prvPbsAddSubTlvToTlv(&message, &tlv_bit_map_l2,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_RAW_ECG_DATA_MAP,
					msg->ecg_lead_2->raw_comp.bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);
			break;

		case ECG_MEAS_SIGNAL_250HZ_FILTERED_COMPRESSED:
		case ECG_MEAS_SIGNAL_500HZ_FILTERED_COMPRESSED:

			// lead 1
			data.byte_array = msg->ecg_lead_1->filtered_comp.samples;
			prvPbsAddSubTlvToTlv(&message, &tlv_samples_l1,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_FILTERED_ECG_DATA,
					msg->ecg_lead_1->filtered_comp.sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);

			data.byte_array = msg->ecg_lead_1->filtered_comp.bit_map;
			prvPbsAddSubTlvToTlv(&message, &tlv_bit_map_l1,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_COMPRESSED_FILTERED_ECG_DATA_MAP,
					msg->ecg_lead_1->filtered_comp.bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);

			// lead 2
			data.byte_array = msg->ecg_lead_2->filtered_comp.samples;
			prvPbsAddSubTlvToTlv(&message, &tlv_samples_l2,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_FILTERED_ECG_DATA,
					msg->ecg_lead_2->filtered_comp.sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);

			data.byte_array = msg->ecg_lead_2->filtered_comp.bit_map;
			prvPbsAddSubTlvToTlv(&message, &tlv_bit_map_l2,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_COMPRESSED_FILTERED_ECG_DATA_MAP,
					msg->ecg_lead_2->filtered_comp.bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);

			break;
		case ECG_MEAS_SIGNAL_FILTERED:
			// lead 1
			data.byte_array = msg->ecg_lead_1->filtered.buff.raw;
			prvPbsAddSubTlvToTlv(&message, &tlv_samples_l1, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_1_FILTERED_ECG_DATA,
					msg->ecg_lead_1->filtered.buff_byte_length, PBS_PAYLOAD_BYTE_ARRAY, data);

			// lead 2
			data.byte_array = msg->ecg_lead_2->filtered.buff.raw;
			prvPbsAddSubTlvToTlv(&message, &tlv_samples_l2, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_LEAD_2_FILTERED_ECG_DATA,
					msg->ecg_lead_2->filtered.buff_byte_length, PBS_PAYLOAD_BYTE_ARRAY, data);

			break;
		default:
			return PBS_RESULT_NOT_IMPLEMENTED;
			break;
		}

		// attach respiration signal
		switch (msg->resp_type)
		{
		case ECG_MEAS_RESPIRATION_SIGNAL_500HZ_RAW_COMPRESSED:
			data.byte_array = msg->respiration->raw_comp.samples;
			prvPbsAddSubTlvToTlv(&message, &tlv_resp_samples,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_RAW_DATA_COMPRESSED, msg->respiration->raw_comp.sample_len,
					PBS_PAYLOAD_BYTE_ARRAY, data);
			data.byte_array = msg->respiration->raw_comp.bit_map;
			prvPbsAddSubTlvToTlv(&message, &tlv_resp_bit_map,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_RAW_DATA_COMPRESSED_MAP,
					msg->respiration->raw_comp.bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);
			break;
		case ECG_MEAS_RESPIRATION_SIGNAL_50HZ_FILTERED_COMPRESSED:
			data.byte_array = msg->respiration->filtered_comp.samples;
			prvPbsAddSubTlvToTlv(&message, &tlv_resp_samples,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_COMPRESSED_FILTERED_DATA,
					msg->respiration->filtered_comp.sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);
			data.byte_array = msg->respiration->filtered_comp.bit_map;
			prvPbsAddSubTlvToTlv(&message, &tlv_resp_bit_map,
					PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_COMPRESSED_FILTERED_DATA_MAP,
					msg->respiration->filtered_comp.bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);
			break;
      case ECG_MEAS_RESPIRATION_OFF:
         break;
			// TODO: Not Implemented
		case ECG_MEAS_RESPIRATION_RAW:
		case ECG_MEAS_RESPIRATION_FILTERED:
		default:
			return PBS_RESULT_NOT_IMPLEMENTED;
			break;
		}

		// Respiration algorithm results
		if (msg->resp_type != ECG_MEAS_RESPIRATION_OFF)
		{
			data.sh = msg->common.respiration_algo_results->respiration_rate;
			prvPbsAddSubTlvToTlv(&message, &tlv_respiration_rate, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESPIRATION_RATE, 0,
					PBS_PAYLOAD_SHORT, data);
		}

		if (msg->common.algo_results->peaks.peaks_len > 0)
		{
			data.byte_array = (uint8_t *) msg->common.algo_results->peaks.ecgPeakIndex;
			prvPbsAddSubTlvToTlv(&message, &tlv_peaks, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ECG_PEAKS,
					(msg->common.algo_results->peaks.peaks_len * sizeof(uint32_t)), PBS_PAYLOAD_BYTE_ARRAY, data);
		}

		data.sh = msg->common.algo_results->heart_rate;
		prvPbsAddSubTlvToTlv(&message, &tlv_heart_rate, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_HEART_RATE, 0, PBS_PAYLOAD_SHORT,
				data);

		// TODO: is is_send_arrythmias needed?
		if (is_send_arrythmias && (msg->common.algo_results->arrhythmia) && (msg->common.arrythmias != NULL))
		{
			prvPbsTlvInit(&tlv_sub, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ALGORITHM_EXTENDED_RESULT, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

			data.ch = PBS_ECG_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE_CURRENT_STATE;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_algo_repoty_type, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE, 0,
					PBS_PAYLOAD_CHAR, data);

			//		data.sh = 120;	// TODO: fix according t
			//		pbs_tlv_add_sub_tlv(&message, &tlv_time_period, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TIME_PERIOD, 0, PBS_PAYLOAD_SHORT, data);
			if (msg->common.arrythmias->events_flags.raw)
			{
				data.sh = msg->common.arrythmias->events_flags.raw;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_event_bitmap, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_EVENTS_BITMAP, 0,
						PBS_PAYLOAD_SHORT, data);
			}
			if (msg->common.arrythmias->runs)
			{
				data.sh = msg->common.arrythmias->runs;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_runs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_RUNS, 0, PBS_PAYLOAD_SHORT, data);
			}
			if (msg->common.arrythmias->pvc)
			{
				data.sh = msg->common.arrythmias->pvc;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_pvc, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_PVC, 0, PBS_PAYLOAD_SHORT, data);
			}
			if (msg->common.arrythmias->bigeminy)
			{
				data.sh = msg->common.arrythmias->bigeminy;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_bigemeny, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_BIGEMENY, 0, PBS_PAYLOAD_SHORT,
						data);
			}
			if (msg->common.arrythmias->couplet_pvcs)
			{
				data.sh = msg->common.arrythmias->couplet_pvcs;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_couplet_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_COUPLET_OF_PVCS, 0,
						PBS_PAYLOAD_SHORT, data);
			}
			if (msg->common.arrythmias->triplet_pvcs)
			{
				data.sh = msg->common.arrythmias->triplet_pvcs;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_triplet_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TRIPLET_OF_PVCS, 0,
						PBS_PAYLOAD_SHORT, data);
			}
			if (msg->common.arrythmias->trigeminy)
			{
				data.sh = msg->common.arrythmias->trigeminy;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_trigemeny, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TRIGEMENY, 0,
						PBS_PAYLOAD_SHORT, data);
			}
			if (msg->common.arrythmias->r_on_t)
			{
				data.sh = msg->common.arrythmias->r_on_t;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_r_on_t, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_R_ON_T, 0, PBS_PAYLOAD_SHORT,
						data);
			}
			if (msg->common.arrythmias->premature_atrial_contraction)
			{
				data.sh = msg->common.arrythmias->premature_atrial_contraction;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_premature_atrial_contraction,
						PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_PREMATURE_ATRIAL_CONTRACTION, 0, PBS_PAYLOAD_SHORT, data);
			}
			if (msg->common.arrythmias->multifocal_of_pvc)
			{
				data.sh = msg->common.arrythmias->multifocal_of_pvc;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_multifocal_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_MULTIFOCAL_PVCS, 0,
						PBS_PAYLOAD_SHORT, data);
			}
			if (msg->common.arrythmias->interpolated_pvc)
			{
				data.sh = msg->common.arrythmias->interpolated_pvc;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_interpolated_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_INTERPOLATED_PVCS, 0,
						PBS_PAYLOAD_SHORT, data);
			}

			if (msg->common.respiration_algo_results->apnea_time)
			{
				data.sh = msg->common.respiration_algo_results->apnea_time;
				prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_resp_apnea_duration, PBS_ECG_ALGORITHM_EXTENDED_RESULTS_RESPIRATION_APNEA_DURATION, 0,
						PBS_PAYLOAD_SHORT, data);
			}

			prvPbsAddSubTlvToTlv(&message, &tlv_sub, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_ALGORITHM_EXTENDED_RESULT, 0,
					PBS_PAYLOAD_TLV_ARRAY, data);

		}

	}
	PBS_RESULT_ENUM res;

	res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_SD_ONLY);

	return res;
}

//ECG_ALGO_ARRYTHMIA_EVENT_HEADER_STRUCT * arr, ECG_ARRYTHMIA_CHANGE_E change, PBS_TIMESTAMP_STRUCT * timestamp, uint32_t arr_uuid
PBS_RESULT_ENUM prvPbsSendECGMeasurementEventIndHeader(PBS_SD_EVENTS_MAP_ENTRY_STRUCT * msg)
{
	if (msg == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	TLV_STRUCT 		message;
	PBS_DATA_U 	data;
	TLV_STRUCT		tlv_fc, tlv_ecg_index, tlv_packet_type,    tlv_timestamp, tlv_event_duration;
	TLV_STRUCT 		tlv_uuid, tlv_timestamp_onset;

	TLV_STRUCT 		tlv_sub, tlv_algo_repoty_type, /*tlv_time_period,*/ tlv_event_bitmap, tlv_runs, tlv_pvc, tlv_bigemeny;
	TLV_STRUCT 		tlv_couplet_pvcs, tlv_triplet_pvcs, tlv_trigemeny, tlv_r_on_t, tlv_premature_atrial_contraction;
	TLV_STRUCT 		tlv_multifocal_pvcs, tlv_interpolated_pvcs;

	prvPbsTlvInit(&message, PBS_ECG_MEASUREMENT_EVENT_IND, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.ch = -1;
	prvPbsAddSubTlvToTlv(&message, &tlv_fc, PBS_FLOW_CONTROL_TYPE, 0, PBS_PAYLOAD_CHAR, data);

	data.i = -1;
	prvPbsAddSubTlvToTlv(&message, &tlv_ecg_index, PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX, 0, PBS_PAYLOAD_INTEGER, data);

	data.byte_array = msg->fields.timestamp.data;
	prvPbsAddSubTlvToTlv(&message, &tlv_timestamp, PBS_ECG_MEASUREMENT_EVENT_IND_BASE_TIMESTAMP, sizeof(msg->fields.timestamp.data), PBS_PAYLOAD_BYTE_ARRAY, data);

	if (msg->fields.event_uuid != (uint32_t)-1)
	{
		data.i = msg->fields.event_uuid;
		prvPbsAddSubTlvToTlv(&message, &tlv_uuid, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_UUID, 0, PBS_PAYLOAD_INTEGER, data);
	}

    data.sh = msg->fields.event_duration;
    prvPbsAddSubTlvToTlv(&message, &tlv_event_duration, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_DURATION, 0, PBS_PAYLOAD_SHORT, data);

	if (msg->fields.is_sent == PBS_SD_EVENT_IS_SENT_FAILED)	// message to be sent upon files failure
	{
		data.ch = ECG_MEAS_EVENT_PACKET_TYPE_FAILED;
		prvPbsAddSubTlvToTlv(&message, &tlv_packet_type, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_PACKET_TYPE, 0, PBS_PAYLOAD_CHAR, data);
		if (prvPbsIsTimestampOk(&msg->fields.onset_timestamp) == PBS_RESULT_OK)
		{
			data.byte_array = msg->fields.onset_timestamp.data;
			prvPbsAddSubTlvToTlv(&message, &tlv_timestamp_onset, PBS_ECG_MEASUREMENT_EVENT_IND_ONSET_BASE_TIMESTAMP, sizeof(msg->fields.onset_timestamp.data), PBS_PAYLOAD_BYTE_ARRAY, data);
		}
	}
	else
	{
		switch (msg->fields.event_type)
			{
			case ECG_ARRYTHMIA_CHANGE_START:
				data.ch = ECG_MEAS_EVENT_PACKET_TYPE_HEADER_ONSET;
				break;
			case ECG_ARRYTHMIA_CHANGE_STOP:
				if (prvPbsIsTimestampOk(&msg->fields.onset_timestamp) == PBS_RESULT_OK)
				{
					data.byte_array = msg->fields.onset_timestamp.data;
					prvPbsAddSubTlvToTlv(&message, &tlv_timestamp_onset, PBS_ECG_MEASUREMENT_EVENT_IND_ONSET_BASE_TIMESTAMP, sizeof(msg->fields.onset_timestamp.data), PBS_PAYLOAD_BYTE_ARRAY, data);
				}
				data.ch = ECG_MEAS_EVENT_PACKET_TYPE_HEADER_OFFSET;
				break;
			case ECG_ARRYTHMIA_CHANGE_BUTTON:
				data.ch = ECG_MEAS_EVENT_PACKET_TYPE_HEADER_BUTTON;
				break;
			case ECG_ARRYTHMIA_CHANGE_REMOTE:
				data.ch = ECG_MEAS_EVENT_PACKET_TYPE_HEADER_REMOTE;
				break;
			default:
				LOG("prvPbsSendECGMeasurementEventIndHeader Error change %d\r\n", msg->fields.event_type);
				break;
			}
			prvPbsAddSubTlvToTlv(&message, &tlv_packet_type, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_PACKET_TYPE, 0, PBS_PAYLOAD_CHAR, data);
	}



//	data.byte_array = (uint8_t *)&msg->base_timestamp;
//	prvPbsAddSubTlvToTlv(&message, &tlv_timestamp, PBS_ECG_MEASUREMENT_EVENT_IND_BASE_TIMESTAMP, sizeof(msg->base_timestamp), PBS_PAYLOAD_BYTE_ARRAY, data);


	// TODO: is is_send_arrythmias needed?
	if ( msg->fields.arr_type.raw != 0 )
	{
		prvPbsTlvInit(&tlv_sub, PBS_ECG_MEASUREMENT_EVENT_IND_ALGORITHM_EXTENDED_RESULT, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

		data.ch = PBS_ECG_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE_CURRENT_STATE;
		prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_algo_repoty_type, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE, 0, PBS_PAYLOAD_CHAR, data);

		if (msg->fields.arr_type.bitmap.runs)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_runs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_RUNS, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.pvc)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_pvc, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_PVC, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.bigeminy)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_bigemeny, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_BIGEMENY, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.couplet_pvcs)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_couplet_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_COUPLET_OF_PVCS, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.triplet_pvcs)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_triplet_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TRIPLET_OF_PVCS, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.trigeminy)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_trigemeny, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TRIGEMENY, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.r_on_t)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_r_on_t, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_R_ON_T, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.premature_atrial_contraction)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_premature_atrial_contraction, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_PREMATURE_ATRIAL_CONTRACTION, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.multifocal_of_pvc)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_multifocal_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_MULTIFOCAL_PVCS, 0, PBS_PAYLOAD_SHORT, data);
		}
		else if (msg->fields.arr_type.bitmap.interpolated_pvc)
		{
			data.sh = 1;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_interpolated_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_INTERPOLATED_PVCS, 0, PBS_PAYLOAD_SHORT, data);
		}

		else
		{
			ECG_ARRYTH_EVENT_BITMAP events_flags = {0};
			if (msg->fields.arr_type.bitmap.ventricular_asystole)
			{
			 events_flags.bitmap.ventricular_asystole = 1;
			}
			else if (msg->fields.arr_type.bitmap.ventricular_fibrillation)
			{
				 events_flags.bitmap.ventricular_fibrillation = 1;
			}
			else if (msg->fields.arr_type.bitmap.ventricular_tachycardia)
			{
				 events_flags.bitmap.ventricular_tachycardia = 1;
			}
			else if (msg->fields.arr_type.bitmap.pause)
			{
				 events_flags.bitmap.pause = 1;
			}
			else if (msg->fields.arr_type.bitmap.bradicardia)
			{
				 events_flags.bitmap.bradicardia = 1;
			}
			else if (msg->fields.arr_type.bitmap.tachycardia)
			{
				 events_flags.bitmap.tachycardia = 1;
			}
			else if (msg->fields.arr_type.bitmap.irregular_rhythm)
			{
				 events_flags.bitmap.irregular_rhythm = 1;
			}
			else if (msg->fields.arr_type.bitmap.ideoventricular_rhythm)
			{
				 events_flags.bitmap.ideoventricular_rhythm = 1;
			}
			else if (msg->fields.arr_type.bitmap.supravent_tachycardia)
			{
				 events_flags.bitmap.supravent_tachycardia = 1;
			}
			else if (msg->fields.arr_type.bitmap.resp_ecg_coincidence)
			{
				 events_flags.bitmap.resp_ecg_coincidence = 1;
			}
			else
			{
				LOG("prvPbsSendECGMeasurementEventIndHeader no arrythmia?\r\n");
			}

			data.sh = events_flags.raw;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_event_bitmap, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_EVENTS_BITMAP, 0, PBS_PAYLOAD_SHORT, data);
		}

//
//		if (msg->arr.bitmap.apnea_time)
//		{
//			data.sh = 1;
//			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_resp_apnea_duration, PBS_ECG_ALGORITHM_EXTENDED_RESULTS_RESPIRATION_APNEA_DURATION, 0, PBS_PAYLOAD_SHORT, data);
//		}


		prvPbsAddSubTlvToTlv(&message, &tlv_sub, PBS_ECG_MEASUREMENT_EVENT_IND_ALGORITHM_EXTENDED_RESULT, 0, PBS_PAYLOAD_TLV_ARRAY, data);

	}


	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}

PBS_RESULT_ENUM pbsSendECGContactInfoInd(PBS_ECG_GET_INFO_STATUS_E status, ECG_LEAD_STATUS_ENUM lead_status, IMPEDANCE_EVENT_STRUCT * impedance_results)
{
	TLV_STRUCT 		message;
	PBS_DATA_U 		data;
	TLV_STRUCT		tlv_ecg_lead_status, tlv_status;
	TLV_STRUCT		tlv_sub_imp, tlv_imp_summary, tlv_imp_ra, tlv_imp_la, tlv_imp_ll;

	prvPbsTlvInit(&message, PBS_ECG_MEASUREMENT_CONTACT_IND, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	if (status != PBS_ECG_GET_INFO_STATUS_OK)
	{
		data.sh = status;
		prvPbsAddSubTlvToTlv(&message, &tlv_status, PBS_ECG_CONTACT_INFO_RESP_STATUS, 0, PBS_PAYLOAD_SHORT, data);
	}
	else if (impedance_results != NULL)
	{
		data.sh = status;
		prvPbsAddSubTlvToTlv(&message, &tlv_status, PBS_ECG_CONTACT_INFO_RESP_STATUS, 0, PBS_PAYLOAD_SHORT, data);

	    data.ch = lead_status;
		prvPbsAddSubTlvToTlv(&message, &tlv_ecg_lead_status, PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_STATUS, 0, PBS_PAYLOAD_CHAR, data);

		prvPbsTlvInit(&tlv_sub_imp, PBS_ECG_MEASUREMENT_EVENT_IND_IMPEDANCE, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

		data.ch = impedance_results->leads_status;
		prvPbsAddSubTlvToTlv(&tlv_sub_imp, &tlv_imp_summary, PBS_ECG_IMPEDANCE_RESULTS_SUMMARY, 0, PBS_PAYLOAD_CHAR, data);

		data.sh = impedance_results->measurements.ra_kohm;
		prvPbsAddSubTlvToTlv(&tlv_sub_imp, &tlv_imp_ra, PBS_ECG_IMPEDANCE_RESULTS_RA_KOHM, 0, PBS_PAYLOAD_SHORT, data);

		data.sh = impedance_results->measurements.la_kohm;
		prvPbsAddSubTlvToTlv(&tlv_sub_imp, &tlv_imp_la, PBS_ECG_IMPEDANCE_RESULTS_LA_KOHM, 0, PBS_PAYLOAD_SHORT, data);

		data.sh = impedance_results->measurements.ll_kohm;
		prvPbsAddSubTlvToTlv(&tlv_sub_imp, &tlv_imp_ll, PBS_ECG_IMPEDANCE_RESULTS_LL_KOHM, 0, PBS_PAYLOAD_SHORT, data);

		prvPbsAddSubTlvToTlv(&message, &tlv_sub_imp, PBS_ECG_MEASUREMENT_EVENT_IND_IMPEDANCE, 0, PBS_PAYLOAD_TLV_ARRAY, data);
	}


	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}
PBS_RESULT_ENUM pbsSendECGMeasurementEventInd(ECG_MEAS_EVENT_INDICATION_STRUCT * msg, IMPEDANCE_EVENT_STRUCT * impedance_results)
{
	if (msg == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	TLV_STRUCT 		message;
	PBS_DATA_U 	data;
	TLV_STRUCT		tlv_ecg_index, tlv_packet_type, tlv_ecg_lead_status, tlv_pace_maker_start, tlv_pace_maker_end, tlv_timestamp, tlv_event_type;
	TLV_STRUCT 		tlv_samples_l1, tlv_bit_map_l1, tlv_samples_l2, tlv_bit_map_l2, tlv_flow_control;
	TLV_STRUCT 		tlv_resp_samples, tlv_resp_bit_map;
	TLV_STRUCT		tlv_respiration_rate, tlv_heart_rate, tlv_peaks;

	TLV_STRUCT 		tlv_sub, tlv_algo_repoty_type, /*tlv_time_period,*/ tlv_event_bitmap, tlv_runs, tlv_pvc, tlv_bigemeny;
	TLV_STRUCT 		tlv_couplet_pvcs, tlv_triplet_pvcs, tlv_trigemeny, tlv_r_on_t, tlv_premature_atrial_contraction;
	TLV_STRUCT 		tlv_multifocal_pvcs, tlv_interpolated_pvcs, tlv_resp_apnea_duration;

	TLV_STRUCT		tlv_sub_imp, tlv_imp_summary, tlv_imp_ra, tlv_imp_la, tlv_imp_ll;

	prvPbsTlvInit(&message, PBS_ECG_MEASUREMENT_EVENT_IND, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	// TODO: SW_FLOW_CONTROL * fc should be owned by PBS or Device
	prvPbsAttachFlowControlToMessage(&message, &tlv_flow_control);
    LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_1, "pbs event ind add fc %d\r\n", tlv_flow_control.data.data.ch);


	data.i = msg->common.ecg_index;
	prvPbsAddSubTlvToTlv(&message, &tlv_ecg_index, PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX, 0, PBS_PAYLOAD_INTEGER, data);

	data.ch = msg->packet_type;
	prvPbsAddSubTlvToTlv(&message, &tlv_packet_type, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_PACKET_TYPE, 0, PBS_PAYLOAD_CHAR, data);

	data.ch = msg->common.lead_status;
	prvPbsAddSubTlvToTlv(&message, &tlv_ecg_lead_status, PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_STATUS, 0, PBS_PAYLOAD_CHAR, data);

	if (msg->common.pace_maker.index_start != PM_POSITION_NOT_ASSIGNED)
	{
    	LOG_D(DEBUG_MODULE_ECG, DEBUG_LEVEL_1, "** PM pbs %d\r\n", msg->common.pace_maker.index_start);
		data.sh = msg->common.pace_maker.index_start;
		prvPbsAddSubTlvToTlv(&message, &tlv_pace_maker_start, PBS_ECG_MEASUREMENT_EVENT_IND_PACE_MAKER_START_INDEX, 0, PBS_PAYLOAD_SHORT, data);

		if (msg->common.pace_maker.index_stop != PM_POSITION_NOT_ASSIGNED)
		{
			data.sh = msg->common.pace_maker.index_stop;
			prvPbsAddSubTlvToTlv(&message, &tlv_pace_maker_end, PBS_ECG_MEASUREMENT_EVENT_IND_PACE_MAKER_STOP_INDEX, 0, PBS_PAYLOAD_SHORT, data);
		}
	}

	data.byte_array = (uint8_t *)&msg->base_timestamp;
	prvPbsAddSubTlvToTlv(&message, &tlv_timestamp, PBS_ECG_MEASUREMENT_EVENT_IND_BASE_TIMESTAMP, sizeof(msg->base_timestamp), PBS_PAYLOAD_BYTE_ARRAY, data);

	if (msg->event_type != ECG_MEAS_EVENT_TYPE_MAX)
	{
		data.ch = msg->event_type;
		prvPbsAddSubTlvToTlv(&message, &tlv_event_type, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_TYPE, 0, PBS_PAYLOAD_CHAR, data);
	}


	// attach ECG signal
	// lead 1
	if (msg->common.lead_status & ECG_LEAD_STATUS_LEAD_1_ON)
	{
		data.byte_array = msg->lead_1->samples;
		prvPbsAddSubTlvToTlv(&message, &tlv_samples_l1, PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_1_COMPRESSED_RAW_ECG_DATA,
				msg->lead_1->sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);

		data.byte_array = msg->lead_1->bit_map;
		prvPbsAddSubTlvToTlv(&message, &tlv_bit_map_l1, PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_1_COMPRESSED_RAW_ECG_DATA_MAP,
				msg->lead_1->bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);
	}

	// lead 2
	if (msg->common.lead_status & ECG_LEAD_STATUS_LEAD_2_ON)
	{
		data.byte_array = msg->lead_2->samples;
		prvPbsAddSubTlvToTlv(&message, &tlv_samples_l2, PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_2_COMPRESSED_RAW_ECG_DATA,
				msg->lead_2->sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);

		data.byte_array = msg->lead_2->bit_map;
		prvPbsAddSubTlvToTlv(&message, &tlv_bit_map_l2, PBS_ECG_MEASUREMENT_EVENT_IND_LEAD_2_COMPRESSED_RAW_ECG_DATA_MAP,
				msg->lead_2->bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);
	}


	// attach respiration signal, TODO: is check enough to verify lead is on (other ADS)
	if (msg->common.lead_status & ECG_LEAD_STATUS_LEAD_1_ON)
	{
		data.byte_array = msg->filt_comp_resp->samples;
		prvPbsAddSubTlvToTlv(&message, &tlv_resp_samples, PBS_ECG_MEASUREMENT_EVENT_IND_RESPIRATION_COMPRESSED_FILTERED_DATA,
				msg->filt_comp_resp->sample_len, PBS_PAYLOAD_BYTE_ARRAY, data);
		data.byte_array = msg->filt_comp_resp->bit_map;
		prvPbsAddSubTlvToTlv(&message, &tlv_resp_bit_map, PBS_ECG_MEASUREMENT_EVENT_IND_RESPIRATION_COMPRESSED_FILTERED_DATA_MAP,
				msg->filt_comp_resp->bit_map_len, PBS_PAYLOAD_BYTE_ARRAY, data);
	}

	// Respiration algorithm results
	data.sh = msg->common.respiration_algo_results->respiration_rate;
	prvPbsAddSubTlvToTlv(&message, &tlv_respiration_rate, PBS_ECG_MEASUREMENT_EVENT_IND_RESPIRATION_RATE,
			0, PBS_PAYLOAD_SHORT, data);

	if (msg->common.algo_results->peaks.peaks_len > 0)
	{
		data.byte_array = (uint8_t *)msg->common.algo_results->peaks.ecgPeakIndex;
		prvPbsAddSubTlvToTlv(&message, &tlv_peaks, PBS_ECG_MEASUREMENT_EVENT_IND_ECG_PEAKS,
				(msg->common.algo_results->peaks.peaks_len * sizeof(uint32_t)), PBS_PAYLOAD_BYTE_ARRAY, data);
	}

	data.sh = msg->common.algo_results->heart_rate;
	prvPbsAddSubTlvToTlv(&message, &tlv_heart_rate, PBS_ECG_MEASUREMENT_EVENT_IND_HEART_RATE, 0, PBS_PAYLOAD_SHORT, data);

	// TODO: is is_send_arrythmias needed?
	if ( (msg->common.algo_results->arrhythmia) && (msg->common.arrythmias != NULL) )
	{
		prvPbsTlvInit(&tlv_sub, PBS_ECG_MEASUREMENT_EVENT_IND_ALGORITHM_EXTENDED_RESULT, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

		data.ch = PBS_ECG_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE_CURRENT_STATE;
		prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_algo_repoty_type, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_REPORT_TYPE, 0, PBS_PAYLOAD_CHAR, data);

		//		data.sh = 120;	// TODO: fix according t
		//		pbs_tlv_add_sub_tlv(&message, &tlv_time_period, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TIME_PERIOD, 0, PBS_PAYLOAD_SHORT, data);
		if (msg->common.arrythmias->events_flags.raw)
		{
			data.sh = msg->common.arrythmias->events_flags.raw;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_event_bitmap, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_EVENTS_BITMAP, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->runs)
		{
			data.sh = msg->common.arrythmias->runs;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_runs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_RUNS, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->pvc)
		{
			data.sh = msg->common.arrythmias->pvc;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_pvc, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_PVC, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->bigeminy)
		{
			data.sh = msg->common.arrythmias->bigeminy;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_bigemeny, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_BIGEMENY, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->couplet_pvcs)
		{
			data.sh = msg->common.arrythmias->couplet_pvcs;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_couplet_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_COUPLET_OF_PVCS, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->triplet_pvcs)
		{
			data.sh = msg->common.arrythmias->triplet_pvcs;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_triplet_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TRIPLET_OF_PVCS, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->trigeminy)
		{
			data.sh = msg->common.arrythmias->trigeminy;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_trigemeny, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_TRIGEMENY, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->r_on_t)
		{
			data.sh = msg->common.arrythmias->r_on_t;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_r_on_t, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_R_ON_T, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->premature_atrial_contraction)
		{
			data.sh = msg->common.arrythmias->premature_atrial_contraction;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_premature_atrial_contraction, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_PREMATURE_ATRIAL_CONTRACTION, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->multifocal_of_pvc)
		{
			data.sh = msg->common.arrythmias->multifocal_of_pvc;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_multifocal_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_MULTIFOCAL_PVCS, 0, PBS_PAYLOAD_SHORT, data);
		}
		if (msg->common.arrythmias->interpolated_pvc)
		{
			data.sh = msg->common.arrythmias->interpolated_pvc;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_interpolated_pvcs, PBS_ECG_RESPIRATIONM_ALGORITHM_EXTENDED_RESULTS_INTERPOLATED_PVCS, 0, PBS_PAYLOAD_SHORT, data);
		}



		if (msg->common.respiration_algo_results->apnea_time)
		{
			data.sh = msg->common.respiration_algo_results->apnea_time;
			prvPbsAddSubTlvToTlv(&tlv_sub, &tlv_resp_apnea_duration, PBS_ECG_ALGORITHM_EXTENDED_RESULTS_RESPIRATION_APNEA_DURATION, 0, PBS_PAYLOAD_SHORT, data);
		}


		prvPbsAddSubTlvToTlv(&message, &tlv_sub, PBS_ECG_MEASUREMENT_EVENT_IND_ALGORITHM_EXTENDED_RESULT, 0, PBS_PAYLOAD_TLV_ARRAY, data);

	}

	if (impedance_results != NULL)
	{
		prvPbsTlvInit(&tlv_sub_imp, PBS_ECG_MEASUREMENT_EVENT_IND_IMPEDANCE, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

		data.ch = impedance_results->leads_status;
		prvPbsAddSubTlvToTlv(&tlv_sub_imp, &tlv_imp_summary, PBS_ECG_IMPEDANCE_RESULTS_SUMMARY, 0, PBS_PAYLOAD_CHAR, data);

		data.sh = impedance_results->measurements.ra_kohm;
		prvPbsAddSubTlvToTlv(&tlv_sub_imp, &tlv_imp_ra, PBS_ECG_IMPEDANCE_RESULTS_RA_KOHM, 0, PBS_PAYLOAD_SHORT, data);

		data.sh = impedance_results->measurements.la_kohm;
		prvPbsAddSubTlvToTlv(&tlv_sub_imp, &tlv_imp_la, PBS_ECG_IMPEDANCE_RESULTS_LA_KOHM, 0, PBS_PAYLOAD_SHORT, data);

		data.sh = impedance_results->measurements.ll_kohm;
		prvPbsAddSubTlvToTlv(&tlv_sub_imp, &tlv_imp_ll, PBS_ECG_IMPEDANCE_RESULTS_LL_KOHM, 0, PBS_PAYLOAD_SHORT, data);

		prvPbsAddSubTlvToTlv(&message, &tlv_sub_imp, PBS_ECG_MEASUREMENT_EVENT_IND_IMPEDANCE, 0, PBS_PAYLOAD_TLV_ARRAY, data);
	}


	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_SD_ONLY);

	return res;
}




// TODO:
PBS_RESULT_ENUM pbsSendECGConfigSetResp(PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS_ENUM status)
{
	TLV_STRUCT message, tlv_status;
	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_ECG_MEASUREMENT_CONFIG_SET, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.sh = status;
	prvPbsAddSubTlvToTlv(&message, &tlv_status, PBS_ECG_COMMAND_CONFIG_SET_RESP_STATUS, 0, PBS_PAYLOAD_SHORT, data);

	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}


// TODO:
PBS_RESULT_ENUM pbsSendECGConfigGetResp(ECG_RESP_CONFIG_OBJECT_STRUCT * ecg_params)
{
	if (ecg_params == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;


	TLV_STRUCT message, tlv_weight, tlv_output, tlv_notch, tlv_gain, tlv_bpf, tlv_output_filter;
	TLV_STRUCT tlv_loff, tlv_resp_output, tlv_resp_notch, tlv_resp_bpf;
	TLV_STRUCT tlv_resp_lsb, tlv_resp_ads_gain, tlv_resp_algo_gain;
	TLV_STRUCT tlv_meas_en, tlv_comp_sig, tlv_rec_to_mem, tlv_send_events, tlv_prog_flow_cont;
	TLV_STRUCT tlv_peaks, tlv_resp_phase, tlv_resp_clock;
	TLV_STRUCT tlv_pace_maker;

	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_ECG_MEASUREMENT_CONFIG_GET, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.ch = ecg_params->general_config.measurement_enable;
	prvPbsAddSubTlvToTlv(&message, &tlv_meas_en, PBS_ECG_RESPIRATION_CONFIG_OBJECT_MEASUREMENT_ENABLE, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->general_config.compression;
	prvPbsAddSubTlvToTlv(&message, &tlv_comp_sig, PBS_ECG_RESPIRATION_CONFIG_OBJECT_COMPRESSED_SIGNAL, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->general_config.copy_to_memory;
	prvPbsAddSubTlvToTlv(&message, &tlv_rec_to_mem, PBS_ECG_RESPIRATION_CONFIG_OBJECT_RECORD_TO_MEMORY, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->general_config.send_events;
	prvPbsAddSubTlvToTlv(&message, &tlv_send_events, PBS_ECG_RESPIRATION_CONFIG_OBJECT_SEND_EVENTS, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->general_config.progress_ind_flow_control;
	prvPbsAddSubTlvToTlv(&message, &tlv_prog_flow_cont, PBS_ECG_RESPIRATION_CONFIG_OBJECT_PROGRESS_INDICATIONS_FLOW_CONTROL, 1, PBS_PAYLOAD_CHAR, data);


	data.ch = ecg_params->ecg_output.peaks_output;
	prvPbsAddSubTlvToTlv(&message, &tlv_peaks, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_PEAKS_OUTPUT, 1, PBS_PAYLOAD_CHAR, data);
	data.f = ecg_params->ecg_output.lsb_weight;
	prvPbsAddSubTlvToTlv(&message, &tlv_weight, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_LSB_WEIGHT, 1, PBS_PAYLOAD_FLOAT, data);
	data.ch = ecg_params->ecg_output.output_frequency;
	prvPbsAddSubTlvToTlv(&message, &tlv_output, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_OUTPUT_FREQUENCY, 1, PBS_PAYLOAD_CHAR, data);
	data.ch = ecg_params->ecg_output.notch_filter;
	prvPbsAddSubTlvToTlv(&message, &tlv_notch, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_50_60HZ_FILTER, 1, PBS_PAYLOAD_CHAR, data);
	data.ch = ecg_params->ecg_output.output_filter;
	prvPbsAddSubTlvToTlv(&message, &tlv_output_filter, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_OUTPUT_FILTER, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->ads.gain;
	prvPbsAddSubTlvToTlv(&message, &tlv_gain, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ADS_GAIN_ECG, 1, PBS_PAYLOAD_CHAR, data);
	data.ch = ecg_params->ecg_output.bandpass_filter;
	prvPbsAddSubTlvToTlv(&message, &tlv_bpf, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ECG_BANDPASS_FILTER, 1, PBS_PAYLOAD_CHAR, data);
	data.ch = ecg_params->general_config.on_lead_off_event;
	prvPbsAddSubTlvToTlv(&message, &tlv_loff, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ON_LEAD_OFF_EVENT, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->respiration_output.output_frequency;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_output, PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_OUTPUT, 1, PBS_PAYLOAD_CHAR, data);
	data.ch = ecg_params->respiration_output.notch_filter;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_notch, PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_50_60HZ_FILTER, 1, PBS_PAYLOAD_CHAR, data);
	data.ch = ecg_params->respiration_output.bandpass_filter;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_bpf, PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_BANDPASS_FILTER, 1, PBS_PAYLOAD_CHAR, data);
	data.f = ecg_params->respiration_output.lsb_weight;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_lsb, PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_LSB_WEIGHT, 1, PBS_PAYLOAD_FLOAT, data);

	data.ch = ecg_params->ads.respiration_gain;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_ads_gain, PBS_ECG_RESPIRATION_CONFIG_OBJECT_ADS_GAIN_RESPIRATION, 1, PBS_PAYLOAD_CHAR, data);
	data.ch = ecg_params->respiration_output.algo_gain;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_algo_gain, PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_ALGORITHM_GAIN, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->ads.respiration_phase_shift;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_phase, PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_PHASE_SHIFT, 1, PBS_PAYLOAD_CHAR, data);
	data.ch = ecg_params->ads.respiration_clock_freq;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_clock, PBS_ECG_RESPIRATION_CONFIG_OBJECT_RESPIRATION_CLOCK, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->general_config.pace_maker_enable;
	prvPbsAddSubTlvToTlv(&message, &tlv_pace_maker, PBS_ECG_RESPIRATION_CONFIG_OBJECT_PACE_MAKER_ENABLE, 1, PBS_PAYLOAD_CHAR, data);

	data.ch = ecg_params->general_config.impedance_settings;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_clock, PBS_ECG_RESPIRATION_CONFIG_OBJECT_IMPEDANCE_MEASUREMENT_SETTINGS, 1, PBS_PAYLOAD_CHAR, data);
	data.sh = ecg_params->general_config.impedance_period_min;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_clock, PBS_ECG_RESPIRATION_CONFIG_OBJECT_IMPEDANCE_MEASUREMENT_INTERVAL, 1, PBS_PAYLOAD_SHORT, data);



	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}

// TODO:
PBS_RESULT_ENUM pbsSendECGAlgoConfigSetResp(PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_STATUS_ENUM status)
{
	TLV_STRUCT message, tlv_status;
	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_ECG_ALGORITHM_CONFIG_SET, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.sh = status;
	prvPbsAddSubTlvToTlv(&message, &tlv_status, PBS_ECG_RESPIRATION_ALGO_CONFIG_SET_RESP_STATUS, 0, PBS_PAYLOAD_SHORT, data);

	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;
}

// TODO:
PBS_RESULT_ENUM pbsSendECGAlgoConfigGetResp(ECG_RESP_CONFIG_OBJECT_STRUCT * ecg_params)
{
	if (ecg_params == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	TLV_STRUCT message;
	TLV_STRUCT tlv_limit_runs, tlv_limit_bigeminy, tlv_limit_pause, tlv_limit_vtach, tlv_limit_ebrad, tlv_limit_etach;
	TLV_STRUCT tlv_limit_resp_apnea, tlv_resp_min, tlv_resp_max;

	PBS_DATA_U data;

	prvPbsTlvInit(&message, PBS_COMMAND_RESP_ECG_ALGORITHM_CONFIG_GET, PBS_PAYLOAD_TLV_ARRAY, NULL, 0);

	data.sh = (int16_t)ecg_params->ecg_algorithm.limit_runs;
	prvPbsAddSubTlvToTlv(&message, &tlv_limit_runs, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_RUNS_LIMIT, 1, PBS_PAYLOAD_SHORT, data);
	data.sh = (int16_t)ecg_params->ecg_algorithm.limit_bigeminy;
	prvPbsAddSubTlvToTlv(&message, &tlv_limit_bigeminy, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_BIGEMINY_LIMIT, 1, PBS_PAYLOAD_SHORT, data);
	data.sh = (int16_t)ecg_params->ecg_algorithm.limit_pause;
	prvPbsAddSubTlvToTlv(&message, &tlv_limit_pause, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_PAUSE_LIMIT, 1, PBS_PAYLOAD_SHORT, data);
	data.sh = (int16_t)ecg_params->ecg_algorithm.limit_vtach;
	prvPbsAddSubTlvToTlv(&message, &tlv_limit_vtach, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_VTACH_LIMIT_1, 1, PBS_PAYLOAD_SHORT, data);
	data.sh = (int16_t)ecg_params->ecg_algorithm.limit_ebrad;
	prvPbsAddSubTlvToTlv(&message, &tlv_limit_ebrad, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_EBRAD_LIMIT_1, 1, PBS_PAYLOAD_SHORT, data);
	data.sh = (int16_t)ecg_params->ecg_algorithm.limit_etach;
	prvPbsAddSubTlvToTlv(&message, &tlv_limit_etach, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_ETACH_LIMIT_1, 1, PBS_PAYLOAD_SHORT, data);

	data.ch = (int16_t)ecg_params->respiration_algorithm.limit_apnea;
	prvPbsAddSubTlvToTlv(&message, &tlv_limit_resp_apnea, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_APNEA_DETECTION_LIMIT, 1, PBS_PAYLOAD_CHAR, data);
	data.sh = (int16_t)ecg_params->respiration_algorithm.respiration_rate_min;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_min, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_MINIMUM_RESPIRATION_RATE, 1, PBS_PAYLOAD_SHORT, data);
	data.sh = (int16_t)ecg_params->respiration_algorithm.respiration_rate_max;
	prvPbsAddSubTlvToTlv(&message, &tlv_resp_max, PBS_ECG_RESPIRATION_ALGO_CONFIG_OBJECT_MAXIMUM_RESPIRATION_RATE, 1, PBS_PAYLOAD_SHORT, data);

	PBS_RESULT_ENUM res = prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return res;

}

// TODO:
PBS_RESULT_ENUM pbsSendAccelerometerConfigSetResp()
{
	//	TLV_STRUCT message;
	//
	//	RESULT_E res = pbs_send_message(&message);
	//
	//	return res == RESULT_OK ? PBS_RESULT_OK : PBS_RESULT_ERROR;

	return PBS_RESULT_NOT_IMPLEMENTED;
}
// TODO:
PBS_RESULT_ENUM pbsSendAccelerometerConfigGetResp()
{
	//	TLV_STRUCT message;
	//
	//	RESULT_E res = pbs_send_message(&message);
	//
	//	return res == RESULT_OK ? PBS_RESULT_OK : PBS_RESULT_ERROR;

	return PBS_RESULT_NOT_IMPLEMENTED;
}
// TODO:
PBS_RESULT_ENUM pbsSendAccelerometerEventInd ()
{
	//	TLV_STRUCT message;
	//
	//	RESULT_E res = pbs_send_message(&message);
	//
	//	return res == RESULT_OK ? PBS_RESULT_OK : PBS_RESULT_ERROR;

	return PBS_RESULT_NOT_IMPLEMENTED;
}

// TODO:
PBS_RESULT_ENUM pbsSendJumpToBootloaderResp()
{
	//	TLV_STRUCT message;
	//
	//	RESULT_E res = pbs_send_message(&message);
	//
	//	return res == RESULT_OK ? PBS_RESULT_OK : PBS_RESULT_ERROR;

	return PBS_RESULT_NOT_IMPLEMENTED;
}


RESULT_E pbsSendErrorEvent(PBS_GENERAL_ERRORS_EVENTS_ENUM error_event)
{
	TLV_STRUCT message;
	PBS_DATA_U data;

	data.sh = error_event;
	prvPbsTlvInit(&message, PBS_ERROR_EVENT, PBS_PAYLOAD_SHORT, &data, 0);

	prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return RESULT_OK;
}



RESULT_E prvPbsSendUnknownCommandResponse(void)
{
	TLV_STRUCT 		message;

	prvPbsTlvInit(&message, PBS_UNKNOWN_COMMAND, PBS_PAYLOAD_NONE, 0, 0);
	prvPbsSendMessage(&message, PBS_OUTPUT_TYPE_BLE_ONLY);

	return RESULT_OK;
}

//--------------------------------------- PBS INTERFACE END --------------------------------------------------
//REVIEW end




// ---------------------------------------- INFRASTRUCTURE  START ---------------------------------


RESULT_E pbsSendMsgToQueue(uint8_t* data)
{
   if ( (pbs_output_mode == PBS_OUTPUT_TYPE_SD_BLE) || (pbs_output_mode == PBS_OUTPUT_TYPE_BLE_ONLY) )
   {
      //	static uint8_t data_out[200] = {0};//TODO: find other solution

      static PBS_QUEUE_MSG_STRUCT msg;

      uint16_t total_length = strlen((char*)data);

      if ((total_length % 2) != 0)
      {
         LOG("Failed to send message to PBS parser, Wrong length\r\n");
         return RESULT_ERROR;
      }


      uint8_t * data_ptr = data;
      while (data_ptr < data + total_length)
      {
         uint8_t next_length = min(strlen((char*)data_ptr), 2 * sizeof(msg.pbs_data));

         for(uint8_t i = 0; i < (next_length / 2);i++)
         {
            msg.pbs_data[i] = (data_ptr[i*2] <= '9' ? data_ptr[i*2] - '0' : data_ptr[i*2] - 'a' + 10) << 4;
            msg.pbs_data[i] |= data_ptr[i*2 + 1] <= '9' ? data_ptr[i*2 + 1] - '0' : data_ptr[i*2 + 1] - 'a' + 10;
         }

         msg.msg_length = next_length/2;

         if (osMessagePut (pbs_rx_queue_handle, (uint32_t)&msg, 0) != osOK)
         {
            LOG("Failed to send message to PBS parser\r\n");
         }

         data_ptr += next_length;
      }
   }
	return RESULT_OK;
}

//static PBS_RESULT_ENUM prvPbsGetTlvFlowControl(TLV_STRUCT * tlv, uint8_t * fc)
//{
//	if ( (tlv == NULL) || (fc == NULL) )
//		return PBS_RESULT_ERROR_WRONG_PARAM;


static PBS_RESULT_ENUM prvPbsAddCrcToBuffer(IN uint8_t * buff, IN uint16_t * length)
{
	uint16_t crc;
	// add CRC at the end
	PBS_RESULT_ENUM res = prvPbsCrc16Calc(buff, *length, &crc);
	if (res != PBS_RESULT_OK)
	{
		return PBS_RESULT_ERROR_CRC;
	}

	memcpy(&buff[*length], &crc, sizeof(uint16_t));
	*length += sizeof(uint16_t);

	return PBS_RESULT_OK;
}

static PBS_RESULT_ENUM prvPbsSendMessage(TLV_STRUCT * tlv, PBS_OUTPUT_TYPE_E mode)
{
	osStatus 				resMutex;
//	uint16_t 				crc;
	BSP_TIME_STRUCT 		time;
	static uint8_t 			hour = 99;
	static uint8_t 			day = 99;
	BSP_DATE_STRUCT 		date;


	if (tlv == NULL)
        {
		return PBS_RESULT_ERROR_WRONG_PARAM;
        }

	if (tlv->type == PBS_MSG_UNINITIALIZED)
        {
		return PBS_RESULT_ERROR_WRONG_PARAM;
        }

	resMutex = osMutexWait(pbs_tx_mutex, PBS_TX_TIMEOUT);

	if (resMutex != osOK)
        {
		return PBS_RESULT_ERROR_OS;
        }

	pbs_buffer[pbs_buffer_index].header = PBS_HEADER;

	tlv_send_index = PBS_HEADER_LENGTH;

	// build tx buffer from linked list (byte buffer are only pointing data)
	PBS_RESULT_ENUM res = pbsFillBufferWithTlv(tlv, 0);

	if (res != PBS_RESULT_OK)
	{
		osMutexRelease(pbs_tx_mutex);
		return PBS_RESULT_ERROR;
	}

	// add CRC at the end
	res = prvPbsAddCrcToBuffer(pbs_buffer[pbs_buffer_index].txBuffer, &tlv_send_index);
	if (res != PBS_RESULT_OK)
	{
		osMutexRelease(pbs_tx_mutex);
		return PBS_RESULT_ERROR_CRC;
	}

        //                              send to sd card
        //-------------------------------------------------------------------------------------------------
        //if ( (pbs_flow_control.enable == SW_FLOW_CONTROL_ON) &&
        //	((tlv->type == PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_IND) || (tlv->type == PBS_ECG_MEASUREMENT_EVENT_IND)))
	if ( (mode == PBS_OUTPUT_TYPE_SD_ONLY) || (mode == PBS_OUTPUT_TYPE_SD_BLE))
	{
		bspRtcTimeGet(&time, &date);
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_1, "pbs time msec = %d\r\n", time.msec);
		if ((hour == 99) || (time.hours - hour >= pbs_file_duration) || (day != date.date))
		{
			hour = time.hours;
			day = date.date;
			sprintf((char*)pbs_current_ecg_file, PBS_SD_ECG_FILE_FORMAT, ++pbs_current_ecg_file_num);
		}


                //#ifndef SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
                #ifndef VSMS_EXTENDED_HOLTER_MOCKUP

                //send sd card:
		sdSendMsgToQueue(pbs_current_ecg_file, pbs_buffer[pbs_buffer_index].txBuffer, tlv_send_index);

                #endif //VSMS_EXTENDED_HOLTER_MOCKUP
                //#endif	//SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
                //		prvPbsSendControlledMessage(pbs_buffer[pbs_buffer_index].txBuffer, tlv_send_index, flow_control);
	}

        //                              send to ble
        //-------------------------------------------------------------------------------------------------
	if ((mode == PBS_OUTPUT_TYPE_BLE_ONLY) || (mode == PBS_OUTPUT_TYPE_SD_BLE) || (pbs_holter_remote_event == PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENABLE))
	{
                // res = prvPbsFlushTxBuffer(pbs_buffer[pbs_buffer_index].txBuffer, tlv_send_index);
		if (pbs_holter_remote_event == PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENABLE)
		{
			if (pbs_holter_remote_event_packets <= 0)
                        {
				pbs_holter_remote_event = PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_DISABLE;
                        }
			else
                        {
				pbs_holter_remote_event_packets--;
                        }
                        #ifdef VSMS_EXTENDED_HOLTER_MOCKUP
			// prvPbsReadLastMinutes();
                        #else
                        //send  ble message:

			res = prvPbsFlushTxBuffer(pbs_buffer[pbs_buffer_index].txBuffer, tlv_send_index);
                        #endif //VSMS_EXTENDED_HOLTER_MOCKUP
		}
		else
                {

			res = prvPbsFlushTxBuffer(pbs_buffer[pbs_buffer_index].txBuffer, tlv_send_index);
		}
	}

	tlv_send_index = 0;

	resMutex = osMutexRelease(pbs_tx_mutex);

	if (res != PBS_RESULT_OK)
		return res;

	return PBS_RESULT_OK;
}

/*
 * Recursive TLV send function
 * for subTLV (true): continue copying data if it fits into buffer, otherwise, flush and continue copying
 * recur_depth should be called with 0 when MESSAGE is sent
 */
static PBS_RESULT_ENUM pbsFillBufferWithTlv(TLV_STRUCT * tlv, uint8_t recur_depth)
{

	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_4, "PBS pbs_send_tlv - payload_type %d length %d recur_depth %d\r\n", tlv->data.payload_type, tlv->length, recur_depth);

	if (recur_depth > 3)	// Max depth
		return PBS_RESULT_ERROR_NESTED_TLV_TOO_DEEP;

	memcpy(&pbs_buffer[pbs_buffer_index].txBuffer[tlv_send_index], &tlv->type, PBS_TLV_TYPE_SIZE);
	memcpy(&pbs_buffer[pbs_buffer_index].txBuffer[tlv_send_index+PBS_TLV_TYPE_SIZE], &tlv->length, PBS_TLV_LENGTH_SIZE);
	tlv_send_index += PBS_TLV_SUB_HEADER_SIZE;


	switch (tlv->data.payload_type)
	{
	case PBS_PAYLOAD_INTEGER:
		memcpy(&pbs_buffer[pbs_buffer_index].txBuffer[tlv_send_index], &tlv->data.data.i, tlv->length);
		tlv_send_index += tlv->length;
		break;
	case PBS_PAYLOAD_FLOAT:
		memcpy(&pbs_buffer[pbs_buffer_index].txBuffer[tlv_send_index], &tlv->data.data.f, tlv->length);
		tlv_send_index += tlv->length;
		break;
	case PBS_PAYLOAD_CHAR:
		memcpy(&pbs_buffer[pbs_buffer_index].txBuffer[tlv_send_index], &tlv->data.data.ch, tlv->length);
		tlv_send_index += tlv->length;
		break;
	case PBS_PAYLOAD_SHORT:
		memcpy(&pbs_buffer[pbs_buffer_index].txBuffer[tlv_send_index], &tlv->data.data.sh, tlv->length);
		tlv_send_index += tlv->length;
		break;
	case PBS_PAYLOAD_NONE:
		break;

	case PBS_PAYLOAD_BYTE_ARRAY:
		if ( (tlv_send_index + tlv->length) < PBS_TX_MAX_PACKET_SIZE)	// tlv header was copied already
		{
			memcpy(&pbs_buffer[pbs_buffer_index].txBuffer[tlv_send_index], tlv->data.data.byte_array, tlv->length);
			tlv_send_index += tlv->length;
		}
		else
		{
			LOG("\r\nPBS ERROR BUILDING BUFFER, MESSAGE TOO LONG!!\r\n");
			return PBS_RESULT_ERROR_MESSAGE_TOO_LONG_BYTE_ARRAY;

		}
		break;
	case PBS_PAYLOAD_TLV_ARRAY:
		{
			if (tlv->data.data.tlv_list == NULL) {
				break;
			}
			TLV_STRUCT *curr_tlv = tlv->data.data.tlv_list;
			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_4, "PBS pbs_send_message - curr_tlv %x , next= %x\r\n", curr_tlv, curr_tlv->next);
			PBS_RESULT_ENUM res;
			while (curr_tlv != NULL)
			{
				res = pbsFillBufferWithTlv(curr_tlv, recur_depth+1);
				if (res != PBS_RESULT_OK)
				{
					LOG("PBS send nested error\r\n");
					return res;	//PBS_RESULT_ERROR_NESTED_TLV_ERROR;
				}
				curr_tlv = curr_tlv->next;
			}
		}
		break;
	default:
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "PBS UN-HANDLED \r\n");
		return PBS_RESULT_ERROR_UNKNOWN_TYPE;
		break;
	}


	return PBS_RESULT_OK;
}


// crc

static PBS_RESULT_ENUM prvPbsCrc16Calc(IN const uint8_t *data, IN uint16_t size, OUT uint16_t * crc)
{
    uint16_t out = 0;
    int32_t bits_read = 0, bit_flag;

    /* Sanity check: */
    if( (data == NULL) || (size == 0) || (crc == NULL))
        return PBS_RESULT_ERROR_WRONG_PARAM;

    while(size > 0)
    {
        bit_flag = out >> 15;

        /* Get next bit: */
        out <<= 1;
        out |= (*data >> bits_read) & 1; // item a) work from the least significant bits

        /* Increment bit counter: */
        bits_read++;
        if(bits_read > 7)
        {
            bits_read = 0;
            data++;
            size--;
        }

        /* Cycle check: */
        if(bit_flag)
            out ^= PBS_CRC16;

    }

    // item b) "push out" the last 16 bits
    int32_t i;
    for (i = 0; i < 16; ++i) {
        bit_flag = out >> 15;
        out <<= 1;
        if(bit_flag)
            out ^= PBS_CRC16;
    }

    // item c) reverse the bits
//    uint16_t crc = 0;
    *crc = 0;
    i = 0x8000;
    int j = 0x0001;
    for (; i != 0; i >>=1, j <<= 1)
    {
        if (i & out)
        {
        	*crc |= j;
        }
    }

    return PBS_RESULT_OK;
}


static PBS_RESULT_ENUM prvPbsGetLastFileName(void)
{
	uint8_t 	i;
	uint8_t		file[15] = {0};//TODO:Constant
	uint32_t 	file_size = 0;
	SD_RESULT_E	sd_res = SD_RESULT_OK;

	if (sdDirExist(PBS_SD_ECG_FILES_DIRECTORY) != SD_RESULT_OK)
	{
		if (sdMakeDir(PBS_SD_ECG_FILES_DIRECTORY)!=  SD_RESULT_OK)
		{
			LOG("Couldn't create ECG directory\r\n");
			pbsSetDeviceErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD);
			return PBS_RESULT_ERROR_SD;
		}
	}

	for(i = 1; i <= PBS_MAX_NUMBER_OF_FILES; i++)
	{
		sprintf((char*)file, PBS_SD_ECG_FILE_FORMAT, i);
		if (sdFileExist(file) == SD_RESULT_ERROR_FILE_DOES_NOT_EXIST)
		{
			pbs_current_ecg_file_num = i - 1;
			sprintf((char*)file, PBS_SD_ECG_FILE_FORMAT, pbs_current_ecg_file_num);
			sd_res = sdGetFileSize(file, &file_size);
			if ((sd_res == SD_RESULT_OK) && (file_size < PBS_SD_FILE_SIZE_MIN))
			{//if last file is smaller than 2048 Bytes delete it
				sdDeleteFile(file);
				strcpy((char*)pbs_current_ecg_file, (char*)file);
				pbs_current_ecg_file_num--;
				break;
			}
			sprintf((char*)file, PBS_SD_ECG_FILE_FORMAT, i);
			strcpy((char*)pbs_current_ecg_file, (char*)file);
			break;
		}
	}

	if (i > PBS_MAX_NUMBER_OF_FILES)
		return PBS_RESULT_ERROR_TOO_MANY_FILES;

	LOG("%d ECG files\r\n", (i - 1));

	return PBS_RESULT_OK;
}

static PBS_RESULT_ENUM prvPbsGetValue(IN uint8_t* msg,
									  IN uint8_t type,
									  OUT uint16_t* length,
									  OUT uint8_t* value)
{
	PBS_TLV_LIST_STRUCT tlvArray[20];

	prvPbsBufferToTlvList(GET_PBS_LEN_FROM_HEADER_LOC(msg), msg, &tlvArray[0], 10);

	PBS_TLV_LIST_STRUCT *curr_tlv = &tlvArray[0];

	while(curr_tlv != NULL)
	{

		if (type == curr_tlv->subtype)
		{
			*length = curr_tlv->length;
			memcpy(value, curr_tlv->value, *length);
			return PBS_RESULT_OK;
		}

		curr_tlv = curr_tlv->next;
	}

	return PBS_RESULT_ERROR_TYPE_NOT_FOUND;
}

static PBS_RESULT_ENUM prvPbsSetValidRtc(void)
{
	BSP_TIME_DATE_STRUCT	rtc;
	uint16_t				file_read_actual_length;
	uint32_t				sd_file_offset_curr = 0;
	uint8_t					previous_ecg_file[15] = {0};//TODO:Constant
	uint8_t	*				header_loc;
	uint16_t				crc_calc = 0, data_read_len = 0;
	BSP_RTC_TIMESTAMP 		time_stamp;

	if (bspRtcTimeGet(&rtc.time, &rtc.date) != BSP_RESULT_OK)
		return PBS_RESULT_ERROR_RTC_READ_ERROR;

	if (rtc.date.year == 0)
	{//If year equals to 0 it means the RTC is not valid
		if (pbs_current_ecg_file_num != 0)
		{//If previous file exists
			sprintf((char*)previous_ecg_file, PBS_SD_ECG_FILE_FORMAT, pbs_current_ecg_file_num);
			sdGetFileSize(previous_ecg_file, &sd_file_offset_curr);
			if (sdReadDataFromFile(previous_ecg_file, sizeof(file_payload), (sd_file_offset_curr - sizeof(file_payload)), file_payload, &file_read_actual_length) != SD_RESULT_OK)
				return PBS_RESULT_ERROR_FILE_READ_ERROR;
			do{//Check for latest valid PBS header to copy RTC from
				if (prvPbsSearchHeaderInBufferMessageFromEnd(file_payload, sizeof(file_payload) - data_read_len, &header_loc) != PBS_RESULT_OK)
					return PBS_RESULT_ERROR_HEADER_NOT_FOUND;//No valid header in buffer

				if (prvPbsCrc16Calc(header_loc, GET_PBS_LEN_FROM_HEADER_LOC(header_loc) + PBS_TX_OVERHEAD, &crc_calc) != PBS_RESULT_OK)
					return PBS_RESULT_ERROR_CRC;

				if (memcmp((uint8_t*)&crc_calc, (header_loc + GET_PBS_LEN_FROM_HEADER_LOC(header_loc) + PBS_TX_OVERHEAD), sizeof(crc_calc)) == 0)
					break; //Found valid header, exit while

				data_read_len = sizeof(file_payload) - (uint16_t)(header_loc - file_payload) + 1;
			}while(data_read_len > 0);

			//Get RTC from message and set GMP RTC
			if (prvPbsGetValue(header_loc + PBS_TX_OVERHEAD, PBS_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_TIMESTAMP, &data_read_len, (uint8_t*)&time_stamp) == PBS_RESULT_OK)
			{//If time stamp was found in message write it to RTC
				time_stamp.field.seconds += PBS_UNKNOWN_TIME_DIFF;
				if (bspRtcTimeSetBCD(&time_stamp) != BSP_RESULT_OK)
					return PBS_RESULT_ERROR_TIMESTAMP_EMPTY;
			}
		}
	}


	return PBS_RESULT_OK;
}


static void pbsTask(void const * argument)
{
	static uint8_t rx_buff[PBS_RX_MAX_PACKET_SIZE] = { 0 };
	uint8_t rx_buff_index = 0;
	osEvent ret;
	uint16_t i = 0;
	uint16_t type;
	uint16_t length;
	PBS_RX_STATE_E pbs_rx_state = PBS_RX_STATE_WAITING_FOR_MSG;
	PBS_QUEUE_MSG_STRUCT* pbs_msg;
	uint16_t current_msg_length = 0;
	PBS_RESULT_ENUM res;

	if (prvPbsGetLastFileName() != PBS_RESULT_OK)
		pbsSetDeviceErrorState(PBS_DEVICE_STATUS_BIT_ERROR_SD_NOT_ENOUGH_FREE_SPACE);

	prvPbsSetValidRtc();

	for (;;)
	{
		if ((pbs_output_mode == PBS_OUTPUT_TYPE_OFF) || (pbs_output_mode == PBS_OUTPUT_TYPE_SD_ONLY)) // TODO
		{
			osDelay(2000);
		}
		else
		{
			ret = osMessageGet(pbs_rx_queue_handle, PBS_RX_MSG_TIMEOUT);
			// Wait 1 sec timeout while keepalives should be received every timeout/2
			if (ret.status == osEventMessage)
			{
				pbs_msg = (PBS_QUEUE_MSG_STRUCT*) ret.value.v;
				if (pbs_msg->msg_length > 20)
				{
					LOG("pbs_msg.msg_length > 20\r\n");
				}
				//			PRINT("PBS RX:" );
				//			DEBUG_PRINT(DEBUG_LEVEL_10, (char*)pbs_msg->pbs_data, pbs_msg->msg_length);
				//			DEBUG_PRINT(DEBUG_LEVEL_8, "\r\n");

				if (pbs_rx_state == PBS_RX_STATE_INCOMPLETE_MSG)
				{
					memcpy(&rx_buff[rx_buff_index], pbs_msg->pbs_data, pbs_msg->msg_length);
					rx_buff_index += pbs_msg->msg_length;
					if ((current_msg_length + PBS_CRC16_LENGTH) == rx_buff_index)
					{   //Message is complete, send to parser
						//					PRINT("\r\nPBS RX:" );
						//					DEBUG_PRINT(DEBUG_LEVEL_10, (char*)rx_buff, current_msg_length);
						//					DEBUG_PRINT(DEBUG_LEVEL_8, "\r\n");

						// reset for next time
						pbs_rx_state = PBS_RX_STATE_WAITING_FOR_MSG;

						LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "PBS RX length: %d\r\n", rx_buff_index);
						res = prvPbsPreparsingCrcCheck(rx_buff, length + PBS_RX_HEADER_TO_VALUE_OFFSET);
						if (res != PBS_RESULT_OK)
						{
							// handle error, drop packet
						}
						else
						{
							prvPbsRxMessageParser(type, length, &rx_buff[PBS_RX_HEADER_TO_VALUE_OFFSET]);
						}
					}
				}
				else if (pbs_rx_state == PBS_RX_STATE_WAITING_FOR_MSG)
				{
					for (i = 0; i < pbs_msg->msg_length; i++)	//PBS_RX_MAX_PACKET_SIZE
					{
						if (!IS_PBS_HEADER(&pbs_msg->pbs_data[i]))
							continue;

						// Found Header
						memcpy(&type, (pbs_msg->pbs_data + i + PBS_RX_HEADER_TO_TYPE_OFFSET), 2);
						memcpy(&length, (pbs_msg->pbs_data + i + PBS_RX_HEADER_TO_LEN_OFFSET), 2);

						if ((length + PBS_TX_OVERHEAD) > (pbs_msg->msg_length))
						{//If message total length is larger than packet length add it to message buffer and set state to incomplete message
							pbs_rx_state = PBS_RX_STATE_INCOMPLETE_MSG;
							memcpy(rx_buff, pbs_msg->pbs_data, pbs_msg->msg_length);
							rx_buff_index = pbs_msg->msg_length;
							current_msg_length = length + PBS_TX_OVERHEAD;
							break;
						}
						else
						{	//Regular message under max limit
							res = prvPbsPreparsingCrcCheck(pbs_msg->pbs_data, length + PBS_RX_HEADER_TO_VALUE_OFFSET);
							if (res != PBS_RESULT_OK)
							{
								// handle error, drop packet
							}
							else
							{
								prvPbsRxMessageParser(type, length, (pbs_msg->pbs_data + i + PBS_RX_HEADER_TO_VALUE_OFFSET));
							}
							i = i + length + PBS_RX_HEADER_TO_VALUE_OFFSET - 1;	// -1 compensation of for-loop declaration

						}
					}

				}
			}
			else if (ret.status == osEventTimeout)
			{
				// Handle Timeout: drop non-complete packet ?
				pbs_rx_state = PBS_RX_STATE_WAITING_FOR_MSG;
				memset(rx_buff, 0, sizeof(rx_buff));
			}
		}
	}
}

static PBS_RESULT_ENUM prvPbsPreparsingCrcCheck(uint8_t * msg, uint16_t total_length)
{
	uint16_t crc_in, crc_calc;

	memcpy(&crc_in, msg + total_length, sizeof(crc_in));

	if (prvPbsCrc16Calc(msg, total_length, &crc_calc) == PBS_RESULT_OK)
	{
		if (crc_calc != crc_in)
		{
			LOG("PBS RX CRC MISMATCH\r\n");
			return PBS_RESULT_ERROR_CRC;
		}
	}
	else
	{
		LOG("PBS RX CRC EXTRACTION ERROR\r\n");
		return PBS_RESULT_ERROR_CRC;
	}
	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "PBS RX CRC OK\r\n");

	return PBS_RESULT_OK;
}

static RESULT_E prvPbsRxMessageParser(uint16_t type, uint16_t length, uint8_t *value)
{



	static PBS_TLV_LIST_STRUCT tlvArray[PBS_TLV_ARRAY_MAX_LENGTH];

	RX_PAYLOAD_TYPE payload_type = NONE;

	PBS_RX_PAYLOAD_STRUCT payload = { NULL };

	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "PBS -> type %x, length %d\r\n", type, length);
	switch (type)
	{
	// --------------------- COMMAND_GENERAL ----------------------------------
	case PBS_COMMAND_GENERAL_GET_INFO:
	case PBS_COMMAND_GENERAL_GET_STATUS:
	case PBS_COMMAND_GENERAL_CONTROL:
	case PBS_COMMAND_DEVICE_CONFIGURATION_SET:
	case PBS_COMMAND_DEVICE_CONFIGURATION_GET:
	// ------------------- COMMAND_ECG ----------------------------------------
	case PBS_COMMAND_ECG_MEASUREMENT_REALTIME_ENABLE:
	case PBS_COMMAND_ECG_MEASUREMENT_REALTIME_PROGRESS_INDICATION_RESP:
	case PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT:
	case PBS_COMMAND_ECG_MEASUREMENT_EVENT_IND_RESP:
	case PBS_COMMAND_ECG_MEASUREMENT_CONFIG_SET:
	case PBS_COMMAND_ECG_MEASUREMENT_CONFIG_GET:
	case PBS_COMMAND_ECG_ALGORITHM_CONFIG_SET:
	case PBS_COMMAND_ECG_ALGORITHM_CONFIG_GET:
	case PBS_COMMAND_ECG_MEASUREMENT_INFO_GET:
	case PBS_COMMAND_ECG_STATISTICS_IND_RESP:

	// ------------------- COMMAND_ACCELEROMETER ------------------------------
	case PBS_COMMAND_ACCELEROMETER_CONFIG_SET:
	case PBS_COMMAND_ACCELEROMETER_CONFIG_GET:
	case PBS_FW_UPGRADE_COMMAND_JUMP_TO_BOOTLOADER:
		payload_type = TLV_LIST;
		break;

	default:
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "pbs_rx_message_parser --> UNHANDLED \r\n");
		prvPbsSendUnknownCommandResponse();
		break;
	}


	if (payload_type == TLV_LIST)
	{
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "pbs_rx_message_parser() payload_type-> TLV_LIST\r\n");
		prvPbsBufferToTlvList(length, value, &tlvArray[0], PBS_TLV_ARRAY_MAX_LENGTH);
		payload.tlv_list = &tlvArray[0];
	}
	else if (payload_type == NONE)
	{
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "pbs_rx_message_parser() payload_type-> NONE\r\n");
		payload.data = NULL;
		payload.tlv_list = NULL;	// ^same action
	}
	else
	{
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "pbs_rx_message_parser() payload_type-> DATA_UNION\r\n");
	}

	if (pbsRxMessageHandler(type , payload) != RESULT_OK)
	{
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "PBS: call of pbs_rx_message_handler()!= RESULT_OK\r\n");
	}

	return RESULT_OK;
}

static RESULT_E prvPbsBufferToTlvList(uint16_t length, uint8_t *inBuff, PBS_TLV_LIST_STRUCT *tlv, uint8_t tlv_list_length)
{
	if ( (tlv_list_length == 0) || (tlv == NULL) || (inBuff == NULL) )
		return RESULT_WRONG_PARAMTER;

	uint8_t *ptrData = inBuff;
	uint8_t i = 0;
	uint16_t debug_counter = 0;

	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_4, "pbsGetTlvArray(), length= %d \r\n", length);

	while ((ptrData < (inBuff + length)) && (i < tlv_list_length))
	{
		tlv[i].next = NULL;
		memcpy(&tlv[i].subtype, ptrData, PBS_TLV_TYPE_SIZE);
		memcpy(&tlv[i].length, ptrData+PBS_TLV_TYPE_SIZE, PBS_TLV_LENGTH_SIZE);
		ptrData += PBS_TLV_VALUE_OFFSET;
		tlv[i].value = ptrData;
		ptrData += tlv[i].length;
		debug_counter = debug_counter + 3 + tlv[i].length;
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_3, "pbsGetTlvArray(), i= %d , tlv[i].length= %d \r\n", i, tlv[i].length);
		i++;
	}
	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_4, "pbsGetTlvArray(), debug_counter= %d \r\n", debug_counter);

	for (uint8_t j = 0; j < (i - 1); j++)
		tlv[j].next = &tlv[j + 1];
	return RESULT_OK;
}

static PBS_RESULT_ENUM prvPbsSearchHeaderInBufferMessage(IN uint8_t *buff, IN uint16_t length, uint8_t ** header_loc)
{
	if ( (buff == NULL) || (header_loc == NULL) )
		return PBS_RESULT_ERROR_WRONG_PARAM;

	uint8_t *ptrData = buff;
	while (ptrData < (buff + length))
	{
		if (IS_PBS_HEADER(ptrData))
		{
			break;
		}
		ptrData++;
	}
	if ( !(IS_PBS_HEADER(ptrData)) || (ptrData >= (buff + length)) )
		return PBS_RESULT_ERROR_HEADER_NOT_FOUND;

	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsFindTypeInBufferMessage Header found at offset %d\r\n", (ptrData-buff) );
	*header_loc = ptrData;

	return PBS_RESULT_OK;
}

static PBS_RESULT_ENUM prvPbsSearchHeaderInBufferMessageFromEnd(IN uint8_t *buff, IN uint16_t length, uint8_t ** header_loc)
{
	if ( (buff == NULL) || (header_loc == NULL) )
		return PBS_RESULT_ERROR_WRONG_PARAM;

	uint8_t *ptrData = buff + length;
	while (ptrData >= buff)
	{
		if (IS_PBS_HEADER(ptrData))
		{
			break;
		}
		ptrData--;
	}
	if ( !(IS_PBS_HEADER(ptrData)) || (ptrData < buff) )
		return PBS_RESULT_ERROR_HEADER_NOT_FOUND;

	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsFindTypeInBufferMessage Header found at offset %d\r\n", (ptrData-buff) );
	*header_loc = ptrData;

	return PBS_RESULT_OK;
}

// TODO: Fix - first find header, then jump according to type length
static PBS_RESULT_ENUM prvPbsFindTypeInBufferMessage(IN uint8_t *buff, IN uint16_t length, IN uint16_t main_type, IN uint16_t type_to_find, OUT uint8_t ** buff_index, OUT uint16_t *main_length)
{
	if ( (buff == NULL) || (buff_index == NULL) )
		return PBS_RESULT_ERROR_WRONG_PARAM;

	uint8_t *ptrData = buff;
	uint16_t inner_type;
	uint16_t inner_length;
//	while (ptrData < (buff + length))
//	{
//		if (IS_PBS_HEADER(ptrData))
//		{
//			break;
//		}
//		ptrData++;
//	}
	if ( !(IS_PBS_HEADER(ptrData)) || (ptrData >= (buff + length)) )
		return PBS_RESULT_ERROR_HEADER_NOT_FOUND;
//	else
//	{
//		DEBUG_PRINT(DEBUG_LEVEL_5, "\r\nprvPbsFindTypeInBufferMessage Header found at offset %d", (ptrData-buff) );
//	}

	ptrData += PBS_HEADER_LENGTH;
	if ( (*(uint16_t*)ptrData != main_type) || (ptrData >= (buff + length)) )
		return PBS_RESULT_ERROR_TYPE_NOT_FOUND;

	ptrData += sizeof(main_type);
	if (main_length != NULL)
		memcpy(main_length, ptrData, sizeof(uint16_t));

	ptrData += sizeof(uint16_t);
	while ( (ptrData < (buff + length) ) && (ptrData < (buff + *main_length)) )
	{
		if (*(uint16_t*)ptrData == type_to_find)
		{
			*buff_index = ptrData;
			return PBS_RESULT_OK;
		}
		memcpy(&inner_type, ptrData, sizeof(inner_type));
		ptrData += sizeof(uint16_t);
		memcpy(&inner_length, ptrData, sizeof(inner_length));
		ptrData += sizeof(uint16_t);
		ptrData = ptrData + inner_length;
	}

	return PBS_RESULT_ERROR_TYPE_NOT_FOUND;
}

// ---------------------------------------- INFRASTRUCTURE  END ---------------------------------


// ---------------------------------------- MEMORY POOL START ---------------------------------

#if 0

#define PBS_TX_POOL_SIZE_BYTES 800

osPoolDef (pbs_tx_queue_pool, PBS_TX_POOL_SIZE_BYTES, uint8_t);  // Declare memory pool
osPoolId  (pbs_tx_queue_pool_id);                 // Memory pool ID

static PBS_RESULT_ENUM prvPbsMemoryPoolInit(uint16_t length)
{
	pbs_tx_queue_pool_id = osPoolCreate(osPool(pbs_tx_queue_pool));

	return PBS_RESULT_OK;
}



static PBS_RESULT_ENUM prvPbsTxDequeue()
{
	osEvent ret = osMessageGet(pbs_tx_queue_handle, 0);
	if (ret.status != osEventMessage)
		return PBS_RESULT_ERROR_OS;

	PBS_TX_MESSAGE_STRUCT * msg = ret.value.p;

	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsTxDequeue fc %d, length %d\r\n", msg->flow_control, msg->msg_len);
	vPortFree(msg);

	return PBS_RESULT_OK;
}

PBS_RESULT_ENUM pbsTxEmptyQueue()
{
	PBS_RESULT_ENUM res = PBS_RESULT_OK;

	while (res == PBS_RESULT_OK)
	{
		res = prvPbsTxDequeue();
	}
	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsTxEmptyQueue end\r\n");
	return PBS_RESULT_OK;
}



// to be sent for ACK controlled messages
static PBS_RESULT_ENUM prvPbsSendControlledMessage(uint8_t * msg, uint16_t length, uint8_t flow_control)
{
   if ( (pbs_output_mode == PBS_OUTPUT_TYPE_OFF) || (pbs_output_mode == PBS_OUTPUT_TYPE_SD_ONLY) )
      return PBS_RESULT_OK;

   PBS_TX_MESSAGE_STRUCT 	* msg_q;
	if ( (msg == NULL) || (length == 0) )
		return PBS_RESULT_ERROR_WRONG_PARAM;

//	uint8_t * mem_ptr;

	PBS_RESULT_ENUM res;

	uint16_t free_mem = xPortGetFreeHeapSize();
   uint16_t tot_len = length + sizeof(PBS_TX_MESSAGE_STRUCT);

	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "\r\nprvPbsSendControlledMessage mem: req %d, available %d, time = %d, fc %d", tot_len, free_mem, osKernelSysTick(), flow_control);
	if (free_mem < tot_len )
	{
		DEBUG_PRINT(DEBUG_LEVEL_5, ", try free first");
		// free space first: drop first packet
		res = prvPbsTxDequeue();
		if (res != PBS_RESULT_OK)
			return res;
      free_mem = xPortGetFreeHeapSize();
      LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, ", new available %d", free_mem);
	}

   uint32_t n = osMessageWaiting(pbs_tx_queue_handle);
   if (n >= PBS_TX_QUEUE_SIZE)
   {
	   LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, ", queue full - drop first message");
		// free space first: drop first packet
		res = prvPbsTxDequeue();

		if (res != PBS_RESULT_OK)
			return res;

   }


	msg_q = pvPortMalloc(tot_len);
	if (msg_q == NULL)
	{
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, ", allocation failed!\r\n");
		return PBS_RESULT_ERROR_OS;
	}


	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, ", allocation succeed\r\n");
	memcpy(msg_q->msg, msg, length);

	msg_q->flow_control = flow_control;
	msg_q->msg_len = length;
//	msg_q->msg = mem_ptr;
	msg_q->tx_count = 0;

	if (osMessagePut(pbs_tx_queue_handle, (uint32_t)msg_q, 0) != osOK)
	{
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2,"\r\nPBS failed to enqueue controlled message fc=%d\r\n", flow_control);
	}

	return PBS_RESULT_OK;
}



PBS_RESULT_ENUM pbsMessageAckRecieved(uint8_t flow_control_index)
{
   if ( (pbs_output_mode == PBS_OUTPUT_TYPE_OFF) || (pbs_output_mode == PBS_OUTPUT_TYPE_SD_ONLY) )
      return PBS_RESULT_OK;

   PBS_RESULT_ENUM pbs_res;
	osEvent ret = osMessagePeek(pbs_tx_queue_handle, PBS_TX_MSG_TIMEOUT);	// TODO: timeout?
	if (ret.status == osEventMessage)
	{
		PBS_TX_MESSAGE_STRUCT * msg = ret.value.p;

		if (msg->flow_control == flow_control_index)
		{
			pbs_res = prvPbsTxDequeue();
			if (pbs_res != PBS_RESULT_OK)
			{
				LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2,"PBS failed to dequeue controlled message, fc %d\r\n", flow_control_index);
				return pbs_res;
			}
			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsMessageAckRecieved fc %d, length %d\r\n", msg->flow_control, msg->msg_len);
			return PBS_RESULT_OK;
		}
		else
		{
			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2,"PBS failed to dequeue controlled message, fc %d not found \r\n", flow_control_index);
			return PBS_RESULT_ERROR_TX_FLOW_CONTROL_NOT_FOUND;
		}
	}
	return PBS_RESULT_ERROR_TX_QUEUE_ERROR;
}

static PBS_RESULT_ENUM prvPbsSendTxMessage(PBS_TX_MESSAGE_STRUCT * msg)
{
	if (msg == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	osStatus resMutex = osMutexWait(pbs_tx_mutex, PBS_TX_MSG_TIMEOUT);
	if (resMutex != osOK)
	{
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2,"pbsTxTask failed to lock pbs_tx_mutex\r\n");
		return PBS_RESULT_ERROR_OS;
	}
	PBS_RESULT_ENUM res;
	uint8_t *local_ptr = msg->msg;
	uint16_t local_length;
	while (local_ptr < (msg->msg+msg->msg_len) )
	{
		local_length = min((msg->msg+msg->msg_len) - local_ptr, 20);
		res = prvPbsFlushTxBuffer(local_ptr, local_length);
		if (res != PBS_RESULT_OK)
		{
			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2,"pbsTxTask failed to send via prvPbsFlushTxBuffer\r\n");
			osMutexRelease(pbs_tx_mutex);
			return PBS_RESULT_ERROR;
		}
		local_ptr += local_length;
		osDelay(5);
	}


//	PBS_RESULT_ENUM res = prvPbsFlushTxBuffer(msg->msg, msg->msg_len);
	resMutex = osMutexRelease(pbs_tx_mutex);
	if (res != PBS_RESULT_OK)
	{
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2,"pbsTxTask failed to send via prvPbsFlushTxBuffer\r\n");
		return PBS_RESULT_ERROR;
	}
	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask sent fc %d, length %d, tx_cnt=%d\r\n", msg->flow_control, msg->msg_len, msg->tx_count);
	msg->tx_count++;
	return PBS_RESULT_OK;
}


static uint8_t send_en = 1000;

static void pbsTxTask(void const * argument)
{
	osEvent ret;
	osStatus resMutex;
	uint32_t time_sent = 0;
	uint16_t timeout = 250;
	BLE_RESULT ble_res;
	PBS_RESULT_ENUM res;

	uint8_t fc = 0;
	uint8_t tst[] = {0xAA, 0xAA, 0x13, 0x12, 0x0E, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x0, 0x00, 0x00, 0x04, 0x00, 0x86, 0x37, 0x01, 0x00, 0x01};

	for (;;)
	{
#if 0
		if (send_en)
		{
			prvPbsFlushTxBuffer(tst, sizeof(tst));
			fc++;
			tst[10] = fc;
		}
		osDelay(send_en);

#else
		ret = osMessagePeek(pbs_tx_queue_handle, osWaitForever);
		if (ret.status == osEventMessage)
		{
			PBS_TX_MESSAGE_STRUCT * msg = (PBS_TX_MESSAGE_STRUCT *)ret.value.p;

			switch (msg->tx_count)
			{
				case 0:
				{

					res = prvPbsSendTxMessage(msg);
					if (res != PBS_RESULT_OK)
						break;

					ble_res = bleCalculateSendTime(msg->msg_len, &timeout);
					if (ble_res != BLE_RESULT_OK)
						timeout = BLE_TX_DEFAULT_SEND_TIME;

					time_sent = osKernelSysTick();
					osDelay(180);

					LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, ", time_sent=%d, timeout=%d", time_sent, timeout);

				}
				break;
//				case 1:
//					res = prvPbsSendTxMessage(msg);
////					if (res != PBS_RESULT_OK)
//						osDelay(timeout);
//
//					break;
//
//				case 2:
//					prvPbsSendTxMessage(msg);
//					prvPbsTxDequeue();
//					osDelay(timeout);
//					break;

				default:	// retransmissions
				{
//					if ( (osKernelSysTick() - time_sent) < timeout)
//					{
//						break;
//					}

					res = prvPbsSendTxMessage(msg);
					if (res != PBS_RESULT_OK)
						break;

					LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, ", sent after=%d\r\n", osKernelSysTick() - time_sent);
					time_sent = osKernelSysTick();
					osDelay(180);

					LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, ", new time_sent=%d\r\n", time_sent);

				}
				break;


			}
		}
#endif   // 0
	}
}

static PBS_RESULT_ENUM prvPbsTxPeek()
{
	osEvent ret;
	osStatus resMutex;

	uint32_t n = osMessageWaiting(pbs_tx_queue_handle);
	uint16_t free_mem = xPortGetFreeHeapSize();

//	ret = osMessagePeek(pbs_tx_queue_handle, PBS_TX_MSG_TIMEOUT);
//	n = osMessageWaiting(pbs_tx_queue_handle);
	LOG("prvPbsTxPeek N = %d, free_mem = %d\r\n", n, free_mem);

//	if (ret.status == osEventMessage)
//	{
//		PBS_TX_MESSAGE_STRUCT * msg = ret.value.p;
//		PRINT("\r\nprvPbsTxPeek N = %d, fc %d, len= %d", n, msg->flow_control, msg->msg_len);
//	}
//	else
//	{
//		PRINT("\r\nprvPbsTxPeek failed or empty");
//	}

	return PBS_RESULT_OK;
}

PBS_RESULT_ENUM pbsTxTaskTest(int8_t cmd, int8_t param)
{
   if ( (pbs_output_mode == PBS_OUTPUT_TYPE_OFF) || (pbs_output_mode == PBS_OUTPUT_TYPE_SD_ONLY) )
      return PBS_RESULT_OK;

	switch (cmd)
	{
	case 0:
		pbsTxEmptyQueue();
		break;
	case 1:
	{
		prvPbsTxDequeue();
	}
	break;
	case 2:
	{
		pbsMessageAckRecieved(param);
	}
	break;
   case 3:
	{
		ECG_MEAS_REAL_TIME_PROGRESS_INDICATION_STRUCT msg;
		memset (&msg, 0, sizeof(msg));
		msg.common.ecg_index = param;

		pbsSendECGMeasurementRealtimeProgressInd(&msg, 0);

	}
	break;
   case 4:
	   prvPbsTxPeek();
	   break;
   case 5:
   {
	   PBS_TX_MESSAGE_STRUCT msg_q1, msg_q2;
	   osMessagePut(pbs_tx_queue_handle, (uint32_t)&msg_q1, 0);
	   osMessagePut(pbs_tx_queue_handle, (uint32_t)&msg_q1, 0);
	   osMessagePut(pbs_tx_queue_handle, (uint32_t)&msg_q1, 0);
	   uint32_t n = osMessageWaiting(pbs_tx_queue_handle);
      LOG("N=%d\r\n", n);
      osMessagePeek(pbs_tx_queue_handle, PBS_TX_MSG_TIMEOUT);
	   n = osMessageWaiting(pbs_tx_queue_handle);
	   osMessagePut(pbs_tx_queue_handle, (uint32_t)&msg_q1, 0);
	   n = osMessageWaiting(pbs_tx_queue_handle);
      osMessageGet(pbs_tx_queue_handle, 0);
	   n = osMessageWaiting(pbs_tx_queue_handle);
      osMessageGet(pbs_tx_queue_handle, 0);
      LOG("N=%d\r\n", n);
   }
   break;
   case 10:
   {
	   send_en = param;
   }
   break;

   case 100:
   {
      if (param == 0)
         pbsFlowControlEnable(SW_FLOW_CONTROL_OFF);
      else
         pbsFlowControlEnable(SW_FLOW_CONTROL_ON);
   }
   break;
	}

	return PBS_RESULT_OK;
}

#endif	//0

PBS_RESULT_ENUM pbsSetConnectionState(PBS_CONNECTION_STATE_ENUM new_state)
{

   pbsGetBleConnected = new_state;

   if ( (pbs_output_mode == PBS_OUTPUT_TYPE_OFF) || (pbs_output_mode == PBS_OUTPUT_TYPE_SD_ONLY) )
      return PBS_RESULT_OK;

   if (new_state >= PBS_CONNECTION_STATE_MAX)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	pbs_conn_state = new_state;

	return PBS_RESULT_OK;
}

PBS_CONNECTION_STATE_ENUM pbsGetBleConnectionState()
{
    return pbsGetBleConnected;
}
// ---------------------------------------- MEMORY POOL  END ---------------------------------

PBS_RESULT_ENUM pbsSetOutputMode(PBS_OUTPUT_TYPE_E mode)
{
   if (mode >= PBS_OUTPUT_TYPE_MAX)
      return PBS_RESULT_ERROR_WRONG_PARAM;

   pbs_output_mode = mode;

   return PBS_RESULT_OK;
}


//	---------------------------------------	ECG EVENTS - SD FLUSH	----------------------------


void sdWriteEndedEvent(uint8_t * file_name, uint32_t write_index)
{
	static uint8_t skip_count;

	// save only every 4th event message
	if ( (skip_count++ % 4) != 0)	// TODO: define 4 as buff length / Rs
		return;

	// check if new file was opened
	if (strcmp((char*)pbs_sd_file[pbs_sd_file_index], (char*)file_name) != 0)
	{
		pbs_sd_file_index = (pbs_sd_file_index + 1) % PBS_SD_FLUSH_NUM_OF_FILES;
		strncpy((char*)pbs_sd_file[pbs_sd_file_index], (char*)file_name, SD_FILE_NAME_MAX_LENGTH);
	}

	// save record
	pbs_sd_flush[pbs_sd_flush_index].file_index = write_index;
	pbs_sd_flush[pbs_sd_flush_index].file_num = pbs_sd_file_index;
	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "Pbs sdWriteEndedEvent cyc_buff_index %d, file write index %d, file num %d\r\n", pbs_sd_flush_index, write_index, pbs_sd_file_index);
	// increment index
	pbs_sd_flush_index = (pbs_sd_flush_index + 1) % PBS_SD_FLUSH_LENGTH;
}



static PBS_RESULT_ENUM prvPbsGetArrTableIndex(ECG_ALGO_ARRYTHMIA_EVENT_HEADER_STRUCT * arr, OUT uint8_t * index)
{
	if (arr == NULL || arr->raw == 0)
		return PBS_RESULT_ERROR_WRONG_PARAM;
	uint32_t tmp = 1;
	uint8_t tmp_index = 0;

	while (tmp != arr->raw)
	{
		tmp = (tmp << 1);
		(tmp_index)++;
	}

	if (tmp == arr->raw)
		*index = tmp_index;

	return PBS_RESULT_OK;
}

//static PBS_RESULT_ENUM prvPbsSendEventFromSdCard(PBS_SD_FLUSH_MSG * msg)
//{
//	if (msg == NULL)
//		return PBS_RESULT_ERROR_WRONG_PARAM;
//
//	uint8_t	*	header_loc;
//	uint16_t	header_offset;
//	uint16_t	file_read_actual_length;
//	uint16_t	file_chunk_total_length;
//	uint8_t	 *	find_loc;
//	uint32_t 	ecg_index_first;
//	uint32_t	ecg_index_last;
//	uint32_t	ecg_index_curr = 0;	//not PBS_EVENT_IND_INVALID_ECG_INDEX
//	ECG_MEAS_EVENT_PACKET_TYPE_ENUM	packet_type;
//	uint32_t	sd_file_offset_curr;
//	uint8_t curr_fc;
//	PBS_RESULT_ENUM pbs_res;
//	uint8_t 	tx_count;
//
//	uint8_t		flag_first = 1;
//
//	uint8_t		uuid_arr_table_index = -1;
//	uint32_t	uuid = -1;
//
//	if ( (msg->arr_change.change == ECG_ARRYTHMIA_CHANGE_START) || (msg->arr_change.change == ECG_ARRYTHMIA_CHANGE_STOP) )
//	{
//		pbs_res = prvPbsGetArrTableIndex(&msg->arr_change.arr, &uuid_arr_table_index);
//		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsGetArrTableIndex result %d,  index %d\r\n", pbs_res, uuid_arr_table_index);
//		if ( (pbs_res == PBS_RESULT_OK) && (uuid_arr_table_index >= 0)  && (uuid_arr_table_index < sizeof(uuid_arr_table) ) )
//		{
//			if (msg->arr_change.change == ECG_ARRYTHMIA_CHANGE_START)
//			{
//				uuid = ++uuid_arr_table[uuid_arr_table_index];
//			}
//			else if (msg->arr_change.change == ECG_ARRYTHMIA_CHANGE_STOP)
//			{
//				uuid = uuid_arr_table[uuid_arr_table_index];
//			}
//		}
//	}
//
//	flag_halt_tx = 0;	// get ready for tx
//
//	pbs_event_ind_acked_fc = PBS_EVENT_IND_INIT_FC;
//
//	tx_count = 0;
//	while (pbs_event_ind_acked_fc != (uint8_t)PBS_ECG_EVENT_IND_HEADER_FC)
//	{
//		pbs_res = prvPbsSendECGMeasurementEventIndHeader(&msg->arr_change, &msg->timestamp, uuid);
//		if (pbs_res != PBS_RESULT_OK)
//		{
//			LOG("Error Sending prvPbsSendECGMeasurementEventIndHeader\r\n");
//		}
//		if (tx_count++ >= PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT)
//		{
//			LOG("No Ack After %d retries, Aborting event flush\r\n", PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT);
//			return PBS_RESULT_ERROR_RETRY_COUNT_EXCEED_MAX;
//		}
//		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbs flush tx: fc %d, tx count %d, tx timeout %d\r\n", 255, tx_count, (flush_tx_delay_timeout + (tx_count-1)*SD_FLUSH_TIMEOUT_EXTENSION));
//		osDelay(flush_tx_delay_timeout + (tx_count-1)*SD_FLUSH_TIMEOUT_EXTENSION );
//	}
//
//
//	sd_file_offset_curr = msg->sd_file_offset;
//	// TODO: what if offset too close to end of given file
//
//
//	while (flag_halt_tx == 0)
//	{
//		SD_RESULT_E sd_res = sdReadDataFromFile(msg->sd_file_name, sizeof(file_payload), sd_file_offset_curr, file_payload, &file_read_actual_length);
//		if (sd_res != SD_RESULT_OK)
//		{
//			LOG("pbsTxTask SD read error %d\r\n", sd_res);
//			return PBS_RESULT_ERROR_SD;
//		}
//		if (file_read_actual_length == 0)
//		{
//			LOG("pbsTxTask SD read error zero bytes read\r\n");
//			return PBS_RESULT_ERROR_SD;
//		}
//		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask SD read offset %d\r\n", sd_file_offset_curr);
//
//		// TODO: if header offsets from given SD file offset - ??
//		pbs_res = prvPbsSearchHeaderInBufferMessage(file_payload, sizeof(file_payload), &header_loc);
//		if (pbs_res != PBS_RESULT_OK)
//		{
//			LOG("pbsTxTask error HEADER NOT FOUND, ABORTING\r\n");
//			return pbs_res;
//		}
//		header_offset = header_loc - file_payload;
//
//
//		// 	seek PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX - to get first index
//		pbs_res = prvPbsFindTypeInBufferMessage(header_loc, sizeof(file_payload) - header_offset, PBS_ECG_MEASUREMENT_EVENT_IND, PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX, &find_loc, &file_chunk_total_length);
//		if (pbs_res != PBS_RESULT_OK)
//		{
//			LOG("pbsTxTask error parsing byte array\r\n");
//			return pbs_res;
//		}
//
//		if (file_chunk_total_length > file_read_actual_length)
//		{
//			LOG("pbsTxTask error SD data read is not enough\r\n");
//			return PBS_RESULT_ERROR_SD;
//		}
//
//		memcpy(&ecg_index_curr, find_loc + PBS_TLV_VALUE_OFFSET, sizeof(ecg_index_curr));
//		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask ECG index found at offset %d, index = %d\r\n", (find_loc - header_loc), ecg_index_curr);
//
//		// Find Flow Control Index
//		pbs_res = prvPbsFindTypeInBufferMessage(header_loc, sizeof(file_payload) - header_offset, PBS_ECG_MEASUREMENT_EVENT_IND, PBS_ECG_MEASUREMENT_EVENT_IND_SW_FC_INDEX, &find_loc, NULL);
//		if (pbs_res != PBS_RESULT_OK)
//		{
//			LOG("pbsTxTask error parsing byte array\r\n");
//			return pbs_res;
//		}
//		curr_fc = find_loc[PBS_TLV_VALUE_OFFSET];
//		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask ECG flow_conrtol found at offset %d, curr_fc = %d\r\n", (find_loc - header_loc), curr_fc);
//
//
//		if (flag_first == 1)	// calculate last index to send
//		{
//			ecg_index_last = ecg_index_curr + ECG_BUFFER_LENGTH*msg->event_duration * 4/2; // TODO: define 4 as buffer time / sec, /2 for downsampling to 250 hz
//			flag_first = 0;
//		}
//
//		if (ecg_index_curr == ecg_index_last)
//		{
//			//	send ecg event header message
//			// 	seek PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_PACKET_TYPE - to set first packet as header
//			pbs_res = prvPbsFindTypeInBufferMessage(header_loc, sizeof(file_payload) - header_offset, PBS_ECG_MEASUREMENT_EVENT_IND, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_PACKET_TYPE, &find_loc, NULL);
//			if (pbs_res != PBS_RESULT_OK)
//			{
//				LOG("pbsTxTask error parsing byte array\r\n");
//				return pbs_res;
//			}
//			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX found at offset %d\r\n", (header_loc - file_payload) );
//			find_loc[PBS_TLV_VALUE_OFFSET] = ECG_MEAS_EVENT_PACKET_TYPE_END;
//
//			uint16_t len = file_chunk_total_length + PBS_TX_OVERHEAD;
//			pbs_res = prvPbsAddCrcToBuffer(header_loc, &len);
//			if (pbs_res != PBS_RESULT_OK)
//			{
//				LOG("pbsTxTask error prvPbsAddCrcToBuffer of last ECG event message\r\n");
//			}
//			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsSendEventFromSdCard LAST message\r\n");
//		}
//
//		tx_count = 0;
//		while ( (pbs_event_ind_acked_fc != curr_fc) && (flag_halt_tx == 0) )
//		{
//			pbs_res = prvPbsFlushTxBuffer(header_loc, file_chunk_total_length + PBS_TX_OVERHEAD + PBS_CRC16_LENGTH);
//			if (pbs_res != PBS_RESULT_OK)
//			{
//				LOG("prvPbsSendEventFromSdCard Error Sending\r\n");
//				return PBS_RESULT_ERROR_FLUSH_BLE;
//			}
//			if (tx_count++ >= PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT)
//			{
//				LOG("No Ack After %d retries, Aborting event flush\r\n", PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT);
//				return PBS_RESULT_ERROR_RETRY_COUNT_EXCEED_MAX;
//			}
//			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbs flush tx: fc %d, tx count %d, tx timeout %d\r\n", curr_fc, tx_count, (flush_tx_delay_timeout + (tx_count-1)*SD_FLUSH_TIMEOUT_EXTENSION));
//			osDelay(flush_tx_delay_timeout + (tx_count-1)*SD_FLUSH_TIMEOUT_EXTENSION);
//
//		}
//		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsSendEventFromSdCard inc sending, acked  %d\r\n", pbs_event_ind_acked_fc);
//		if (ecg_index_curr >= ecg_index_last)
//		{
//			break;
//		}
//		if (pbs_event_ind_acked_fc == curr_fc)
//		{
//			sd_file_offset_curr +=  (file_chunk_total_length + PBS_TX_OVERHEAD + PBS_CRC16_LENGTH);
//		}
//
//	}
//	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsSendEventFromSdCard finished sending\r\n");
//
//
//	return PBS_RESULT_OK;
//}


//// Events flush task
//static void pbsTxTask(void const * argument)
//{
//	osEvent os_res;
//
//	for (;;)
//	{
//		os_res = osMessageGet(pbs_tx_queue_handle, osWaitForever);
//		if (os_res.status == osEventMessage)
//		{
//			PBS_SD_FLUSH_MSG * msg = (PBS_SD_FLUSH_MSG *)os_res.value.p;
//			if (msg == NULL)
//				continue;
//
//			PBS_RESULT_ENUM pbs_res = prvPbsSendEventFromSdCard(msg);
//			if (pbs_res != PBS_RESULT_OK)
//			{
//				LOG("pbsTxTask prvPbsSdEventFlushFromFileIndex error\r\n");
//			}
//			vPortFree(msg);
//		}
//	}
//}



PBS_RESULT_ENUM pbsFlushSdEvent(ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT * arr_change, uint16_t duration, PBS_TIMESTAMP_STRUCT * timestamp)
{
//	uint8_t offset;
//	switch (arr_change->change)
//	{
//	case ECG_ARRYTHMIA_CHANGE_START:
//   case ECG_ARRYTHMIA_CHANGE_BUTTON:
//	case ECG_ARRYTHMIA_CHANGE_REMOTE:
//		// start flushing 60 seconds prior to event
//		offset = PBS_SD_FLUSH_EVENT_TIME_OFFSET_START;
//		break;
//	case ECG_ARRYTHMIA_CHANGE_STOP:
//		// start flushing 120 seconds prior to event
//		offset = PBS_SD_FLUSH_EVENT_TIME_OFFSET_END;
//		break;
//	case ECG_ARRYTHMIA_CHANGE_REMOTE_STOP:
//		flag_halt_tx = 1;
//		return PBS_RESULT_OK;
//		break;
//	default:
//	      return PBS_RESULT_ERROR_WRONG_PARAM;
//		break;
//	}
//	uint8_t index = (pbs_sd_flush_index + PBS_SD_FLUSH_LENGTH - offset) % PBS_SD_FLUSH_LENGTH;
//
//	if (pbs_sd_flush[index].file_index == PBS_EVENT_IND_FLUSH_INVALID_OFFSET)
//	{
//		LOG("PBS Error SD flush -> cyclic buffer still not ready\r\n");
//		return PBS_RESULT_ERROR_CYCLIC_BUFFER_NOT_READY;
//	}
//	if (pbs_sd_flush[index].file_num != pbs_sd_flush[pbs_sd_flush_index].file_num)
//	{
//		LOG("PBS Error SD flush -> event is saved in more than 1 file\r\n");
//		return PBS_RESULT_ERROR_CYCLIC_BUFFER_NOT_READY;
//	}
//
//	PBS_SD_FLUSH_MSG * msg;
// 	msg = pvPortMalloc(sizeof(PBS_SD_FLUSH_MSG));
//
// 	memcpy(&msg->timestamp, timestamp, sizeof(msg->timestamp));
// 	memcpy(&msg->arr_change, arr_change, sizeof(ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT) );
// 	msg->sd_file_offset = pbs_sd_flush[index].file_index;
//	strncpy((char*)msg->sd_file_name, (char*)pbs_sd_file[pbs_sd_flush[index].file_num], SD_FILE_NAME_MAX_LENGTH);
////	msg->event_type = event_type;
//	if (duration != 0)
//	{
//		msg->event_duration = duration;
//	}
//	else
//	{
//		msg->event_duration = duration_ext_for_test;	//SD_FLUSH_DURATION_DEFAULT;	// TODO: remove if not needed
//	}
//
//	osStatus ret;
//	ret = osMessagePut(pbs_tx_queue_handle, (uint32_t)msg, 0);
//	if (ret != osOK)
//	{
//		LOG("Error SD flush start message put\r\n");
//		return PBS_RESULT_ERROR_OS;
//	}

	return PBS_RESULT_OK;
}

PBS_RESULT_ENUM pbsMessageAckRecieved(uint8_t flow_control_index)
{
   if ( (pbs_output_mode == PBS_OUTPUT_TYPE_OFF) || (pbs_output_mode == PBS_OUTPUT_TYPE_SD_ONLY) )
      return PBS_RESULT_OK;
   pbs_event_ind_acked_fc = flow_control_index;
   LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsMessageAckRecieved fc= %d\r\n", pbs_event_ind_acked_fc);
   return PBS_RESULT_OK;
}



//#define SD_TEST_FILE_NAME				"test/1.txt"
//#define SD_TEST_FILE_CHUNK_SIZE			4000
//#define SD_TEST_EVENT_RECORD_LENGTH		100
PBS_RESULT_ENUM prvPbsTestSdCard()
{
//	static uint8_t 		file_payload[SD_TEST_FILE_CHUNK_SIZE];
//	uint16_t 		data_length;
//	SD_RESULT_E 	sd_res;
//	uint16_t		r_index;
//	uint16_t		r_offset = 0;
//	sd_res = sdFileExist(SD_TEST_FILE_NAME);
//	if (sd_res != SD_RESULT_OK)
//	{
//		LOG("prvPbsTestSdCard file doesn't exists\r\n");
//		return PBS_RESULT_ERROR_SD;
//	}
//
//	for (uint8_t i=0; i<50; i++)
//	{
//		sd_res = sdReadDataFromFile(SD_TEST_FILE_NAME, sizeof(file_payload), r_offset, file_payload, &data_length);
//		LOG("prvPbsTestSdCard SD read result %d for i %d\r\n", sd_res, i);
//		r_offset += data_length;
//		if (sd_res != SD_RESULT_OK)
//		{
////			return PBS_RESULT_ERROR_SD;
//			LOG("fail\r\n");
//		}
//		else
//		{
//			r_index = 0;
//			while (r_index <= data_length)
//			{
//				LOG("record tx = 0x%2x\r\n", file_payload[r_index]);
//				r_index += SD_TEST_EVENT_RECORD_LENGTH;
//			}
//		}
//	}

	return PBS_RESULT_OK;
}


PBS_RESULT_ENUM pbsTxTaskTest(int8_t cmd, int32_t param)
{
//	PBS_SD_FLUSH_MSG * msg_1;
	switch (cmd)
	{
	case 0:
	{
//		int32_t index = param;	//(pbs_sd_flush_index + PBS_SD_FLUSH_LENGTH - PBS_SD_FLUSH_EVENT_TIME_OFFSET_START) % PBS_SD_FLUSH_LENGTH;
//
//		msg_1 = pvPortMalloc(sizeof(PBS_SD_FLUSH_MSG));
//
//		msg_1->sd_file_offset = pbs_sd_flush[index].file_index;
//		strncpy((char*) msg_1->sd_file_name, (char*) pbs_sd_file[pbs_sd_flush[index].file_num], SD_FILE_NAME_MAX_LENGTH);
////		msg_1->event_type = ECG_MEAS_EVENT_TYPE_MANUAL;
//		msg_1->event_duration = 90;	// TODO: remove if not needed
//		msg_1->arr_change.change = ECG_ARRYTHMIA_CHANGE_START;
//
//		osStatus ret;
//		ret = osMessagePut(pbs_tx_queue_handle, (uint32_t) msg_1, 0);
//		if (ret != osOK)
//		{
//			LOG("Error SD flush start message put\r\n");
//			vPortFree(msg_1);
//			return PBS_RESULT_ERROR_OS;
//		}
	}
		break;
	case 1:
	{
		pbsMessageAckRecieved(param);
	}
		break;
	case 2:
	{
//		int32_t index = 0;
//
//		msg_1 = pvPortMalloc(sizeof(PBS_SD_FLUSH_MSG));
//
//		msg_1->sd_file_offset = pbs_sd_flush[index].file_index;
//		strncpy((char*) msg_1->sd_file_name, (char*) pbs_sd_file[pbs_sd_flush[index].file_num], SD_FILE_NAME_MAX_LENGTH);
//		msg_1->event_duration = (param == 0 ? 5 : param);
//		msg_1->arr_change.change = ECG_ARRYTHMIA_CHANGE_START;
//		msg_1->arr_change.arr.raw = 0;
//		msg_1->arr_change.arr.bitmap.pvc = 1;
//
//		osStatus ret;
//		ret = osMessagePut(pbs_tx_queue_handle, (uint32_t) msg_1, 0);
//		if (ret != osOK)
//		{
//			LOG("Error SD flush start message put\r\n");
//			vPortFree(msg_1);
//			return PBS_RESULT_ERROR_OS;
//		}
	}
		break;
	case 3:
	{
//		int32_t index = 0;
//
//		msg_1 = pvPortMalloc(sizeof(PBS_SD_FLUSH_MSG));
//
//		msg_1->sd_file_offset = pbs_sd_flush[index].file_index;
//		strncpy((char*) msg_1->sd_file_name, (char*) pbs_sd_file[pbs_sd_flush[index].file_num], SD_FILE_NAME_MAX_LENGTH);
//		msg_1->event_duration = (param == 0 ? 5 : param);
//		msg_1->arr_change.change = ECG_ARRYTHMIA_CHANGE_STOP;
//		msg_1->arr_change.arr.raw = 0;
//		msg_1->arr_change.arr.bitmap.pvc = 1;
//
//		osStatus ret;
//		ret = osMessagePut(pbs_tx_queue_handle, (uint32_t) msg_1, 0);
//		if (ret != osOK)
//		{
//			LOG("Error SD flush start message put\r\n");
//			vPortFree(msg_1);
//			return PBS_RESULT_ERROR_OS;
//		}
	}
		break;
	case 4:
	{
//		static uint8_t file_payload[1500];
//		uint16_t file_read_actual_length;
//
//		SD_RESULT_E sd_res;
//		PBS_RESULT_ENUM pbs_res;
//		uint8_t * header_loc;
//		uint8_t * find_loc;
//		uint16_t header_offset, file_chunk_total_length;
//		uint8_t fn[] = "AsherTest";
//
//		switch (param)
//		{
//
//		case 0: // continuous SD read
//		{
//			for (int i = 0; i < 1000; i++)
//			{
//				sd_res = sdReadDataFromFile(pbs_sd_file[pbs_sd_flush[0].file_num], sizeof(file_payload), 0, file_payload,
//						&file_read_actual_length);
//				LOG("pbsTxTask SD read result %d\r\n", sd_res);
//				osDelay(1);
//			}
//
//		}
//			break;
//
//		case 1: // continuous SD read
//		{
//			for (int i = 0; i < 1000; i++)
//			{
//				sd_res = sdWriteDataToFile(fn, file_payload, sizeof(file_payload), NULL);
//				LOG("pbsTxTask SD write result %d\r\n", sd_res);
//				osDelay(1);
//			}
//
//		}
//			break;
//		case 12:
//		{
//			for (int i = 0; i < 1000; i++)
//			{
//				sd_res = sdReadDataFromFile(pbs_sd_file[pbs_sd_flush[0].file_num], sizeof(file_payload), 0, file_payload,
//						&file_read_actual_length);
//				LOG("\r\npbsTxTask SD read result %d", sd_res);
//				sd_res = sdWriteDataToFile(fn, file_payload, sizeof(file_payload), NULL);
//				LOG("\tpbsTxTask SD write result %d\r\n", sd_res);
//				osDelay(1);
//			}
//
//		}
//			break;
//
//		case 2:	// continuous send
//		{
//			sd_res = sdReadDataFromFile(pbs_sd_file[pbs_sd_flush[0].file_num], sizeof(file_payload), 0, file_payload,
//					&file_read_actual_length);
//			LOG("pbsTxTask SD read result %d\r\n", sd_res);
//			for (int i = 0; i < 1000; i++)
//			{
//				pbs_res = prvPbsFlushTxBuffer(file_payload, file_read_actual_length);
//				LOG("prvPbsFlushTxBuffer res %d\r\n", pbs_res);
//				osDelay(1);
//			}
//
//		}
//			break;
//
//		case 3: // both SD read and TX
//		{
//			for (int i = 0; i < 1000; i++)
//			{
//				sd_res = sdReadDataFromFile(pbs_sd_file[pbs_sd_flush[0].file_num], sizeof(file_payload), 0, file_payload,
//						&file_read_actual_length);
//				LOG("pbsTxTask SD read result %d", sd_res);
//				pbs_res = prvPbsFlushTxBuffer(file_payload, file_read_actual_length);
//				LOG("\tprvPbsFlushTxBuffer res %d\r\n", pbs_res);
//				osDelay(1);
//			}
//
//		}
//			break;
//
//		case 4: // both SD read, search and TX
//		{
//			for (int i = 0; i < 100; i++)
//			{
//				sd_res = sdReadDataFromFile(pbs_sd_file[pbs_sd_flush[0].file_num], sizeof(file_payload), 0, file_payload,
//						&file_read_actual_length);
//
//				pbs_res = prvPbsSearchHeaderInBufferMessage(file_payload, sizeof(file_payload), &header_loc);
//				if (pbs_res != PBS_RESULT_OK)
//				{
//					LOG("pbsTxTask error HEADER NOT FOUND");
//				}
//				header_offset = header_loc - file_payload;
//
//				// 	seek PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX - to get first index
//				pbs_res = prvPbsFindTypeInBufferMessage(header_loc, sizeof(file_payload) - header_offset, PBS_ECG_MEASUREMENT_EVENT_IND,
//						PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX, &find_loc, &file_chunk_total_length);
//				if (pbs_res != PBS_RESULT_OK)
//				{
//					LOG("\tpbsTxTask error parsing byte array");
//				}
//
//				LOG("\tpbsTxTask SD read result %d", sd_res);
//				pbs_res = prvPbsFlushTxBuffer(file_payload, file_read_actual_length);
//				LOG("\tprvPbsFlushTxBuffer res %d", pbs_res);
//				LOG("\r\n");
//
//				osDelay(1);
//			}
//
//		}
//			break;
//
//		}

	}
		break;

	case 5:
	{
		if ((param > 0) && (param < 255))
		{
			duration_ext_for_test = param;
			LOG("duration set %d\r\n", duration_ext_for_test);
		}
		else
		{
			LOG("duration set failed (%d), [1,255]\r\n", param);
		}
	}
		break;
	case 6:
	{
		if ( (param >= 125) && (param < 5000) )	//PBS_EVENT_IND_FLUSH_TX_TIMEOUT_MSEC)
		{
			flush_tx_delay_timeout = param;
			LOG("tx timout set %d\r\n", flush_tx_delay_timeout);
		}
		else
		{
			LOG("tx timout set failed (%d), [250, 1000)\r\n", flush_tx_delay_timeout);
		}
	}
	break;
	case 7:
	{
		prvPbsTestSdCard();
	}
		break;
	case 10:
	{
		flag_halt_tx = 1;
	}
		break;
	}
	return PBS_RESULT_OK;
}



PBS_RESULT_ENUM pbsGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (pbs_task_handle == NULL) || (ret_val == NULL) )
		return PBS_RESULT_ERROR_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark( pbs_task_handle );
	return PBS_RESULT_OK;
}

PBS_RESULT_ENUM pbsTxGetHighWaterMark(UBaseType_t * ret_val)
{
	if ( (pbs_tx_task_handle == NULL) || (ret_val == NULL) )
		return PBS_RESULT_ERROR_WRONG_PARAM;
	*ret_val = uxTaskGetStackHighWaterMark( pbs_tx_task_handle );
	return PBS_RESULT_OK;
}



// save events map record



static PBS_RESULT_ENUM pbsEcgEventsMapEntryValidate(PBS_SD_EVENTS_MAP_ENTRY_STRUCT * event)
{
	if ( (event->fields.start_delim != PBS_SD_EVENT_START_DELIMITER) || (event->fields.end_delim != PBS_SD_EVENT_END_DELIMITER) )
		return PBS_RESULT_ERROR;

	return PBS_RESULT_OK;
}

static PBS_RESULT_ENUM pbsEcgEventsMapEntryLog(IN PBS_SD_EVENTS_MAP_ENTRY_STRUCT * event)
{
	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsEcgEventsMapEntryLog:\r\n"
			"delims start %d, end %d, sent %d file_name %s, offset %d\r\narr %x, "
			"event type %d, duration %d, uuid %d\r\n"
			"onset's timestamp? %s\r\n",
			event->fields.start_delim,
			event->fields.end_delim,
			event->fields.is_sent,
			event->fields.sd_file_name,
			event->fields.sd_file_offset,
			event->fields.arr_type.raw,
			event->fields.event_type,
			event->fields.event_duration,
			event->fields.event_uuid,
			prvPbsIsTimestampOk(&event->fields.onset_timestamp) == PBS_RESULT_OK ? "Y" : "N"
//			, event->fields.timestamp,
//			event->fields.additional_sd_file_name,
//			event->fields.additional_sd_file_offset
			);

	return PBS_RESULT_OK;
}

static PBS_RESULT_ENUM pbsEcgEventsMapGetNext(IN uint8_t is_load_from_sd, OUT PBS_SD_EVENTS_MAP_ENTRY_STRUCT ** event)
{
	static uint32_t		sd_card_file_running_offset = 0;
	static uint32_t		payload_running_index = 0;
	static uint16_t		data_length = 0;
	SD_RESULT_E 		sd_res;
	PBS_RESULT_ENUM		pbs_res;

	*event = NULL;		// will be set only if a valid event is found

	if (is_load_from_sd)
	{
		sd_res = sdReadDataFromFile(PBS_SD_EVENT_MAP_FILE_NAME, sizeof(events_map_file_payload), sd_card_file_running_offset, events_map_file_payload, &data_length);
		LOG("pbsEcgEventsMapGet SD read result %d, from offset %d\r\n", sd_res, sd_card_file_running_offset);

		if (sd_res != SD_RESULT_OK)
		{
			LOG("pbsEcgEventsMapGet fail, %d\r\n", sd_res);
			return PBS_RESULT_ERROR_ECG_EVENT_MAP_FILE_ERROR;
		}

		if (data_length == 0)
		{
		   	LOG("pbsEcgEventsMapGet data_length == 0\r\n");
			return PBS_RESULT_ERROR_ECG_EVENT_MAP_FILE_ERROR;
		}

		// opened successfully, reset running index
		payload_running_index = 0;
	}


	while ( (payload_running_index + sizeof(PBS_SD_EVENTS_MAP_ENTRY_STRUCT)) <= data_length)
	{
//		LOG("record tx = 0x%2x\r\n", file_payload[payload_running_index]);
		PBS_SD_EVENTS_MAP_ENTRY_STRUCT * temp_ptr = (PBS_SD_EVENTS_MAP_ENTRY_STRUCT *)&events_map_file_payload[payload_running_index];
		pbsEcgEventsMapEntryLog(temp_ptr);

		pbs_res = pbsEcgEventsMapEntryValidate(temp_ptr);

		if (pbs_res != PBS_RESULT_OK)
		{
			payload_running_index++;			// TODO: search for a valid section ?
		}
#ifdef SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
		else if(( temp_ptr->fields.is_sent == PBS_SD_EVENT_IS_SENT_YES) || ( temp_ptr->fields.is_sent == PBS_SD_EVENT_IS_SENT_NO))	// transmit every event, unless it was broken
#else	// SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
		else if( temp_ptr->fields.is_sent == PBS_SD_EVENT_IS_SENT_NO)	// event is ready to be transmitted
#endif	// SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
			{
			*event = temp_ptr;
			events_map_file_current_entry_offset = sd_card_file_running_offset + payload_running_index;
			sd_card_file_running_offset += (payload_running_index + sizeof(PBS_SD_EVENTS_MAP_ENTRY_STRUCT) );	// set for next SD card reload

			payload_running_index += sizeof(PBS_SD_EVENTS_MAP_ENTRY_STRUCT);	// increase only after events_map_file_current_entry_offset is saved
			return PBS_RESULT_OK;
		}
		else		// valid section but already sent (or failed)
		{
			payload_running_index += sizeof(PBS_SD_EVENTS_MAP_ENTRY_STRUCT);
		}
	}

	sd_card_file_running_offset += data_length;	// reached end of read section from sd card file

	return PBS_RESULT_ERROR_ECG_EVENT_NOT_FOUND_IN_MAP_FILE;	// on next call should be used with is_load_from_sd = TRUE (because last time reached end of file)

}


static PBS_RESULT_ENUM prvPbsSendEventFromSdCard(PBS_SD_EVENTS_MAP_ENTRY_STRUCT * msg)
{
	if (msg == NULL)
		return PBS_RESULT_ERROR_WRONG_PARAM;

	uint8_t	*	header_loc;
	uint16_t	header_offset;
	uint16_t	file_read_actual_length;
	uint16_t	file_chunk_total_length;
	uint8_t	 *	find_loc;
	uint32_t	ecg_index_last;
	uint32_t	ecg_index_curr = 0;	//not PBS_EVENT_IND_INVALID_ECG_INDEX
	uint32_t	sd_file_offset_curr;
	uint8_t curr_fc;
	PBS_RESULT_ENUM pbs_res;
	uint8_t 	tx_count;

	uint8_t		flag_first = 1;


	flag_halt_tx = 0;	// get ready for tx

	pbs_event_ind_acked_fc = PBS_EVENT_IND_INIT_FC;

	tx_count = 0;
	while (pbs_event_ind_acked_fc != (uint8_t)PBS_ECG_EVENT_IND_HEADER_FC)
	{
		pbs_res = prvPbsSendECGMeasurementEventIndHeader(msg);
		if (pbs_res != PBS_RESULT_OK)
		{
			LOG("Error Sending prvPbsSendECGMeasurementEventIndHeader\r\n");
		}
		if (tx_count++ >= PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT)
		{
			LOG("No Ack After %d retries, Aborting event flush\r\n", PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT);
			return PBS_RESULT_ERROR_RETRY_COUNT_EXCEED_MAX;
		}
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbs flush tx: fc %d, tx count %d, tx timeout %d\r\n", 255, tx_count, (flush_tx_delay_timeout + (tx_count-1)*SD_FLUSH_TIMEOUT_EXTENSION));
		osDelay(flush_tx_delay_timeout + (tx_count-1)*SD_FLUSH_TIMEOUT_EXTENSION );
	}


	sd_file_offset_curr = msg->fields.sd_file_offset;
	// TODO: what if offset too close to end of given file

	uint8_t * curr_file_name = msg->fields.sd_file_name;

	while (flag_halt_tx == 0)//read sdcard from end to backword and search index.
	{
		SD_RESULT_E sd_res = sdReadDataFromFile(curr_file_name, sizeof(file_payload), sd_file_offset_curr, file_payload, &file_read_actual_length);
		if (sd_res != SD_RESULT_OK)
		{
			LOG("pbsTxTask SD read error %d\r\n", sd_res);
			return PBS_RESULT_ERROR_SD;
		}
//		if (file_read_actual_length == 0)
//		{
//			LOG("pbsTxTask SD read error zero bytes read\r\n");
//			return PBS_RESULT_ERROR_SD;
//		}
		if (file_read_actual_length == 0)
		{
			if (curr_file_name == msg->fields.sd_file_name)
			{
				LOG("prvPbsSendEventFromSdCard file_read_actual_length is 0, try switching file name\r\n");
				curr_file_name = msg->fields.additional_sd_file_name;
				sd_file_offset_curr = 0;
				continue;
			}
		}

		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask SD read offset %d\r\n", sd_file_offset_curr);

		// TODO: if header offsets from given SD file offset - ??
                //search header
		pbs_res = prvPbsSearchHeaderInBufferMessage(file_payload, sizeof(file_payload), &header_loc);
		if (pbs_res != PBS_RESULT_OK)
		{
			LOG("pbsTxTask error HEADER NOT FOUND, ABORTING\r\n");
			return pbs_res;
		}
		header_offset = header_loc - file_payload;


		// 	seek PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX - to get first index
		pbs_res = prvPbsFindTypeInBufferMessage(header_loc, sizeof(file_payload) - header_offset, PBS_ECG_MEASUREMENT_EVENT_IND, PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX, &find_loc, &file_chunk_total_length);
		if (pbs_res != PBS_RESULT_OK)
		{
			LOG("pbsTxTask error parsing byte array\r\n");
			return pbs_res;
		}


		if (file_chunk_total_length > file_read_actual_length)
		{
			LOG("pbsTxTask error SD data read is not enough\r\n");
			return PBS_RESULT_ERROR_SD;
		}

		memcpy(&ecg_index_curr, find_loc + PBS_TLV_VALUE_OFFSET, sizeof(ecg_index_curr));
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask ECG index found at offset %d, index = %d\r\n", (find_loc - header_loc), ecg_index_curr);

		// Find Flow Control Index
		pbs_res = prvPbsFindTypeInBufferMessage(header_loc, sizeof(file_payload) - header_offset, PBS_ECG_MEASUREMENT_EVENT_IND, PBS_ECG_MEASUREMENT_EVENT_IND_SW_FC_INDEX, &find_loc, NULL);
		if (pbs_res != PBS_RESULT_OK)
		{
			LOG("pbsTxTask error parsing byte array\r\n");
			return pbs_res;
		}
		curr_fc = find_loc[PBS_TLV_VALUE_OFFSET];
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask ECG flow_conrtol found at offset %d, curr_fc = %d\r\n", (find_loc - header_loc), curr_fc);


		if (flag_first == 1)	// calculate last index to send
		{
			ecg_index_last = ecg_index_curr + ECG_BUFFER_LENGTH*msg->fields.event_duration * 4/2; // TODO: define 4 as buffer time / sec, /2 for downsampling to 250 hz
			flag_first = 0;
		}

		if (ecg_index_curr == ecg_index_last)
		{
			//	send ecg event header message
			// 	seek PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_PACKET_TYPE - to set first packet as header
			pbs_res = prvPbsFindTypeInBufferMessage(header_loc, sizeof(file_payload) - header_offset, PBS_ECG_MEASUREMENT_EVENT_IND, PBS_ECG_MEASUREMENT_EVENT_IND_EVENT_PACKET_TYPE, &find_loc, NULL);
			if (pbs_res != PBS_RESULT_OK)
			{
				LOG("pbsTxTask error parsing byte array\r\n");
				return pbs_res;
			}
			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsTxTask PBS_ECG_MEASUREMENT_EVENT_IND_ECG_INDEX found at offset %d\r\n", (header_loc - file_payload) );
			find_loc[PBS_TLV_VALUE_OFFSET] = ECG_MEAS_EVENT_PACKET_TYPE_END;

			uint16_t len = file_chunk_total_length + PBS_TX_OVERHEAD;
			pbs_res = prvPbsAddCrcToBuffer(header_loc, &len);
			if (pbs_res != PBS_RESULT_OK)
			{
				LOG("pbsTxTask error prvPbsAddCrcToBuffer of last ECG event message\r\n");
			}
			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsSendEventFromSdCard LAST message\r\n");
		}

		tx_count = 0;
		while ( (pbs_event_ind_acked_fc != curr_fc) && (flag_halt_tx == 0) )
		{

			pbs_res = prvPbsFlushTxBuffer(header_loc, file_chunk_total_length + PBS_TX_OVERHEAD + PBS_CRC16_LENGTH);
			if (pbs_res != PBS_RESULT_OK)
			{
				LOG("prvPbsSendEventFromSdCard Error Sending\r\n");
				return PBS_RESULT_ERROR_FLUSH_BLE;
			}
			if (tx_count++ >= PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT)
			{
				LOG("No Ack After %d retries, Aborting event flush\r\n", PBS_EVENT_IND_FLUSH_TX_MAX_RETRY_COUNT);
				return PBS_RESULT_ERROR_RETRY_COUNT_EXCEED_MAX;
			}
			LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbs flush tx: fc %d, tx count %d, tx timeout %d\r\n", curr_fc, tx_count, (flush_tx_delay_timeout + (tx_count-1)*SD_FLUSH_TIMEOUT_EXTENSION));
			osDelay(flush_tx_delay_timeout + (tx_count-1)*SD_FLUSH_TIMEOUT_EXTENSION);

		}
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsSendEventFromSdCard inc sending, acked  %d\r\n", pbs_event_ind_acked_fc);
		if (ecg_index_curr >= ecg_index_last)
		{
			break;
		}
		if (pbs_event_ind_acked_fc == curr_fc)
		{
			sd_file_offset_curr +=  (file_chunk_total_length + PBS_TX_OVERHEAD + PBS_CRC16_LENGTH);
		}

	}

	if (flag_halt_tx != 0)
		return PBS_RESULT_ERROR_TX_HALTED;

	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsSendEventFromSdCard finished sending\r\n");

	return PBS_RESULT_OK;
}

static PBS_RESULT_ENUM prvPbsSendEventFromSdCardWrapper(PBS_SD_EVENTS_MAP_ENTRY_STRUCT * pending_tx_ptr)
{
	PBS_RESULT_ENUM pbs_res;
	SD_RESULT_E sd_res;
	pbs_res = prvPbsSendEventFromSdCard(pending_tx_ptr);
	PBS_SD_EVENT_IS_SENT_ENUM sent_flag = PBS_SD_EVENT_IS_SENT_MAX;
	LOG("pbsTxTask call prvPbsSendEventFromSdCard : res %d\r\n", pbs_res);
	switch (pbs_res)
	{
	case PBS_RESULT_OK:
		sent_flag = PBS_SD_EVENT_IS_SENT_YES;
		LOG("sent_flag = PBS_SD_EVENT_IS_SENT_YES\r\n");
		break;
		//		case PBS_RESULT_ERROR_TX_HALTED:		// TODO: maybe some day, option to stop sending event - for good? temporary?
		//			sent_flag = PBS_SD_EVENT_IS_SENT_HALTED;
		//			break;
	case PBS_RESULT_ERROR_HEADER_NOT_FOUND:
	case PBS_RESULT_ERROR_TYPE_NOT_FOUND:
	case PBS_RESULT_ERROR_SD:
		//		case PBS_RESULT_ERROR_FLUSH_BLE:
		sent_flag = PBS_SD_EVENT_IS_SENT_FAILED;
		LOG("sent_flag = PBS_SD_EVENT_IS_SENT_FAILED\r\n");
		pending_tx_ptr->fields.is_sent = sent_flag;
		prvPbsSendECGMeasurementEventIndHeader(pending_tx_ptr);
		break;
	case PBS_RESULT_ERROR_RETRY_COUNT_EXCEED_MAX:
	   if (pbs_conn_state == PBS_CONNECTION_STATE_DISCONNECTED)
	   {
//		  continue;
	   }
		break;
	default:
		// no change
		break;
	}
	if (sent_flag != PBS_SD_EVENT_IS_SENT_MAX)
	{
		pending_tx_ptr->fields.is_sent = sent_flag;
		sd_res = sdReplaceDataInFile(PBS_SD_EVENT_MAP_FILE_NAME, (uint8_t *) pending_tx_ptr, sizeof(pending_tx_ptr),
				events_map_file_current_entry_offset);
		LOG("pbsTxTask call sdReplaceDataInFile res = %d\r\n", sd_res);
	}
	//		pbs_res = pbsEcgEventsMapGetNext(FALSE, &pending_tx_ptr);
	//		LOG("pbsTxTask reload from sd res %d\r\n", pbs_res);
	//
	//		if (pbs_res == PBS_RESULT_ERROR_ECG_EVENT_NOT_FOUND_IN_MAP_FILE)
	{
		pbs_res = pbsEcgEventsMapGetNext(TRUE, &pending_tx_ptr);
		LOG("pbsTxTask reload from sd res %d\r\n", pbs_res);
	}

	return PBS_RESULT_OK;
}



#ifdef SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
static uint8_t shitty_demo_flag_please_remove_me = FALSE;

PBS_RESULT_ENUM pbsSetDemoFlag()
{
	shitty_demo_flag_please_remove_me = TRUE;
}
#endif	//SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME

static PBS_RESULT_ENUM prvPbsGetOpenEvents(void)
{
	static uint32_t								sd_card_file_running_offset = 0;
	static uint32_t								payload_running_index = 0;
	static uint16_t								data_length = 0, event_index = 0;
	SD_RESULT_E 								sd_res;
	PBS_RESULT_ENUM								pbs_res;


	sd_res = sdReadDataFromFile(PBS_SD_EVENT_MAP_FILE_NAME, sizeof(events_map_file_payload), sd_card_file_running_offset, events_map_file_payload, &data_length);
	LOG("pbsEcgEventsMapGet SD read result %d, from offset %d\r\n", sd_res, sd_card_file_running_offset);

	if (sd_res != SD_RESULT_OK)
	{
		LOG("pbsEcgEventsMapGet fail, %d\r\n", sd_res);
		return PBS_RESULT_ERROR_ECG_EVENT_MAP_FILE_ERROR;
	}

	if (data_length == 0)
	{
		LOG("pbsEcgEventsMapGet data_length == 0\r\n");
		return PBS_RESULT_ERROR_ECG_EVENT_MAP_FILE_ERROR;
	}

	// opened successfully, reset running index
	payload_running_index = 0;

	while ( (payload_running_index + sizeof(PBS_SD_EVENTS_MAP_ENTRY_STRUCT)) <= data_length)
	{
		PBS_SD_EVENTS_MAP_ENTRY_STRUCT * temp_ptr = (PBS_SD_EVENTS_MAP_ENTRY_STRUCT *)&events_map_file_payload[payload_running_index];
		pbsEcgEventsMapEntryLog(temp_ptr);

		pbs_res = pbsEcgEventsMapEntryValidate(temp_ptr);
		if (pbs_res != PBS_RESULT_OK)
		{
			payload_running_index++;			// TODO: search for a valid section ?
		}
		else if (temp_ptr->fields.event_type == ECG_ARRYTHMIA_CHANGE_START)
		{
			event_index = (uint16_t)(log(temp_ptr->fields.arr_type.raw) / log(2));
			if (pbs_ecg_events[event_index].event_status == PBS_ECG_EVENT_STATUS_ON)
				LOG("\r\n");//TODO
			pbs_ecg_events[event_index].uuid = temp_ptr->fields.event_uuid;
			memcpy(pbs_ecg_events[event_index].time_stamp.data, temp_ptr->fields.timestamp.data, sizeof(pbs_ecg_events[event_index].time_stamp.data));
			pbs_ecg_events[event_index].event_status = PBS_ECG_EVENT_STATUS_ON;

		}
		else if (temp_ptr->fields.event_type == ECG_ARRYTHMIA_CHANGE_STOP)
		{
			event_index = (uint16_t)(log(temp_ptr->fields.arr_type.raw) / log(2));
			if ((pbs_ecg_events[event_index].event_status == PBS_ECG_EVENT_STATUS_ON) &&
				(pbs_ecg_events[event_index].uuid == temp_ptr->fields.event_uuid))
			{
				pbs_ecg_events[event_index].event_status = PBS_ECG_EVENT_STATUS_OFF;
			}
			else
				LOG("No onset or wrong UUID, old UUID: %d, new UUID:%d\r\n", temp_ptr->fields.event_uuid, pbs_ecg_events[event_index].uuid);

		}
		else		// valid section but already sent (or failed)
		{
			payload_running_index += sizeof(PBS_SD_EVENTS_MAP_ENTRY_STRUCT);
		}
		payload_running_index += sizeof(PBS_SD_EVENTS_MAP_ENTRY_STRUCT);
	}

	sd_card_file_running_offset += data_length;	// reached end of read section from sd card file

	return PBS_RESULT_ERROR_ECG_EVENT_NOT_FOUND_IN_MAP_FILE;	// on next call should be used with is_load_from_sd = TRUE (because last time reached end of file)

}

PBS_RESULT_ENUM pbsCheckArrhythmia(ECG_ALGO_ARRYTHMIA_EVENT_HEADER_STRUCT current_arr)
{
	uint8_t 									i;
	ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT 	arrh_changes;
	PBS_TIMESTAMP_STRUCT 						timestamp;
	BSP_RTC_TIMESTAMP 							time;
	BSP_RESULT_E 								bsp_res;


	for(i = 0;i < PBS_EVENT_ARR_TABLE_SIZE;i++)
	{
		if (pbs_ecg_events[i].event_status == PBS_ECG_EVENT_STATUS_ON)
		{
			if (!(current_arr.raw & (1 << i)))
			{
				bsp_res = bspRtcTimeGetBCD(&time);
				if (bsp_res == BSP_RESULT_OK)
					memcpy(timestamp.data, time.raw, sizeof(timestamp.data));
				else
					memset(timestamp.data, PBS_TIMESTAMP_INVALID_PATTERN, sizeof(timestamp.data));

				arrh_changes.arr.raw |= (1 << i);
				arrh_changes.change = ECG_ARRYTHMIA_CHANGE_STOP;
				pbsSaveEventEntryToSdCard(&arrh_changes, 0, &timestamp);
				memset((uint8_t*)&arrh_changes, 0, sizeof(arrh_changes));
			}
		}

	}

	return PBS_RESULT_OK;
}

// Events flush task
static void pbsTxTask(void const * argument)
{
	PBS_RESULT_ENUM pbs_res;
	PBS_SD_EVENTS_MAP_ENTRY_STRUCT * pending_tx_ptr = NULL;
	SD_RESULT_E sd_res;

	prvPbsGetOpenEvents();

	pbs_res = pbsEcgEventsMapGetNext(TRUE, &pending_tx_ptr);
	while (pbs_res == PBS_RESULT_ERROR_ECG_EVENT_NOT_FOUND_IN_MAP_FILE)
	{
		pbs_res = pbsEcgEventsMapGetNext(TRUE, &pending_tx_ptr);
	}
	LOG("pbsTxTask init: load from sd res %d, pending tx=%d\r\n", pbs_res, (pending_tx_ptr == NULL ? 0 : 1));

	osDelay(5*PBS_TX_TASK_DELAY_MS);

	for (;;)
	{
		osDelay(5*PBS_TX_TASK_DELAY_MS);
		if (pbs_conn_state == PBS_CONNECTION_STATE_DISCONNECTED)
			continue;

#ifdef 	SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
		if (shitty_demo_flag_please_remove_me == FALSE)
			continue;
#endif 	// SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME

		if (pending_tx_ptr == NULL)
		{
			if (new_event_pending_flag == FALSE)
				continue;

			pbs_res = pbsEcgEventsMapGetNext(TRUE, &pending_tx_ptr);
			LOG("pbsTxTask reload from sd res %d\r\n", pbs_res);

			// managed to get event according to flag, additional events should raise flag again, or collected later on
			new_event_pending_flag = FALSE;

			if (pending_tx_ptr == NULL)
			{
				LOG("pbsTxTask pending_tx_ptr is NULL\r\n");
				continue;
			}
		}
		// pending_tx_ptr should not be null here
		pbs_res = prvPbsSendEventFromSdCard(pending_tx_ptr);
		PBS_SD_EVENT_IS_SENT_ENUM sent_flag = PBS_SD_EVENT_IS_SENT_MAX;
		LOG("pbsTxTask call prvPbsSendEventFromSdCard : res %d\r\n", pbs_res);
		switch (pbs_res)
		{
		case PBS_RESULT_OK:
			sent_flag = PBS_SD_EVENT_IS_SENT_YES;
			LOG("sent_flag = PBS_SD_EVENT_IS_SENT_YES\r\n");
			break;
//		case PBS_RESULT_ERROR_TX_HALTED:		// TODO: maybe some day, option to stop sending event - for good? temporary?
//			sent_flag = PBS_SD_EVENT_IS_SENT_HALTED;
//			break;
		case PBS_RESULT_ERROR_HEADER_NOT_FOUND:
		case PBS_RESULT_ERROR_TYPE_NOT_FOUND:
		case PBS_RESULT_ERROR_SD:
//		case PBS_RESULT_ERROR_FLUSH_BLE:
			sent_flag = PBS_SD_EVENT_IS_SENT_FAILED;
			LOG("sent_flag = PBS_SD_EVENT_IS_SENT_FAILED\r\n");
			pending_tx_ptr->fields.is_sent = sent_flag;
			prvPbsSendECGMeasurementEventIndHeader(pending_tx_ptr);
			break;
		case PBS_RESULT_ERROR_RETRY_COUNT_EXCEED_MAX:
			if (pbs_conn_state == PBS_CONNECTION_STATE_DISCONNECTED)
				continue;
			sent_flag = PBS_SD_EVENT_IS_SENT_FAILED;	// event might be corrupted
			break;
		default:
			// no change
			break;
		}
#ifdef 	SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
		shitty_demo_flag_please_remove_me = FALSE;
#else 	// SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
		if (sent_flag != PBS_SD_EVENT_IS_SENT_MAX)
		{
			pending_tx_ptr->fields.is_sent = sent_flag;
			sd_res = sdReplaceDataInFile(PBS_SD_EVENT_MAP_FILE_NAME, (uint8_t *)pending_tx_ptr, sizeof(pending_tx_ptr), events_map_file_current_entry_offset);
			LOG("pbsTxTask call sdReplaceDataInFile res = %d\r\n", sd_res);
		}
#endif 	// SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
//		pbs_res = pbsEcgEventsMapGetNext(FALSE, &pending_tx_ptr);
//		LOG("pbsTxTask reload from sd res %d\r\n", pbs_res);
//
//		if (pbs_res == PBS_RESULT_ERROR_ECG_EVENT_NOT_FOUND_IN_MAP_FILE)
		{
			pbs_res = pbsEcgEventsMapGetNext(TRUE, &pending_tx_ptr);
			LOG("pbsTxTask reload from sd res %d\r\n", pbs_res);
		}
	}
//	LOG("pbsTxTask task ended\r\n", pbs_res);
}

static PBS_RESULT_ENUM prvPbsIsTimestampOk(PBS_TIMESTAMP_STRUCT * timestamp)
{
	for (int i=0; i< sizeof(timestamp->data); i++)
		if (timestamp->data[i] != PBS_TIMESTAMP_INVALID_PATTERN )
			return PBS_RESULT_OK;
	return PBS_RESULT_ERROR_TIMESTAMP_EMPTY;
}

PBS_RESULT_ENUM pbsSaveEventEntryToSdCard(ECG_ALGO_ARRYTHMIA_TRACK_CHANGES_STRUCT * arr_change, uint16_t duration, PBS_TIMESTAMP_STRUCT * timestamp)
{
	static PBS_SD_EVENTS_MAP_ENTRY_STRUCT 	new_event;
	uint8_t 								index;
	uint8_t									uuid_arr_table_index = -1;
	PBS_RESULT_ENUM 						pbs_res;

	if ( (arr_change == NULL) || (timestamp == NULL) )
		return PBS_RESULT_ERROR_WRONG_PARAM;

#ifdef 	SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME
	if ( (arr_change->change == ECG_ARRYTHMIA_CHANGE_BUTTON) || (arr_change->change == ECG_ARRYTHMIA_CHANGE_REMOTE) )
		pbsSetDemoFlag();

//	if (arr_change->change != ECG_ARRYTHMIA_CHANGE_REMOTE)
		return PBS_RESULT_OK;
#endif	// SHITTY_DEMO_BUTTON_PLEASE_REMOVE_ME

	memset (&new_event, 0, sizeof(new_event));

	index = ((pbs_sd_flush_index + PBS_SD_FLUSH_LENGTH - PBS_SD_FLUSH_EVENT_TIME_OFFSET_START) % PBS_SD_FLUSH_LENGTH) - 1;
	if (pbs_sd_flush[index + 1].file_index == PBS_EVENT_IND_FLUSH_INVALID_OFFSET)
	{
		if  ((arr_change->change == ECG_ARRYTHMIA_CHANGE_START) && (pbs_ecg_events[uuid_arr_table_index].event_status == PBS_ECG_EVENT_STATUS_ON))
		{
			LOG("pbsSaveEventEntryToSdCard, event is already on\r\n");
			return PBS_RESULT_ERROR_ECG_EVENT_IS_ALREADY_ON;//In first 60 seconds if there is an ECG event already there is no need to add another onset
		}
		else
			duration = index + (SD_FLUSH_DURATION_DEFAULT - PBS_SD_FLUSH_EVENT_TIME_OFFSET_START);
	}

	if ( (arr_change->change == ECG_ARRYTHMIA_CHANGE_START) || (arr_change->change == ECG_ARRYTHMIA_CHANGE_STOP) )
	{
		pbs_res = prvPbsGetArrTableIndex(&arr_change->arr, &uuid_arr_table_index);
		LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "prvPbsGetArrTableIndex result %d,  index %d\r\n", pbs_res, uuid_arr_table_index);
		if ( (pbs_res == PBS_RESULT_OK) && (uuid_arr_table_index >= 0)  && (uuid_arr_table_index < PBS_EVENT_ARR_TABLE_SIZE ) )
		{
			if ((pbs_sd_flush[index + 1].file_index == PBS_EVENT_IND_FLUSH_INVALID_OFFSET) && (arr_change->change == ECG_ARRYTHMIA_CHANGE_STOP) && (pbs_ecg_events[uuid_arr_table_index].event_status == PBS_ECG_EVENT_STATUS_ON))
			{
				LOG("Event %d is open from last start and now is off\r\n", uuid_arr_table_index);//In first 60 seconds if there is no ECG event that was before startup write offset
				duration = index + (SD_FLUSH_DURATION_DEFAULT - PBS_SD_FLUSH_EVENT_TIME_OFFSET_START);
			}

			if (arr_change->change == ECG_ARRYTHMIA_CHANGE_START)
			{
				pbs_ecg_events[uuid_arr_table_index].uuid++;
				memcpy(pbs_ecg_events[uuid_arr_table_index].time_stamp.data, timestamp->data, sizeof(PBS_TIMESTAMP_STRUCT));
				new_event.fields.event_uuid = pbs_ecg_events[uuid_arr_table_index].uuid;

				LOG("Onset event %d added\r\n", uuid_arr_table_index);
			}
			else if (arr_change->change == ECG_ARRYTHMIA_CHANGE_STOP)
			{
				if (prvPbsIsTimestampOk(&pbs_ecg_events[uuid_arr_table_index].time_stamp) == PBS_RESULT_OK)
				{
					LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsSaveEventEntryToSdCard copy onset timestamp to offset header\r\n");
					memcpy(new_event.fields.onset_timestamp.data, pbs_ecg_events[uuid_arr_table_index].time_stamp.data, sizeof(PBS_TIMESTAMP_STRUCT));
				}
				else
				{
					LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "pbsSaveEventEntryToSdCard set timestamp offset header zero\r\n");
					memset(new_event.fields.onset_timestamp.data, PBS_TIMESTAMP_INVALID_PATTERN, sizeof(PBS_TIMESTAMP_STRUCT));
				}

				new_event.fields.event_uuid = pbs_ecg_events[uuid_arr_table_index].uuid;

				LOG("Offset event %d added\r\n", uuid_arr_table_index);
			}
		}
	}

	memcpy(&new_event.fields.timestamp, timestamp, sizeof(new_event.fields.timestamp));
	memcpy(&new_event.fields.event_type, &arr_change->change, sizeof(new_event.fields.event_type));
	memcpy(&new_event.fields.arr_type, &arr_change->arr, sizeof(new_event.fields.arr_type));

	new_event.fields.sd_file_offset = pbs_sd_flush[index].file_index;

	strncpy((char*) new_event.fields.sd_file_name, (char*) pbs_sd_file[pbs_sd_flush[index].file_num], SD_FILE_NAME_MAX_LENGTH);

	if (duration != 0)
	{
		new_event.fields.event_duration = duration;
	}
	else
	{
		new_event.fields.event_duration = duration_ext_for_test;	//SD_FLUSH_DURATION_DEFAULT;	// TODO: remove if not needed
	}

	new_event.fields.start_delim = PBS_SD_EVENT_START_DELIMITER;
	new_event.fields.end_delim = PBS_SD_EVENT_END_DELIMITER;
	new_event.fields.is_sent = PBS_SD_EVENT_IS_SENT_NO;



	SD_RESULT_E sd_res;
//	sd_res = sdWriteDataToFile(PBS_SD_EVENT_MAP_FILE_NAME, (uint8_t *)&new_event, sizeof(new_event), NULL);

	sd_res = sdSendMsgToQueue(PBS_SD_EVENT_MAP_FILE_NAME, (uint8_t *)&new_event, sizeof(new_event));
	if (sd_res != SD_RESULT_OK)
	{
		LOG("Failed to append new event to events map file on SD card\r\n");
		return PBS_RESULT_ERROR_SD;
	}

	new_event_pending_flag = TRUE;
	LOG_D(DEBUG_MODULE_PBS, DEBUG_LEVEL_2, "Added new ECG event entry to SD card queue successfully, new_event_pending_flag = TRUE\r\n");

	return PBS_RESULT_OK;
}


#define PBS_SD_ECG_STATISTICS_FILE_NAME		"stats.tlv"
static uint8_t								pbs_ecg_statistics_flush_flag = 0;

PBS_RESULT_ENUM pbsFlushStatisticsStart()
{
	SD_RESULT_E sd_res = sdFileExist(PBS_SD_ECG_STATISTICS_FILE_NAME);

	if (sd_res == SD_RESULT_ERROR_FILE_DOES_NOT_EXIST)
		return PBS_RESULT_ERROR_STATISTICS_FILE_DOES_NOT_EXISTS;
	if (sd_res != SD_RESULT_OK)
		return PBS_RESULT_ERROR_STATISTICS_FILE_ERROR;

	if (pbs_ecg_statistics_flush_flag == TRUE)
		return PBS_RESULT_ERROR_STATISTICS_FILE_DOES_NOT_EXISTS;

	pbs_ecg_statistics_flush_flag = 1;

	return PBS_RESULT_OK;
}


static PBS_RESULT_ENUM prvPbsFlushStatistics()
{
	// 	while flag is on

	// 	if not enough (end of buff / incomplete message) load from file

	//

	return PBS_RESULT_OK;
}


void pbsSetHourlyMode(PBS_HOURLY_TYPE_E command)
{
  pbs_send_hourly_mode = command;
}


//remote event:(from ble)
PBS_RESULT_ENUM pbsHolterRemoteEvent(PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENUM command, uint16_t duration)
{
	pbs_holter_remote_event = command;

	if ((pbs_holter_remote_event == PBS_COMMAND_ECG_MEASUREMENT_REMOTE_EVENT_SNAPSHOT_ENABLE) && (duration == 0))
		duration = PBS_SD_EVENT_DURATION_DEFAULT;//default duration

	pbs_holter_remote_event_packets = duration * 2; //every seond we send 2 packets

	return PBS_RESULT_OK;
}
PBS_RESULT_ENUM pbsSetFileDurationLen(uint8_t duration)
{

	if ((duration > PBS_SD_FILE_DURATION_MAX) || (duration < PBS_SD_FILE_DURATION_MIN))
		return PBS_RESULT_ERROR_WRONG_PARAM;

	LOG("Previous duration: %d hours\r\n", pbs_file_duration);
	pbs_file_duration = duration;
	LOG("New duration: %d hours\r\n", pbs_file_duration);
	return PBS_RESULT_OK;
}

static bool shouldStop(bool panicButton)
{
	return ((!panicButton) && // check if functin is called becuase of panic button event
			((xEventGroupGetBits(getPanicButton()) & (BIT_0)) == (BIT_0))); // check if panic event in not on
}

PBS_RESULT_ENUM prvPbsReadLastMinutes(uint8_t minutes, bool panicButton)
{
	static uint32_t 		sd_file_offset = 0;
	uint16_t	 			actual_length_read = 0;
	uint32_t				file_size = 0;
    SD_RESULT_E	            sd_res = SD_RESULT_OK;
	uint32_t 				file_location = 0;
	PBS_RESULT_ENUM 		result = PBS_RESULT_OK;
	uint8_t					currFile[15];

	LOG("Entered prvPbsReadLastMinutes with minutes %d, panicButton: %d \r\n", minutes, panicButton);
	LOG("PBS_PACKETS_COUNTER_IN_HALF_SECONDS(minutes) is: %d \r\n", PBS_PACKETS_COUNTER_IN_HALF_SECONDS(minutes));

	if(shouldStop(panicButton))
	{
		return result;
	}
	LOG("prvPbsReadLastMinutes is permited to run\r\n");
	    // check if ECGxx.tlv file exist:

	if (sdFileExist(pbs_current_ecg_file) == SD_RESULT_OK) //file exist and no SD_RESULT_ERROR_FILE_DOES_NOT_EXIST
    {
    	sd_res = sdGetFileSize(pbs_current_ecg_file, &file_size);
	}else
	{
		LOG("Currnet file does not exist\r\n");
		return PBS_RESULT_ERROR_FILE_READ_ERROR;
	}

	sprintf((char*)currFile, PBS_SD_ECG_FILE_FORMAT, pbs_current_ecg_file_num);
	file_location = file_size; // start reading from the end of the file
	uint16_t i, half_sec_inc = 0;
	LOG("prvPbsReadLastMinutes start reading from file\r\n");
	//each paket represents 0.5 second,
	//count number of AAAA in buffer and add number to counter.
	// this while will point to the start of last 10 minutes:
	while( (!shouldStop(panicButton)) && // check if panic event in not on
			(file_location > 0) &&     // check if there is still data to read from file
		    (half_sec_inc < PBS_PACKETS_COUNTER_IN_HALF_SECONDS(minutes)) )  // check if we reached 10 minutes
	{
		if(file_location < sizeof(file_payload)) // set reader head to the start of the file
		{
			sd_file_offset = 0;
		}
		else
		{
			sd_file_offset = file_location - sizeof(file_payload);//move reader head back 1500 bytes
															//read buffer is 1500 bytes
		}	

		sd_res = sdReadDataFromFile(currFile, sizeof(file_payload), sd_file_offset, file_payload, &actual_length_read);//TODO does not check if in a start of a file
		if(sd_res != SD_RESULT_OK)
		{
			LOG("prvPbsReadLastMinutes reading from file douring hourly send failed\r\n");
			return PBS_RESULT_ERROR_FILE_READ_ERROR;
		}

		for(i = actual_length_read ; i > 0 ; i--)
		{
			if((file_payload[i] == 0xAA) && (file_payload[i-1] == 0xAA))
			{
			half_sec_inc++;
			file_location = sd_file_offset + i - 1; // set index to the first 0xAAAA found
			}
			if(half_sec_inc >= PBS_PACKETS_COUNTER_IN_HALF_SECONDS(minutes))
			{
			break;
			}
		}

		if( (file_location == 0) && // we reached the begining of current file
			(pbs_current_ecg_file_num > 0) && // lower bound of file numbers is 0
			(half_sec_inc < PBS_PACKETS_COUNTER_IN_HALF_SECONDS(minutes))) // if current file is not big enugh we will have to go to a previous file
		{
			sprintf((char*)currFile, PBS_SD_ECG_FILE_FORMAT, pbs_current_ecg_file_num - 1);
			if(sdFileExist(currFile) != SD_RESULT_OK)
			{
				sprintf((char*)currFile, PBS_SD_ECG_FILE_FORMAT, pbs_current_ecg_file_num);
			}else
			{
				sdGetFileSize(currFile, &file_size);
				file_location = file_size;
				LOG("Current file number %d is too small, moving to previous file %s\r\n", pbs_current_ecg_file_num, currFile);
			}
		}
	}

	uint16_t len;
	LOG("Start packets sending from file %s, in file_location %d, amount of half seconds to send is %d, the last created file until now is %s.\r\n", currFile, file_location, half_sec_inc, pbs_current_ecg_file);
    pbsSetHourlyMode(PBS_OUTPUT_TYPE_BLE_HOURLY); // enable BLE sending
	while( (!shouldStop(panicButton)) && // check if panic event in not on
			(half_sec_inc > 0) ) // check if there are still packets to send
	{
		if(file_location >= file_size && strcmp(currFile, pbs_current_ecg_file) != 0) // we reached end of currnt file
		{
			sprintf((char*)currFile, PBS_SD_ECG_FILE_FORMAT, pbs_current_ecg_file_num);
			if(sdFileExist(currFile) != SD_RESULT_OK)
			{
				break;
			}
			LOG("Current file location %d, current file size %d, therefore, moving to next file %s.\r\n", file_location, file_size, currFile);
			sdGetFileSize(currFile, &file_size);
			file_location = 0;
		}

		//read first packet in buffer
		sd_res = sdReadDataFromFile(currFile, sizeof(file_payload), file_location, file_payload, &actual_length_read);
		if(sd_res != SD_RESULT_OK)
		{
			LOG("Reading from file douring hourly send failed\r\n");
			return PBS_RESULT_ERROR_FILE_READ_ERROR;
		}

		//read length from packet
		sd_res = sdReadDataFromFile(currFile, sizeof(len), file_location + 4, (uint8_t *)&len, &actual_length_read);
		if(sd_res != SD_RESULT_OK)
		{
			LOG("Reading from file douring hourly send failed\r\n");
			return PBS_RESULT_ERROR_FILE_READ_ERROR;
		}

		//send packet
		result = prvPbsFlushTxBuffer(file_payload, len + 8);

		if (result != PBS_RESULT_OK)
		{
			LOG("prvPbsSendEventFromSdCard Error Sending\r\n");
			break;
		}

		//move to next packet
		file_location += 8 + len;
		half_sec_inc--;
	}
    pbsSetHourlyMode(PBS_OUTPUT_TYPE_HOURLY_OFF); // disable BLE sending
	LOG("Finish packets sending.\r\n");
	turn_off_BLE_after_delay(30);
	return result;
}
