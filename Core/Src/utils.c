#include "utils.h"
//#include "common.h"


uint8_t ascii2Hex(uint8_t char1, uint8_t char2)
{
	uint8_t num = 0;
	if (((char1 >= 0x30)) && ((char1 <= 0x39)))
		num = (char1 - 0x30) << 4;
	else if (((char1 >= 0x61)) && ((char1 <= 0x66)))
		num = (char1 - 0x57) << 4;
	else
		return 0; //TODO:Return error

	if (((char2 >= 0x30)) && ((char2 <= 0x39)))
		num += (char2 - 0x30);
	else if (((char2 >= 0x61)) && ((char2 <= 0x66)))
		num += (char2 - 0x57);
	else
		return 0; //TODO:Return error
	
	return num;
}